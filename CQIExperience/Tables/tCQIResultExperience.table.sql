CREATE TABLE [CQIExperience].[tCQIResultExperience] (
    [CQIResultExperienceID] INT      IDENTITY (1, 1) NOT NULL,
    [CQIResultID]           INT      NOT NULL,
    [CQIExperienceID]       INT      NOT NULL,
    [NumTimestamps]         INT      NOT NULL,
    [SumSquaredError]       REAL     NOT NULL,
    [ExperienceTimestamp]   DATETIME NOT NULL,
    [ExperienceValue]       REAL     NOT NULL,
    CONSTRAINT [PK_tCQIResultExperience] PRIMARY KEY CLUSTERED ([CQIResultExperienceID] ASC),
    CONSTRAINT [FK_tCQIResultExperience_CQIExperienceID_tCQIExperience] FOREIGN KEY ([CQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tCQIResultExperience_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIExperience', @level1type = N'TABLE', @level1name = N'tCQIResultExperience';

