CREATE TABLE [CQIExperience].[tCQIExperienceTimestamp] (
    [CQIExperienceTimestampID] INT      IDENTITY (1, 1) NOT NULL,
    [UnitID]                   INT      NOT NULL,
    [FuelBasinID]              INT      NOT NULL,
    [ExperienceTimestamp]      DATETIME NOT NULL,
    CONSTRAINT [PK_tCQIExperienceTimestamp] PRIMARY KEY CLUSTERED ([CQIExperienceTimestampID] ASC),
    CONSTRAINT [FK_tCQIExperienceTimestamp_FuelBasinID_tFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID]),
    CONSTRAINT [FK_tCQIExperienceTimestamp_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIExperience', @level1type = N'TABLE', @level1name = N'tCQIExperienceTimestamp';

