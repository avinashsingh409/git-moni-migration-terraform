CREATE TABLE [CQIExperience].[tCQIExperienceType] (
    [CQIExperienceTypeID] INT            NOT NULL,
    [CQIExperienceDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tCQIExperienceType] PRIMARY KEY CLUSTERED ([CQIExperienceTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'CQIExperience', @level1type = N'TABLE', @level1name = N'tCQIExperienceType';

