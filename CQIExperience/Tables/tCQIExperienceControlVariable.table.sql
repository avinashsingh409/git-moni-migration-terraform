CREATE TABLE [CQIExperience].[tCQIExperienceControlVariable] (
    [CQIExperienceControlVariableID] INT  IDENTITY (1, 1) NOT NULL,
    [ControlCQIExperienceID]         INT  NOT NULL,
    [ManipulatedCQIExperienceID]     INT  NOT NULL,
    [SensitivityMultiplier]          REAL NOT NULL,
    CONSTRAINT [PK_tCQIExperienceControlVariable] PRIMARY KEY CLUSTERED ([CQIExperienceControlVariableID] ASC),
    CONSTRAINT [FK_tCQIExperienceControlVariable_ControlCQIExperienceID_tCQIExperience] FOREIGN KEY ([ControlCQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID]),
    CONSTRAINT [FK_tCQIExperienceControlVariable_ManipulatedCQIExperienceID_tCQIExperience] FOREIGN KEY ([ManipulatedCQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIExperience', @level1type = N'TABLE', @level1name = N'tCQIExperienceControlVariable';

