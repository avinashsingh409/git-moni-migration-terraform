CREATE TABLE [CQIExperience].[tCQIExperienceTimestampData] (
    [CQIExperienceTimestampDataID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIExperienceTimestampID]     INT  NOT NULL,
    [CQIExperienceID]              INT  NOT NULL,
    [ReferenceData]                REAL NOT NULL,
    CONSTRAINT [PK_tCQIExperienceTimestampData] PRIMARY KEY CLUSTERED ([CQIExperienceTimestampDataID] ASC),
    CONSTRAINT [FK_tCQIExperienceTimestampData_CQIExperienceID_tCQIExperience] FOREIGN KEY ([CQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID]),
    CONSTRAINT [FK_tCQIExperienceTimestampData_CQIExperienceTimestampID_tCQIExperienceTimestamp] FOREIGN KEY ([CQIExperienceTimestampID]) REFERENCES [CQIExperience].[tCQIExperienceTimestamp] ([CQIExperienceTimestampID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIExperience', @level1type = N'TABLE', @level1name = N'tCQIExperienceTimestampData';

