CREATE TABLE [CQIExperience].[tCQIExperience] (
    [CQIExperienceID]       INT            IDENTITY (1, 1) NOT NULL,
    [CQIExperienceDesc]     NVARCHAR (255) NOT NULL,
    [UnitID]                INT            NOT NULL,
    [PDTagID]               INT            NULL,
    [NormalizationMinimum]  REAL           NOT NULL,
    [NormalizationMaximum]  REAL           NOT NULL,
    [CQIExperienceTypeID]   INT            NOT NULL,
    [LowerMargin]           REAL           NOT NULL,
    [UpperMargin]           REAL           NOT NULL,
    [LocalTable]            NVARCHAR (255) NOT NULL,
    [LocalColumn]           NVARCHAR (255) NOT NULL,
    [LocalDateFilterColumn] NVARCHAR (255) NOT NULL,
    [IsFilter]              BIT            CONSTRAINT [DF_tCQIExperience_IsFilter] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCQIExperience] PRIMARY KEY CLUSTERED ([CQIExperienceID] ASC),
    CONSTRAINT [FK_tCQIExperience_CQIExperienceTypeID_tCQIExperienceType] FOREIGN KEY ([CQIExperienceTypeID]) REFERENCES [CQIExperience].[tCQIExperienceType] ([CQIExperienceTypeID]),
    CONSTRAINT [FK_tCQIExperience_PDTagID_tPDTag] FOREIGN KEY ([PDTagID]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tCQIExperience_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIExperience', @level1type = N'TABLE', @level1name = N'tCQIExperience';

