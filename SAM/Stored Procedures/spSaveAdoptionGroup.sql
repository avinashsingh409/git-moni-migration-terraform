﻿
CREATE PROCEDURE [SAM].[spSaveAdoptionGroup]
	@AdoptionGroupID INT = NULL,
	@ProjectID INT,
	@AdoptionGroupName NVARCHAR(50) = NULL,
	@AdoptionGroupDesc NVARCHAR(255) = NULL,
	@AdoptionTargets NVARCHAR(200) = NULL,
	@MarketAcceptance NVARCHAR(500) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @AdoptionGroupID IS NULL
	BEGIN
		-- insert the adoption group
		IF @AdoptionGroupName IS NULL
			RAISERROR (N'Cannot create the Adoption Group without a name', 16, 1, 'SAM.spSaveAdoptionGroup')
		IF @AdoptionGroupDesc IS NULL
			RAISERROR (N'Cannot create the Adoption Group without a description', 16, 1, 'SAM.spSaveAdoptionGroup')

		INSERT INTO SAM.tAdoptionGroup
		(ProjectID, AdoptionGroupName, AdoptionGroupDesc, AdoptionTargets, MarketAcceptance)
		VALUES
		(@ProjectID, @AdoptionGroupName, @AdoptionGroupDesc, @AdoptionTargets, @MarketAcceptance)

		SELECT @AdoptionGroupID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 * FROM SAM.tAdoptionGroup WHERE AdoptionGroupID = @AdoptionGroupID)
			RAISERROR (N'Could not update AdoptionGroup, invalid ID', 16, 1, 'SAM.spSaveAdoptionGroup')

		DECLARE @oldProjectID INT;
		DECLARE @oldName NVARCHAR(50);
		DECLARE @oldDesc NVARCHAR(255);
		DECLARE @oldTargets NVARCHAR(200);
		DECLARE @oldAcceptance NVARCHAR(500);

		SELECT TOP 1
			@oldProjectID = ProjectID,
			@oldName = AdoptionGroupName,
			@oldDesc = AdoptionGroupDesc,
			@oldTargets = AdoptionTargets,
			@oldAcceptance = MarketAcceptance
		FROM SAM.tAdoptionGroup
		WHERE AdoptionGroupID = @AdoptionGroupID

		UPDATE SAM.tAdoptionGroup
		SET
			ProjectID = COALESCE(@ProjectID, @oldProjectID),
			AdoptionGroupName = COALESCE(@AdoptionGroupName, @oldName),
			AdoptionGroupDesc = COALESCE(@AdoptionGroupDesc, @oldDesc),
			AdoptionTargets = COALESCE(@AdoptionTargets, @oldtargets),
			MarketAcceptance = COALESCE(@MarketAcceptance, @oldAcceptance)
		WHERE AdoptionGroupID = @AdoptionGroupID

	END

	SELECT @AdoptionGroupID;
END
GO

GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spSaveAdoptionGroup] TO [TEUser]
    AS [dbo];

