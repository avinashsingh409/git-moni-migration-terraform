
CREATE PROCEDURE [SAM].[spSaveSAMScenarioRun]
	@SAMScenarioRunID int = null,
	@SAMScenarioInputSetID int,
	@SAMSiteID int,
	@RunStatus int,
	@LastModified DateTime,
	@NewSAMScenarioRunID int output
AS
BEGIN

	SET @NewSAMScenarioRunID = -1;

	IF (@SAMScenarioRunID IS NULL)
	BEGIN
		INSERT INTO SAM.tSAMScenarioRun
		(
			SAMScenarioInputSetID,
			SAMSiteID,
			RunStatus,
			LastModified
		)
		VALUES
		(
			@SAMScenarioInputSetID,
			@SAMSiteID,
			@RunStatus,
			@LastModified
		)
		SET @NewSAMScenarioRunID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT SAMScenarioRunID FROM SAM.tSAMScenarioRun WHERE SAMScenarioRunID = @SAMScenarioRunID)
		BEGIN
			UPDATE SAM.tSAMScenarioRun
			SET
				SAMScenarioInputSetID = @SAMScenarioInputSetID,
				SAMSiteID = @SAMSiteID,
				RunStatus = @RunStatus,
				LastModified = @LastModified
			WHERE SAMScenarioRunID = @SAMScenarioRunID;
			SET @NewSAMScenarioRunID = @SAMScenarioRunID;
		END
		ELSE
		BEGIN
			RAISERROR ('SAMScenarioRunID does not exist, cannot update.', 11, -1, -1)
			RETURN
		END
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spSaveSAMScenarioRun] TO [TEUser]
    AS [dbo];

