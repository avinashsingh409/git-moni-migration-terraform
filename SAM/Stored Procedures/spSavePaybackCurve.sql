CREATE PROCEDURE [SAM].[spSavePaybackCurve]
	@PaybackCurveID int = null,
	@PaybackCurveDesc nvarchar(255),
	@ProjectID int,
	@PaybackGroupID int,
	@Payback nvarchar(4000),
	@AcceptanceRate nvarchar(4000),
	@UserID int,
	@NewPaybackCurveID int output
AS
BEGIN

	SET @NewPaybackCurveID = -1;

	declare @ProjectCreatedBy int;
	IF @PaybackCurveID IS NULL
	BEGIN
		SET @ProjectCreatedBy = (SELECT CreatedBy FROM SAM.tProject WHERE ProjectID = @ProjectID);
	END
	ELSE
	BEGIN
		SET @ProjectCreatedBy = (SELECT p.CreatedBy FROM SAM.tProject p JOIN SAM.tPaybackCurve c ON p.ProjectID = c.ProjectID WHERE c.PaybackCurveID = @PaybackCurveID);
	END

	IF @ProjectCreatedBy IS NOT NULL AND @ProjectCreatedBy <> @UserID
	BEGIN
		RAISERROR ('User does not have permission to PaybackCurve.', 11, -1, -1)
		RETURN
	END

	declare @ResultID table (PaybackCurveID int);

	IF @PaybackCurveID IS NULL
	BEGIN
		INSERT INTO SAM.tPaybackCurve
		(
			PaybackCurveDesc,
			ProjectID,
			PaybackGroupID,
			Payback,
			AcceptanceRate
		)
		OUTPUT inserted.PaybackCurveID INTO @ResultID
		VALUES
		(
			@PaybackCurveDesc,
			@ProjectID,
			@PaybackGroupID,
			@Payback,
			@AcceptanceRate
		)
		SELECT TOP 1 @NewPaybackCurveID = PaybackCurveID FROM @ResultID;
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT PaybackCurveID FROM SAM.tPaybackCurve WHERE PaybackCurveID = @PaybackCurveID)
		BEGIN
			UPDATE SAM.tPaybackCurve
			SET
				PaybackCurveDesc = @PaybackCurveDesc,
				ProjectID = @ProjectID,
				PaybackGroupID = @PaybackGroupID,
				Payback = @Payback,
				AcceptanceRate = @AcceptanceRate
			WHERE PaybackCurveID = @PaybackCurveID;
			SET @NewPaybackCurveID = @PaybackCurveID;
		END
		ELSE
		BEGIN
			RAISERROR ('PaybackCurveID does not exist, cannot update.', 11, -1, -1)
			RETURN
		END
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spSavePaybackCurve] TO [TEUser]
    AS [dbo];

