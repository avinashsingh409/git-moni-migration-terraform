
CREATE PROCEDURE [SAM].[spSaveSiteSelection]
	@SiteSelectionID INT = NULL,
	@SamSiteID INT,
	@ProjectID INT,
	@RandomNumber REAL,
	@YearInstalled INT,
	@UserID INT,
	@NewSiteSelectionID INT OUTPUT
AS
BEGIN
	SET @NewSiteSelectionID = -1;

	DECLARE @ProjectCreatedBy INT;
	IF @SiteSelectionID IS NULL
	BEGIN
		SET @ProjectCreatedBy = (SELECT CreatedBy FROM SAM.tProject WHERE ProjectID = @ProjectID);
	END
	ELSE
	BEGIN
		SET @ProjectCreatedBy = (SELECT p.CreatedBy FROM SAM.tProject p JOIN SAM.tSiteSelection s ON p.ProjectID = s.ProjectID WHERE s.SiteSelectionID = @SiteSelectionID);
	END

	IF @ProjectCreatedBy IS NOT NULL AND @ProjectCreatedBy <> @UserID
	BEGIN
		RAISERROR('User does not have permission to SiteSelectionID.', 11, -1, -1);
		RETURN
	END

	DECLARE @ResultID TABLE (SiteSelectionID INT);

	IF (@SiteSelectionID IS NULL)
	BEGIN
		INSERT INTO SAM.tSiteSelection
		(
			SAMSiteID,
			ProjectID,
			RandomNumber,
			YearInstalled
		)
		OUTPUT inserted.SiteSelectionID into @ResultID
		VALUES
		(
			@SamSiteID,
			@ProjectID,
			@RandomNumber,
			@YearInstalled
		);
		SELECT TOP 1 @NewSiteSelectionID = SiteSelectionID FROM @ResultID;
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT SiteSelectionID FROM SAM.tSiteSelection WHERE SiteSelectionID = @SiteSelectionID)
		BEGIN
			UPDATE SAM.tSiteSelection
			SET
				SAMSiteID = @SamSiteID,
				ProjectID = @ProjectID,
				RandomNumber = @RandomNumber,
				YearInstalled = @YearInstalled
			WHERE SiteSelectionID = @SiteSelectionID;
			SET @NewSiteSelectionID = @SiteSelectionID;
		END
		ELSE
		BEGIN
			RAISERROR('SiteSelectionID does not exist, cannot update.', 11, -1, -1);
			RETURN
		END
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spSaveSiteSelection] TO [TEUser]
    AS [dbo];

