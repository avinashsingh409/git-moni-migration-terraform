CREATE PROCEDURE [SAM].[spDeleteAdoptionGroup]
	@AdoptionGroupID INT
AS
BEGIN
	IF NOT EXISTS (SELECT TOP 1 * FROM SAM.tAdoptionGroup WHERE AdoptionGroupID = @AdoptionGroupID)
		RAISERROR (N'Could not delete AdoptionGroup, invalid ID', 16, 1, 'SAM.spSaveAdoptionGroup')
	DELETE FROM SAM.tAdoptionGroup WHERE AdoptionGroupID = @AdoptionGroupID
END
GO

GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spDeleteAdoptionGroup] TO [TEUser]
    AS [dbo];

