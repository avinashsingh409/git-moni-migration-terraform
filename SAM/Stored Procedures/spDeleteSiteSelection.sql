
CREATE PROCEDURE [SAM].[spDeleteSiteSelection]
	@SiteSelectionID INT,
	@UserID INT,
	@DeletedSiteSelectionID INT OUTPUT
AS
BEGIN
	SET @DeletedSiteSelectionID = -1;

	IF NOT EXISTS (SELECT SiteSelectionID FROM SAM.tSiteSelection WHERE SiteSelectionID = @SiteSelectionID)
	BEGIN
		RAISERROR('SiteSelectionID does not exist, cannot delete.', 11, -1, -1);
		RETURN
	END

	DECLARE @ProjectCreatedBy INT;
	SET @ProjectCreatedBy = (SELECT p.CreatedBy FROM SAM.tProject p JOIN SAM.tSiteSelection s ON p.ProjectID = s.ProjectID WHERE s.SiteSelectionID = @SiteSelectionID);

	IF (@ProjectCreatedBy IS NOT NULL AND @ProjectCreatedBy <> @UserID)
	BEGIN
		RAISERROR('User does not have permission to SiteSelectionID.', 11, -1, -1);
		RETURN
	END

	DECLARE @ResultID TABLE (SiteSelectionID INT);

	DELETE FROM SAM.tSiteSelection
	OUTPUT deleted.SiteSelectionID INTO @ResultID
	WHERE SiteSelectionID = @SiteSelectionID

	SELECT TOP 1 @DeletedSiteSelectionID = SiteSelectionID FROM @ResultID;
END
GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spDeleteSiteSelection] TO [TEUser]
    AS [dbo];

