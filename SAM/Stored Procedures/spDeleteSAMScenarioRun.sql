
CREATE PROCEDURE [SAM].[spDeleteSAMScenarioRun]
	@SAMScenarioRunID int
AS
BEGIN


	IF NOT EXISTS (SELECT SAMScenarioRunID FROM SAM.tSAMScenarioRun WHERE SAMScenarioRunID = @SAMScenarioRunID)
	BEGIN
		RAISERROR ('SAMScenarioRunID does not exist, cannot delete.', 11, -1, -1)
	END

	DELETE FROM SAM.tSAMScenarioRun
	WHERE SAMScenarioRunID = @SAMScenarioRunID;

END
GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spDeleteSAMScenarioRun] TO [TEUser]
    AS [dbo];

