









CREATE PROCEDURE [SAM].[spDeletePaybackCurve]
	@PaybackCurveID int,
	@UserID int,
	@DeletedPaybackCurveID int output
AS
BEGIN

	SET @DeletedPaybackCurveID = -1;

	IF NOT EXISTS (SELECT PaybackCurveID FROM SAM.tPaybackCurve WHERE PaybackCurveID = @PaybackCurveID)
	BEGIN
		RAISERROR ('PaybackCurveID does not exist, cannot delete.', 11, -1, -1)
		RETURN
	END

	declare @ProjectCreatedBy int = (SELECT p.CreatedBy FROM SAM.tProject p JOIN SAM.tPaybackCurve c ON p.ProjectID = c.ProjectID WHERE c.PaybackCurveID = @PaybackCurveID);

	IF @ProjectCreatedBy IS NOT NULL AND @ProjectCreatedBy <> @UserID
	BEGIN
		RAISERROR ('User does not have permission to PaybackCurve.', 11, -1, -1)
		RETURN
	END

	declare @ResultID table (PaybackCurveID int);

	DELETE FROM SAM.tPaybackCurve
	OUTPUT deleted.PaybackCurveID INTO @ResultID
	WHERE PaybackCurveID = @PaybackCurveID;
	SELECT TOP 1 @DeletedPaybackCurveID = PaybackCurveID FROM @ResultID;

END
GO
GRANT EXECUTE
    ON OBJECT::[SAM].[spDeletePaybackCurve] TO [TEUser]
    AS [dbo];

