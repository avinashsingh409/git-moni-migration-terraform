﻿--tSAMScenarioInputSet
CREATE TABLE [SAM].[tSAMScenarioInputSet] (
    [SAMScenarioInputSetID]           INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]                         INT            NOT NULL,
    [ProjectID]                       INT            NOT NULL,
    [SAMMasterInputSetID]             INT            NOT NULL,
    [SectorTypeID]                    NVARCHAR (50)  NOT NULL,
    [FinancialModel]                  NVARCHAR (50)  NOT NULL,
    [TestYear]                        INT            NOT NULL,
    [DebtFraction]                    REAL           NOT NULL,
    [LoanTerm]                        REAL           NOT NULL,
    [LoanRate]                        REAL           NOT NULL,
    [LoanType]                        NVARCHAR (50)  NOT NULL,
    [FederalIncomeTaxRate]            REAL           NOT NULL,
    [StateIncomeTaxRate]              REAL           NOT NULL,
    [SalesTax]                        REAL           NOT NULL,
    [InsuranceRate]                   REAL           NOT NULL,
    [AssessedPropertyTaxRate]         REAL           NOT NULL,
    [AnnualDecline]                   REAL           NOT NULL,
    [PropertyTaxRate]                 REAL           NOT NULL,
    [NetSalvageValue]                 REAL           NOT NULL,
    [FederalDepreciationType]         NVARCHAR (50)  NOT NULL,
    [StateDepreciationType]           NVARCHAR (50)  NOT NULL,
    [DiscountRate]                    REAL           NOT NULL,
    [TotalInstalledCostCurve]         NVARCHAR (200) NOT NULL,
    [OMCostCurve]                     NVARCHAR (200) NOT NULL,
    [CustomIncentiveCurveA]           NVARCHAR (200) NOT NULL,
    [CustomIncentiveCurveB]           NVARCHAR (200) NOT NULL,
    [ApplyOptimization]               BIT            CONSTRAINT [DF_tSAMScenarioInputSet_ApplyOptimization] DEFAULT ((0)) NOT NULL,
    [ItcFedPercent]                   REAL           NULL,
    [ItcFedPercentMaxValue]           REAL           NULL,
    [ItcFedPercentDeprbasFed]         INT            NULL,
    [ItcFedPercentDeprbasSta]         INT            NULL,
    [ItcStaPercent]                   REAL           NULL,
    [ItcStaPercentMaxValue]           REAL           NULL,
    [ItcStaPercentDeprbasFed]         INT            NULL,
    [ItcStaPercentDeprbasSta]         INT            NULL,
    [PtcFedAmount]                    REAL           NULL,
    [PtcFedTerm]                      INT            NULL,
    [PtcStaAmount]                    REAL           NULL,
    [PtcStaTerm]                      INT            NULL,
    [IbiFedAmount]                    REAL           NULL,
    [IbiFedAmountTaxFed]              INT            NULL,
    [IbiFedAmountTaxSta]              INT            NULL,
    [IbiFedAmountDeprbasFed]          INT            NULL,
    [IbiFedAmountDeprbasSta]          INT            NULL,
    [IbiStaAmount]                    REAL           NULL,
    [IbiStaAmountTaxFed]              INT            NULL,
    [IbiStaAmountTaxSta]              INT            NULL,
    [IbiStaAmountDeprbasFed]          INT            NULL,
    [IbiStaAmountDeprbasSta]          INT            NULL,
    [IbiUtiAmount]                    REAL           NULL,
    [IbiUtiAmountTaxFed]              INT            NULL,
    [IbiUtiAmountTaxSta]              INT            NULL,
    [IbiUtiAmountDeprbasFed]          INT            NULL,
    [IbiUtiAmountDeprbasSta]          INT            NULL,
    [IbiOthAmount]                    REAL           NULL,
    [IbiOthAmountTaxFed]              INT            NULL,
    [IbiOthAmountTaxSta]              INT            NULL,
    [IbiOthAmountDeprbasFed]          INT            NULL,
    [IbiOthAmountDeprbasSta]          INT            NULL,
    [CbiFedAmount]                    REAL           NULL,
    [CbiFedMaxvalue]                  REAL           NULL,
    [CbiFedTaxFed]                    INT            NULL,
    [CbiFedTaxSta]                    INT            NULL,
    [CbiFedDeprbasFed]                INT            NULL,
    [CbiFedDeprbasSta]                INT            NULL,
    [CbiStaAmount]                    REAL           NULL,
    [CbiStaMaxvalue]                  REAL           NULL,
    [CbiStaTaxFed]                    INT            NULL,
    [CbiStaTaxSta]                    INT            NULL,
    [CbiStaDeprbasFed]                INT            NULL,
    [CbiStaDeprbasSta]                INT            NULL,
    [CbiUtiAmount]                    REAL           NULL,
    [CbiUtiMaxvalue]                  REAL           NULL,
    [CbiUtiTaxFed]                    INT            NULL,
    [CbiUtiTaxSta]                    INT            NULL,
    [CbiUtiDeprbasFed]                INT            NULL,
    [CbiUtiDeprbasSta]                INT            NULL,
    [CbiOthAmount]                    REAL           NULL,
    [CbiOthMaxvalue]                  REAL           NULL,
    [CbiOthTaxFed]                    INT            NULL,
    [CbiOthTaxSta]                    INT            NULL,
    [CbiOthDeprbasFed]                INT            NULL,
    [CbiOthDeprbasSta]                INT            NULL,
    [PbiFedAmount]                    REAL           NULL,
    [PbiFedTerm]                      INT            NULL,
    [PbiFedTaxFed]                    INT            NULL,
    [PbiFedTaxSta]                    INT            NULL,
    [PbiStaAmount]                    REAL           NULL,
    [PbiStaTerm]                      INT            NULL,
    [PbiStaTaxFed]                    INT            NULL,
    [PbiStaTaxSta]                    INT            NULL,
    [PbiUtiAmount]                    REAL           NULL,
    [PbiUtiTerm]                      INT            NULL,
    [PbiUtiTaxFed]                    INT            NULL,
    [PbiUtiTaxSta]                    INT            NULL,
    [PbiOthAmount]                    REAL           NULL,
    [PbiOthTerm]                      INT            NULL,
    [PbiOthTaxFed]                    INT            NULL,
    [PbiOthTaxSta]                    INT            NULL,
    [CustomIncentiveCurveAMax]        REAL           NULL,
    [CustomIncentiveCurveAFedTaxable] INT            NULL,
    [CustomIncentiveCurveAStaTaxable] INT            NULL,
    [CustomIncentiveCurveBMax]        REAL           NULL,
    [CustomIncentiveCurveBFedTaxable] INT            NULL,
    [CustomIncentiveCurveBStaTaxable] INT            NULL,
    [SAMScenarioInputSetDesc]         NVARCHAR (255) NULL,
    CONSTRAINT [PK_tSAMScenarioInputSet] PRIMARY KEY CLUSTERED ([SAMScenarioInputSetID] ASC),
    CONSTRAINT [FK_tSAMScenarioInputSet_tSAMMasterInputSet] FOREIGN KEY ([SAMMasterInputSetID]) REFERENCES [SAM].[tSAMMasterInputSet] ([SAMMasterInputSetID])
);


GO

GO

