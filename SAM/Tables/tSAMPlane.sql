﻿--tSAMPlane
CREATE TABLE [SAM].[tSAMPlane] (
    [SAMPlaneID]            INT            IDENTITY (1, 1) NOT NULL,
    [SAMPlaneSetID]         INT            NOT NULL,
    [AssetID]               INT            NOT NULL,
    [ProjectID]             INT            NOT NULL,
    [SiteID]                NVARCHAR (100) NOT NULL,
    [PlaneID]               NVARCHAR (100) NOT NULL,
    [PVAzimuth]             REAL           NOT NULL,
    [PVTilt]                REAL           NOT NULL,
    [PVShadingFactors]      NVARCHAR (100) NOT NULL,
    [PVSystemCapacity]      REAL           NOT NULL,
    [PVGroundCoverageRatio] REAL           NULL,
    [ProductionProfile]     NVARCHAR (50)  NULL,
    CONSTRAINT [PK_tSAMPlane] PRIMARY KEY CLUSTERED ([SAMPlaneID] ASC),
    CONSTRAINT [FK_tSAMPlane_SAMPlaneSetID_tSAMPlaneSet] FOREIGN KEY ([SAMPlaneSetID]) REFERENCES [SAM].[tSAMPlaneSet] ([SAMPlaneSetID]),
    CONSTRAINT [UK_tSAMPlane] UNIQUE NONCLUSTERED ([AssetID] ASC, [ProjectID] ASC, [SiteID] ASC, [PlaneID] ASC)
);

