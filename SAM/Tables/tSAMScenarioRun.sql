﻿--tSAMScenarioRun
CREATE TABLE [SAM].[tSAMScenarioRun](
	[SAMScenarioRunID] [int] IDENTITY(1,1) NOT NULL,
	[SAMScenarioInputSetID] [int] NOT NULL,
	[SAMSiteID] [int] NOT NULL,
	[RunStatus] [int] NOT NULL,
	[LastModified] [datetime] NOT NULL CONSTRAINT [DF_tSAMScenarioRun_LastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_tSAMScenarioRun] PRIMARY KEY CLUSTERED 
(
	[SAMScenarioRunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [SAM].[tSAMScenarioRun]  ADD  CONSTRAINT [FK_tSAMScenarioRun_tSAMScenarioInputSet] FOREIGN KEY([SAMScenarioInputSetID])
REFERENCES [SAM].[tSAMScenarioInputSet] ([SAMScenarioInputSetID])
GO

ALTER TABLE [SAM].[tSAMScenarioRun] CHECK CONSTRAINT [FK_tSAMScenarioRun_tSAMScenarioInputSet]