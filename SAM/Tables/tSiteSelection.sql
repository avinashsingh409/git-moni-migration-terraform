﻿--tSiteSelection
CREATE TABLE [SAM].[tSiteSelection](
	[SiteSelectionID] [int] IDENTITY(1,1) NOT NULL,
	[SAMSiteID] [int] NOT NULL,
	[ProjectID] [int] NOT NULL,
	[RandomNumber] [real] NOT NULL,
	[YearInstalled] [int] NULL,
 CONSTRAINT [PK_tSiteSelection] PRIMARY KEY CLUSTERED 
(
	[SiteSelectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]