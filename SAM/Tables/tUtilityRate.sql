﻿--tUtilityRate
CREATE TABLE [SAM].[tUtilityRate] (
    [UtilityRateID]       INT            IDENTITY (1, 1) NOT NULL,
    [CustomerRateClassID] NVARCHAR (150) NOT NULL,
    [TestYear]            INT            NOT NULL,
    [UtilityRateFile]     NVARCHAR (150) NOT NULL,
    [UtilityRateSetID]    INT            NOT NULL,
    CONSTRAINT [PK_UtilityRateID] PRIMARY KEY CLUSTERED ([UtilityRateID] ASC),
    CONSTRAINT [FK_SAM_tUtilityRate_UtilityRateSetID_tUtilityRateSet] FOREIGN KEY ([UtilityRateSetID]) REFERENCES [SAM].[tUtilityRateSet] ([UtilityRateSetID]),
    CONSTRAINT [UK_tUtilityRate_CustomerRateClassID_TestYear_UtilityRateSetID] UNIQUE NONCLUSTERED ([CustomerRateClassID] ASC, [TestYear] ASC, [UtilityRateSetID] ASC)
);





