﻿CREATE TABLE [SAM].[tSAMSiteSet] (
    [SAMSiteSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [SAMSiteSetDesc] NVARCHAR (255) NOT NULL,
    [ProjectID]      INT            NOT NULL,
    CONSTRAINT [PK_tSAMSiteSet] PRIMARY KEY CLUSTERED ([SAMSiteSetID] ASC),
    CONSTRAINT [FK_SAM_tSAMSiteSet_ProjectID_tProject] FOREIGN KEY ([ProjectID]) REFERENCES [SAM].[tProject] ([ProjectID]),
    CONSTRAINT [UK_tSAMSiteSet_SAMSiteSetDesc] UNIQUE NONCLUSTERED ([SAMSiteSetDesc] ASC)
);

