﻿--Create tables for SAM/SmartDG

--tAdoptionGroup
CREATE TABLE [SAM].[tAdoptionGroup](
	[AdoptionGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[AdoptionGroupName] [nvarchar](50) NOT NULL,
	[AdoptionGroupDesc] [nvarchar](255) NOT NULL,
	[AdoptionTargets] [nvarchar](200) NULL,
	[MarketAcceptance] [nvarchar](500) NULL,
 CONSTRAINT [PK_tAdoptionGroup] PRIMARY KEY CLUSTERED 
(
	[AdoptionGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]