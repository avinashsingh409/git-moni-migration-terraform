﻿CREATE TABLE [SAM].[tUtilityRateSet] (
    [UtilityRateSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [UtilityRateSetDesc] NVARCHAR (255) NULL,
    [ProjectID]          INT            NULL,
    CONSTRAINT [PK_tUtilityRateSet] PRIMARY KEY CLUSTERED ([UtilityRateSetID] ASC),
    CONSTRAINT [UK_tUtilityRateSet_UtilityRateSetDesc] UNIQUE NONCLUSTERED ([UtilityRateSetDesc] ASC)
);



