﻿CREATE TABLE [SAM].[tSAMPlaneSet] (
    [SAMPlaneSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [SAMPlaneSetDesc] NVARCHAR (255) NOT NULL,
    [ProjectID]       INT            NOT NULL,
    CONSTRAINT [PK_tSAMPlaneSet] PRIMARY KEY CLUSTERED ([SAMPlaneSetID] ASC),
    CONSTRAINT [FK_SAM_tSAMPlaneSet_ProjectID_tProject] FOREIGN KEY ([ProjectID]) REFERENCES [SAM].[tProject] ([ProjectID]),
    CONSTRAINT [UK_tSAMPlaneSet_SAMPlaneSetDesc] UNIQUE NONCLUSTERED ([SAMPlaneSetDesc] ASC)
);

