﻿--tScenarioOutputs
CREATE TABLE [SAM].[tScenarioOutputs](
	[ScenarioID] [int] NOT NULL,
	[lcoe_real] [real] NULL,
	[lcoe_nom] [real] NULL,
	[payback] [real] NULL,
	[npv] [real] NULL,
	[sv_capacity_factor] [real] NULL,
	[sv_kwh_per_kw] [real] NULL,
	[cf_energy_net] [nvarchar](max) NULL,
	[analysis_years] [int] NULL,
	[ibi_total] [real] NULL,
	[cbi_total] [real] NULL,
	[cf_pbi_total] [nvarchar](max) NULL,
	[itc_total] [real] NULL,
	[sv_wacc] [real] NULL,
	[cf_after_tax_cash_flow] [nvarchar](max) NULL,
	[max_site_capacity] [real] NULL,
	[total_installed_cost] [real] NULL,
	[load_limited_capacity] [real] NULL,
	[SiteID] [nvarchar](100) NULL,
	[SiteName] [nvarchar](100) NULL,
	[optimized_capacity] [real] NULL,
	[AcceptanceRate] [real] NULL,
	[AcceptanceProduct] [real] NULL,
	[cbi_total_fed] [real] NULL,
	[cbi_total_sta] [real] NULL,
	[cbi_total_uti] [real] NULL,
	[cbi_total_oth] [real] NULL,
	[itc_total_fed] [real] NULL,
	[itc_total_sta] [real] NULL,
	[ibi_total_fed] [real] NULL,
	[ibi_total_sta] [real] NULL,
	[ibi_total_uti] [real] NULL,
	[ibi_total_oth] [real] NULL,
	[cf_pbi_total_fed] [nvarchar](max) NULL,
	[cf_pbi_total_sta] [nvarchar](max) NULL,
	[cf_pbi_total_uti] [nvarchar](max) NULL,
	[cf_pbi_total_oth] [nvarchar](max) NULL,
	[cf_ptc_total_fed] [nvarchar](max) NULL,
	[cf_ptc_total_sta] [nvarchar](max) NULL,
	[cf_ptc_total] [nvarchar](max) NULL,
 CONSTRAINT [PK_tScenarioOutputs] PRIMARY KEY CLUSTERED 
(
	[ScenarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UX_tScenarioOutputs] UNIQUE NONCLUSTERED 
(
	[ScenarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)