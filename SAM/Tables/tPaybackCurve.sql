﻿--tPaybackCurve
CREATE TABLE [SAM].[tPaybackCurve](
	[PaybackCurveID] [int] IDENTITY(1,1) NOT NULL,
	[PaybackCurveDesc] [nvarchar](255) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[PaybackGroupID] [int] NOT NULL,
	[Payback] [nvarchar](4000) NOT NULL,
	[AcceptanceRate] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_tPaybackCurve] PRIMARY KEY CLUSTERED 
(
	[PaybackCurveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]