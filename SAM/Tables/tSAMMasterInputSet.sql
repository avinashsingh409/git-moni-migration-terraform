﻿--tSAMMasterInputSet
CREATE TABLE [SAM].[tSAMMasterInputSet] (
    [SAMMasterInputSetID]       INT            IDENTITY (1, 1) NOT NULL,
    [InflationRate]             REAL           NOT NULL,
    [ModuleType]                INT            NOT NULL,
    [DCtoACRatio]               REAL           NOT NULL,
    [RateEscalation]            REAL           NOT NULL,
    [AnalysisPeriod]            INT            NOT NULL,
    [BaseYear]                  INT            NOT NULL,
    [InverterEfficiency]        REAL           NOT NULL,
    [Availability]              REAL           NOT NULL,
    [Degradation]               REAL           NOT NULL,
    [RemainingSystemLosses]     REAL           NOT NULL,
    [DoNEMWorkAround]           BIT            CONSTRAINT [DF_tSAMMasterInputSet_DoNEMWorkAround] DEFAULT ((0)) NOT NULL,
    [CapacityLimitDC]           REAL           NULL,
    [AdjustConstant]            REAL           CONSTRAINT [DF_tSAMMasterInputSet_AdjustConstant] DEFAULT ((1)) NULL,
    [MinimumSystemCapacity]     REAL           CONSTRAINT [DF_tSAMMasterInputSet_MinimumSystemCapacity] DEFAULT ((0.96)) NULL,
    [MinimumPaybackDelta]       REAL           CONSTRAINT [DF_tSAMMasterInputSet_MinimumPaybackDelta] DEFAULT ((0.1)) NULL,
    [CapacityReductionStepSize] REAL           CONSTRAINT [DF_tSAMMasterInputSet_CapacityReductionStepSize] DEFAULT ((0.1)) NULL,
    [UseLifetimeOutput]         INT            CONSTRAINT [DF_tSAMMasterInputSet_UseLifetimeOutput] DEFAULT ((0)) NULL,
    [EnergyCurtailment]         NVARCHAR (250) NULL,
    [ArrayType]                 INT            DEFAULT ((1)) NULL,
    [SAMMasterInputSetDesc]     NVARCHAR (255) NULL,
    CONSTRAINT [PK_tSAMMasterInputSet] PRIMARY KEY CLUSTERED ([SAMMasterInputSetID] ASC)
);

