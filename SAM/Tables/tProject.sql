﻿--tProject
CREATE TABLE [SAM].[tProject](
	[ProjectID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectDesc] [nvarchar](255) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ChangedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tProject_CreateDate]  DEFAULT (getdate()),
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tProject_ChangeDate]  DEFAULT (getdate()),
	[Status] [int] NOT NULL CONSTRAINT [DF_tProject_Status]  DEFAULT ((0)),
	[StartYear] [int] NOT NULL,
	[EndYear] [int] NOT NULL,
 CONSTRAINT [PK_tProject] PRIMARY KEY CLUSTERED 
(
	[ProjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]