﻿CREATE PROCEDURE [Monitoring].[spGetNDModelTemplate]
  (@ModelTemplateID int = NULL)
AS
BEGIN
  IF @ModelTemplateID IS NULL OR @ModelTemplateID = 0
    SELECT [ModelTemplateID],
          [AssetClassTypeVariableTypeMapID],
          [ParentAssetClassTypeID],
          [ModelTypeID],
          [Temporal],
          [ConcerningBoundTypeID],
          [CreatedByUserID],
          [CreateDate],
          [ChangedByUserID],
          [ChangeDate],
		  [UpperCriticality],
		  [LowerCriticality]
    FROM [Monitoring].[tNDModelTemplate];
  ELSE
    SELECT [ModelTemplateID],
          [AssetClassTypeVariableTypeMapID],
          [ParentAssetClassTypeID],
          [ModelTypeID],
          [Temporal],
          [ConcerningBoundTypeID],
          [CreatedByUserID],
          [CreateDate],
          [ChangedByUserID],
          [ChangeDate],
		  [UpperCriticality],
		  [LowerCriticality]
    FROM [Monitoring].[tNDModelTemplate] WHERE [ModelTemplateID] = @ModelTemplateID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelTemplate] TO [TEUser]
    AS [dbo];

