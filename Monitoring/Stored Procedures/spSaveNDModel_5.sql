﻿CREATE PROCEDURE [Monitoring].[spSaveNDModel]
  @ModelID int = NULL,
   @NDProjectPath nvarchar(400),
   @NDProjectName nvarchar(255),
   @NDResultsPath nvarchar(400),
   @ModelExtID nvarchar(255),
   @ModelName nvarchar(255),
   @ModelDesc nvarchar(255) = NULL,
   @ModelTemplateID int = NULL,
   @AssetVariableTypeTagMapID int,
   @ModelTypeID int,
   @OpModeParamsID int,
   @Temporal bit,
   @Built bit,
   @BuiltSinceTime datetime = NULL,
   @Active bit,
   @ActiveSinceTime datetime = NULL,
   @PreserveAlertStatus bit = NULL,
   @AlertStatusTypeID int = NULL,
   @IsPublic bit,
   @DisplayOrder int = NULL,
   @SecurityUserID int,
   @ChangeDate datetime,
   @BuildInterval int = NULL,
   @BuildIntervalTemporalTypeID int = NULL,
   @LastInterrogatedTime datetime = NULL,
   @CalcPointID int = NULL,
   @Release bit = NULL,
   @AutoRetrain bit = false,
   @ReferenceModelID nvarchar(400) = NULL,
   @Locked bit,
   @LockReasonID int = NULL,
   @LockedSinceTime datetime = NULL,
   @ModelConfigURL varchar(2048) = NULL,
   @DiagnosticDrilldownURL varchar(2048) = NULL,
   @IsStandard bit = null
AS
BEGIN
  IF @ModelID IS NULL OR @ModelID < 1
    BEGIN
      DECLARE @newExtID nvarchar(255);
	  SET @newExtID = CONVERT(nvarchar(36), NEWID());
	  INSERT INTO [Monitoring].[tNDModel]
        ([NDProjectPath],
		 [NDProjectName],
		 [NDResultsPath],
		 [ModelExtID],
         [ModelName],
         [ModelDesc],
         [ModelTemplateID],
         [AssetVariableTypeTagMapID],
         [ModelTypeID],
         [OpModeParamsID],
         [Temporal],
         [Built],
         [BuiltSinceTime],
         [Active],
         [ActiveSinceTime],
         [PreserveAlertStatus],
         [AlertStatusTypeID],
         [IsPublic],
         [DisplayOrder],
         [CreatedByUserID],
         [CreateDate],
         [ChangedByUserID],
         [ChangeDate],
         [BuildInterval],
         [BuildIntervalTemporalTypeID],
         [AutoRetrain],
         [LastInterrogatedTime],
		 [ReferenceModelID],
		 [Locked],
		 [LockReasonID],
		 [LockedSinceTime],
		 [ModelConfigURL],
		 [DiagnosticDrilldownURL],
         [IsStandard])
      VALUES
        (@NDProjectPath,
		 @NDProjectName,
		 @NDResultsPath,
		 COALESCE(@ModelExtID, @newExtID),
         @ModelName,
         @ModelDesc,
         @ModelTemplateID,
         @AssetVariableTypeTagMapID,
         @ModelTypeID,
         @OpModeParamsID,
         @Temporal,
         @Built,
         @BuiltSinceTime,
         @Active,
         @ActiveSinceTime,
         @PreserveAlertStatus,
         @AlertStatusTypeID,
         @IsPublic,
         @DisplayOrder,
         @SecurityUserID,
         @ChangeDate,
         @SecurityUserID,
         @ChangeDate,
         @BuildInterval,
         @BuildIntervalTemporalTypeID,
         @AutoRetrain,
         @LastInterrogatedTime,
		 @ReferenceModelID,
		 @Locked,
		 @LockReasonID,
		 @LockedSinceTime,
		 @ModelConfigURL,
		 @DiagnosticDrilldownURL,
         COALESCE(@IsStandard, 0));

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
	  DECLARE @exRelease bit;
	  DECLARE @exLockedSinceTime datetime;
	  SELECT @exRelease = Release, @exLockedSinceTime = LockedSinceTime FROM [Monitoring].[tNDModel] WHERE ModelID = @ModelID;
      UPDATE [Monitoring].[tNDModel]
        SET [NDProjectPath] = @NDProjectPath,
			[NDProjectName] = @NDProjectName,
			[NDResultsPath] = @NDResultsPath,
			[ModelExtID] = @ModelExtID,
            [ModelName] = @ModelName,
            [ModelDesc] = @ModelDesc,
            [ModelTemplateID] = @ModelTemplateID,
            [AssetVariableTypeTagMapID] = @AssetVariableTypeTagMapID,
            [ModelTypeID] = @ModelTypeID,
            [OpModeParamsID] = @OpModeParamsID,
            [Temporal] = @Temporal,
            [Built] = @Built,
            [BuiltSinceTime] = @BuiltSinceTime,
            [Active] = @Active,
            [ActiveSinceTime] = @ActiveSinceTime,
            [PreserveAlertStatus] = @PreserveAlertStatus,
            [AlertStatusTypeID] = @AlertStatusTypeID,
            [IsPublic] = @IsPublic,
            [DisplayOrder] = @DisplayOrder,
            [ChangedByUserID] = @SecurityUserID,
            [ChangeDate] = @ChangeDate,
            [BuildInterval] = @BuildInterval,
            [BuildIntervalTemporalTypeID] = @BuildIntervalTemporalTypeID,
            [LastInterrogatedTime] = @LastInterrogatedTime,
            [AutoRetrain] = @AutoRetrain,
            [CalcPointID] = @CalcPointID,
			[Release] = COALESCE(@Release, @exRelease),
			[ReferenceModelID] = @ReferenceModelID,
			[Locked] = @Locked,
			[LockReasonID] = @LockReasonID,
			[LockedSinceTime] = CASE @Locked WHEN 1 THEN COALESCE (@exLockedSinceTime, @LockedSinceTime) ELSE NULL END,
			[ModelConfigURL] = @ModelConfigURL,
			[DiagnosticDrilldownURL] = @DiagnosticDrilldownURL,
            [IsStandard] = COALESCE(@IsStandard, [IsStandard])
        WHERE [ModelID] = @ModelID;

		SELECT @ModelID;
    END
END

GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModel] TO [TEUser]
    AS [dbo];

