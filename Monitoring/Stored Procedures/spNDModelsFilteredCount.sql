CREATE PROCEDURE [Monitoring].[spNDModelsFilteredCount] 
	@securityUserID int,
	@startAssetID int = NULL,
	@astVarTypeTagMapId int = NULL,
	@active bit = NULL,
	@built bit = NULL,
	@hasCalcPoint bit = NULL,
	@tagName NVARCHAR(400) = NULL,
	@tagDesc NVARCHAR(400) = NULL,
	@modelName NVARCHAR(255) = NULL,
	@modelTypeID Base.tpIntList READONLY,
	@engUnits NVARCHAR(255) = NULL,
	@modelOpModeID Base.tpIntList READONLY,
	@isDiagnose bit = NULL,
	@isWatch bit = NULL,
	@isWatchOverride bit = NULL,
	@isMaintenance bit = NULL,
	@isIgnore bit = NULL,
	@isOpenIssue bit = NULL,
	@evaluationErrorMessage NVARCHAR(400) = NULL,
	@priority int = NULL,
	@outOfBounds bit = NULL,
	@alertPriority int = NULL,
	@alertType NVARCHAR(255) = NULL,
	@isAlert bit = NULL,
	@addedToAlertsStart datetime = NULL,
	@addedToAlertEnd datetime = NULL,
	@currentBuildStatusTypeID Base.tpIntList READONLY,
	@assetCriticality			Base.TPINTLIST READONLY,
	@modelCriticality			Base.TPINTLIST READONLY,
	@modelId int = null,
	@filterCategoryIDs Base.tpIntList READONLY
	WITH RECOMPILE
AS
BEGIN

	DECLARE @assets base.tpIntList

	-- only get assets that are associated with models, and prefilter based on active and/or built, calc point, and tag map
	if @startAssetID is not null
	begin
	  INSERT INTO @assets
	  select distinct b.assetid from monitoring.tndmodel a JOIN ProcessData.tAssetVariableTypeTagMap b on 
	  a.AssetVariableTypeTagMapID = b.AssetVariableTypeTagMapID
	  JOIN  Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @startAssetID) c on b.AssetID = c.AssetID
	  WHERE (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) AND (@modelId is null or a.ModelID = @modelId) OPTION (RECOMPILE)
	end
	else if @astVarTypeTagMapId is not null
	begin
	  insert into @assets
	  select distinct map.AssetID from ProcessData.tAssetVariableTypeTagMap map
	  join Monitoring.tNDModel a on map.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
	  where (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) AND (@modelId is null or a.ModelID = @modelId)
			AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID, @securityUserID, -1) = 1 OPTION (RECOMPILE)
	end
	else if @modelId is not null
	begin
		insert into @assets
		select map.AssetID from ProcessData.tAssetVariableTypeTagMap map
		join Monitoring.tNDModel a on map.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID		
		where (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) AND (a.ModelID = @modelId)
			AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID, @securityUserID, -1) = 1 OPTION (RECOMPILE)
	end
	else -- fall back to all assets to which the user has access
	begin
		insert into @assets
		select distinct map.AssetID from ProcessData.tAssetVariableTypeTagMap map
		join Monitoring.tNDModel a on map.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
		join Asset.ufnGetAssetIdsForUser(@securityUserID) s on map.AssetID = s.AssetID
  		WHERE (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) AND (@modelId is null or a.ModelID = @modelId) OPTION (RECOMPILE)
	end


	DECLARE @usedModelIDs BASE.tpIntList
	
	IF @modelID is not null
	  BEGIN
	  INSERT INTO @usedModelIDs values (@modelID)
	  END

	DECLARE @modelIDs TABLE(
		ModelID int
	)

	INSERT INTO @modelIDs (ModelID)
	SELECT
		modelIDCheck.ModelID
	FROM [Monitoring].[ufnGetFilteredModelInfoFromAssetIDs] (
		@assets, @isAlert, @addedToAlertsStart, @addedToAlertEnd, @active,
		@built, @hasCalcPoint, @tagName, @tagDesc, @modelName,
		@modelTypeID, @engUnits, @modelOpModeID, @isDiagnose, @isWatch,
		@isWatchOverride, @isMaintenance, @isIgnore,
		@isOpenIssue, @evaluationErrorMessage, @priority, @outOfBounds, @alertPriority, @alertType
		, @currentBuildStatusTypeID, @securityUserID, @assetCriticality, @modelCriticality, @usedModelIDs, @filterCategoryIDs
	) modelIDCheck WHERE (@modelId is null or ModelID = @modelId) OPTION (RECOMPILE);

	SELECT COUNT(*) 
		FROM Monitoring.tNDModel mdl
		INNER JOIN @modelIDs modelIDCheck ON modelIDCheck.ModelID = mdl.ModelID
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spNDModelsFilteredCount] TO [TEUser]
    AS [dbo];
GO