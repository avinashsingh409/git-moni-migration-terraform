﻿CREATE PROCEDURE [Monitoring].[spGetNDOpModeParamsAstMapByOpModeTypeIDAndAssetID] 
	@opModeTypeID int,
	@assetID Base.tpIntList readonly
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		map.OpModeParamsAssetMapID
		, map.OpModeTypeID
		, map.OpModeParamsID
		, map.AssetID
	FROM [Monitoring].[tNDOpModeParamsAssetMap] map
	JOIN @assetID a on map.AssetID = a.id
	WHERE OpModeTypeID = @opModeTypeID
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeParamsAstMapByOpModeTypeIDAndAssetID] TO [TEUser]
    AS [dbo];

