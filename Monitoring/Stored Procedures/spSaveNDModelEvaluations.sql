﻿CREATE PROCEDURE [Monitoring].[spSaveNDModelEvaluations]
      @tblNDModelEval Monitoring.tpNDModelEval READONLY
AS
BEGIN

       SET NOCOUNT ON;            
	   declare @now as DateTime = getdate()

	   INSERT INTO Monitoring.tndModelEvaluation (
                     ModelID
                     , NDAlert
                     , Alert
                     , NDActual
                     , NDExpected
                     , NDUpper
                     , NDLower
                     , PercentOOB
                     , HiHiAlert
                     , HiHiAlertSinceTime
                     , LowLowAlert
                     , LowLowAlertSinceTime
                     , AreaFastResponseAlert
                     , AreaFastResponseAlertSinceTime
                     , AreaSlowResponseAlert
                     , AreaSlowResponseAlertSinceTime
                     , FrequencyAlert
                     , FrequencyAlertSinceTime
                     , OscillationAlert
                     , OscillationAlertSinceTime
					 , FrozenDataAlert
					 , FrozenDataAlertSinceTime
                     , BatchEvaluationStarttime
                     , EvaluationError
                     , EvaluationErrorMessage
                     , EvaluationTime
                     , WatchOverrideAlert
                     , WatchOverrideAlertSinceTime
					 , IgnoreOverrideAlert
					 , IgnoreOverrideAlertSinceTime
					 , SerializedOpMode
					 , PrioritizedOpMode
					 , EvaluationStatus
					 , AlertStatusPriorityTypeID
					 , ActionWatchOverride
					 , ActionWatchOverrideSinceTime
              )
		SELECT  modelID
                     , nDAlert
                     , alert
                     , nDActual
                     , nDExpected
                     , nDUpper
                     , nDLower
                     , percentOOB
                     , hiHiAlert
                     , CASE WHEN hiHiAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
                     , lowLowAlert
                     , CASE WHEN lowLowAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
                     , areaFastResponseAlert
                     , CASE WHEN areaFastResponseAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
                     , areaSlowResponseAlert
                     , CASE WHEN areaSlowResponseAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
                     , frequencyAlert
                     , CASE WHEN frequencyAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
                     , oscillationAlert
                     , CASE WHEN oscillationAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
					 , frozenDataAlert
					 , CASE WHEN frozenDataAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
                     , batchEvaluationStartTime
                     , evaluationError
                     , evaluationErrorMessage
                     , isnull(modelEvalTime,@now)
                     , watchOverrideAlert
                     , CASE WHEN watchOverrideAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
					 , IgnoreOverrideAlert
					 , CASE WHEN IgnoreOverrideAlert = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
					 , serializedOpMode
					 , prioritizedOpMode
					 , evaluationStatus 
					 , AlertStatusPriorityTypeID
					 , ActionWatchOverride
					 , CASE WHEN ActionWatchOverride = 1 THEN isnull(modelEvalTime,@now) ELSE NULL END
					 FROM @tblNDModelEval WHERE modelID not in (select modelID from Monitoring.tNDModelEvaluation)
              UPDATE b
              SET
                     NDAlert = a.nDAlert
                     , Alert = a.alert
                     , NDActual = a.nDActual
                     , NDExpected = a.nDExpected
                     , NDUpper = a.nDUpper
                     , NDLower = a.nDLower
                     , PercentOOB = a.percentOOB
                     , HiHiAlert = a.hiHiAlert
                     , HiHiAlertSinceTime = CASE WHEN a.hiHiAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
                     , LowLowAlert = a.lowLowAlert
                     , LowLowAlertSinceTime = CASE WHEN a.lowLowAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
                     , AreaFastResponseAlert = a.areaFastResponseAlert
                     , AreaFastResponseAlertSinceTime = CASE WHEN a.areaFastResponseAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
                     , AreaSlowResponseAlert = a.areaSlowResponseAlert
                     , AreaSlowResponseAlertSinceTime = CASE WHEN a.areaSlowResponseAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
                     , FrequencyAlert = a.frequencyAlert
                     , FrequencyAlertSinceTime = CASE WHEN a.frequencyAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
                     , OscillationAlert = a.oscillationAlert
                     , OscillationAlertSinceTime = CASE WHEN a.oscillationAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
					 , FrozenDataAlert = a.FrozenDataAlert
                     , FrozenDataAlertSinceTime = CASE WHEN a.FrozenDataAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
                     , BatchEvaluationStartTime = a.batchEvaluationStartTime
                     , EvaluationError = a.evaluationError
                     , EvaluationErrorMessage = a.evaluationErrorMessage
                     , EvaluationTime = modelEvalTime
                     , WatchOverrideAlert = a.watchOverrideAlert
                     , WatchOverrideAlertSinceTime = CASE WHEN a.watchOverrideAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END   
					 , IgnoreOverrideAlert = a.IgnoreOverrideAlert
                     , IgnoreOverrideAlertSinceTime = CASE WHEN a.IgnoreOverrideAlert = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END
					 , SerializedOpMode = a.serializedOpMode
					 , PrioritizedOpMode = a.prioritizedOpMode
					 , EvaluationStatus = a.evaluationStatus
					 , AlertStatusPriorityTypeID = a.AlertStatusPriorityTypeID
					 , ActionWatchOverride = a.ActionWatchOverride
					 , ActionWatchOverrideSinceTime = CASE WHEN a.ActionWatchOverride = 1 THEN isnull(a.modelEvalTime,@now) ELSE NULL END   
					 from @tblNDModelEval a JOIN Monitoring.tNDModelEvaluation b on a.modelid = b.modelid
              
       

END

GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelEvaluations] TO [TEUser]
    AS [dbo];


GO