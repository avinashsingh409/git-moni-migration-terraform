﻿
Create Procedure [Monitoring].[spGetNDAssetHierarchySyncStatus]
	@assetId int, 
	@securityUserId int
As
Begin
	Declare @globalId varchar(255)
	Declare @lastJob table (
		Success bit NOT NULL,
		Complete bit NOT NULL,
		OpenedDate datetime NULL,
		ClosedDate datetime NULL)
	Declare @lastStatus table (
		Completed int Not Null,
		LastRecord datetime Null)

    If @securityuserid >= 1 And Asset.ufnDoesUserHaveAccessToAsset(@assetId, @securityUserId, -1) = 1
		Begin
		Set @globalId = Convert(varchar(255), (Select GlobalID From Asset.tAsset Where AssetID = @assetId))

		Insert Into @lastJob Select Top 1 Success, Complete, OpenedDate, ClosedDate From Jobs.tJob
			Where JobTypeId = 2 And CharIndex(@globalId, JobName) > 0 
			Order By OpenedDate Desc

		If Not Exists (Select * From @lastJob)
			Insert Into @lastStatus (Completed, LastRecord) Select 1, Null --1 No, Null
		Else
			Begin
			Insert Into @lastStatus (Completed, LastRecord)
			Select
				Case
				When Complete = 0 And Success = 0 And ClosedDate Is Null Then 2     --2 Initiated, OpenedDate
				When Complete = 1 And Success = 1 And ClosedDate Is Not Null Then 3 --3 Yes,       ClosedDate
				When Complete = 1 And Success = 0 And ClosedDate Is Not Null Then 4 --4 Failed,    ClosedDate
				Else 0																--0 Error,	   ClosedDate Or OpenedDate
				End
				, Coalesce(ClosedDate, OpenedDate)
			From @lastJob
			End
		End
	Else
		Insert Into @lastStatus (Completed, LastRecord) Select 0, Null --0 Error, Null
	
	Select * From @lastStatus
End
Go
Grant Execute
    On Object::[Monitoring].[spGetNDAssetHierarchySyncStatus] To [TEUser]
    As [dbo];
