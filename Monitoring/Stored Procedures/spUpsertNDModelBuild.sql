﻿
CREATE PROCEDURE [Monitoring].[spUpsertNDModelBuild]
	@ModelBuildID int = NULL,
	@ModelExtID nvarchar(255),
	@BuildStatusTypeID int,
	@BuildStatus nvarchar(max),
	@BuildStatusUserID int,
	@BuildStatusTime datetime = NULL,
	@NewModelBuildID int OUTPUT
AS
BEGIN

	SET @NewModelBuildID = -1
	
	IF (@ModelBuildID IS NULL)
	BEGIN
		
		INSERT INTO Monitoring.tNDModelBuild
		(
			ModelExtID,
			BuildStatusTypeID,
			BuildStatus,
			BuildStatusUserID,
			BuildStatusTime
		)
		VALUES
		(
			@ModelExtID,
			@BuildStatusTypeID,
			@BuildStatus,
			@BuildStatusUserID,
			COALESCE(@BuildStatusTime, GETDATE())
		)
		
		SET @NewModelBuildID = SCOPE_IDENTITY();
		
	END
	ELSE
	BEGIN
	
		UPDATE Monitoring.tNDModelBuild
		SET
			ModelExtID = @ModelExtID,
			BuildStatusTypeID = @BuildStatusTypeID,
			BuildStatus = @BuildStatus,
			BuildStatusUserID = @BuildStatusUserID,
			BuildStatusTime = COALESCE(@BuildStatusTime, GETDATE())
		WHERE Monitoring.tNDModelBuild.ModelBuildID = @ModelBuildID
		
		SET @NewModelBuildID = @ModelBuildID;
	
	END
	

END

SELECT @NewModelBuildID
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpsertNDModelBuild] TO [TEUser]
    AS [dbo];

