﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDModelOpModeTypeMap]
  (@ModelOpModeTypeMapID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDModelOpModeTypeMap] WHERE [ModelOpModeTypeMapID]=@ModelOpModeTypeMapID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDModelOpModeTypeMap] TO [TEUser]
    AS [dbo];

