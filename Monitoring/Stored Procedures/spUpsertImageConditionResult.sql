﻿CREATE PROCEDURE [Monitoring].[spUpsertImageConditionResult]
	@SecurityUserID INT
	,@ImageConditionResultID int = NULL
	,@Result NVARCHAR(255)
	,@AssetID INT
	,@IsDefault BIT = 0
	,@NewImageConditionResultID INT OUTPUT
AS
BEGIN
	DECLARE @user int = null;
	SELECT TOP 1 @user = SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID; 
	IF @user IS NULL
	BEGIN
		RAISERROR('User does not exist',16,1)
	END

	IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User does not have permission to access asset',16, 1);
	END

	DECLARE @OldAssetID INT
	SELECT @OldAssetID = AssetID FROM [Monitoring].[tImageConditionResult]
		WHERE @ImageConditionResultID = tImageConditionResult.ImageConditionResultID

	IF (@OldAssetID != @AssetID)
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@OldAssetID, @SecurityUserID, -1) = 0
		BEGIN
			RAISERROR( 'User does not have permissions to overwrite the existing asset',16, 1);
		END
	END			


	IF(@ImageConditionResultID IS NULL OR @ImageConditionResultID <= 0)
	BEGIN
		INSERT INTO Monitoring.tImageConditionResult(
			Result
			,AssetID
			,CreatedBy
			,ChangedBy
			,IsDefault
		)
		VALUES (
			@Result
			,@AssetID
			,@SecurityUserID
			,@SecurityUserID
			,@IsDefault
		)
		SET @NewImageConditionResultID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE Monitoring.tImageConditionResult
		SET
			Result = @Result
			,AssetID = @AssetID
			,ChangedBy = @SecurityUserID
			,ChangeDate = GETDATE()
			,IsDefault = @IsDefault
		WHERE ImageConditionResultID = @ImageConditionResultID
		SET @NewImageConditionResultID = @ImageConditionResultID
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpsertImageConditionResult] TO [TEUser]
    AS [dbo];