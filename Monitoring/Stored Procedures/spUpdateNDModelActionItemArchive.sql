﻿


CREATE PROCEDURE [Monitoring].[spUpdateNDModelActionItemArchive]
	@modelActionItemArchiveID int,
	@modelActionItemPriorityTypeId int = NULL,
	@noteText nvarchar(max),
	@watchDuration real = NULL,
	@watchDurationTemporalTypeId int = NULL,
	@watchCriteriaTypeId int = NULL,
	@watchLimit real = NULL,
	@watchLimitMeetsTemporalTypeId int = NULL,
	@watchRelativeToExpected bit = 0,
	@modelActionItemTypeId int,
	@favorite bit = 0,
	@userId int,
	@watchEmailIfAlert bit = 0,
	@watchDuration2 real = NULL,
	@watchDurationTemporalTypeId2 int = NULL,
	@watchCriteriaTypeId2 int = NULL,
	@watchLimit2 real = NULL,
	@watchLimitMeetsTemporalTypeId2 int = NULL,
	@watchRelativeToExpected2 bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT ModelActionItemArchiveID FROM Monitoring.tNDModelActionItemArchive WHERE ModelActionItemArchiveID = @modelActionItemArchiveID)
	BEGIN
		DECLARE @xmodelActionItemPriorityTypeId int;
		DECLARE @xnoteText nvarchar(max);
		DECLARE @xwatchDuration real;
		DECLARE @xwatchDurationTemporalTypeId int;
		DECLARE @xwatchCriteriaTypeId int;
		DECLARE @xwatchLimit real;
		DECLARE @xwatchLimitMeetsTemporalTypeId int;
		DECLARE @xmodelActionItemTypeId int;
		DECLARE @xchangeDate datetime;
		DECLARE @xwatchDuration2 real;
		DECLARE @xwatchDurationTemporalTypeId2 int;
		DECLARE @xwatchCriteriaTypeId2 int;
		DECLARE @xwatchLimit2 real;
		DECLARE @xwatchLimitMeetsTemporalTypeId2 int;

		SELECT
			@xmodelActionItemPriorityTypeId = ModelActionItemPriorityTypeID
			, @xnoteText = NoteText
			, @xwatchDuration = WatchDuration
			, @xwatchDurationTemporalTypeId = WatchDurationTemporalTypeID
			, @xwatchCriteriaTypeId = WatchCriteriaTypeID
			, @xwatchLimit = WatchLimit
			, @xwatchLimitMeetsTemporalTypeId = WatchLimitMeetsTemporalTypeID  
			, @xmodelActionItemTypeId = ModelActionItemTypeID
			, @xchangeDate = ChangeDate
			, @xwatchDuration2 = WatchDuration2
			, @xwatchDurationTemporalTypeId2 = WatchDurationTemporalTypeID2
			, @xwatchCriteriaTypeId2 = WatchCriteriaTypeID2
			, @xwatchLimit2 = WatchLimit2
			, @xwatchLimitMeetsTemporalTypeId2 = WatchLimitMeetsTemporalTypeID2
		FROM Monitoring.tNDModelActionItemArchive WHERE ModelActionItemArchiveID = @modelActionItemArchiveID

		UPDATE Monitoring.tNDModelActionItemArchive
		SET
			Monitoring.tNDModelActionItemArchive.ModelActionItemPriorityTypeID = COALESCE(@modelActionItemPriorityTypeId, @xmodelActionItemPriorityTypeId)
			, Monitoring.tNDModelActionItemArchive.NoteText = COALESCE(@noteText, @xnoteText)
			, Monitoring.tNDModelActionItemArchive.ChangedByUserID = @userId
			, Monitoring.tNDModelActionItemArchive.ChangeDate = COALESCE(@xchangeDate, GETDATE())
			, Monitoring.tNDModelActionItemArchive.WatchDuration = COALESCE(@watchDuration, @xwatchDuration)
			, Monitoring.tNDModelActionItemArchive.WatchDurationTemporalTypeID = COALESCE(@watchDurationTemporalTypeId, @xwatchDurationTemporalTypeId)
			, Monitoring.tNDModelActionItemArchive.WatchCriteriaTypeID = COALESCE(@watchCriteriaTypeId, @xwatchCriteriaTypeId)
			, Monitoring.tNDModelActionItemArchive.WatchLimit = COALESCE(@watchLimit, @xwatchLimit)
			, Monitoring.tNDModelActionItemArchive.WatchLimitMeetsTemporalTypeID = COALESCE(@watchLimitMeetsTemporalTypeId, @xwatchLimitMeetsTemporalTypeId)
			, Monitoring.tNDModelActionItemArchive.WatchRelativeToExpected = @watchRelativeToExpected
			, Monitoring.tNDModelActionItemArchive.ModelActionItemTypeID = COALESCE(@modelActionItemTypeId, @xmodelActionItemTypeId)
			, Monitoring.tNDModelActionItemArchive.Favorite = @favorite
			, Monitoring.tNDModelActionItemArchive.WatchEmailIfAlert = @watchEmailIfAlert
			, Monitoring.tNDModelActionItemArchive.WatchDuration2 = COALESCE(@watchDuration2, @xwatchDuration2)
			, Monitoring.tNDModelActionItemArchive.WatchDurationTemporalTypeID2 = COALESCE(@watchDurationTemporalTypeId2, @xwatchDurationTemporalTypeId2)
			, Monitoring.tNDModelActionItemArchive.WatchCriteriaTypeID2 = COALESCE(@watchCriteriaTypeId2, @xwatchCriteriaTypeId2)
			, Monitoring.tNDModelActionItemArchive.WatchLimit2 = COALESCE(@watchLimit2, @xwatchLimit2)
			, Monitoring.tNDModelActionItemArchive.WatchLimitMeetsTemporalTypeID2 = COALESCE(@watchLimitMeetsTemporalTypeId2, @xwatchLimitMeetsTemporalTypeId2)
			, Monitoring.tNDModelActionItemArchive.WatchRelativeToExpected2 = @watchRelativeToExpected2
		FROM Monitoring.tNDModelActionItemArchive arc
		INNER JOIN Monitoring.tNDModel mdl ON arc.ModelID = mdl.ModelID
		INNER JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
		INNER JOIN (
			SELECT AssetId
			FROM [Asset].[ufnGetAssetIdsForUser] (@userId)
		) accessibleAssetIds ON map.AssetID = accessibleAssetIds.AssetID
		WHERE arc.ModelActionItemArchiveID = @modelActionItemArchiveID

		SELECT @modelActionItemArchiveID
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpdateNDModelActionItemArchive] TO [TEUser]
    AS [dbo];

