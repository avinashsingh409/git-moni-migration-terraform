﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDModel]
  (@ModelID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDModel] WHERE [ModelID]=@ModelID AND Locked=0;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDModel] TO [TEUser]
    AS [dbo];

