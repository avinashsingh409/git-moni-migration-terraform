﻿

CREATE PROCEDURE [Monitoring].[spGetFilledNDModelInputTagMap]
	@modelID int
AS
BEGIN
	SET NOCOUNT ON;

SELECT
	map.ModelInputTagMapID
	, map.ModelID
	, map.AssetVariableTypeTagMapID
	, map.Active
    , map.[Required]
	, tag.PDTagID
	, tag.TagName
	, tag.TagDesc
	, tag.ExternalID
	, tag.GlobalID
	, tag.NDCreated
	, tag.NDPath
	, tag.NDName
	, tag.NDId
	, recInput.RecInputVariableMapID
	, recInput.Series
	, recInput.Priority
	, recInput.MultipleTagPreprocessingTypeID
FROM [Monitoring].[tNDModelInputTagMap] map
JOIN [ProcessData].[tAssetVariableTypeTagMap] tmap ON map.AssetVariableTypeTagMapID = tmap.AssetVariableTypeTagMapID
JOIN [ProcessData].[tTag] tag ON tmap.TagID = tag.PDTagID
LEFT JOIN [Monitoring].[tNDRecInputVariableMap] recInput ON map.RecInputVariableMapID = recInput.RecInputVariableMapID
WHERE map.ModelID = @modelID
ORDER BY recInput.Series, recInput.Priority


END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetFilledNDModelInputTagMap] TO [TEUser]
    AS [dbo];

