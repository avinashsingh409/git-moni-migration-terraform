﻿		CREATE PROCEDURE [Monitoring].[spOverrideIgnore]
		@modelIdList Base.tpIntList READONLY,
		@securityUserId int
		AS
		BEGIN
			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			INSERT INTO Monitoring.tNDModelActionItem
			(
				ModelID,
				ModelActionItemPriorityTypeID,
				NoteText,
				ModelActionItemTypeID,
				CreatedByUserID,
				CreateDate,
				ChangedByUserID,
				ChangeDate
			)
			SELECT a.ModelID, 1, 'Ignore Override. Value went out of bounds.', 12, @securityUserId, GETDATE(), @securityUserId, GETDATE()
			FROM Monitoring.tNDModel_AlertScreening a
			INNER JOIN @modelIdList b ON a.ModelID = b.id
			LEFT JOIN Monitoring.tNDModelActionItem c ON c.ModelID = a.ModelID
			WHERE c.ModelID IS NULL

			UPDATE Monitoring.tNDModelActionItem
			SET
				Monitoring.tNDModelActionItem.ModelActionItemPriorityTypeID = 1, -- int
				Monitoring.tNDModelActionItem.NoteText = N'Ignore Override. Value went out of bounds.', -- nvarchar
				Monitoring.tNDModelActionItem.ChangedByUserID = @securityUserId, -- int
				Monitoring.tNDModelActionItem.ChangeDate = GETDATE(),
				Monitoring.tNDModelActionItem.ModelActionItemTypeID = 12 -- int	
			FROM @modelIdList b 
			LEFT JOIN Monitoring.tNDModelActionItem c ON c.ModelID = b.id
			WHERE c.ModelID IS NOT NULL

			DELETE Monitoring.tNDModel_AlertScreening
			FROM Monitoring.tNDModel_AlertScreening
			INNER JOIN @modelIdList b ON Monitoring.tNDModel_AlertScreening.ModelID = b.id
			
		END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spOverrideIgnore] TO [TEUser]
    AS [dbo];

