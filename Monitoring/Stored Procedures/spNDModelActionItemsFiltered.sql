﻿CREATE PROCEDURE [Monitoring].[spNDModelActionItemsFiltered]
 @securityUserId int,    
 @startAssetId int,    
 @modelId int,    
 @actionItemTypeID Base.tpIntList READONLY,    
 @timestampStart datetime = NULL,    
 @timestampEnd datetime = NULL,    
 @executor NVARCHAR(100) = NULL,    
 @noteText NVARCHAR(255) = NULL,    
 @issueCreated bit = NULL,    
 @issueClosed bit = NULL,    
 @startIndex int = 1,    
 @endIndex int = 1,    
 @sort NVARCHAR(100) = NULL,    
 @sortOrder bit = 0,    
 @favorite bit = NULL    
WITH RECOMPILE    
AS    
BEGIN    
 SET NOCOUNT ON;    
 SET ARITHABORT ON    
    
 Declare @noteAddedTypeID int = 10    
    
 DECLARE @assets base.tpIntList    
    
 IF @modelId IS NULL    
   BEGIN    
   INSERT INTO @assets SELECT AssetId FROM Asset.ufnGetAssetIdsForUserStartingAt(@securityUserId, @startAssetId);    
   END    
    

 CREATE TABLE #modelArchiveIds (    
  ModelArchiveId int,    
  ModelConditionId int,    
  NoteSource int
 )    
   
  /*TABLOCK hints here give parity with SELECT INTO*/ 
 --Alerts Notes: Monitoring.tNDModelActionItemArchive    
 INSERT INTO #modelArchiveIds WITH(TABLOCK) (ModelArchiveId, ModelConditionId, NoteSource)    
 SELECT    
  alertsNoteIds.ModelActionItemArchiveID, null, 1 as NoteSource     
 FROM [Monitoring].[ufnGetFilteredModelActionItemArchiveIDs] (    
  @modelId, @actionItemTypeID,    
  @timestampStart, @timestampEnd, @executor, @noteText, @assets, @favorite    
 ) alertsNoteIds;    
     
 --Dashboard Notes: Monitoring.tModelConditionNote    
 Insert Into #modelArchiveIds WITH(TABLOCK) (ModelArchiveId, ModelConditionId, NoteSource)    
 Select     
  null, dashboardNoteIds.ModelConditionNoteID, 2 as NoteSource    
 From [Monitoring].[ufnGetFilteredModelConditionNoteIDs] (    
  @modelId, @actionItemTypeID,    
  @timestampStart, @timestampEnd, @executor, @noteText, @assets       
 ) dashboardNoteIds;    

 /*This is a nice thing to do*/
 CREATE CLUSTERED INDEX c ON #modelArchiveIds (ModelArchiveId, ModelConditionId, NoteSource);
    

/*Way too much going on in the original query, dump this into a temp table*/
 WITH records AS (    
  SELECT ROW_NUMBER() OVER (    
    
  ORDER BY    
  CASE WHEN @sort = 'Favorite' AND @sortOrder = 0 THEN mdlArch.Favorite ELSE NULL END,    
  CASE WHEN @sort = 'ActionItemTypeAbbrev' AND @sortOrder = 0 THEN mdlArch.ModelActionItemTypeAbbrev ELSE NULL END,    
  CASE WHEN @sort = 'HistoryTimestamp' AND @sortOrder = 0 THEN mdlArch.ChangeDate ELSE NULL END,    
  CASE WHEN @sort = 'Executor' AND @sortOrder = 0 THEN mdlArch.Executor ELSE NULL END,    
  CASE WHEN @sort = 'NoteText' AND @sortOrder = 0 THEN mdlArch.NoteText ELSE NULL END,    
  CASE WHEN @sort = 'Favorite' AND @sortOrder = 1 THEN mdlArch.Favorite ELSE NULL END DESC, --Default case of prioritizing favorites over the rest, sorted in descending order of change date.    
  CASE WHEN @sort = 'Favorite' AND @sortOrder = 1 THEN mdlArch.ChangeDate ELSE NULL END DESC, --Default case of prioritizing favorites over the rest, sorted in descending order of change date.    
  CASE WHEN @sort = 'ActionItemTypeAbbrev' AND @sortOrder = 1 THEN mdlArch.ModelActionItemTypeAbbrev ELSE NULL END DESC,    
  CASE WHEN @sort = 'HistoryTimestamp' AND @sortOrder = 1 THEN mdlArch.ChangeDate ELSE NULL END DESC,    
  CASE WHEN @sort = 'Executor' AND @sortOrder = 1 THEN mdlArch.Executor ELSE NULL END DESC,    
  CASE WHEN @sort = 'NoteText' AND @sortOrder = 1 THEN mdlArch.NoteText ELSE NULL END DESC,    
  mdlArch.ModelActionItemArchiveID, mdlArch.ModelConditionNoteID    
  ) AS ROW_NUMBER, mdlArch.ModelActionItemArchiveID, mdlArch.ModelConditionNoteID, mdlArch.NoteSource  
  FROM (    
   SELECT    
    mdlArchive.ModelActionItemArchiveID as ModelActionItemArchiveID    
    , null as ModelConditionNoteID    
    , 1 as NoteSource    
    , mdlArchive.ModelID    
    , mdlArchive.ModelActionItemTypeID    
    , mdlactiontype.ModelActionItemTypeAbbrev    
    , mdlArchive.ModelActionItemPriorityTypeID    
    , mdlArchive.ChangeDate    
    , case when usr.ServiceAccount=1 then 'Asset360 System' else usr.Email end as Executor    
    , mdlArchive.NoteText    
    , mdlArchive.WatchDuration    
    , mdlArchive.WatchDurationTemporalTypeID    
    , mdlArchive.WatchCriteriaTypeID    
    , mdlArchive.WatchLimit    
    , mdlArchive.WatchLimitMeetsTemporalTypeID    
    , mdlArchive.WatchRelativeToExpected    
    , map.AssetID    
    , tag.EngUnits    
    , mdlArchive.Favorite    
    , mdlArchive.WatchEmailIfAlert    
    , mdlArchive.WatchDuration2 
    , mdlArchive.WatchDurationTemporalTypeID2
    , mdlArchive.WatchCriteriaTypeID2
    , mdlArchive.WatchLimit2
    , mdlArchive.WatchLimitMeetsTemporalTypeID2
    , mdlArchive.WatchRelativeToExpected2
   FROM [Monitoring].[tNDModelActionItemArchive] mdlArchive    
   JOIN [Monitoring].[tNDModelActionItemType] mdlactiontype ON mdlArchive.ModelActionItemTypeID = mdlactiontype.ModelActionItemTypeID    
   JOIN [Monitoring].[tNDModel] model ON mdlArchive.ModelID = model.ModelID    
   JOIN [ProcessData].[tAssetVariableTypeTagMap] map ON model.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID    
   LEFT JOIN [ProcessData].[tTag] tag ON map.TagID = tag.PDTagID    
   JOIN [AccessControl].[tUser] usr ON mdlArchive.ChangedByUserID = usr.SecurityUserID    
   INNER JOIN #modelArchiveIds modelArchiveIdCheck 
    ON modelArchiveIdCheck.ModelArchiveId = mdlArchive.ModelActionItemArchiveID 
   Where modelArchiveIdCheck.NoteSource = 1 	
	
   UNION ALL
    
   select    
    null as ModelActionItemArchiveID    
    , n.ModelConditionNoteID as ModelConditionNoteID    
    , 2 as NoteSource    
    , a.ModelID --Alerts ModelID, Dashboard ModelID is d.ModelID    
    , @noteAddedTypeID as ModelActionItemTypeID    
    , ait.ModelActionItemTypeAbbrev    
    , 0 as ModelActionItemPriorityTypeID    
    , n.[TimeStamp] as ChangeDate    
    , n.ChangedBy as Executor    
    , n.Note as NoteText    
    , null as WatchDuration    
    , null as WatchDurationTemporalTypeID    
    , null as WatchCriteriaTypeID    
    , null as WatchLimit    
    , null as WatchLimitMeetsTemporalTypeID    
    , null as WatchRelativeToExpected    
    , m.AssetID    
    , t.EngUnits    
    , null as Favorite
    , null as WatchEmailIfAlert    
    , null as WatchDuration2  
    , null as WatchDurationTemporalTypeID2
    , null as WatchCriteriaTypeID2
    , null as WatchLimit2
    , null as WatchLimitMeetsTemporalTypeID2
    , null as WatchRelativeToExpected2
   from Monitoring.tNDModel a    
   join Monitoring.tModel d on a.AssetVariableTypeTagMapID = d.AssetVariableTypeTagMapID    
   join Monitoring.tModelCondition o on d.ModelID = o.ModelID    
   join Monitoring.tModelConditionNote n on o.ModelConditionID = n.ModelConditionID    
   join Monitoring.tNDModelActionItemType ait on @noteAddedTypeID = ait.ModelActionItemTypeID    
   join ProcessData.tAssetVariableTypeTagMap m on d.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID    
   join ProcessData.tTag t on m.TagID = t.PDTagID
  INNER JOIN #modelArchiveIds modelArchiveIdCheck 
    ON modelArchiveIdCheck.ModelConditionId = n.ModelConditionNoteID
 Where modelArchiveIdCheck.NoteSource = 2 	
    
  ) mdlArch         
 )   
 SELECT *
 INTO #records 
 FROM records AS r
 WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex    
  
  /*This is also nice to do*/
 CREATE CLUSTERED INDEX c ON #records(ModelActionItemArchiveID, ModelConditionNoteID, NoteSource, ROW_NUMBER)



 SELECT    
    modelarchiverecs.ROW_NUMBER    
  , modelarchiverecs.ModelActionItemArchiveID    
  , modelarchiverecs.ModelConditionNoteID    
  , modelarchiverecs.ModelID    
  , modelarchiverecs.ModelActionItemTypeID    
  , modelarchiverecs.ModelActionItemTypeAbbrev    
  , modelarchiverecs.ModelActionItemPriorityTypeID    
  , modelarchiverecs.ChangeDate    
  , modelarchiverecs.Executor    
  , modelarchiverecs.NoteText    
  , modelarchiverecs.WatchDuration    
  , modelarchiverecs.WatchDurationTemporalTypeID    
  , modelarchiverecs.WatchCriteriaTypeID    
  , modelarchiverecs.WatchLimit    
  , modelarchiverecs.WatchLimitMeetsTemporalTypeID    
  , modelarchiverecs.WatchRelativeToExpected    
  , modelarchiverecs.AssetID    
  , modelarchiverecs.EngUnits    
  , modelarchiverecs.Favorite
  , modelarchiverecs.WatchEmailIfAlert    
  , modelarchiverecs.WatchDuration2  
  , modelarchiverecs.WatchDurationTemporalTypeID2
  , modelarchiverecs.WatchCriteriaTypeID2
  , modelarchiverecs.WatchLimit2
  , modelarchiverecs.WatchLimitMeetsTemporalTypeID2
  , modelarchiverecs.WatchRelativeToExpected2
 FROM (    
  SELECT    
   mdlArchive.ModelActionItemArchiveID as ModelActionItemArchiveID    
   , null as ModelConditionNoteID    
   , 1 as NoteSource    
   , mdlArchive.ModelID    
   , mdlArchive.ModelActionItemTypeID    
   , mdlactiontype.ModelActionItemTypeAbbrev    
   , mdlArchive.ModelActionItemPriorityTypeID    
   , mdlArchive.ChangeDate    
   , case when usr.ServiceAccount=1 then 'Asset360 System' else usr.Email end as Executor    
   , mdlArchive.NoteText    
   , mdlArchive.WatchDuration    
   , mdlArchive.WatchDurationTemporalTypeID    
   , mdlArchive.WatchCriteriaTypeID    
   , mdlArchive.WatchLimit    
   , mdlArchive.WatchLimitMeetsTemporalTypeID    
   , mdlArchive.WatchRelativeToExpected    
   , map.AssetID    
   , tag.EngUnits    
   , mdlArchive.Favorite
   , mdlArchive.WatchEmailIfAlert    
   , mdlArchive.WatchDuration2  
   , mdlArchive.WatchDurationTemporalTypeID2
   , mdlArchive.WatchCriteriaTypeID2
   , mdlArchive.WatchLimit2
   , mdlArchive.WatchLimitMeetsTemporalTypeID2
   , mdlArchive.WatchRelativeToExpected2
   , r.ROW_NUMBER 
  FROM [Monitoring].[tNDModelActionItemArchive] mdlArchive    
  JOIN [Monitoring].[tNDModelActionItemType] mdlactiontype ON mdlArchive.ModelActionItemTypeID = mdlactiontype.ModelActionItemTypeID    
  JOIN [Monitoring].[tNDModel] model ON mdlArchive.ModelID = model.ModelID    
  JOIN [ProcessData].[tAssetVariableTypeTagMap] map ON model.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID    
  LEFT JOIN [ProcessData].[tTag] tag ON map.TagID = tag.PDTagID    
  JOIN [AccessControl].[tUser] usr ON mdlArchive.ChangedByUserID = usr.SecurityUserID
  INNER JOIN #records r ON mdlArchive.ModelActionItemArchiveID = r.ModelActionItemArchiveID
  WHERE (r.NoteSource = 1)
    
  UNION ALL 
    
  select    
   null as ModelActionItemArchiveID    
   , n.ModelConditionNoteID as ModelConditionNoteID    
   , 2 as NoteSource    
   , a.ModelID --Alerts ModelID, Dashboard ModelID is d.ModelID    
   , @noteAddedTypeID as ModelActionItemTypeID    
   , ait.ModelActionItemTypeAbbrev    
   , 0 as ModelActionItemPriorityTypeID    
   , n.[TimeStamp] as ChangeDate    
   , n.ChangedBy as Executor    
   , n.Note as NoteText    
   , null as WatchDuration    
   , null as WatchDurationTemporalTypeID    
   , null as WatchCriteriaTypeID    
   , null as WatchLimit    
   , null as WatchLimitMeetsTemporalTypeID    
   , null as WatchRelativeToExpected    
   , m.AssetID    
   , t.EngUnits    
   , null as Favorite
   , null as WatchEmailIfAlert    
   , null as WatchDuration2 
   , null as WatchDurationTemporalTypeID2
   , null as WatchCriteriaTypeID2
   , null as WatchLimit2
   , null as WatchLimitMeetsTemporalTypeID2
   , null as WatchRelativeToExpected2
   , r.ROW_NUMBER
  from Monitoring.tNDModel a    
  join Monitoring.tModel d on a.AssetVariableTypeTagMapID = d.AssetVariableTypeTagMapID    
  join Monitoring.tModelCondition o on d.ModelID = o.ModelID    
  join Monitoring.tModelConditionNote n on o.ModelConditionID = n.ModelConditionID    
  join Monitoring.tNDModelActionItemType ait on @noteAddedTypeID = ait.ModelActionItemTypeID    
  join ProcessData.tAssetVariableTypeTagMap m on d.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID    
  join ProcessData.tTag t on m.TagID = t.PDTagID
 INNER JOIN #records r ON n.ModelConditionNoteID = r.ModelConditionNoteID 
 WHERE (r.NoteSource = 2)    
 ) modelarchiverecs     
 WHERE modelarchiverecs.ROW_NUMBER BETWEEN @startIndex AND @endIndex    
 ORDER BY modelarchiverecs.ROW_NUMBER    
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spNDModelActionItemsFiltered] TO [TEUser]
    AS [dbo];
