﻿
CREATE PROCEDURE [Monitoring].[spGetNDModelOpModeTypeMap]
  (@ModelOpModeTypeMapID int = NULL)
AS
BEGIN
  IF @ModelOpModeTypeMapID IS NULL OR @ModelOpModeTypeMapID = 0
    SELECT [ModelOpModeTypeMapID],
          [ModelID],
          [OpModeTypeID]
    FROM [Monitoring].[tNDModelOpModeTypeMap];
  ELSE
    SELECT [ModelOpModeTypeMapID],
          [ModelID],
          [OpModeTypeID]
    FROM [Monitoring].[tNDModelOpModeTypeMap] WHERE [ModelOpModeTypeMapID] = @ModelOpModeTypeMapID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelOpModeTypeMap] TO [TEUser]
    AS [dbo];

