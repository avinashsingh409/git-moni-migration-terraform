CREATE   PROCEDURE [Monitoring].[spNDModelsFilteredWithModelIDs]
  @securityUserID           INT,
  @startAssetID             INT = NULL,
  @astVarTypeTagMapId       INT = NULL,
  @active                   BIT = NULL,
  @built                    BIT = NULL,
  @hasCalcPoint             BIT = NULL,
  @tagName                  NVARCHAR(400) = NULL,
  @tagDesc                  NVARCHAR(400) = NULL,
  @modelName                NVARCHAR(255) = NULL,
  @modelTypeID              Base.TPINTLIST READONLY,
  @engUnits                 NVARCHAR(255) = NULL,
  @modelOpModeID            Base.TPINTLIST READONLY,
  @isDiagnose               BIT = NULL,
  @isWatch                  BIT = NULL,
  @isWatchOverride          BIT = NULL,
  @isMaintenance            BIT = NULL,
  @isIgnore                 BIT = NULL,
  @isOpenIssue              BIT = NULL,
  @evaluationErrorMessage   NVARCHAR(400) = NULL,
  @priority                 INT = NULL,
  @outOfBounds              BIT = NULL,
  @alertPriority            INT = NULL,
  @alertType                NVARCHAR(255) = NULL,
  @isAlert                  BIT = NULL,
  @addedToAlertsStart       DATETIME = NULL,
  @addedToAlertEnd          DATETIME = NULL,
  @currentBuildStatusTypeID Base.TPINTLIST READONLY,
  @startIndex               INT = 1,
  @endIndex                 INT = 15,
  @sort                     NVARCHAR(100) = NULL,
  @sortOrder                BIT = 0,
  @assetCriticality			Base.TPINTLIST READONLY,
  @modelCriticality			Base.TPINTLIST READONLY,
  @modelIDs					Base.TPINTLIST READONLY,
  @filterCategoryIDs		Base.TPINTLIST READONLY
WITH RECOMPILE
AS
BEGIN

  SET NOCOUNT ON;
  SET ARITHABORT ON

  DECLARE @assets base.TPINTLIST

  DECLARE @mdlCnt as int
  SELECT @mdlCnt = COUNT(*) FROM @modelIDs

  -- only get assets that are associated with models, and prefilter based on active and/or built, calc point, and tag map
  if @mdlCnt >= 1
  begin
		insert into @assets
		select DISTINCT map.AssetID from ProcessData.tAssetVariableTypeTagMap map
		join Monitoring.tNDModel a on map.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
		join @modelIDs m on a.ModelID = m.id
		where (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) 			
			AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID, @securityUserID, -1) = 1 
			OPTION (RECOMPILE)			
  end
  else if @startAssetID is not null and @mdlCnt>0
  begin
	  INSERT INTO @assets
	  select distinct b.assetid from monitoring.tndmodel a JOIN ProcessData.tAssetVariableTypeTagMap b on 
	  a.AssetVariableTypeTagMapID = b.AssetVariableTypeTagMapID
	  JOIN  Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @startAssetID) c on b.AssetID = c.AssetID
	  JOIN @modelIDs m ON a.ModelID = m.id
	  WHERE (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) 
			OPTION (RECOMPILE)
  end
  else if @startAssetID is not null
  begin
	  INSERT INTO @assets
	  select distinct b.assetid from monitoring.tndmodel a JOIN ProcessData.tAssetVariableTypeTagMap b on 
	  a.AssetVariableTypeTagMapID = b.AssetVariableTypeTagMapID
	  JOIN  Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @startAssetID) c on b.AssetID = c.AssetID
	  WHERE (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) 
			AND (@mdlCnt = 0 OR a.ModelID IN (SELECT MODELID FROM @modelIDs)) OPTION (RECOMPILE)
  end
  else if @astVarTypeTagMapId is not null
  begin
	  insert into @assets
	  select distinct map.AssetID from ProcessData.tAssetVariableTypeTagMap map
	  join Monitoring.tNDModel a on map.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
	  where (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) 
			AND (@mdlCnt = 0 OR a.ModelID IN (SELECT MODELID FROM @modelIDs))
			AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID, @securityUserID, -1) = 1 OPTION (RECOMPILE)
  end  
  else -- fall back to all assets to which the user has access
  begin
		insert into @assets
		select distinct map.AssetID from ProcessData.tAssetVariableTypeTagMap map
		join Monitoring.tNDModel a on map.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
		join Asset.ufnGetAssetIdsForUser(@securityUserID) s on map.AssetID = s.AssetID
  		WHERE (@active is null or a.Active = @active) AND (@built is null or a.Built=@built) 
			AND (@hasCalcPoint is null or (@hasCalcPoint = 1 AND a.CalcPointID IS NOT NULL) or (@hasCalcPoint = 0 AND a.CalcPointID IS NULL))
			AND (@astVarTypeTagMapId is null or a.AssetVariableTypeTagMapID = @astVarTypeTagMapId) 
			AND (@mdlCnt = 0 OR a.ModelID IN (SELECT MODELID FROM @modelIDs)) OPTION (RECOMPILE)
  end
 
  DECLARE @modelInfo TABLE
  (
    ModelID                        INT      NULL,
	ReferenceModelID			   NVARCHAR(400) NULL,
    ModelExtID                     VARCHAR(MAX),
    Active                         BIT      NULL,
    Built                          BIT      NULL,
    TagName                        VARCHAR(MAX),
    TagDesc                        VARCHAR(MAX),
	TagNDId						   NVARCHAR(400) NULL,
    ModelName                      VARCHAR(MAX),
    ModelTypeID                    INT      NULL,
    ModelTypeAbbrev                VARCHAR(MAX),
    Score                          INT      NULL,
    EngUnits                       VARCHAR(MAX),
    AssetAbbrev                    VARCHAR(MAX),
    AssetId                        INT      NULL,
	GlobalID					   VARCHAR(MAX),
    UnitAbbrev                     VARCHAR(MAX),
    UnitId                         INT      NULL,
    MAELower                       REAL     NULL,
    MAEUpper                       REAL     NULL,
    ActionDiagnose                 BIT      NULL,
    ActionWatch                    BIT      NULL,
    ActionWatchOverride            BIT      NULL,
    ActionModelMaintenance         BIT      NULL,
    HasOpenIssue                   BIT      NULL,
    Alerts                         INT      NULL,
    Alert                          BIT      NULL,
    PercentOutOfBounds             REAL     NULL,
    UpperLimit                     REAL     NULL,
    LowerLimit                     REAL     NULL,
    Actual                         REAL     NULL,
    Expected                       REAL     NULL,
    HiHiAlert                      BIT      NULL,
    HiHiAlertSinceTime             DATETIME NULL,
    LowLowAlert                    BIT      NULL,
    LowLowAlertSinceTime           DATETIME NULL,
    AreaFastResponseAlert          BIT      NULL,
    AreaFastResponseAlertSinceTime DATETIME NULL,
    AreaSlowResponseAlert          BIT      NULL,
    AreaSlowResponseAlertSinceTime DATETIME NULL,
    FrequencyAlert                 BIT      NULL,
    FrequencyAlertSinceTime        DATETIME NULL,
    OscillationAlert               BIT      NULL,
    OscillationAlertSinceTime      DATETIME NULL,
    FrozenDataAlert                BIT      NULL,
    FrozenDataAlertSinceTime       DATETIME NULL,
    WatchOverrideAlert             BIT      NULL,
    WatchOverrideAlertSinceTime    DATETIME NULL,
    IgnoreOverrideAlert            BIT      NULL,
    IgnoreOverrideAlertSinceTime   DATETIME NULL,
    SerializedOpMode               INT      NULL,
    PrioritizedOpMode              VARCHAR(MAX),
    EvaluationErrorMessage         VARCHAR(MAX),
    AddedToAlerts                  DATETIME NULL,
    AlertStatusPriorityTypeID      INT      NULL,
    CalcAlertPriority              INT,
    AlertStatusPriorityTypeDesc    VARCHAR(255),
    ModelActionItemPriorityTypeID  INT      NULL,
    NotePriority                   VARCHAR(MAX),
    LastNote                       VARCHAR(MAX),
    LastNoteDate                   DATETIME NULL,
    TagGroup                       VARCHAR(MAX),
    OutOfBounds                    INT      NULL,
    Ignore                         BIT      NULL,
    BuildStatusTypeAbbrev          VARCHAR(MAX),
    BuildStatus                    VARCHAR(MAX),
	IsStandard					   BIT,
	AssetCriticality			   INT,
	ModelCriticality			   INT,	
    ModelConfigURL                 VARCHAR(2048) NULL,
    DiagnosticDrilldownURL         VARCHAR(2048) NULL
  )

  INSERT INTO @modelInfo
    SELECT
      ModelID,
	  ReferenceModelID,
      ModelExtID,
      Active,
      Built,
      TagName,
      TagDesc,
	  TagNDId,
      ModelName,
      ModelTypeID,
      ModelTypeAbbrev,
      Score,
      EngUnits,
      AssetAbbrev,
      AssetID,
	  GlobalID,
      UnitAbbrev,
      UnitID,
      MAELower,
      MAEUpper,
      ActionDiagnose,
      ActionWatch,
      ActionWatchOverride,
      ActionModelMaintenance,
      HasOpenIssue,
      Alerts,
      Alert,
      PercentOutOfBounds,
      UpperLimit,
      LowerLimit,
      Actual,
      Expected,
      HiHiAlert,
      HiHiAlertSinceTime,
      LowLowAlert,
      LowLowAlertSinceTime,
      AreaFastResponseAlert,
      AreaFastResponseAlertSinceTime,
      AreaSlowResponseAlert,
      AreaSlowResponseAlertSinceTime,
      FrequencyAlert,
      FrequencyAlertSinceTime,
      OscillationAlert,
      OscillationAlertSinceTime,
      FrozenDataAlert,
      FrozenDataAlertSinceTime,
      WatchOverrideAlert,
      WatchOverrideAlertSinceTime,
      IgnoreOverrideAlert,
      IgnoreOverrideAlertSinceTime,
      SerializedOpMode,
      PrioritizedOpMode,
      EvaluationErrorMessage,
      AddedToAlerts,
      AlertStatusPriorityTypeID,
      CalcAlertPriority,
      AlertStatusPriorityTypeDesc,
      ModelActionItemPriorityTypeID,
      NotePriority,
      LastNote,
      LastNoteDate,
      TagGroup,
      OutofBounds,
      Ignore,
      BuildStatusTypeAbbrev,
      BuildStatus,
	  IsStandard,
	  AssetCriticality,
	  ModelCriticality,
      ModelConfigURL,
      DiagnosticDrilldownURL
    FROM [Monitoring].[ufnGetFilteredModelInfoFromAssetIDs]( @assets, @isAlert, @addedToAlertsStart, @addedToAlertEnd, @active,
             @built, @hasCalcPoint, @tagName, @tagDesc, @modelName,
             @modelTypeID, @engUnits, @modelOpModeID, @isDiagnose, @isWatch,
             @isWatchOverride, @isMaintenance, @isIgnore,
             @isOpenIssue, @evaluationErrorMessage, @priority, @outOfBounds, @alertPriority, @alertType
             , @currentBuildStatusTypeID, @securityUserID, @assetCriticality, @modelCriticality, @modelIds, @filterCategoryIDs) OPTION (RECOMPILE);

  WITH records AS (
      SELECT
        ROW_NUMBER()
        OVER (

          ORDER BY
            CASE WHEN @sort = 'ModelID' AND @sortOrder = 0
              THEN model.ModelID
            ELSE NULL END,
			CASE WHEN @sort = 'Active' AND @sortOrder = 0
              THEN model.Active
            ELSE NULL END,
            CASE WHEN @sort = 'Built' AND @sortOrder = 0
              THEN model.Built
            ELSE NULL END,
            CASE WHEN @sort = 'TagName' AND @sortOrder = 0
              THEN model.TagName
            ELSE NULL END,
            CASE WHEN @sort = 'TagDesc' AND @sortOrder = 0
              THEN model.TagDesc
            ELSE NULL END,
            CASE WHEN @sort = 'ModelName' AND @sortOrder = 0
              THEN model.ModelName
            ELSE NULL END,
            CASE WHEN @sort = 'ModelTypeAbbrev' AND @sortOrder = 0
              THEN model.ModelTypeAbbrev
            ELSE NULL END,
            CASE WHEN @sort = 'Score' AND @sortOrder = 0
              THEN model.Score
            ELSE NULL END,
            CASE WHEN @sort = 'EngUnits' AND @sortOrder = 0
              THEN model.EngUnits
            ELSE NULL END,
            CASE WHEN @sort = 'AssetAbbrev' AND @sortOrder = 0
              THEN model.AssetAbbrev
            ELSE NULL END,
            CASE WHEN @sort = 'NotePriority' AND @sortOrder = 0
              THEN model.NotePriority
            ELSE NULL END,
            CASE WHEN @sort = 'LastNoteDate' AND @sortOrder = 0
              THEN model.LastNoteDate
            ELSE NULL END,
            CASE WHEN @sort = 'TagGroup' AND @sortOrder = 0
              THEN model.TagGroup
            ELSE NULL END,
            CASE WHEN @sort = 'BuildStatusAbbrev' AND @sortOrder = 0
              THEN model.BuildStatusTypeAbbrev
            ELSE NULL END,
            CASE WHEN @sort = 'UnitAbbrev' AND @sortOrder = 0
              THEN model.UnitAbbrev
            ELSE NULL END,
            CASE WHEN @sort = 'PercentOutOfBounds' AND @sortOrder = 0
              THEN model.PercentOutOfBounds
            ELSE NULL END,
            CASE WHEN @sort = 'LowerLimit' AND @sortOrder = 0
              THEN model.LowerLimit
            ELSE NULL END,
            CASE WHEN @sort = 'UpperLimit' AND @sortOrder = 0
              THEN model.UpperLimit
            ELSE NULL END,
            CASE WHEN @sort = 'MAELower' AND @sortOrder = 0
              THEN model.MAELower
            ELSE NULL END,
            CASE WHEN @sort = 'MAEUpper' AND @sortOrder = 0
              THEN model.MAEUpper
            ELSE NULL END,
            CASE WHEN @sort = 'Actual' AND @sortOrder = 0
              THEN model.Actual
            ELSE NULL END,
            CASE WHEN @sort = 'Expected' AND @sortOrder = 0
              THEN model.Expected
            ELSE NULL END,
            CASE WHEN @sort = 'PrioritizedOpMode' AND @sortOrder = 0
              THEN model.PrioritizedOpMode
            ELSE NULL END,
            CASE WHEN @sort = 'StatusInfo' AND @sortOrder = 0
              THEN model.EvaluationErrorMessage
            ELSE NULL END,
            CASE WHEN @sort = 'AddedToAlerts' AND @sortOrder = 0
              THEN model.AddedToAlerts
            ELSE NULL END,
            CASE WHEN @sort = 'AlertPriority,TagGroup' AND @sortOrder = 0
              THEN model.CalcAlertPriority
            ELSE NULL END,
            CASE WHEN @sort = 'AlertPriority,TagGroup' AND @sortOrder = 0
              THEN model.TagGroup
            ELSE NULL END,
			CASE WHEN @sort = 'IsStandard' AND @sortOrder = 0
			  THEN model.IsStandard
			ELSE NULL END,
			CASE WHEN @sort = 'AssetCriticality' AND @sortOrder = 0
			  THEN model.AssetCriticality
			ELSE NULL END,
			CASE WHEN @sort = 'ModelCriticality' AND @sortOrder = 0
			  THEN model.ModelCriticality
			ELSE NULL END,
            CASE WHEN @sort = 'ModelID' AND @sortOrder = 1
              THEN model.ModelID
            ELSE NULL END DESC,
            CASE WHEN @sort = 'Active' AND @sortOrder = 1
              THEN model.Active
            ELSE NULL END DESC,
            CASE WHEN @sort = 'Built' AND @sortOrder = 1
              THEN model.Built
            ELSE NULL END DESC,
            CASE WHEN @sort = 'TagName' AND @sortOrder = 1
              THEN model.TagName
            ELSE NULL END DESC,
            CASE WHEN @sort = 'TagDesc' AND @sortOrder = 1
              THEN model.TagDesc
            ELSE NULL END DESC,
            CASE WHEN @sort = 'ModelName' AND @sortOrder = 1
              THEN model.ModelName
            ELSE NULL END DESC,
            CASE WHEN @sort = 'ModelTypeAbbrev' AND @sortOrder = 1
              THEN model.ModelTypeAbbrev
            ELSE NULL END DESC,
            CASE WHEN @sort = 'Score' AND @sortOrder = 1
              THEN model.Score
            ELSE NULL END DESC,
            CASE WHEN @sort = 'EngUnits' AND @sortOrder = 1
              THEN model.EngUnits
            ELSE NULL END DESC,
            CASE WHEN @sort = 'AssetAbbrev' AND @sortOrder = 1
              THEN model.AssetAbbrev
            ELSE NULL END DESC,
            CASE WHEN @sort = 'NotePriority' AND @sortOrder = 1
              THEN model.NotePriority
            ELSE NULL END DESC,
            CASE WHEN @sort = 'LastNoteDate' AND @sortOrder = 1
              THEN model.LastNoteDate
            ELSE NULL END DESC,
            CASE WHEN @sort = 'TagGroup' AND @sortOrder = 1
              THEN model.TagGroup
            ELSE NULL END DESC,
            CASE WHEN @sort = 'BuildStatusAbbrev' AND @sortOrder = 1
              THEN model.BuildStatusTypeAbbrev
            ELSE NULL END DESC,
            CASE WHEN @sort = 'UnitAbbrev' AND @sortOrder = 1
              THEN model.UnitAbbrev
            ELSE NULL END DESC,
            CASE WHEN @sort = 'PercentOutOfBounds' AND @sortOrder = 1
              THEN model.PercentOutOfBounds
            ELSE NULL END DESC,
            CASE WHEN @sort = 'LowerLimit' AND @sortOrder = 1
              THEN model.LowerLimit
            ELSE NULL END DESC,
            CASE WHEN @sort = 'UpperLimit' AND @sortOrder = 1
              THEN model.UpperLimit
            ELSE NULL END DESC,
            CASE WHEN @sort = 'MAELower' AND @sortOrder = 1
              THEN model.MAELower
            ELSE NULL END DESC,
            CASE WHEN @sort = 'MAEUpper' AND @sortOrder = 1
              THEN model.MAEUpper
            ELSE NULL END DESC,
            CASE WHEN @sort = 'Actual' AND @sortOrder = 1
              THEN model.Actual
            ELSE NULL END DESC,
            CASE WHEN @sort = 'Expected' AND @sortOrder = 1
              THEN model.Expected
            ELSE NULL END DESC,
            CASE WHEN @sort = 'PrioritizedOpMode' AND @sortOrder = 1
              THEN model.PrioritizedOpMode
            ELSE NULL END DESC,
            CASE WHEN @sort = 'StatusInfo' AND @sortOrder = 1
              THEN model.EvaluationErrorMessage
            ELSE NULL END DESC,
            CASE WHEN @sort = 'AddedToAlerts' AND @sortOrder = 1
              THEN model.AddedToAlerts
            ELSE NULL END DESC,
            CASE WHEN @sort = 'AlertPriority,TagGroup' AND @sortOrder = 1
              THEN model.CalcAlertPriority
            ELSE NULL END DESC,
            CASE WHEN @sort = 'AlertPriority,TagGroup' AND @sortOrder = 1
              THEN model.TagGroup
            ELSE NULL END DESC,
			CASE WHEN @sort = 'IsStandard' AND @sortOrder = 1
			  THEN model.IsStandard
			ELSE NULL END DESC,
			CASE WHEN @sort = 'AssetCriticality' AND @sortOrder = 1
			  THEN model.AssetCriticality
			ELSE NULL END DESC,
			CASE WHEN @sort = 'ModelCriticality' AND @sortOrder = 1
			  THEN model.ModelCriticality
			ELSE NULL END DESC,
            model.ModelID
          ) AS ROW_NUMBER,
        model.ModelID
      FROM @modelInfo model
  )

  SELECT
    r.ROW_NUMBER
    , modelrecs.*
  FROM @modelInfo modelrecs
    INNER JOIN records r
      ON modelrecs.ModelID = r.ModelID
  WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex
  ORDER BY r.ROW_NUMBER
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spNDModelsFilteredWithModelIDs] TO [TEUser]
    AS [dbo];

GO
