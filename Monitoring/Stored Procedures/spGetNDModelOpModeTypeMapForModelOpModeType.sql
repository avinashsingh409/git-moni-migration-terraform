﻿
CREATE PROCEDURE [Monitoring].[spGetNDModelOpModeTypeMapForModelOpModeType]
  @ModelID int = NULL,
  @OpModeTypeID int = NULL
AS
BEGIN
    SELECT [ModelOpModeTypeMapID],
          [ModelID],
          [OpModeTypeID]
    FROM [Monitoring].[tNDModelOpModeTypeMap]
	WHERE
		(@ModelID IS NULL OR [ModelID] = @ModelID)
		AND (@OpModeTypeID IS NULL OR [OpModeTypeID] = @OpModeTypeID);
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelOpModeTypeMapForModelOpModeType] TO [TEUser]
    AS [dbo];

