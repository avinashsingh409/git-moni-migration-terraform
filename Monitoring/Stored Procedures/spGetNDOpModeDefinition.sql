﻿
CREATE PROCEDURE [Monitoring].[spGetNDOpModeDefinition]
  (@OpModeDefinitionID int = NULL)
AS
BEGIN
  IF @OpModeDefinitionID IS NULL OR @OpModeDefinitionID = 0
    SELECT [OpModeDefinitionID],
          [NDProjectPath],
          [NDProjectName],
          [OpModeDefinitionExtID],
          [OpModeDefinitionTitle],
          [OpModeTypeID],
          [CreatedByUserID],
          [CreateDate],
          [ChangedByUserID],
          [ChangeDate]
    FROM [Monitoring].[tNDOpModeDefinition];
  ELSE
    SELECT [OpModeDefinitionID],
          [NDProjectPath],
          [NDProjectName],
          [OpModeDefinitionExtID],
          [OpModeDefinitionTitle],
          [OpModeTypeID],
          [CreatedByUserID],
          [CreateDate],
          [ChangedByUserID],
          [ChangeDate]
    FROM [Monitoring].[tNDOpModeDefinition] WHERE [OpModeDefinitionID] = @OpModeDefinitionID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeDefinition] TO [TEUser]
    AS [dbo];

