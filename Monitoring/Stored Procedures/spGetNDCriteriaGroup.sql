﻿
CREATE PROCEDURE [Monitoring].[spGetNDCriteriaGroup]
  (@OpModeDefinitionID int = NULL)
AS
BEGIN
  IF @OpModeDefinitionID IS NULL OR @OpModeDefinitionID = 0
    SELECT [CriteriaGroupID],
          [OpModeDefinitionID],
          [IsStart],
          [DisplayOrder]
    FROM [Monitoring].[tNDCriteriaGroup];
  ELSE
    SELECT [CriteriaGroupID],
          [OpModeDefinitionID],
          [IsStart],
          [DisplayOrder]
    FROM [Monitoring].[tNDCriteriaGroup] WHERE [OpModeDefinitionID] = @OpModeDefinitionID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDCriteriaGroup] TO [TEUser]
    AS [dbo];

