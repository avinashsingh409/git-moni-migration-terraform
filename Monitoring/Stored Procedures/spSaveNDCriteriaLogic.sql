﻿




CREATE PROCEDURE [Monitoring].[spSaveNDCriteriaLogic]
  @CriteriaLogicID int = NULL,
   @CriteriaGroupID int,
   @AssetVariableTypeTagMapID int,
   @LogicOperatorTypeID int,
   @Value real = NULL,
   @TransiencyStdDevMultiplier real = NULL,
   @TransiencyDuration int = NULL,
   @TransiencyDurationTemporalTypeID int = NULL,
   @DisplayOrder int = NULL,
   @LogicStartDate datetime = NULL,
   @LogicEndDate datetime = NULL
AS
BEGIN
  IF @CriteriaLogicID IS NULL OR @CriteriaLogicID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDCriteriaLogic]
        ([CriteriaGroupID],
         [AssetVariableTypeTagMapID],
         [LogicOperatorTypeID],
         [Value],
         [TransiencyStdDevMultiplier],
         [TransiencyDuration],
         [TransiencyDurationTemporalTypeID],
         [DisplayOrder],
		 [LogicStartDate],
		 [LogicEndDate])
      VALUES
        (@CriteriaGroupID,
         @AssetVariableTypeTagMapID,
         @LogicOperatorTypeID,
         @Value,
         @TransiencyStdDevMultiplier,
         @TransiencyDuration,
         @TransiencyDurationTemporalTypeID,
         @DisplayOrder,
		 @LogicStartDate,
		 @LogicEndDate);

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDCriteriaLogic]
        SET [CriteriaGroupID] = @CriteriaGroupID,
            [AssetVariableTypeTagMapID] = @AssetVariableTypeTagMapID,
            [LogicOperatorTypeID] = @LogicOperatorTypeID,
            [Value] = @Value,
            [TransiencyStdDevMultiplier] = @TransiencyStdDevMultiplier,
            [TransiencyDuration] = @TransiencyDuration,
            [TransiencyDurationTemporalTypeID] = @TransiencyDurationTemporalTypeID,
            [DisplayOrder] = @DisplayOrder,
			[LogicStartDate] = @LogicStartDate,
			[LogicEndDate] = @LogicEndDate
        WHERE [CriteriaLogicID] = @CriteriaLogicID;

	  SELECT @CriteriaLogicID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDCriteriaLogic] TO [TEUser]
    AS [dbo];

