﻿


CREATE PROCEDURE [Monitoring].[spFillNDModelTemplateRecInputs]
	@OutputAstVarTagMapID int, 
	@ModelTemplateID int
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @ParentID int;
SET @ParentID = NULL;
DECLARE @GrandparentID int;
SET @GrandparentID = NULL;

DECLARE @RecSeries int;

DECLARE @RecInputID int;
DECLARE @TarACTypeID int;
DECLARE @VarTypeID int;
DECLARE @TarTypeID int;
DECLARE @MuTagPrepTypeID int;
DECLARE @OutputValueTypeID int;
SET @OutputValueTypeID = 23;
DECLARE @MeasuredValueTypeID int;
SET @MeasuredValueTypeID = 1

-- we need to know @OutputAstVarTagMapID's asset id
DECLARE @OutputAssetID int;
SELECT @OutputAssetID = (SELECT AssetID
FROM ProcessData.tAssetVariableTypeTagMap
WHERE AssetVariableTypeTagMapID = @OutputAstVarTagMapID);

-- we need to know @OutputAstVarTagMapID's asset class type id
DECLARE @OutputAssetClassTypeID int;
SELECT @OutputAssetclassTypeID = (SELECT AssetClassTypeID FROM Asset.tAsset WHERE AssetID = @OutputAssetID);

-- we need to know @OutputAstVarTagMapID's server id
-- so that only inputs that belong to the same server are returned
DECLARE @OutputPDServerID int;
SELECT @OutputPDServerID = (SELECT tag.PDServerID FROM ProcessData.tAssetVariableTypeTagMap map
JOIN ProcessData.tTag tag on map.TagID = tag.PDTagID
WHERE map.AssetVariableTypeTagMapID = @OutputAstVarTagMapID);

DECLARE @seriesTable table(
	Series int NOT NULL
)
INSERT INTO @seriesTable (Series)
SELECT DISTINCT Series
FROM Monitoring.tNDRecInputVariableMap
WHERE ModelTemplateID = @ModelTemplateID
ORDER BY Series

DECLARE @outputTable table(
	RecInputVariableMapID int NOT NULL,
	AssetVariableTypeTagMapID int NOT NULL
)

DECLARE @SeriesOK int;
DECLARE @OK int;

DECLARE curSeries INSENSITIVE CURSOR
FOR
	SELECT Series FROM @seriesTable

OPEN curSeries
FETCH NEXT FROM curSeries INTO @RecSeries

SET @SeriesOK = 1
WHILE @SeriesOK = 1
	IF @@FETCH_STATUS = 0
		BEGIN

			DECLARE curRec INSENSITIVE CURSOR
			FOR
				SELECT
					rec.RecInputVariableMapID
					, map.AssetClassTypeID
					, map.VariableTypeID
					, rec.RecInputVariableTargetTypeID
					, rec.MultipleTagPreprocessingTypeID
				FROM Monitoring.tNDRecInputVariableMap rec
				JOIN ProcessData.tAssetClassTypeVariableTypeMap map
				ON rec.AssetClassTypeVariableTypeMapID = map.AssetClassTypeVariableTypeMapID
				LEFT JOIN Monitoring.tNDRecInputVariableTargetType tartype
				ON rec.RecInputVariableTargetTypeID = tartype.RecInputVariableTargetTypeID
				LEFT JOIN Monitoring.tNDMultipleTagPreprocessingType multype
				ON rec.MultipleTagPreprocessingTypeID = multype.MultipleTagPreprocessingTypeID
				WHERE
					rec.ModelTemplateID = @ModelTemplateID
					AND rec.Series = @RecSeries
				ORDER BY rec.Priority

			OPEN curRec;
			FETCH NEXT FROM curRec INTO @RecInputID, @TarACTypeID,@VarTypeID,@TarTypeID,@MuTagPrepTypeID

			SET @OK = 1
			WHILE @OK = 1
				IF @@FETCH_STATUS = 0
					BEGIN
						SET @ParentID = NULL;
						SET @GrandparentID = NULL;
						
						-- look for the one tag of Value Type "Output" first
						-- if more than one map matches take most recent ChangeDate
						-- if no Output tags then look for "Measured"
						-- do not need to check count because there should be a multi-tag preprocessing action
						-- in the model template
						
						IF @TarTypeID IS NULL AND @TarACTypeID = @OutputAssetClassTypeID
							BEGIN
								-- any independent variables will be found in @OutputAstVarTagMapID's asset id
								IF EXISTS(
									SELECT map.AssetVariableTypeTagMapID
									FROM ProcessData.tAssetVariableTypeTagMap map
									JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
									WHERE
										map.AssetID = @OutputAssetID
										AND map.VariableTypeID = @VarTypeID
										AND map.ValueTypeID = @OutputValueTypeID
										AND tag.ExistsOnServer = 1
										AND map.TagID IS NOT NULL
										AND tag.PDServerID = @OutputPDServerID
								)
									BEGIN
										INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
										SELECT @RecInputID, matches.AssetVariableTypeTagMapID
										FROM (
											SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
												, map.AssetVariableTypeTagMapID
											FROM ProcessData.tAssetVariableTypeTagMap map
											JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
											WHERE
												map.AssetID = @OutputAssetID
												AND map.VariableTypeID = @VarTypeID
												AND map.ValueTypeID = @OutputValueTypeID
												AND tag.ExistsOnServer = 1
												AND map.TagID IS NOT NULL
												AND tag.PDServerID = @OutputPDServerID
										) matches
										WHERE matches.RowNumber = 1
										
										GOTO Next_Series
									END
								ELSE
									BEGIN
										IF EXISTS(
											SELECT map.AssetVariableTypeTagMapID
											FROM ProcessData.tAssetVariableTypeTagMap map
											JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
											WHERE
												map.AssetID = @OutputAssetID
												AND map.VariableTypeID = @VarTypeID
												AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
												AND tag.ExistsOnServer = 1
												AND map.TagID IS NOT NULL
												AND tag.PDServerID = @OutputPDServerID
										)
											BEGIN
												INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
												SELECT @RecInputID, map.AssetVariableTypeTagMapID
												FROM ProcessData.tAssetVariableTypeTagMap map
												JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
												WHERE
													map.AssetID = @OutputAssetID
													AND map.VariableTypeID = @VarTypeID
													AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
													AND tag.ExistsOnServer = 1
													AND map.TagID IS NOT NULL
													AND tag.PDServerID = @OutputPDServerID
												
												GOTO Next_Series
											
											END
									END
							END
						ELSE
							BEGIN
							-- check @TarTypeID to see where to begin the search
							IF @TarTypeID IS NOT NULL
								BEGIN
								IF @TarTypeID = 1
									BEGIN
										-- start from Parent and take only Siblings
										SELECT @ParentID = (SELECT ParentAssetID FROM Asset.tAsset WHERE AssetID = @OutputAssetID);
										IF @ParentID IS NOT NULL
											BEGIN
												IF EXISTS
												(
													SELECT map.AssetVariableTypeTagMapID
													FROM Asset.tAsset ast
													JOIN ProcessData.tAssetVariableTypeTagMap map ON ast.AssetID = map.AssetID
													JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
													WHERE
														ast.ParentAssetID = @ParentID
														AND ast.AssetClassTypeID = @TarACTypeID
														AND map.VariableTypeID = @VarTypeID
														AND map.ValueTypeID = @OutputValueTypeID
														AND ast.AssetID <> @OutputAssetID
														AND tag.ExistsOnServer = 1
														AND map.TagID IS NOT NULL
														AND tag.PDServerID = @OutputPDServerID
												)
													BEGIN
														INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
														SELECT @RecInputID, matches.AssetVariableTypeTagMapID
														FROM (
															SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
																, map.AssetVariableTypeTagMapID
															FROM Asset.tAsset ast
															JOIN ProcessData.tAssetVariableTypeTagMap map ON ast.AssetID = map.AssetID
															JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
															WHERE
																ast.ParentAssetID = @ParentID
																AND ast.AssetClassTypeID = @TarACTypeID
																AND map.VariableTypeID = @VarTypeID
																AND map.ValueTypeID = @OutputValueTypeID
																AND ast.AssetID <> @OutputAssetID
																AND tag.ExistsOnServer = 1
																AND map.TagID IS NOT NULL
																AND tag.PDServerID = @OutputPDServerID
														) matches
														WHERE matches.RowNumber = 1

														GOTO Next_Series
													END
												ELSE
													BEGIN
														IF EXISTS(
															SELECT map.AssetVariableTypeTagMapID
															FROM Asset.tAsset ast
															JOIN ProcessData.tAssetVariableTypeTagMap map ON ast.AssetID = map.AssetID
															JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
															WHERE
																ast.ParentAssetID = @ParentID
																AND ast.AssetClassTypeID = @TarACTypeID
																AND map.VariableTypeID = @VarTypeID
																AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
																AND ast.AssetID <> @OutputAssetID
																AND tag.ExistsOnServer = 1
																AND map.TagID IS NOT NULL
																AND tag.PDServerID = @OutputPDServerID
														)
															BEGIN
																INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
																SELECT @RecInputID, map.AssetVariableTypeTagMapID
																FROM Asset.tAsset ast
																JOIN ProcessData.tAssetVariableTypeTagMap map ON ast.AssetID = map.AssetID
																JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
																WHERE
																	ast.ParentAssetID = @ParentID
																	AND ast.AssetClassTypeID = @TarACTypeID
																	AND map.VariableTypeID = @VarTypeID
																	AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
																	AND ast.AssetID <> @OutputAssetID
																	AND tag.ExistsOnServer = 1
																	AND map.TagID IS NOT NULL
																	AND tag.PDServerID = @OutputPDServerID
															
																GOTO Next_Series
															END
													END
											END
									END
								ELSE IF @TarTypeID = 2 -- Children
									BEGIN
										-- start from @OutputAstVarTagMapID's asset and take Descendants
										IF EXISTS
										(
											SELECT map.AssetVariableTypeTagMapID
											FROM Asset.ufnGetAssetTreeBranch(@OutputAssetID) br
											JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
											JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
											JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
											WHERE
												ast.AssetClassTypeID = @TarACTypeID
												AND map.VariableTypeID = @VarTypeID
												AND map.ValueTypeID = @OutputValueTypeID
												AND ast.AssetID <> @OutputAssetID
												AND tag.ExistsOnServer = 1
												AND map.TagID IS NOT NULL
												AND tag.PDServerID = @OutputPDServerID
										)
											BEGIN
												INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
												SELECT @RecInputID, matches.AssetVariableTypeTagMapID
												FROM (
													SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
														, map.AssetVariableTypeTagMapID
													FROM Asset.ufnGetAssetTreeBranch(@OutputAssetID) br
													JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
													JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
													JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
													WHERE
														ast.AssetClassTypeID = @TarACTypeID
														AND map.VariableTypeID = @VarTypeID
														AND map.ValueTypeID = @OutputValueTypeID
														AND ast.AssetID <> @OutputAssetID
														AND tag.ExistsOnServer = 1
														AND map.TagID IS NOT NULL
														AND tag.PDServerID = @OutputPDServerID
												) matches
												WHERE matches.RowNumber = 1
													
												GOTO Next_Series
											END
										ELSE
											BEGIN
												IF EXISTS(
													SELECT map.AssetVariableTypeTagMapID
													FROM Asset.ufnGetAssetTreeBranch(@OutputAssetID) br
													JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
													JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
													JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
													WHERE
														ast.AssetClassTypeID = @TarACTypeID
														AND map.VariableTypeID = @VarTypeID
														AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
														AND ast.AssetID <> @OutputAssetID
														AND tag.ExistsOnServer = 1
														AND map.TagID IS NOT NULL
														AND tag.PDServerID = @OutputPDServerID
												)
													BEGIN
														INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
														SELECT @RecInputID, map.AssetVariableTypeTagMapID
														FROM Asset.ufnGetAssetTreeBranch(@OutputAssetID) br
														JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
														JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
														JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
														WHERE
															ast.AssetClassTypeID = @TarACTypeID
															AND map.VariableTypeID = @VarTypeID
															AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
															AND ast.AssetID <> @OutputAssetID
															AND tag.ExistsOnServer = 1
															AND map.TagID IS NOT NULL
															AND tag.PDServerID = @OutputPDServerID

														GOTO Next_Series
													END
											END
									END
								ELSE IF @TarTypeID = 3
									BEGIN
										-- start from Parent and take Parent and any Descendants
										SELECT @ParentID = (SELECT ParentAssetID FROM Asset.tAsset WHERE AssetID = @OutputAssetID);
										IF @ParentID IS NOT NULL
											BEGIN
												IF EXISTS
												(
													SELECT map.AssetVariableTypeTagMapID
													FROM Asset.ufnGetAssetTreeBranch(@ParentID) br
													JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
													JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
													JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
													WHERE
														ast.AssetClassTypeID = @TarACTypeID
														AND map.VariableTypeID = @VarTypeID
														AND map.ValueTypeID = @OutputValueTypeID
														AND tag.ExistsOnServer = 1
														AND map.TagID IS NOT NULL
														AND tag.PDServerID = @OutputPDServerID
												)
													BEGIN
														INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
														SELECT @RecInputID, matches.AssetVariableTypeTagMapID
														FROM (
															SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
																, map.AssetVariableTypeTagMapID
															FROM Asset.ufnGetAssetTreeBranch(@ParentID) br
															JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
															JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
															JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
															WHERE
																ast.AssetClassTypeID = @TarACTypeID
																AND map.VariableTypeID = @VarTypeID
																AND map.ValueTypeID = @OutputValueTypeID
																AND tag.ExistsOnServer = 1
																AND map.TagID IS NOT NULL
																AND tag.PDServerID = @OutputPDServerID
														) matches
														WHERE matches.RowNumber = 1
															
														GOTO Next_Series
													END
												ELSE
													BEGIN
														IF EXISTS(
															SELECT map.AssetVariableTypeTagMapID
															FROM Asset.ufnGetAssetTreeBranch(@ParentID) br
															JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
															JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
															JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
															WHERE
																ast.AssetClassTypeID = @TarACTypeID
																AND map.VariableTypeID = @VarTypeID
																AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
																AND tag.ExistsOnServer = 1
																AND map.TagID IS NOT NULL
																AND tag.PDServerID = @OutputPDServerID
														)
															BEGIN
																INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
																SELECT @RecInputID, map.AssetVariableTypeTagMapID
																FROM Asset.ufnGetAssetTreeBranch(@ParentID) br
																JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
																JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
																JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
																WHERE
																	ast.AssetClassTypeID = @TarACTypeID
																	AND map.VariableTypeID = @VarTypeID
																	AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
																	AND tag.ExistsOnServer = 1
																	AND map.TagID IS NOT NULL
																	AND tag.PDServerID = @OutputPDServerID
																
																GOTO Next_Series
															END
													END
											END
										END
								ELSE IF @TarTypeID = 4
									BEGIN
										-- start from Grandparent and take Grandparent and any Descendants
										SELECT @GrandparentID = (
											SELECT par.ParentAssetID
											FROM Asset.tAsset ast
											JOIN Asset.tAsset par
											ON ast.ParentAssetID = par.AssetID
											WHERE ast.AssetID = @OutputAssetID
										);
										IF @GrandparentID IS NOT NULL
										BEGIN
											IF EXISTS
											(
												SELECT map.AssetVariableTypeTagMapID
												FROM Asset.ufnGetAssetTreeBranch(@GrandparentID) br
												JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
												JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
												JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
												WHERE
													ast.AssetClassTypeID = @TarACTypeID
													AND map.VariableTypeID = @VarTypeID
													AND map.ValueTypeID = @OutputValueTypeID
													AND tag.ExistsOnServer = 1
													AND map.TagID IS NOT NULL
													AND tag.PDServerID = @OutputPDServerID
											)
												BEGIN
													INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
													SELECT @RecInputID, matches.AssetVariableTypeTagMapID
													FROM (
														SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
															, map.AssetVariableTypeTagMapID
														FROM Asset.ufnGetAssetTreeBranch(@GrandparentID) br
														JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
														JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
														JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
														WHERE
															ast.AssetClassTypeID = @TarACTypeID
															AND map.VariableTypeID = @VarTypeID
															AND map.ValueTypeID = @OutputValueTypeID
															AND tag.ExistsOnServer = 1
															AND map.TagID IS NOT NULL
															AND tag.PDServerID = @OutputPDServerID
													) matches
													WHERE matches.RowNumber = 1
													
													GOTO Next_Series
												END
											ELSE
												BEGIN
													IF EXISTS(
														SELECT map.AssetVariableTypeTagMapID
														FROM Asset.ufnGetAssetTreeBranch(@GrandparentID) br
														JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
														JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
														JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
														WHERE
															ast.AssetClassTypeID = @TarACTypeID
															AND map.VariableTypeID = @VarTypeID
															AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
															AND tag.ExistsOnServer = 1
															AND map.TagID IS NOT NULL
															AND tag.PDServerID = @OutputPDServerID
													)
														BEGIN
															INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
															SELECT @RecInputID, map.AssetVariableTypeTagMapID
															FROM Asset.ufnGetAssetTreeBranch(@GrandparentID) br
															JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
															JOIN ProcessData.tAssetVariableTypeTagMap map ON br.AssetID = map.AssetID
															JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
															WHERE
																ast.AssetClassTypeID = @TarACTypeID
																AND map.VariableTypeID = @VarTypeID
																AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
																AND tag.ExistsOnServer = 1
																AND map.TagID IS NOT NULL
																AND tag.PDServerID = @OutputPDServerID
															
															GOTO Next_Series
														END
												END
										END
									END
								ELSE IF @TarTypeID = 5 -- Unit
									BEGIN
										DECLARE @UnitID int;
										SELECT @UnitID = NULL;
										SELECT @UnitID = (Asset.ufnGetParentUnitAssetIDOfAssetID(@OutputAssetID));
										
										IF @UnitID IS NOT NULL
										BEGIN											
											-- start from Unit and take Unit and any Descendants
											IF EXISTS
											(
												SELECT map.AssetVariableTypeTagMapID
												FROM ProcessData.tAssetVariableTypeTagMap map
												JOIN Asset.ufnGetAssetTreeBranch(@UnitID) br ON map.AssetID = br.AssetID
												JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
												JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
												WHERE
													ast.AssetClassTypeID = @TarACTypeID
													AND map.VariableTypeID = @VarTypeID
													AND map.ValueTypeID = @OutputValueTypeID
													AND tag.ExistsOnServer = 1
													AND map.TagID IS NOT NULL
													AND tag.PDServerID = @OutputPDServerID
											)
												BEGIN
													INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
													SELECT @RecInputID, matches.AssetVariableTypeTagMapID
													FROM (
														SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
															, map.AssetVariableTypeTagMapID
														FROM ProcessData.tAssetVariableTypeTagMap map
														JOIN Asset.ufnGetAssetTreeBranch(@UnitID) br ON map.AssetID = br.AssetID
														JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
														JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
														WHERE
															ast.AssetClassTypeID = @TarACTypeID
															AND map.VariableTypeID = @VarTypeID
															AND map.ValueTypeID = @OutputValueTypeID
															AND tag.ExistsOnServer = 1
															AND map.TagID IS NOT NULL
															AND tag.PDServerID = @OutputPDServerID
													) matches
													WHERE matches.RowNumber = 1
													
													GOTO Next_Series
												END
											ELSE
												BEGIN
													IF EXISTS(
														SELECT map.AssetVariableTypeTagMapID
														FROM ProcessData.tAssetVariableTypeTagMap map
														JOIN Asset.ufnGetAssetTreeBranch(@UnitID) br ON map.AssetID = br.AssetID
														JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
														JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
														WHERE
															ast.AssetClassTypeID = @TarACTypeID
															AND map.VariableTypeID = @VarTypeID
															AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
															AND tag.ExistsOnServer = 1
															AND map.TagID IS NOT NULL
															AND tag.PDServerID = @OutputPDServerID
													)
														BEGIN
															INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
															SELECT @RecInputID, map.AssetVariableTypeTagMapID
															FROM ProcessData.tAssetVariableTypeTagMap map
															JOIN Asset.ufnGetAssetTreeBranch(@UnitID) br ON map.AssetID = br.AssetID
															JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
															JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
															WHERE
																ast.AssetClassTypeID = @TarACTypeID
																AND map.VariableTypeID = @VarTypeID
																AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
																AND tag.ExistsOnServer = 1
																AND map.TagID IS NOT NULL
																AND tag.PDServerID = @OutputPDServerID
															
															GOTO Next_Series
														END
												END
										END
									END
								END
							ELSE
								-- @TarTypeID is null but @TarACTypeID didn't match @OutputAssetClassTypeID
								-- so search the Unit and all of its descendants
								BEGIN
									DECLARE @implicitUnitID int;
									SELECT @implicitUnitID = NULL;
									SELECT @implicitUnitID = (Asset.ufnGetParentUnitAssetIDOfAssetID(@OutputAssetID));
										
									IF @implicitUnitID IS NOT NULL
									BEGIN											
										-- start from Unit and take Unit and any Descendants
										IF EXISTS
										(
											SELECT map.AssetVariableTypeTagMapID
											FROM ProcessData.tAssetVariableTypeTagMap map
											JOIN Asset.ufnGetAssetTreeBranch(@implicitUnitID) br ON map.AssetID = br.AssetID
											JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
											JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
											WHERE
												ast.AssetClassTypeID = @TarACTypeID
												AND map.VariableTypeID = @VarTypeID
												AND map.ValueTypeID = @OutputValueTypeID
												AND tag.ExistsOnServer = 1
												AND map.TagID IS NOT NULL
												AND tag.PDServerID = @OutputPDServerID
										)
											BEGIN
												INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
												SELECT @RecInputID, matches.AssetVariableTypeTagMapID
												FROM (
													SELECT ROW_NUMBER() OVER(PARTITION BY map.AssetID, map.TagID ORDER BY map.ChangeDate DESC) AS 'RowNumber'
														, map.AssetVariableTypeTagMapID
													FROM ProcessData.tAssetVariableTypeTagMap map
													JOIN Asset.ufnGetAssetTreeBranch(@implicitUnitID) br ON map.AssetID = br.AssetID
													JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
													JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
													WHERE
														ast.AssetClassTypeID = @TarACTypeID
														AND map.VariableTypeID = @VarTypeID
														AND map.ValueTypeID = @OutputValueTypeID
														AND tag.ExistsOnServer = 1
														AND map.TagID IS NOT NULL
														AND tag.PDServerID = @OutputPDServerID
												) matches
												WHERE matches.RowNumber = 1
													
												GOTO Next_Series
											END
										ELSE
											BEGIN
												IF EXISTS(
													SELECT map.AssetVariableTypeTagMapID
													FROM ProcessData.tAssetVariableTypeTagMap map
													JOIN Asset.ufnGetAssetTreeBranch(@implicitUnitID) br ON map.AssetID = br.AssetID
													JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
													JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
													WHERE
														ast.AssetClassTypeID = @TarACTypeID
														AND map.VariableTypeID = @VarTypeID
														AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
														AND tag.ExistsOnServer = 1
														AND map.TagID IS NOT NULL
														AND tag.PDServerID = @OutputPDServerID
												)
													BEGIN
														INSERT INTO @outputTable (RecInputVariableMapID, AssetVariableTypeTagMapID)
														SELECT @RecInputID, map.AssetVariableTypeTagMapID
														FROM ProcessData.tAssetVariableTypeTagMap map
														JOIN Asset.ufnGetAssetTreeBranch(@implicitUnitID) br ON map.AssetID = br.AssetID
														JOIN Asset.tAsset ast ON br.AssetID = ast.AssetID
														JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
														WHERE
															ast.AssetClassTypeID = @TarACTypeID
															AND map.VariableTypeID = @VarTypeID
															AND (map.ValueTypeID = @MeasuredValueTypeID OR map.ValueTypeID IS NULL)
															AND tag.ExistsOnServer = 1
															AND map.TagID IS NOT NULL
															AND tag.PDServerID = @OutputPDServerID
															
														GOTO Next_Series
													END
											END
									END
								END
							END
			
					FETCH NEXT FROM curRec INTO @RecInputID, @TarACTypeID,@VarTypeID,@TarTypeID,@MuTagPrepTypeID
					END
				ELSE
					BEGIN
					SET @OK = 0
					END
					
			Next_Series:
			

			CLOSE curRec;
			DEALLOCATE curRec;

		FETCH NEXT FROM curSeries INTO @RecSeries
		END
	ELSE
		BEGIN
		SET @SeriesOK = 0
		END


CLOSE curSeries;
DEALLOCATE curSeries;

WITH cte AS (
SELECT
	inputs.RecInputVariableMapID
	, map.Series
	, map.Priority
	, map.MultipleTagPreprocessingTypeID
	, inputs.AssetVariableTypeTagMapID
	, tag.PDTagID
	, tag.TagName
	, tag.TagDesc
	, tag.ExternalID
	, tag.GlobalID
	, tag.NDCreated
	, tag.NDPath
	, tag.NDName
	, tag.NDId
	, ROW_NUMBER() OVER(PARTITION BY tag.PDTagID ORDER BY map.Series, map.Priority) AS row_number
FROM @outputTable inputs
JOIN Monitoring.tNDRecInputVariableMap map
ON inputs.RecInputVariableMapID = map.RecInputVariableMapID
JOIN ProcessData.tAssetVariableTypeTagMap tmap ON inputs.AssetVariableTypeTagMapID = tmap.AssetVariableTypeTagMapID
JOIN ProcessData.tTag tag ON tmap.TagID = tag.PDTagID)
SELECT 	RecInputVariableMapID
	, Series
	, Priority
	, MultipleTagPreprocessingTypeID
	, AssetVariableTypeTagMapID
	, PDTagID
	, TagName
	, TagDesc
	, ExternalID
	, GlobalID
	, NDCreated
	, NDPath
	, NDName
	, NDId 
FROM cte WHERE row_number = 1 ORDER BY Series, Priority;

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spFillNDModelTemplateRecInputs] TO [TEUser]
    AS [dbo];

