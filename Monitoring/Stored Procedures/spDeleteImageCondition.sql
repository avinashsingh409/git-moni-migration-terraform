﻿CREATE PROCEDURE [Monitoring].[spDeleteImageCondition]
	@ImageConditionID INT
    ,@SecurityUserID int
AS
BEGIN
	DECLARE @user int = null;
	SELECT TOP 1 @user = SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID; 
	IF @user IS NULL
	BEGIN
		RAISERROR('User does not exist',16,1)
	END

	DECLARE @AssetID INT
	SELECT @AssetID = AssetID FROM [Monitoring].[tImageConditionResult] ICR
		INNER JOIN Monitoring.tImageCondition IC ON ICR.ImageConditionResultID = IC.ImageConditionResultID
		WHERE @ImageConditionID = IC.ImageConditionID


	IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User cannot access asset',16, 1);
	END
	
	DELETE FROM Monitoring.tImageCondition WHERE ImageConditionID = @ImageConditionID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteImageCondition] TO [TEUser]
    AS [dbo];