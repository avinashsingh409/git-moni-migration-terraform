﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDModelBuild]
	@ModelBuildID int
AS
BEGIN
	DECLARE @Result int = 0;
	
	DELETE FROM Monitoring.tNDModelBuild WHERE ModelBuildID = @ModelBuildID
	SET @Result = @@ROWCOUNT;
	
	SELECT @Result;
	
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDModelBuild] TO [TEUser]
    AS [dbo];

