﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDCriteriaLogic]
  (@CriteriaLogicID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDCriteriaLogic] WHERE [CriteriaLogicID]=@CriteriaLogicID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDCriteriaLogic] TO [TEUser]
    AS [dbo];

