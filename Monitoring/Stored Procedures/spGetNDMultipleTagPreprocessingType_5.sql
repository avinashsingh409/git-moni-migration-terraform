﻿
CREATE PROCEDURE [Monitoring].[spGetNDMultipleTagPreprocessingType]
  (@multipleTagPreprocessingTypeID int = NULL)
AS
BEGIN
  IF @multipleTagPreprocessingTypeID IS NULL OR @multipleTagPreprocessingTypeID = 0
    SELECT [MultipleTagPreprocessingTypeID],
          [MultipleTagPreprocessingTypeAbbrev],
          [MultipleTagPreprocessingTypeDesc],
          [DisplayOrder]
    FROM [Monitoring].[tNDMultipleTagPreprocessingType];
  ELSE
    SELECT [MultipleTagPreprocessingTypeID],
          [MultipleTagPreprocessingTypeAbbrev],
          [MultipleTagPreprocessingTypeDesc],
          [DisplayOrder]
    FROM [Monitoring].[tNDMultipleTagPreprocessingType] WHERE [MultipleTagPreprocessingTypeID] = @multipleTagPreprocessingTypeID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDMultipleTagPreprocessingType] TO [TEUser]
    AS [dbo];

