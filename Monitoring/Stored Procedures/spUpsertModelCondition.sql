﻿CREATE PROCEDURE [Monitoring].[spUpsertModelCondition]
	@ModelConditionID int = null,
	@ModelID int,
	@ConditionTimeBasis DateTime = null,
	@ConditionOutOfBounds_percent float = null,
	@ConditionLastActive DateTime = null,
	@IsKeepConditionActive BIT,
	@KeepConditionActiveTimestamp DateTime = null,
	@Timestamp DateTime = null,
	@ActualOutOfBounds_percent float = null,
	@ConditionLastClear DateTime = null,
	@StateChanged BIT,
	@IgnoreUntil DateTime = null,
	@OutDirection int = null,
	@RSquared float = null,
	@BeganAlertingTimestamp DateTime = null,
	@LastIgnoreActualValue float = null,
	@LastIgnoreExpectedValue float = null,
	@LastIgnoreUpperLimit float = null,
	@LastIgnoreLowerLimit float = null,
	@IgnoreOverridden BIT,
	@NewModelConditionID int OUTPUT
AS
BEGIN
	IF(@ModelConditionID IS NULL OR @ModelConditionID <= 0)
	BEGIN
		INSERT INTO Monitoring.tModelCondition(
			ModelID
			, ConditionTimeBasis
			, ConditionOutOfBounds_percent
			, ConditionLastActive
			, IsKeepConditionActive
			, KeepConditionActiveTimestamp
			, Timestamp
			, ActualOutOfBounds_percent
			, ConditionLastClear
			, StateChanged
			, IgnoreUntil
			, OutDirection
			, RSquared
			, BeganAlertingTimestamp
			, LastIgnoreActualValue
			, LastIgnoreExpectedValue
			, LastIgnoreUpperLimit
			, LastIgnoreLowerLimit
			, IgnoreOverridden
		)
		VALUES (
			@ModelID
			, @ConditionTimeBasis
			, @ConditionOutOfBounds_percent
			, @ConditionLastActive
			, @IsKeepConditionActive
			, @KeepConditionActiveTimestamp
			, @Timestamp
			, @ActualOutOfBounds_percent
			, @ConditionLastClear
			, @StateChanged
			, @IgnoreUntil
			, @OutDirection
			, @RSquared
			, @BeganAlertingTimestamp
			, @LastIgnoreActualValue
			, @LastIgnoreExpectedValue
			, @LastIgnoreUpperLimit
			, @LastIgnoreLowerLimit
			, @IgnoreOverridden
		)
		SET @NewModelConditionID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE Monitoring.tModelCondition
		SET
			ModelID = @ModelID
			, ConditionTimeBasis = @ConditionTimeBasis
			, ConditionOutOfBounds_percent = @ConditionOutOfBounds_percent
			, ConditionLastActive = @ConditionLastActive
			, IsKeepConditionActive = @IsKeepConditionActive
			, KeepConditionActiveTimestamp = @KeepConditionActiveTimestamp
			, [Timestamp] = @Timestamp
			, ActualOutOfBounds_percent = @ActualOutOfBounds_percent
			, ConditionLastClear = @ConditionLastClear
			, StateChanged = @StateChanged
			, IgnoreUntil = @IgnoreUntil
			, OutDirection = @OutDirection
			, RSquared = @RSquared
			, BeganAlertingTimeStamp = @BeganAlertingTimestamp
			, LastIgnoreActualValue = @LastIgnoreActualValue
			, LastIgnoreExpectedValue = @LastIgnoreExpectedValue
			, LastIgnoreUpperLimit = @LastIgnoreUpperLimit
			, LastIgnoreLowerLimit = @LastIgnoreLowerLimit
			, IgnoreOverridden = @IgnoreOverridden
		WHERE ModelConditionID = @ModelConditionID
		SET @NewModelConditionID = @ModelConditionID
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpsertModelCondition] TO [TEUser]
    AS [dbo];

