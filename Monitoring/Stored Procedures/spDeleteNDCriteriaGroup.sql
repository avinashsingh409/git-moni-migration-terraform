﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDCriteriaGroup]
  (@CriteriaGroupID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDCriteriaGroup] WHERE [CriteriaGroupID]=@CriteriaGroupID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDCriteriaGroup] TO [TEUser]
    AS [dbo];

