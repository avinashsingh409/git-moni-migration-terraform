﻿
CREATE PROCEDURE [Monitoring].[spSaveNDModelPredictiveMethodMap]
  @modelPredictiveMethodMapID int = NULL,
   @modelID int,
   @predictiveMethodTypeID int,
   @score int = NULL,
   @rvalue float = NULL,
   @rmse float = NULL,
   @mae float = NULL,
   @active bit,
   @commonTracesID nvarchar(400) = NULL,
   @commonTraceFastID nvarchar(400) = NULL,
   @iqr float = NULL
AS
BEGIN
  IF @modelPredictiveMethodMapID IS NULL OR @modelPredictiveMethodMapID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDModelPredictiveMethodMap]
        ([ModelID],
         [PredictiveMethodTypeID],
         [Score],
         [RValue],
         [RMSE],
         [MAE],
         [Active],
		 [CommonTracesID],
		 [CommonTracesFastID],
         [IQR])
      VALUES
        (@modelID,
         @predictiveMethodTypeID,
         @score,
		 @rvalue,
		 @rmse,
		 @mae,
         @Active,
		 @commonTracesID,
		 @commonTraceFastID,
		 @iqr);

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDModelPredictiveMethodMap]
        SET [ModelID] = @modelID,
            [PredictiveMethodTypeID] = @predictiveMethodTypeID,
            [Score] = @score,
			[RValue] = @rvalue,
			[RMSE] = @rmse,
			[MAE] = @mae,
            [Active] = @active,
			[CommonTracesID] = @commonTracesID,
		    [CommonTracesFastID] = @commonTraceFastID,
			[IQR] = @iqr
        WHERE [ModelPredictiveMethodMapID] = @modelPredictiveMethodMapID;

	  SELECT @modelPredictiveMethodMapID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelPredictiveMethodMap] TO [TEUser]
    AS [dbo];

