﻿CREATE PROCEDURE [Monitoring].[spSaveNDModelInputTagMap]
  @modelInputTagMapID int = NULL,
   @modelID int,
   @assetVariableTypeTagMapID int,
   @recInputVariableMapID int = NULL,
   @active bit,
   @required bit = 0
AS
BEGIN
  IF @modelInputTagMapID IS NULL OR @modelInputTagMapID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDModelInputTagMap]
        ([ModelID],
         [AssetVariableTypeTagMapID],
         [RecInputVariableMapID],
         [Active],
         [Required])
      VALUES
        (@modelID,
         @assetVariableTypeTagMapID,
         @recInputVariableMapID,
         @active,
         @required);

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDModelInputTagMap]
        SET [ModelID] = @modelID,
            [AssetVariableTypeTagMapID] = @assetVariableTypeTagMapID,
            [RecInputVariableMapID] = @recInputVariableMapID,
            [Active] = @active,
            [Required] = @required
        WHERE [ModelInputTagMapID] = @modelInputTagMapID;

	  SELECT @modelInputTagMapID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelInputTagMap] TO [TEUser]
    AS [dbo];

