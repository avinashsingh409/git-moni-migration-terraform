CREATE PROCEDURE [Monitoring].[spSaveNDModelCategoryMap]
  @NDModelCategoryMapID int = NULL,
   @ModelID int,
   @CategoryID int
   
AS
BEGIN
  Declare @Result  table (CategoryMapID int)
  IF @NDModelCategoryMapID IS NULL OR @NDModelCategoryMapID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDModelCategoryMap]
        ([ModelID],
         [CategoryID])
      OUTPUT INSERTED.CategoryMapID INTO @Result 
	  VALUES
        (@ModelID,
         @CategoryID)
  SELECT CategoryMapID from @Result
	 
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDModelCategoryMap]
        SET [ModelID] = @ModelID,
            [CategoryID] = @CategoryID
        WHERE [CategoryMapID] = @NDModelCategoryMapID;

	  SELECT  @NDModelCategoryMapID
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelCategoryMap] TO [TEUser]
    AS [dbo];


