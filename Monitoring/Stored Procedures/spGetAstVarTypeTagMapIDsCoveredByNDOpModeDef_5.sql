﻿CREATE PROCEDURE [Monitoring].[spGetAstVarTypeTagMapIDsCoveredByNDOpModeDef]
	@securityUserID int,
	@opModeTypeID int = null,
	@opModeDefinitionID int = null,
	@astVarTypeTagMapID int = null,
	@returnMeshOrExcludes int = null
AS
BEGIN


	SET NOCOUNT ON;

	DECLARE @opModeDefID int;
	DECLARE @assetID int;
	DECLARE @tagMapID int;
	DECLARE @include bit;
	DECLARE @bringDescendants bit
	DECLARE @OK int;
	
	DECLARE @includes TABLE(
		OpModeDefinitionID int,
		AssetVariableTypeTagMapID int		
	)

	DECLARE @excludes TABLE(
		OpModeDefinitionID int,
		AssetVariableTypeTagMapID int		
	)

	DECLARE curCovereds INSENSITIVE CURSOR
	FOR
		SELECT
			opmodedef.OpModeDefinitionID
			, map.AssetID
			, map.AssetVariableTypeTagMapID
			, map.Include
			, map.BringDescendants
		FROM Monitoring.tNDopModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		RIGHT JOIN Asset.tAsset secAset on map.AssetID = secAset.assetid
		WHERE
			(@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)

	OPEN curCovereds
	FETCH NEXT FROM curCovereds INTO @opModeDefID, @assetID, @tagMapID, @include, @bringDescendants

	SET @OK = 1
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN				
				IF @include = 1
					BEGIN
						IF @assetID IS NOT NULL
							BEGIN
								IF @bringDescendants = 1
									BEGIN
										INSERT INTO @includes (OpModeDefinitionID, AssetVariableTypeTagMapID)
										SELECT @opModeDefID, map.AssetVariableTypeTagMapID
										FROM ProcessData.tAssetVariableTypeTagMap map
										--JOIN @assetIds secAset on map.AssetID = secAset.assetid
										JOIN Asset.ufnGetAssetTreeBranch(@assetID) tree ON map.AssetID = tree.AssetID
										JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
										WHERE
											tag.ExistsOnServer = 1
											AND map.VariableTypeID IS NOT NULL
											AND map.TagID IS NOT NULL
											AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID,@securityUserId,-1)=1
									END
								ELSE
									BEGIN
										INSERT INTO @includes (OpModeDefinitionID, AssetVariableTypeTagMapID)
										SELECT @opModeDefID, map.AssetVariableTypeTagMapID
										FROM ProcessData.tAssetVariableTypeTagMap map
										JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
										WHERE
											tag.ExistsOnServer = 1
											AND map.VariableTypeID IS NOT NULL
											AND map.TagID IS NOT NULL
											AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID,@securityUserId,-1)=1
									END
							END
							
						IF @tagMapID IS NOT NULL AND Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@securityUserId,-1)=1
							BEGIN
							    
								INSERT INTO @includes (OpModeDefinitionID, AssetVariableTypeTagMapID) VALUES (@opModeDefID, @tagMapID)
							END
					END
				ELSE
					BEGIN
						IF @assetID IS NOT NULL
							BEGIN
								IF @bringDescendants = 1
									BEGIN
										INSERT INTO @excludes (OpModeDefinitionID, AssetVariableTypeTagMapID)
										SELECT @opModeDefID, map.AssetVariableTypeTagMapID
										FROM ProcessData.tAssetVariableTypeTagMap map
										--JOIN @assetIds secAset on map.AssetID = secAset.assetid
										JOIN Asset.ufnGetAssetTreeBranch(@assetID) tree ON map.AssetID = tree.AssetID
										JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
										WHERE
											tag.ExistsOnServer = 1
											AND map.VariableTypeID IS NOT NULL
											AND map.TagID IS NOT NULL
											AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID,@securityUserId,-1)=1
									END
								ELSE
									BEGIN
										INSERT INTO @excludes (OpModeDefinitionID, AssetVariableTypeTagMapID)
										SELECT @opModeDefID, map.AssetVariableTypeTagMapID
										FROM ProcessData.tAssetVariableTypeTagMap map
										JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
										WHERE
											tag.ExistsOnServer = 1
											AND map.VariableTypeID IS NOT NULL
											AND map.TagID IS NOT NULL
											AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID,@securityUserId,-1)=1
									END
							END
							
						IF @tagMapID IS NOT NULL AND Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@securityUserId,-1)=1
							BEGIN
								INSERT INTO @excludes (OpModeDefinitionID, AssetVariableTypeTagMapID) VALUES (@opModeDefID, @tagMapID)
							END
					END
			
				FETCH NEXT FROM curCovereds INTO @opModeDefID,@assetID, @tagMapID, @include, @bringDescendants
			END
		ELSE
			BEGIN
				SET @OK = 0
			END

	CLOSE curCovereds
	DEALLOCATE curCovereds

	IF @returnMeshOrExcludes IS NULL OR @returnMeshOrExcludes = 1
		BEGIN
			SELECT OpModeDefinitionID, AssetVariableTypeTagMapID
			FROM @includes
			WHERE
				(@astVarTypeTagMapID IS NULL OR AssetVariableTypeTagMapID = @astVarTypeTagMapID)
			EXCEPT
			SELECT OpModeDefinitionID, AssetVariableTypeTagMapID
			FROM @excludes
			WHERE
				(@astVarTypeTagMapID IS NULL OR AssetVariableTypeTagMapID = @astVarTypeTagMapID)
		END
	ELSE IF @returnMeshOrExcludes = 2
		BEGIN
			SELECT OpModeDefinitionID, AssetVariableTypeTagMapID
			FROM @excludes
			WHERE
				(@astVarTypeTagMapID IS NULL OR AssetVariableTypeTagMapID = @astVarTypeTagMapID)
		END

END


GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetAstVarTypeTagMapIDsCoveredByNDOpModeDef] TO [TEUser]
    AS [dbo];

