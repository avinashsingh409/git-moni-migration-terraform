﻿CREATE PROCEDURE [Monitoring].[spNDModelsCountInAlertGroupedByChild]
@securityUserID int,
@assetID int,
@onlyIncludeChildren bit
AS
BEGIN

declare @groupAssets base.tpIntList
if @onlyIncludeChildren = 1
  begin
  insert into @groupAssets select assetid from Asset.tAsset where ParentAssetID = @assetId
  end
else
  begin
  insert into @groupAssets values (@assetId);
  end


declare @modelAssets as table
(ModelID int, AssetID int, GroupAssetID int);

insert into @modelAssets
  select 
	distinct model.ModelID,map.AssetID,0 as GroupAssetID
	from monitoring.tndmodel model JOIN ProcessData.tAssetVariableTypeTagMap map on 
	  model.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID  
	  JOIN Monitoring.tNDModelEvaluation mdleval ON model.ModelID = mdleval.ModelID	  
	  LEFT JOIN Monitoring.tNDModel_AlertScreening tmas
			ON tmas.ModelID = model.ModelID and tmas.Until > GETDATE()
	  WHERE  model.Active = 1 and mdleval.Alert = 1 
	  And (isnull(tmas.IgnoreStatus,0) = 0 or mdleval.IgnoreOverrideAlert = 1)

	  declare @modelIDs as base.tpintlist
	  insert into @modelIDs select distinct modelid from @modelAssets
	  declare @modelstatus as table
	  (
	     modelid int primary key,
		 IsWatchSet bit
	  )

insert into @modelstatus
	  select modelid,iswatchset from Monitoring.ufnMOdelCurrentActionItemStatuses (@modelIDs)

delete x from @modelAssets x join @modelstatus b on x.ModelID = b.modelid where b.IsWatchSet<>0
    

declare @uniqueAssets as Base.tpIntList
insert into @uniqueAssets select distinct assetid from @modelAssets

-- remove models from the population that the user does not have access to
delete from @modelAssets where AssetID not in (select AssetID from Asset.ufnUserHasAccessToAssets(@securityUserID,@uniqueAssets))

update a set GroupAssetID = b.assetid from @modelAssets a cross apply Asset.ufnAssetAncestors(a.assetid) b join @groupAssets c on b.AssetID = c.id
-- ufnAssetAncestors does not include the base asset, so next update statement looks for models that are tied directly to the grouping asset
update a set GroupAssetID = a.AssetID from @modelAssets a join @groupAssets c on a.AssetID = c.id

delete from @modelAssets where GroupAssetID = 0
select groupassetid,COUNT(*) as cnt from @modelAssets group by groupassetid
  
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spNDModelsCountInAlertGroupedByChild] TO [TEUser]
    AS [dbo];
