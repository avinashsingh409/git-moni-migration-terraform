﻿CREATE PROCEDURE [Monitoring].[spGetIssuesAndAlertsCount]
(
@securityUserID INT,
@assetID INT,
@levels INT
)
AS
BEGIN

DECLARE @assets Base.tpIntList
DECLARE @allowedAssets Base.tpIntList

INSERT INTO @assets(id)
SELECT a.AssetID FROM Asset.tAsset a
INNER JOIN Asset.tAssetHop h
ON a.AssetID = h.EndingAssetId
WHERE h.HopAssetId = @assetID 
AND 
h.AncestryLevel BETWEEN (-1 * @levels) AND 0

INSERT INTO @allowedAssets(id)
SELECT a.AssetID FROM Asset.ufnUserHasAccessToAssets(@securityUserID, @assets) a

SELECT a.id assetID, COALESCE(issues.issueCount , 0) issueCount, COALESCE(alerts.alertCount,0) alertCount FROM  @assets a
LEFT JOIN (
SELECT ai.AssetID id, Count(*) issueCount
FROM Diagnostics.tAssetIssue ai
INNER JOIN @allowedAssets a ON ai.AssetID = a.id
INNER JOIN Diagnostics.tAssetIssueActivityStatusType s ON ai.ActivityStatusID = s.AssetIssueActivityStatusTypeID
WHERE s.IsOpen = 1
GROUP BY ai.AssetID 
) issues ON a.id = issues.id
LEFT JOIN  (
SELECT map.AssetID id, Count(*) alertCount
FROM Monitoring.tNDModel m
INNER JOIN ProcessData.tAssetVariableTypeTagMap map ON m.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
INNER JOIN @allowedAssets a ON map.AssetID = a.id
WHERE m.AlertStatusTypeID IS NOT NULL
GROUP BY map.AssetID 
) alerts ON a.id = alerts.id

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetIssuesAndAlertsCount] TO [TEUser]
    AS [dbo];