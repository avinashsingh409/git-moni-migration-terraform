﻿
CREATE PROCEDURE [Monitoring].[spDeleteSubscriptionNDAlertSecurityUserIDMap]
	@ModelID INT
	,@AlertStatusTypeID INT
	,@SubscriberID INT

AS
BEGIN
	IF EXISTS (SELECT * FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID
	AND AlertStatusTypeID = @AlertStatusTypeID AND SecurityUserID = @SubscriberID)
	BEGIN
		DELETE FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID
		AND AlertStatusTypeID = @AlertStatusTypeID AND SecurityUserID = @SubscriberID
	END
END

GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteSubscriptionNDAlertSecurityUserIDMap] TO [TEUser]
    AS [dbo];

