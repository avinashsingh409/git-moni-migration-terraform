﻿CREATE PROCEDURE [Monitoring].[spUpsertImageCondition]
	@ImageConditionID int = null,
	@TagID int,
	@MaxValue real = null,
	@MinValue real = null,
	@SecurityUserID int,
	@ImageConditionResultID INT,
	@NewImageConditionID INT OUTPUT
AS
BEGIN
	DECLARE @user int = null;
	SELECT TOP 1 @user = SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID; 
	IF @user IS NULL
	BEGIN
		RAISERROR('User does not exist',16,1)
	END

	DECLARE @AssetID INT
	SELECT @AssetID = AssetID FROM [Monitoring].[tImageConditionResult]
		WHERE @ImageConditionResultID = [tImageConditionResult].[ImageConditionResultID]


	IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User cannot access asset',16, 1);
	END


	IF(@ImageConditionID IS NULL OR @ImageConditionID <= 0)
	BEGIN
		INSERT INTO Monitoring.tImageCondition(
			TagID
			, MaxValue
			, MinValue
			, ImageConditionResultID
		)
		VALUES (
			@TagID
			, @MaxValue
			, @MinValue
			, @ImageConditionResultID
		)
		SET @NewImageConditionID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE Monitoring.tImageCondition
		SET
			TagID = @TagID
			, MaxValue = @MaxValue
			, MinValue = @MinValue
			, ImageConditionResultID  = @ImageConditionResultID
		WHERE ImageConditionID = @ImageConditionID
		SET @NewImageConditionID = @ImageConditionID
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpsertImageCondition] TO [TEUser]
    AS [dbo];