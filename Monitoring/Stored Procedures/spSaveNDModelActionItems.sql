﻿CREATE PROCEDURE [Monitoring].[spSaveNDModelActionItems]
	@actionItems Monitoring.tpNDModelActionItem READONLY,
	@userId int
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #DISABLEMODELACTIONITEMTRIGGER(col1 SMALLINT);

	-- If IsArchive = TRUE, insert into [Monitoring].[tNDModelActionItemArchive]
	INSERT INTO [Monitoring].[tNDModelActionItemArchive]
	(	[ModelID],
		[ModelActionItemPriorityTypeID],
		[ModelActionItemTypeID],
		[NoteText],
		[CreatedByUserID],
		[CreateDate],
		[ChangedByUserID],
		[ChangeDate],
		[WatchDuration],
		[WatchDurationTemporalTypeID],
		[WatchCriteriaTypeID],
		[WatchLimit],
		[WatchLimitMeetsTemporalTypeID],
		[WatchRelativeToExpected],
		[NDAlert],
		[NDActual],
		[NDExpected],
		[NDLower],
		[NDUpper],
		[PercentOOB],
		[SerializedOpMode],
		[PrioritizedOpMode],
		[EvaluationStatus],
		[HiHiAlert],
		[LowLowAlert],
		[AreaFastResponseAlert],
		[AreaSlowResponseAlert],
		[FrequencyAlert],
		[OscillationAlert],
		[FrozenDataAlert],
		[WatchOverrideAlert],
		[IgnoreOverrideAlert],
		[AddedtoAlerts],
		[AlertStatusPriorityTypeID],
		[IgnoreDurationHours],
		[Favorite],
		[WatchEmailIfAlert],
		[WatchDuration2],
		[WatchDurationTemporalTypeID2],
		[WatchCriteriaTypeID2],
		[WatchLimit2],
		[WatchLimitMeetsTemporalTypeID2],
		[WatchRelativeToExpected2]
	)
	SELECT ai.ModelID,
		ai.ModelActionItemPriorityTypeID,
		ai.ModelActionItemTypeID,
		ai.NoteText,
		@userId,
		ai.ChangeDate,
		@userId,
		ai.CreateDate,
		ai.WatchDuration,
		ai.WatchDurationTemporalTypeID,
		ai.WatchCriteriaTypeID,
		ai.WatchLimit,
		ai.WatchLimitMeetsTemporalTypeID,
		ai.WatchRelativeToExpected,
	    ai.NDAlert,
		ai.NDActual,
		ai.NDExpected,
		ai.NDLower,
		ai.NDUpper,
		ai.PercentOOB,
		ai.SerializedOpMode,
		ai.PrioritizedOpMode,
		ai.EvaluationStatus,
		ai.HiHiAlert,
		ai.LowLowAlert,
		ai.AreaFastResponseAlert,
		ai.AreaSlowResponseAlert,
		ai.FrequencyAlert,
		ai.OscillationAlert,
		ai.FrozenDataAlert,
		ai.WatchOverrideAlert,
		ai.IgnoreOverrideAlert,
		ai.AddedtoAlerts,
		ai.AlertStatusPriorityTypeID,
		ai.IgnoreDurationHours,
		ai.Favorite,
		ai.WatchEmailIfAlert,
		ai.WatchDuration2,
		ai.WatchDurationTemporalTypeID2,
		ai.WatchCriteriaTypeID2,
		ai.WatchLimit2,
		ai.WatchLimitMeetsTemporalTypeID2,
		ai.WatchRelativeToExpected2
	FROM @actionItems ai
	WHERE ai.IsArchive = 1;

	-- If IsArchive = FALSE, Insert or Update [Monitoring].[tNDModelActionItem]
	-- UPDATE
	UPDATE tnai
	SET
		tnai.ModelActionItemPriorityTypeID = ai.ModelActionItemPriorityTypeId
		,tnai.ModelActionItemTypeID = ai.ModelActionItemTypeId
		,tnai.NoteText = ai.NoteText
		,tnai.ChangedByUserID = @userId
		,tnai.ChangeDate = ai.ChangeDate
		,tnai.WatchDuration = ai.WatchDuration
		,tnai.WatchDurationTemporalTypeID = ai.WatchDurationTemporalTypeId
		,tnai.WatchCriteriaTypeID = ai.WatchCriteriaTypeId
		,tnai.WatchLimit = ai.WatchLimit
		,tnai.WatchLimitMeetsTemporalTypeID = ai.WatchLimitMeetsTemporalTypeId
		,tnai.WatchRelativeToExpected = ai.WatchRelativeToExpected
		,tnai.Favorite = ai.Favorite 
		,tnai.WatchEmailIfAlert = ai.WatchEmailIfAlert 
		,tnai.WatchDuration2 = ai.WatchDuration2
		,tnai.WatchDurationTemporalTypeID2 = ai.WatchDurationTemporalTypeId2
		,tnai.WatchCriteriaTypeID2 = ai.WatchCriteriaTypeId2
		,tnai.WatchLimit2 = ai.WatchLimit2
		,tnai.WatchLimitMeetsTemporalTypeID2 = ai.WatchLimitMeetsTemporalTypeId2
		,tnai.WatchRelativeToExpected2 = ai.WatchRelativeToExpected2
	FROM Monitoring.tNDModelActionItem tnai
	INNER JOIN @actionItems ai
	ON ai.ModelId = tnai.ModelID
	INNER JOIN Monitoring.tNDModel tn
		ON tn.ModelID = tnai.ModelID
	INNER JOIN ProcessData.tAssetVariableTypeTagMap tavttm
		ON tavttm.AssetVariableTypeTagMapID = tn.AssetVariableTypeTagMapID
	WHERE Asset.ufnDoesUserHaveAccessToAsset(tavttm.AssetID, @userId, -1) = 1
	AND ai.IsArchive = 0;

	-- CREATE
	INSERT INTO Monitoring.tNDModelActionItem
	(
		ModelID,
		ModelActionItemPriorityTypeID,
		NoteText,
		CreatedByUserID,
		CreateDate,
		ChangedByUserID,
		ChangeDate,
		WatchDuration,
		WatchDurationTemporalTypeID,
		WatchCriteriaTypeID,
		WatchLimit,
		WatchLimitMeetsTemporalTypeID,
		ModelActionItemTypeID,
		WatchRelativeToExpected,
		Favorite,
		WatchEmailIfAlert,
		WatchDuration2,
		WatchDurationTemporalTypeID2,
		WatchCriteriaTypeID2,
		WatchLimit2,
		WatchLimitMeetsTemporalTypeID2,
		WatchRelativeToExpected2
	)
	SELECT
		ai.ModelId
		, ai.ModelActionItemPriorityTypeId
		, ai.NoteText
		, @userId
		, ai.CreateDate
		, @userId
		, ai.ChangeDate
		, ai.WatchDuration
		, ai.WatchDurationTemporalTypeId
		, ai.WatchCriteriaTypeId
		, ai.WatchLimit
		, ai.WatchLimitMeetsTemporalTypeId
		, ai.ModelActionItemTypeId
		, ai.WatchRelativeToExpected
		, ai.Favorite
		, ai.WatchEmailIfAlert
		, ai.WatchDuration2
		, ai.WatchDurationTemporalTypeId2
		, ai.WatchCriteriaTypeId2
		, ai.WatchLimit2
		, ai.WatchLimitMeetsTemporalTypeId2
		, ai.WatchRelativeToExpected2
	FROM @actionItems ai
	LEFT JOIN Monitoring.tNDModelActionItem tnai
	ON ai.ModelId = tnai.ModelID
	WHERE tnai.ModelID IS NULL
	AND ai.[IsArchive] = 0;

	IF OBJECT_ID('tempdb..#DISABLEMODELACTIONITEMTRIGGER') IS NOT NULL DROP TABLE #DISABLEMODELACTIONITEMTRIGGER;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelActionItems] TO [TEUser]
    AS [dbo];

