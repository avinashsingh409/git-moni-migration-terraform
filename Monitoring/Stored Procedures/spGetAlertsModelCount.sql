﻿CREATE PROCEDURE [Monitoring].[spGetAlertsModelCount] 
	@securityUserId INT,
	@assetId INT
AS
BEGIN
	
	DECLARE @modelInfo TABLE
	(
	  ModelID                        INT      PRIMARY KEY,
	  ActionWatch                    BIT      NULL
	);

	INSERT INTO @modelInfo(ModelID, ActionWatch)
		SELECT
		  model.ModelID,
		  NULL AS ActionWatch
		FROM Asset.ufnGetAssetIdsForUserStartingAt(@securityUserId,@assetId) ufnAllAssets 
		INNER JOIN ProcessData.tAssetVariableTypeTagMap map ON ufnAllAssets.AssetID = map.AssetID
		INNER JOIN Monitoring.tNDModel model ON map.AssetVariableTypeTagMapID = model.AssetVariableTypeTagMapID
		LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = model.ModelID and tmas.Until > GETDATE()
		LEFT JOIN Monitoring.tNDModelEvaluation mdleval ON model.ModelID = mdleval.ModelID
		WHERE
		   model.Active = 1
		   AND mdleval.Alert = 1
		   AND (ISNULL(tmas.IgnoreStatus, 0) = 0 OR mdleval.IgnoreOverrideAlert = 1 )
	 
	DECLARE @usedModelIDs AS base.tpintlist
	INSERT INTO @usedModelIDs 
	SELECT DISTINCT ModelID FROM @modelInfo
	 
	IF EXISTS(SELECT * FROM @usedModelIDs)
	BEGIN
		
		DECLARE @modelCurrentActionItemTypeStatus TABLE (
			ModelID int PRIMARY KEY,
			IsWatchSet BIT null
		);
		INSERT INTO @modelCurrentActionItemTypeStatus (ModelID, IsWatchSet)
		SELECT ModelID, IsWatchSet  FROM Monitoring.ufnModelCurrentActionItemStatuses(@usedModelIDs)  
	 
		UPDATE a SET ActionWatch = b.IsWatchSet FROM @modelInfo a JOIN @modelCurrentActionItemTypeStatus b ON a.ModelID = b.ModelID

	END

	DELETE modelinfo FROM @modelInfo modelinfo
	WHERE 0 <> ISNULL(modelinfo.ActionWatch, 0)

	SELECT COUNT(ModelID) as ModelCount from @modelInfo;
	RETURN;
	
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetAlertsModelCount] TO [TEUser]
    AS [dbo];
GO