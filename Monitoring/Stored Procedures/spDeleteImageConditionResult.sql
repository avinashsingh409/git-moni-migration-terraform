﻿CREATE PROCEDURE [Monitoring].[spDeleteImageConditionResult]
	@ImageConditionResultID INT
    ,@SecurityUserID int
AS
BEGIN
	DECLARE @user int = null;
	SELECT TOP 1 @user = SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID; 
	IF @user IS NULL
	BEGIN
		RAISERROR('User does not exist',16,1)
	END

	DECLARE @OldAssetID INT
	SELECT @OldAssetID = AssetID FROM [Monitoring].[tImageConditionResult]
		WHERE @ImageConditionResultID = tImageConditionResult.ImageConditionResultID

	IF Asset.ufnDoesUserHaveAccessToAsset(@OldAssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User does not have permission to access asset',16, 1);
	END
	
	DELETE FROM Monitoring.tImageConditionResult WHERE ImageConditionResultID = @ImageConditionResultID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteImageConditionResult] TO [TEUser]
    AS [dbo];