﻿CREATE PROCEDURE [Monitoring].[spGetNDModelInputTagMap]
  (@modelID int = NULL)
AS
BEGIN
  IF @modelID IS NULL OR @modelID = 0
    SELECT [ModelInputTagMapID],
          [ModelID],
          [AssetVariableTypeTagMapID],
          [RecInputVariableMapID],
          [Active],
          [Required]
    FROM [Monitoring].[tNDModelInputTagMap];
  ELSE
    SELECT [ModelInputTagMapID],
          [ModelID],
          [AssetVariableTypeTagMapID],
          [RecInputVariableMapID],
          [Active],
          [Required]
    FROM [Monitoring].[tNDModelInputTagMap] WHERE [ModelID] = @modelID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelInputTagMap] TO [TEUser]
    AS [dbo];

