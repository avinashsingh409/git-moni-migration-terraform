﻿
CREATE PROCEDURE [Monitoring].[spUpdateModelSnapshots]
    @TVP Monitoring.tpModelSnapshot READONLY
    AS 
	UPDATE Monitoring.tModelSnapshot SET
		 Timestamp = S.Timestamp
		,ActualValue = S.ActualValue
		,ExpectedValue = S.ExpectedValue
		,UpperLimit = S.UpperLimit
		,LowerLimit = S.LowerLimit
		,LastAnomaly = S.LastAnomaly
		,StateChanged = S.StateChanged
		,CalculationStatus = S.Status
		,CalculationStatusInfo = S.StatusInfo
	FROM Monitoring.tModelSnapshot T
	INNER JOIN @TVP S ON S.ModelSnapshotID = T.ModelSnapshotID
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpdateModelSnapshots] TO [TEUser]
    AS [dbo];

