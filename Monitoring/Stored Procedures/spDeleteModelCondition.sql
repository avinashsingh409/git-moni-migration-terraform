﻿CREATE PROCEDURE [Monitoring].[spDeleteModelCondition]
	@ModelConditionID int
AS
BEGIN
	DELETE FROM Monitoring.tModelCondition WHERE ModelConditionID = @ModelConditionID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteModelCondition] TO [TEUser]
    AS [dbo];

