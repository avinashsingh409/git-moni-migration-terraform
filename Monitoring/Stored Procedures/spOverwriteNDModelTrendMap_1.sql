﻿
CREATE PROCEDURE [Monitoring].[spOverwriteNDModelTrendMap]
	@UserID int,
	@ModelID int,
	@ModelTrendAssociations Monitoring.tpNDModelTrendMap READONLY,
	@Success bit OUTPUT,
	@FilterCategoryIDs Base.tpIntList READONLY
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @modelAssetID int;
	SET @modelAssetID = (
		SELECT map.AssetID
		FROM ProcessData.tAssetVariableTypeTagMap map
		JOIN Monitoring.tNDModel m ON map.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID
		WHERE m.ModelID = @ModelID
	);

	IF Asset.ufnDoesUserHaveAccessToAsset(@modelAssetID, @UserID, -1) = 0
	BEGIN
		RAISERROR('User does not have access to NDModel asset', 11, 1);
	END


	DELETE FROM mta
	FROM Monitoring.tNDModelTrendMap mta
	JOIN Monitoring.tNDModel m ON mta.ModelID = m.ModelID
	JOIN ProcessData.tAssetVariableTypeTagMap map ON m.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
	JOIN Asset.tAsset a ON map.AssetID = a.AssetID
	WHERE
		mta.ModelID = @ModelID
		AND Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @UserID, -1) = 1 AND Asset.ufnDoesUserHaveAccessToAsset(mta.AssetID, @UserID, -1) = 1;


	-- standard trends first
	INSERT INTO Monitoring.tNDModelTrendMap (ModelID, PDTrendID, AssetID, ReplaceTrendContents, DisplayOrder, CreatedByUserID, ChangedByUserID)
	SELECT
		@ModelID
		, mta.PDTrendID
		, mta.AssetID
		, mta.ReplaceTrendContents
		, mta.DisplayOrder
		, @UserID
		, @UserID
	FROM @ModelTrendAssociations mta
	JOIN ProcessData.tTrend t ON mta.PDTrendID = t.PDTrendID
	LEFT JOIN ProcessData.tAssetTrendMap map ON t.PDTrendID = map.TrendID
	INNER JOIN ProcessData.ufnGetRoleSecuritizedTrendsFromUser(@UserID, @FilterCategoryIDs,7) trend ON t.PDTrendID = trend.PDTrendID
	WHERE map.AssetTrendMapID IS NULL AND Asset.ufnDoesUserHaveAccessToAsset(mta.AssetID, @UserID, -1) = 1;

	-- instance trends second
	INSERT INTO Monitoring.tNDModelTrendMap (ModelID, PDTrendID, AssetID, ReplaceTrendContents, DisplayOrder, CreatedByUserID, ChangedByUserID)
	SELECT
		@ModelID
		, mta.PDTrendID
		, mta.AssetID
		, mta.ReplaceTrendContents
		, mta.DisplayOrder
		, @UserID
		, @UserID
	FROM @ModelTrendAssociations mta
	JOIN ProcessData.tTrend t ON mta.PDTrendID = t.PDTrendID
	JOIN ProcessData.tAssetTrendMap map ON t.PDTrendID = map.TrendID
	JOIN Asset.tAsset a ON map.AssetID = a.AssetID
    JOIN ProcessData.ufnGetRoleSecuritizedTrendsFromUser(@UserID, @FilterCategoryIDs,7) trend ON t.PDTrendID = trend.PDTrendID
	WHERE Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @UserID, -1) = 1 AND Asset.ufnDoesUserHaveAccessToAsset(mta.AssetID, @UserID, -1) = 1;
		

	SET @Success = 1;

	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		SET @Success = 0;
	END CATCH

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spOverwriteNDModelTrendMap] TO [TEUser]
    AS [dbo];

