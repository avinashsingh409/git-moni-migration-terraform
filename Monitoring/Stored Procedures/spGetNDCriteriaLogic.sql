﻿


CREATE PROCEDURE [Monitoring].[spGetNDCriteriaLogic]
  (@CriteriaGroupID int = NULL)
AS
BEGIN
  IF @CriteriaGroupID IS NULL OR @CriteriaGroupID = 0
    SELECT [CriteriaLogicID],
          [CriteriaGroupID],
          [AssetVariableTypeTagMapID],
          [LogicOperatorTypeID],
          [Value],
          [TransiencyStdDevMultiplier],
          [TransiencyDuration],
          [TransiencyDurationTemporalTypeID],
          [DisplayOrder],
		  [LogicStartDate],
		  [LogicEndDate]
    FROM [Monitoring].[tNDCriteriaLogic];
  ELSE
    SELECT [CriteriaLogicID],
          [CriteriaGroupID],
          [AssetVariableTypeTagMapID],
          [LogicOperatorTypeID],
          [Value],
          [TransiencyStdDevMultiplier],
          [TransiencyDuration],
          [TransiencyDurationTemporalTypeID],
          [DisplayOrder],
		  [LogicStartDate],
		  [LogicEndDate]
    FROM [Monitoring].[tNDCriteriaLogic] WHERE [CriteriaGroupID] = @CriteriaGroupID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDCriteriaLogic] TO [TEUser]
    AS [dbo];

