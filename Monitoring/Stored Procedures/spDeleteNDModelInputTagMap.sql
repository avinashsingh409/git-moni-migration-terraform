﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDModelInputTagMap]
  (@ModelInputTagMapID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDModelInputTagMap] WHERE [ModelInputTagMapID]=@ModelInputTagMapID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDModelInputTagMap] TO [TEUser]
    AS [dbo];

