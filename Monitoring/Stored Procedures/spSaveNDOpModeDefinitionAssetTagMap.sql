﻿
CREATE PROCEDURE [Monitoring].[spSaveNDOpModeDefinitionAssetTagMap]
  @OpModeDefinitionAssetTagMapID int = NULL,
   @OpModeDefinitionID int,
   @AssetID int = NULL,
   @AssetVariableTypeTagMapID int = NULL,
   @Include bit,
   @BringDescendants bit
AS
BEGIN
  IF @OpModeDefinitionAssetTagMapID IS NULL OR @OpModeDefinitionAssetTagMapID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDOpModeDefinitionAssetTagMap]
        ([OpModeDefinitionID],
         [AssetID],
         [AssetVariableTypeTagMapID],
         [Include],
         [BringDescendants])
      VALUES
        (@OpModeDefinitionID,
         @AssetID,
         @AssetVariableTypeTagMapID,
         @Include,
         @BringDescendants);

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDOpModeDefinitionAssetTagMap]
        SET [OpModeDefinitionID] = @OpModeDefinitionID,
            [AssetID] = @AssetID,
            [AssetVariableTypeTagMapID] = @AssetVariableTypeTagMapID,
            [Include] = @Include,
            [BringDescendants] = @BringDescendants
        WHERE [OpModeDefinitionAssetTagMapID] = @OpModeDefinitionAssetTagMapID;

	  SELECT @OpModeDefinitionAssetTagMapID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDOpModeDefinitionAssetTagMap] TO [TEUser]
    AS [dbo];

