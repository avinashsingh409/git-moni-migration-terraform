﻿
CREATE PROCEDURE [Monitoring].[spSaveNDAlertNotification] 
	@NDAlertNotificationID UNIQUEIDENTIFIER = NULL
	,@SecurityUserID INT 
	,@ModelID INT 
	,@AlertStatusTypeID INT 
	,@NotificationMode BIT 
	,@NotificationFrequency INT 
	,@NotificationTitle varchar(255) 
	,@NotificationSummary nvarchar(max) 
	,@NotificationLastSentTime [datetime]
    ,@NewID UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN

		IF (Monitoring.ufnDoesUserHaveAccessToNDModel(@ModelID, @SecurityUserID) = 0)
			BEGIN
				RAISERROR( 'User cannot access NDModel',16, 1);
			END
		ELSE
			BEGIN
				IF NOT EXISTS (SELECT * FROM [Monitoring].[tNDAlertNotification] WHERE NDAlertNotificationID = @NDAlertNotificationID)
				BEGIN
					IF @ModelID IS NULL OR @AlertStatusTypeID  IS NULL
						RAISERROR (N'Cannot create a Alerts Notification with no associated ModelID',11,11, 'Monitoring.spSaveNDAlertNotification');

					DECLARE @TmpTable TABLE (NDAlertNotificationID uniqueidentifier);
					INSERT INTO [Monitoring].[tNDAlertNotification]([ModelID],[AlertStatusTypeID],[NotificationMode],[NotificationFrequency],[NotificationTitle]
							,[NotificationSummary], [CreatedByUserID],[CreateDate],[ChangedByUserID],[ChangeDate],[NotificationLastSentTime] )
					OUTPUT INSERTED.NDAlertNotificationID INTO @TmpTable
					VALUES (@ModelID, @AlertStatusTypeID, @NotificationMode, @NotificationFrequency,@NotificationTitle 
							,@NotificationSummary, @SecurityUserID,GETDATE(),@SecurityUserID,GETDATE(), @NotificationLastSentTime)
					SELECT @NewID = NDAlertNotificationID FROM @TmpTable;
				END
				ELSE
				BEGIN
					UPDATE [Monitoring].[tNDAlertNotification] 
					SET ModelID = @ModelID
					,AlertStatusTypeID = @AlertStatusTypeID
					,NotificationMode  = @NotificationMode
					,NotificationFrequency = @NotificationFrequency
					,NotificationTitle = @NotificationTitle 
					,NotificationSummary = @NotificationSummary 
					,ChangedByUserID = @SecurityUserID
					,ChangeDate = getdate()
					,NotificationLastSentTime = @NotificationLastSentTime
					WHERE NDAlertNotificationID  = @NDAlertNotificationID;

					SET @NewID = @NDAlertNotificationID;
				END
			END
		END
	END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDAlertNotification] TO [TEUser]
    AS [dbo];

