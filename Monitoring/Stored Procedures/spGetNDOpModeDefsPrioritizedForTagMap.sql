﻿


CREATE PROCEDURE [Monitoring].[spGetNDOpModeDefsPrioritizedForTagMap]
	@astVarTypeTagMapID int,
	@opModeTypeID int = null,
	@opModeDefinitionID int = null,
	@returnMeshOrExcludes int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @includes TABLE(
		Priority int,
		OpModeDefinitionID int
	)

	DECLARE @excludes TABLE(
		Priority int,
		OpModeDefinitionID int
	)

	DECLARE @incPriority int;
	SET @incPriority = 1;
	DECLARE @exPriority int;
	SET @exPriority = 1;

	IF EXISTS(
		SELECT map.OpModeDefinitionAssetTagMapID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetVariableTypeTagMapID = @astVarTypeTagMapID
			AND map.Include = 1
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
	)
	BEGIN
		INSERT INTO @includes (Priority, OpModeDefinitionID)
		SELECT @incPriority, map.OpModeDefinitionID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetVariableTypeTagMapID = @astVarTypeTagMapID
			AND map.Include = 1
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
			
		SET @incPriority = @incPriority + 1;
	END

	IF EXISTS(
		SELECT map.OpModeDefinitionAssetTagMapID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetVariableTypeTagMapID = @astVarTypeTagMapID
			AND map.Include = 0
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
	)
	BEGIN
		INSERT INTO @excludes (Priority, OpModeDefinitionID)
		SELECT @exPriority, map.OpModeDefinitionID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetVariableTypeTagMapID = @astVarTypeTagMapID
			AND map.Include = 0
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
			
		SET @exPriority = @exPriority + 1;
	END

	DECLARE @assetID int;
	SET @assetID = (SELECT AssetID FROM ProcessData.tAssetVariableTypeTagMap WHERE AssetVariableTypeTagMapID = @astVarTypeTagMapID);

	IF EXISTS(
		SELECT map.OpModeDefinitionAssetTagMapID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetID = @assetID
			AND map.Include = 1
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
	)
	BEGIN
		INSERT INTO @includes (Priority, OpModeDefinitionID)
		SELECT @incPriority, map.OpModeDefinitionID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetID = @assetID
			AND map.Include = 1
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
			
		SET @incPriority = @incPriority + 1;
	END

	IF EXISTS(
		SELECT map.OpModeDefinitionAssetTagMapID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetID = @assetID
			AND map.Include = 0
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
	)
	BEGIN
		INSERT INTO @excludes (Priority, OpModeDefinitionID)
		SELECT @exPriority, map.OpModeDefinitionID
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
		JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
		WHERE
			map.AssetID = @assetID
			AND map.Include = 0
			AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
			AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
			
		SET @exPriority = @exPriority + 1;
	END

	DECLARE @ancestors TABLE (
		ROWNUMBER int,
		AssetID int
	)
	INSERT INTO @ancestors (ROWNUMBER, AssetID)
	SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ROWNUMBER,
	AssetID
	FROM Asset.ufnAssetAncestors(@assetID)

	DECLARE @ancAssetID int;
	DECLARE curAncestry INSENSITIVE CURSOR
	FOR
		SELECT AssetID
		FROM @ancestors anc
		ORDER BY ROWNUMBER DESC

	OPEN curAncestry
	FETCH NEXT FROM curAncestry INTO @ancAssetID

	DECLARE @OK int;
	SET @OK = 1;
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN
			
				IF EXISTS(
					SELECT map.OpModeDefinitionAssetTagMapID
					FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
					JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
					WHERE
						map.AssetID = @ancAssetID
						AND map.Include = 1
						AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
						AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
				)
				BEGIN
					INSERT INTO @includes (Priority, OpModeDefinitionID)
					SELECT @incPriority, map.OpModeDefinitionID
					FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
					JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
					WHERE
						map.AssetID = @ancAssetID
						AND map.Include = 1
						AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
						AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
						
					SET @incPriority = @incPriority + 1;
				END

				IF EXISTS(
					SELECT map.OpModeDefinitionAssetTagMapID
					FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
					JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
					WHERE
						map.AssetID = @ancAssetID
						AND map.Include = 0
						AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
						AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
				)
				BEGIN
					INSERT INTO @excludes (Priority, OpModeDefinitionID)
					SELECT @exPriority, map.OpModeDefinitionID
					FROM Monitoring.tNDOpModeDefinitionAssetTagMap map
					JOIN Monitoring.tNDOpModeDefinition opmodedef ON map.OpModeDefinitionID = opmodedef.OpModeDefinitionID
					WHERE
						map.AssetID = @ancAssetID
						AND map.Include = 0
						AND (@opModeTypeID IS NULL OR opmodedef.OpModeTypeID = @opModeTypeID)
						AND (@opModeDefinitionID IS NULL OR map.OpModeDefinitionID = @opModeDefinitionID)
						
					SET @exPriority = @exPriority + 1;
				END

				FETCH NEXT FROM curAncestry INTO @ancAssetID
			END
		ELSE
			BEGIN
				SET @OK = 0
			END

	CLOSE curAncestry
	DEALLOCATE curAncestry
		
	IF @returnMeshOrExcludes IS NULL OR @returnMeshOrExcludes = 1
		BEGIN
			DECLARE @opModeDefs TABLE(
				OpModeDefinitionID int
			)
			
			INSERT INTO @opModeDefs (OpModeDefinitionID)
			SELECT OpModeDefinitionID
			FROM @includes
			EXCEPT
			SELECT OpModeDefinitionID
			FROM @excludes
			
			SELECT inc.Priority, inc.OpModeDefinitionID
			FROM @includes inc
			JOIN @opModeDefs op ON inc.OpModeDefinitionID = op.OpModeDefinitionID
		END
	ELSE IF @returnMeshOrExcludes = 2
		BEGIN
			SELECT Priority, OpModeDefinitionID
			FROM @excludes
		END

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeDefsPrioritizedForTagMap] TO [TEUser]
    AS [dbo];

