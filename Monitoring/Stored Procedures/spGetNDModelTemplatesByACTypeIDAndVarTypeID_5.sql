﻿CREATE PROCEDURE [Monitoring].[spGetNDModelTemplatesByACTypeIDAndVarTypeID]
	@AstVarTagMapIds Base.tpIntList readonly,
	@ModelTypeId int = null
AS
BEGIN
	SET NOCOUNT ON;

	select
		ModelTemplateID
		, AssetClassTypeVariableTypeMapID
		, ParentAssetClassTypeID
		, ModelTypeID
		, Temporal
		, ConcerningBoundTypeID
		, CreatedByUserID
		, CreateDate
		, ChangedByUserID
		, ChangeDate
		, UpperCriticality
		, LowerCriticality
		, AssetVariableTypeTagMapID
	from Monitoring.ufnGetNDModelTemplatesByACTypeIDAndVarTypeID(@AstVarTagMapIds, @ModelTypeId)
	order by ModelTemplateID
	;

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelTemplatesByACTypeIDAndVarTypeID] TO [TEUser]
    AS [dbo];

