﻿
CREATE PROCEDURE [Monitoring].[spGetMostRecentModelActionItemArchive]
	@securityUserId int,
	@modelID int
AS
BEGIN
	SET NOCOUNT ON
	Declare @noteAddedTypeID int = 10

	Select Top 1 r.*
	From (
		SELECT
			mdlArchive.ModelActionItemArchiveID as ModelActionItemArchiveID
			, null as ModelConditionNoteID
			, mdlArchive.ModelID
			, mdlArchive.ModelActionItemTypeID
			, mdlactiontype.ModelActionItemTypeAbbrev
			, mdlArchive.ModelActionItemPriorityTypeID
			, mdlArchive.ChangeDate
			, usr.Email as Executor
			, mdlArchive.NoteText
			, mdlArchive.WatchDuration
			, mdlArchive.WatchDurationTemporalTypeID
			, mdlArchive.WatchCriteriaTypeID
			, mdlArchive.WatchLimit
			, mdlArchive.WatchLimitMeetsTemporalTypeID
			, mdlArchive.WatchRelativeToExpected
			, map.AssetID
			, tag.EngUnits
			, mdlArchive.Favorite
			, mdlArchive.WatchEmailIfAlert
			, mdlArchive.WatchDuration2
			, mdlArchive.WatchDurationTemporalTypeID2
			, mdlArchive.WatchCriteriaTypeID2
			, mdlArchive.WatchLimit2
			, mdlArchive.WatchLimitMeetsTemporalTypeID2
			, mdlArchive.WatchRelativeToExpected2
		FROM [Monitoring].[tNDModelActionItemArchive] mdlArchive
		JOIN [Monitoring].[tNDModelActionItemType] mdlactiontype ON mdlArchive.ModelActionItemTypeID = mdlactiontype.ModelActionItemTypeID
		JOIN [Monitoring].[tNDModel] model ON mdlArchive.ModelID = model.ModelID
		JOIN [ProcessData].[tAssetVariableTypeTagMap] map ON model.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
		JOIN [ProcessData].[tTag] tag ON map.TagID = tag.PDTagID
		JOIN [AccessControl].[tUser] usr ON mdlArchive.ChangedByUserID = usr.SecurityUserID

		Union

		select
			null as ModelActionItemArchiveID
			, n.ModelConditionNoteID
			, a.ModelID --Alerts ModelID, Dashboard ModelID is d.ModelID
			, @noteAddedTypeID as ModelActionItemTypeID
			, ait.ModelActionItemTypeAbbrev
			, 0 as ModelActionItemPriorityTypeID
			, n.[TimeStamp] as ChangeDate
			, n.ChangedBy as Executor
			, n.Note as NoteText
			, null as WatchDuration
			, null as WatchDurationTemporalTypeID
			, null as WatchCriteriaTypeID
			, null as WatchLimit
			, null as WatchLimitMeetsTemporalTypeID
			, null as WatchRelativeToExpected
			, m.AssetID
			, t.EngUnits
			, null as Favorite
			, null as WatchEmailIfAlert
			, null as WatchDuration2
			, null as WatchDurationTemporalTypeID2
			, null as WatchCriteriaTypeID2
			, null as WatchLimit2
			, null as WatchLimitMeetsTemporalTypeID2
			, null as WatchRelativeToExpected2
		from Monitoring.tNDModel a
		join Monitoring.tModel d on a.AssetVariableTypeTagMapID = d.AssetVariableTypeTagMapID
		join Monitoring.tModelCondition o on d.ModelID = o.ModelID
		join Monitoring.tModelConditionNote n on o.ModelConditionID = n.ModelConditionID
		join Monitoring.tNDModelActionItemType ait on @noteAddedTypeID = ait.ModelActionItemTypeID
		join ProcessData.tAssetVariableTypeTagMap m on d.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID
		join ProcessData.tTag t on m.TagID = t.PDTagID
	) r
	Where r.ModelID = @modelID
	Order by r.ChangeDate Desc
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetMostRecentModelActionItemArchive] TO [TEUser]
    AS [dbo];
