﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDModelTrendMap]
	@UserID int,
	@MTAID int,
	@DeleteCount int OUTPUT
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @modelAssetID int;
	SET @modelAssetID = (
		SELECT map.AssetID
		FROM ProcessData.tAssetVariableTypeTagMap map
		JOIN Monitoring.tNDModel m ON map.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID
		JOIN Monitoring.tNDModelTrendMap mta ON m.ModelID = mta.ModelID
		WHERE mta.NDModelTrendMapID = @MTAID
	);

	IF Asset.ufnDoesUserHaveAccessToAsset(@modelAssetID, @UserID, -1) = 0
	BEGIN
		RAISERROR('User does not have access to NDModel asset', 11, 1);
	END

	IF EXISTS (
		SELECT AssetTrendMapID
		FROM ProcessData.tAssetTrendMap map
		JOIN Monitoring.tNDModelTrendMap mta ON map.TrendID = mta.PDTrendID
		WHERE mta.NDModelTrendMapID = @MTAID
	)
	BEGIN
		DECLARE @trendAssetID int;
		SET @trendAssetID = (
			SELECT map.AssetID
			FROM ProcessData.tAssetTrendMap map
			JOIN Monitoring.tNDModelTrendMap mta ON map.TrendID = mta.PDTrendID
			WHERE mta.NDModelTrendMapID = @MTAID
		);

		IF Asset.ufnDoesUserHaveAccessToAsset(@trendAssetID, @UserID, -1) = 0
		BEGIN
			RAISERROR('User does not have access to Trend asset', 11, 1);
		END

		DECLARE @mappedAssetID int;
		SET @mappedAssetID = (
			SELECT AssetID
			FROM tNDModelTrendMap
			WHERE NDModelTrendMapID = @MTAID
		);

		IF Asset.ufnDoesUserHaveAccessToAsset(@mappedAssetID, @UserID, -1) = 0
		BEGIN
			RAISERROR('User does not have access to mapped Trend asset', 11, 1);
		END

	END


	DELETE FROM Monitoring.tNDModelTrendMap WHERE NDModelTrendMapID = @MTAID;

	SET @DeleteCount = @@ROWCOUNT;

	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		SET @DeleteCount = 0;
	END CATCH
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDModelTrendMap] TO [TEUser]
    AS [dbo];

