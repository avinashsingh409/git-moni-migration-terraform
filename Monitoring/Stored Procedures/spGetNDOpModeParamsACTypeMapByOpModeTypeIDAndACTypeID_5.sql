﻿






CREATE PROCEDURE [Monitoring].[spGetNDOpModeParamsACTypeMapByOpModeTypeIDAndACTypeID] 
	@opModeTypeID int,
	@acTypeID Base.tpIntList readonly
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		map.OpModeParamsAssetClassTypeMapID
		, map.OpModeTypeID
		, map.OpModeParamsID
		, map.AssetClassTypeID
	FROM [Monitoring].[tNDOpModeParamsAssetClassTypeMap] map
	JOIN @acTypeID ac on map.AssetClassTypeID = ac.id
	WHERE OpModeTypeID = @opModeTypeID
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeParamsACTypeMapByOpModeTypeIDAndACTypeID] TO [TEUser]
    AS [dbo];

