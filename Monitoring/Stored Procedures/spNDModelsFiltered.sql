CREATE PROCEDURE [Monitoring].[spNDModelsFiltered]
  @securityUserID           INT,
  @startAssetID             INT = NULL,
  @astVarTypeTagMapId       INT = NULL,
  @active                   BIT = NULL,
  @built                    BIT = NULL,
  @hasCalcPoint             BIT = NULL,
  @tagName                  NVARCHAR(400) = NULL,
  @tagDesc                  NVARCHAR(400) = NULL,
  @modelName                NVARCHAR(255) = NULL,
  @modelTypeID              Base.TPINTLIST READONLY,
  @engUnits                 NVARCHAR(255) = NULL,
  @modelOpModeID            Base.TPINTLIST READONLY,
  @isDiagnose               BIT = NULL,
  @isWatch                  BIT = NULL,
  @isWatchOverride          BIT = NULL,
  @isMaintenance            BIT = NULL,
  @isIgnore                 BIT = NULL,
  @isOpenIssue              BIT = NULL,
  @evaluationErrorMessage   NVARCHAR(400) = NULL,
  @priority                 INT = NULL,
  @outOfBounds              BIT = NULL,
  @alertPriority            INT = NULL,
  @alertType                NVARCHAR(255) = NULL,
  @isAlert                  BIT = NULL,
  @addedToAlertsStart       DATETIME = NULL,
  @addedToAlertEnd          DATETIME = NULL,
  @currentBuildStatusTypeID Base.TPINTLIST READONLY,
  @startIndex               INT = 1,
  @endIndex                 INT = 15,
  @sort                     NVARCHAR(100) = NULL,
  @sortOrder                BIT = 0,
  @assetCriticality			Base.TPINTLIST READONLY,
  @modelCriticality			Base.TPINTLIST READONLY,
  @modelID					INT = NULL,
  @filterCategoryIDs		Base.TPINTLIST READONLY
WITH RECOMPILE
AS
BEGIN

  SET NOCOUNT ON;
  SET ARITHABORT ON

  DECLARE @modelIDs as BASE.tpIntList
  if @modelID is not null
    BEGIN
	INSERT INTO @modelIDs VALUES (@modelID)
	END

  EXECUTE [Monitoring].[spNDModelsFilteredWithModelIDs] 
   @securityUserID
  ,@startAssetID
  ,@astVarTypeTagMapId
  ,@active
  ,@built
  ,@hasCalcPoint
  ,@tagName
  ,@tagDesc
  ,@modelName
  ,@modelTypeID
  ,@engUnits
  ,@modelOpModeID
  ,@isDiagnose
  ,@isWatch
  ,@isWatchOverride
  ,@isMaintenance
  ,@isIgnore
  ,@isOpenIssue
  ,@evaluationErrorMessage
  ,@priority
  ,@outOfBounds
  ,@alertPriority
  ,@alertType
  ,@isAlert
  ,@addedToAlertsStart
  ,@addedToAlertEnd
  ,@currentBuildStatusTypeID
  ,@startIndex
  ,@endIndex
  ,@sort
  ,@sortOrder
  ,@assetCriticality
  ,@modelCriticality
  ,@modelIDs
  ,@filterCategoryIDs
END
GO


GRANT EXECUTE
    ON OBJECT::[Monitoring].[spNDModelsFiltered] TO [TEUser]
    AS [dbo];
