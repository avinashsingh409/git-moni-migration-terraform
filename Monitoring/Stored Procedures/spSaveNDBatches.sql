﻿CREATE PROCEDURE [Monitoring].[spSaveNDBatches]
	@tblBatches Monitoring.tpNDBatch READONLY,
	@tblModels Monitoring.tpNDBatchModel READONLY,
	@Username nvarchar(50),
	@SavedBatchesCount int output
AS
BEGIN

	SET NOCOUNT ON;

	declare @errorMsg varchar(max);
	declare @userId int;
	declare @savedBatches Monitoring.tpNDBatch;

	-- need a valid user
	set @userId = (select SecurityUserID from AccessControl.tUser where Username = @Username);
	if @userId is null
	begin
		set @errorMsg = 'Invalid user';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- model and batch batchid have to line up
	if exists (
		select e.BatchId from (select distinct BatchId from @tblModels except select distinct BatchId from @tblBatches) e
	)
	begin
		set @errorMsg = 'All models must be in a batch';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- each model may only appear once in a batch
	if exists (
		select BatchId, ModelExtID from @tblModels group by BatchId, ModelExtID having count (*) > 1
	)
	begin
		set @errorMsg = 'Models may only appear once in a batch';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- no empty batches
	if exists (
		select e.BatchId from @tblBatches e left join @tblModels m on e.BatchId = m.BatchId group by e.BatchId having count(m.ModelExtID) = 0
	)
	begin
		set @errorMsg = 'Every batch must have at least one model';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- updates to existing batches are disallowed
	declare @existingCount int = 0;
	set @existingCount = (select count (*) from Monitoring.tNDBatch b join @tblBatches a on b.BatchId = a.BatchId);
	if @existingCount <> 0
	begin
		set @errorMsg = 'Updates to existing batches is disallowed';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- user must have access to all existing models' dependent variable assets
	if exists (
		select m.ModelExtID from Monitoring.tNDModel m
		join ProcessData.tAssetVariableTypeTagMap map on m.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
		join @tblModels b on m.ModelExtID = b.ModelExtID
		where Asset.ufnDoesUserHaveAccessToAsset(map.AssetID, @userId, -1) = 0
	)
	begin
		set @errorMsg = 'User does not have permission to all models';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end


	insert into Monitoring.tNDBatch (BatchId, BatchTypeId, UserId, BatchDate)
	output inserted.Batch, inserted.BatchId, inserted.BatchTypeId, @Username, inserted.BatchDate into @savedBatches
	select BatchId, BatchTypeId, @userId, BatchDate from @tblBatches order by Batch;

	insert into Monitoring.tNDBatchModel (BatchId, ModelExtID)
	select BatchId, ModelExtID from @tblModels order by BatchModel;

	set @SavedBatchesCount = (select count (*) from @savedBatches sb join Monitoring.tNDBatch b on sb.BatchId = b.BatchId join Monitoring.tNDBatchModel m on b.BatchId = m.BatchId);

	select b.*, m.*, @Username as Username from @savedBatches sb join Monitoring.tNDBatch b on sb.BatchId = b.BatchId join Monitoring.tNDBatchModel m on b.BatchId = m.BatchId order by b.Batch, m.BatchModel;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDBatches] TO [TEUser]
    AS [dbo];

