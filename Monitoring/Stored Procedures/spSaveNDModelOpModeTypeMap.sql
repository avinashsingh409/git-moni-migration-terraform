﻿
CREATE PROCEDURE [Monitoring].[spSaveNDModelOpModeTypeMap]
  @ModelOpModeTypeMapID int = NULL,
   @ModelID int,
   @OpModeTypeID int
AS
BEGIN
  IF @ModelOpModeTypeMapID IS NULL OR @ModelOpModeTypeMapID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDModelOpModeTypeMap]
        ([ModelID],
         [OpModeTypeID])
      VALUES
        (@ModelID,
         @OpModeTypeID);

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDModelOpModeTypeMap]
        SET [ModelID] = @ModelID,
            [OpModeTypeID] = @OpModeTypeID
        WHERE [ModelOpModeTypeMapID] = @ModelOpModeTypeMapID;

	  SELECT @ModelOpModeTypeMapID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelOpModeTypeMap] TO [TEUser]
    AS [dbo];

