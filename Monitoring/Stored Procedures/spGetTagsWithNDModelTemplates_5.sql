﻿





CREATE PROCEDURE [Monitoring].[spGetTagsWithNDModelTemplates] 
	@securityUserID int,
	@assetID int,
	@opModeTypeID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @mapAssetID int;
	DECLARE @mapVarTypeID int;
	DECLARE @OK int;

	DECLARE @prmaps TABLE(
		AssetVariableTypeTagMapID int
	)

	DECLARE curMaps INSENSITIVE CURSOR
	FOR
		SELECT DISTINCT
			secAstIDs.AssetID
			, map.VariableTypeID
		FROM Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @assetID) secAstIDs
		JOIN Asset.tAsset ast ON secAstIDs.AssetID = ast.AssetID
		JOIN ProcessData.tAssetVariableTypeTagMap map ON ast.AssetID = map.AssetID
		JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
		WHERE
			tag.ExistsOnServer = 1
			AND map.VariableTypeID IS NOT NULL
			AND map.TagID IS NOT NULL

	OPEN curMaps
	FETCH NEXT FROM curMaps INTO @mapAssetID, @mapVarTypeID

	SET @OK = 1
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN
				
				INSERT INTO @prmaps (AssetVariableTypeTagMapID)
				SELECT AssetVariableTypeTagMapID
				FROM ProcessData.ufnGetOutputPrioritizedMaps(@mapAssetID, @mapVarTypeID)
							
				FETCH NEXT FROM curMaps INTO @mapAssetID, @mapVarTypeID
			END
		ELSE
			BEGIN
				SET @OK = 0
			END

	CLOSE curMaps
	DEALLOCATE curMaps

	DECLARE @createdBy NVARCHAR(MAX) = NULL;
	SELECT @createdBy = Username FROM AccessControl.tUser WHERE SecurityUserID = @securityUserID

	SELECT
		-1 AS ModelID
		,'' AS NDProjectPath
		, '' AS NDProjectName
		, '' AS NDResultsPath
		, '' AS ReferenceModelID
		, CAST(NEWID() as nvarchar(50)) AS ModelExtID
		, 'QD_' + tag.TagDesc + ' - ' + optype.OpModeTypeAbbrev  AS ModelName
		, '' AS ModelDesc
		, mdltmplt.ModelTemplateID AS ModelTemplateID
		, prmaps.AssetVariableTypeTagMapID AS AssetVariableTypeTagMapID
		, mdltmplt.ModelTypeID AS ModelTypeID
		, opmodemap.OpModeTypeID AS OpModeTypeID
		, -1 AS OpModeParamsID
		, mdltmplt.Temporal AS Temporal
		, CAST(0 AS bit) AS Built
		, NULL AS BuiltSinceTime
		, CAST(0 AS bit) AS Active
		, NULL AS ActiveSinceTime
		, NULL AS PreserveAlertStatus
		, NULL AS AlertStatusTypeID
		, CAST(1 AS bit) AS IsPublic
		, NULL AS DisplayOrder
		, @createdBy AS CreatedBy
		, GETDATE() AS CreateDate
		, @createdBy AS ChangedBy
		, GETDATE() AS ChangeDate
		, NULL AS BuildInterval
		, NULL AS BuildIntervalTemporalTypeID
		, NULL AS LastInterrogatedTime
		, NULL AS CalcPointID
		, CAST(0 AS bit) AS AutoRetrain
		, CAST(0 AS bit) AS Locked
		, NULL AS LockReasonID
		, NULL As LockedSinceTime
		, NULL AS CriticalityComputedModelID
		, NULL AS ModelConfigURL
		, NULL As DiagnosticDrilldownURL
	FROM @prmaps prmaps
	JOIN ProcessData.tAssetVariableTypeTagMap map ON prmaps.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
	JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
	JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
	JOIN Asset.tAsset parent ON ast.ParentAssetID = parent.AssetID
	JOIN ProcessData.tAssetClassTypeVariableTypeMap acvarmap ON (ast.AssetClassTypeID = acvarmap.AssetClassTypeID AND map.VariableTypeID = acvarmap.VariableTypeID)
	JOIN Monitoring.tNDModelTemplate mdltmplt ON (acvarmap.AssetClassTypeVariableTypeMapID = mdltmplt.AssetClassTypeVariableTypeMapID AND (CASE WHEN mdltmplt.ParentAssetClassTypeID IS NULL OR parent.AssetClassTypeID = mdltmplt.ParentAssetClassTypeID THEN 1 ELSE 0 END = 1))
	JOIN Monitoring.tNDModelTemplateOpModeMap opmodemap ON mdltmplt.ModelTemplateID = opmodemap.ModelTemplateID
	JOIN Monitoring.tNDOpModeType optype ON opmodemap.OpModeTypeID = optype.OpModeTypeID
	WHERE
		(@opModeTypeID IS NULL OR opmodemap.OpModeTypeID = @opModeTypeID)

	ORDER BY
		ast.AssetID
		, map.VariableTypeID
		, map.TagID
	
	
	END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetTagsWithNDModelTemplates] TO [TEUser]
    AS [dbo];

