﻿
CREATE PROCEDURE [Monitoring].spInsertSubscriptionNDAlertSecurityUserIDMap 
	@ModelID INT
	,@AlertStatusTypeID INT
	,@SubscriberID INT 	 
	,@SecurityUserID INT

AS
BEGIN

	IF (Monitoring.ufnDoesUserHaveAccessToNDModel(@ModelID, @SecurityUserID) = 0)
		BEGIN
			RAISERROR( 'User cannot access NDModel',16, 1);
		END
	ELSE
		BEGIN
			IF NOT EXISTS (SELECT * FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID 
			AND AlertStatusTypeID = @AlertStatusTypeID AND SecurityUserID = @SubscriberID)
			BEGIN
				INSERT INTO [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] (ModelID,AlertStatusTypeID,SecurityUserID) 
					VALUES (@ModelID,@AlertStatusTypeID,@SubscriberID)
			END
		END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spInsertSubscriptionNDAlertSecurityUserIDMap] TO [TEUser]
    AS [dbo];

