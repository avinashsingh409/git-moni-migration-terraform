﻿
CREATE PROCEDURE [Monitoring].[spGetNDOpModeType]
  (@OpModeTypeID int = NULL)
AS
BEGIN
  IF @OpModeTypeID IS NULL OR @OpModeTypeID = 0
    SELECT [OpModeTypeID],
          [OpModeTypeAbbrev],
          [OpModeTypeDesc],
          [Priority],
          [DisplayOrder]
    FROM [Monitoring].[tNDOpModeType];
  ELSE
    SELECT [OpModeTypeID],
          [OpModeTypeAbbrev],
          [OpModeTypeDesc],
          [Priority],
          [DisplayOrder]
    FROM [Monitoring].[tNDOpModeType] WHERE [OpModeTypeID] = @OpModeTypeID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeType] TO [TEUser]
    AS [dbo];

