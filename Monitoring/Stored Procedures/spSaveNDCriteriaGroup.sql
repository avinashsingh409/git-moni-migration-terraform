﻿
CREATE PROCEDURE [Monitoring].[spSaveNDCriteriaGroup]
  @CriteriaGroupID int = NULL,
   @OpModeDefinitionID int,
   @IsStart bit,
   @DisplayOrder int = NULL
AS
BEGIN
  IF @CriteriaGroupID IS NULL OR @CriteriaGroupID = 0
    BEGIN
      INSERT INTO [Monitoring].[tNDCriteriaGroup]
        ([OpModeDefinitionID],
         [IsStart],
         [DisplayOrder])
      VALUES
        (@OpModeDefinitionID,
         @IsStart,
         @DisplayOrder);

	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDCriteriaGroup]
        SET [OpModeDefinitionID] = @OpModeDefinitionID,
            [IsStart] = @IsStart,
            [DisplayOrder] = @DisplayOrder
        WHERE [CriteriaGroupID] = @CriteriaGroupID;

	  SELECT @CriteriaGroupID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDCriteriaGroup] TO [TEUser]
    AS [dbo];

