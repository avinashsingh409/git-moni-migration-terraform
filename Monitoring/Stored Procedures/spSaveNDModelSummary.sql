CREATE PROCEDURE [Monitoring].[spSaveNDModelSummary]
    @modelId int,
	@modelActionItemTypeId int,
	@modelActionItemPriorityTypeId int=NULL,
	--@isDiagnose bit=NULL,
	--@isWatch bit=NULL,
	--@IsModelMaintenance bit=NULL,
	@noteText nvarchar(max),
	@watchDuration real=NULL,
	@watchDurationTemporalTypeId int=NULL,
	@watchCriteriaTypeId int=NULL,
	@watchLimit real=NULL,
	@watchLimitMeetsTemporalTypeId int=NULL,
	@watchRelativeToExpected bit=0,
	@favorite bit = 0,
	@userId int,
	@watchEmailIfAlert bit=0,
	@watchDuration2 real=NULL,
	@watchDurationTemporalTypeId2 int=NULL,
	@watchCriteriaTypeId2 int=NULL,
	@watchLimit2 real=NULL,
	@watchLimitMeetsTemporalTypeId2 int=NULL,
	@watchRelativeToExpected2 bit=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT tmai.ModelID FROM Monitoring.tNDModelActionItem tmai WHERE tmai.ModelID=@modelId ) BEGIN

	DECLARE @xmodelActionItemPriorityTypeId int;
	DECLARE @xmodelActionItemTypeId int;
	--DECLARE @xisDiagnose bit;
	--DECLARE @xisWatch bit;
	--DECLARE @xIsModelMaintenance bit;
	DECLARE @xwatchDuration real;
	DECLARE @xwatchDurationTemporalTypeId int;
	DECLARE @xwatchCriteriaTypeId int;
	DECLARE @xwatchLimit real;
	DECLARE @xwatchLimitMeetsTemporalTypeId int;
	DECLARE @xwatchRelativeToExpected bit;
	DECLARE @xwatchEmailIfAlert bit;

	DECLARE @watchExpiredActionItemTypeID int;
	DECLARE @watchOverriddenActionItemTypeID int;
	DECLARE @xwatchDuration2 real;
	DECLARE @xwatchDurationTemporalTypeId2 int;
	DECLARE @xwatchCriteriaTypeId2 int;
	DECLARE @xwatchLimit2 real;
	DECLARE @xwatchLimitMeetsTemporalTypeId2 int;
	DECLARE @xwatchRelativeToExpected2 bit;

	SELECT @xmodelActionItemPriorityTypeId =tmai.ModelActionItemPriorityTypeID ,
	@xmodelActionItemTypeId = tmai.ModelActionItemTypeID,
	--@xisDiagnose = tmai.IsDiagnose,
	--@xisWatch = tmai.IsWatch,
	--@xIsModelMaintenance = tmai.IsModelMaintenance,
	@xwatchDuration = tmai.WatchDuration,
	@xwatchDurationTemporalTypeId = tmai.WatchDurationTemporalTypeID,
	@xwatchCriteriaTypeId = tmai.WatchCriteriaTypeID,
	@xwatchLimit = tmai.WatchLimit,
	@xwatchLimitMeetsTemporalTypeId = tmai.WatchLimitMeetsTemporalTypeID,
	@xwatchRelativeToExpected = tmai.WatchRelativeToExpected,
	@xwatchEmailIfAlert = tmai.WatchEmailIfAlert,
	@xwatchDuration2 = tmai.WatchDuration2,
	@xwatchDurationTemporalTypeId2 = tmai.WatchDurationTemporalTypeID2,
	@xwatchCriteriaTypeId2 = tmai.WatchCriteriaTypeID2,
	@xwatchLimit2 = tmai.WatchLimit2,
	@xwatchLimitMeetsTemporalTypeId2 = tmai.WatchLimitMeetsTemporalTypeID2,
	@xwatchRelativeToExpected2 = tmai.WatchRelativeToExpected2

	FROM Monitoring.tNDModelActionItem tmai WHERE tmai.ModelID = @modelId

	--These three ActionItemTypes should make sure that the Watch parameters are cleared.
	IF (@modelActionItemTypeId = 4 OR @modelActionItemTypeId = 5 OR @modelActionItemTypeId = 14)
	BEGIN
		SET @watchDuration = NULL
		SET @watchDurationTemporalTypeId = NULL
		SET @watchCriteriaTypeId = NULL
		SET @watchLimit = NULL
		SET @watchLimitMeetsTemporalTypeId = NULL
		SET @watchRelativeToExpected = 0
		SET @watchEmailIfAlert = 0
		SET @watchDuration2 = NULL
		SET @watchDurationTemporalTypeId2 = NULL
		SET @watchCriteriaTypeId2 = NULL
		SET @watchLimit2 = NULL
		SET @watchLimitMeetsTemporalTypeId2 = NULL
		SET @watchRelativeToExpected2 = 0
	END
	ELSE
	BEGIN
		SET @watchDuration = COALESCE(@watchDuration,@xwatchDuration)
		SET @watchDurationTemporalTypeId = COALESCE(@watchDurationTemporalTypeId,@xwatchDurationTemporalTypeId)
		SET @watchCriteriaTypeId = COALESCE(@watchCriteriaTypeId,@xwatchCriteriaTypeId)
		SET @watchLimit = COALESCE(@watchLimit,@xwatchLimit)
		SET @watchLimitMeetsTemporalTypeId = COALESCE(@watchLimitMeetsTemporalTypeId,@xwatchLimitMeetsTemporalTypeId)		
		SET @watchDuration2 = COALESCE(@watchDuration2,@xwatchDuration2)
		SET @watchDurationTemporalTypeId2 = COALESCE(@watchDurationTemporalTypeId2,@xwatchDurationTemporalTypeId2)
		SET @watchCriteriaTypeId2 = COALESCE(@watchCriteriaTypeId2,@xwatchCriteriaTypeId2)
		SET @watchLimit2 = COALESCE(@watchLimit2,@xwatchLimit2)
		SET @watchLimitMeetsTemporalTypeId2 = COALESCE(@watchLimitMeetsTemporalTypeId2,@xwatchLimitMeetsTemporalTypeId2)
		--If not Watch Set or Quickwatchset, the existing RelativeToExpected should persist
		IF(@modelActionItemTypeId <> 3 AND @modelActionItemTypeId <> 16 ) 
		BEGIN 
			SET @watchRelativeToExpected = @xwatchRelativeToExpected			
			SET @watchRelativeToExpected2 = @xwatchRelativeToExpected2
		END
		SET @watchEmailIfAlert = COALESCE(@watchEmailIfAlert,@xwatchEmailIfAlert)
	END

		UPDATE Monitoring.tNDModelActionItem
		SET	Monitoring.tNDModelActionItem.ModelActionItemPriorityTypeID = COALESCE(@modelActionItemPriorityTypeId,@xmodelActionItemPriorityTypeId), -- int
			--Monitoring.tNDModelActionItem.IsDiagnose = COALESCE(@isDiagnose,@xisDiagnose), -- bit
			--Monitoring.tNDModelActionItem.IsWatch = COALESCE(@isWatch,@xisWatch), -- bit
			--Monitoring.tNDModelActionItem.IsModelMaintenance = COALESCE(@isModelMaintenance,@xisModelMaintenance), -- bit
			Monitoring.tNDModelActionItem.ModelActionItemTypeID = COALESCE(@modelActionItemTypeId,@xmodelActionItemTypeId), -- int
			Monitoring.tNDModelActionItem.NoteText = @noteText, -- nvarchar
			Monitoring.tNDModelActionItem.ChangedByUserID = @userId, -- int
			Monitoring.tNDModelActionItem.ChangeDate = GETDATE(),
			Monitoring.tNDModelActionItem.WatchDuration = @watchDuration, -- real
			Monitoring.tNDModelActionItem.WatchDurationTemporalTypeID = @watchDurationTemporalTypeId, -- int
			Monitoring.tNDModelActionItem.WatchCriteriaTypeID = @watchCriteriaTypeId, -- int
			Monitoring.tNDModelActionItem.WatchLimit = @watchLimit, -- real
			Monitoring.tNDModelActionItem.WatchLimitMeetsTemporalTypeID = @watchLimitMeetsTemporalTypeId, -- int
			Monitoring.tNDModelActionItem.WatchRelativeToExpected = @watchRelativeToExpected, -- bit
			Monitoring.tNDModelActionItem.Favorite = @favorite, --bit
			Monitoring.tNDModelActionItem.WatchEmailIfAlert = @watchEmailIfAlert, -- bit
			Monitoring.tNDModelActionItem.WatchDuration2 = @watchDuration2, -- real
			Monitoring.tNDModelActionItem.WatchDurationTemporalTypeID2 = @watchDurationTemporalTypeId2, -- int
			Monitoring.tNDModelActionItem.WatchCriteriaTypeID2 = @watchCriteriaTypeId2, -- int
			Monitoring.tNDModelActionItem.WatchLimit2 = @watchLimit2, -- real
			Monitoring.tNDModelActionItem.WatchLimitMeetsTemporalTypeID2 = @watchLimitMeetsTemporalTypeId2, -- int
			Monitoring.tNDModelActionItem.WatchRelativeToExpected2 = @watchRelativeToExpected2 -- bit
		FROM Monitoring.tNDModelActionItem tmai
		INNER JOIN Monitoring.tNDModel tn ON tn.ModelID = tmai.ModelID
		INNER JOIN ProcessData.tAssetVariableTypeTagMap tavttm ON tavttm.AssetVariableTypeTagMapID = tn.AssetVariableTypeTagMapID
		WHERE tmai.ModelID = @modelId
		AND Asset.ufnDoesUserHaveAccessToAsset(tavttm.AssetID,@userId,-1)=1
	END ELSE BEGIN
		--If new action item for a fresh model, default @watchRelativeToExpected to false if not Watch Set or QuickWatchSet. 
		IF(@modelActionItemTypeId <> 3 AND @modelActionItemTypeId <> 16) 
		BEGIN 
			SET @watchRelativeToExpected = 0
			SET @watchRelativeToExpected2 = 0
		END

		INSERT INTO Monitoring.tNDModelActionItem
		(
			ModelID,
			ModelActionItemPriorityTypeID,
			--IsDiagnose,
			--IsWatch,
			--IsModelMaintenance,
			ModelActionItemTypeID,
			NoteText,
			CreatedByUserID,
			CreateDate,
			ChangedByUserID,
			ChangeDate,
			WatchDuration,
			WatchDurationTemporalTypeID,
			WatchCriteriaTypeID,
			WatchLimit,
			WatchLimitMeetsTemporalTypeID,
			WatchRelativeToExpected,
			Favorite,
			WatchEmailIfAlert,
			WatchDuration2,
			WatchDurationTemporalTypeID2,
			WatchCriteriaTypeID2,
			WatchLimit2,
			WatchLimitMeetsTemporalTypeID2,
			WatchRelativeToExpected2
		)
		VALUES
		(
			@modelId, -- ModelID - int
			@modelActionItemPriorityTypeId, -- ModelActionItemPriorityTypeID - int
			--@isDiagnose, -- IsDiagnose - bit
			--@isWatch, -- IsWatch - bit
			--@IsModelMaintenance, -- IsModelMaintenance - bit
			@modelActionItemTypeId, -- ModelActionItemTypeID - int
			@noteText, -- NoteText - nvarchar
			@userId, -- CreatedByUserID - int
			GetDate(), -- CreateDate - datetime
			@userId, -- ChangedByUserID - int
			GetDate(), -- ChangeDate - datetime
			@watchDuration, -- WatchDuration - real
			@watchDurationTemporalTypeId, -- WatchDurationTemporalTypeID - int
			@watchCriteriaTypeId, -- WatchCriteriaTypeID - int
			@watchLimit, -- WatchLimit - real
			@watchLimitMeetsTemporalTypeId, -- WatchLimitMeetsTemporalTypeID - int
			@watchRelativeToExpected, -- WatchRelativeToExpected - bit
			@favorite, -- Favorite - bit
			@watchEmailIfAlert, -- WatchEmailIfAlert - bit
			@watchDuration2, -- WatchDuration2 - real
			@watchDurationTemporalTypeId2, -- WatchDurationTemporalTypeID2 - int
			@watchCriteriaTypeId2, -- WatchCriteriaTypeID2 - int
			@watchLimit2, -- WatchLimit2 - real
			@watchLimitMeetsTemporalTypeId2, -- WatchLimitMeetsTemporalTypeID2 - int
			@watchRelativeToExpected2 -- WatchRelativeToExpected2 - bit

		)
	END
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDModelSummary] TO [TEUser]
    AS [dbo];

