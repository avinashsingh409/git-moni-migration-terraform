﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDAlertNotification] 
	@NDAlertNotificationID UNIQUEIDENTIFIER 
	,@SecurityUserID INT 
AS
BEGIN
	DECLARE @ModelID INT;
	DECLARE @AlertStatusTypeID INT;

	IF EXISTS (SELECT * FROM [Monitoring].[tNDAlertNotification] WHERE NDAlertNotificationID = @NDAlertNotificationID)
	BEGIN
		SELECT TOP 1 @ModelID = [ModelID], @AlertStatusTypeID = [AlertStatusTypeID]
		FROM [Monitoring].[tNDAlertNotification] 
		WHERE NDAlertNotificationID = @NDAlertNotificationID;
			
		IF (Monitoring.ufnDoesUserHaveAccessToNDModel(@ModelID, @SecurityUserID) = 0)
			BEGIN
				RAISERROR( 'User cannot access NDModel',16, 1);
			END
		ELSE
			BEGIN
				IF EXISTS (SELECT * FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID AND AlertStatusTypeID = @AlertStatusTypeID)
				BEGIN
					DELETE FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID AND AlertStatusTypeID = @AlertStatusTypeID
				END

				DELETE FROM [Monitoring].[tNDAlertNotification] WHERE NDAlertNotificationID = @NDAlertNotificationID;
			END
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDAlertNotification] TO [TEUser]
    AS [dbo];

