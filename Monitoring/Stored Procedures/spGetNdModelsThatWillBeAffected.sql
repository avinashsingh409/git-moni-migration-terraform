﻿CREATE   PROCEDURE [Monitoring].[spGetNdModelsThatWillBeAffected]
	-- Add the parameters for the stored procedure here
	@includeAssetIdsAndChildren base.tpIntList READONLY,
	@includeAssetIdsWithoutChildren base.tpIntList READONLY,
	@excludeAssetIdsAndChildren base.tpIntList READONLY,
	@excludeAssetIdsWithoutChildren base.tpIntList READONLY,
	@assetVariableTypeTagMapIdsFromCriteriaLogic base.tpIntList READONLY
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @assetId int;

	DECLARE @allAssetIdsOnBranch TABLE (
	   AssetId int
	)

	DECLARE @excludedAssetIdsOnBranch TABLE (
	   AssetId int
	)
	
	-- Include Assets With Children
	BEGIN	
	   DECLARE cIncludeWithChildren CURSOR FAST_FORWARD FOR 
		  SELECT id as AssetId
		  FROM @includeAssetIdsAndChildren
	   
	   OPEN cIncludeWithChildren
	   FETCH NEXT FROM cIncludeWithChildren INTO @assetId
	   WHILE @@FETCH_STATUS = 0 BEGIN
		  INSERT INTO @allAssetIdsOnBranch(AssetId) 
		  SELECT t.AssetId as AssetID 
		  FROM Asset.ufnGetAssetTreeBranch(@assetId) t
			 LEFT JOIN @allAssetIdsOnBranch aaiob ON t.AssetId = aaiob.AssetId
		  WHERE aaiob.AssetId IS NULL
		  FETCH NEXT FROM cIncludeWithChildren INTO @assetId
	   END
	   CLOSE cIncludeWithChildren
	   DEALLOCATE cIncludeWithChildren
	END

	-- Include Assets Without Children	 
	BEGIN	
	   INSERT INTO @allAssetIdsOnBranch(AssetId)
	   SELECT t.id AS AssetId
	   FROM @includeAssetIdsWithoutChildren t 
		  LEFT JOIN @allAssetIdsOnBranch aaiob ON t.id = aaiob.AssetId
	   WHERE aaiob.AssetId IS NULL
	END

	-- Exclude Assets With Children
	BEGIN	
	   DECLARE cExcludeWithChildren CURSOR FAST_FORWARD FOR
		  SELECT id as AssetId
		  FROM @excludeAssetIdsAndChildren

	   OPEN cExcludeWithChildren
	   FETCH NEXT FROM cExcludeWithChildren INTO @assetId
	   WHILE @@FETCH_STATUS = 0 BEGIN
		  INSERT INTO @excludedAssetIdsOnBranch(AssetId) 
		  SELECT t.AssetId as AssetID 
		  FROM Asset.ufnGetAssetTreeBranch(@assetId) t
			 LEFT JOIN @excludedAssetIdsOnBranch aaiob ON t.AssetId = aaiob.AssetId
		  WHERE aaiob.AssetId IS NULL
		  FETCH NEXT FROM cExcludeWithChildren INTO @assetId
	   END
	   CLOSE cExcludeWithChildren
	   DEALLOCATE cExcludeWithChildren
	END

	-- Exclude Assets Without Children
	BEGIN	
	   INSERT INTO @excludedAssetIdsOnBranch(AssetId)
	   SELECT t.id AS AssetId
	   FROM @excludeAssetIdsWithoutChildren t 
		  LEFT JOIN @excludedAssetIdsOnBranch eaiob ON t.id = eaiob.AssetId
	   WHERE eaiob.AssetId IS NULL
	END


	SELECT DISTINCT ModelID, 
		NDProjectPath, 
		NDProjectName, 
		NDResultsPath, 
		ModelExtID, 
		ModelName, 
		ModelDesc, 
		ModelTemplateID, 
		AssetVariableTypeTagMapID, 
		ModelTypeID, 
		OpModeParamsID, 
		Temporal, 
		Built, 
		BuiltSinceTime, 
		Active, 
		ActiveSinceTime, 
		PreserveAlertStatus, 
		AlertStatusTypeID, 
		IsPublic, 
		DisplayOrder, 
		CreatedByUserID, 
		CreateDate, 
		ChangedByUserID, 
		ChangeDate, 
		BuildInterval, 
		BuildIntervalTemporalTypeID, 
		LastInterrogatedTime, 
		CalcPointID, 
		Release, 
		QueueForBuild,
		AutoRetrain,
		ReferenceModelID,
		Locked,
		LockReasonID,
		LockedSinceTime,
		ModelConfigURL,
		DiagnosticDrilldownURL
	FROM (
	   SELECT tn.*
	   FROM (
		  SELECT aaiob.AssetId
		  FROM @allAssetIdsOnBranch aaiob
		  LEFT JOIN @excludedAssetIdsOnBranch eaiob ON aaiob.AssetId = eaiob.AssetId
		  WHERE eaiob.AssetId IS NULL
	   ) assetsInPlay 
	   INNER JOIN ProcessData.tAssetVariableTypeTagMap tavttm ON assetsInPlay.AssetId = tavttm.AssetId
	   INNER JOIN Monitoring.tNDModel tn ON tn.AssetVariableTypeTagMapID = tavttm.AssetVariableTypeTagMapID
		
	   UNION
		
	   SELECT tn.*
	   FROM Monitoring.tNDModel tn
	   INNER JOIN @assetVariableTypeTagMapIdsFromCriteriaLogic t ON tn.AssetVariableTypeTagMapID = t.id
	) allModels

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNdModelsThatWillBeAffected] TO [TEUser]
    AS [dbo];

