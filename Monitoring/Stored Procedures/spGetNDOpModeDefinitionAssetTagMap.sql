﻿
CREATE PROCEDURE [Monitoring].[spGetNDOpModeDefinitionAssetTagMap]
  (@OpModeDefinitionID int)
AS
BEGIN
  IF @OpModeDefinitionID IS NULL OR @OpModeDefinitionID = 0
    SELECT [OpModeDefinitionAssetTagMapID],
          [OpModeDefinitionID],
          [AssetID],
          [AssetVariableTypeTagMapID],
          [Include],
          [BringDescendants]
    FROM [Monitoring].[tNDOpModeDefinitionAssetTagMap];
  ELSE
    SELECT [OpModeDefinitionAssetTagMapID],
          [OpModeDefinitionID],
          [AssetID],
          [AssetVariableTypeTagMapID],
          [Include],
          [BringDescendants]
    FROM [Monitoring].[tNDOpModeDefinitionAssetTagMap] WHERE [OpModeDefinitionID] = @OpModeDefinitionID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeDefinitionAssetTagMap] TO [TEUser]
    AS [dbo];

