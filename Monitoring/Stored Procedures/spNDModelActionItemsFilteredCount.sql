﻿
CREATE PROCEDURE [Monitoring].[spNDModelActionItemsFilteredCount]
	@securityUserId int,
	@startAssetId int,
	@modelId int,
	@actionItemTypeID Base.tpIntList READONLY,
	@timestampStart datetime = NULL,
	@timestampEnd datetime = NULL,
	@executor NVARCHAR(100) = NULL,
	@noteText NVARCHAR(255) = NULL,
	@issueCreated bit = NULL,
	@issueClosed bit = NULL,
	@favorite bit = NULL
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;
	SET ARITHABORT ON
	
	DECLARE @assets base.tpIntList
	IF @modelId IS NULL
	  BEGIN
	  INSERT INTO @assets SELECT AssetId FROM Asset.ufnGetAssetIdsForUserStartingAt(@securityUserId, @startAssetId);
	  END

	DECLARE @modelArchiveIds TABLE(
		ModelArchiveId int,
		ModelConditionId int
	)
	
	 --Alerts Note
	INSERT INTO @modelArchiveIds (ModelArchiveId, ModelConditionId)
	SELECT
		alertsNoteIds.ModelActionItemArchiveID, null as ModelConditionId
	FROM [Monitoring].[ufnGetFilteredModelActionItemArchiveIDs] (
		@modelId, @actionItemTypeID,
		@timestampStart, @timestampEnd, @executor, @noteText, @assets, @favorite
	) alertsNoteIds;

	--Dashboard Notes
	Insert Into @modelArchiveIds (ModelArchiveId, ModelConditionId)
	Select 
		null as ModelArchivId, dashboardNoteIds.ModelConditionNoteID 
	From [Monitoring].[ufnGetFilteredModelConditionNoteIDs] (
		@modelId, @actionItemTypeID,
		@timestampStart, @timestampEnd, @executor, @noteText, @assets			
	) dashboardNoteIds;

	Select Count(*) From @modelArchiveIds
END
Go
GRANT EXECUTE
    On Object::[Monitoring].[spNDModelActionItemsFilteredCount] To [TEUser]
    As [dbo];
