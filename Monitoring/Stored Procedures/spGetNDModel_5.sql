﻿CREATE PROCEDURE [Monitoring].[spGetNDModel]
  (@ModelID int = NULL)
AS
BEGIN
  IF @ModelID IS NULL OR @ModelID = 0
    SELECT [ModelID],
		  [NDProjectPath],
		  [NDProjectName],
		  [NDResultsPath],
          [ModelExtID],
          [ModelName],
          [ModelDesc],
          [ModelTemplateID],
          [AssetVariableTypeTagMapID],
          [ModelTypeID],
          [OpModeParamsID],
          [Temporal],
          [Built],
          [BuiltSinceTime],
          [Active],
          [ActiveSinceTime],
          [PreserveAlertStatus],
          [AlertStatusTypeID],
          [IsPublic],
          [DisplayOrder],
          [CreatedByUserID],
          [CreateDate],
          [ChangedByUserID],
          [ChangeDate],
          [BuildInterval],
          [BuildIntervalTemporalTypeID],
          [LastInterrogatedTime],
		  [CalcPointID],
		  [Release],
		  [QueueForBuild],
		  [AutoRetrain],
		  [ReferenceModelID],
		  [Locked],
		  [LockReasonID],
		  [LockedSinceTime],
		  [ModelConfigURL],
		  [DiagnosticDrilldownURL]
    FROM [Monitoring].[tNDModel];
  ELSE
    SELECT [ModelID],
		  [NDProjectPath],
		  [NDProjectName],
		  [NDResultsPath],
          [ModelExtID],
          [ModelName],
          [ModelDesc],
          [ModelTemplateID],
          [AssetVariableTypeTagMapID],
          [ModelTypeID],
          [OpModeParamsID],
          [Temporal],
          [Built],
          [BuiltSinceTime],
          [Active],
          [ActiveSinceTime],
          [PreserveAlertStatus],
          [AlertStatusTypeID],
          [IsPublic],
          [DisplayOrder],
          [CreatedByUserID],
          [CreateDate],
          [ChangedByUserID],
          [ChangeDate],
          [BuildInterval],
          [BuildIntervalTemporalTypeID],
          [LastInterrogatedTime],
		  [CalcPointID],
		  [Release],
		  [QueueForBuild],
		  [AutoRetrain],
		  [ReferenceModelID],
		  [Locked],
		  [LockReasonID],
		  [LockedSinceTime],
		  [ModelConfigURL],
		  [DiagnosticDrilldownURL]
    FROM [Monitoring].[tNDModel] WHERE [ModelID] = @ModelID;
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModel] TO [TEUser]
    AS [dbo];
GO

