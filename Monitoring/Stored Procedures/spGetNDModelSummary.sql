﻿CREATE PROCEDURE [Monitoring].[spGetNDModelSummary]
				@modelId int,
				@userId int
			AS
			BEGIN
				SET NOCOUNT ON;
				SELECT mdl.ModelID, 
					mdl.Active, 
					tag.TagName, 
					tag.TagDesc, 
					mdl.ModelName, 
					mdl.ModelTypeID, 
					modeltype.ModelTypeAbbrev, 
					predmap.Score, 
					tag.EngUnits, 
					ast.AssetAbbrev, 
					unit.AssetAbbrev UnitAbbrev,
					unit.AssetID UnitID,
					ast.AssetID, 
					params.MAELower, 
					params.MAEUpper, 
					tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
					tnst.AlertStatusPriorityTypeID AS AlertPriority,
					tmaicur.IsDiagnoseSet AS ActionDiagnose,
					tmaicur.IsWatchSet AS ActionWatch,
					tmaicur.IsModelMaintenanceSet AS ActionModelMaintenance,
					issues.has_issue AS HasOpenIssue,
					tmasfn.PercentOOB AS PercentOutOfBounds,
					tmasfn.UpperLimit AS UpperLimit,
					tmasfn.LowerLimit AS LowerLimit,
					tmasfn.Actual AS Actual,
					tmasfn.Expected AS Expected,
					mdl.ActiveSinceTime AS AddedToAlerts,
					tmai.ModelActionItemPriorityTypeID as ModelActionItemPriorityTypeID,
					tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
					tmai.NoteText AS LastNote,
					tmai.CreateDate AS LastNoteDate,
					tmas.TagGroup AS TagGroup,
					CASE WHEN tmasfn.PercentOOB <> 0 THEN 1 ELSE 0 END AS OutOfBounds,
					tmas.IgnoreStatus AS Ignore,
					tnst.AlertStatusTypeAbbrev AS StatusInfo,
					CAST(CASE WHEN mdl.ModelTemplateID IS NOT NULL AND mdl.ModelTemplateID > 0 THEN 1 ELSE 0 END AS BIT) AS IsStandard
				FROM Monitoring.tNDModel mdl
					JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
					JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
					JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
					JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
					LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
					JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
					LEFT JOIN Asset.tAsset unit
							ON unit.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(map.AssetID,'UN')
					LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
					LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
					OUTER APPLY Monitoring.ufnModelCurrentActionItemStatus (mdl.ModelID) tmaicur
					LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
					LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
					LEFT JOIN Asset.tUnit tu ON map.AssetID = tu.AssetID
					LEFT JOIN (SELECT Diagnostics.tAssetIssue.AssetID,CAST(1 AS BIT) has_issue, COUNT(*) issue_cnt FROM Diagnostics.tAssetIssue WHERE Diagnostics.tAssetIssue.ActivityStatusID = 1 AND Diagnostics.tAssetIssue.IsHidden=0 GROUP BY Diagnostics.tAssetIssue.AssetID) issues ON issues.AssetID = map.AssetID
				WHERE mdl.ModelID = @modelID AND Asset.ufnDoesUserHaveAccessToAsset(map.AssetID,@userID,-1)	= 1
			END
		
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelSummary] TO [TEUser]
    AS [dbo];

