﻿
CREATE PROCEDURE [Monitoring].[spGetNDModelType]
  (@ModelTypeID int = NULL)
AS
BEGIN
  IF @ModelTypeID IS NULL OR @ModelTypeID = 0
    SELECT [ModelTypeID],
          [ModelTypeAbbrev],
          [ModelTypeDesc],
          [ModelTypeEdictID],
          [DisplayOrder]
    FROM [Monitoring].[tNDModelType];
  ELSE
    SELECT [ModelTypeID],
          [ModelTypeAbbrev],
          [ModelTypeDesc],
          [ModelTypeEdictID],
          [DisplayOrder]
    FROM [Monitoring].[tNDModelType] WHERE [ModelTypeID] = @ModelTypeID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelType] TO [TEUser]
    AS [dbo];

