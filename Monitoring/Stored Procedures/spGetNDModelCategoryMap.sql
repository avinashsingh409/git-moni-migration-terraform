CREATE PROCEDURE [Monitoring].[spGetNDModelCategoryMap]
  @ModelID int = NULL
AS
BEGIN
    SELECT [CategoryMapID],
          [ModelID],
          [CategoryID]
    FROM [Monitoring].[tNDModelCategoryMap]
	WHERE [ModelID] = @ModelID
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelCategoryMap] TO [TEUser]
    AS [dbo];

