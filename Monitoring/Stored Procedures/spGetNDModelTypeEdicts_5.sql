﻿
CREATE PROCEDURE [Monitoring].[spGetNDModelTypeEdicts]
  (@ModelTypeEdictID int = NULL)
AS
BEGIN
  IF @ModelTypeEdictID IS NULL OR @ModelTypeEdictID = 0
    SELECT [ModelTypeEdictID],
          [InputsAvailable],
          [AltAreaAvailable],
          [AltFrequencyAvailable],
          [AltOscillationAvailable],
          [AltHighHighAvailable],
          [AltLowLowAvailable],
          [AltFrozenDataAvailable],
          [AnoMAEAvailable],
          [AnoBiasAvailable],
          [AnoFixedLimitAvailable],
          [DataAvailable],
          [MustHaveAnoMAEOrAnoBias],
          [MustHaveAnoFixedLimit],
          [MustHaveAltFrozenData],
          [PredictiveMethodTypeID]
    FROM [Monitoring].[tNDModelTypeEdicts];
  ELSE
    SELECT [ModelTypeEdictID],
          [InputsAvailable],
          [AltAreaAvailable],
          [AltFrequencyAvailable],
          [AltOscillationAvailable],
          [AltHighHighAvailable],
          [AltLowLowAvailable],
          [AltFrozenDataAvailable],
          [AnoMAEAvailable],
          [AnoBiasAvailable],
          [AnoFixedLimitAvailable],
          [DataAvailable],
          [MustHaveAnoMAEOrAnoBias],
          [MustHaveAnoFixedLimit],
          [MustHaveAltFrozenData],
          [PredictiveMethodTypeID]
    FROM [Monitoring].[tNDModelTypeEdicts] WHERE [ModelTypeEdictID] = @ModelTypeEdictID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelTypeEdicts] TO [TEUser]
    AS [dbo];

