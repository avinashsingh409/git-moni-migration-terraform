﻿
CREATE PROCEDURE [Monitoring].[spUpdateModelConditionStates]
    @TVP Monitoring.tpModelConditionState READONLY
    AS 
	UPDATE Monitoring.tModelCondition SET
		[Timestamp] = S.[Timestamp],
		RSquared = S.RSquared,
		ActualOutOfBounds_percent = S.ActualOutOfBounds_percent,
		StateChanged = S.StateChanged,
		BeganAlertingTimestamp = CASE WHEN S.NewAlert=1 THEN GETDATE() ELSE T.BeganAlertingTimestamp END,
		ConditionLastActive = S.ConditionLastActive,
		ConditionLastClear = S.ConditionLastClear
	FROM Monitoring.tModelCondition T
	INNER JOIN @TVP S ON S.ModelConditionID = T.ModelConditionID
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpdateModelConditionStates] TO [TEUser]
    AS [dbo];

