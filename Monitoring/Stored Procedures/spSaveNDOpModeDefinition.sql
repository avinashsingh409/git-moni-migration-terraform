﻿
CREATE PROCEDURE [Monitoring].[spSaveNDOpModeDefinition]
  @OpModeDefinitionID int = NULL,
   @NDProjectPath nvarchar(400),
   @NDProjectName nvarchar(255),
   @OpModeDefinitionExtID nvarchar(255),
   @OpModeDefinitionTitle nvarchar(255),
   @OpModeTypeID int,
   @CreatedByUserID int,
   @CreateDate datetime,
   @ChangedByUserID int,
   @ChangeDate datetime
AS
BEGIN
  IF @OpModeDefinitionID IS NULL OR @OpModeDefinitionID = 0
    BEGIN
	  DECLARE @newExtID nvarchar(255);
	  SET @newExtID = CONVERT(nvarchar(36), NEWID());
      INSERT INTO [Monitoring].[tNDOpModeDefinition]
        ([NDProjectPath],
         [NDProjectName],
         [OpModeDefinitionExtID],
         [OpModeDefinitionTitle],
         [OpModeTypeID],
         [CreatedByUserID],
         [CreateDate],
         [ChangedByUserID],
         [ChangeDate])
      VALUES
        (@NDProjectPath,
         @NDProjectName,
         @newExtID,
         @OpModeDefinitionTitle,
         @OpModeTypeID,
         @CreatedByUserID,
         @CreateDate,
         @ChangedByUserID,
         @ChangeDate);
	  
	  SELECT CAST(SCOPE_IDENTITY() as int);
    END
  ELSE
    BEGIN
      UPDATE [Monitoring].[tNDOpModeDefinition]
        SET [NDProjectPath] = @NDProjectPath,
            [NDProjectName] = @NDProjectName,
            [OpModeDefinitionExtID] = @OpModeDefinitionExtID,
            [OpModeDefinitionTitle] = @OpModeDefinitionTitle,
            [OpModeTypeID] = @OpModeTypeID,
            [CreatedByUserID] = @CreatedByUserID,
            [CreateDate] = @CreateDate,
            [ChangedByUserID] = @ChangedByUserID,
            [ChangeDate] = @ChangeDate
        WHERE [OpModeDefinitionID] = @OpModeDefinitionID;

	  SELECT @OpModeDefinitionID;
    END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveNDOpModeDefinition] TO [TEUser]
    AS [dbo];

