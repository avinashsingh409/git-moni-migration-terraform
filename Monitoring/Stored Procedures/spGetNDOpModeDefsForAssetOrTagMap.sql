﻿CREATE PROCEDURE [Monitoring].[spGetNDOpModeDefsForAssetOrTagMap]
	@securityUserID int,
	@assetID int,
	@opModeTypeID int = null,
	@opModeDefinitionID int = null,
	@astVarTypeTagMapID int = null,
	@returnMeshOrExcludes int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @covereds TABLE (
		OpModeDefinitionID int,
		AssetVariableTypeTagMapID int
	)
	INSERT INTO @covereds EXEC Monitoring.spGetAstVarTypeTagMapIDsCoveredByNdOpModeDef @securityUserID, @opModeTypeID, @opModeDefinitionID, @astVarTypeTagMapID, @returnMeshOrExcludes

	SELECT DISTINCT cov.OpModeDefinitionID
	FROM @covereds cov
	JOIN ProcessData.tAssetVariableTypeTagMap map ON cov.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
	JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
	JOIN Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @assetID) br ON map.AssetID = br.AssetID
	WHERE
		tag.ExistsOnServer = 1
		AND map.VariableTypeID IS NOT NULL
		AND map.TagID IS NOT NULL

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeDefsForAssetOrTagMap] TO [TEUser]
    AS [dbo];

