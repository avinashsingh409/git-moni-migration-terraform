﻿




CREATE PROCEDURE [Monitoring].[spInsertNDModelBuilds]
      @tblNDModelBuild Monitoring.tpNDModelBuild READONLY
AS
BEGIN

       SET NOCOUNT ON;            
	   declare @now as DateTime = getdate()

	   INSERT INTO Monitoring.tNDModelBuild (
                     ModelExtID
                     , BuildStatusTypeID
                     , BuildStatus
                     , BuildStatusUserID
                     , BuildStatusTime
              )
		SELECT  ModelExtID
                     , BuildStatusTypeID
                     , BuildStatus
                     , BuildStatusUserID
                     , isnull(BuildStatusTime,@now)
					 FROM @tblNDModelBuild              
       

END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spInsertNDModelBuilds] TO [TEUser]
    AS [dbo];

