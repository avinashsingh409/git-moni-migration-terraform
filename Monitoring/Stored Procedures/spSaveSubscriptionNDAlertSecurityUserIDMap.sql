﻿
CREATE PROCEDURE [Monitoring].[spSaveSubscriptionNDAlertSecurityUserIDMap] 
	 @SecurityUserID INT 
	,@ModelID INT 
	,@AlertStatusTypeID INT 
	,@UserID INT
    ,@NewID UNIQUEIDENTIFIER OUTPUT
AS
BEGIN

	IF (Monitoring.ufnDoesUserHaveAccessToNDModel(@ModelID, @SecurityUserID) = 0)
		BEGIN
			RAISERROR( 'User cannot access NDModel',16, 1);
		END
	ELSE
		BEGIN
			IF NOT EXISTS (SELECT * FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID 
			AND AlertStatusTypeID = @AlertStatusTypeID AND SecurityUserID = @UserID)
			BEGIN
				DECLARE @TmpTable TABLE (SubscriptionNDAlertSecurityUserIDMapID uniqueidentifier);
				INSERT INTO [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap]([ModelID],[AlertStatusTypeID],[SecurityUserID])
				OUTPUT INSERTED.SubscriptionNDAlertSecurityUserIDMapID INTO @TmpTable
				VALUES (@ModelID, @AlertStatusTypeID, @UserID)
				SELECT @NewID = SubscriptionNDAlertSecurityUserIDMapID FROM @TmpTable;
			END
			ELSE
			BEGIN
				SELECT TOP 1 @NewID = SubscriptionNDAlertSecurityUserIDMapID FROM [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] WHERE ModelID = @ModelID 
				AND AlertStatusTypeID = @AlertStatusTypeID AND SecurityUserID = @UserID
			END
		END
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spSaveSubscriptionNDAlertSecurityUserIDMap] TO [TEUser]
    AS [dbo];

