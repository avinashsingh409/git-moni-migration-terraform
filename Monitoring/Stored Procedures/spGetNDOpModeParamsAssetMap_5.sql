﻿
CREATE PROCEDURE [Monitoring].[spGetNDOpModeParamsAssetMap]
  (@OpModeParamsAssetMapID int = NULL)
AS
BEGIN
  IF @OpModeParamsAssetMapID IS NULL OR @OpModeParamsAssetMapID = 0
    SELECT [OpModeParamsAssetMapID],
          [OpModeTypeID],
          [OpModeParamsID],
          [AssetID]
    FROM [Monitoring].[tNDOpModeParamsAssetMap];
  ELSE
    SELECT [OpModeParamsAssetMapID],
          [OpModeTypeID],
          [OpModeParamsID],
          [AssetID]
    FROM [Monitoring].[tNDOpModeParamsAssetMap] WHERE [OpModeParamsAssetMapID] = @OpModeParamsAssetMapID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeParamsAssetMap] TO [TEUser]
    AS [dbo];

