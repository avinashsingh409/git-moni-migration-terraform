﻿
CREATE PROCEDURE [Monitoring].[spGetNDModelTemplateOpModeMap]
  (@ModelTemplateOpModeMapID int = NULL)
AS
BEGIN
  IF @ModelTemplateOpModeMapID IS NULL OR @ModelTemplateOpModeMapID = 0
    SELECT [ModelTemplateOpModeMapID],
          [ModelTemplateID],
          [OpModeTypeID]
    FROM [Monitoring].[tNDModelTemplateOpModeMap];
  ELSE
    SELECT [ModelTemplateOpModeMapID],
          [ModelTemplateID],
          [OpModeTypeID]
    FROM [Monitoring].[tNDModelTemplateOpModeMap] WHERE [ModelTemplateOpModeMapID] = @ModelTemplateOpModeMapID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelTemplateOpModeMap] TO [TEUser]
    AS [dbo];

