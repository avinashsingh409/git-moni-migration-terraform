﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDOpModeParams]
	(@OpModeParamsID int)
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [Monitoring].[tNDOpModeParams] WHERE [OpModeParamsID]=@OpModeParamsID;
	SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDOpModeParams] TO [TEUser]
    AS [dbo];

