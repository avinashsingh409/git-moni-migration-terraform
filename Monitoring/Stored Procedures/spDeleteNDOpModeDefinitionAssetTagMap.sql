﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDOpModeDefinitionAssetTagMap]
  (@OpModeDefinitionAssetTagMapID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDOpModeDefinitionAssetTagMap] WHERE [OpModeDefinitionAssetTagMapID]=@OpModeDefinitionAssetTagMapID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDOpModeDefinitionAssetTagMap] TO [TEUser]
    AS [dbo];

