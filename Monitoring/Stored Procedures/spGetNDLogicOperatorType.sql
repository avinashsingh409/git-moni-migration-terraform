﻿
CREATE PROCEDURE [Monitoring].[spGetNDLogicOperatorType]
  (@LogicOperatorTypeID int = NULL)
AS
BEGIN
  IF @LogicOperatorTypeID IS NULL OR @LogicOperatorTypeID = 0
    SELECT [LogicOperatorTypeID],
          [LogicOperatorTypeAbbrev],
          [LogicOperatorTypeDesc],
          [LogicOperatorCategoryTypeID],
          [DisplayOrder]
    FROM [Monitoring].[tNDLogicOperatorType];
  ELSE
    SELECT [LogicOperatorTypeID],
          [LogicOperatorTypeAbbrev],
          [LogicOperatorTypeDesc],
          [LogicOperatorCategoryTypeID],
          [DisplayOrder]
    FROM [Monitoring].[tNDLogicOperatorType] WHERE [LogicOperatorTypeID] = @LogicOperatorTypeID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDLogicOperatorType] TO [TEUser]
    AS [dbo];

