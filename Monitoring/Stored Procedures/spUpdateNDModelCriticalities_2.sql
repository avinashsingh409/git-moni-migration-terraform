﻿





-- update procedure
create procedure [Monitoring].[spUpdateNDModelCriticalities]
	@tblCriticalities Monitoring.tpNDModelCriticality readonly,
	@UserSecId int
as
begin

	set nocount on;

	declare @errorMsg varchar(max);
	declare @userId int;

	-- need a valid user
	set @userId = (select SecurityUserID from AccessControl.tUser where SecurityUserID = @UserSecId);
	if @userId is null
	begin
		set @errorMsg = 'Invalid user';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- user must have access to all existing models' dependent variable assets
	declare @criticalitiesCount int = (select count (ModelID) from @tblCriticalities);
	declare @accessError varchar(max) = 'User does not have permission to all models';
	if @criticalitiesCount < 1000
	begin
		if exists (
			select m.ModelID from Monitoring.tNDModel m
			join ProcessData.tAssetVariableTypeTagMap map on m.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
			join @tblCriticalities c on m.ModelID = c.ModelID
			where Asset.ufnDoesUserHaveAccessToAsset (map.AssetID, @userId, -1) = 0
		)
		begin
			set @errorMsg = @accessError;
		end
	end
	else
	begin
		declare @assets Base.tpIntList;
		insert into @assets (id)
		select map.AssetID
		from Monitoring.tNDModel m
		join ProcessData.tAssetVariableTypeTagMap map on m.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
		join @tblCriticalities c on m.ModelID = c.ModelID

		if exists (
			select id from @assets
			except
			select AssetID from Asset.ufnUserHasAccessToAssets(@userId, @assets)
		)
		begin
			set @errorMsg = @accessError;
		end
	end
	if @errorMsg <> ''
	begin
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- every criticality must already have an entry in tNDModelCriticality;
	-- the trigger on tNDModel is responsible for creating entries
	if exists (
		select ModelID from @tblCriticalities
		except
		select e.ModelID from Monitoring.tNDModelCriticality e join @tblCriticalities c on e.ModelID = c.ModelID
	)
	begin
		set @errorMsg = 'spUpdateNDModelCriticalities disallows addition of new Criticalities';
		raiserror (@errorMsg, 11, -1, -1);
		return;
	end

	-- standard models must use default
	-- need a local table since @tblCriticalities is readonly
	declare @myCriticalities Monitoring.tpNDModelCriticality
	insert into @myCriticalities
	select
		ModelID, UseDefault, UpperCriticality, LowerCriticality, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate
	from @tblCriticalities;
	
	-- Removed the update statement that was updating the criticality values to NULL if associated with a template

	-- update
	declare @now datetime = getdate();
	update e
	set
		e.UseDefault = c.UseDefault
		, e.UpperCriticality = c.UpperCriticality
		, e.LowerCriticality = c.LowerCriticality
		, e.ChangedByUserID = @userId
		, e.ChangeDate = @now
	from Monitoring.tNDModelCriticality e
	join @myCriticalities c on e.ModelID = c.ModelID;

	select
		e.ModelID
		, e.UseDefault
		, e.UpperCriticality
		, e.LowerCriticality
		, r.Username as CreatedByUsername
		, e.CreateDate
		, h.Username as ChangedByUsername
		, e.ChangeDate
	from Monitoring.tNDModelCriticality e
	join @tblCriticalities c on e.ModelID = c.ModelID
	join AccessControl.tUser r on e.CreatedByUserID = r.SecurityUserID
	join AccessControl.tUser h on e.ChangedByUserID = h.SecurityUserID
	;

end
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpdateNDModelCriticalities] TO [TEUser]
    AS [dbo];
