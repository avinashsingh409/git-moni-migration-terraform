﻿
CREATE PROCEDURE [Monitoring].[spGetNDModelPredictiveMethodMap]
  (@modelID int = NULL)
AS
BEGIN
  IF @modelID IS NULL OR @modelID = 0
    SELECT [ModelPredictiveMethodMapID],
          [ModelID],
          [PredictiveMethodTypeID],
          [Score],
		  [RValue],
		  [RMSE],
		  [MAE],
          [Active],
		  [CommonTracesID],
		  [CommonTracesFastID],
		  [IQR]
    FROM [Monitoring].[tNDModelPredictiveMethodMap];
  ELSE
    SELECT [ModelPredictiveMethodMapID],
          [ModelID],
          [PredictiveMethodTypeID],
          [Score],
		  [RValue],
		  [RMSE],
		  [MAE],
          [Active],
		  [CommonTracesID],
		  [CommonTracesFastID],
		  [IQR]
    FROM [Monitoring].[tNDModelPredictiveMethodMap] WHERE [ModelID] = @modelID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelPredictiveMethodMap] TO [TEUser]
    AS [dbo];

