
CREATE PROCEDURE [Monitoring].[spUpdateModelConditions]
    @TVP Monitoring.tpModelCondition READONLY
    AS 
	UPDATE Monitoring.tModelCondition SET
		ConditionTimeBasis = S.ConditionTimeBasis,
		ConditionOutOfBounds_percent = S.ConditionOutOfBounds_percent,
		ConditionLastActive = S.ConditionLastActive,
		IsKeepConditionActive = S.IsKeepConditionActive,
		KeepConditionActiveTimestamp = S.KeepConditionActiveTimestamp,
		[Timestamp] = S.[Timestamp],
		ActualOutOfBounds_percent = S.ActualOutOfBounds_percent,
		ConditionLastClear = S.ConditionLastClear,
		StateChanged = S.StateChanged,
		IgnoreUntil = S.IgnoreUntil,
		OutDirection = S.OutDirection,
		RSquared = S.RSquared,
		BeganAlertingTimestamp = S.BeganAlertingTimestamp
	FROM Monitoring.tModelCondition T
	INNER JOIN @TVP S ON S.ModelConditionID = T.ModelConditionID
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spUpdateModelConditions] TO [TEUser]
    AS [dbo];

