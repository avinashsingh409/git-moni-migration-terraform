﻿
CREATE PROCEDURE [Monitoring].[spDeleteNDOpModeDefinition]
  (@OpModeDefinitionID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDOpModeDefinition] WHERE [OpModeDefinitionID]=@OpModeDefinitionID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDOpModeDefinition] TO [TEUser]
    AS [dbo];

