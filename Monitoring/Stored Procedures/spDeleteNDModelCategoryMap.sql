CREATE PROCEDURE [Monitoring].[spDeleteNDModelCategoryMap]
  @NDModelCategoryMapID int
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Monitoring].[tNDModelCategoryMap] WHERE [CategoryMapID]=@NDModelCategoryMapID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spDeleteNDModelCategoryMap] TO [TEUser]
    AS [dbo];

