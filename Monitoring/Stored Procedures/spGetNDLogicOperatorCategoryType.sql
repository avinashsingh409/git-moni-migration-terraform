﻿
CREATE PROCEDURE [Monitoring].[spGetNDLogicOperatorCategoryType]
  (@LogicOperatorCategoryTypeID int = NULL)
AS
BEGIN
  IF @LogicOperatorCategoryTypeID IS NULL OR @LogicOperatorCategoryTypeID = 0
    SELECT [LogicOperatorCategoryTypeID],
          [LogicOperatorCategoryTypeAbbrev],
          [LogicOperatorCategoryTypeDesc],
          [LogicOperatorCategoryTypeLabel],
          [DisplayOrder]
    FROM [Monitoring].[tNDLogicOperatorCategoryType];
  ELSE
    SELECT [LogicOperatorCategoryTypeID],
          [LogicOperatorCategoryTypeAbbrev],
          [LogicOperatorCategoryTypeDesc],
          [LogicOperatorCategoryTypeLabel],
          [DisplayOrder]
    FROM [Monitoring].[tNDLogicOperatorCategoryType] WHERE [LogicOperatorCategoryTypeID] = @LogicOperatorCategoryTypeID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDLogicOperatorCategoryType] TO [TEUser]
    AS [dbo];

