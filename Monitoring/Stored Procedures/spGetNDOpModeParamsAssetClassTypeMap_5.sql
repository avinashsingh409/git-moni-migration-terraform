﻿
CREATE PROCEDURE [Monitoring].[spGetNDOpModeParamsAssetClassTypeMap]
  (@OpModeParamsAssetClassTypeMapID int = NULL)
AS
BEGIN
  IF @OpModeParamsAssetClassTypeMapID IS NULL OR @OpModeParamsAssetClassTypeMapID = 0
    SELECT [OpModeParamsAssetClassTypeMapID],
          [OpModeTypeID],
          [OpModeParamsID],
          [AssetClassTypeID]
    FROM [Monitoring].[tNDOpModeParamsAssetClassTypeMap];
  ELSE
    SELECT [OpModeParamsAssetClassTypeMapID],
          [OpModeTypeID],
          [OpModeParamsID],
          [AssetClassTypeID]
    FROM [Monitoring].[tNDOpModeParamsAssetClassTypeMap] WHERE [OpModeParamsAssetClassTypeMapID] = @OpModeParamsAssetClassTypeMapID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDOpModeParamsAssetClassTypeMap] TO [TEUser]
    AS [dbo];

