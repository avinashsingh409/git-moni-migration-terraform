﻿CREATE   PROCEDURE [Monitoring].[spRebuildNdModelsDependentOnOpMode]
	@opModeDefinitionId int,
	@queueForBuild bit = 1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @includeAssetIdsAndChildren base.tpIntList
	DECLARE @includeAssetIdsWithoutChildren base.tpIntList 
	DECLARE @excludeAssetIdsAndChildren base.tpIntList 
	DECLARE @excludeAssetIdsWithoutChildren base.tpIntList 
	DECLARE @assetVariableTypeTagMapIdsFromCriteriaLogic base.tpIntList

	INSERT INTO @includeAssetIdsAndChildren 
		SELECT DISTINCT AssetId
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap tnmdatm
		WHERE tnmdatm.OpModeDefinitionID = @opModeDefinitionId 
		AND tnmdatm.[INCLUDE] = 1
		AND tnmdatm.[BringDescendants] = 1
		AND tnmdatm.AssetID IS NOT NULL
		
	INSERT INTO @includeAssetIdsWithoutChildren 
		SELECT DISTINCT AssetId
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap tnmdatm
		WHERE tnmdatm.OpModeDefinitionID = @opModeDefinitionId 
		AND tnmdatm.[INCLUDE] = 1
		AND tnmdatm.[BringDescendants] = 0
		AND tnmdatm.AssetID IS NOT NULL
	
	INSERT INTO @excludeAssetIdsWithoutChildren 
		SELECT DISTINCT AssetId
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap tnmdatm
		WHERE tnmdatm.OpModeDefinitionID = @opModeDefinitionId 
		AND tnmdatm.[INCLUDE] = 0
		AND tnmdatm.[BringDescendants] = 1
		AND tnmdatm.AssetID IS NOT NULL
	
	INSERT INTO @includeAssetIdsWithoutChildren 
		SELECT DISTINCT AssetId
		FROM Monitoring.tNDOpModeDefinitionAssetTagMap tnmdatm
		WHERE tnmdatm.OpModeDefinitionID = @opModeDefinitionId 
		AND tnmdatm.[INCLUDE] = 0
		AND tnmdatm.[BringDescendants] = 0
		AND tnmdatm.AssetID IS NOT NULL

	INSERT INTO @assetVariableTypeTagMapIdsFromCriteriaLogic
		SELECT DISTINCT AssetVariableTypeTagMapID 
		FROM Monitoring.tNDCriteriaGroup tng
		INNER JOIN Monitoring.tNDCriteriaLogic tnl ON tnl.CriteriaGroupID = tng.CriteriaGroupID
		WHERE tng.OpModeDefinitionID = @opModeDefinitionId
	
	DECLARE @tAffectedModel TABLE(
		[ModelID] [int],
		[NDProjectPath] [nvarchar](400) ,
		[NDProjectName] [nvarchar](255) ,
		[NDResultsPath] [nvarchar](400) ,
		[ModelExtID] [nvarchar](255) ,
		[ModelName] [nvarchar](255) ,
		[ModelDesc] [nvarchar](255) ,
		[ModelTemplateID] [int] ,
		[AssetVariableTypeTagMapID] [int] ,
		[ModelTypeID] [int] ,
		[OpModeParamsID] [int] ,
		[Temporal] [bit] ,
		[Built] [bit] ,
		[BuiltSinceTime] [datetime] ,
		[Active] [bit] ,
		[ActiveSinceTime] [datetime] ,
		[PreserveAlertStatus] [bit] ,
		[AlertStatusTypeID] [int] ,
		[IsPublic] [bit] ,
		[DisplayOrder] [int] ,
		[CreatedByUserID] [int] ,
		[CreateDate] [datetime] ,
		[ChangedByUserID] [int] ,
		[ChangeDate] [datetime] ,
		[BuildInterval] [int] ,
		[BuildIntervalTemporalTypeID] [int] ,
		[LastInterrogatedTime] [datetime] ,
		[CalcPointID] [int] ,
		[Release] [bit] ,
		[QueueForBuild] [bit],
		[AutoRetrain] [bit],
		[ReferenceModelID] [nvarchar](400),
		[Locked] [bit],
		[LockReasonID] [int],
		[LockedSinceTime] [datetime],
		[ModelConfigURL] [varchar](2048),
		[DiagnosticDrilldownURL] [varchar](2048)
	)

	INSERT INTO @tAffectedModel
	(
		ModelID,
		NDProjectPath,
		NDProjectName,
		NDResultsPath,
		ModelExtID,
		ModelName,
		ModelDesc,
		ModelTemplateID,
		AssetVariableTypeTagMapID,
		ModelTypeID,
		OpModeParamsID,
		Temporal,
		Built,
		BuiltSinceTime,
		Active,
		ActiveSinceTime,
		PreserveAlertStatus,
		AlertStatusTypeID,
		IsPublic,
		DisplayOrder,
		CreatedByUserID,
		CreateDate,
		ChangedByUserID,
		ChangeDate,
		BuildInterval,
		BuildIntervalTemporalTypeID,
		LastInterrogatedTime,
		CalcPointID,
		Release,
		QueueForBuild,
		AutoRetrain,
		ReferenceModelID,
		Locked,
		LockReasonID,
		LockedSinceTime,
		ModelConfigURL,
		DiagnosticDrilldownURL
	)
	EXEC [Monitoring].[spGetNdModelsThatWillBeAffected] 
		@includeAssetIdsAndChildren = @includeAssetIdsAndChildren ,
		@includeAssetIdsWithoutChildren = @includeAssetIdsWithoutChildren,
		@excludeAssetIdsAndChildren = @excludeAssetIdsAndChildren,
		@excludeAssetIdsWithoutChildren = @excludeAssetIdsWithoutChildren,
		@assetVariableTypeTagMapIdsFromCriteriaLogic = @assetVariableTypeTagMapIdsFromCriteriaLogic 
	
	UPDATE Monitoring.tNDModel
	SET Monitoring.tNDModel.QueueForBuild = @queueForBuild
	FROM Monitoring.tNDModel tn
	INNER JOIN @tAffectedModel tam ON tn.ModelID = tam.ModelID
	
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[spRebuildNdModelsDependentOnOpMode] TO [TEUser]
    AS [dbo];

