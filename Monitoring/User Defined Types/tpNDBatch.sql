﻿CREATE TYPE [Monitoring].[tpNDBatch] AS TABLE (
    [Batch]       INT              NULL,
    [BatchId]     UNIQUEIDENTIFIER NOT NULL,
    [BatchTypeId] INT              NOT NULL,
    [Username]    NVARCHAR (50)    NOT NULL,
    [BatchDate]   DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([BatchId] ASC));
GO

GRANT EXEC ON TYPE::[Monitoring].[tpNDBatch] TO [TEUser]
GO