﻿CREATE TYPE [Monitoring].[tpModelSnapshot] AS TABLE (
    [ModelSnapshotID] INT           NULL,
    [ModelID]         INT           NULL,
    [Timestamp]       DATETIME      NULL,
    [ActualValue]     REAL          NULL,
    [ExpectedValue]   REAL          NULL,
    [UpperLimit]      REAL          NULL,
    [LowerLimit]      REAL          NULL,
    [LastAnomaly]     DATETIME      NULL,
    [StateChanged]    BIT           NULL,
    [Status]          TINYINT       NULL,
    [StatusInfo]      VARCHAR (255) NULL);




GO
GRANT EXECUTE
    ON TYPE::[Monitoring].[tpModelSnapshot] TO [TEUser];



