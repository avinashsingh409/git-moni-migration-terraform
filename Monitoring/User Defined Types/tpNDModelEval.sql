﻿
CREATE TYPE [Monitoring].[tpNDModelEval] AS TABLE(
	[modelID] [int] NULL,
	[nDAlert] [int] NULL,
	[alert] [bit] NULL,
	[nDActual] [real] NULL,
	[nDExpected] [real] NULL,
	[nDUpper] [real] NULL,
	[nDLower] [real] NULL,
	[percentOOB] [real] NULL,
	[hiHiAlert] [bit] NULL,
	[lowLowAlert] [bit] NULL,
	[areaFastResponseAlert] [bit] NULL,
	[areaSlowResponseAlert] [bit] NULL,
	[frequencyAlert] [bit] NULL,
	[oscillationAlert] [bit] NULL,
	[FrozenDataAlert] [bit] NULL,
	[IgnoreOverrideAlert] [bit] NULL,
	[batchEvaluationStartTime] [datetime] NULL,
	[evaluationError] [bit] NULL,
	[evaluationErrorMessage] [nvarchar](max) NULL,
	[watchOverrideAlert] [bit] NULL,
	[serializedOpMode] [int] NULL,
	[prioritizedOpMode] [nvarchar](max) NULL,
	[evaluationStatus] [int] NULL,
	[AlertStatusPriorityTypeID] [int] NULL,
	[modelEvalTime] [datetime] NULL,	
	[ActionWatchOverride] bit NULL
)
GO

GRANT EXEC ON TYPE::[Monitoring].[tpNDModelEval] TO [TEUser]
GO