﻿CREATE TYPE [Monitoring].[tpNDBatchModel] AS TABLE (
    [BatchModel] INT              NULL,
    [BatchId]    UNIQUEIDENTIFIER NOT NULL,
    [ModelExtID] NVARCHAR (255)   NOT NULL);
GO


GRANT EXEC ON TYPE::[Monitoring].[tpNDBatchModel] TO [TEUser]
GO