﻿CREATE TYPE [Monitoring].[tpNDModelTrendMap] AS TABLE (
    [NDModelTrendMapID]	   INT NULL,
	[ModelID]              INT NOT NULL,
    [PDTrendID]            INT NOT NULL,
	[AssetID]			   INT NOT NULL,
    [ReplaceTrendContents] BIT NOT NULL,
    [DisplayOrder]         INT NOT NULL);


GO
GRANT EXECUTE
    ON TYPE::[Monitoring].[tpNDModelTrendMap] TO [TEUser];

