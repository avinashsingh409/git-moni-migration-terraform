﻿CREATE TYPE [Monitoring].[tpModelConditionState] AS TABLE (
    [ModelConditionID]          INT      NULL,
    [Timestamp]                 DATETIME NULL,
    [RSquared]                  REAL     NULL,
    [ActualOutOfBounds_percent] REAL     NULL,
    [StateChanged]              BIT      NOT NULL,
    [NewAlert]                  BIT      NOT NULL,
    [ConditionLastActive]       DATETIME NULL,
    [ConditionLastClear]        DATETIME NULL);




GO
GRANT EXECUTE
    ON TYPE::[Monitoring].[tpModelConditionState] TO [TEUser];



