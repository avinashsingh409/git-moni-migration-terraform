﻿CREATE TYPE [Monitoring].[tpModelCondition] AS TABLE (
    [ModelConditionID]             INT      NULL,
    [ModelID]                      INT      NULL,
    [ConditionTimeBasis]           DATETIME NULL,
    [ConditionOutOfBounds_percent] REAL     NULL,
    [ConditionLastActive]          DATETIME NULL,
    [IsKeepConditionActive]        BIT      NOT NULL,
    [KeepConditionActiveTimestamp] DATETIME NULL,
    [Timestamp]                    DATETIME NULL,
    [ActualOutOfBounds_percent]    REAL     NULL,
    [ConditionLastClear]           DATETIME NULL,
    [StateChanged]                 BIT      NOT NULL,
    [IgnoreUntil]                  DATETIME NULL,
    [OutDirection]                 INT      NULL,
    [RSquared]                     REAL     NULL,
    [BeganAlertingTimestamp]       DATETIME NULL);










GO
GRANT EXECUTE
    ON TYPE::[Monitoring].[tpModelCondition] TO [TEUser];



