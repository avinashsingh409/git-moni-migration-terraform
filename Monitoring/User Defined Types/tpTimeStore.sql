CREATE TYPE [Monitoring].[tpTimeStore] AS TABLE(
	[ModelGlobalId] [uniqueidentifier] NOT NULL,
	[LatestPrediction] [datetimeoffset](7) NULL,
	PRIMARY KEY CLUSTERED 
(
	[ModelGlobalId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)

GO
GRANT EXEC ON TYPE::[Monitoring].[tpTimeStore] to [TEUser];
GO