﻿CREATE TYPE [Monitoring].[tpNDModelCriticality] AS TABLE (
    [ModelID]          INT      NOT NULL,
    [UseDefault]       BIT      NOT NULL,
    [UpperCriticality] INT      NULL,
    [LowerCriticality] INT      NULL,
    [CreatedByUserID]  INT      NOT NULL,
    [CreateDate]       DATETIME NOT NULL,
    [ChangedByUserID]  INT      NOT NULL,
    [ChangeDate]       DATETIME NOT NULL,
    PRIMARY KEY CLUSTERED ([ModelID] ASC));




GO
GRANT EXECUTE
    ON TYPE::[Monitoring].[tpNDModelCriticality] TO [TEUser];

