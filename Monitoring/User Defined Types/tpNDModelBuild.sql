﻿CREATE TYPE [Monitoring].[tpNDModelBuild] AS TABLE (
    [ModelExtID]        NVARCHAR (255) NULL,
    [BuildStatusTypeID] INT            NULL,
    [BuildStatus]       NVARCHAR (MAX) NULL,
    [BuildStatusUserID] INT            NULL,
    [BuildStatusTime]   DATETIME       NULL
)
GO

GRANT EXEC ON TYPE::[Monitoring].[tpNDModelBuild] TO [TEUser]
GO