﻿CREATE VIEW [Monitoring].[vModelDiagnosticTag]
AS
SELECT  ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID
       ,ProcessData.tAssetVariableTypeTagMap.VariableTypeID
       ,ProcessData.tAssetVariableTypeTagMap.AssetID
       ,Asset.tAsset.AssetAbbrev
       ,ProcessData.tTag.PDTagID
       ,ProcessData.tTag.TagName
       ,ProcessData.tTag.TagDesc
       ,ProcessData.tTag.EngUnits
       ,ProcessData.tServer.PDServerID
       ,ProcessData.tServer.ServerDesc
       ,ProcessData.tServerType.PDServerTypeID
       ,ProcessData.tServerType.ServerTypeDesc
       ,ProcessData.tServer.Comment
FROM ProcessData.tAssetVariableTypeTagMap
     INNER JOIN ProcessData.tTag
        ON ProcessData.tAssetVariableTypeTagMap.TagID = ProcessData.tTag.PDTagID
     INNER JOIN ProcessData.tServer
        ON ProcessData.tTag.PDServerID = ProcessData.tServer.PDServerID
     INNER JOIN ProcessData.tServerType
        ON ProcessData.tServer.PDServerTypeID = ProcessData.tServerType.PDServerTypeID
     INNER JOIN Asset.tAsset
        ON ProcessData.tAssetVariableTypeTagMap.AssetID = Asset.tAsset.AssetID
