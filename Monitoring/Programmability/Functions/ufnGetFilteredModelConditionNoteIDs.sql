﻿CREATE Function [Monitoring].[ufnGetFilteredModelConditionNoteIDs]
(
	@modelId int,
	@modelActionTypeID Base.tpIntList READONLY,
	@timestampStart datetime = NULL,
	@timestampEnd datetime = NULL,
	@executor NVARCHAR(100) = NULL,
	@noteText NVARCHAR(255) = NULL,
	@assetIds Base.tpIntList READONLY
)
RETURNS @results TABLE (
	ModelConditionNoteID int
)
As
Begin
	Declare @noteAddedTypeID int = 10
	
	IF @modelId is null
	  BEGIN
		Insert Into @results (
			ModelConditionNoteID
		)
		Select
			DISTINCT n.ModelConditionNoteID
			FROM @assetIds br
			JOIN ProcessData.tAssetVariableTypeTagMap m ON br.id = m.AssetID
			JOIN Monitoring.tNDModel a ON m.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
			JOIN Monitoring.tModel d ON a.AssetVariableTypeTagMapID = d.AssetVariableTypeTagMapID
			JOIN Monitoring.tModelCondition o on d.ModelID = o.ModelID
			JOIN Monitoring.tModelConditionNote n on o.ModelConditionID = n.ModelConditionID
		WHERE
			(NOT EXISTS (SELECT 1 FROM @modelActionTypeID) OR (@noteAddedTypeID IN (SELECT id FROM @modelActionTypeID))) AND
			((@timestampStart IS NULL OR @timestampEnd IS NULL) OR (n.[TimeStamp] BETWEEN @timestampStart AND @timestampEnd)) AND
			(@executor IS NULL OR (n.ChangedBy like '%' + @executor + '%')) AND
			(@noteText IS NULL OR (n.Note like '%' + @noteText + '%'))  OPTION (RECOMPILE)
	  END

	ELSE
	   BEGIN
	   Insert Into @results (
			ModelConditionNoteID
		)
		Select
			DISTINCT n.ModelConditionNoteID
			FROM 			
			Monitoring.tNDModel a 
			JOIN ProcessData.tAssetVariableTypeTagMap m ON m.AssetVariableTypeTagMapID = a.AssetVariableTypeTagMapID
			JOIN Monitoring.tModel d ON a.AssetVariableTypeTagMapID = d.AssetVariableTypeTagMapID
			JOIN Monitoring.tModelCondition o on d.ModelID = o.ModelID
			JOIN Monitoring.tModelConditionNote n on o.ModelConditionID = n.ModelConditionID
		WHERE
			(a.ModelID = @modelId) AND
			(NOT EXISTS (SELECT 1 FROM @modelActionTypeID) OR (@noteAddedTypeID IN (SELECT id FROM @modelActionTypeID))) AND
			((@timestampStart IS NULL OR @timestampEnd IS NULL) OR (n.[TimeStamp] BETWEEN @timestampStart AND @timestampEnd)) AND
			(@executor IS NULL OR (n.ChangedBy like '%' + @executor + '%')) AND
			(@noteText IS NULL OR (n.Note like '%' + @noteText + '%'))  OPTION (RECOMPILE)
	   END
	Return
End
GO

GRANT SELECT
    On Object::[Monitoring].[ufnGetFilteredModelConditionNoteIDs] To [TEUser]
    As [dbo];
