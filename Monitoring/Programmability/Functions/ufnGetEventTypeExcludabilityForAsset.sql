﻿
-- ===============================================
-- Author:		Ryan Irwin
-- Create date: 7/10/2015
-- Description:	Gets the excludability from availability
-- calcs for a specific event type at a given asset,
-- defined either for the asset itself or inherited 
-- from an ancestor asset. If no setting is found,
-- the default value of 0 is returned: events are
-- not excludable unless specifically declared so.
-- ===============================================
CREATE FUNCTION [Monitoring].[ufnGetEventTypeExcludabilityForAsset]
(
	-- Add the parameters for the function here
	@AssetID int,
	@EventTypeID int,
	@SecurityUserID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit;
	
	if(@SecurityUserID is not null AND @SecurityUserID > 0)
	BEGIN
		-- Add the T-SQL statements to compute the return value here
		SELECT @result = tETAEM.[ExcludableFromAvailabilityCalcs]
		FROM [Monitoring].[tEventTypeAssetExcludableMap] tETAEM
		INNER JOIN Asset.ufnGetAssetIdsForUser(@SecurityUserID) a
		on a.AssetID = tETAEM.AssetID
		WHERE tETAEM.AssetID = @AssetID 
		AND tETAEM.EventTypeID = @EventTypeID;
	END
	ELSE
	BEGIN
		-- Add the T-SQL statements to compute the return value here
		SELECT @result = tETAEM.[ExcludableFromAvailabilityCalcs]
		FROM [Monitoring].[tEventTypeAssetExcludableMap] tETAEM
		WHERE tETAEM.AssetID = @AssetID 
		AND tETAEM.EventTypeID = @EventTypeID;
	END
	if @result is null 
	BEGIN
		DECLARE @parentAssetID int;
		SELECT @parentAssetID = ParentAssetID
		FROM Asset.tAsset
		WHERE AssetID = @AssetID;
		
		if @parentAssetID is not null
		BEGIN
			set @result = [Monitoring].[ufnGetEventTypeExcludabilityForAsset](@parentAssetID, @EventTypeID, @SecurityUserID);
		END
		ELSE
		BEGIN
			set @result = 0;
		END
	END
	
	-- Return the result of the function
	RETURN @result
END
GO
