﻿CREATE TABLE [Monitoring].[tNDConcerningBoundType] (
    [ConcerningBoundTypeID]     INT            NOT NULL,
    [ConcerningBoundTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [ConcerningBoundTypeDesc]   NVARCHAR (255) NOT NULL,
    [DisplayOrder]              INT            NOT NULL,
    CONSTRAINT [PK_tNDConcerningBoundType] PRIMARY KEY CLUSTERED ([ConcerningBoundTypeID] ASC)
);

