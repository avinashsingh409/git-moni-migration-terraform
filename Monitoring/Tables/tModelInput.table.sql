CREATE TABLE [Monitoring].[tModelInput] (
    [ModelInputID]              INT IDENTITY (1, 1) NOT NULL,
    [ModelID]                   INT NOT NULL,
    [InputNumber]               INT NULL,
    [AssetVariableTypeTagMapID] INT NULL,
    CONSTRAINT [PK_Monitoring_tModelInput] PRIMARY KEY CLUSTERED ([ModelInputID] ASC),
    CONSTRAINT [FK_Monitoring_tModelInput_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]),
    CONSTRAINT [FK_Monitoring_tModelInput_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelInput';


GO
CREATE INDEX [IDX_tModelInput_ModelID_includes] ON [Monitoring].[tModelInput] ([ModelID])  INCLUDE ([InputNumber], [AssetVariableTypeTagMapID]) ;