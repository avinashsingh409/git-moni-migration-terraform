CREATE TABLE [Monitoring].[tModelInputDefault] (
    [ModelInputDefaultID]             INT IDENTITY (1, 1) NOT NULL,
    [ModelDefaultID]                  INT NOT NULL,
    [InputNumber]                     INT NULL,
    [AssetClassTypeVariableTypeMapID] INT NULL,
    CONSTRAINT [PK_Monitoring_tModelInputDefault] PRIMARY KEY CLUSTERED ([ModelInputDefaultID] ASC),
    CONSTRAINT [FK_Monitoring_tModelInputDefault_AssetClassTypeVariableTypeMapID_tAssetClassTypeVariableTypeMap] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID]),
    CONSTRAINT [FK_Monitoring_tModelInputDefault_ModelDefaultID_tModelDefault] FOREIGN KEY ([ModelDefaultID]) REFERENCES [Monitoring].[tModelDefault] ([ModelDefaultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelInputDefault';

