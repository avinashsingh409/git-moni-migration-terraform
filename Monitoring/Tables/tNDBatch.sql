﻿CREATE TABLE [Monitoring].[tNDBatch] (
    [Batch]       INT              IDENTITY (1, 1) NOT NULL,
    [BatchId]     UNIQUEIDENTIFIER NOT NULL,
    [BatchTypeId] INT              NOT NULL,
    [UserId]      INT              NOT NULL,
    [BatchDate]   DATETIME         CONSTRAINT [DF_tNDBatch_BatchDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tNDBatch] PRIMARY KEY CLUSTERED ([BatchId] ASC),
    CONSTRAINT [FK_tNDBatch_tNDBatchType_BatchTypeId] FOREIGN KEY ([BatchTypeId]) REFERENCES [Monitoring].[tNDBatchType] ([BatchTypeId]),
    CONSTRAINT [FK_tNDBatch_tUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE
);

