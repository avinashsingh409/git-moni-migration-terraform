﻿CREATE TABLE [Monitoring].[tNDModelEvaluation] (
    [ModelID]                        INT            NOT NULL,
    [NDAlert]                        INT            NOT NULL,
    [Alert]                          BIT            NOT NULL,
    [NDActual]                       REAL           NULL,
    [NDExpected]                     REAL           NULL,
    [NDUpper]                        REAL           NULL,
    [NDLower]                        REAL           NULL,
    [PercentOOB]                     REAL           NOT NULL,
    [HiHiAlert]                      BIT            NOT NULL,
    [HiHiAlertSinceTime]             DATETIME       NULL,
    [LowLowAlert]                    BIT            NOT NULL,
    [LowLowAlertSinceTime]           DATETIME       NULL,
    [AreaFastResponseAlert]          BIT            NOT NULL,
    [AreaFastResponseAlertSinceTime] DATETIME       NULL,
    [AreaSlowResponseAlert]          BIT            NOT NULL,
    [AreaSlowResponseAlertSinceTime] DATETIME       NULL,
    [FrequencyAlert]                 BIT            NOT NULL,
    [FrequencyAlertSinceTime]        DATETIME       NULL,
    [OscillationAlert]               BIT            NOT NULL,
    [OscillationAlertSinceTime]      DATETIME       NULL,
    [BatchEvaluationStartTime]       DATETIME       NOT NULL,
    [EvaluationError]                BIT            NOT NULL,
    [EvaluationErrorMessage]         NVARCHAR (MAX) NULL,
    [EvaluationTime]                 DATETIME       NOT NULL,
    [WatchOverrideAlert]             BIT            NULL,
    [WatchOverrideAlertSinceTime]    DATETIME       NULL,
    [SerializedOpMode]               INT            NULL,
    [PrioritizedOpMode]              NVARCHAR (MAX) NULL,
    [EvaluationStatus]               INT            NULL,
    [AddedToAlerts]                  DATETIME       NULL,
    [FrozenDataAlert]                BIT            NOT NULL,
    [FrozenDataAlertSinceTime]       DATETIME       NULL,
    [IgnoreOverrideAlert]            BIT            NOT NULL,
    [IgnoreOverrideAlertSinceTime]   DATETIME       NULL,
    [AlertStatusPriorityTypeID]      INT            NULL,
    [ActionWatchOverride]            BIT            CONSTRAINT [DF_tNDModelEvaluation_ActionWatchOverride] DEFAULT ((0)) NOT NULL,
    [ActionWatchOverrideSinceTime]   DATETIME       NULL,
    CONSTRAINT [PK_tNDModelEvaluation] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [FK_tNDModelEvaluation_tNDAlertStatusPriorityType_AlertStatusPriorityTypeID] FOREIGN KEY ([AlertStatusPriorityTypeID]) REFERENCES [Monitoring].[tNDAlertStatusPriorityType] ([AlertStatusPriorityTypeID]),
    CONSTRAINT [FK_tNDModelEvaluation_tNDModel_ModelID] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID])
);




























GO
CREATE TRIGGER [Monitoring].[tr_tNDModelEvaluation_Update]
ON [Monitoring].[tNDModelEvaluation]
INSTEAD OF UPDATE
AS
BEGIN

DECLARE @Now as DateTime
SET @Now = GETDATE()

UPDATE A
	    SET
		NDAlert = b.NDAlert
		, Alert =  b.Alert
		, NDActual =  b.NDActual
		, NDExpected =  b.NDExpected
		, NDUpper =  b.NDUpper
		, NDLower =  b.NDLower
		, PercentOOB =  b.PercentOOB
		, HiHiAlert =  b.HiHiAlert
		, HiHiAlertSinceTime = CASE WHEN  b.HiHiAlert = 1 THEN COALESCE( a.HiHiAlertSinceTime,  COALESCE(b.EvaluationTime,@Now) ) ELSE NULL END
		, LowLowAlert =  b.LowLowAlert
		, LowLowAlertSinceTime = CASE WHEN  b.LowLowAlert = 1 THEN COALESCE( a.LowLowAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, AreaFastResponseAlert =  b.AreaFastResponseAlert
		, AreaFastResponseAlertSinceTime = CASE WHEN  b.AreaFastResponseAlert = 1 THEN COALESCE( a.AreaFastResponseAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, AreaSlowResponseAlert =  b.AreaSlowResponseAlert
		, AreaSlowResponseAlertSinceTime = CASE WHEN  b.AreaSlowResponseAlert = 1 THEN COALESCE( a.AreaSlowResponseAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, FrequencyAlert =  b.FrequencyAlert
		, FrequencyAlertSinceTime = CASE WHEN  b.FrequencyAlert = 1 THEN COALESCE( a.FrequencyAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, OscillationAlert =  b.OscillationAlert
		, OscillationAlertSinceTime = CASE WHEN  b.OscillationAlert = 1 THEN COALESCE( a.OscillationAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, FrozenDataAlert =  b.FrozenDataAlert
		, FrozenDataAlertSinceTime = CASE WHEN  b.FrozenDataAlert = 1 THEN COALESCE( a.FrozenDataAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, IgnoreOverrideAlert =  b.IgnoreOverrideAlert
		, IgnoreOverrideAlertSinceTime = CASE WHEN  b.IgnoreOverrideAlert = 1 THEN COALESCE( a.IgnoreOverrideAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, WatchOverrideAlert =  b.WatchOverrideAlert
		, WatchOverrideAlertSinceTime = CASE WHEN  b.WatchOverrideAlert = 1 THEN COALESCE( a.WatchOverrideAlertSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, ActionWatchOverride =  b.ActionWatchOverride
		, ActionWatchOverrideSinceTime = CASE WHEN  b.ActionWatchOverride = 1 THEN COALESCE( a.ActionWatchOverrideSinceTime,  COALESCE(b.EvaluationTime,@Now)) ELSE NULL END
		, BatchEvaluationStartTime =  b.BatchEvaluationStartTime
		, EvaluationError =  b.EvaluationError
		, EvaluationErrorMessage =  b.EvaluationErrorMessage
		, EvaluationTime =  b.EvaluationTime
		, SerializedOpMode =  b.SerializedOpMode
		, PrioritizedOpMode =  b.PrioritizedOpMode
		, EvaluationStatus =  b.EvaluationStatus
		, AlertStatusPriorityTypeID =  b.AlertStatusPriorityTypeID
		, AddedToAlerts = CASE WHEN  a.Alert = 0 AND  b.Alert = 1 THEN  COALESCE(b.EvaluationTime,@Now) ELSE  a.AddedToAlerts END

	    FROM
		[Monitoring].[tNDModelEvaluation] a JOIN inserted  b on a.ModelID=b.ModelID

END

GO
