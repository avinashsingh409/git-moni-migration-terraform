﻿CREATE TABLE [Monitoring].[tNDAlertNotification] (
    [NDAlertNotificationID]     UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [ModelID]                   INT              NOT NULL,
    [AlertStatusTypeID]         INT              NOT NULL,
    [NotificationMode]          BIT              NOT NULL,
    [NotificationFrequency]     INT              NOT NULL,
    [NotificationTitle]         VARCHAR (255)    NULL,
    [NotificationSummary]       NVARCHAR (MAX)   NULL,
    [CreatedByUserID]           INT              NOT NULL,
    [CreateDate]                DATETIME         CONSTRAINT [DF_tNDAlertNotification_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]           INT              NOT NULL,
    [ChangeDate]                DATETIME         CONSTRAINT [DF_tNDAlertNotification_ChangeDate] DEFAULT (getdate()) NOT NULL,
	[NotificationLastSentTime] [datetime]  NULL
    CONSTRAINT [PK_NDAlertNotification] PRIMARY KEY CLUSTERED ([NDAlertNotificationID] ASC),
    CONSTRAINT [FK_tNDAlertNotification_AlertStatusTypeID] FOREIGN KEY ([AlertStatusTypeID]) REFERENCES [Monitoring].[tNDAlertStatusType] ([AlertStatusTypeID]) ON DELETE CASCADE,
    CONSTRAINT [Fk_tNDAlertNotification_ChangedByUserID_AccessControl_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [Fk_tNDAlertNotification_CreatedByUserID_AccessControl_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tNDAlertNotification_tNDModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]) ON DELETE CASCADE
);






GO
CREATE INDEX [IDX_tNDAlertNotification_ModelID_AlertStatusTypeID] ON [Monitoring].[tNDAlertNotification] ([ModelID], [AlertStatusTypeID])  ;