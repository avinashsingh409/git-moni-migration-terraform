--EventType table
CREATE TABLE [Monitoring].[tEventType] (
    [EventTypeID]          INT         NOT NULL,
    [EventTypeAbbrev]      NCHAR (255) NOT NULL,
    [EventTypeDesc]        NCHAR (255) NOT NULL,
    [DelayBeforeNextEvent] INT         NULL,
    CONSTRAINT [PK_tEventType] PRIMARY KEY CLUSTERED ([EventTypeID] ASC)
);







GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tEventType';

