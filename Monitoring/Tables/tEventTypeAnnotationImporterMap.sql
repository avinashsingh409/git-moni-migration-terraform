﻿CREATE TABLE [Monitoring].[tEventTypeAnnotationImporterMap](
	[EventTypeImporterMapID] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeID] [int] NOT NULL,
	[AnnotationImporterID] [int] NOT NULL,
 CONSTRAINT [PK_tEventTypeAnnotationImporterMap] PRIMARY KEY CLUSTERED 
(
	[EventTypeImporterMapID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [Monitoring].[tEventTypeAnnotationImporterMap]  ADD  CONSTRAINT [FK_tEventTypeAnnotationImporterMap_tAnnotationImporter] FOREIGN KEY([AnnotationImporterID])
REFERENCES [ProcessData].[tAnnotationImporter] ([AnnotationImporterID])
GO

ALTER TABLE [Monitoring].[tEventTypeAnnotationImporterMap] CHECK CONSTRAINT [FK_tEventTypeAnnotationImporterMap_tAnnotationImporter]
GO

ALTER TABLE [Monitoring].[tEventTypeAnnotationImporterMap]  ADD  CONSTRAINT [FK_tEventTypeAnnotationImporterMap_tEventType] FOREIGN KEY([EventTypeID])
REFERENCES [Monitoring].[tEventType] ([EventTypeID])
GO

ALTER TABLE [Monitoring].[tEventTypeAnnotationImporterMap] CHECK CONSTRAINT [FK_tEventTypeAnnotationImporterMap_tEventType]
GO