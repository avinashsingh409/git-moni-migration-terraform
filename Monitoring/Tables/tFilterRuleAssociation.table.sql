CREATE TABLE [Monitoring].[tFilterRuleAssociation] (
    [FilterRuleAssociationID]   INT IDENTITY (1, 1) NOT NULL,
    [FilterRuleID]              INT NOT NULL,
    [AssetVariableTypeTagMapID] INT NOT NULL,
    CONSTRAINT [PK_tFilterRuleAssociation] PRIMARY KEY CLUSTERED ([FilterRuleAssociationID] ASC),
    CONSTRAINT [FK_Monitoring_tFilterRuleAssociation_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Monitoring_tFilterRuleAssociation_FilterRuleID_tFilterRule] FOREIGN KEY ([FilterRuleID]) REFERENCES [Monitoring].[tFilterRule] ([FilterRuleID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tFilterRuleAssociation] UNIQUE NONCLUSTERED ([FilterRuleID] ASC, [AssetVariableTypeTagMapID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tFilterRuleAssociation';

