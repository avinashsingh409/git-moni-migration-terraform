﻿CREATE TABLE [Monitoring].[tImageConditionResult] (
    [ImageConditionResultID] INT            IDENTITY (1, 1) NOT NULL,
    [Result]                 NVARCHAR (255) NOT NULL,
    [IsDefault] BIT NOT NULL CONSTRAINT [DF_tImageConditionResult_IsDefault] DEFAULT 0, 
	[AssetID]                INT            NOT NULL,
    [CreatedBy] INT NOT NULL, 
    [ChangedBy] INT NOT NULL, 
    [CreateDate] DATETIME NOT NULL CONSTRAINT [DF_tImageConditionResult_CreateDate]  DEFAULT (getdate()), 
    [ChangeDate] DATETIME NOT NULL CONSTRAINT [DF_tImageConditionResult_ChangeDate]  DEFAULT (getdate()), 
    CONSTRAINT [PK_Monitoring_tImageConditionResult] PRIMARY KEY CLUSTERED ([ImageConditionResultID] ASC),
    CONSTRAINT [FK_Monitoring_tImageConditionResult_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
);