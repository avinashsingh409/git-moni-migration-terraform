﻿CREATE TABLE [Monitoring].[tModelConditionNote] (
    [ModelConditionNoteID] INT            IDENTITY (1, 1) NOT NULL,
    [TimeStamp]            DATETIME       CONSTRAINT [DF_ModelConditionNote_TimeStamp] DEFAULT (getdate()) NOT NULL,
    [ModelConditionID]     INT            NOT NULL,
    [Note]                 NVARCHAR (MAX) NULL,
    [CreatedBy]            VARCHAR (255)  NOT NULL,
    [NeedsAttention]       BIT            CONSTRAINT [DF_tModelConditionNote_NeedsAttention] DEFAULT ((0)) NOT NULL,
    [ChangedBy]            VARCHAR (255)  NOT NULL,
    CONSTRAINT [PK_odelConditionNote] PRIMARY KEY CLUSTERED ([ModelConditionNoteID] ASC),
    CONSTRAINT [FK_ModelConditionNote_ModelConditionID_ModelConditionID] FOREIGN KEY ([ModelConditionID]) REFERENCES [Monitoring].[tModelCondition] ([ModelConditionID]),
    CONSTRAINT [FK_Monitoring_tModelConditionNode_ModelConditionID_tModelCondition] FOREIGN KEY ([ModelConditionID]) REFERENCES [Monitoring].[tModelCondition] ([ModelConditionID]) ON DELETE CASCADE
);

















GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelConditionNote';


GO


CREATE TRIGGER [Monitoring].[tModelConditionNote_Update] 
on [Monitoring].[tModelConditionNote] for update
AS
DECLARE 
@OperationDate datetime,
@AppName varchar(128),
@ClientName varchar(128),
@ModelConditionID int,
@ModelConditionNoteID int,
@ModelID int,
@ConditionTimeBasis datetime,
@ConditionsOOBPercent real,
@ConditionsLastActive datetime,
@IsKeepConditionActive bit,
@KeepConditionActiveTimestamp datetime,
@Timestamp datetime,
@ActualOOBPercent real,
@ConditionLastClear datetime,
@NeedsAttention bit,
@StateChanged bit,
@IgnoreUntil datetime,
@OutDirection int,
@BucketNo int,
@RSquared real,
@StandardError real,
@ResultsDate datetime,
@BeganAlertingTimestamp datetime,
@Update bit = 0,
@FieldChanged varchar(256)= '',
@ActualValue real,
@ExpectedValue real,
@UpperLimit real,
@LowerLimit real,
@NeedsAttn bit,
@OLD_NeedsAttn bit,
@NEW_NeedsAttn bit

select @OperationDate = CURRENT_TIMESTAMP
Select @ClientName = HOST_NAME()
Select @AppName = APP_NAME()


SELECT @NEW_NeedsAttn = NeedsAttention
from inserted 

SELECT @OLD_NeedsAttn = NeedsAttention
from deleted
     
IF (@NEW_NeedsAttn != @OLD_NeedsAttn)
  BEGIN
    IF (@NEW_NeedsAttn = 1 AND @OLD_NeedsAttn = 0)
      BEGIN 
        SET @Update = 1
        SET @FieldChanged = RTRIM(@FieldChanged) + 'Needs Attention; ';
      END
    IF (@NEW_NeedsAttn = 0 and @OLD_NeedsAttn = 1)
      BEGIN
        SET @Update = 1
        SET @FieldChanged = RTRIM(@FieldChanged) + 'Needs Attention Cleared; ';
      END
  END
    
  IF (@Update=1)
    BEGIN
     select @ModelConditionNoteID = i.ModelConditionNoteID,
            @ModelConditionID = i.ModelConditionID,
            @NeedsAttention = i.NeedsAttention,
            @ModelID = mc.ModelID, @ConditionTimeBasis = mc.ConditionTimeBasis,
            @ConditionsOOBPercent = mc.ConditionOutOfBounds_percent,
            @ConditionsLastActive= mc.ConditionLastActive,
            @IsKeepConditionActive= mc.IsKeepConditionActive, 
            @KeepConditionActiveTimeStamp = mc.KeepConditionActiveTimestamp,
            @Timestamp = mc.Timestamp, 
            @ActualOOBPercent = mc.ActualOutOfBounds_percent,
            @ConditionLastClear = mc.ConditionLastClear, 
            @StateChanged = mc.StateChanged,
            @IgnoreUntil = mc.IgnoreUntil, 
            @OutDirection =  mc.OutDirection,
            @BeganAlertingTimestamp = mc.BeganAlertingTimestamp,
            @RSquared =  mc.RSquared,
            @ActualValue = ms.ActualValue,
            @ExpectedValue = ms.ExpectedValue,
            @UpperLimit = ms.UpperLimit,
            @LowerLimit = ms.LowerLimit
            from inserted i, Monitoring.tModelCondition mc
            LEFT OUTER JOIN Monitoring.tModelSnapshot ms ON ms.ModelID = mc.ModelID
            where i.ModelConditionID = mc.ModelConditionID
 
    Insert Monitoring.tModelConditionArchive (ModelConditionID,
          ModelID, ConditionTimeBasis, ConditionOutOfBounds_percent,
          ConditionLastActive, IsKeepConditionActive, KeepConditionActiveTimestamp,
          Timestamp, ActualOutOfBounds_percent, ConditionLastClear,
          StateChanged, IgnoreUntil, OutDirection, RSquared, 
          NeedsAttention, ActualValue, ExpectedValue, UpperLimit,
          LowerLimit, BeganAlertingTimestamp, ModelConditionNoteID,
          OperationDate, ClientName, AppName, OperationFieldChanged)
          values (@ModelConditionID, @ModelID, @ConditionTimeBasis, @ConditionsOOBPercent,
          @ConditionsLastActive, @IsKeepConditionActive, @KeepConditionActiveTimestamp,
          @Timestamp, @ActualOOBPercent, @ConditionLastClear,
          @StateChanged, @IgnoreUntil, @OutDirection, @RSquared,          
          @NeedsAttention, @ActualValue, @ExpectedValue, @UpperLimit,
          @LowerLimit, @BeganAlertingTimestamp, @ModelConditionNoteID,
          @OperationDate, @ClientName, @AppName, @FieldChanged)
          
 END
GO
/*** Trigger to check for inserts to Model Condition Notes **/
/*** to see if the NeedsAttention flag has been set for the new note****/

CREATE TRIGGER [Monitoring].[tModelConditionNote_Insert] 
on [Monitoring].[tModelConditionNote] FOR insert
AS
DECLARE 
@OperationDate datetime,
@AppName varchar(128),
@ClientName varchar(128),
@ModelConditionID int,
@ModelConditionNoteID int,
@ModelID int,
@ConditionTimeBasis datetime,
@ConditionsOOBPercent real,
@ConditionsLastActive datetime,
@IsKeepConditionActive bit,
@KeepConditionActiveTimestamp datetime,
@Timestamp datetime,
@ActualOOBPercent real,
@ConditionLastClear datetime,
@NeedsAttention bit,
@StateChanged bit,
@IgnoreUntil datetime,
@OutDirection int,
@BucketNo int,
@RSquared real,
@StandardError real,
@ResultsDate datetime,
@BeganAlertingTimestamp datetime,
@Update bit = 0,
@FieldChanged varchar(256)= '',
@ActualValue real,
@ExpectedValue real,
@UpperLimit real,
@LowerLimit real,
@NeedsAttn bit,
@OLD_NeedsAttn bit,
@NEW_NeedsAttn bit,
@RowCount int,
@NeedsLogging bit

select @OperationDate = CURRENT_TIMESTAMP
Select @ClientName = HOST_NAME()
Select @AppName = APP_NAME()
SET @NeedsLogging = 0

SELECT @NEW_NeedsAttn = NeedsAttention
from inserted 

--New record so Needs Attention needs to get logged
IF (@NEW_NeedsAttn = 1)
  BEGIN
    SET @NeedsLogging = 1
    SET @FieldChanged = RTRIM(@FieldChanged) + 'Needs Attention; ';
  END
  
    
 IF (@NeedsLogging = 1)
   BEGIN
       select @ModelConditionNoteID = i.ModelConditionNoteID,
            @ModelConditionID = i.ModelConditionID,
            @NeedsAttention = i.NeedsAttention,
            @ModelID = mc.ModelID, @ConditionTimeBasis = mc.ConditionTimeBasis,
            @ConditionsOOBPercent = mc.ConditionOutOfBounds_percent,
            @ConditionsLastActive= mc.ConditionLastActive,
            @IsKeepConditionActive= mc.IsKeepConditionActive, 
            @KeepConditionActiveTimeStamp = mc.KeepConditionActiveTimestamp,
            @Timestamp = mc.Timestamp, 
            @ActualOOBPercent = mc.ActualOutOfBounds_percent,
            @ConditionLastClear = mc.ConditionLastClear, 
            @StateChanged = mc.StateChanged,
            @IgnoreUntil = mc.IgnoreUntil, 
            @OutDirection =  mc.OutDirection,
            @BeganAlertingTimestamp = mc.BeganAlertingTimestamp,
            @RSquared =  mc.RSquared,
            @ActualValue = ms.ActualValue,
            @ExpectedValue = ms.ExpectedValue,
            @UpperLimit = ms.UpperLimit,
            @LowerLimit = ms.LowerLimit
            from inserted i, Monitoring.tModelCondition mc
            LEFT OUTER JOIN Monitoring.tModelSnapshot ms ON ms.ModelID = mc.ModelID
            where i.ModelConditionID = mc.ModelConditionID
 
      Insert Monitoring.tModelConditionArchive (ModelConditionID,
          ModelID, ConditionTimeBasis, ConditionOutOfBounds_percent,
          ConditionLastActive, IsKeepConditionActive, KeepConditionActiveTimestamp,
          Timestamp, ActualOutOfBounds_percent, ConditionLastClear,
          StateChanged, IgnoreUntil, OutDirection, RSquared, 
          NeedsAttention, ActualValue, ExpectedValue, UpperLimit,
          LowerLimit, BeganAlertingTimestamp, ModelConditionNoteID,
          OperationDate, ClientName, AppName, OperationFieldChanged)
          values (@ModelConditionID, @ModelID, @ConditionTimeBasis, @ConditionsOOBPercent,
          @ConditionsLastActive, @IsKeepConditionActive, @KeepConditionActiveTimestamp,
          @Timestamp, @ActualOOBPercent, @ConditionLastClear,
          @StateChanged, @IgnoreUntil, @OutDirection, @RSquared,          
          @NeedsAttention, @ActualValue, @ExpectedValue, @UpperLimit,
          @LowerLimit, @BeganAlertingTimestamp,@ModelConditionNoteID,
          @OperationDate, @ClientName, @AppName, @FieldChanged)
          
   END
GO
CREATE INDEX [IDX_tModelConditionNote_TimeStamp_ModelConditionID_includes] ON [Monitoring].[tModelConditionNote] ([TimeStamp], [ModelConditionID])  INCLUDE ([Note], [NeedsAttention]) ;
GO
CREATE INDEX [IDX_tModelConditionNote_ModelConditionID_TimeStamp] ON [Monitoring].[tModelConditionNote] ([ModelConditionID], [TimeStamp])  ;