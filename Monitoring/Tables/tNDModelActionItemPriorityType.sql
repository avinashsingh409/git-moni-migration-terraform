﻿CREATE TABLE [Monitoring].[tNDModelActionItemPriorityType] (
		[ModelActionItemPriorityTypeID] INTEGER  NOT NULL  
		, [ModelActionItemPriorityValue] INTEGER  NOT NULL  
		, [ModelActionItemPriorityAbbrev] nvarchar(50)  NOT NULL  
		, [ModelActionItemPriorityDesc] nvarchar(255)  NOT NULL  
		, CONSTRAINT [tNDModelActionItemPriorityType_PK] PRIMARY KEY CLUSTERED ([ModelActionItemPriorityTypeID])
		)
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDModelActionItemPriorityType'