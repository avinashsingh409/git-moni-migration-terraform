﻿CREATE TABLE [Monitoring].[tNDModelActionItemArchive] (
    [ModelActionItemArchiveID]      INT            IDENTITY (1, 1) NOT NULL,
    [ModelID]                       INT            NOT NULL,
    [ModelActionItemPriorityTypeID] INT            NULL,
    [NoteText]                      NVARCHAR (MAX) NULL,
    [CreatedByUserID]               INT            NOT NULL,
    [CreateDate]                    DATETIME       NOT NULL,
    [ChangedByUserID]               INT            NOT NULL,
    [ChangeDate]                    DATETIME       NOT NULL,
    [WatchDuration]                 REAL           NULL,
    [WatchDurationTemporalTypeID]   INT            NULL,
    [WatchCriteriaTypeID]           INT            NULL,
    [WatchLimit]                    REAL           NULL,
    [WatchLimitMeetsTemporalTypeID] INT            NULL,
    [ModelActionItemTypeID]         INT            NULL,
    [WatchRelativeToExpected]       BIT            CONSTRAINT [DF_tNDModelActionItemArchive_WatchRelativeToExpected] DEFAULT ((0)) NOT NULL,
    [NDAlert]                       INT            NULL,
    [NDActual]                      REAL           NULL,
    [NDExpected]                    REAL           NULL,
    [NDLower]                       REAL           NULL,
    [NDUpper]                       REAL           NULL,
    [PercentOOB]                    REAL           NULL,
    [SerializedOpMode]              INT            NULL,
    [PrioritizedOpMode]             NVARCHAR (MAX) NULL,
    [EvaluationStatus]              INT            NULL,
    [AddedtoAlerts]                 DATETIME       NULL,
    [AlertStatusPriorityTypeID]     INT            NULL,
    [IgnoreDurationHours]           INT            NULL,
    [Favorite] BIT NOT NULL DEFAULT ((0)), 
    [WatchEmailIfAlert] BIT NOT NULL DEFAULT ((0)),
    [WatchDuration2]                 REAL           NULL,
    [WatchDurationTemporalTypeID2]   INT            NULL,
    [WatchCriteriaTypeID2]           INT            NULL,
    [WatchLimit2]                    REAL           NULL,
    [WatchLimitMeetsTemporalTypeID2] INT            NULL,
    [WatchRelativeToExpected2]       BIT            CONSTRAINT [DF_tNDModelActionItemArchive_WatchRelativeToExpected2] DEFAULT ((0)) NOT NULL,
    [HiHiAlert]                     BIT            NULL,
    [LowLowAlert]                   BIT            NULL,
    [AreaFastResponseAlert]         BIT            NULL,
    [AreaSlowResponseAlert]         BIT            NULL,
    [FrequencyAlert]                BIT            NULL,
    [OscillationAlert]              BIT            NULL,
    [FrozenDataAlert]               BIT            NULL,
    [WatchOverrideAlert]            BIT            NULL,
    [IgnoreOverrideAlert]           BIT            NULL,
    CONSTRAINT [tNDModelActionItemArchive_PK] PRIMARY KEY CLUSTERED ([ModelActionItemArchiveID] ASC),
    CONSTRAINT [FK_tNDModelActionItemType_tNDModelActionItemArchive] FOREIGN KEY ([ModelActionItemTypeID]) REFERENCES [Monitoring].[tNDModelActionItemType] ([ModelActionItemTypeID])
);










GO
CREATE NONCLUSTERED INDEX [IDX_tNDModelActionItemArchive_ModelID]
    ON [Monitoring].[tNDModelActionItemArchive]([ModelID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tNDModelActionItemArchive_ModelActionItemTypeId]
    ON [Monitoring].[tNDModelActionItemArchive]([ModelActionItemTypeID] ASC)
    INCLUDE([ModelActionItemArchiveID], [ModelID]);


GO
CREATE INDEX [IX_ChangeDate_Includes] ON [Monitoring].[tNDModelActionItemArchive] ([ChangeDate])  INCLUDE ([ModelID], [ModelActionItemTypeID]) WITH (FILLFACTOR=100);

GO