﻿-- Model Update Trigger
CREATE TRIGGER [Monitoring].[tr_tModel_Update]
   ON  [Monitoring].[tModel]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF UPDATE(UpperLimitMultiplier)
	BEGIN
		INSERT INTO tModelHistory (ModelID, Operation, Data)
		SELECT ModelID, 'UM', CONVERT(NVARCHAR(255), UpperLimitMultiplier) FROM Deleted
	END

	IF UPDATE(LowerLimitMultiplier)
	BEGIN
		INSERT INTO tModelHistory (ModelID, Operation, Data)
		SELECT ModelID, 'LM', CONVERT(NVARCHAR(255), LowerLimitMultiplier) FROM Deleted
	END

	IF UPDATE(UpperLimitAdder)
	BEGIN
		INSERT INTO tModelHistory (ModelID, Operation, Data)
		SELECT ModelID, 'UA', CONVERT(NVARCHAR(255), UpperLimitAdder) FROM Deleted
	END

	IF UPDATE(LowerLimitAdder)
	BEGIN
		INSERT INTO tModelHistory (ModelID, Operation, Data)
		SELECT ModelID, 'LA', CONVERT(NVARCHAR(255), LowerLimitAdder) FROM Deleted
	END

	IF UPDATE(SetLowerSameAsUpper)
	BEGIN
		INSERT INTO tModelHistory (ModelID, Operation, Data)
		SELECT ModelID, 'LU', CONVERT(NVARCHAR(255), SetLowerSameAsUpper) FROM Deleted
	END

	IF UPDATE(ModelTypeID)
	BEGIN
		INSERT INTO tModelHistory (ModelID, Operation, Data)
		SELECT ModelID, 'T', CONVERT(NVARCHAR(255), ModelTypeID) FROM Deleted
	END
END
