﻿-- Model Input Insert Trigger
CREATE TRIGGER [Monitoring].[tr_tModelInput_Insert]
   ON  [Monitoring].[tModelInput]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Monitoring.tModelHistory (ModelID, Operation, Data)
	SELECT ModelID, 'AI', ProcessData.tTag.TagName
	FROM Inserted
		INNER JOIN ProcessData.tAssetVariableTypeTagMap ON Inserted.AssetVariableTypeTagMapID = ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID
		INNER JOIN ProcessData.tTag ON ProcessData.tAssetVariableTypeTagMap.TagID = ProcessData.tTag.PDTagID
END
