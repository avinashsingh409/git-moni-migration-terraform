﻿-- Model Insert Trigger
CREATE TRIGGER [Monitoring].[tr_tModel_Insert]
   ON  [Monitoring].[tModel]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO tModelHistory (ModelID, Operation) SELECT ModelID, 'C' FROM Inserted
END
