﻿-- Model Input Delete Trigger
CREATE TRIGGER [Monitoring].[tr_tModelInput_Delete]
   ON  [Monitoring].[tModelInput]
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Monitoring.tModelHistory (ModelID, Operation, Data)
	SELECT ModelID, 'RI', ProcessData.tTag.TagName
	FROM Deleted
		INNER JOIN ProcessData.tAssetVariableTypeTagMap ON Deleted.AssetVariableTypeTagMapID = ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID
		INNER JOIN ProcessData.tTag ON ProcessData.tAssetVariableTypeTagMap.TagID = ProcessData.tTag.PDTagID
END
