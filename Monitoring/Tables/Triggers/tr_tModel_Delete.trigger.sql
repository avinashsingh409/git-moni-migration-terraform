﻿-- Model Delete Trigger
CREATE TRIGGER [Monitoring].[tr_tModel_Delete]
   ON  [Monitoring].[tModel]
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	INSERT INTO tModelHistory (ModelID, Operation, Data)
	SELECT ModelID, 'D', CONVERT(NVARCHAR(255), AssetVariableTypeTagMapID) FROM Deleted
END
