﻿CREATE TABLE [Monitoring].[tNDOpModeDefinitionAssetTagMap] (
    [OpModeDefinitionAssetTagMapID] INT IDENTITY (1, 1) NOT NULL,
    [OpModeDefinitionID]            INT NOT NULL,
    [AssetID]                       INT NULL,
    [AssetVariableTypeTagMapID]     INT NULL,
    [Include]                       BIT NOT NULL,
    [BringDescendants]              BIT NOT NULL,
    CONSTRAINT [PK_tNDOpModeDefinitionAssetTagMap] PRIMARY KEY CLUSTERED ([OpModeDefinitionAssetTagMapID] ASC),
    CONSTRAINT [FK_tNDOpModeDefinitionAssetTagMap_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tNDOpModeDefinitionAssetTagMap_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tNDOpModeDefinitionAssetTagMap_tNDOpModeDefinition_OpModeDefinitionID] FOREIGN KEY ([OpModeDefinitionID]) REFERENCES [Monitoring].[tNDOpModeDefinition] ([OpModeDefinitionID])
);

GO

CREATE INDEX [IDX_AssetID_Include] ON [Monitoring].[tNDOpModeDefinitionAssetTagMap] ([AssetID], [Include])  WITH (FILLFACTOR=100);
GO






