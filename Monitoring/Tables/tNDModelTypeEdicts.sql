﻿CREATE TABLE [Monitoring].[tNDModelTypeEdicts] (
    [ModelTypeEdictID]        INT NOT NULL,
    [InputsAvailable]         BIT NOT NULL,
    [AltAreaAvailable]        BIT NOT NULL,
    [AltFrequencyAvailable]   BIT NOT NULL,
    [AltOscillationAvailable] BIT NOT NULL,
    [AltHighHighAvailable]    BIT NOT NULL,
    [AltLowLowAvailable]      BIT NOT NULL,
    [AltFrozenDataAvailable]  BIT NOT NULL,
    [AnoMAEAvailable]         BIT NOT NULL,
    [AnoBiasAvailable]        BIT NOT NULL,
    [AnoFixedLimitAvailable]  BIT NOT NULL,
    [DataAvailable]           BIT NOT NULL,
    [MustHaveAnoMAEOrAnoBias] BIT NOT NULL,
    [MustHaveAnoFixedLimit]   BIT NOT NULL,
    [MustHaveAltFrozenData]   BIT NOT NULL,
    [PredictiveMethodTypeID]  INT NULL,
    CONSTRAINT [PK_tNDModelTypeEdicts] PRIMARY KEY CLUSTERED ([ModelTypeEdictID] ASC),
    CONSTRAINT [FK_tEdicts_tPredictiveMethodType_PredictiveMethodTypeID] FOREIGN KEY ([PredictiveMethodTypeID]) REFERENCES [Monitoring].[tNDPredictiveMethodType] ([PredictiveMethodTypeID])
);

