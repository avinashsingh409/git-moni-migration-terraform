﻿CREATE TABLE [Monitoring].[tNDModelActionItemType] (
    [ModelActionItemTypeID]     INT            NOT NULL,
    [ModelActionItemTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [ModelActionItemTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tNDModelActionItemType] PRIMARY KEY CLUSTERED ([ModelActionItemTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDModelActionItemType';

