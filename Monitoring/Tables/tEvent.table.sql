﻿--Event table
CREATE TABLE [Monitoring].[tEvent] (
    [EventID]                 INT      IDENTITY (1, 1) NOT NULL,
    [StartTime]               DATETIME NOT NULL,
    [StopTime]                DATETIME NULL,
    [EventTypeID]             INT      NOT NULL,
    [AssetID]                 INT      NOT NULL,
    [ExcludeFromCalculations] BIT      CONSTRAINT [D_ExcludeFromCalculations] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tEvent] PRIMARY KEY CLUSTERED ([EventID] ASC),
    CONSTRAINT [FK_tEvent_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tEvent_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Monitoring].[tEventType] ([EventTypeID])
);





 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tEvent';

