﻿CREATE TABLE [Monitoring].[tNDMultipleTagPreprocessingType] (
    [MultipleTagPreprocessingTypeID]     INT            NOT NULL,
    [MultipleTagPreprocessingTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [MultipleTagPreprocessingTypeDesc]   NVARCHAR (255) NOT NULL,
    [DisplayOrder]                       INT            NOT NULL,
    CONSTRAINT [PK_tNDMultipleTagPreprocessingType] PRIMARY KEY CLUSTERED ([MultipleTagPreprocessingTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDMultipleTagPreprocessingType';

