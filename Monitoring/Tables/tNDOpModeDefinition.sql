﻿CREATE TABLE [Monitoring].[tNDOpModeDefinition] (
    [OpModeDefinitionID]    INT            IDENTITY (1, 1) NOT NULL,
    [OpModeDefinitionExtID] NVARCHAR (255) NOT NULL,
    [OpModeDefinitionTitle] NVARCHAR (255) NOT NULL,
    [OpModeTypeID]          INT            NOT NULL,
    [CreatedByUserID]       INT            NOT NULL,
    [CreateDate]            DATETIME       CONSTRAINT [DF_tOpModeDefinition_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]       INT            NOT NULL,
    [ChangeDate]            DATETIME       CONSTRAINT [DF_tOpModeDefinition_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tOpModeDefinition] PRIMARY KEY CLUSTERED ([OpModeDefinitionID] ASC),
    CONSTRAINT [FK_tOpModeDefinition_tNDOpModeType_OpModeTypeID] FOREIGN KEY ([OpModeTypeID]) REFERENCES [Monitoring].[tNDOpModeType] ([OpModeTypeID]),
    CONSTRAINT [FK_tOpModeDefinition_tUser_ChangedBySecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tOpModeDefinition_tUser_CreatedBySecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tNDOpModeDefinition_OpModeDefinitionExtID] UNIQUE NONCLUSTERED ([OpModeDefinitionExtID] ASC)
);


GO

CREATE TRIGGER [Monitoring].[tr_tNDOpModeDefinition_Delete]
	ON [Monitoring].[tNDOpModeDefinition]
	FOR DELETE
AS
BEGIN

	DECLARE @OpModeDefinitionID as int
	DECLARE @OK as int
	
	DECLARE curDEL INSENSITIVE CURSOR
		FOR
			SELECT d.OpModeDefinitionID FROM deleted d
	
	OPEN curDEL;
	FETCH NEXT FROM curDEL INTO @OpModeDefinitionID
	
	SET @OK = 1
	WHILE @OK = 1
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
					
					DELETE FROM [Monitoring].[tNDOpModeDefinitionAssetTagMap] WHERE OpModeDefinitionID = @OpModeDefinitionID
					
					DELETE FROM [Monitoring].[tNDCriteriaGroup] WHERE OpModeDefinitionID = @OpModeDefinitionID
					
				FETCH NEXT FROM curDEL INTO @OpModeDefinitionID	
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	
	CLOSE curDEL;
	DEALLOCATE curDEL;
	
END