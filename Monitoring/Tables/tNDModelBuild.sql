﻿CREATE TABLE [Monitoring].[tNDModelBuild] (
    [ModelBuildID]      INT            IDENTITY (1, 1) NOT NULL,
    [ModelExtID]        NVARCHAR (255) NOT NULL,
    [BuildStatusTypeID] INT            NOT NULL,
    [BuildStatus]       NVARCHAR (MAX) NULL,
    [BuildStatusUserID] INT            NOT NULL,
    [BuildStatusTime]   DATETIME       NOT NULL,
    CONSTRAINT [PK_tNDModelBuild] PRIMARY KEY CLUSTERED ([ModelBuildID] ASC),
    CONSTRAINT [FK_tNDModelBuild_tNDModelBuildStatusType_BuildStatusTypeID] FOREIGN KEY ([BuildStatusTypeID]) REFERENCES [Monitoring].[tNDModelBuildStatusType] ([ModelBuildStatusTypeID]),
    CONSTRAINT [FK_tNDModelBuild_tUser_SecurityUserID] FOREIGN KEY ([BuildStatusUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);






GO
CREATE NONCLUSTERED INDEX [IX_tNDModelBuild_ModelExtID]
    ON [Monitoring].[tNDModelBuild]([ModelBuildID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_Monitoring_tNDModelBuild_ModelExtID]
    ON [Monitoring].[tNDModelBuild]([ModelExtID] ASC);

GO
CREATE INDEX [IX_BuildStatusTypeID] ON [Monitoring].[tNDModelBuild] ([BuildStatusTypeID])  WITH (FILLFACTOR=100);


GO