﻿CREATE TABLE [Monitoring].[tNDModelType] (
    [ModelTypeID]      INT            NOT NULL,
    [ModelTypeAbbrev]  NVARCHAR (50)  NOT NULL,
    [ModelTypeDesc]    NVARCHAR (255) NOT NULL,
    [ModelTypeEdictID] INT            NOT NULL,
    [DisplayOrder]     INT            NOT NULL,
    CONSTRAINT [PK_tNDModelType] PRIMARY KEY CLUSTERED ([ModelTypeID] ASC),
    CONSTRAINT [FK_tModelType_tModelTypeEdicts_ModelTypeEdictID] FOREIGN KEY ([ModelTypeEdictID]) REFERENCES [Monitoring].[tNDModelTypeEdicts] ([ModelTypeEdictID])
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDModelType';

