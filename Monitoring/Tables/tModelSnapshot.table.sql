﻿CREATE TABLE [Monitoring].[tModelSnapshot] (
    [ModelSnapshotID]       INT           IDENTITY (1, 1) NOT NULL,
    [ModelID]               INT           NOT NULL,
    [Timestamp]             DATETIME      NULL,
    [ActualValue]           REAL          NULL,
    [ExpectedValue]         REAL          NULL,
    [UpperLimit]            REAL          NULL,
    [LowerLimit]            REAL          NULL,
    [LastAnomaly]           DATETIME      NULL,
    [StateChanged]          BIT           CONSTRAINT [DF_Monitoring_tModelSnapshot_IsChanged] DEFAULT ((0)) NOT NULL,
    [CalculationStatus]     TINYINT       DEFAULT ((0)) NOT NULL,
    [CalculationStatusInfo] VARCHAR (255) NULL,
    CONSTRAINT [PK_Monitoring_tModelSnapshot] PRIMARY KEY CLUSTERED ([ModelSnapshotID] ASC),
    CONSTRAINT [FK_Monitoring_tModelSnapshot_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE,
    CONSTRAINT [IX_tModelSnapshot_ModelID] UNIQUE NONCLUSTERED ([ModelID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelSnapshot';

