﻿CREATE TABLE [Monitoring].[tNDLogicOperatorCategoryType] (
    [LogicOperatorCategoryTypeID]     INT            NOT NULL,
    [LogicOperatorCategoryTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [LogicOperatorCategoryTypeDesc]   NVARCHAR (255) NOT NULL,
    [LogicOperatorCategoryTypeLabel]  NVARCHAR (50)  NULL,
    [DisplayOrder]                    INT            NOT NULL,
    CONSTRAINT [PK_tNDLogicOperatorCategoryType] PRIMARY KEY CLUSTERED ([LogicOperatorCategoryTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDLogicOperatorCategoryType';

