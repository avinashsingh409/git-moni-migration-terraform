﻿CREATE TABLE [Monitoring].[tNDModelIssueMap] (
		[ModelIssueMapID] INTEGER  NOT NULL  
		, [ModelID] INTEGER  NOT NULL  
		, [AssetIssueID] INTEGER  NOT NULL  
		, CONSTRAINT [tNDModelIssueMap_PK] PRIMARY KEY CLUSTERED ([ModelIssueMapID])
		, CONSTRAINT [tAssetIssue_tNDModelIssueMap_FK1] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID])
		, CONSTRAINT [tNDModel_tNDModelIssueMap_FK1] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID])
		)
GO
