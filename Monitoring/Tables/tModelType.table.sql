CREATE TABLE [Monitoring].[tModelType] (
    [ModelTypeID]   INT            NOT NULL,
    [ModelTypeDesc] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Moitoring_tModelType] PRIMARY KEY CLUSTERED ([ModelTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelType';

