﻿CREATE TABLE [Monitoring].[tNDWatchLimitMeetsTemporalType] (
		[WatchLimitMeetsTemporalTypeID] INTEGER  NOT NULL  
		, [WatchLimitMeetsTemporalTypeAbbrev] nvarchar(50)  NOT NULL  
		, [WatchLimitMeetsTemporalTypeDesc] nvarchar(255)  NOT NULL  
		, CONSTRAINT [tNDWatchLimitMeetsTemporalType_PK] PRIMARY KEY CLUSTERED ([WatchLimitMeetsTemporalTypeID])
		)
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDWatchLimitMeetsTemporalType'