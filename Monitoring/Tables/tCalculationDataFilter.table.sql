﻿CREATE TABLE [Monitoring].[tCalculationDataFilter] (
    [CalculationDataFilterID] INT           IDENTITY (1, 1) NOT NULL,
    [UnitAssetID]             INT           NOT NULL,
    [ResultDate]              DATETIME      NOT NULL,
    [CurrentStartDate]        DATETIME      NOT NULL,
    [InputEndDate]            DATETIME      NOT NULL,
    [InputStartDate]          DATETIME      NOT NULL,
    [BucketTagName]           NVARCHAR (81) NULL,
    [BucketLowerLimit]        REAL          NULL,
    [BucketUpperLimit]        REAL          NULL,
    CONSTRAINT [PK_tCalculationDataFilter] PRIMARY KEY CLUSTERED ([CalculationDataFilterID] ASC),
    CONSTRAINT [UK_Monitoring_tCalculationDataFilter] UNIQUE NONCLUSTERED ([UnitAssetID] ASC, [ResultDate] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tCalculationDataFilter';

