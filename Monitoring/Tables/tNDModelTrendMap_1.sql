﻿CREATE TABLE [Monitoring].[tNDModelTrendMap] (
    [NDModelTrendMapID]    INT      IDENTITY (1, 1) NOT NULL,
    [ModelID]              INT      NOT NULL,
    [PDTrendID]            INT      NOT NULL,
    [AssetID]              INT      NOT NULL,
    [ReplaceTrendContents] BIT      CONSTRAINT [DF_tNDModelTrendMap_ReplaceTrendContents] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]         INT      NOT NULL,
    [CreatedByUserID]      INT      NOT NULL,
    [CreateDate]           DATETIME CONSTRAINT [DF_tNDModelTrendMap_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]      INT      NOT NULL,
    [ChangeDate]           DATETIME CONSTRAINT [DF_tNDModelTrendMap_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tNDModelTrendMap] PRIMARY KEY CLUSTERED ([NDModelTrendMapID] ASC),
    CONSTRAINT [FK_tNDModelTrendMap_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tNDModelTrendMap_tNDModel_ModelID] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tNDModelTrendMap_tTrend_PDTrendID] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tNDModelTrendMap_tUser_ChangedByUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tNDModelTrendMap_tUser_CreatedByUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tNDModelTrendMap_ModelID_PDTrendID_AssetID] UNIQUE NONCLUSTERED ([ModelID] ASC, [PDTrendID] ASC, [AssetID] ASC)
);



