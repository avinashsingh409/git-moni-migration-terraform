﻿CREATE TABLE [Monitoring].[tNDAlertPriorityType] (
		[AlertPriorityTypeID] INTEGER  NOT NULL  
		, [AlertPriorityTypeAbbrev] nvarchar(50)  NOT NULL  
		, [AlertPriorityTypeDesc] nvarchar(255)  NOT NULL  
		, [AlertPriorityLevel] nvarchar(50)  NOT NULL  
		, CONSTRAINT [tNDAlertPriorityType_PK] PRIMARY KEY CLUSTERED ([AlertPriorityTypeID])
		)
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDAlertPriorityType'