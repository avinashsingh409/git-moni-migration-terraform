﻿CREATE TABLE [Monitoring].[tNDRecInputVariableTargetType] (
    [RecInputVariableTargetTypeID]     INT            NOT NULL,
    [RecInputVariableTargetTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [RecInputVariableTargetTypeDesc]   NVARCHAR (255) NOT NULL,
    [DisplayOrder]                     INT            NOT NULL,
    CONSTRAINT [PK_tNDModelInputVariableLookupType] PRIMARY KEY CLUSTERED ([RecInputVariableTargetTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDRecInputVariableTargetType';

