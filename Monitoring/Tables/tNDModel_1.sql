﻿CREATE TABLE [Monitoring].[tNDModel] (
    [ModelID]                     INT            IDENTITY (1, 1) NOT NULL,
    [NDProjectPath]               NVARCHAR (400) NOT NULL,
    [NDProjectName]               NVARCHAR (255) NOT NULL,
    [NDResultsPath]               NVARCHAR (400) NOT NULL,
    [ModelExtID]                  NVARCHAR (255) NOT NULL,
    [ModelName]                   NVARCHAR (255) NOT NULL,
    [ModelDesc]                   NVARCHAR (512) NULL,
    [ModelTemplateID]             INT            NULL,
    [AssetVariableTypeTagMapID]   INT            NOT NULL,
    [ModelTypeID]                 INT            NOT NULL,
    [OpModeParamsID]              INT            NOT NULL,
    [Temporal]                    BIT            NOT NULL,
    [Built]                       BIT            NOT NULL,
    [BuiltSinceTime]              DATETIME       NULL,
    [Active]                      BIT            NOT NULL,
    [ActiveSinceTime]             DATETIME       NULL,
    [PreserveAlertStatus]         BIT            NULL,
    [AlertStatusTypeID]           INT            NULL,
    [IsPublic]                    BIT            NOT NULL,
    [DisplayOrder]                INT            NULL,
    [CreatedByUserID]             INT            NOT NULL,
    [CreateDate]                  DATETIME       CONSTRAINT [DF_tNDModel_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]             INT            NOT NULL,
    [ChangeDate]                  DATETIME       CONSTRAINT [DF_tNDModel_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [BuildInterval]               INT            NULL,
    [BuildIntervalTemporalTypeID] INT            NULL,
    [LastInterrogatedTime]        DATETIME       NULL,
    [CalcPointID]                 INT            NULL,
    [Release]                     BIT            CONSTRAINT [DF_tNDModel_Release] DEFAULT ((1)) NOT NULL,
    [QueueForBuild]               BIT            DEFAULT ((0)) NOT NULL,
    [AutoRetrain]                 BIT            CONSTRAINT [DF_tNDModel_AutoRetrain] DEFAULT ((0)) NOT NULL,
    [ReferenceModelID]            NVARCHAR (400) NULL,
    [Locked]                      BIT            CONSTRAINT [DF_tNDModel_LockedDefault] DEFAULT ((0)) NOT NULL,
    [LockReasonID]                INT            NULL,
    [LockedSinceTime]             DATETIME       NULL,
	[ModelConfigURL]			  VARCHAR (2048) NULL,
	[DiagnosticDrilldownURL]	  VARCHAR (2048) NULL,
	[IsStandard]				  BIT			 CONSTRAINT [DF_tNDModel_IsStandard] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tNDModel] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [CHK_tNDModel_LockedSinceTime] CHECK ([Locked]=(0) OR [LockedSinceTime] IS NOT NULL),
    CONSTRAINT [CHK_tNDModel_LockReasonID] CHECK ([Locked]=(0) OR [LockReasonID] IS NOT NULL),
    CONSTRAINT [FK_tNDModel_tAssetVariableTypeTagMap_AssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]),
    CONSTRAINT [FK_tNDModel_tCalcPoint_CalcPointID] FOREIGN KEY ([CalcPointID]) REFERENCES [ProcessData].[tCalcPoint] ([CalcPointID]),
    CONSTRAINT [FK_tNDModel_tNDAlertStatusType_AlertStatusTypeID] FOREIGN KEY ([AlertStatusTypeID]) REFERENCES [Monitoring].[tNDAlertStatusType] ([AlertStatusTypeID]),
    CONSTRAINT [FK_tNDModel_tNDModel] FOREIGN KEY ([BuildIntervalTemporalTypeID]) REFERENCES [Monitoring].[tNDTemporalType] ([TemporalTypeID]),
    CONSTRAINT [FK_tNDModel_tNDModelLockReasonType_LockReasonID] FOREIGN KEY ([LockReasonID]) REFERENCES [Monitoring].[tNDModelLockReasonType] ([LockReasonID]),
    CONSTRAINT [FK_tNDModel_tNDModelTemplate_ModelTemplateID] FOREIGN KEY ([ModelTemplateID]) REFERENCES [Monitoring].[tNDModelTemplate] ([ModelTemplateID]),
    CONSTRAINT [FK_tNDModel_tNDModelType_ModelTypeID] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tNDModelType] ([ModelTypeID]),
    CONSTRAINT [FK_tNDModel_tNDOpModeParams_OpModeParamsID] FOREIGN KEY ([OpModeParamsID]) REFERENCES [Monitoring].[tNDOpModeParams] ([OpModeParamsID]),
    CONSTRAINT [UK_tNDModel_DepTagMapID_ModelName] UNIQUE NONCLUSTERED ([AssetVariableTypeTagMapID] ASC, [ModelName] ASC),
    CONSTRAINT [UK_tNDModel_ModelExtID] UNIQUE NONCLUSTERED ([ModelExtID] ASC)
);






































GO


CREATE TRIGGER [Monitoring].[tr_tNDModel_Delete]
	ON [Monitoring].[tNDModel]
	INSTEAD OF DELETE
AS
BEGIN

	DECLARE @ModelID as int
	DECLARE @ModelTemplateID as int
	DECLARE @OpModeParamsID as int
	DECLARE @ModelExtID as nvarchar(255)
	DECLARE @OK as int
	
	DECLARE curDEL INSENSITIVE CURSOR
		FOR
			SELECT d.ModelID,d.ModelTemplateID,d.OpModeParamsID,d.ModelExtID FROM [DELETED] d
	
	OPEN curDEL;
	FETCH NEXT FROM curDEL INTO @ModelID,@ModelTemplateID,@OpModeParamsID,@ModelExtID
	
	SET @OK = 1
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN
			
				DELETE FROM [Monitoring].[tNDModelInputTagMap] WHERE ModelID = @ModelID
				
				DELETE FROM [Monitoring].[tNDModelEvaluation] WHERE ModelID = @ModelID
								
				DELETE FROM [Monitoring].[tNDModelPredictiveMethodMap] WHERE ModelID = @ModelID
				
				
				DELETE FROM [Monitoring].[tNDAlertPriority] WHERE ModelID = @ModelID
				DELETE FROM [Monitoring].[tNDModel_AlertScreening_FromND] WHERE ModelID = @ModelID
				DELETE FROM [Monitoring].[tNDModel_AlertScreening_FromAsset360] WHERE ModelID = @ModelID
				DELETE FROM [Monitoring].[tNDModel_AlertScreening] WHERE ModelID = @ModelID
				
				DELETE FROM [Monitoring].[tNDModelActionItem] WHERE ModelID = @ModelID
				
				DELETE FROM [Monitoring].[tNDModelIssueMap] WHERE ModelID = @ModelID

				DELETE FROM [Monitoring].[tNDModelOpModeTypeMap] WHERE ModelID = @ModelID

				DELETE FROM [Monitoring].[tNDModelCriticality] WHERE ModelID = @ModelID

				DELETE FROM [Monitoring].[tModelLatestPredict] WHERE ModelID = @ModelID

				DELETE FROM [Monitoring].[tNDModel] WHERE ModelID = @ModelID
				
				IF @ModelTemplateID IS NULL
					BEGIN
						
						IF NOT ((EXISTS (SELECT OpModeParamsAssetMapID FROM [Monitoring].[tNDOpModeParamsAssetMap] WHERE OpModeParamsID = @OpModeParamsID) OR (EXISTS (SELECT OpModeParamsAssetClassTypeMapID FROM [Monitoring].[tNDOpModeParamsAssetClassTypeMap] WHERE OpModeParamsID = @OpModeParamsID))))
						BEGIN
							IF NOT EXISTS(SELECT ModelID FROM [Monitoring].[tNDModel] WHERE OpModeParamsID = @OpModeParamsID AND ModelID <> @ModelID)
							BEGIN
								DELETE FROM [Monitoring].[tNDOpModeParams] WHERE OpModeParamsID = @OpModeParamsID
							END
						END
					
					END

				IF EXISTS(SELECT ModelBuildID FROM [Monitoring].[tNDModelBuild] WHERE ModelExtID = @ModelExtID)
				BEGIN
					DELETE FROM [Monitoring].[tNDModelBuild] WHERE ModelExtID = @ModelExtID
				END

				IF EXISTS(SELECT BatchId FROM [Monitoring].[tNDBatchModel] WHERE ModelExtID = @ModelExtID)
				BEGIN
					DELETE FROM [Monitoring].[tNDBatchModel] WHERE ModelExtID = @ModelExtID
				END
			
			FETCH NEXT FROM curDEL INTO @ModelID,@ModelTemplateID,@OpModeParamsID,@ModelExtID
			END
		ELSE
			BEGIN
			SET @OK = 0
			END
	
	CLOSE curDEL;
	DEALLOCATE curDEL;
	
END
GO


CREATE TRIGGER [Monitoring].[tNDModel_Update]
ON [Monitoring].[tNDModel]
AFTER UPDATE
AS
BEGIN    
    SET NOCOUNT ON
	DELETE FROM Monitoring.tNDModelEvaluation WHERE ModelID IN (SELECT ModelID from inserted WHERE Active=0)
	
	-- Deleted the update statement that sets the UseDefault flag to 1 if associated with a template
END
GO





CREATE TRIGGER [Monitoring].[tr_tNDModel_Insert]
	ON [Monitoring].[tNDModel]
	AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	insert into Monitoring.tNDModelCriticality (ModelID, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate)
	select ModelID, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate from inserted
	;

	--Set the default model criticality values for the newly created criticality record. 
	DECLARE @modelIds Base.tpIntList;
	INSERT INTO @modelIds 
	SELECT ModelID FROM inserted	

	UPDATE crit
	SET crit.LowerCriticality = defaultCrit.LowerCriticality,
	 crit.UpperCriticality = defaultCrit.UpperCriticality
	FROM Monitoring.tNDModelCriticality crit
	JOIN 
	(SELECT ModelID, UpperCriticality, LowerCriticality 
		FROM Monitoring.ufnGetConfigCriticality(@modelIds)) defaultCrit
	ON defaultCrit.ModelID = crit.ModelID
END
GO
CREATE INDEX [IDX_tNDModel_Built_Active_CalcPointID] ON [Monitoring].[tNDModel] ([Built], [Active], [CalcPointID]) INCLUDE ([AssetVariableTypeTagMapID]) ;


GO
CREATE INDEX [IX_ReferenceModelID] ON [Monitoring].[tNDModel] ([ReferenceModelID])  WITH (FILLFACTOR=100);


GO
CREATE INDEX [IX_Locked_Includes] ON [Monitoring].[tNDModel] ([Locked])  INCLUDE ([AssetVariableTypeTagMapID], [CreatedByUserID], [ChangedByUserID]) WITH (FILLFACTOR=100);


GO
CREATE INDEX 
	[IX_OpModeParamsID_Active_Includes] 
ON 
	[Monitoring].[tNDModel] ([OpModeParamsID], [Active])  
INCLUDE 
	([NDProjectPath], [ModelExtID], [ModelName], [ModelTemplateID], [AssetVariableTypeTagMapID], [ModelTypeID], [Built], [CalcPointID], [ReferenceModelID], [ModelConfigURL], [DiagnosticDrilldownURL]) 
WITH 
	(FILLFACTOR=100);