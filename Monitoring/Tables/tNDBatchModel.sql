﻿CREATE TABLE [Monitoring].[tNDBatchModel] (
    [BatchModel] INT              IDENTITY (1, 1) NOT NULL,
    [BatchId]    UNIQUEIDENTIFIER NOT NULL,
    [ModelExtID] NVARCHAR (255)   NOT NULL,
    CONSTRAINT [PK_tNDBatchModel] PRIMARY KEY CLUSTERED ([BatchModel] ASC),
    CONSTRAINT [FK_tNDBatchModel_tNDBatch_BatchId] FOREIGN KEY ([BatchId]) REFERENCES [Monitoring].[tNDBatch] ([BatchId]) ON DELETE CASCADE,
    CONSTRAINT [UK_tNDBatchModel] UNIQUE NONCLUSTERED ([BatchId] ASC, [ModelExtID] ASC)
);





