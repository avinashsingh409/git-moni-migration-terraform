﻿CREATE TABLE [Monitoring].[tNDLogicOperatorType] (
    [LogicOperatorTypeID]         INT            NOT NULL,
    [LogicOperatorTypeAbbrev]     NVARCHAR (50)  NOT NULL,
    [LogicOperatorTypeDesc]       NVARCHAR (255) NOT NULL,
    [LogicOperatorCategoryTypeID] INT            NOT NULL,
    [DisplayOrder]                INT            NOT NULL,
    CONSTRAINT [PK_tNDLogicOperatorType] PRIMARY KEY CLUSTERED ([LogicOperatorTypeID] ASC),
    CONSTRAINT [FK_tLogicOperatorType_tLogicOperatorCategoryType_LogicOperatorCategoryTypeID] FOREIGN KEY ([LogicOperatorCategoryTypeID]) REFERENCES [Monitoring].[tNDLogicOperatorCategoryType] ([LogicOperatorCategoryTypeID])
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDLogicOperatorType';

