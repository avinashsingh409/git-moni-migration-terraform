﻿CREATE TABLE [Monitoring].[tNDOpModeParamsAssetClassTypeMap] (
    [OpModeParamsAssetClassTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [OpModeTypeID]                    INT NOT NULL,
    [OpModeParamsID]                  INT NOT NULL,
    [AssetClassTypeID]                INT NOT NULL,
    CONSTRAINT [PK_tNDOpModeParamsAssetClassTypeMap] PRIMARY KEY CLUSTERED ([OpModeParamsAssetClassTypeMapID] ASC),
    CONSTRAINT [FK_tNDOpModeParamsAssetClassTypeMap_tAssetClassType_AssetClassTypeID] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tNDOpModeParamsAssetClassTypeMap_tNDOpModeParams_OpModeParamsID] FOREIGN KEY ([OpModeParamsID]) REFERENCES [Monitoring].[tNDOpModeParams] ([OpModeParamsID]),
    CONSTRAINT [FK_tNDOpModeParamsAssetClassTypeMap_tNDOpModeType_OpModeTypeID] FOREIGN KEY ([OpModeTypeID]) REFERENCES [Monitoring].[tNDOpModeType] ([OpModeTypeID]),
    CONSTRAINT [UK_tNDOpModeParamsAssetClassTypeMap_OpModeTypeID_AssetClassTypeID] UNIQUE NONCLUSTERED ([OpModeTypeID] ASC, [AssetClassTypeID] ASC)
);


GO

CREATE TRIGGER [Monitoring].[tr_tNDOpModeParamsAssetClassTypeMap_Delete]
	ON [Monitoring].[tNDOpModeParamsAssetClassTypeMap]
	FOR DELETE
AS
BEGIN

	DECLARE @OpModeParamsID as int
	DECLARE @OK as int
	
	DECLARE curDEL INSENSITIVE CURSOR
		FOR
			SELECT d.OpModeParamsID FROM deleted d
	
	OPEN curDEL;
	FETCH NEXT FROM curDEL INTO @OpModeParamsID
	
	SET @OK = 1
	WHILE @OK = 1
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
				
					DELETE FROM [Monitoring].[tNDModel] WHERE OpModeParamsID = @OpModeParamsID
					
					DELETE FROM [Monitoring].[tNDOpModeParams] WHERE OpModeParamsID = @OpModeParamsID
				
				FETCH NEXT FROM curDEL INTO @OpModeParamsID
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		
		END
	
	CLOSE curDEL
	DEALLOCATE curDEL;

END
GO
CREATE INDEX
	[IX_OpModeParamsID]
ON
	[Monitoring].[tNDOpModeParamsAssetClassTypeMap] ([OpModeParamsID])
WITH
	(FILLFACTOR=100)