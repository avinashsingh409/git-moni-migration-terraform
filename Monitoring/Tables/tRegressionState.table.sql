CREATE TABLE [Monitoring].[tRegressionState] (
    [UnitAssetID]     INT             NOT NULL,
    [LastResultsTime] DATETIME        NULL,
    [DataLength]      INT             NULL,
    [RegressionState] VARBINARY (MAX) NULL,
    CONSTRAINT [PK_Monitoring_tRegressionState] PRIMARY KEY CLUSTERED ([UnitAssetID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tRegressionState';

