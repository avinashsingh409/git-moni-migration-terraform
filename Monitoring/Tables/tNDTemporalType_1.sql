﻿CREATE TABLE [Monitoring].[tNDTemporalType] (
    [TemporalTypeID]     INT            NOT NULL,
    [TemporalTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [TemporalTypeDesc]   NVARCHAR (255) NOT NULL,
    [DisplayOrder]       INT            NOT NULL,
    CONSTRAINT [PK_tTemporalType] PRIMARY KEY CLUSTERED ([TemporalTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDTemporalType';

