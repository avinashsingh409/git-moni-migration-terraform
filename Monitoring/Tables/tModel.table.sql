﻿CREATE TABLE [Monitoring].[tModel] (
    [ModelID]                   INT            IDENTITY (1, 1) NOT NULL,
    [UnitAssetID]               INT            NOT NULL,
    [ModelTypeID]               INT            NOT NULL,
    [ModelDesc]                 NVARCHAR (100) NULL,
    [AssetVariableTypeTagMapID] INT            NOT NULL,
    [UpperLimitMultiplier]      REAL           CONSTRAINT [DF_Monitoring_tModel_UpperLimitMultiplier] DEFAULT ((1.0)) NOT NULL,
    [UpperLimitAdder]           REAL           CONSTRAINT [DF_Monitoring_tModel_UpperLimitAdder] DEFAULT ((0.0)) NOT NULL,
    [LowerLimitMultiplier]      REAL           CONSTRAINT [DF_Monitoring_tModel_LowerLimitMultiplier] DEFAULT ((1.0)) NOT NULL,
    [LowerLimitAdder]           REAL           CONSTRAINT [DF_Monitoring_tModel_LowerLimitAdder] DEFAULT ((0.0)) NOT NULL,
    [SetLowerSameAsUpper]       BIT            DEFAULT ((1)) NOT NULL,
    [Enabled]                   BIT            CONSTRAINT [DF_tModel_Enabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Monitoring_tModel] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [FK_Monitoring_tModel_AssetVariableTypeTagMapID_tAssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Monitoring_tModel_ModelTypeID_tModelType] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tModelType] ([ModelTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Monitoring_tModel_UnitAsset_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModel';


GO
CREATE NONCLUSTERED INDEX ["FK_ProcessData_AssetVariableTypeTagMap"]
    ON [Monitoring].[tModel]([AssetVariableTypeTagMapID] ASC)
    INCLUDE([ModelID]);


GO
CREATE INDEX [IDX_tModel_UnitAssetID_includes] ON [Monitoring].[tModel] ([UnitAssetID])  INCLUDE ([ModelTypeID], [ModelDesc], [AssetVariableTypeTagMapID], [UpperLimitMultiplier], [UpperLimitAdder], [LowerLimitMultiplier], [LowerLimitAdder], [SetLowerSameAsUpper], [Enabled]) ;