﻿CREATE TABLE [Monitoring].[tNDModelInputTagMap] (
    [ModelInputTagMapID]        INT IDENTITY (1, 1) NOT NULL,
    [ModelID]                   INT NOT NULL,
    [AssetVariableTypeTagMapID] INT NOT NULL,
    [RecInputVariableMapID]     INT NULL,
    [Active]                    BIT CONSTRAINT [DF_tNDModelInputTagMap_Active] DEFAULT ((0)) NOT NULL,
    [Required]                  BIT CONSTRAINT [DF_tNDModelInputTagMap_Required] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tNDModelInputTagMap] PRIMARY KEY CLUSTERED ([ModelInputTagMapID] ASC),
    CONSTRAINT [FK_tNDModelInputTagMap_tAssetVariableTypeTagMap_AssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tNDModelInputTagMap_tNDModel_ModelID] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]),
    CONSTRAINT [FK_tNDModelInputTagMap_tNDRecInputVariableMap_RecInputVariableMapID] FOREIGN KEY ([RecInputVariableMapID]) REFERENCES [Monitoring].[tNDRecInputVariableMap] ([RecInputVariableMapID])
);






GO

CREATE INDEX [IDX_ModelID] ON [Monitoring].[tNDModelInputTagMap] ([ModelID])  WITH (FILLFACTOR=100);
