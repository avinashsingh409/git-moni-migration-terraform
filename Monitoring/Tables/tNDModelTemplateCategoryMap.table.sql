﻿CREATE TABLE [Monitoring].[tNDModelTemplateCategoryMap](
	[CategoryMapID] [int] IDENTITY(1,1) NOT NULL,
	[ModelTemplateID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_tNDModelTemplateCategoryMap] PRIMARY KEY CLUSTERED
(
	[CategoryMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UK_tNDModelTemplateCategoryMap] UNIQUE NONCLUSTERED
(
	[CategoryID] ASC,
	[ModelTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [Monitoring].[tNDModelTemplateCategoryMap]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID])
GO

ALTER TABLE [Monitoring].[tNDModelTemplateCategoryMap]  WITH CHECK ADD  CONSTRAINT [FK_tNDModelTemplateToCategoryMap_tCategoryMapModelTemplateID] FOREIGN KEY([ModelTemplateID])
REFERENCES [Monitoring].[tNDModelTemplate] ([ModelTemplateID])
ON DELETE CASCADE
GO