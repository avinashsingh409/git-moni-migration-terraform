﻿CREATE TABLE [Monitoring].[tNDModel_AlertScreening_FromND] (
		[ModelID] INTEGER  NOT NULL  
		, [NDAlertStatus] BIT  NOT NULL  
		, [PercentOOB] real  NOT NULL  
		, [Actual] real  NOT NULL  
		, [LowerLimit] real  NOT NULL  
		, [Expected] real  NOT NULL  
		, [UpperLimit] real  NOT NULL  
		, CONSTRAINT [tNDModel_AlertScreening_tNDModel_AlertScreening_FromND_FK1] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel_AlertScreening] ([ModelID])	
		, CONSTRAINT [tNDModel_AlertScreening_FromND_PK] PRIMARY KEY CLUSTERED ([ModelID])
)
