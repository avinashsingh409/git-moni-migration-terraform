﻿CREATE TABLE [Monitoring].[tNDModel] (
    [ModelID]                   INT            IDENTITY (1, 1) NOT NULL,
    [ModelExtID]                NVARCHAR (255) NOT NULL,
    [ModelName]                 NVARCHAR (255) NOT NULL,
    [ModelDesc]                 NVARCHAR (255) NULL,
    [ModelTemplateID]           INT            NULL,
    [AssetVariableTypeTagMapID] INT            NOT NULL,
    [ModelTypeID]               INT            NOT NULL,
    [OpModeTypeID]              INT            NOT NULL,
    [OpModeParamsID]            INT            NOT NULL,
    [Temporal]                  BIT            NOT NULL,
    [Built]                     BIT            NOT NULL,
    [BuiltSinceTime]            DATETIME       NULL,
    [Active]                    BIT            NOT NULL,
    [ActiveSinceTime]           DATETIME       NULL,
    [PreserveAlertStatus]       BIT            NULL,
    [AlertStatusTypeID]         INT            NULL,
    [IsPublic]                  BIT            NOT NULL,
    [DisplayOrder]              INT            NULL,
    [CreatedByUserID]           INT            NOT NULL,
    [CreateDate]                DATETIME       CONSTRAINT [DF_tNDModel_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]           INT            NOT NULL,
    [ChangeDate]                DATETIME       CONSTRAINT [DF_tNDModel_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tNDModel] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [FK_tNDModel_tAssetVariableTypeTagMap_AssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]),
    CONSTRAINT [FK_tNDModel_tNDAlertStatusType_AlertStatusTypeID] FOREIGN KEY ([AlertStatusTypeID]) REFERENCES [Monitoring].[tNDAlertStatusType] ([AlertStatusTypeID]),
    CONSTRAINT [FK_tNDModel_tNDModelTemplate_ModelTemplateID] FOREIGN KEY ([ModelTemplateID]) REFERENCES [Monitoring].[tNDModelTemplate] ([ModelTemplateID]),
    CONSTRAINT [FK_tNDModel_tNDModelType_ModelTypeID] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tNDModelType] ([ModelTypeID]),
    CONSTRAINT [FK_tNDModel_tNDOpModeParams_OpModeParamsID] FOREIGN KEY ([OpModeParamsID]) REFERENCES [Monitoring].[tNDOpModeParams] ([OpModeParamsID]),
    CONSTRAINT [FK_tNDModel_tNDOpModeType_OpModeTypeID] FOREIGN KEY ([OpModeTypeID]) REFERENCES [Monitoring].[tNDOpModeType] ([OpModeTypeID]),
    CONSTRAINT [UK_tNDModel_DepTagMapID_OpModeTypeID] UNIQUE NONCLUSTERED ([AssetVariableTypeTagMapID] ASC, [OpModeTypeID] ASC),
    CONSTRAINT [UK_tNDModel_ModelExtID] UNIQUE NONCLUSTERED ([ModelExtID] ASC)
);


GO

CREATE TRIGGER [Monitoring].[tr_tNDModel_Delete]
	ON [Monitoring].[tNDModel]
	FOR DELETE
AS
BEGIN

	DECLARE @ModelID as int
	DECLARE @ModelTemplateID as int
	DECLARE @OpModeParamsID as int
	DECLARE @OK as int
	
	DECLARE curDEL INSENSITIVE CURSOR
		FOR
			SELECT d.ModelID,d.ModelTemplateID,d.OpModeParamsID FROM deleted d
	
	OPEN curDEL;
	FETCH NEXT FROM curDEL INTO @ModelID,@ModelTemplateID,@OpModeParamsID
	
	SET @OK = 1
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN
			
				IF @ModelTemplateID IS NULL
					BEGIN
					
						DELETE FROM [Monitoring].[tNDOpModeParams] WHERE OpModeParamsID = @OpModeParamsID
					
					END
				
				DELETE FROM [Monitoring].[tNDModelInputTagMap] WHERE ModelID = @ModelID
				
				DELETE FROM [Monitoring].[tNDModelPredictiveMethodMap] WHERE ModelID = @ModelID
                
				DELETE FROM [Monitoring].[tModelLatestPredict] WHERE ModelID = @ModelID
			
			FETCH NEXT FROM curDEL INTO @ModelID,@ModelTemplateID,@OpModeParamsID
			END
		ELSE
			BEGIN
			SET @OK = 0
			END
	
	CLOSE curDEL;
	DEALLOCATE curDEL;

END