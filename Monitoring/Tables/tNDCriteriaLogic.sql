﻿CREATE TABLE [Monitoring].[tNDCriteriaLogic] (
    [CriteriaLogicID]                  INT  IDENTITY (1, 1) NOT NULL,
    [CriteriaGroupID]                  INT  NOT NULL,
    [AssetVariableTypeTagMapID]        INT  NOT NULL,
    [LogicOperatorTypeID]              INT  NOT NULL,
    [Value]                            REAL NULL,
    [TransiencyStdDevMultiplier]       REAL NULL,
    [TransiencyDuration]               INT  NULL,
    [TransiencyDurationTemporalTypeID] INT  NULL,
    [DisplayOrder]                     INT  NULL,
    CONSTRAINT [PK_tNDCriteriaLogic] PRIMARY KEY CLUSTERED ([CriteriaLogicID] ASC),
    CONSTRAINT [FK_tNDCriteriaLogic_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]),
    CONSTRAINT [FK_tNDCriteriaLogic_tNDCriteriaGroup_CriteriaGroupID] FOREIGN KEY ([CriteriaGroupID]) REFERENCES [Monitoring].[tNDCriteriaGroup] ([CriteriaGroupID]),
    CONSTRAINT [FK_tNDCriteriaLogic_tNDLogicOperatorType_LogicOperatorTypeID] FOREIGN KEY ([LogicOperatorTypeID]) REFERENCES [Monitoring].[tNDLogicOperatorType] ([LogicOperatorTypeID])
);

