﻿CREATE TABLE [Monitoring].[tNDModel_AlertScreening_FromAsset360] (
		[ModelID] INTEGER  NOT NULL  
		, [Unit] nvarchar(50)  NOT NULL  
		, [HasOpenIssue] BIT  NOT NULL  
		, CONSTRAINT [tNDModel_AlertScreening_tNDModel_AlertScreening_FromAsset360_FK1] FOREIGN KEY ([ModelID])	REFERENCES [Monitoring].[tNDModel_AlertScreening] ([ModelID])
		, CONSTRAINT [tNDModel_AlertScreening_FromAsset360_PK] PRIMARY KEY CLUSTERED ([ModelID])
		)
