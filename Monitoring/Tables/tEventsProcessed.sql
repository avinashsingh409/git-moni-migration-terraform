﻿CREATE TABLE [Monitoring].[tEventsProcessed] (
    [EventsProcessedID] INT      IDENTITY (1, 1) NOT NULL,
    [AssetID]           INT      NOT NULL,
    [LastProcessedTime] DATETIME NOT NULL,
    CONSTRAINT [PK_tEventsProcessed] PRIMARY KEY CLUSTERED ([EventsProcessedID] ASC),
    CONSTRAINT [FK_tEventsProcessed_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tEventsProcessed] UNIQUE NONCLUSTERED ([AssetID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tEventsProcessed';

