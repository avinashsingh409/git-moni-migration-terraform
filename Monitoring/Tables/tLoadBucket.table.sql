CREATE TABLE [Monitoring].[tLoadBucket] (
    [LoadBucketID] INT      IDENTITY (1, 1) NOT NULL,
    [UnitAssetID]  INT      NOT NULL,
    [BucketNo]     SMALLINT NOT NULL,
    [LowerLimit]   REAL     NULL,
    [UpperLimit]   REAL     NULL,
    CONSTRAINT [PK_Monitoring_tLoadBucket] PRIMARY KEY CLUSTERED ([LoadBucketID] ASC),
    CONSTRAINT [FK_Monitoring_tLoadBucket_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tLoadBucket';

