﻿CREATE Table Monitoring.tModelLatestPredict (
	ModelID int NOT NULL,
	LatestPrediction datetimeoffset NULL,
	CONSTRAINT [PK_tModelLatestPredict] PRIMARY KEY ([ModelID]),
	CONSTRAINT [FK_Monitoring_tModelLatestPredict_ModelID_tNDModel] FOREIGN KEY (ModelID) REFERENCES Monitoring.tNDModel(ModelID)
);