CREATE TABLE [Monitoring].[tFilterType] (
    [FilterTypeID]   INT            NOT NULL,
    [FilterTypeDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Monitoring_tFilterType] PRIMARY KEY CLUSTERED ([FilterTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tFilterType';

