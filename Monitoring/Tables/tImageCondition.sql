﻿CREATE TABLE [Monitoring].[tImageCondition] (
    [ImageConditionID]       INT        IDENTITY (1, 1) NOT NULL,
	[ImageConditionResultID] INT        NOT NULL,
    [TagID]                  INT        NOT NULL,
    [MaxValue]               REAL NULL,
    [MinValue]               REAL NULL,
    CONSTRAINT [PK_Monitoring_tImageCondition] PRIMARY KEY CLUSTERED ([ImageConditionID] ASC),
    CONSTRAINT [FK_Monitoring_tImageCondition_ImageConditionResultID_tImageConditionResult] FOREIGN KEY ([ImageConditionResultID]) REFERENCES [Monitoring].[tImageConditionResult] ([ImageConditionResultID]) ON DELETE CASCADE
);

