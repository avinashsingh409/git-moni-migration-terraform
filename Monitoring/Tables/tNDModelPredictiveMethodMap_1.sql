﻿CREATE TABLE [Monitoring].[tNDModelPredictiveMethodMap] (
    [ModelPredictiveMethodMapID] INT IDENTITY (1, 1) NOT NULL,
    [ModelID]                    INT NOT NULL,
    [PredictiveMethodTypeID]     INT NOT NULL,
    [Score]                      INT NULL,
    [Active]                     BIT NOT NULL,
	[CommonTracesID]			 NVARCHAR(400) NULL,
	[CommonTracesFastID]		 NVARCHAR(400) NULL,
    [RValue]                     FLOAT NULL,
    [RMSE]                       FLOAT NULL,
    [MAE]						 FLOAT NULL,
    [IQR]						 FLOAT NULL,
    CONSTRAINT [PK_tNDModelPredictiveMethodMap] PRIMARY KEY CLUSTERED ([ModelPredictiveMethodMapID] ASC),
    CONSTRAINT [FK_tNDModelPredictiveMethodMap_tNDModel_ModelID] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]),
    CONSTRAINT [FK_tNDModelPredictiveMethodMap_tNDPredictiveMethodType_PredictiveMethodTypeID] FOREIGN KEY ([PredictiveMethodTypeID]) REFERENCES [Monitoring].[tNDPredictiveMethodType] ([PredictiveMethodTypeID]),
    CONSTRAINT [UK_tNDModelPredictiveMethodMap_ModelID_PredictiveMethodTypeID] UNIQUE NONCLUSTERED ([ModelID] ASC, [PredictiveMethodTypeID] ASC)
);

GO
CREATE INDEX [IX_Active_Includes] ON [Monitoring].[tNDModelPredictiveMethodMap] ([Active])  INCLUDE ([ModelID]) WITH (FILLFACTOR=100);


GO