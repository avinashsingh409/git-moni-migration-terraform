﻿--EventTypeAssetVariableTypeTagMap table
CREATE TABLE [Monitoring].[tEventTypeAssetVariableTypeTagMap] (
    [EventTypeAssetVariableTypeTagMapID] INT       IDENTITY (1, 1) NOT NULL,
    [EventTypeID]                        INT       NOT NULL,
    [AssetVariableTypeTagMapID]          INT       NOT NULL,
    [EventRuleIsUpperComparedToVariable] BIT       NOT NULL,
    [EventRuleIsLowerComparedToVariable] BIT       NOT NULL,
    [EventRuleUpperLimitValue]           REAL      NULL,
    [EventRuleLowerLimitValue]           REAL      NULL,
    [EventRuleUpperLimitVariable]        INT       NULL,
    [EventRuleLowerLimitVariable]        INT       NULL,
    [EventRuleUpperLimitOperator]        NCHAR (2) NULL,
    [EventRuleLowerLimitOperator]        NCHAR (2) NULL,
    [EventRulePercentTimeMustBeTrue]     INT       NULL,
    [EventRuleTimePeriodInHours]         INT       NULL,
    CONSTRAINT [PK_tEventTypeAssetVariableTypeTagMap] PRIMARY KEY CLUSTERED ([EventTypeAssetVariableTypeTagMapID] ASC),
    CONSTRAINT [FK_tEventTypeAssetVariableTypeTagMap_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tEventTypeAssetVariableTypeTagMap_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Monitoring].[tEventType] ([EventTypeID])
);





 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tEventTypeAssetVariableTypeTagMap';

