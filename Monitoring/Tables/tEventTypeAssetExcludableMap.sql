﻿
CREATE TABLE [Monitoring].[tEventTypeAssetExcludableMap] (
    [EventTypeAssetExcludableMapID]   INT IDENTITY (1, 1) NOT NULL,
    [EventTypeID]                     INT NOT NULL,
    [AssetID]                         INT NOT NULL,
    [ExcludableFromAvailabilityCalcs] BIT NOT NULL,
    CONSTRAINT [PK_tEventTypeAssetExcludableMap] PRIMARY KEY CLUSTERED ([EventTypeAssetExcludableMapID] ASC),
    CONSTRAINT [FK_tEventTypeAssetExcludableMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tEventTypeAssetExcludableMap_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Monitoring].[tEventType] ([EventTypeID]),
    CONSTRAINT [UK_tEventTypeAssetExcludableMap_AssetID_EventTypeID] UNIQUE NONCLUSTERED ([AssetID] ASC, [EventTypeID] ASC)
);



GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tEventTypeAssetExcludableMap'
GO

EXEC sys.sp_addextendedproperty @name=N'Monitoring tEventTypeAssetExcludableMap mapping table', @value=N'Data' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tEventTypeAssetExcludableMap'
GO


GO


GO


GO


GO