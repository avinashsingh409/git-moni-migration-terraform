﻿CREATE TABLE [Monitoring].[tNDModelTemplate] (
    [ModelTemplateID]                 INT      IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeVariableTypeMapID] INT      NOT NULL,
    [ParentAssetClassTypeID]          INT      NOT NULL,
    [ModelTypeID]                     INT      NOT NULL,
    [Temporal]                        BIT      NOT NULL,
    [ConcerningBoundTypeID]           INT      NOT NULL,
    [CreatedByUserID]                 INT      NOT NULL,
    [CreateDate]                      DATETIME CONSTRAINT [DF_tNDModelTemplate_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]                 INT      NOT NULL,
    [ChangeDate]                      DATETIME CONSTRAINT [DF_tNDModelTemplate_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tNDModelTemplate] PRIMARY KEY CLUSTERED ([ModelTemplateID] ASC),
    CONSTRAINT [FK_tModelTemplate_tAssetClassType_ParentAssetClassTypeID] FOREIGN KEY ([ParentAssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tModelTemplate_tAssetClassTypeVariableTypeMap_AssetClassTypeVariableTypeMapID] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID]),
    CONSTRAINT [FK_tModelTemplate_tModelType_ModelTypeID] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tNDModelType] ([ModelTypeID]),
    CONSTRAINT [FK_tModelTemplate_tUser_ChangedBySecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tModelTemplate_tUser_CreatedBySecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tNDModelTemplate_tNDConcerningBoundType_ConcerningBoundTypeID] FOREIGN KEY ([ConcerningBoundTypeID]) REFERENCES [Monitoring].[tNDConcerningBoundType] ([ConcerningBoundTypeID]),
    CONSTRAINT [UK_tNDModelTemplate_DepVarMapID_PACTID] UNIQUE NONCLUSTERED ([AssetClassTypeVariableTypeMapID] ASC, [ParentAssetClassTypeID] ASC)
);


GO

CREATE TRIGGER [Monitoring].[tr_tNDModelTemplate_Delete]
	ON [Monitoring].[tNDModelTemplate]
	FOR DELETE
AS
BEGIN

	DECLARE @ModelTemplateID as int
	DECLARE @OK as int
	
	DECLARE curDEL INSENSITIVE CURSOR
		FOR
			SELECT d.ModelTemplateID FROM deleted d
	
	OPEN curDEL;
	FETCH NEXT FROM curDEL INTO @ModelTemplateID
	
	SET @OK = 1
	WHILE @OK = 1
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
				
					DELETE FROM [Monitoring].[tNDRecInputVariableMap] WHERE ModelTemplateID = @ModelTemplateID
					
					DELETE FROM [Monitoring].[tNDModel] WHERE ModelTemplateID = @ModelTemplateID
					
					DELETE FROM [Monitoring].[tNDModelTemplateOpModeMap] WHERE ModelTemplateID = @ModelTemplateID
				
				FETCH NEXT FROM curDEL INTO @ModelTemplateID
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	
	CLOSE curDEL;
	DEALLOCATE curDEL;

END