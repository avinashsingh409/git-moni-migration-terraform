﻿CREATE TABLE [Monitoring].[tUnit] (
    [UnitAssetID]     INT            NOT NULL,
    [PDServerID]      INT            NOT NULL,
    [LoadSensorID]    INT            NULL,
    [TransferArchive] NVARCHAR (MAX) NULL,
    [InputArchive]    NVARCHAR (MAX) NULL,
    [FilteredArchive] NVARCHAR (MAX) NULL,
    [ExpectedArchive] NVARCHAR (MAX) NULL,
    [TransferLag]     INT            CONSTRAINT [DF_tUnit_TransferLag] DEFAULT ((15)) NOT NULL,
    [CalculatedLag]   INT            CONSTRAINT [DF_tUnit_CalculatedLag] DEFAULT ((10)) NOT NULL,
    [FilteredLag]     INT            CONSTRAINT [DF_tUnit_FilteredLag] DEFAULT ((30)) NOT NULL,
    [ExpectedLag]     INT            CONSTRAINT [DF_tUnit_ExpectedLag] DEFAULT ((15)) NOT NULL,
    CONSTRAINT [PK_Monitoring_tUnit] PRIMARY KEY CLUSTERED ([UnitAssetID] ASC),
    CONSTRAINT [FK_Monitoring_tUnit_PDServerID_tServer] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]),
    CONSTRAINT [FK_Monitoring_tUnit_tAssetVariableTypeTagMapID_AssetVariableTypeTagMap] FOREIGN KEY ([LoadSensorID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Monitoring_tUnit_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);










GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tUnit';

