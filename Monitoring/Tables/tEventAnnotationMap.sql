﻿CREATE TABLE [Monitoring].[tEventAnnotationMap] (
    [EventAnnotationID] BIGINT IDENTITY (1, 1) NOT NULL,
    [AnnotationID]      BIGINT NOT NULL,
    [EventID]           INT    NOT NULL,
    CONSTRAINT [FK_tEventAnnotationMap_tAnnotation] FOREIGN KEY ([AnnotationID]) REFERENCES [ProcessData].[tAnnotation] ([AnnotationID]),
    CONSTRAINT [FK_tEventAnnotationMap_tEvent] FOREIGN KEY ([EventID]) REFERENCES [Monitoring].[tEvent] ([EventID]) ON DELETE CASCADE
);



GO


GO


GO


GO


GO




