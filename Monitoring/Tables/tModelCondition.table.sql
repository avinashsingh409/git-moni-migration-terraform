﻿CREATE TABLE [Monitoring].[tModelCondition] (
    [ModelConditionID]             INT      IDENTITY (1, 1) NOT NULL,
    [ModelID]                      INT      NOT NULL,
    [ConditionTimeBasis]           DATETIME CONSTRAINT [DF_Monitoring_tModelAlert_TimeBasis] DEFAULT ('1/1/1900 2:00') NULL,
    [ConditionOutOfBounds_percent] REAL     CONSTRAINT [DF_Table_1_OutOfBoundsLimit_percent] DEFAULT ((50)) NULL,
    [ConditionLastActive]          DATETIME NULL,
    [IsKeepConditionActive]        BIT      CONSTRAINT [DF_Monitoring_tModelCondition_IsKeepAlertActive] DEFAULT ((0)) NOT NULL,
    [KeepConditionActiveTimestamp] DATETIME NULL,
    [Timestamp]                    DATETIME NULL,
    [ActualOutOfBounds_percent]    REAL     NULL,
    [ConditionLastClear]           DATETIME NULL,
    [StateChanged]                 BIT      CONSTRAINT [DF_Monitoring_tModelCondition_IsChanged] DEFAULT ((0)) NOT NULL,
    [IgnoreUntil]                  DATETIME NULL,
    [OutDirection]                 INT      NULL,
    [RSquared]                     REAL     NULL,
    [BeganAlertingTimestamp]       DATETIME NULL,
    [LastIgnoreActualValue]        REAL     NULL,
    [LastIgnoreExpectedValue]      REAL     NULL,
    [LastIgnoreUpperLimit]         REAL     NULL,
    [LastIgnoreLowerLimit]         REAL     NULL,
    [IgnoreOverridden]             BIT      CONSTRAINT [DF_tModelCondition_IgnoreOverridden] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Monitoring_tModelAlert] PRIMARY KEY CLUSTERED ([ModelConditionID] ASC),
    CONSTRAINT [FK_Monitoring_tModelCondition_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE,
    CONSTRAINT [IX_tModelCondition_ModelID] UNIQUE NONCLUSTERED ([ModelID] ASC)
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Used with alerts that are raised if the value is out of bounds more than some percent of this amount of time.', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelCondition', @level2type = N'COLUMN', @level2name = N'ConditionTimeBasis';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Used with alerts that are raised if the value is out of bounds more than this percent of the time.', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelCondition', @level2type = N'COLUMN', @level2name = N'ConditionOutOfBounds_percent';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelCondition';


GO



CREATE TRIGGER [Monitoring].[tModelCondition_Update] 
on [Monitoring].[tModelCondition] for update
AS
DECLARE @OperationType char(1),
@OperationDate datetime,
@NewColValue varchar(200),
@OldColValue varchar(200),
@UserName varchar(128),
@AppName varchar(128),
@ClientName varchar(128),
@ModelConditionID int,
@ModelID int,
@BucketNo int,
@ConditionTimeBasis datetime,
@ConditionsOOBPercent real,
@ConditionsLastActive datetime,
@IsKeepConditionActive bit,
@KeepConditionActiveTimestamp datetime,
@StandardError real,
@Timestamp datetime,
@ActualOOBPercent real,
@ConditionLastClear datetime,
@StateChanged bit,
@OutDirection int,
@RSquared real,
@ResultsDate datetime,
@Update bit = 0,
@FieldChanged varchar(256)= '',
@NeedsAttention bit,
@ActualValue real,
@ExpectedValue real,
@UpperLimit real,
@LowerLimit real,
@IgnoreUntil datetime,
@BeganAlertingTimeStamp datetime,
@NEW_CheckOOB real,
@OLD_CheckOOB real,
@NEW_AlertTimeStamp datetime,
@OLD_AlertTimeStamp datetime,
@NEW_IgnoreUntil datetime,
@OLD_IgnoreUntil datetime

select @OperationDate = CURRENT_TIMESTAMP
Select @ClientName = HOST_NAME()
Select @AppName = APP_NAME()
     Select @NEW_CheckOOB = ActualOutOfBounds_percent
       from inserted
     Select @OLD_CheckOOB = ActualOutOfBounds_percent 
       from deleted
     IF ((@NEW_CheckOOB = -1) AND (@OLD_CheckOOB != -1))
      BEGIN
      SET @FieldChanged = RTRIM(@FieldChanged) + 'Cleared; ';
      SET @Update = 1;
     END
    
      SELECT @NEW_AlertTimeStamp = BeganAlertingTimestamp
      FROM  inserted 
      SELECT @OLD_AlertTimeStamp = BeganAlertingTimeStamp
      FROM deleted 
      IF (@NEW_AlertTimeStamp != @OLD_AlertTimeStamp)
      BEGIN
        SET @FieldChanged = RTRIM(@FieldChanged) + 'Alert; ';
        SET @Update = 1;
      END

    SELECT @NEW_IgnoreUntil = IgnoreUntil 
    FROM inserted 
    SELECT @OLD_IgnoreUntil = IgnoreUntil 
    FROM deleted
    IF (@NEW_IgnoreUntil != @OLD_IgnoreUntil)
    BEGIN
     SET @FieldChanged = RTRIM(@FieldChanged) + 'IgnoreUntil; ';
     SET @Update = 1;
    END
    
    IF (@Update=1)
    BEGIN
     select distinct @ModelConditionID = i.ModelConditionID,
            @ModelID = i.ModelID, @ConditionTimeBasis = i.ConditionTimeBasis,
            @ConditionsOOBPercent = i.ConditionOutOfBounds_percent,
            @ConditionsLastActive= i.ConditionLastActive,
            @IsKeepConditionActive= i.IsKeepConditionActive, 
            @KeepConditionActiveTimeStamp = i.KeepConditionActiveTimestamp,
            @Timestamp = i.Timestamp, 
            @ActualOOBPercent = i.ActualOutOfBounds_percent,
            @ConditionLastClear = i.ConditionLastClear, 
            @StateChanged = i.StateChanged,
            @IgnoreUntil = i.IgnoreUntil, 
            @OutDirection =  i.OutDirection,
            @NeedsAttention = mcn.NeedsAttention,
            @ActualValue = ms.ActualValue,
            @ExpectedValue = ms.ExpectedValue,
            @UpperLimit = ms.UpperLimit,
            @LowerLimit = ms.LowerLimit,
            @RSquared = i.RSquared,
            @BeganAlertingTimestamp = i.BeganAlertingTimestamp
            from inserted i
            LEFT OUTER JOIN Monitoring.tModelConditionNote mcn on mcn.ModelConditionID = i.ModelConditionID
            LEFT OUTER JOIN Monitoring.tModelSnapshot ms on ms.ModelID = i.ModelID
            
  Insert Monitoring.tModelConditionArchive (ModelConditionID,
          ModelID, ConditionTimeBasis, ConditionOutOfBounds_percent,
          ConditionLastActive, IsKeepConditionActive, KeepConditionActiveTimestamp,
          Timestamp, ActualOutOfBounds_percent, ConditionLastClear,
          StateChanged, IgnoreUntil, OutDirection,
          NeedsAttention, ActualValue, ExpectedValue, UpperLimit,
          LowerLimit, RSquared, BeganAlertingTimestamp,
          OperationDate, ClientName, AppName, OperationFieldChanged)
          values (@ModelConditionID, @ModelID, @ConditionTimeBasis, @ConditionsOOBPercent,
          @ConditionsLastActive, @IsKeepConditionActive, @KeepConditionActiveTimestamp,
          @Timestamp, @ActualOOBPercent, @ConditionLastClear, 
          @StateChanged, @IgnoreUntil, @OutDirection,
          @NeedsAttention, @ActualValue, @ExpectedValue, @UpperLimit,
          @LowerLimit, @RSquared, @BeganAlertingTimestamp,
          @OperationDate, @ClientName, @AppName, @FieldChanged)
 END