﻿CREATE TABLE [Monitoring].[tNDModelCriticality] (
    [ModelID]          INT      NOT NULL,
    [UseDefault]       BIT      CONSTRAINT [DF_tNDModelCriticality_UseDefault] DEFAULT ((1)) NOT NULL,
    [UpperCriticality] INT      NULL,
    [LowerCriticality] INT      NULL,
    [CreatedByUserID]  INT      NOT NULL,
    [CreateDate]       DATETIME CONSTRAINT [DF_tNDModelCriticality_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]  INT      NOT NULL,
    [ChangeDate]       DATETIME CONSTRAINT [DF_tNDModelCriticality_ChangeDate] DEFAULT (getdate()) NOT NULL,
	[UseUpperDefault]  BIT		CONSTRAINT [DF_tNDModelCriticality_UseUpperDefault] DEFAULT ((0)) NOT NULL,
	[UseLowerDefault]  BIT		CONSTRAINT [DF_tNDModelCriticality_UseLowerDefault] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tNDModelCriticality] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [FK_tNDModelCriticality_tCriticalityType] FOREIGN KEY ([UpperCriticality]) REFERENCES [Asset].[tCriticalityType] ([CriticalityTypeID]),
    CONSTRAINT [FK_tNDModelCriticality_tCriticalityType_LowerCriticality] FOREIGN KEY ([LowerCriticality]) REFERENCES [Asset].[tCriticalityType] ([CriticalityTypeID]),
    CONSTRAINT [FK_tNDModelCriticality_tNDModel_ModelID] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]),
    CONSTRAINT [FK_tNDModelCriticality_tUser_ChangedByUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tNDModelCriticality_tUser_CreatedByUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);


GO




CREATE TRIGGER [Monitoring].[TR_tNDModelCriticality_Upsert] 
   ON  [Monitoring].[tNDModelCriticality]
   INSTEAD OF INSERT, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	-- we want to enforce two business rules here:
	-- (1) UseDefault = 1 for standard model
	-- if we're enforcing the constraints here, we really don't need the constraints because
	-- they'll never be broken. but eh, someone might come along and drop this trigger, so we'll leave the constraints in place

	declare @incoming table (
		ModelID int not null
		, UseDefault bit not null
		, UpperCriticality int null
		, LowerCriticality int null
		, CreatedByUserID int not null
		, CreateDate datetime not null
		, ChangedByUserID int not null
		, ChangeDate datetime not null
	);

	insert into @incoming (ModelID, UseDefault, UpperCriticality, LowerCriticality, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate)
	select ModelID, UseDefault, UpperCriticality, LowerCriticality, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate
	from inserted
	;	
	
	-- Deleted the update statement that sets the UseDefault flag to 1 if associated with a template

	-- update existing criticality
	update c
	set
		c.UseDefault = inc.UseDefault
		, c.UpperCriticality = inc.UpperCriticality
		, c.LowerCriticality = inc.LowerCriticality
		, c.CreatedByUserID = inc.CreatedByUserID
		, c.CreateDate = inc.CreateDate
		, c.ChangedByUserID = inc.ChangedByUserID
		, c.ChangeDate = inc.ChangeDate
	from Monitoring.tNDModelCriticality c
	join @incoming inc on c.ModelID = inc.ModelID
	;

	-- add new criticalities
	insert into Monitoring.tNDModelCriticality (ModelID, UseDefault, UpperCriticality, LowerCriticality, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate)
	select ModelID, UseDefault, UpperCriticality, LowerCriticality, CreatedByUserID, CreateDate, ChangedByUserID, ChangeDate
	from @incoming
	except
	select c.ModelID, c.UseDefault, c.UpperCriticality, c.LowerCriticality, c.CreatedByUserID, c.CreateDate, c.ChangedByUserID, c.ChangeDate
	from Monitoring.tNDModelCriticality c
	join @incoming inc on c.ModelID = inc.ModelID
	;

END