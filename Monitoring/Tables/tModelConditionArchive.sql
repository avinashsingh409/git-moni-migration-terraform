﻿CREATE TABLE [Monitoring].[tModelConditionArchive] (
    [ModelConditionArchiveID]      INT           IDENTITY (1, 1) NOT NULL,
    [ModelConditionID]             INT           NULL,
    [ModelID]                      INT           NOT NULL,
    [ConditionTimeBasis]           DATETIME      NULL,
    [ConditionOutOfBounds_percent] REAL          NULL,
    [ConditionLastActive]          DATETIME      NULL,
    [IsKeepConditionActive]        BIT           NOT NULL,
    [KeepConditionActiveTimestamp] DATETIME      NULL,
    [Timestamp]                    DATETIME      NULL,
    [ActualOutOfBounds_percent]    REAL          NULL,
    [ConditionLastClear]           DATETIME      NULL,
    [StateChanged]                 BIT           NOT NULL,
    [IgnoreUntil]                  DATETIME      NULL,
    [NeedsAttention]               BIT           NULL,
    [OutDirection]                 INT           NULL,
    [RSquared]                     REAL          NULL,
    [BeganAlertingTimestamp]       DATETIME      NULL,
    [ActualValue]                  REAL          NULL,
    [ExpectedValue]                REAL          NULL,
    [UpperLimit]                   REAL          NULL,
    [LowerLimit]                   REAL          NULL,
    [AppName]                      VARCHAR (128) NULL,
    [ClientName]                   VARCHAR (128) NULL,
    [OperationDate]                DATETIME      NOT NULL,
    [OperationFieldChanged]        VARCHAR (128) NULL,
    [ModelConditionNoteID]         INT           NULL,
    CONSTRAINT [PK_Monitoring_tModelConditionArchive] PRIMARY KEY CLUSTERED ([ModelConditionArchiveID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelConditionArchive';

