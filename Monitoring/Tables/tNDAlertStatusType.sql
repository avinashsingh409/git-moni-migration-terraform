﻿CREATE TABLE [Monitoring].[tNDAlertStatusType] (
    [AlertStatusTypeID]         INT            NOT NULL,
    [AlertStatusTypeAbbrev]     NVARCHAR (50)  NOT NULL,
    [AlertStatusTypeDesc]       NVARCHAR (255) NOT NULL,
    [AlertStatusPriorityTypeID] INT            NOT NULL,
    CONSTRAINT [PK_tNDAlertStatusType] PRIMARY KEY CLUSTERED ([AlertStatusTypeID] ASC),
    CONSTRAINT [FK_tNDAlertStatusType_tNDAlertStatusPriorityType] FOREIGN KEY ([AlertStatusPriorityTypeID]) REFERENCES [Monitoring].[tNDAlertStatusPriorityType] ([AlertStatusPriorityTypeID])
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDAlertStatusType';

