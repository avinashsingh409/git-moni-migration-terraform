﻿CREATE TABLE [Monitoring].[tNDModelBuildStatusType] (
    [ModelBuildStatusTypeID]     INT            NOT NULL,
    [ModelBuildStatusTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [ModelBuildStatusTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tNDModelBuildStatusType] PRIMARY KEY CLUSTERED ([ModelBuildStatusTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDModelBuildStatusType';

