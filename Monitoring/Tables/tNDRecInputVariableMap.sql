﻿CREATE TABLE [Monitoring].[tNDRecInputVariableMap] (
    [RecInputVariableMapID]           INT IDENTITY (1, 1) NOT NULL,
    [ModelTemplateID]                 INT NOT NULL,
    [AssetClassTypeVariableTypeMapID] INT NOT NULL,
    [RecInputVariableTargetTypeID]    INT NULL,
    [MultipleTagPreprocessingTypeID]  INT NULL,
    [Priority]                        INT NOT NULL,
    [Series]                          INT NOT NULL,
    CONSTRAINT [PK_tNDRecInputVariableMap] PRIMARY KEY CLUSTERED ([RecInputVariableMapID] ASC),
    CONSTRAINT [FK_tNDRecInputVariableMap_tAssetClassTypeVariableTypeMap_AssetClassTypeVariableTypeMapID] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID]),
    CONSTRAINT [FK_tNDRecInputVariableMap_tNDModelTemplate_ModelTemplateID] FOREIGN KEY ([ModelTemplateID]) REFERENCES [Monitoring].[tNDModelTemplate] ([ModelTemplateID]),
    CONSTRAINT [FK_tNDRecInputVariableMap_tNDMultipleTagPreprocessingType_MultipleTagPreprocessingTypeID] FOREIGN KEY ([MultipleTagPreprocessingTypeID]) REFERENCES [Monitoring].[tNDMultipleTagPreprocessingType] ([MultipleTagPreprocessingTypeID]),
    CONSTRAINT [FK_tNDRecInputVariableMap_tNDRecInputVariableTargetType_RecInputVariableTargetTypeID] FOREIGN KEY ([RecInputVariableTargetTypeID]) REFERENCES [Monitoring].[tNDRecInputVariableTargetType] ([RecInputVariableTargetTypeID])
);

