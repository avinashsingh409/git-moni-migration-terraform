CREATE TABLE [Monitoring].[tModelHistory] (
    [ModelHistoryID] INT            IDENTITY (1, 1) NOT NULL,
    [Timestamp]      DATETIME       CONSTRAINT [DF_Monitoring_tModelHistory_Timestamp] DEFAULT (getdate()) NOT NULL,
    [ModelID]        INT            NOT NULL,
    [Operation]      NVARCHAR (2)   NOT NULL,
    [Data]           NVARCHAR (255) NULL,
    CONSTRAINT [PK_Monitoring_tModelHistory] PRIMARY KEY CLUSTERED ([ModelHistoryID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'C - Create Model, D - Delete Model, A - Add Input, R - Remove Input, M - Change Limit Multiplier', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelHistory', @level2type = N'COLUMN', @level2name = N'Operation';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelHistory';

