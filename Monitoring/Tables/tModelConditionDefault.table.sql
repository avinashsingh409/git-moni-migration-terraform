CREATE TABLE [Monitoring].[tModelConditionDefault] (
    [ModelConditionDefaultID]      INT      IDENTITY (1, 1) NOT NULL,
    [ModelDefaultID]               INT      NOT NULL,
    [ConditionTimeBasis]           DATETIME CONSTRAINT [DF_Monitoring_tModelConditionDefault_ConditionTimeBasis] DEFAULT ('1/1/1900 2:00') NULL,
    [ConditionOutOfBounds_percent] REAL     CONSTRAINT [DF_tModelConditionDefault_ConditionOutOfBounds_percent] DEFAULT ((50)) NULL,
    [OutDirection]                 INT      CONSTRAINT [DF_tModelConditionDefault_OutDirection] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Monitoring_tModelConditionDefault] PRIMARY KEY CLUSTERED ([ModelConditionDefaultID] ASC),
    CONSTRAINT [FK_Monitoring_tModelConditionDefault_ModelDefaultID_tModelDefault] FOREIGN KEY ([ModelDefaultID]) REFERENCES [Monitoring].[tModelDefault] ([ModelDefaultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelConditionDefault';

