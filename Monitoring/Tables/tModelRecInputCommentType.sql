﻿CREATE TABLE [Monitoring].[tModelRecInputCommentType]
(
	[CommentTypeID]		INT NOT NULL,
	[CommentDesc]		NVARCHAR(255) NOT NULL,
	[CreatedByUserID]	INT NOT NULL,
	[CreateDate]		DATETIME CONSTRAINT DF_tModelRecInputCommentType_CreateDate DEFAULT (getdate()) NOT NULL,
	[ChangedByUserID]	INT NOT NULL,
	[ChangedDate]		DATETIME CONSTRAINT DF_tModelRecInputCommentType_ChangedDate DEFAULT (getdate()) NOT NULL,
	CONSTRAINT [PK_tModelRecInputCommentType] PRIMARY KEY CLUSTERED ([CommentTypeID] ASC),
	CONSTRAINT [FK_tModelRecInputCommentType_CreatedByUserID_tUser_SecurityUserID]  FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [FK_tModelRecInputCommentType_ChangedByUserID_tUser_SecurityUserID]  FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
)
