﻿CREATE TABLE [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] (
    [SubscriptionNDAlertSecurityUserIDMapID] UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [ModelID]                              INT              NOT NULL,
    [AlertStatusTypeID]                    INT              NOT NULL,
    [SecurityUserID]                       INT              NOT NULL,
    CONSTRAINT [PK_tSubscriptionNDAlertSecurityUserIDMap] PRIMARY KEY CLUSTERED ([SubscriptionNDAlertSecurityUserIDMapID] ASC),
    CONSTRAINT [FK_tSubscriptionNDAlertSecurityUserIDMap_AlertStatusTypeID] FOREIGN KEY ([AlertStatusTypeID]) REFERENCES [Monitoring].[tNDAlertStatusType] ([AlertStatusTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSubscriptionNDAlertSecurityUserIDMap_tNDModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSubscriptionNDAlertSecurityUserIDMap_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tSubscriptionNDAlertSecurityUserIDMap';

