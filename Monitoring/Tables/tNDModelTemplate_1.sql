﻿CREATE TABLE [Monitoring].[tNDModelTemplate] (
    [ModelTemplateID]                 INT      IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeVariableTypeMapID] INT      NOT NULL,
    [ParentAssetClassTypeID]          INT      NULL,
    [ModelTypeID]                     INT      NOT NULL,
    [Temporal]                        BIT      NOT NULL,
    [ConcerningBoundTypeID]           INT      NOT NULL,
    [CreatedByUserID]                 INT      NOT NULL,
    [CreateDate]                      DATETIME CONSTRAINT [DF_tNDModelTemplate_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedByUserID]                 INT      NOT NULL,
    [ChangeDate]                      DATETIME CONSTRAINT [DF_tNDModelTemplate_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [UpperCriticality]                INT      NULL,
    [LowerCriticality]                INT      NULL,
    CONSTRAINT [PK_tNDModelTemplate] PRIMARY KEY CLUSTERED ([ModelTemplateID] ASC),
    CONSTRAINT [FK_tModelTemplate_tAssetClassType_ParentAssetClassTypeID] FOREIGN KEY ([ParentAssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tModelTemplate_tAssetClassTypeVariableTypeMap_AssetClassTypeVariableTypeMapID] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID]),
    CONSTRAINT [FK_tModelTemplate_tModelType_ModelTypeID] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tNDModelType] ([ModelTypeID]),
    CONSTRAINT [FK_tModelTemplate_tUser_ChangedBySecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tModelTemplate_tUser_CreatedBySecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tNDModelTemplate_LowerCriticality] FOREIGN KEY ([LowerCriticality]) REFERENCES [Asset].[tCriticalityType] ([CriticalityTypeID]),
    CONSTRAINT [FK_tNDModelTemplate_tNDConcerningBoundType_ConcerningBoundTypeID] FOREIGN KEY ([ConcerningBoundTypeID]) REFERENCES [Monitoring].[tNDConcerningBoundType] ([ConcerningBoundTypeID]),
    CONSTRAINT [FK_tNDModelTemplate_UpperCriticality] FOREIGN KEY ([UpperCriticality]) REFERENCES [Asset].[tCriticalityType] ([CriticalityTypeID]),
    CONSTRAINT [UK_tNDModelTemplate_DepVarMapID_PACTID] UNIQUE NONCLUSTERED ([AssetClassTypeVariableTypeMapID] ASC, [ParentAssetClassTypeID] ASC)
);






GO
