CREATE TABLE [Monitoring].[tModelTrendMap] (
    [ModelTrendMapID] INT IDENTITY (1, 1) NOT NULL,
    [ModelID]         INT NOT NULL,
    [PDTrendID]       INT NOT NULL,
    [DisplayOrder]    INT NOT NULL,
    CONSTRAINT [PK_Monitoring_tModelTrendMap] PRIMARY KEY CLUSTERED ([ModelTrendMapID] ASC),
    CONSTRAINT [FK_Monitoring_tModelTrendMap_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Monitoring_tModelTrendMap_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE,
    CONSTRAINT [UK_Monitoring_tModelTrendMap_ModelID_PDTrendID] UNIQUE NONCLUSTERED ([ModelID] ASC, [PDTrendID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelTrendMap';

