﻿CREATE TABLE [Monitoring].[tNDModelOpModeTypeMap] (
    [ModelOpModeTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [ModelID]              INT NOT NULL,
    [OpModeTypeID]         INT NOT NULL,
    CONSTRAINT [PK_tNDModelOpModeTypeMap] PRIMARY KEY CLUSTERED ([ModelOpModeTypeMapID] ASC),
    CONSTRAINT [FK_tNDModelOpModeTypeMap_tNDModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]),
    CONSTRAINT [FK_tNDModelOpModeTypeMap_tNDOpModeType] FOREIGN KEY ([OpModeTypeID]) REFERENCES [Monitoring].[tNDOpModeType] ([OpModeTypeID]),
    CONSTRAINT [UK_tNDModelOpModeTypeMap_ModelID_OpModeTypeID] UNIQUE NONCLUSTERED ([ModelID] ASC, [OpModeTypeID] ASC)
);

