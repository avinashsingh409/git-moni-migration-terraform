﻿CREATE TABLE [Monitoring].[tNDModel_AlertScreening] (
    [ModelID]             INT                NOT NULL,
    [IgnoreStatus]        BIT                NULL,
    [IgnoreDurationHours] INT                NULL,
    [TagGroup]            INT                NULL,
    [Until]               DATETIMEOFFSET (7) NOT NULL,
    [LowerBound]          FLOAT (53)         DEFAULT ((0)) NOT NULL,
    [UpperBound]          FLOAT (53)         DEFAULT ((0)) NOT NULL,
    [IgnoreOverrideValue] FLOAT (53)         NULL,
    CONSTRAINT [tNDModel_AlertScreening_PK] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [tNDModel_tNDModel_AlertScreening_FK1] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID])
);


GO
