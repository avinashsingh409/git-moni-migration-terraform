CREATE TABLE [Monitoring].[tFilterRule] (
    [FilterRuleID]              INT  IDENTITY (1, 1) NOT NULL,
    [UnitAssetID]               INT  NOT NULL,
    [SortOrder]                 INT  NOT NULL,
    [FilterTypeID]              INT  NOT NULL,
    [AssetVariableTypeTagMapID] INT  NULL,
    [FilterAllTags]             BIT  CONSTRAINT [DF_tMDFilterRule_FilterAllTags] DEFAULT ((0)) NOT NULL,
    [IntParam1]                 INT  NULL,
    [IntParam2]                 INT  NULL,
    [RealParam1]                REAL NULL,
    [RealParam2]                REAL NULL,
    CONSTRAINT [PK_Monitoring_tFilterRule] PRIMARY KEY CLUSTERED ([FilterRuleID] ASC),
    CONSTRAINT [FK_Monitoring_tFilterRule_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE SET NULL,
    CONSTRAINT [FK_Monitoring_tFilterRule_FilterTypeID_tFilterType] FOREIGN KEY ([FilterTypeID]) REFERENCES [Monitoring].[tFilterType] ([FilterTypeID]),
    CONSTRAINT [FK_Monitoring_tFilterRule_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tFilterRule';

