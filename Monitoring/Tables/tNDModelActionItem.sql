﻿CREATE TABLE [Monitoring].[tNDModelActionItem] (
    [ModelID]                       INT            NOT NULL,
    [ModelActionItemPriorityTypeID] INT            NULL,
    [NoteText]                      NVARCHAR (MAX) NULL,
    [CreatedByUserID]               INT            NOT NULL,
    [CreateDate]                    DATETIME       NOT NULL,
    [ChangedByUserID]               INT            NOT NULL,
    [ChangeDate]                    DATETIME       NOT NULL,
    [WatchDuration]                 REAL           NULL,
    [WatchDurationTemporalTypeID]   INT            NULL,
    [WatchCriteriaTypeID]           INT            NULL,
    [WatchLimit]                    REAL           NULL,
    [WatchLimitMeetsTemporalTypeID] INT            NULL,
    [ModelActionItemTypeID]         INT            NULL,
    [WatchRelativeToExpected]       BIT            CONSTRAINT [DF_tNDModelActionItem_WatchRelativeToExpected] DEFAULT ((0)) NOT NULL,
	[Favorite]                      BIT            NOT NULL DEFAULT ((0)),
	[WatchEmailIfAlert]             BIT            NOT NULL DEFAULT ((0)),
    [WatchDuration2]                REAL           NULL,
    [WatchDurationTemporalTypeID2]  INT            NULL,
    [WatchCriteriaTypeID2]          INT            NULL,
    [WatchLimit2]                   REAL           NULL,
    [WatchLimitMeetsTemporalTypeID2] INT            NULL,
	[WatchRelativeToExpected2]      BIT            CONSTRAINT [DF_tNDModelActionItem_WatchRelativeToExpected2] DEFAULT ((0)) NOT NULL,
    [LastDiagnoseAction] tinyint NULL,
	[LastWatchAction] tinyint NULL,
	[LastMaintenanceAction] tinyint NULL,
	[LastIssueAction] tinyint NULL,
	[LastNoteAction] tinyint NULL,
	[LastAlertAction] tinyint NULL
	CONSTRAINT [tNDModelActionItem_PK] PRIMARY KEY CLUSTERED ([ModelID] ASC),
    CONSTRAINT [FK_tNDModelActionItemType_tNDModelActionItem] FOREIGN KEY ([ModelActionItemTypeID]) REFERENCES [Monitoring].[tNDModelActionItemType] ([ModelActionItemTypeID]),
    CONSTRAINT [tNDModel_tNDModelActionItem_FK1] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel] ([ModelID]),
    CONSTRAINT [tNDModelActionItemPriorityType_tNDModelActionItem_FK1] FOREIGN KEY ([ModelActionItemPriorityTypeID]) REFERENCES [Monitoring].[tNDModelActionItemPriorityType] ([ModelActionItemPriorityTypeID]),
    CONSTRAINT [tNDWatchCriteriaType_tNDModelActionItem_FK1] FOREIGN KEY ([WatchCriteriaTypeID]) REFERENCES [Monitoring].[tNDWatchCriteriaType] ([WatchCriteriaTypeID]),
    CONSTRAINT [tNDWatchLimitMeetsTemporalType_tNDModelActionItem_FK1] FOREIGN KEY ([WatchLimitMeetsTemporalTypeID]) REFERENCES [Monitoring].[tNDWatchLimitMeetsTemporalType] ([WatchLimitMeetsTemporalTypeID])
);
GO

CREATE TRIGGER [Monitoring].[tNDModelActionItem_AfterInsertUpdate]
	ON  [Monitoring].[tNDModelActionItem]
	AFTER INSERT,UPDATE
AS
BEGIN
	IF OBJECT_ID('tempdb..#DISABLEMODELACTIONITEMTRIGGER') IS NOT NULL RETURN;

	IF UPDATE(ModelActionItemTypeID)
	BEGIN
		UPDATE Monitoring.tNDModelActionItem SET
		  LastDiagnoseAction = CASE WHEN i.ModelActionItemTypeID IN (1,2) THEN i.ModelActionItemTypeID ELSE t.LastDiagnoseAction END,
		  LastWatchAction = CASE WHEN i.ModelActionItemTypeID IN (3,4,5,14,16) THEN i.ModelActionItemTypeID ELSE t.LastWatchAction END,
		  LastMaintenanceAction = CASE WHEN i.ModelActionItemTypeID IN (6,7) THEN i.ModelActionItemTypeID ELSE t.LastMaintenanceAction END,
		  LastIssueAction = CASE WHEN i.ModelActionItemTypeID IN (8,9) THEN i.ModelActionItemTypeID ELSE t.LastIssueAction END,
		  LastNoteAction = CASE WHEN i.ModelActionItemTypeID IN (10) THEN i.ModelActionItemTypeID ELSE t.LastNoteAction END,
		  LastAlertAction = CASE WHEN i.ModelActionItemTypeID IN (11,12,13,15) THEN i.ModelActionItemTypeID ELSE t.LastAlertAction END
		FROM Monitoring.tNDModelActionItem t
		INNER JOIN inserted i on t.ModelID = i.ModelID
	END

	INSERT INTO [Monitoring].[tNDModelActionItemArchive]
	(	  [ModelID],
		[ModelActionItemPriorityTypeID],
		[ModelActionItemTypeID],
		[NoteText],
		[CreatedByUserID],
		[CreateDate],
		[ChangedByUserID],
		[ChangeDate],
		[WatchDuration],
		[WatchDurationTemporalTypeID],
		[WatchCriteriaTypeID],
		[WatchLimit],
		[WatchLimitMeetsTemporalTypeID],
		[WatchRelativeToExpected],
		[NDAlert],
		[NDActual],
		[NDExpected],
		[NDLower],
		[NDUpper],
		[PercentOOB],
		[SerializedOpMode],
		[PrioritizedOpMode],
		[EvaluationStatus],
		[HiHiAlert],
		[LowLowAlert],
		[AreaFastResponseAlert],
		[AreaSlowResponseAlert],
		[FrequencyAlert],
		[OscillationAlert],
		[FrozenDataAlert],
		[WatchOverrideAlert],
		[IgnoreOverrideAlert],
		[AddedtoAlerts],
		[AlertStatusPriorityTypeID],
		[IgnoreDurationHours],
		[Favorite],
		[WatchEmailIfAlert],
		[WatchDuration2],
		[WatchDurationTemporalTypeID2],
		[WatchCriteriaTypeID2],
		[WatchLimit2],
		[WatchLimitMeetsTemporalTypeID2],
		[WatchRelativeToExpected2]
	)
	SELECT i.[ModelID],
		i.[ModelActionItemPriorityTypeID],
		i.[ModelActionItemTypeID],
		i.[NoteText],
		i.[CreatedByUserID],
		i.[CreateDate],
		i.[ChangedByUserID],
		i.[ChangeDate],
		i.[WatchDuration],
		i.[WatchDurationTemporalTypeID],
		i.[WatchCriteriaTypeID],
		i.[WatchLimit],
		i.[WatchLimitMeetsTemporalTypeID],
		i.[WatchRelativeToExpected],
		meval.NDAlert,
		meval.NDActual,
		meval.NDExpected, 
		meval.NDLower, 
		meval.NDUpper, 
		meval.PercentOOB, 
		meval.SerializedOpMode, 
		meval.PrioritizedOpMode, 
		meval.EvaluationStatus,
		meval.HiHiAlert,
		meval.LowLowAlert,
		meval.AreaFastResponseAlert,
		meval.AreaSlowResponseAlert,
		meval.FrequencyAlert,
		meval.OscillationAlert,
		meval.FrozenDataAlert,
		meval.WatchOverrideAlert,
		meval.IgnoreOverrideAlert,			
		meval.AddedtoAlerts, 
		meval.AlertStatusPriorityTypeID,
		CASE WHEN i.ModelActionItemTypeID = 11 THEN mas.IgnoreDurationHours ELSE null END, --11 is Ignore Set,
		i.[Favorite],
		i.[WatchEmailIfAlert],
		i.[WatchDuration2],
		i.[WatchDurationTemporalTypeID2],
		i.[WatchCriteriaTypeID2],
		i.[WatchLimit2],
		i.[WatchLimitMeetsTemporalTypeID2],
		i.[WatchRelativeToExpected2]
	FROM [INSERTED] i
	LEFT JOIN Monitoring.tNDModelEvaluation meval on i.ModelID = meval.ModelID
	LEFT JOIN Monitoring.tNDModel_AlertScreening mas on mas.ModelID = i.ModelID
END
GO

CREATE TRIGGER [Monitoring].[tNDModelActionItem_Delete]
	ON  [Monitoring].[tNDModelActionItem]
	AFTER DELETE
AS
BEGIN
	DELETE [Monitoring].[tNDModelActionItemArchive]
	FROM [Monitoring].[tNDModelActionItemArchive] a
	INNER JOIN [DELETED] d ON a.[ModelID] = d.ModelId
END