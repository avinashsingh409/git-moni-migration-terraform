﻿CREATE TABLE [Monitoring].[tNDAlertPriority] (
		[ModelID] INT  NOT NULL  
		, [AlertPriorityTypeID] INTEGER  NOT NULL  
		, [AlertPriorityAbbrev] nvarchar(50)  NOT NULL  
		, [AlertPriorityDesc] nvarchar(255)  NOT NULL  
		, CONSTRAINT [tNDModel_AlertScreening_FromND_tNDAlertPriority_FK1] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tNDModel_AlertScreening_FromND] ([ModelID])
		, CONSTRAINT [tNDAlertPriorityType_tNDAlertPriority_FK1] FOREIGN KEY ([AlertPriorityTypeID]) REFERENCES [Monitoring].[tNDAlertPriorityType] ([AlertPriorityTypeID])
		, CONSTRAINT [tNDAlertPriority_PK] PRIMARY KEY CLUSTERED ([ModelID])
)
