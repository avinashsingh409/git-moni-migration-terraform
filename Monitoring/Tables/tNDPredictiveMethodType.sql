﻿CREATE TABLE [Monitoring].[tNDPredictiveMethodType] (
    [PredictiveMethodTypeID] INT            NOT NULL,
    [PredictiveMethodAbbrev] NVARCHAR (50)  NOT NULL,
    [PredictiveMethodDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tNDOutputPredictiveMethodType] PRIMARY KEY CLUSTERED ([PredictiveMethodTypeID] ASC),
    CONSTRAINT [UK_tNDPredictiveMethodType_PredictiveMethodAbbrev] UNIQUE NONCLUSTERED ([PredictiveMethodAbbrev] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDPredictiveMethodType';

