CREATE TABLE [Monitoring].[tModelDefault] (
    [ModelDefaultID]                  INT            IDENTITY (1, 1) NOT NULL,
    [ModelTypeID]                     INT            NOT NULL,
    [ModelDesc]                       NVARCHAR (100) NULL,
    [AssetClassTypeVariableTypeMapID] INT            NOT NULL,
    [UpperLimitMultiplier]            REAL           CONSTRAINT [DF_Monitoring_tModelDefault_UpperLimitMultiplier] DEFAULT ((1.0)) NOT NULL,
    [UpperLimitAdder]                 REAL           CONSTRAINT [DF_Monitoring_tModelDefault_UpperLimitAdder] DEFAULT ((0.0)) NOT NULL,
    [LowerLimitMultiplier]            REAL           CONSTRAINT [DF_Monitoring_tModelDefault_LowerLimitMultiplier] DEFAULT ((1.0)) NOT NULL,
    [LowerLimitAdder]                 REAL           CONSTRAINT [DF_Monitoring_tModelDefault_LowerLimitAdder] DEFAULT ((0.0)) NOT NULL,
    [SetLowerSameAsUpper]             BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Monitoring_tModelDefault] PRIMARY KEY CLUSTERED ([ModelDefaultID] ASC),
    CONSTRAINT [FK_Monitoring_tModelDefault_AssetClassTypeVariableTypeMapID_tAssetClassTypeVariableTypeMapID] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Monitoring_tModelDefault_ModelTypeID_tModelType] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tModelType] ([ModelTypeID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelDefault';

