﻿CREATE TABLE [Monitoring].[tNDOpModeParamsAssetMap] (
    [OpModeParamsAssetMapID] INT IDENTITY (1, 1) NOT NULL,
    [OpModeTypeID]           INT NOT NULL,
    [OpModeParamsID]         INT NOT NULL,
    [AssetID]                INT NOT NULL,
    CONSTRAINT [PK_tOpModeParamsAssetMap] PRIMARY KEY CLUSTERED ([OpModeParamsAssetMapID] ASC),
    CONSTRAINT [FK_tOpModeParamsAssetMap_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tOpModeParamsAssetMap_tNDOpModeParams_OpModeParamsID] FOREIGN KEY ([OpModeParamsID]) REFERENCES [Monitoring].[tNDOpModeParams] ([OpModeParamsID]),
    CONSTRAINT [FK_tOpModeParamsAssetMap_tNDOpModeType_OpModeTypeID] FOREIGN KEY ([OpModeTypeID]) REFERENCES [Monitoring].[tNDOpModeType] ([OpModeTypeID]),
    CONSTRAINT [UK_tNDOpModeParamsAssetMap_OpModeTypeID_AssetID] UNIQUE NONCLUSTERED ([OpModeTypeID] ASC, [AssetID] ASC)
);


GO

CREATE TRIGGER [Monitoring].[tr_tNDOpModeParamsAssetMap_Delete]
	ON [Monitoring].[tNDOpModeParamsAssetMap]
	FOR DELETE
AS
BEGIN

	DECLARE @OpModeParamsID as int
	DECLARE @OK as int
	
	DECLARE curDEL INSENSITIVE CURSOR
		FOR
			SELECT d.OpModeParamsID FROM deleted d
	
	OPEN curDEL;
	FETCH NEXT FROM curDEL INTO @OpModeParamsID
	
	SET @OK = 1;
	WHILE @OK = 1
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
				
					DELETE FROM [Monitoring].[tNDModel] WHERE OpModeParamsID = @OpModeParamsID
					
					DELETE FROM [Monitoring].[tNDOpModeParams] WHERE OpModeParamsID = @OpModeParamsID
				
				FETCH NEXT FROM curDEL INTO @OpModeParamsID
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	
	CLOSE curDEL;
	DEALLOCATE curDEL;

END
GO
CREATE INDEX
	[IX_OpModeParamsID]
ON
	[Monitoring].[tNDOpModeParamsAssetMap] ([OpModeParamsID])
WITH
	(FILLFACTOR=100)