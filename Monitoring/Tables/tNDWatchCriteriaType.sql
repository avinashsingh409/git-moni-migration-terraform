﻿CREATE TABLE [Monitoring].[tNDWatchCriteriaType] (
		[WatchCriteriaTypeID] INTEGER  NOT NULL  
		, [WatchCriteriaTypeAbbrev] nvarchar(50)  NOT NULL  
		, [WatchCriteriaTypeDesc] nvarchar(255)  NOT NULL  
		, CONSTRAINT [tNDWatchCriteriaType_PK] PRIMARY KEY CLUSTERED ([WatchCriteriaTypeID])

		)
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDWatchCriteriaType'