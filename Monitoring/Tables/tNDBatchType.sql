﻿CREATE TABLE [Monitoring].[tNDBatchType] (
    [BatchTypeId] INT            NOT NULL,
    [BatchAbbrev] NVARCHAR (50)  NOT NULL,
    [BatchDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tNDBatchType] PRIMARY KEY CLUSTERED ([BatchTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDBatchType';

