﻿CREATE TABLE [Monitoring].[tNDCriteriaGroup] (
    [CriteriaGroupID]    INT IDENTITY (1, 1) NOT NULL,
    [OpModeDefinitionID] INT NOT NULL,
    [IsStart]            BIT NOT NULL,
    [DisplayOrder]       INT NULL,
    CONSTRAINT [PK_tNDCriteriaGroup] PRIMARY KEY CLUSTERED ([CriteriaGroupID] ASC),
    CONSTRAINT [FK_tNDCriteriaGroup_tNDOpModeDefinition_OpModeDefinitionID] FOREIGN KEY ([OpModeDefinitionID]) REFERENCES [Monitoring].[tNDOpModeDefinition] ([OpModeDefinitionID])
);




GO

CREATE TRIGGER [Monitoring].[tr_tNDCriteriaGroup_Delete]
	ON [Monitoring].[tNDCriteriaGroup]
	INSTEAD OF DELETE
AS
BEGIN

	DECLARE @CriteriaGroupID as int
	DECLARE @OK as int
	
	DECLARE curGroupDEL INSENSITIVE CURSOR
		FOR
			SELECT d.CriteriaGroupID FROM deleted d
	
	OPEN curGroupDEL;
	FETCH NEXT FROM curGroupDEL INTO @CriteriaGroupID
	
	SET @OK = 1
	WHILE @OK = 1
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
				
					DELETE FROM [Monitoring].[tNDCriteriaLogic] WHERE CriteriaGroupID = @CriteriaGroupID
					
					DELETE FROM [Monitoring].[tNDCriteriaGroup] WHERE CriteriaGroupID = @CriteriaGroupID
				
				FETCH NEXT FROM curGroupDEL INTO @CriteriaGroupID
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	
	CLOSE curGroupDEL;
	DEALLOCATE curGroupDEL;

END