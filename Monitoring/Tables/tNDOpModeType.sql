﻿CREATE TABLE [Monitoring].[tNDOpModeType] (
    [OpModeTypeID]     INT            NOT NULL,
    [OpModeTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [OpModeTypeDesc]   NVARCHAR (255) NOT NULL,
    [Priority]         INT            NOT NULL,
    [DisplayOrder]     INT            NOT NULL,
    CONSTRAINT [PK_tNDOpModeType] PRIMARY KEY CLUSTERED ([OpModeTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDOpModeType';

