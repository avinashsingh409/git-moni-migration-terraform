﻿CREATE TABLE [Monitoring].[tNDModelTemplateOpModeMap] (
    [ModelTemplateOpModeMapID] INT IDENTITY (1, 1) NOT NULL,
    [ModelTemplateID]          INT NOT NULL,
    [OpModeTypeID]             INT NOT NULL,
    CONSTRAINT [PK_tNDModelTemplateOpModeMap] PRIMARY KEY CLUSTERED ([ModelTemplateOpModeMapID] ASC),
    CONSTRAINT [FK_tNDModelTemplateOpModeMap_tNDModelTemplate_ModelTemplateID] FOREIGN KEY ([ModelTemplateID]) REFERENCES [Monitoring].[tNDModelTemplate] ([ModelTemplateID]),
    CONSTRAINT [FK_tNDModelTemplateOpModeMap_tNDOpModeType_OpModeTypeID] FOREIGN KEY ([OpModeTypeID]) REFERENCES [Monitoring].[tNDOpModeType] ([OpModeTypeID]),
    CONSTRAINT [UK_tNDModelTemplateOpModeMap_ModelTemplateID_OpModeTypeID] UNIQUE NONCLUSTERED ([ModelTemplateID] ASC, [OpModeTypeID] ASC)
);

