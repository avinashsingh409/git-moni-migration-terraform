﻿CREATE TABLE [Monitoring].[tNDAlertStatusPriorityType] (
    [AlertStatusPriorityTypeID]     INT            NOT NULL,
    [AlertStatusPriorityTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [AlertStatusPriorityTypeDesc]   NVARCHAR (255) NOT NULL,
    [DisplayOrder]                  INT            NOT NULL,
    CONSTRAINT [PK_tNDAlertStatusPriorityType] PRIMARY KEY CLUSTERED ([AlertStatusPriorityTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tNDAlertStatusPriorityType';

