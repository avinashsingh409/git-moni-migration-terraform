﻿CREATE TABLE [Monitoring].[tModelResults] (
    [ModelResultsID] INT      IDENTITY (1, 1) NOT NULL,
    [ModelID]        INT      NULL,
    [BucketNo]       INT      NULL,
    [ResultsDate]    DATETIME NULL,
    [RSquared]       REAL     NULL,
    [StandardError]  REAL     NULL,
    CONSTRAINT [PK_Monitoring_tModelResults] PRIMARY KEY CLUSTERED ([ModelResultsID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Monitoring', @level1type = N'TABLE', @level1name = N'tModelResults';

