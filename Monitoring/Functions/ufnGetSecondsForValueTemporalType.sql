﻿
CREATE FUNCTION [Monitoring].[ufnGetSecondsForValueTemporalType]
(
	@temporalValue int,
	@temporalTypeId int
)
RETURNS int
AS
BEGIN
	DECLARE @result int;
	
	IF @temporalTypeId = 1  -- seconds
	BEGIN
		SET @result = @temporalValue
	END
	ELSE IF @temporalTypeId = 2  -- minutes
	BEGIN
		SET @result = [Monitoring].[ufnGetSecondsForValueTemporalType] (@temporalValue * 60, 1)
	END
	ELSE IF @temporalTypeId = 3  -- hours
	BEGIN
		SET @result = [Monitoring].[ufnGetSecondsForValueTemporalType] (@temporalValue * 60, 2)
	END
	ELSE IF @temporalTypeId = 4  -- days
	BEGIN
		SET @result = [Monitoring].[ufnGetSecondsForValueTemporalType] (@temporalValue * 24, 3)
	END
	ELSE IF @temporalTypeId = 5  -- months
	BEGIN
		SET @result = [Monitoring].[ufnGetSecondsForValueTemporalType] (@temporalValue * 30, 4)
	END
	ELSE IF @temporalTypeId = 6  -- years
	BEGIN
		SET @result = [Monitoring].[ufnGetSecondsForValueTemporalType] (@temporalValue * 12, 5)
	END
	ELSE IF @temporalTypeId = 7  -- weeks
	BEGIN
		SET @result = [Monitoring].[ufnGetSecondsForValueTemporalType] (@temporalValue * 7, 4)
	END	
	RETURN @result;
END
GO
GRANT EXECUTE
    ON OBJECT::[Monitoring].[ufnGetSecondsForValueTemporalType] TO [TEUser]
    AS [dbo];

