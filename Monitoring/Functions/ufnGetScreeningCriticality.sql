﻿
create function [Monitoring].[ufnGetScreeningCriticality]
(
	@Assets Base.tpIntList readonly,
	@UserId int
)
returns @Criticality table
(
	ModelID int not null,
	AssetCriticality int not null,
	ModelCriticality int not null,
	primary key clustered
	(
		ModelID asc
	)
)
as
begin

	-- first get all of the models and their assets
	declare @models table (
		ModelID int not null,
		AssetID int not null,
		primary key clustered
		(
			ModelID asc
		)
	);
	insert into @models (ModelID, AssetID)
	select m.ModelID, a.id
	from @Assets a
	join ProcessData.tAssetVariableTypeTagMap map on a.id = map.AssetID
	join Monitoring.tNDModel m on map.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID
	;

	declare @targetCount int = (select count (ModelID) from @models);


	-- now get Asset Criticality
	declare @criticalityOITypeID int = 204; -- Asset.tCriticality CriticalityOI
	declare @stopAtLevel int = 4; -- Asset.tAssetType unit
	declare @fallbackCriticality int = 1;
	declare @fallbackOptionTypeID int = (select AttributeOptionTypeID from Asset.tAttributeOptionType where AttributeTypeID = @criticalityOITypeID and AttributeOptionTypeDesc = cast(@fallbackCriticality as nvarchar(255)));
	declare @assetCriticality table (
		AssetID int not null,
		AssetCriticality int not null,
		primary key clustered
		(
			AssetID asc
		)
	);
	insert into @assetCriticality (AssetID, AssetCriticality)
	select t.AssetID, cast(opt.AttributeOptionTypeDesc as int) from Asset.ufnGetInheritedAttributes(@Assets, @criticalityOITypeID, @UserId, default, default, @fallbackOptionTypeID, default, default, default, default, 1, default, default, default) t
																join Asset.tAttributeOptionType opt on t.AttributeOptionTypeID = opt.AttributeOptionTypeID
	;


	-- now get Model Criticality
	-- if model is out of bounds, check actual against upper and lower
	--	if actual > upper, then take model's upper criticality
	--	if actual < lower, then take model's lower criticality
	--	if model criticality is null, take Asset Criticality
	-- if model is not out of bounds, use fallback

	-- first get current model eval
	declare @modelEval table (
		ModelID int not null,
		Actual real null,
		EUpper real null,
		ELower real null,
		PercentOOB real null,
		primary key clustered
		(
			ModelID asc
		)
	);
	insert into @modelEval (ModelID, Actual, EUpper, ELower, PercentOOB)
	select m.ModelID, e.NDActual, e.NDUpper, e.NDLower, e.PercentOOB
	from @models m left join Monitoring.tNDModelEvaluation e on m.ModelID = e.ModelID
	;

	-- and split them into out-of-bounds and in-bounds batches
	declare @ob Base.tpIntList;
	insert into @ob (id) select ModelID from @modelEval where PercentOOB <> 0;
	declare @ib Base.tpIntList;
	insert into @ib (id)
	select ModelID from @modelEval
	except
	select id from @ob
	;

	-- start with models that are in bounds
	insert into @Criticality (ModelID, AssetCriticality, ModelCriticality)
	select mdl.ModelID, a.AssetCriticality, @fallbackCriticality
	from @models mdl join @assetCriticality a on mdl.AssetID = a.AssetID join @ib m on mdl.ModelID = m.id
	;

	-- if everything is accounted for we can return
	if (select count (ModelID) from @Criticality) = @targetCount
	begin
		return;
	end

	-- now split our out-of-bounds into upper and lowers
	declare @obUpper Base.tpIntList;
	insert into @obUpper (id)
	select id from @ob m join @modelEval e on m.id = e.ModelID where e.Actual > e.EUpper
	;

	declare @obLower Base.tpIntList;
	insert into @obLower (id)
	select id from @ob
	except
	select id from @obUpper
	;

	-- and get model config criticality
	declare @modelCriticality table (
		ModelID int not null,
		UpperCriticality int null,
		LowerCriticality int null
		primary key clustered
		(
			ModelID asc
		)
	);
	insert into @modelCriticality (ModelID, UpperCriticality, LowerCriticality)
	select ModelID, UpperCriticality, LowerCriticality 
	from Monitoring.tNDModelCriticality crit
	JOIN  @ob obModelId on obModelId.id = crit.ModelID

	-- and fill in any gaps
	update c set c.UpperCriticality = a.AssetCriticality
	from @modelCriticality c join @models m on c.ModelID = m.ModelID join @assetCriticality a on m.AssetID = a.AssetID
	where c.UpperCriticality is null
	;
	update c set c.LowerCriticality = a.AssetCriticality
	from @modelCriticality c join @models m on c.ModelID = m.ModelID join @assetCriticality a on m.AssetID = a.AssetID
	where c.LowerCriticality is null
	;

	-- and now add them to the return
	insert into @Criticality (ModelID, AssetCriticality, ModelCriticality)
	select mdl.ModelID, a.AssetCriticality, c.UpperCriticality
	from @models mdl join @assetCriticality a on mdl.AssetID = a.AssetID join @obUpper m on mdl.ModelID = m.id join @modelCriticality c on mdl.ModelID = c.ModelID
	;
	insert into @Criticality (ModelID, AssetCriticality, ModelCriticality)
	select mdl.ModelID, a.AssetCriticality, c.LowerCriticality
	from @models mdl join @assetCriticality a on mdl.AssetID = a.AssetID join @obLower m on mdl.ModelID = m.id join @modelCriticality c on mdl.ModelID = c.ModelID
	;

	return;
end
GO
GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetScreeningCriticality] TO [TEUser]
    AS [dbo];

