﻿
/****** Object:  Table [Monitoring].[tSubscriptionNDAlertSecurityUserIDMap] ******/
CREATE FUNCTION [Monitoring].[ufnDoesUserHaveAccessToNDModel] (
	@ModelID int, 
	@SecurityUserID int
	)
RETURNS bit
AS
BEGIN
	DECLARE @Result as Bit
	DECLARE @AssetID as Int
    IF @securityuserid >= 1
	  BEGIN
		SET @AssetID = (SELECT map.AssetID FROM Monitoring.tNDModel model
		INNER JOIN ProcessData.tAssetVariableTypeTagMap map ON model.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
		WHERE ModelID = @ModelID)

		Set @Result = Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1)

	  END
	ELSE
	  BEGIN
	    Set @Result = 0
	  END
	RETURN @Result
END
GO
GRANT EXECUTE ON OBJECT::Monitoring.ufnDoesUserHaveAccessToNDModel TO TEUser AS dbo;