﻿create function [Monitoring].[ufnGetConfigCriticality]
(
	@ModelIDs Base.tpIntList readonly
)
returns @Criticality table
(
	ModelID int not null,
	UpperCriticality int null,
	LowerCriticality int null,
	primary key clustered
	(
		ModelID asc
	)
)
as
begin

	-- business logic (spread across this function and Monitoring.ufnGetDefaultConfigCriticality)
	--	if the model is standard
	--		take its template's upper and lower criticality values
	--	if not,
	--		if the model's criticality record's UseDefault is true
	--			search for a template
	--				if template is found, take its upper and lower criticality values
	-- ** note that this logic DOES NOT operate under the contention that there could be separate upper and lower criticality providers.
	-- ** the P.O. has confirmed this to be correct. if, in the future, we desire to support separates,
	-- ** this function and its Default partner will need to be refactored in such a way that
	-- ** we can't blindly take the min template that covers the steady state, then prioritized, then any op mode(s) for a model;
	-- ** we'd have to bifurcate the lookup to run separately for upper and lower criticalities and move from a min aggregator to a
	-- ** select top 1 technique with a where clause throwing out any non-null providers

	-- handle the standards first
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select m.ModelID, mt.UpperCriticality, mt.LowerCriticality
	from Monitoring.tNDModel m join @ModelIDs ids on m.ModelID = ids.id join Monitoring.tNDModelTemplate mt on m.ModelTemplateID = mt.ModelTemplateID
	;

	-- check for completion; no need to continue if we're finished
	if (select count (ModelID) from @Criticality) = (select count (id) from @ModelIDs)
	begin
		return;
	end

	-- get customs
	declare @customs Base.tpIntList;
	insert into @customs (id)
	select ModelID from Monitoring.tNDModel m join @ModelIds ids on m.ModelID = ids.id where m.ModelTemplateID is null
	;

	-- there is a chance that a model's criticality record doesn't exist because it's been
	-- deleted outside of the normal procedures. in that case, give it an nulled record; hitherto shalt thou come, but no further
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select id, null, null from @customs
	except
	select cstm.id, null, null
	from @customs cstm
	join Monitoring.tNDModelCriticality c on cstm.id = c.ModelID
	;

	if (select count (ModelID) from @Criticality) = (select count (id) from @ModelIDs)
	begin
		return;
	end

	-- we can shrink our @customs table to toss out any that didn't have a criticality record
	delete from @customs where id in (select ModelID from @Criticality);

	-- handle customs with UseDefault = 0
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select c.ModelID, c.UpperCriticality, c.LowerCriticality
	from Monitoring.tNDModelCriticality c join @customs cstm on c.ModelID = cstm.id where c.UseDefault = 0
	;

	if (select count (ModelID) from @Criticality) = (select count (id) from @ModelIDs)
	begin
		return;
	end

	-- now we can shrink away customs with UseDefault = 0
	delete from @customs where id in (select ModelID from @Criticality);



	-- and now it gets interesting


	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select ModelID, UpperCriticality, LowerCriticality from Monitoring.ufnGetDefaultConfigCriticality(@customs);

	return;
end
GO
GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetConfigCriticality] TO [TEUser]
    AS [dbo];

