﻿CREATE FUNCTION [Monitoring].[ufnModelCurrentActionItemStatus] 
(	
   @modelID as int = NULL
)
RETURNS @modelCurrentActionItemTypeStatus TABLE(
	ModelID int,
	IsDiagnoseSet bit null,
	IsDiagnoseCleared bit null,
	IsWatchSet bit null,
	IsWatchCleared bit null,
	IsWatchExpired bit null,
	IsModelMaintenanceSet bit null,
	IsModelMaintenanceCleared bit null,
	IsIssueCreated bit null,
	IsIssueClosed bit null,
	IsNoteAdded bit null,
	IsIgnoreSet bit null,
	IsClearAlertStatus bit null,
	IsIgnoreExpiration bit null,
	IsWatchOverride bit NULL,
	IsStopIgnoring bit NULL
)
AS
BEGIN

    DECLARE @modelIDs as Base.tpIntList
	IF @modelID IS NOT NULL
	  BEGIN
	  INSERT INTO @modelIDs VALUES (@modelID)
	  END

	INSERT INTO @modelCurrentActionItemTypeStatus select 
		ModelID,
		IsDiagnoseSet,
		IsDiagnoseCleared,
		IsWatchSet,
		IsWatchCleared,
		IsWatchExpired,
		IsModelMaintenanceSet,
		IsModelMaintenanceCleared,
		IsIssueCreated,
		IsIssueClosed,
		IsNoteAdded,
		IsIgnoreSet,
		IsClearAlertStatus,
		IsIgnoreExpiration,
		IsWatchOverride,
		IsStopIgnoring
	 from [Monitoring].[ufnModelCurrentActionItemStatuses] (@modelIDs)
		  	
	RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnModelCurrentActionItemStatus] TO [TEUser]
    AS [dbo];
GO