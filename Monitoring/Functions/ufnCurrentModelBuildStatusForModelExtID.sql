﻿
CREATE FUNCTION [Monitoring].[ufnCurrentModelBuildStatusForModelExtID]
(
	@modelExtID nvarchar(255)
)
RETURNS @currentModelBuild TABLE(
	ModelBuildID int,
	ModelExtID nvarchar(255),
	BuildStatusTypeID int,
	BuildStatusTypeAbbrev nvarchar(50),
	BuildStatus nvarchar(max),
	BuildStatusTime datetime
)
AS
BEGIN

	INSERT INTO @currentModelBuild (
		ModelBuildID
		, ModelExtID
		, BuildStatusTypeID
		, BuildStatusTypeAbbrev
		, BuildStatus
		, BuildStatusTime
	)
	SELECT TOP 1
		bld.ModelBuildID
		, bld.ModelExtID
		, bld.BuildStatusTypeID
		, btyp.ModelBuildStatusTypeAbbrev
		, bld.BuildStatus
		, bld.BuildStatusTime
	FROM Monitoring.tNDModelBuild bld
	JOIN Monitoring.tNDModelBuildStatusType btyp ON bld.BuildStatusTypeID = btyp.ModelBuildStatusTypeID
	WHERE bld.ModelExtID = @modelExtID
	ORDER BY bld.BuildStatusTime DESC

	RETURN
END