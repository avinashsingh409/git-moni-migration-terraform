﻿
CREATE FUNCTION [Monitoring].[ufnGetDisplayModelPath] (
	@ModelID int, 
	@SecurityUserID int
	)
RETURNS NVARCHAR(MAX) 
AS
BEGIN
	DECLARE @AssetID as int
    DECLARE @AssetIDs as BASE.tpIntList
	DECLARE @AssetHierarchy table
	(
		  [AssetID] int NOT NULL
		, [AssetAbbrev] NVARCHAR(50) NOT NULL
		, [Level] int NOT NULL
		, [AssetTypeID] int NOT NULL
		, [HasAccess] bit NOT NULL DEFAULT (0)
	)
	DECLARE @Path NVARCHAR(MAX) 
	DECLARE @UnitAsset NVARCHAR(50) 
	DECLARE @ParentAsset NVARCHAR(50) 
	DECLARE @ModelAsset NVARCHAR(50) 
	DECLARE @count as int
	DECLARE @unitLevel as int

	SET @Path = '';
    IF @securityuserid >= 1
	  BEGIN
		SET @AssetID = (SELECT map.AssetID FROM Monitoring.tNDModel model
		INNER JOIN ProcessData.tAssetVariableTypeTagMap map ON model.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
		WHERE ModelID = @ModelID)

		--Get all asset ancestor up to the Unit Level
		INSERT INTO @AssetHierarchy ([AssetID], [AssetAbbrev], [Level], [AssetTypeID])
			SELECT  a.AssetID, b.AssetAbbrev, a.AncestryGenerationsAway, c.AssetTypeID
			FROM Asset.ufnAssetAncestorsWithLevel(@AssetID) a JOIN Asset.tAsset b ON a.AssetID = b.AssetID
			JOIN Asset.tAssetClassType c ON b.AssetClassTypeID = c.AssetClassTypeID 
			WHERE c.AssetTypeID >= 4
			ORDER BY a.AncestryGenerationsAway DESC;
		
		--Insert asset tied to nDModel
		INSERT INTO @AssetHierarchy ([AssetID], [AssetAbbrev], [Level], [AssetTypeID])
			SELECT  a.AssetID, a.AssetAbbrev, 0, b.AssetTypeID
			FROM Asset.tAsset a 
			JOIN Asset.tAssetClassType b ON a.AssetClassTypeID = b.AssetClassTypeID 
			WHERE a.AssetID = @AssetID

		--Get the closest level unit
		SET @unitLevel = (SELECT MIN ([Level]) FROM @AssetHierarchy WHERE [AssetTypeID] = 4);
		--Remove all units above the selected 'closest' unit.
		DELETE FROM @AssetHierarchy WHERE [Level] > @unitLevel
		--Remove all intermediate assets between unit and model's parent asset
		DELETE FROM @AssetHierarchy WHERE [Level] < @unitLevel AND [Level] > 1

		--Check for security access, and set [HasAccess] = 1 where user has access.
		INSERT INTO @AssetIDs SELECT AssetID FROM @AssetHierarchy;
		UPDATE @AssetHierarchy SET [HasAccess] = 1 FROM @AssetHierarchy a INNER JOIN 
			Asset.ufnUserHasAccessToAssets(@SecurityUserID, @AssetIDs) ac ON a.AssetID = ac.AssetID; 
		
		--Check to ensure user have access to the model or its ancestors before constructing the path
		SELECT @count = COUNT(*) FROM @AssetHierarchy WHERE [HasAccess] = 1
		IF(@count > 0)
		BEGIN
			--Replace [AssetAbbrev] with '~' for asset that user do not have access to
			UPDATE @AssetHierarchy SET [AssetAbbrev] = '~' WHERE [HasAccess] = 0;
		
			--Create the model path and return
			SET @UnitAsset = (SELECT [AssetAbbrev] FROM @AssetHierarchy WHERE [AssetTypeID] = 4)
			SET @ParentAsset = (SELECT [AssetAbbrev] FROM @AssetHierarchy WHERE [Level] = 1 AND [AssetTypeID] != 4)
			SET @ModelAsset = (SELECT [AssetAbbrev] FROM @AssetHierarchy WHERE [Level] = 0 AND [AssetTypeID] != 4)
 
			IF (@UnitAsset IS NOT NULL)
			BEGIN 
				SET @Path = @Path + @UnitAsset;
			END 
			IF (@ParentAsset IS NOT NULL OR @ModelAsset IS NOT NULL)
			BEGIN 
				SET @Path = @Path + ':';
			END 

			IF (@ParentAsset IS NOT NULL)
			BEGIN 
				SET @Path = @Path + @ParentAsset;
			END 

			IF (@ModelAsset IS NOT NULL)
			BEGIN
				IF(@ParentAsset IS NOT NULL)
				BEGIN
					SET @Path = @Path + '\' + @ModelAsset;
				END
				ELSE
				BEGIN
					--Model's parent is Unit
					SET @Path = @Path + @ModelAsset;
				END
			END 
		END
	  END
	RETURN @Path
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[ufnGetDisplayModelPath] TO [TEUser]
    AS [dbo];
GO