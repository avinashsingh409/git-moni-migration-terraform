﻿CREATE FUNCTION [Monitoring].[ufnGetRoleSecuritizedModelsFromUser] 
(	
	-- Add the parameters for the function here
	@securityUserID int,
	@filterCategoryIDs Base.tpIntList READONLY,
	@filterCategoryTypeID int = NULL
)
RETURNS TABLE AS RETURN
(
	SELECT
		*
	FROM
		Monitoring.ufnGetRoleSecuritizedSpecificCategorizedModelsFromUser(@securityUserID, @filterCategoryIDs, @filterCategoryTypeID)
	UNION ALL
	SELECT
		DISTINCT(tNDM.ModelID)
	FROM
		Monitoring.tNDModel tNDM
	LEFT JOIN
		Monitoring.tNDModelCategoryMap tMCM ON tNDM.ModelID = tMCM.ModelID
	WHERE
		tMCM.ModelID IS NULL
)
GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetRoleSecuritizedModelsFromUser] TO [TEUser]
    AS [dbo]
GO