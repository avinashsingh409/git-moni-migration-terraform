CREATE   FUNCTION [Monitoring].[ufnGetFilteredModelInfoFromAssetIDs] 
(	
	@assetIDs Base.tpIntList READONLY,
	@isAlert bit = NULL,
	@addedToAlertsStart datetime = NULL,
	@addedToAlertsEnd datetime = NULL,
	@active bit = NULL,
	@built bit = NULL,
	@hasCalcPoint bit = NULL,
	@tagName NVARCHAR(400) = NULL,
	@tagDesc NVARCHAR(400) = NULL,
	@modelName NVARCHAR(255) = NULL,
	@modelTypeID Base.tpIntList READONLY,
	@engUnits NVARCHAR(255) = NULL,
	@modelOpModeID Base.tpIntList READONLY,
	@isDiagnose bit = NULL,
	@isWatch bit = NULL,
	@isWatchOverride bit = NULL,
	@isMaintenance bit = NULL,
	@isIgnore bit = NULL,
	@isOpenIssue bit = NULL,
	@evaluationErrorMessage NVARCHAR(400) = NULL,
	@priority int = NULL,
	@outOfBounds bit = NULL,
	@alertPriority int = NULL,
	@alertType NVARCHAR(255) = NULL,
	@currentBuildStatusTypeID Base.tpIntList READONLY,
	@securityUserId int = -1,
	@assetCriticality Base.tpIntList READONLY,
	@modelCriticality Base.tpIntList READONLY,
	@modelIDs Base.tpIntList READONLY,
	@filterCategoryIDs Base.tpIntList READONLY
)
RETURNS 
@modelInfo TABLE
  (
    ModelID                        INT      PRIMARY KEY,
	ReferenceModelID			   NVARCHAR(400) NULL,
    ModelExtID                     VARCHAR(MAX),
    Active                         BIT      NULL,
    Built                          BIT      NULL,
    TagName                        VARCHAR(MAX),
    TagDesc                        VARCHAR(MAX),
	TagNDId						   NVARCHAR(400) NULL,
    ModelName                      VARCHAR(MAX),
    ModelTypeID                    INT      NULL,
    ModelTypeAbbrev                VARCHAR(MAX),
    Score                          INT      NULL,
    EngUnits                       VARCHAR(MAX),
    AssetAbbrev                    VARCHAR(MAX),
    AssetId                        INT      NULL,
	GlobalID					   VARCHAR(MAX),
    UnitAbbrev                     VARCHAR(MAX),
    UnitId                         INT      NULL,
    MAELower                       REAL     NULL,
    MAEUpper                       REAL     NULL,
    ActionDiagnose                 BIT      NULL,
    ActionWatch                    BIT      NULL,
    ActionWatchOverride            BIT      NULL,
    ActionModelMaintenance         BIT      NULL,
    HasOpenIssue                   BIT      NULL,
    Alerts                         INT      NULL,
    Alert                          BIT      NULL,
    PercentOutOfBounds             REAL     NULL,
    UpperLimit                     REAL     NULL,
    LowerLimit                     REAL     NULL,
    Actual                         REAL     NULL,
    Expected                       REAL     NULL,
    HiHiAlert                      BIT      NULL,
    HiHiAlertSinceTime             DATETIME NULL,
    LowLowAlert                    BIT      NULL,
    LowLowAlertSinceTime           DATETIME NULL,
    AreaFastResponseAlert          BIT      NULL,
    AreaFastResponseAlertSinceTime DATETIME NULL,
    AreaSlowResponseAlert          BIT      NULL,
    AreaSlowResponseAlertSinceTime DATETIME NULL,
    FrequencyAlert                 BIT      NULL,
    FrequencyAlertSinceTime        DATETIME NULL,
    OscillationAlert               BIT      NULL,
    OscillationAlertSinceTime      DATETIME NULL,
    FrozenDataAlert                BIT      NULL,
    FrozenDataAlertSinceTime       DATETIME NULL,
    WatchOverrideAlert             BIT      NULL,
    WatchOverrideAlertSinceTime    DATETIME NULL,
    IgnoreOverrideAlert            BIT      NULL,
    IgnoreOverrideAlertSinceTime   DATETIME NULL,
    SerializedOpMode               INT      NULL,
    PrioritizedOpMode              VARCHAR(MAX),
    EvaluationErrorMessage         VARCHAR(MAX),
    AddedToAlerts                  DATETIME NULL,
    AlertStatusPriorityTypeID      INT      NULL,
    CalcAlertPriority              INT,
    AlertStatusPriorityTypeDesc    VARCHAR(255),
    ModelActionItemPriorityTypeID  INT      NULL,
    NotePriority                   VARCHAR(MAX),
    LastNote                       VARCHAR(MAX),
    LastNoteDate                   DATETIME NULL,
    TagGroup                       VARCHAR(MAX),
    OutOfBounds                    INT      NULL,
    Ignore                         BIT      NULL,
    BuildStatusTypeAbbrev          VARCHAR(MAX),
    BuildStatus                    VARCHAR(MAX),
	IsStandard					   BIT,
	AssetCriticality			   INT,
	ModelCriticality			   INT,
    ModelConfigURL                 VARCHAR(2048) NULL,
    DiagnosticDrilldownURL         VARCHAR(2048) NULL
  )

AS
BEGIN    

DECLARE @ModelIdCnt as int
SELECT @ModelIdCnt = COUNT(*) FROM @modelIDs

INSERT INTO @modelInfo
SELECT
      model.ModelID,
	  model.ReferenceModelID,
      model.ModelExtID,
      model.Active,
      model.Built,
      tag.TagName,
      tag.TagDesc,
	  tag.NDId AS TagNDId,
      model.ModelName,
      model.ModelTypeID,
      modeltype.ModelTypeAbbrev,
      predmap.Score,
      tag.EngUnits,
      ast.AssetAbbrev,
      ast.AssetID,
	  ast.GlobalID,
	  '' as UnitAbbrev,
	  0 as UnitID,      
      params.MAELower,
      params.MAEUpper,
	  NULL AS ActionDiagnose,
      NULL AS ActionWatch,      
      mdleval.ActionWatchOverride as ActionWatchOverride,
	  NULL AS ActionModelMaintenance,      
      CASE SUM(ISNULL(issues.AssetIssueID,0))
      WHEN 0
        THEN NULL
      ELSE 1 END                           AS HasOpenIssue,
      mdleval.NDAlert                      AS Alerts,
      mdleval.Alert,
      mdleval.PercentOOB                   AS PercentOutOfBounds,
      mdleval.NDUpper                      AS UpperLimit,
      mdleval.NDLower                      AS LowerLimit,
      mdleval.NDActual                     AS Actual,
      mdleval.NDExpected                   AS Expected,
      mdleval.HiHiAlert,
      mdleval.HiHiAlertSinceTime,
      mdleval.LowLowAlert,
      mdleval.LowLowAlertSinceTime,
      mdleval.AreaFastResponseAlert,
      mdleval.AreaFastResponseAlertSinceTime,
      mdleval.AreaSlowResponseAlert,
      mdleval.AreaSlowResponseAlertSinceTime,
      mdleval.FrequencyAlert,
      mdleval.FrequencyAlertSinceTime,
      mdleval.OscillationAlert,
      mdleval.OscillationAlertSinceTime,
      mdleval.FrozenDataAlert,
      mdleval.FrozenDataAlertSinceTime,
      mdleval.WatchOverrideAlert,
      mdleval.WatchOverrideAlertSinceTime,
      mdleval.IgnoreOverrideAlert,
      mdleval.IgnoreOverrideAlertSinceTime,
      mdleval.SerializedOpMode,
      mdleval.PrioritizedOpMode,
      mdleval.EvaluationErrorMessage,
      mdleval.AddedToAlerts,
      mdleval.AlertStatusPriorityTypeID,
      (CASE mdleval.AlertStatusPriorityTypeID
       WHEN NULL
         THEN 0
       WHEN 1
         THEN 2
       WHEN 2
         THEN 1 END)                       AS CalcAlertPriority,
      altp.AlertStatusPriorityTypeDesc,
      tmai.ModelActionItemPriorityTypeID   AS ModelActionItemPriorityTypeID,
      tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
      tmai.NoteText                        AS LastNote,
      tmai.ChangeDate                      AS LastNoteDate,
      model.NDProjectPath                    AS TagGroup,
      CASE WHEN mdleval.PercentOOB <> 0
        THEN 1
      ELSE 0 END                           AS OutofBounds,
      tmas.IgnoreStatus                    AS Ignore,
      buildStatus.BuildStatusTypeAbbrev    AS BuildStatusTypeAbbrev,
      buildStatus.BuildStatus              AS BuildStatus,	  
	  CAST(CASE WHEN model.ModelTemplateID IS NOT NULL AND model.ModelTemplateID > 0 THEN 1 ELSE 0 END AS BIT) AS IsStandard,
	  NULL				   AS AssetCriticality,
	  NULL				   AS ModelCriticality,
      model.ModelConfigURL,
      model.DiagnosticDrilldownURL
	  
	FROM @assetIDs br
	INNER JOIN ProcessData.tAssetVariableTypeTagMap map
		ON br.id = map.AssetID
	INNER JOIN ProcessData.tTag tag
		ON map.TagID = tag.PDTagID
	INNER JOIN Asset.tAsset ast 
		ON map.AssetID = ast.AssetID
	INNER JOIN Monitoring.tNDModel model
		ON map.AssetVariableTypeTagMapID = model.AssetVariableTypeTagMapID
	INNER JOIN Monitoring.tNDModelType modeltype
		ON model.ModelTypeID = modeltype.ModelTypeID
	INNER JOIN Monitoring.tNDModelOpModeTypeMap opmodemap
		ON model.ModelID = opmodemap.ModelID
	INNER JOIN Monitoring.tNDOpModeParams params
		ON model.OpModeParamsID = params.OpModeParamsID
	LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap
		ON (model.ModelID = predmap.ModelID) AND (predmap.Active = 1)
	LEFT JOIN Monitoring.tNDModelActionItem tmai
		ON tmai.ModelID = model.ModelID
	LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt
        ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
	LEFT JOIN Monitoring.tNDModel_AlertScreening tmas
		ON tmas.ModelID = model.ModelID and tmas.Until > GETDATE()
	LEFT JOIN Monitoring.tNDModelEvaluation mdleval
		ON model.ModelID = mdleval.ModelID
	INNER JOIN Monitoring.ufnGetRoleSecuritizedModelsFromUser(@securityUserId, @filterCategoryIDs, 6) AS filteredModels ON model.ModelID = filteredModels.ModelID
	LEFT JOIN Monitoring.tNDAlertStatusPriorityType altp
        ON mdleval.AlertStatusPriorityTypeID = altp.AlertStatusPriorityTypeID	
	LEFT JOIN Diagnostics.tAssetIssue issues on issues.AssetID = map.AssetID AND ActivityStatusID = 1 and issues.IsHidden = 0	
	LEFT JOIN (
			SELECT bld.ModelBuildID as lastModelBuildId
			, bld.ModelExtID
			, bld.BuildStatusTypeID
			, btyp.ModelBuildStatusTypeAbbrev as BuildStatusTypeAbbrev
			, bld.BuildStatus
			, bld.BuildStatusTime
			FROM Monitoring.tNDModelBuild bld
				 INNER JOIN (SELECT ModelExtID as modelExtId, max (ModelBuildID) as lastBuild
								 FROM Monitoring.tNDModelBuild
								 GROUP BY ModelExtID) lastActivity on lastActivity.modelExtId = bld.ModelExtID AND lastActivity.lastBuild = bld.ModelBuildID
			INNER JOIN Monitoring.tNDModelBuildStatusType btyp ON bld.BuildStatusTypeID = btyp.ModelBuildStatusTypeID
      ) buildStatus on buildStatus.ModelExtID = model.ModelExtID
	WHERE
	(@ModelIdCnt = 0 OR model.ModelID IN (SELECT ID FROM @modelIDs)) AND
	(@isAlert IS NULL OR (mdleval.Alert = @isAlert)) AND
	(@addedToAlertsStart IS NULL OR (mdleval.AddedToAlerts >= @addedToAlertsStart)) AND
	(@addedToAlertsEnd IS NULL OR (mdleval.AddedToAlerts <= @addedToAlertsEnd)) AND
	(@active IS NULL OR (model.Active = @active)) AND
	(@built IS NULL OR (model.Built = @built)) AND
	(@hasCalcPoint IS NULL OR (@hasCalcPoint = 1 AND model.CalcPointID IS NOT NULL) 
				OR (@hasCalcPoint = 0 AND model.CalcPointID IS NULL)) AND
	(@tagName IS NULL OR (tag.TagName LIKE '%' + @tagName + '%')) AND
	(@tagDesc IS NULL OR (tag.TagDesc LIKE '%' + @tagDesc + '%')) AND
	(@modelName IS NULL OR (model.ModelName LIKE '%' + @modelName + '%')) AND
	(@engUnits IS NULL OR (tag.EngUnits LIKE '%' + @engUnits + '%')) AND
	(@isIgnore IS NULL OR (tmas.IgnoreStatus=@isIgnore 
				OR (@isIgnore = 0 AND ((tmas.IgnoreStatus IS NULL OR tmas.IgnoreStatus = 0) OR (mdleval.IgnoreOverrideAlert = 1) ))
		  ) 
	 ) AND	
	(@priority IS NULL OR (tmai.ModelActionItemPriorityTypeID=@priority 
				OR (@priority = 0 AND (tmai.ModelActionItemPriorityTypeID IS NULL OR tmai.ModelActionItemPriorityTypeID = 0 ))
		  ) 
	 ) AND	
	(@isWatchOverride IS NULL OR (mdleval.ActionWatchOverride = @isWatchOverride)) AND
	((@isOpenIssue IS NULL) OR (@isOpenIssue = 1 AND issues.GlobalID IS NOT NULL) OR (@isOpenIssue = 0 AND issues.GlobalID IS NULL)) AND
	(@evaluationErrorMessage IS NULL OR (mdleval.EvaluationErrorMessage = @evaluationErrorMessage)) AND
	(@outOfBounds IS NULL OR (CASE WHEN mdleval.PercentOOB <> 0 THEN 1 ELSE 0 END = @outOfBounds)) AND
	(@alertPriority IS NULL or (mdleval.AlertStatusPriorityTypeID = @alertPriority)) AND
	(@alertType IS NULL OR (CASE @alertType WHEN 'HiHi' THEN CASE WHEN mdleval.HiHiAlert = 1 THEN 1 ELSE 0 END
								WHEN 'LowLow' THEN CASE WHEN mdleval.LowLowAlert = 1 THEN 1 ELSE 0 END
								WHEN 'AreaFastResponse' THEN CASE WHEN mdleval.AreaFastResponseAlert = 1 THEN 1 ELSE 0 END
								WHEN 'AreaSlowResponse' THEN CASE WHEN mdleval.AreaSlowResponseAlert = 1 THEN 1 ELSE 0 END
								WHEN 'Frequency' THEN CASE WHEN mdleval.FrequencyAlert = 1 THEN 1 ELSE 0 END
								WHEN 'Oscillation' THEN CASE WHEN mdleval.OscillationAlert = 1 THEN 1 ELSE 0 END
								WHEN 'WatchOverride' THEN CASE WHEN mdleval.WatchOverrideAlert = 1 THEN 1 ELSE 0 END
								WHEN 'FrozenData' THEN CASE WHEN mdleval.FrozenDataAlert = 1 THEN 1 ELSE 0 END
								WHEN 'IgnoreOverride' THEN CASE WHEN mdleval.IgnoreOverrideAlert = 1 THEN 1 ELSE 0 END
							END = 1))
	AND
	(NOT EXISTS (SELECT 1 FROM @modelTypeID) OR (model.ModelTypeID IN (SELECT id FROM @modelTypeID))) AND
	(NOT EXISTS (SELECT 1 FROM @modelOpModeID) OR (opmodemap.OpModeTypeID IN (SELECT id FROM @modelOpModeID))) AND	
	(NOT EXISTS (SELECT 1 FROM @currentBuildStatusTypeID) OR (EXISTS (SELECT 1 FROM Monitoring.ufnCurrentModelBuildStatusForModelExtID(model.ModelExtID) c INNER JOIN @currentBuildStatusTypeID b ON c.BuildStatusTypeID = b.id))
	) 

	GROUP BY model.ModelID, model.ModelExtID,
	  model.ReferenceModelID,
      model.ModelConfigURL,
      model.DiagnosticDrilldownURL,
      model.Active,
      model.Built,
      tag.TagName,
      tag.TagDesc,
	  tag.NDId,
      model.ModelName,
      model.ModelTypeID,
      modeltype.ModelTypeAbbrev,
      predmap.Score,
      tag.EngUnits,
      ast.AssetAbbrev,
      ast.AssetID,
	  ast.GlobalID,      
      params.MAELower,
      params.MAEUpper,      
      mdleval.ActionWatchOverride,      
      mdleval.NDAlert,
      mdleval.Alert,
      mdleval.PercentOOB,
      mdleval.NDUpper,
      mdleval.NDLower,
      mdleval.NDActual,
      mdleval.NDExpected,
      mdleval.HiHiAlert,
      mdleval.HiHiAlertSinceTime,
      mdleval.LowLowAlert,
      mdleval.LowLowAlertSinceTime,
      mdleval.AreaFastResponseAlert,
      mdleval.AreaFastResponseAlertSinceTime,
      mdleval.AreaSlowResponseAlert,
      mdleval.AreaSlowResponseAlertSinceTime,
      mdleval.FrequencyAlert,
      mdleval.FrequencyAlertSinceTime,
      mdleval.OscillationAlert,
      mdleval.OscillationAlertSinceTime,
      mdleval.FrozenDataAlert,
      mdleval.FrozenDataAlertSinceTime,
      mdleval.WatchOverrideAlert,
      mdleval.WatchOverrideAlertSinceTime,
      mdleval.IgnoreOverrideAlert,
      mdleval.IgnoreOverrideAlertSinceTime,
      mdleval.SerializedOpMode,
      mdleval.PrioritizedOpMode,
      mdleval.EvaluationErrorMessage,
      mdleval.AddedToAlerts,
      mdleval.AlertStatusPriorityTypeID,
      altp.AlertStatusPriorityTypeDesc,
      tmai.ModelActionItemPriorityTypeID,
      tmaipt.ModelActionItemPriorityAbbrev,
      tmai.NoteText,
      tmai.ChangeDate,
      model.NDProjectPath,
      mdleval.PercentOOB,
      tmas.IgnoreStatus,
      buildStatus.BuildStatusTypeAbbrev,
	  buildStatus.BuildStatus,
	  CAST(CASE WHEN model.ModelTemplateID IS NOT NULL AND model.ModelTemplateID > 0 THEN 1 ELSE 0 END AS BIT)	  
	  OPTION (RECOMPILE)
	  
declare @usedAssetIds as base.tpintlist
insert into @usedAssetIds select distinct assetid from @modelInfo 

-- Handle the case where the filter criteria returns no models (thus no assets)
if exists(select * from @usedAssetIds)
  BEGIN
	declare @Criticality table
		(
			ModelID int not null,
			AssetCriticality int not null,
			ModelCriticality int not null,
			primary key clustered
			(
				ModelID asc
			)
		)

	insert into @Criticality select * from Monitoring.ufnGetScreeningCriticality(@usedAssetIds, @securityUserID)

	update a set AssetCriticality = b.AssetCriticality, ModelCriticality = b.ModelCriticality from @modelInfo a JOIN @Criticality b on a.ModelID = b.ModelID OPTION (RECOMPILE)

	UPDATE m SET m.UnitAbbrev = parent.AssetAbbrev, m.UnitId = parent.AssetID
	FROM @modelInfo m JOIN Asset.tAsset parent on parent.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(m.AssetID,'UN')  OPTION (RECOMPILE)

	  declare @modelCurrentActionItemTypeStatus TABLE(
		ModelID int PRIMARY KEY,
		IsDiagnoseSet bit null,
		IsDiagnoseCleared bit null,
		IsWatchSet bit null,
		IsWatchCleared bit null,
		IsWatchExpired bit null,
		IsModelMaintenanceSet bit null,
		IsModelMaintenanceCleared bit null,
		IsIssueCreated bit null,
		IsIssueClosed bit null,
		IsNoteAdded bit null,
		IsIgnoreSet bit null,
		IsClearAlertStatus bit null,
		IsIgnoreExpiration bit null,
		IsWatchOverride bit NULL,
		IsStopIgnoring bit NULL
	)

	declare @usedModelIDs as base.tpintlist
	insert into @usedModelIDs select distinct modelid from @modelInfo

	insert into @modelCurrentActionItemTypeStatus SELECT * FROM Monitoring.ufnModelCurrentActionItemStatuses(@usedModelIDs) 
	OPTION (RECOMPILE)

	UPDATE a SET ActionDiagnose = b.IsDiagnoseSet, ActionWatch = b.IsWatchSet, ActionModelMaintenance = b.IsModelMaintenanceSet FROM @modelInfo a JOIN @modelCurrentActionItemTypeStatus b on a.ModelID = b.ModelID OPTION (RECOMPILE)
END

delete from @modelInfo
WHERE	(EXISTS(SELECT * FROM @assetCriticality) AND (AssetCriticality NOT IN (SELECT id from @assetCriticality))) OR
		(EXISTS(SELECT * FROM @modelCriticality) AND (ModelCriticality NOT IN (SELECT id from @modelCriticality))) OR
		(@isDiagnose IS NOT NULL AND @isDiagnose <> ISNULL(ActionDiagnose,0)) OR
		(@isWatch IS NOT NULL AND @isWatch <> ISNULL(ActionWatch,0)) OR
		(@isMaintenance IS NOT NULL AND @isMaintenance <> ISNULL(ActionModelMaintenance,0)) 

RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetFilteredModelInfoFromAssetIDs] TO [TEUser]
    AS [dbo];
GO

