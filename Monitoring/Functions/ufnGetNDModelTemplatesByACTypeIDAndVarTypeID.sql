﻿create function [Monitoring].[ufnGetNDModelTemplatesByACTypeIDAndVarTypeID]
(
	@AstVarTagMapIds Base.tpIntList readonly,
	@ModelTypeId int = null
)
returns table
as
return
(
	select
		mt.ModelTemplateID
		, mt.AssetClassTypeVariableTypeMapID
		, mt.ParentAssetClassTypeID
		, mt.ModelTypeID
		, mt.Temporal
		, mt.ConcerningBoundTypeID
		, mt.CreatedByUserID
		, mt.CreateDate
		, mt.ChangedByUserID
		, mt.ChangeDate
		, mt.UpperCriticality
		, mt.LowerCriticality
		, map.AssetVariableTypeTagMapID
	from ProcessData.tAssetVariableTypeTagMap map
	join @AstVarTagMapIds ids on map.AssetVariableTypeTagMapID = ids.id
	join Asset.tAsset a on map.AssetID = a.AssetID
	join Asset.tAsset p on a.ParentAssetID = p.AssetID
	join ProcessData.tAssetClassTypeVariableTypeMap acvar on a.AssetClassTypeID = acvar.AssetClassTypeID and map.VariableTypeID = acvar.VariableTypeID
	join Monitoring.tNDModelTemplate mt on acvar.AssetClassTypeVariableTypeMapID = mt.AssetClassTypeVariableTypeMapID and (case when mt.ParentAssetClassTypeID is null or mt.ParentAssetClassTypeID = p.AssetClassTypeID then 1 else 0 end = 1)
	where
		@ModelTypeId is null or mt.ModelTypeID = @ModelTypeId
);
GO
GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetNDModelTemplatesByACTypeIDAndVarTypeID] TO [TEUser]
    AS [dbo];

