﻿CREATE function [Monitoring].[ufnGetDefaultConfigCriticality]
(
	@ModelIDs Base.tpIntList readonly
)
returns @Criticality table
(
	ModelID int not null,
	UpperCriticality int null,
	LowerCriticality int null,
	primary key clustered
	(
		ModelID asc
	)
)
as
begin

	declare @customs Base.tpIntList;
	insert into @customs (id) select id from @ModelIDs;

	-- first get all of the templates
	declare @astVarTagMaps base.tpIntList;
	insert into @astVarTagMaps (id)
	select distinct m.AssetVariableTypeTagMapID
	from Monitoring.tNDModel m
	join @customs cstm on m.ModelID = cstm.id
	;
	
	declare @templates table
	(
		ModelTemplateID int not null
		, AssetClassTypeVariableTypeMapID int not null
		, ParentAssetClassTypeID int null
		, ModelTypeID int not null
		, Temporal bit not null
		, ConcerningBoundTypeID int not null
		, CreatedByUserID int not null
		, CreateDate datetime not null
		, ChangedByUserID int not null
		, ChangeDate datetime not null
		, UpperCriticality int null
		, LowerCriticality int null
		, AssetVariableTypeTagMapID int not null
	)
	;
	insert into @templates (ModelTemplateID, AssetClassTypeVariableTypeMapID, ParentAssetClassTypeID,
							ModelTypeID, Temporal, ConcerningBoundTypeID, CreatedByUserID, CreateDate,
							ChangedByUserID, ChangeDate, UpperCriticality, LowerCriticality, AssetVariableTypeTagMapID)
	select ModelTemplateID, AssetClassTypeVariableTypeMapID, ParentAssetClassTypeID,
							ModelTypeID, Temporal, ConcerningBoundTypeID, CreatedByUserID, CreateDate,
							ChangedByUserID, ChangeDate, UpperCriticality, LowerCriticality, AssetVariableTypeTagMapID
	from Monitoring.ufnGetNDModelTemplatesByACTypeIDAndVarTypeID (@astVarTagMaps, default) order by ModelTemplateID
	;

	-- these customs want to use template defaults but alas, none exist to help them
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select id, null, null from @customs
	except
	select distinct m.ModelID, null, null
	from @templates t join Monitoring.tNDModel m on t.AssetVariableTypeTagMapID = m.AssetVariableTypeTagMapID join @customs cstm on m.ModelID = cstm.id
	;

	if (select count (ModelID) from @Criticality) = (select count (id) from @ModelIDs)
	begin
		return;
	end

	-- now shrink customs again to leave those with templates
	delete from @customs where id in (select ModelID from @Criticality);


	-- we need to prioritize templates which means cross-referencing op modes.
	-- we prioritize the steady state op mode.
	declare @steadyStateId int = 4;

	-- any models that have the prioritized op mode and templates that cover the op mode?
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select tt.id, t.UpperCriticality, t.LowerCriticality
	from @templates t
	join (
		select cs.id, mt.AssetVariableTypeTagMapID, min (mt.ModelTemplateID) as ModelTemplateID
		from @customs cs
		join Monitoring.tNDModel m on cs.id = m.ModelID
		join @templates mt on m.AssetVariableTypeTagMapID = mt.AssetVariableTypeTagMapID
		join Monitoring.tNDModelOpModeTypeMap mopmap on cs.id = mopmap.ModelID
		join Monitoring.tNDModelTemplateOpModeMap topmap on mt.ModelTemplateID = topmap.ModelTemplateID
		where mopmap.OpModeTypeID = @steadyStateId and topmap.OpModeTypeID = @steadyStateId
		group by cs.id, mt.AssetVariableTypeTagMapID
	) tt on t.ModelTemplateID = tt.ModelTemplateID and t.AssetVariableTypeTagMapID = tt.AssetVariableTypeTagMapID
	;

	if (select count (ModelID) from @Criticality) = (select count (id) from @ModelIDs)
	begin
		return;
	end

	-- shrink our custom again
	delete from @customs where id in (select ModelID from @Criticality);

	-- these models don't have steady state op modes which means they have 1-3 non-steady state op modes.
	-- so we need to get their op modes and go from there.
	declare @modelsOpModes table(
		ModelID int not null
		, OpModeTypeID int not null
		, OpModePriority int not null -- this is simply their position in Monitoring.tNDModelOpModeTypeMap
	);

	insert into @modelsOpModes (ModelID, OpModeTypeID, OpModePriority)
	select cstm.id, topo.OpModeTypeID, topo.rn
	from @customs cstm join (
		select m.id, opmap.OpModeTypeID, ROW_NUMBER() over (partition by opmap.ModelID order by opmap.ModelOpModeTypeMapID) as rn
		from @customs m join Monitoring.tNDModelOpModeTypeMap opmap on m.id = opmap.ModelID
	) topo on cstm.id = topo.id
	;

	-- any templates covering these "prioritized" op modes?
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select tt.id, t.UpperCriticality, t.LowerCriticality
	from @templates t
	join (
		select cs.id, mt.AssetVariableTypeTagMapID, min (mt.ModelTemplateID) as ModelTemplateID
		from @customs cs
		join Monitoring.tNDModel m on cs.id = m.ModelID
		join @templates mt on m.AssetVariableTypeTagMapID = mt.AssetVariableTypeTagMapID
		join @modelsOpModes mop on cs.id = mop.ModelID
		join Monitoring.tNDModelTemplateOpModeMap topmap on mop.OpModeTypeID = topmap.OpModeTypeID and topmap.ModelTemplateID = mt.ModelTemplateID
		where mop.OpModePriority = 1
		group by cs.id, mt.AssetVariableTypeTagMapID
	) tt on t.ModelTemplateID = tt.ModelTemplateID and t.AssetVariableTypeTagMapID = tt.AssetVariableTypeTagMapID
	;

	if (select count (ModelID) from @Criticality) = (select count (id) from @ModelIDs)
	begin
		return;
	end

	-- shrink our custom again
	delete from @customs where id in (select ModelID from @Criticality);

	-- last chance for these buggers. are there any templates with op modes covering the not-top-priority model op modes?
	insert into @Criticality (ModelID, UpperCriticality, LowerCriticality)
	select tt.id, t.UpperCriticality, t.LowerCriticality
	from @templates t
	join (
		select cs.id, mt.AssetVariableTypeTagMapID, min (mt.ModelTemplateID) as ModelTemplateID
		from @customs cs
		join Monitoring.tNDModel m on cs.id = m.ModelID
		join @templates mt on m.AssetVariableTypeTagMapID = mt.AssetVariableTypeTagMapID
		join @modelsOpModes mop on cs.id = mop.ModelID
		join Monitoring.tNDModelTemplateOpModeMap topmap on mop.OpModeTypeID = topmap.OpModeTypeID and topmap.ModelTemplateID = mt.ModelTemplateID
		group by cs.id, mt.AssetVariableTypeTagMapID
	) tt on t.ModelTemplateID = tt.ModelTemplateID and t.AssetVariableTypeTagMapID = tt.AssetVariableTypeTagMapID
	;

	return;
end
GO
GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetDefaultConfigCriticality] TO [TEUser]
    AS [dbo];

