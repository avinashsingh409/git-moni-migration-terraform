﻿CREATE FUNCTION [Monitoring].[ufnGetFilteredModelActionItemArchiveIDs]
(
	@modelId int,
	@modelActionTypeID Base.tpIntList READONLY,
	@timestampStart datetime = NULL,
	@timestampEnd datetime = NULL,
	@executor NVARCHAR(100) = NULL,
	@noteText NVARCHAR(255) = NULL,
	@assetIds Base.tpIntList READONLY,
	@favorite bit = 0
)
RETURNS @results TABLE (
	ModelActionItemArchiveID int
)
AS
BEGIN
	IF @modelId IS NOT NULL
	  BEGIN
	  INSERT INTO @results
		SELECT
		DISTINCT mdlArchive.ModelActionItemArchiveID
		FROM Monitoring.tNDModel model 
		JOIN ProcessData.tAssetVariableTypeTagMap map ON map.AssetVariableTypeTagMapID = model.AssetVariableTypeTagMapID		
		JOIN Monitoring.tNDModelActionItemArchive mdlArchive ON model.ModelID = mdlArchive.ModelID
		JOIN AccessControl.tUser u ON mdlArchive.ChangedByUserID = u.SecurityUserID
		WHERE
			(mdlArchive.ModelID = @modelId) AND
			(NOT EXISTS (SELECT 1 FROM @modelActionTypeID) OR (mdlArchive.ModelActionItemTypeID IN (SELECT id FROM @modelActionTypeID))) AND
			((@timestampStart IS NULL OR @timestampEnd IS NULL) OR (mdlArchive.ChangeDate BETWEEN @timestampStart AND @timestampEnd)) AND
			(@executor IS NULL OR (u.Username like '%' + @executor + '%')) AND
			(@noteText IS NULL OR (mdlArchive.NoteText like '%' + @noteText + '%')) AND
			(@favorite IS NULL OR (mdlArchive.Favorite = @favorite)) OPTION (RECOMPILE)
	  END
	ELSE
	  BEGIN
	  INSERT INTO @results
		SELECT
		DISTINCT mdlArchive.ModelActionItemArchiveID
		FROM @assetIds br
		JOIN ProcessData.tAssetVariableTypeTagMap map ON br.id = map.AssetID
		JOIN Monitoring.tNDModel model ON map.AssetVariableTypeTagMapID = model.AssetVariableTypeTagMapID
		JOIN Monitoring.tNDModelActionItemArchive mdlArchive ON model.ModelID = mdlArchive.ModelID
		JOIN AccessControl.tUser u ON mdlArchive.ChangedByUserID = u.SecurityUserID
		WHERE
			(NOT EXISTS (SELECT 1 FROM @modelActionTypeID) OR (mdlArchive.ModelActionItemTypeID IN (SELECT id FROM @modelActionTypeID))) AND
			((@timestampStart IS NULL OR @timestampEnd IS NULL) OR (mdlArchive.ChangeDate BETWEEN @timestampStart AND @timestampEnd)) AND
			(@executor IS NULL OR (u.Username like '%' + @executor + '%')) AND
			(@noteText IS NULL OR (mdlArchive.NoteText like '%' + @noteText + '%')) AND
			(@favorite IS NULL OR (mdlArchive.Favorite = @favorite)) OPTION (RECOMPILE)
	  END
	RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetFilteredModelActionItemArchiveIDs] TO [TEUSER]
    AS [dbo];
GO