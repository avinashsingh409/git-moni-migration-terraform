﻿CREATE FUNCTION [Monitoring].[ufnGetRoleSecuritizedSpecificCategorizedModelsFromUser] 
(	
	-- Add the parameters for the function here
	@securityUserID INT,
	@filterCategoryIDs Base.tpIntList READONLY,
	@filterCategoryTypeID int = NULL
)
RETURNS TABLE AS RETURN
(
	SELECT
		DISTINCT(ndmodels.ModelID)
	FROM
		Monitoring.tNDModel ndmodels
	LEFT JOIN
		Monitoring.tNDModelCategoryMap modelCategoryMap ON ndmodels.ModelID = modelCategoryMap.ModelID	
	INNER JOIN
		AccessControl.ufnGetRoleSecuritizedFilteredCategoriesFromUser(@securityUserId, @filterCategoryIDs, 6) permittedCategories ON modelCategoryMap.CategoryID = permittedCategories.CategoryID
)
GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetRoleSecuritizedSpecificCategorizedModelsFromUser] TO [TEUser]
    AS [dbo]
GO
