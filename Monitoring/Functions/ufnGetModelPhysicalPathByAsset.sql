﻿CREATE FUNCTION [Monitoring].[ufnGetModelPhysicalPathByAsset]
(
	@modelAssetId INT
)
RETURNS @rootPath TABLE 
(
	ModelPath VARCHAR(400)
)
AS 
BEGIN
	DECLARE @modelPath nvarchar(1000);
	DECLARE @modelAssetIds Base.tpIntList
	INSERT INTO @modelAssetIds VALUES (@modelAssetId)
	SET @modelPath = (SELECT TOP 1 AssetPath FROM Asset.ufnGetAssetPathsByAssetAbbrev(@modelAssetIds, default));

	-- retrieve model dep var asset root ancestor to strip from path
	DECLARE @rootAbbrev nvarchar(51);
	IF @modelPath IS NOT NULL
	BEGIN
		SET @rootAbbrev =
		(
			SELECT a.AssetAbbrev FROM Asset.ufnAssetAncestors(@modelAssetId) c
			JOIN Asset.tAsset a ON c.AssetID = a.AssetID
			WHERE a.ParentAssetID IS NULL
		) + '/';
		IF @rootAbbrev IS NULL
		BEGIN
			SET @rootAbbrev = @modelPath;
		END
	END

	INSERT INTO @rootPath(ModelPath)
	SELECT RIGHT(@modelPath, LEN(@modelPath) - LEN(@rootAbbrev)) AS ModelPath

	RETURN 
END
GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetModelPhysicalPathByAsset] TO [TEUser]
    AS [dbo];
GO