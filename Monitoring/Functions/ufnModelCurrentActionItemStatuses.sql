﻿CREATE FUNCTION [Monitoring].[ufnModelCurrentActionItemStatuses] 
(	
   @modelIDs as Base.tpIntList READONLY
) 
RETURNS @modelCurrentActionItemTypeStatus TABLE(
	ModelID int PRIMARY KEY,
	IsDiagnoseSet bit null,
	IsDiagnoseCleared bit null,
	IsWatchSet bit null,
	IsWatchCleared bit null,
	IsWatchExpired bit null,
	IsModelMaintenanceSet bit null,
	IsModelMaintenanceCleared bit null,
	IsIssueCreated bit null,
	IsIssueClosed bit null,
	IsNoteAdded bit null,
	IsIgnoreSet bit null,
	IsClearAlertStatus bit null,
	IsIgnoreExpiration bit null,
	IsWatchOverride bit NULL,
	IsStopIgnoring bit NULL
) 
AS 
BEGIN

	IF EXISTS(SELECT 1 FROM @modelIDs)
	BEGIN
		INSERT INTO @modelCurrentActionItemTypeStatus
		SELECT ModelID,
			IsDiagnoseSet = CASE WHEN mai.LastDiagnoseAction IS NULL THEN NULL WHEN mai.LastDiagnoseAction = 1 THEN 1 ELSE 0 END,
			IsDiagnoseCleared = CASE WHEN mai.LastDiagnoseAction IS NULL THEN NULL WHEN mai.LastDiagnoseAction = 2 THEN 1 ELSE 0 END,
			IsWatchSet = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction IN (3,16) THEN 1 ELSE 0 END,
			IsWatchCleared = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction = 4 THEN 1 ELSE 0 END,
			IsWatchExpired = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction = 5 THEN 1 ELSE 0 END,
			IsModelMaintenanceSet = CASE WHEN mai.LastMaintenanceAction IS NULL THEN NULL WHEN mai.LastMaintenanceAction = 6 THEN 1 ELSE 0 END,
			IsModelMaintenanceCleared = CASE WHEN mai.LastMaintenanceAction IS NULL THEN NULL WHEN mai.LastMaintenanceAction = 7 THEN 1 ELSE 0 END		,
			IsIssueCreated = CASE WHEN mai.LastIssueAction IS NULL THEN NULL WHEN mai.LastIssueAction = 8 THEN 1 ELSE 0 END,
			IsIssueClosed = CASE WHEN mai.LastIssueAction IS NULL THEN NULL WHEN mai.LastIssueAction = 9 THEN 1 ELSE 0 END,
			IsNoteAdded = CASE WHEN mai.LastNoteAction IS NULL THEN NULL WHEN mai.LastNoteAction = 10 THEN 1 ELSE 0 END,
			IsIgnoreSet = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 11 THEN 1 ELSE 0 END,
			IsClearAlertStatus = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 12 THEN 1 ELSE 0 END,
			IsIgnoreExpiration = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 13 THEN 1 ELSE 0 END,
			IsWatchOverride = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction = 14 THEN 1 ELSE 0 END,
			IsStopIgnoring = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 15 THEN 1 ELSE 0 END
		FROM Monitoring.tNDModelActionItem mai
		INNER JOIN @modelIDs m ON mai.ModelID = m.id
	END
	ELSE
	BEGIN
		INSERT INTO @modelCurrentActionItemTypeStatus
		SELECT ModelID,
			IsDiagnoseSet = CASE WHEN mai.LastDiagnoseAction IS NULL THEN NULL WHEN mai.LastDiagnoseAction = 1 THEN 1 ELSE 0 END,
			IsDiagnoseCleared = CASE WHEN mai.LastDiagnoseAction IS NULL THEN NULL WHEN mai.LastDiagnoseAction = 2 THEN 1 ELSE 0 END,
			IsWatchSet = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction IN (3,16) THEN 1 ELSE 0 END,
			IsWatchCleared = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction = 4 THEN 1 ELSE 0 END,
			IsWatchExpired = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction = 5 THEN 1 ELSE 0 END,
			IsModelMaintenanceSet = CASE WHEN mai.LastMaintenanceAction IS NULL THEN NULL WHEN mai.LastMaintenanceAction = 6 THEN 1 ELSE 0 END,
			IsModelMaintenanceCleared = CASE WHEN mai.LastMaintenanceAction IS NULL THEN NULL WHEN mai.LastMaintenanceAction = 7 THEN 1 ELSE 0 END		,
			IsIssueCreated = CASE WHEN mai.LastIssueAction IS NULL THEN NULL WHEN mai.LastIssueAction = 8 THEN 1 ELSE 0 END,
			IsIssueClosed = CASE WHEN mai.LastIssueAction IS NULL THEN NULL WHEN mai.LastIssueAction = 9 THEN 1 ELSE 0 END,
			IsNoteAdded = CASE WHEN mai.LastNoteAction IS NULL THEN NULL WHEN mai.LastNoteAction = 10 THEN 1 ELSE 0 END,
			IsIgnoreSet = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 11 THEN 1 ELSE 0 END,
			IsClearAlertStatus = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 12 THEN 1 ELSE 0 END,
			IsIgnoreExpiration = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 13 THEN 1 ELSE 0 END,
			IsWatchOverride = CASE WHEN mai.LastWatchAction IS NULL THEN NULL WHEN mai.LastWatchAction = 14 THEN 1 ELSE 0 END,
			IsStopIgnoring = CASE WHEN mai.LastAlertAction IS NULL THEN NULL WHEN mai.LastAlertAction = 15 THEN 1 ELSE 0 END
		FROM Monitoring.tNDModelActionItem mai
	END
					
	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnModelCurrentActionItemStatuses] TO [TEUser]
    AS [dbo];
GO
