CREATE FUNCTION [Monitoring].[ufnGetFilteredModelIDsFromAssetIDs] 
(	
	@assetIDs Base.tpIntList READONLY,
	@isAlert bit = NULL,
	@addedToAlertsStart datetime = NULL,
	@addedToAlertsEnd datetime = NULL,
	@active bit = NULL,
	@built bit = NULL,
	@hasCalcPoint bit = NULL,
	@tagName NVARCHAR(400) = NULL,
	@tagDesc NVARCHAR(400) = NULL,
	@modelName NVARCHAR(255) = NULL,
	@modelTypeID Base.tpIntList READONLY,
	@engUnits NVARCHAR(255) = NULL,
	@modelOpModeID Base.tpIntList READONLY,
	@isDiagnose bit = NULL,
	@isWatch bit = NULL,
	@isWatchOverride bit = NULL,
	@isMaintenance bit = NULL,
	@isIgnore bit = NULL,
	@isOpenIssue bit = NULL,
	@evaluationErrorMessage NVARCHAR(400) = NULL,
	@priority int = NULL,
	@outOfBounds bit = NULL,
	@alertPriority int = NULL,
	@alertType NVARCHAR(255) = NULL,
	@currentBuildStatusTypeID Base.tpIntList READONLY,
	@securityUserId int = -1,
	@assetCriticality Base.tpIntList READONLY,
	@modelCriticality Base.tpIntList READONLY,
	@modelID int = NULL,
	@filterCategoryIDs Base.tpIntList READONLY
)
RETURNS TABLE AS RETURN
(
	SELECT
	DISTINCT model.ModelID
	FROM @assetIDs br
	INNER JOIN ProcessData.tAssetVariableTypeTagMap map
		ON br.id = map.AssetID
	INNER JOIN ProcessData.tTag tag
		ON map.TagID = tag.PDTagID
	INNER JOIN Monitoring.tNDModel model
		ON map.AssetVariableTypeTagMapID = model.AssetVariableTypeTagMapID
	INNER JOIN Monitoring.tNDModelType modeltype
		ON model.ModelTypeID = modeltype.ModelTypeID
	INNER JOIN Monitoring.tNDModelOpModeTypeMap opmodemap
		ON model.ModelID = opmodemap.ModelID
	INNER JOIN Monitoring.tNDOpModeParams params
		ON model.OpModeParamsID = params.OpModeParamsID
	INNER JOIN Monitoring.ufnGetScreeningCriticality(@assetIDs, @securityUserId) criticality
		ON model.ModelID = criticality.ModelID
	LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap
		ON (model.ModelID = predmap.ModelID) AND (predmap.Active = 1)
	LEFT JOIN Monitoring.tNDModelActionItem tmai
		ON tmai.ModelID = model.ModelID
	LEFT JOIN Monitoring.ufnMOdelCurrentActionItemStatus (@modelID) tmaicur
		ON tmaicur.ModelID = model.ModelID
	LEFT JOIN Monitoring.tNDModel_AlertScreening tmas
		ON tmas.ModelID = model.ModelID and tmas.Until > GETDATE()
	LEFT JOIN Diagnostics.tAssetIssue issues on issues.AssetID = map.AssetID AND ActivityStatusID = 1 and IsHidden = 0
	LEFT JOIN Monitoring.tNDModelEvaluation mdleval
		ON model.ModelID = mdleval.ModelID
	INNER JOIN Monitoring.ufnGetRoleSecuritizedModelsFromUser(@securityUserId, @filterCategoryIDs, 6) AS filteredModels ON model.ModelID = filteredModels.ModelID
	WHERE
	(@isAlert IS NULL OR (mdleval.Alert = @isAlert)) AND
	(@addedToAlertsStart IS NULL OR (mdleval.AddedToAlerts >= @addedToAlertsStart)) AND
	(@addedToAlertsEnd IS NULL OR (mdleval.AddedToAlerts <= @addedToAlertsEnd)) AND
	(@active IS NULL OR (model.Active = @active)) AND
	(@built IS NULL OR (model.Built = @built)) AND
	(@hasCalcPoint IS NULL OR (@hasCalcPoint = 1 AND model.CalcPointID IS NOT NULL) 
				OR (@hasCalcPoint = 0 AND model.CalcPointID IS NULL)) AND
	(@tagName IS NULL OR (tag.TagName LIKE '%' + @tagName + '%')) AND
	(@tagDesc IS NULL OR (tag.TagDesc LIKE '%' + @tagDesc + '%')) AND
	(@modelName IS NULL OR (model.ModelName LIKE '%' + @modelName + '%')) AND
	(@engUnits IS NULL OR (tag.EngUnits LIKE '%' + @engUnits + '%')) AND
	(@isDiagnose IS NULL OR ([tmaicur].[IsDiagnoseSet]=@isDiagnose 
				OR (@isDiagnose = 0 AND ([tmaicur].[IsDiagnoseSet] IS NULL OR [tmaicur].[IsDiagnoseSet] = 0 ))
			) 
		) AND
	(@isWatch IS NULL OR ([tmaicur].IsWatchSet=@isWatch) OR (@isWatch = 0 AND ([tmaicur].IsWatchSet IS NULL OR [tmaicur].IsWatchSet = 0 ))) AND
	(@isMaintenance IS NULL OR ([tmaicur].IsModelMaintenanceSet=@isMaintenance 
				OR (@isMaintenance = 0 AND ([tmaicur].IsModelMaintenanceSet IS NULL OR [tmaicur].IsModelMaintenanceSet = 0 ))
			) 
		) AND
	(@isIgnore IS NULL OR (tmas.IgnoreStatus=@isIgnore 
				OR (@isIgnore = 0 AND ((tmas.IgnoreStatus IS NULL OR tmas.IgnoreStatus = 0) OR (mdleval.IgnoreOverrideAlert = 1) ))
			) 
		) AND	
	(@priority IS NULL OR (tmai.ModelActionItemPriorityTypeID=@priority 
				OR (@priority = 0 AND (tmai.ModelActionItemPriorityTypeID IS NULL OR tmai.ModelActionItemPriorityTypeID = 0 ))
			) 
		) AND	
	(@isWatchOverride IS NULL OR (mdleval.ActionWatchOverride = @isWatchOverride)) AND
	((@isOpenIssue IS NULL) OR (@isOpenIssue = 1 AND issues.GlobalID IS NOT NULL) OR (@isOpenIssue = 0 AND issues.GlobalID IS NULL)) AND
	(@evaluationErrorMessage IS NULL OR (mdleval.EvaluationErrorMessage = @evaluationErrorMessage)) AND
	(@outOfBounds IS NULL OR (CASE WHEN mdleval.PercentOOB <> 0 THEN 1 ELSE 0 END = @outOfBounds)) AND
	(@alertPriority IS NULL or (mdleval.AlertStatusPriorityTypeID = @alertPriority)) AND
	(@alertType IS NULL OR (CASE @alertType WHEN 'HiHi' THEN CASE WHEN mdleval.HiHiAlert = 1 THEN 1 ELSE 0 END
								WHEN 'LowLow' THEN CASE WHEN mdleval.LowLowAlert = 1 THEN 1 ELSE 0 END
								WHEN 'AreaFastResponse' THEN CASE WHEN mdleval.AreaFastResponseAlert = 1 THEN 1 ELSE 0 END
								WHEN 'AreaSlowResponse' THEN CASE WHEN mdleval.AreaSlowResponseAlert = 1 THEN 1 ELSE 0 END
								WHEN 'Frequency' THEN CASE WHEN mdleval.FrequencyAlert = 1 THEN 1 ELSE 0 END
								WHEN 'Oscillation' THEN CASE WHEN mdleval.OscillationAlert = 1 THEN 1 ELSE 0 END
								WHEN 'WatchOverride' THEN CASE WHEN mdleval.WatchOverrideAlert = 1 THEN 1 ELSE 0 END
								WHEN 'FrozenData' THEN CASE WHEN mdleval.FrozenDataAlert = 1 THEN 1 ELSE 0 END
								WHEN 'IgnoreOverride' THEN CASE WHEN mdleval.IgnoreOverrideAlert = 1 THEN 1 ELSE 0 END
							END = 1))
	AND
	(NOT EXISTS (SELECT 1 FROM @modelTypeID) OR (model.ModelTypeID IN (SELECT id FROM @modelTypeID))) AND
	(NOT EXISTS (SELECT 1 FROM @modelOpModeID) OR (opmodemap.OpModeTypeID IN (SELECT id FROM @modelOpModeID))) AND
	(NOT EXISTS (SELECT 1 FROM @assetCriticality) OR (criticality.AssetCriticality IN (SELECT id from @assetCriticality))) AND
	(NOT EXISTS (SELECT 1 FROM @modelCriticality) OR (criticality.ModelCriticality IN (SELECT id from @modelCriticality))) AND
	(NOT EXISTS (SELECT 1 FROM @currentBuildStatusTypeID) OR (EXISTS (SELECT 1 FROM Monitoring.ufnCurrentModelBuildStatusForModelExtID(model.ModelExtID) c INNER JOIN @currentBuildStatusTypeID b ON c.BuildStatusTypeID = b.id))
	)
)
GO

GRANT SELECT
    ON OBJECT::[Monitoring].[ufnGetFilteredModelIDsFromAssetIDs] TO [TEUser]
    AS [dbo];
GO