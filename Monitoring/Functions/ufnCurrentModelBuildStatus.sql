﻿


CREATE FUNCTION [Monitoring].[ufnCurrentModelBuildStatus]
(
)
RETURNS @currentModelBuild TABLE (
	ModelBuildID int,
	ModelExtID nvarchar(255),
	BuildStatusTypeID int,
	BuildStatusTypeAbbrev nvarchar(50),
	BuildStatus nvarchar(max),
	BuildStatusUsername nvarchar(50),
	BuildStatusTime datetime
)
AS
BEGIN

	WITH cteLatest AS
	(
		SELECT *,
			ROW_NUMBER() OVER (PARTITION BY ModelExtID ORDER BY BuildStatusTime DESC) AS rn
		FROM Monitoring.tNDModelBuild
	)

	INSERT INTO @currentModelBuild
	(
		ModelBuildID
		, ModelExtID
		, BuildStatusTypeID
		, BuildStatusTypeAbbrev
		, BuildStatus
		, BuildStatusUsername
		, BuildStatusTime
	)
	SELECT
		bui.ModelBuildID
		, bui.ModelExtID
		, bui.BuildStatusTypeID
		, btype.ModelBuildStatusTypeAbbrev
		, bui.BuildStatus
		, usr.Username
		, bui.BuildStatusTime
	FROM cteLatest bui
	JOIN Monitoring.tNDModelBuildStatusType btype ON bui.BuildStatusTypeID = btype.ModelBuildStatusTypeID
	JOIN AccessControl.tUser usr ON bui.BuildStatusUserID = usr.SecurityUserID
	WHERE bui.rn = 1
	
	RETURN

END