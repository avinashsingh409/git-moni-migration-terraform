﻿CREATE TABLE [GeoSpa].[tAttributesSql] (
    [AttributesSqlID] INT              IDENTITY (1, 1) NOT NULL,
    [ContentID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tAttributesSql_ContentID] DEFAULT (newid()) NOT NULL,
    [TableName]       NVARCHAR (255)   NULL,
    [GeoSpaFK]        NVARCHAR (255)   NULL,
    [TablePK]         NVARCHAR (255)   NULL,
    [IncludeFields]   NVARCHAR (255)   NULL,
    [ExcludeFields]   NVARCHAR (255)   NULL,
    CONSTRAINT [PK_tAttributesSql] PRIMARY KEY CLUSTERED ([AttributesSqlID] ASC)
);

