﻿CREATE TABLE [GeoSpa].[tAssetDecCell] (
    [AssetDecCellID] INT IDENTITY (1, 1) NOT NULL,
    [AssetID]        INT NOT NULL,
    [GeometryTypeID] INT NOT NULL,
    [TileLevel]      INT NOT NULL,
    [TileX]          INT NOT NULL,
    [TileY]          INT NOT NULL,
    CONSTRAINT [PK_tAssetDecCell] PRIMARY KEY CLUSTERED ([AssetDecCellID] ASC),
    CONSTRAINT [FK_tAssetDecCell_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetDecCell_tGeometryType] FOREIGN KEY ([GeometryTypeID]) REFERENCES [GeoSpa].[tGeometryType] ([GeometryTypeID]),
    CONSTRAINT [UK_tAssetDecCell_AssetID_GeometryTypeID_TileLevel] UNIQUE NONCLUSTERED ([AssetID] ASC, [GeometryTypeID] ASC, [TileLevel] ASC)
);

