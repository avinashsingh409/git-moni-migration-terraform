﻿CREATE TABLE [GeoSpa].[tGeoSpaRestLayerType] (
    [GeoSpaRestLayerTypeID] INT            NOT NULL,
    [GeoSpaRestLayerKey]    NVARCHAR (30)  NOT NULL,
    [GeoSpaRestLayerDesc]   NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_tGeoSpaRestLayerType] PRIMARY KEY CLUSTERED ([GeoSpaRestLayerTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'TRUE', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tGeoSpaRestLayerType';

