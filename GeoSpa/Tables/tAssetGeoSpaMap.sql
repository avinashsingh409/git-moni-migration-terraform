﻿CREATE TABLE [GeoSpa].[tAssetGeoSpaMap] (
    [AssetGeoSpaMapID] INT IDENTITY (1, 1) NOT NULL,
    [GeoSpaID]         INT NOT NULL,
    [AssetClassTypeID] INT NOT NULL,
    CONSTRAINT [PK_tAssetGeoSpaMap] PRIMARY KEY CLUSTERED ([AssetGeoSpaMapID] ASC),
    CONSTRAINT [FK_GeoSpa_tAssetGeoSpaMap_AssetClassTypeID_Asset_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tAssetGeoSpaMap_tGeoSpa] FOREIGN KEY ([GeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]) ON DELETE CASCADE
);







