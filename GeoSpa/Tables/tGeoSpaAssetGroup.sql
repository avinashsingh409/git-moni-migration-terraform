﻿CREATE TABLE [GeoSpa].[tGeoSpaAssetGroup] (
    [GeoSpaAssetGroupID]  INT            IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeID]    INT            NOT NULL,
    [GeometryTypeID]      INT            NOT NULL,
    [DisplayOrder]        INT            NULL,
    [GeoSpaSourceID]      INT            NOT NULL,
    [Icon]                NVARCHAR (50)  NULL,
    [SelectedIcon]        NVARCHAR (50)  NULL,
    [SymbologyProperties] NVARCHAR (MAX) NULL,
    [AttributeSourceID]   INT            NOT NULL,
    CONSTRAINT [PK_tGeoSpaAssetGroup] PRIMARY KEY CLUSTERED ([GeoSpaAssetGroupID] ASC),
    CONSTRAINT [FK_tGeoSpaAssetGroup_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tGeoSpaAssetGroup_tAttributeSource] FOREIGN KEY ([AttributeSourceID]) REFERENCES [GeoSpa].[tAttributeSource] ([AttributeSourceID]),
    CONSTRAINT [FK_tGeoSpaAssetGroup_tGeometryType] FOREIGN KEY ([GeometryTypeID]) REFERENCES [GeoSpa].[tGeometryType] ([GeometryTypeID]),
    CONSTRAINT [FK_tGeoSpaAssetGroup_tGeoSpaSource] FOREIGN KEY ([GeoSpaSourceID]) REFERENCES [GeoSpa].[tGeoSpaSource] ([GeoSpaSourceID]),
    CONSTRAINT [UK_tGeoSpaAssetGroup_AssetClassType_GeometryTypeID_GeoSpaSourceID] UNIQUE NONCLUSTERED ([AssetClassTypeID] ASC, [GeometryTypeID] ASC, [GeoSpaSourceID] ASC)
);

