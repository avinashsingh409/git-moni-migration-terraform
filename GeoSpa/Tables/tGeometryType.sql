﻿CREATE TABLE [GeoSpa].[tGeometryType] (
    [GeometryTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [GeometryTypeKey]  NVARCHAR (30)  NOT NULL,
    [GeometryTypeDesc] NVARCHAR (150) NOT NULL,
    CONSTRAINT [PK_tGeometryType] PRIMARY KEY CLUSTERED ([GeometryTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tGeometryType';

