﻿CREATE TABLE [GeoSpa].[tGeoSpaSourceType] (
    [GeoSpaSourceTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [GeoSpaSourceTypeKey]  NVARCHAR (30)  NOT NULL,
    [GeoSpaSourceTypeDesc] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_tGeoSpaSourceType] PRIMARY KEY CLUSTERED ([GeoSpaSourceTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tGeoSpaSourceType';

