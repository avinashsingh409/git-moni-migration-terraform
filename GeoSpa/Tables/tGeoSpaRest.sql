﻿CREATE TABLE [GeoSpa].[tGeoSpaRest] (
    [GeoSpaRestID]          INT              IDENTITY (1, 1) NOT NULL,
    [ContentID]             UNIQUEIDENTIFIER CONSTRAINT [DF_tGeoSpaRest_ContentID] DEFAULT (newid()) NOT NULL,
    [RestUrl]               NVARCHAR (MAX)   NOT NULL,
    [GeoSpaRestLayerTypeID] INT              NOT NULL,
    [Credentials]           NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_tGeoSpaRest] PRIMARY KEY CLUSTERED ([GeoSpaRestID] ASC),
    CONSTRAINT [FK_tGeoSpaRest_tGeoSpaRestLayerType_TypeID] FOREIGN KEY ([GeoSpaRestLayerTypeID]) REFERENCES [GeoSpa].[tGeoSpaRestLayerType] ([GeoSpaRestLayerTypeID])
);

