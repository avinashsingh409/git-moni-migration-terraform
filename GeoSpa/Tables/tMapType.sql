﻿CREATE TABLE [GeoSpa].[tMapType] (
    [MapTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [MapTypeKey]  NVARCHAR (50)  NOT NULL,
    [MapTypeDesc] NVARCHAR (250) NOT NULL,
    [Display]     INT            NULL,
    CONSTRAINT [PK_tMapType] PRIMARY KEY CLUSTERED ([MapTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tMapType';

