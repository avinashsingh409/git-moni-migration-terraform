﻿CREATE TABLE [GeoSpa].[tBaseMapType] (
    [BaseMapID]   INT            IDENTITY (1, 1) NOT NULL,
    [BaseMapKey]  NVARCHAR (30)  NOT NULL,
    [BaseMapDesc] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_tBaseMapType] PRIMARY KEY CLUSTERED ([BaseMapID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tBaseMapType';

