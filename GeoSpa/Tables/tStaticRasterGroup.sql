﻿CREATE TABLE [GeoSpa].[tStaticRasterGroup] (
    [StaticRasterGroupID] INT IDENTITY (1, 1) NOT NULL,
    [StaticRasterLayerID] INT NOT NULL,
    [DisplayOrder]        INT NOT NULL,
    CONSTRAINT [PK_tStaticRasterGroup] PRIMARY KEY CLUSTERED ([StaticRasterGroupID] ASC),
    CONSTRAINT [FK_tStaticRasterGroup_tStaticRasterLayer_StaticRasterLayerID] FOREIGN KEY ([StaticRasterLayerID]) REFERENCES [GeoSpa].[tStaticRasterLayer] ([StaticRasterLayerID]) ON DELETE CASCADE
);

