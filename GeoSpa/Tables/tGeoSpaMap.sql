﻿CREATE TABLE [GeoSpa].[tGeoSpaMap](
	[GeoSpaMapID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[AssetID] int NOT NULL,
	[CreatedByUserID] int NOT NULL,
	[ChangedByUserID] int NOT NULL,
	[CreateDate] DateTime NOT NULL,
	[ChangeDate] DateTime NOT NULL,
	[json] NVARCHAR(MAX) NOT NULL
	
 CONSTRAINT [PK_tGeoSpaMap] PRIMARY KEY CLUSTERED 
	(
	[GeoSpaMapID] ASC
	)
	CONSTRAINT [FK_tGeoSpaMap_ChangedByUserID_tUser] FOREIGN KEY([ChangedByUserID])
	REFERENCES [AccessControl].[tUser] ([SecurityUserID])

	CONSTRAINT [FK_tGeoSpaMap_CreatedByUserID_tUser] FOREIGN KEY([CreatedByUserID])
	REFERENCES [AccessControl].[tUser] ([SecurityUserID])
)

GO