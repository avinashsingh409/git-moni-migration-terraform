﻿CREATE TABLE [GeoSpa].[tStaticRasterWidget] (
    [StaticRasterWidgetID]      INT  IDENTITY (1, 1) NOT NULL,
    [StaticRasterGroupID]       INT  NOT NULL,
    [WidgetTypeID]              INT  NOT NULL,
    [RelativeXPosition]         REAL NOT NULL,
    [RelativeYPosition]         REAL NOT NULL,
    [AssetID]                   INT  NULL,
    [AssetVariableTypeTagMapID] INT  NULL,
    CONSTRAINT [PK_tStaticRasterWidget] PRIMARY KEY CLUSTERED ([StaticRasterWidgetID] ASC),
    CONSTRAINT [FK_tStaticRasterWidget_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tStaticRasterWidget_tAssetVariableTypeTagMap_AssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])  ON DELETE CASCADE,
    CONSTRAINT [FK_tStaticRasterWidget_tStaticRasterGroup_StaticRasterGroupID] FOREIGN KEY ([StaticRasterGroupID]) REFERENCES [GeoSpa].[tStaticRasterGroup] ([StaticRasterGroupID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tStaticRasterWidget_tWidgetType_WidgetTypeID] FOREIGN KEY ([WidgetTypeID]) REFERENCES [GeoSpa].[tWidgetType] ([WidgetTypeID]) ON DELETE CASCADE
);

