﻿CREATE TABLE [GeoSpa].[tAncillaryLayerSourceMap] (
    [AncillaryLayerSourceMapID] INT IDENTITY (1, 1) NOT NULL,
    [GeoSpaAncillaryLayerID]    INT NOT NULL,
    [GeoSpaSourceID]            INT NULL,
    [AttributeSourceID]         INT NULL,
    CONSTRAINT [PK_tAncillaryLayerSourceMap] PRIMARY KEY CLUSTERED ([AncillaryLayerSourceMapID] ASC),
    CONSTRAINT [FK_tAncillaryLayerSourceMap_tAttributeSource_AttributeSourceID] FOREIGN KEY ([AttributeSourceID]) REFERENCES [GeoSpa].[tAttributeSource] ([AttributeSourceID]),
    CONSTRAINT [FK_tAncillaryLayerSourceMap_tGeoSpaAncillaryLayer_LayerID] FOREIGN KEY ([GeoSpaAncillaryLayerID]) REFERENCES [GeoSpa].[tGeoSpaAncillaryLayer] ([GeoSpaAncillaryLayerID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAncillaryLayerSourceMap_tGeoSpaSource_GeoSpaSourceID] FOREIGN KEY ([GeoSpaSourceID]) REFERENCES [GeoSpa].[tGeoSpaSource] ([GeoSpaSourceID])
);

