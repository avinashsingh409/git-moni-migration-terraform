﻿CREATE TABLE [GeoSpa].[tGeoSpaCategoryMap]
(
    [CategoryMapID] int IDENTITY (1, 1) NOT NULL,
    [GeoSpaID]      int NOT NULL,
    [CategoryID]    int NOT NULL REFERENCES [Asset].[tCategory]([CategoryID]),
    CONSTRAINT [PK_tGeoSpaCategoryMap] PRIMARY KEY CLUSTERED ([CategoryMapID] ASC),
    CONSTRAINT [FK_tGeoSpaCategoryMap_tGeoSpa] FOREIGN KEY ([GeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]),
    CONSTRAINT [UK_tGeoSpaCategoryMap] UNIQUE NONCLUSTERED ([CategoryID] ASC, [GeoSpaID] ASC)
);
