﻿CREATE TABLE [GeoSpa].[tGeoSpaSql] (
    [GeoSpaSqlID]   INT              IDENTITY (1, 1) NOT NULL,
    [ContentID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tGeoSpaSql_ContentID] DEFAULT (newid()) NOT NULL,
    [TableName]     NVARCHAR (255)   NULL,
    [GeoSpaFK]      NVARCHAR (255)   NULL,
    [TablePK]       NVARCHAR (255)   NULL,
    [IncludeFields] NVARCHAR (255)   NULL,
    [ExcludeFields] NVARCHAR (255)   NULL,
    CONSTRAINT [PK_tGeoSpaSql] PRIMARY KEY CLUSTERED ([GeoSpaSqlID] ASC)
);

