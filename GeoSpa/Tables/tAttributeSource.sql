﻿CREATE TABLE [GeoSpa].[tAttributeSource] (
    [AttributeSourceID]     INT              IDENTITY (1, 1) NOT NULL,
    [AttributeSourceTypeID] INT              NOT NULL,
    [AttributeSourceGUID]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_tAttributeSource] PRIMARY KEY CLUSTERED ([AttributeSourceID] ASC),
    CONSTRAINT [FK_tAttributeSource_tAttributeSourceType] FOREIGN KEY ([AttributeSourceTypeID]) REFERENCES [GeoSpa].[tAttributeSourceType] ([AttributeSourceTypeID])
);

