﻿CREATE TABLE [GeoSpa].[tGeoSpaAncillaryLayer] (
    [GeoSpaAncillaryLayerID] INT            IDENTITY (1, 1) NOT NULL,
    [GeoSpaID]               INT            NOT NULL,
    [LayerIDExtension]       NVARCHAR (50)  NOT NULL,
    [Title]                  NVARCHAR (255) NULL,
    [AncillaryDescription]   NVARCHAR (MAX) NULL,
    [IsPublic]               BIT            NOT NULL,
    [DisplayOrder]           INT            NULL,
    [CreatedByUserID]        INT            NOT NULL,
    [ChangedByUserID]        INT            NOT NULL,
    [CreateDate]             DATETIME       CONSTRAINT [DF_tGeoSpaAncillaryLayer_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]             DATETIME       CONSTRAINT [DF_tGeoSpaAncillaryLayer_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [DisplayZoomMin]         INT		    NULL,
    [DisplayZoomMax]         INT		    NULL,
    CONSTRAINT [PK_tGeoSpaAncillaryLayer] PRIMARY KEY CLUSTERED ([GeoSpaAncillaryLayerID] ASC),
    CONSTRAINT [FK_tGeoSpaAncillaryLayer_tGeoSpa_GeoSpaID] FOREIGN KEY ([GeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGeoSpaAncillaryLayer_tUser_ChangedByUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tGeoSpaAncillaryLayer_tUser_CreatedByUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [Chk_tGeoSpaAncillaryLayer_DisplayZoomMin] CHECK (DisplayZoomMin >= 0 AND DisplayZoomMin <= 23),
	CONSTRAINT [Chk_tGeoSpaAncillaryLayer_DisplayZoomMax] CHECK (DisplayZoomMax >= 0 AND DisplayZoomMax <= 23)
);

