﻿CREATE TABLE [GeoSpa].[tWidgetType] (
    [WidgetTypeID]   INT           NOT NULL,
    [WidgetAbbrev]   VARCHAR (50)  NOT NULL,
    [WidgetDesc]     VARCHAR (255) NOT NULL,
    [WidgetBehavior] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tWidgetType] PRIMARY KEY CLUSTERED ([WidgetTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tWidgetType';

