﻿CREATE TABLE [GeoSpa].[tGeoSpaSource] (
    [GeoSpaSourceID]     INT              IDENTITY (1, 1) NOT NULL,
    [GeoSpaSourceTypeID] INT              NOT NULL,
    [GeoSpaSourceGUID]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tGeoSpaSource] PRIMARY KEY CLUSTERED ([GeoSpaSourceID] ASC),
    CONSTRAINT [FK_tGeoSpaSource_tGeoSpaSource] FOREIGN KEY ([GeoSpaSourceID]) REFERENCES [GeoSpa].[tGeoSpaSource] ([GeoSpaSourceID]),
    CONSTRAINT [FK_tGeoSpaSource_tGeoSpaSourceType] FOREIGN KEY ([GeoSpaSourceTypeID]) REFERENCES [GeoSpa].[tGeoSpaSourceType] ([GeoSpaSourceTypeID])
);

