﻿CREATE TABLE [GeoSpa].[tJson] (
    [JsonID]    INT              IDENTITY (1, 1) NOT NULL,
    [ContentID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tGeoJson] PRIMARY KEY CLUSTERED ([JsonID] ASC)
);

