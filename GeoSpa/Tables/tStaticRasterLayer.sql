﻿CREATE TABLE [GeoSpa].[tStaticRasterLayer] (
    [StaticRasterLayerID] INT              IDENTITY (1, 1) NOT NULL,
    [GeoSpaID]            INT              NOT NULL,
    [RasterContentID]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tStaticRasterLayer] PRIMARY KEY CLUSTERED ([StaticRasterLayerID] ASC),
    CONSTRAINT [FK_tStaticRasterLayer_tGeoSpa_GeoSpaID] FOREIGN KEY ([GeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]) ON DELETE CASCADE
);

