﻿CREATE TABLE [GeoSpa].[tGeoSpa] (
    [GeoSpaID]          INT              IDENTITY (1, 1) NOT NULL,
    [Title]             NVARCHAR (255)   NOT NULL,
    [GeoSpaDesc]        NVARCHAR (255)   NULL,
    [MapTypeID]         INT              NOT NULL,
    [BaseMapID]         INT              NOT NULL,
    [IsPublic]          BIT              CONSTRAINT [DF_tGeoSpa_IsPublic] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]      INT              NULL,
    [GeoSpaKey]         NVARCHAR (30)    NOT NULL,
    [GeoSpaKeyUnique]   AS               (coalesce([GeoSpaKey],CONVERT([varchar](30),[GeoSpaID],(0)))),
    [InitialExtent]     NVARCHAR (255)   NULL,
    [CreatedByUserID]   INT              NOT NULL,
    [ChangedByUserID]   INT              NOT NULL,
    [CreateDate]        DATETIME         CONSTRAINT [DF_tGeoSpa_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]        DATETIME         CONSTRAINT [DF_tGeoSpa_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [ParentGeoSpaID]    INT              NULL,
    [MapConfig]         NVARCHAR (MAX)   NULL,
    [AssetID]           INT              NULL,
    [GlobalID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tGeoSpa_GlobalID] DEFAULT (newid()) NOT NULL,
    [TimeBaseID]        INT              NULL,
    [AggregationTypeID] INT              NULL,
    [Decimate]          BIT              DEFAULT ((0)) NOT NULL,
    [DescendantsInherit] BIT NOT NULL DEFAULT ((1)), 
    CONSTRAINT [PK_tGeoSpa] PRIMARY KEY CLUSTERED ([GeoSpaID] ASC),
    CONSTRAINT [CK_tGeoSpa_AggregationTypeID] CHECK ([AggregationTypeID] IS NULL OR [TimeBaseID] IS NOT NULL AND [TimeBaseID]=(2)),
    CONSTRAINT [CK_tGeoSpa_Decimate] CHECK ([Decimate]=(0) OR [AggregationTypeID] IS NOT NULL AND ([TimeBaseID] IS NOT NULL AND [TimeBaseID]=(2))),
    CONSTRAINT [CK_tGeoSpa_TimeBaseID] CHECK ([TimeBaseID]<>(2) OR [AggregationTypeID] IS NOT NULL),
    CONSTRAINT [FK_GeoSpa_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGeoSpa_AggregationTypeID_tSummaryType_SummaryTypeID] FOREIGN KEY ([AggregationTypeID]) REFERENCES [ProcessData].[tSummaryType] ([SummaryTypeID]),
    CONSTRAINT [FK_tGeoSpa_ChangedByUserID_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tGeoSpa_CreatedByUserID_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tGeoSpa_tBaseMapType] FOREIGN KEY ([BaseMapID]) REFERENCES [GeoSpa].[tBaseMapType] ([BaseMapID]),
    CONSTRAINT [FK_tGeoSpa_tGeoSpa] FOREIGN KEY ([ParentGeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]),
    CONSTRAINT [FK_tGeoSpa_tMapType] FOREIGN KEY ([MapTypeID]) REFERENCES [GeoSpa].[tMapType] ([MapTypeID])
);
















GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_tGeoSpa_AssetID_Title]
    ON [GeoSpa].[tGeoSpa]([AssetID] ASC, [Title] ASC);

