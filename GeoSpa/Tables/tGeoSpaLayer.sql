﻿CREATE TABLE [GeoSpa].[tGeoSpaLayer] (
    [GeoSpaLayerID]   INT            IDENTITY (1, 1) NOT NULL,
    [GeoSpaID]        INT            NOT NULL,
    [Title]           NVARCHAR (50)  NULL,
    [GeoSpaLayerDesc] NVARCHAR (255) NULL,
    [IsPublic]        BIT            CONSTRAINT [DF_tGeoSpaLayer_IsPublic] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]    INT            NULL,
    [CreatedByUserID] INT            NOT NULL,
    [ChangedByUserID] INT            NOT NULL,
    [CreateDate]      DATETIME       CONSTRAINT [DF_tGeoSpaLayer_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME       CONSTRAINT [DF_tGeoSpaLayer_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [DisplayZoomMin]  INT			 NULL,
    [DisplayZoomMax]  INT			 NULL,
    CONSTRAINT [PK_tGeoSpaLayer] PRIMARY KEY CLUSTERED ([GeoSpaLayerID] ASC),
    CONSTRAINT [FK_tGeoSpaLayer_ChangedByUserID_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tGeoSpaLayer_CreatedByUserID_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tGeoSpaLayer_tGeoSpa_GeoSpaID] FOREIGN KEY ([GeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]) ON DELETE CASCADE,
	CONSTRAINT [Chk_tGeoSpaLayer_DisplayZoomMin] CHECK (DisplayZoomMin >= 0 AND DisplayZoomMin <= 23),
	CONSTRAINT [Chk_tGeoSpaLayer_DisplayZoomMax] CHECK (DisplayZoomMax >= 0 AND DisplayZoomMax <= 23)
);





