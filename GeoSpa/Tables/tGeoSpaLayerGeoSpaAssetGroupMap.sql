﻿CREATE TABLE [GeoSpa].[tGeoSpaLayerGeoSpaAssetGroupMap] (
    [GeoSpaLayerGeoSpaAssetGroupMapID] INT IDENTITY (1, 1) NOT NULL,
    [GeoSpaLayerID]                    INT NOT NULL,
    [GeoSpaAssetGroupID]               INT NOT NULL,
    CONSTRAINT [PK_tGeoSpaLayerGeoSpaAssetGroupMap] PRIMARY KEY CLUSTERED ([GeoSpaLayerGeoSpaAssetGroupMapID] ASC),
    CONSTRAINT [FK_tGeoSpaLayerGeoSpaAssetGroupMap_tGeoSpaAssetGroup] FOREIGN KEY ([GeoSpaAssetGroupID]) REFERENCES [GeoSpa].[tGeoSpaAssetGroup] ([GeoSpaAssetGroupID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGeoSpaLayerGeoSpaAssetGroupMap_tGeoSpaLayer] FOREIGN KEY ([GeoSpaLayerID]) REFERENCES [GeoSpa].[tGeoSpaLayer] ([GeoSpaLayerID]) ON DELETE CASCADE
);



