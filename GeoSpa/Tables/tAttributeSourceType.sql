﻿CREATE TABLE [GeoSpa].[tAttributeSourceType] (
    [AttributeSourceTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [AttributeSourceTypeKey]  NVARCHAR (30)  NOT NULL,
    [AttributeSourceTypeDesc] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_tAttributeSourceType] PRIMARY KEY CLUSTERED ([AttributeSourceTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'GeoSpa', @level1type = N'TABLE', @level1name = N'tAttributeSourceType';

