﻿CREATE TABLE [GeoSpa].[tMapTypeBaseMapMap] (
    [MapTypeBaseMapTypeID] INT IDENTITY (1, 1) NOT NULL,
    [MapTypeID]            INT NOT NULL,
    [BaseMapID]            INT NOT NULL,
    CONSTRAINT [PK_tMapTypeBaseMapMap] PRIMARY KEY CLUSTERED ([MapTypeBaseMapTypeID] ASC),
    CONSTRAINT [FK_tMapTypeBaseMapMap_tBaseMapType] FOREIGN KEY ([BaseMapID]) REFERENCES [GeoSpa].[tBaseMapType] ([BaseMapID]),
    CONSTRAINT [FK_tMapTypeBaseMapMap_tMapType] FOREIGN KEY ([MapTypeID]) REFERENCES [GeoSpa].[tMapType] ([MapTypeID])
);

