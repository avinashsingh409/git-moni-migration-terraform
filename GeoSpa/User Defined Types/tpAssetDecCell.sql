﻿CREATE TYPE [GeoSpa].[tpAssetDecCell] AS TABLE (
    [AssetID]        INT NOT NULL,
    [GeometryTypeID] INT NOT NULL,
    [TileLevel]      INT NOT NULL,
    [TileX]          INT NOT NULL,
    [TileY]          INT NOT NULL);


GO
GRANT EXECUTE
    ON TYPE::[GeoSpa].[tpAssetDecCell] TO [TEUser];

