﻿CREATE   FUNCTION [GeoSpa].[ufnGetAssetIDsForGeography]
(
    @boundary geography,
    @securityUserID int, 
	@startingAssetID int
)
RETURNS TABLE AS RETURN
(
    SELECT assets.AssetID, assets.GlobalID
	from Asset.tAssetSpatial tAS
	inner join [Asset].[ufnGetAssetIdsForUserStartingAt](@securityUserID, @startingAssetID) permittedAssets
		on tAS.AssetID = permittedAssets.AssetID
	inner join asset.tAsset assets on permittedAssets.AssetID = assets.AssetID
	where AssetGeography.Filter(@boundary) = 1	
)
GO
GRANT SELECT
    ON OBJECT::[GeoSpa].[ufnGetAssetIDsForGeography] TO [TEUser]
    AS [dbo];

