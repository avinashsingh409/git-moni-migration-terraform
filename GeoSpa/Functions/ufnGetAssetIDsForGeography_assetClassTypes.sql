﻿CREATE   FUNCTION [GeoSpa].[ufnGetAssetIDsForGeography_assetClassTypes]
(
    @boundary geography,
    @securityUserID int, 
	@startingAssetID int,
	@assetClassTypeIDs base.tpIntList READONLY
)
RETURNS TABLE AS RETURN
(
    SELECT assets.AssetID, assets.GlobalID
	from Asset.tAssetSpatial tAS
	inner join [Asset].[ufnGetAssetIdsForUserStartingAt](@securityUserID, @startingAssetID) permittedAssets
		on tAS.AssetID = permittedAssets.AssetID
	inner join asset.tAsset assets on permittedAssets.AssetID = assets.AssetID
	inner join @assetClassTypeIDs actIDs on assets.AssetClassTypeID = actIDs.id
	where AssetGeography.Filter(@boundary) = 1	
)
GO
GRANT SELECT
    ON OBJECT::[GeoSpa].[ufnGetAssetIDsForGeography_assetClassTypes] TO [TEUser]
    AS [dbo];

