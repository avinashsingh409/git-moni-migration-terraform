﻿CREATE   FUNCTION [GeoSpa].[ufnGetAssetIDsForGeography_filterAssets]
(
    @boundary geography,
    @securityUserID int, 
	@startingAssetID int,
	@filterAssets base.tpIntList READONLY
)
RETURNS TABLE AS RETURN
(
    SELECT assets.AssetID, assets.GlobalID
	from Asset.tAssetSpatial tAS
	inner join [Asset].[ufnGetAssetIdsForUserStartingAt](@securityUserID, @startingAssetID) permittedAssets
		on tAS.AssetID = permittedAssets.AssetID
	inner join @filterAssets providedAssets on permittedAssets.AssetID = providedAssets.id
	inner join asset.tAsset assets on providedAssets.id = assets.AssetID
	where AssetGeography.Filter(@boundary) = 1	
)
GO
GRANT SELECT
    ON OBJECT::[GeoSpa].[ufnGetAssetIDsForGeography_filterAssets] TO [TEUser]
    AS [dbo];

