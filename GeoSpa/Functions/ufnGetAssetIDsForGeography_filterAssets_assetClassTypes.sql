﻿-- Finds the set of assets to which this user has access that lay within the provided boundary polygon,
-- or intersect the boundary at one or more positions. Any point that lay on the boundary will be found,
-- along with any line or polygon that intersets the bounds or lies entirely within the boundary.
CREATE   FUNCTION [GeoSpa].[ufnGetAssetIDsForGeography_filterAssets_assetClassTypes]
(
    @boundary geography,
    @securityUserID int, 
	@startingAssetID int,
	@filterAssets base.tpIntList READONLY,
	@assetClassTypeIDs base.tpIntList READONLY
)
RETURNS TABLE AS RETURN
(
    SELECT assets.AssetID, assets.GlobalID
	from Asset.tAssetSpatial tAS
	inner join [Asset].[ufnGetAssetIdsForUserStartingAt](@securityUserID, @startingAssetID) permittedAssets
		on tAS.AssetID = permittedAssets.AssetID
	inner join @filterAssets providedAssets on permittedAssets.AssetID = providedAssets.id
	inner join asset.tAsset assets on providedAssets.id = assets.AssetID
	inner join @assetClassTypeIDs actIDs on assets.AssetClassTypeID = actIDs.id
	where AssetGeography.Filter(@boundary) = 1	
)
GO
GRANT SELECT
    ON OBJECT::[GeoSpa].[ufnGetAssetIDsForGeography_filterAssets_assetClassTypes] TO [TEUser]
    AS [dbo];

