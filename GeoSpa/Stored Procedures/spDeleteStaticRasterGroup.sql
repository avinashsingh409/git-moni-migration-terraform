﻿
CREATE PROCEDURE [GeoSpa].[spDeleteStaticRasterGroup]
	@StaticRasterGroupID int
	, @DeleteCount int output
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM GeoSpa.tStaticRasterGroup WHERE StaticRasterGroupID = @StaticRasterGroupID
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spDeleteStaticRasterGroup] TO [TEUser]
    AS [dbo];

