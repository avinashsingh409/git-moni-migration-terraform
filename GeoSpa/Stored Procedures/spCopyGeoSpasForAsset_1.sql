﻿CREATE PROC [GeoSpa].[spCopyGeoSpasForAsset] (@sourceAssetId INT, @destAssetId int, @securityUserID int)
AS

BEGIN

DECLARE @returnValue as int = -1

BEGIN TRANSACTION

BEGIN TRY

DECLARE @map_ID int;
DECLARE @map_MapID int;
DECLARE @map_title nvarchar(255);
DECLARE @map_geospadesc nvarchar(255);
DECLARE @map_maptypeid int;
DECLARE @map_basemapid int;
DECLARE @map_ispublic int;
DECLARE @map_displayorder int;
DECLARE @map_geospakey nvarchar(30);
DECLARE @map_initialextent nvarchar(255);
DECLARE @map_parentgeospaid int;
DECLARE @map_mapconfig nvarchar(max);
DECLARE @map_projectNumber int;
DECLARE @map_timebaseid int;
DECLARE @map_aggregationtypeid int;
DECLARE @map_decimate bit;
DECLARE @map_newMapID int; 
DECLARE @map_descendantsInherit bit;
DECLARE @layer_ID int;
DECLARE @layer_assetclasstypeid int;
DECLARE @layer_title nvarchar(255);
DECLARE @layer_symbology nvarchar(max); 
DECLARE @layer_min_zoom INT;
DECLARE @layer_max_zoom INT;
DECLARE @layer_newLayerID int; 
DECLARE @layer_geospalayerdesc nvarchar(255);
DECLARE @layer_geometry int;

DECLARE map_cursor CURSOR
	FOR 
	SELECT GeoSpaID,Title,GeoSpaDesc,MapTypeID,BaseMapID,IsPublic,DisplayOrder,GeoSpaKey,InitialExtent,ParentGeoSpaID,MapConfig,TimeBaseID,AggregationTypeID,Decimate,DescendantsInherit
	FROM GeoSpa.tGeoSpa WHERE AssetID = @sourceAssetID


OPEN map_cursor
FETCH NEXT FROM map_cursor
INTO @map_ID,@map_title,@map_geospadesc,@map_maptypeid,@map_basemapid,@map_ispublic,@map_displayorder,@map_geospakey,@map_initialextent,@map_parentgeospaid,@map_mapconfig,@map_timebaseid,@map_aggregationtypeid,@map_decimate,@map_descendantsInherit

WHILE @@FETCH_STATUS = 0
BEGIN

INSERT INTO [GeoSpa].[tGeoSpa]
        ([Title]
        ,[GeoSpaDesc]
        ,[MapTypeID]
        ,[BaseMapID]
        ,[IsPublic]
        ,[DisplayOrder]
        ,[GeoSpaKey]
        ,[InitialExtent]
        ,[CreatedByUserID]
        ,[ChangedByUserID]
        ,[CreateDate]
        ,[ChangeDate]
        ,[ParentGeoSpaID]
        ,[MapConfig]
        ,[AssetID]
		,[TimeBaseID]
		,[AggregationTypeID]
		,[Decimate]
		,[DescendantsInherit])
    VALUES
        (@map_title
        ,@map_geospadesc
        ,@map_maptypeid
        ,@map_basemapid
        ,@map_ispublic
        ,@map_displayorder
        ,@map_geospakey
        ,@map_initialextent
        ,@securityUserID
        ,@securityUserID
        ,GETDATE()
        ,GETDATE()
        ,@map_parentgeospaid
        ,@map_mapconfig
        ,@destAssetId
		,@map_timebaseid
		,@map_aggregationtypeid
		,@map_decimate
		,@map_descendantsInherit)

		SET @map_newMapID = @@IDENTITY

		DECLARE maplayer_cursor CURSOR
		FOR SELECT 
		

		    a.GeoSpaLayerID ,c.AssetClassTypeID, a.Title, a.GeoSpaLayerDesc, ISNULL(c.GeometryTypeID,1),
			c.SymbologyProperties, a.DisplayZoomMin, a.DisplayZoomMax
			from GeoSpa.tGeoSpaLayer a 
			LEFT JOIN GeoSpa.tGeoSpaLayerGeoSpaAssetGroupMap b on a.GeoSpaLayerID = b.GeoSpaLayerID
			LEFT JOIN GeoSpa.tGeoSpaAssetGroup c on b.GeoSpaAssetGroupID = c.GeoSpaAssetGroupID
			--LEFT JOIN GeoSpa.tAttributeSource d on c.AttributeSourceID = d.AttributeSourceID
			--LEFT JOIN GeoSpa.tAttributesSql e on d.AttributeSourceGUID = e.ContentID
			where a.GeoSpaID = @map_ID

		OPEN maplayer_cursor
		FETCH NEXT FROM maplayer_cursor
		INTO @layer_ID, @layer_assetclasstypeid, @layer_title,@layer_geospalayerdesc, @layer_geometry, @layer_symbology, @layer_min_zoom, @layer_max_zoom
		WHILE @@FETCH_STATUS = 0
		BEGIN

			INSERT INTO [GeoSpa].[tGeoSpaLayer]
					([GeoSpaID]
					,[Title]
					,[GeoSpaLayerDesc]
					,[IsPublic]
					,[DisplayOrder]
					,[DisplayZoomMin]
					,[DisplayZoomMax]
					,[CreatedByUserID]
					,[ChangedByUserID]
					,[CreateDate]
					,[ChangeDate])
				VALUES
					(@map_newMapID
					,@layer_title
					,@layer_geospalayerdesc
					,1
					,null
					,@layer_min_zoom
					,@layer_max_zoom
					,@securityUserID
					,@securityUserID 
					,GETDATE()
					,GETDATE())
					SET @layer_newLayerID = @@IDENTITY;

					DECLARE @sourceIDGUID UNIQUEIDENTIFIER = newid(); 

					INSERT INTO [GeoSpa].[tGeoSpaSql]
						([ContentID]
						,[TableName]
						,[GeoSpaFK]
						,[TablePK]
						,[IncludeFields]
						,[ExcludeFields])
					VALUES
						(@sourceIDGUID
						,'[Asset].[tAssetAttribute]'
						,'AssetID'
						,'AssetID'
						,NULL
						,NULL)

					INSERT INTO GeoSpa.tGeoSpaSource
					(GeoSpaSourceTypeID, GeoSpaSourceGUID)
					VALUES (2, @sourceIDGUID)
					DECLARE @sourceID int = @@IDENTITY ;

					DECLARE @attSourceIDGUID UNIQUEIDENTIFIER = newid(); 

					INSERT INTO [GeoSpa].[tAttributesSql]
					([ContentID]
					,[TableName]
					,[GeoSpaFK]
					,[TablePK]
					,[IncludeFields]
					,[ExcludeFields])
					VALUES
					(@attSourceIDGUID
					,'[UIConfig].[tAssetProperties]'
					,'AssetID'
					,'AssetID'
					,NULL
					,NULL)

					INSERT INTO GeoSpa.tAttributeSource
					(AttributeSourceTypeID, AttributeSourceGUID)
					VALUES (1, @attSourceIDGUID)
					DECLARE @attributesourceID int = @@IDENTITY;

					INSERT INTO [GeoSpa].[tGeoSpaAssetGroup]
						([AssetClassTypeID]
						,[GeometryTypeID]
						,[DisplayOrder]
						,[GeoSpaSourceID]
						,[Icon]
						,[SelectedIcon]
						,[SymbologyProperties]
						,[AttributeSourceID])
					VALUES
						(@layer_assetclasstypeid
						,@layer_geometry --point
						,null
						,@sourceID
						,null
						,null
						,@layer_symbology
						,@attributesourceID)
						DECLARE @groupID INT = @@IDENTITY; 
						
					INSERT INTO GeoSpa.tGeoSpaLayerGeoSpaAssetGroupMap(GeoSpaLayerID, GeoSpaAssetGroupID) VALUES(@layer_newLayerID,@groupID)

			FETCH NEXT FROM maplayer_cursor
			INTO @layer_ID, @layer_assetclasstypeid, @layer_title,@layer_geospalayerdesc, @layer_geometry, @layer_symbology, @layer_min_zoom, @layer_max_zoom
		END 
		CLOSE maplayer_cursor
		DEALLOCATE maplayer_cursor

FETCH NEXT FROM map_cursor
INTO @map_ID,@map_title,@map_geospadesc,@map_maptypeid,@map_basemapid,@map_ispublic,@map_displayorder,@map_geospakey,@map_initialextent,@map_parentgeospaid,@map_mapconfig,@map_timebaseid,@map_aggregationtypeid,@map_decimate,@map_descendantsInherit
END

CLOSE map_cursor
DEALLOCATE map_cursor
COMMIT TRANSACTION

SET @returnValue = 1

END TRY

BEGIN CATCH	
	PRINT N'Script Failed, rolling back';
	ROLLBACK TRANSACTION	
END CATCH

RETURN @returnValue

END
GO

GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spCopyGeoSpasForAsset] TO [TEUser]
    AS [dbo];
GO
