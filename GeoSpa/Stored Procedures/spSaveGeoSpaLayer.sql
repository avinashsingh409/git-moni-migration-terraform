﻿

CREATE PROCEDURE [GeoSpa].[spSaveGeoSpaLayer]
	@SecurityUserID INT,
	@GeoSpaLayerID INT=NULL,
	@GeoSpaID INT,
	@Title NVARCHAR(255),
	@GeoSpaLayerDesc NVARCHAR(255),
	@IsPublic BIT,
	@DisplayOrder INT,
	@DisplayZoomMin INT,
	@DisplayZoomMax INT,
	@NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT * FROM GeoSpa.tGeoSpa WHERE GeoSpaID = @GeoSpaID)
	BEGIN
		RAISERROR( 'No such geospa',16, 1);
	END 

	IF EXISTS (SELECT * FROM GeoSpa.tGeoSpaLayer WHERE GeoSpaLayerID = @GeoSpaLayerID)
	BEGIN
		UPDATE GeoSpa.tGeoSpaLayer
		SET Title = @Title, 
		GeoSpaLayerDesc = @GeoSpaLayerDesc, 
		IsPublic = @IsPublic,
		DisplayOrder = @DisplayOrder,
		DisplayZoomMin = @DisplayZoomMin,
		DisplayZoomMax = @DisplayZoomMax,
		CreatedByUserID = @SecurityUserID,
		ChangedByUserID = @SecurityUserID,
		CreateDate = GETDATE(),
		ChangeDate = GETDATE()
		WHERE GeoSpa.tGeoSpaLayer.GeoSpaLayerID = @GeoSpaLayerID 
		SET @NewID = @GeoSpaLayerID;
	END
	ELSE
	BEGIN
	INSERT INTO [GeoSpa].[tGeoSpaLayer]
           ([GeoSpaID]
           ,[Title]
           ,[GeoSpaLayerDesc]
           ,[IsPublic]
           ,[DisplayOrder]
           ,[DisplayZoomMin]
           ,[DisplayZoomMax]
           ,[CreatedByUserID]
           ,[ChangedByUserID]
           ,[CreateDate]
           ,[ChangeDate])
     VALUES
           (@GeoSpaID
           ,@Title
           ,@GeoSpaLayerDesc
           ,@IsPublic
           ,@DisplayOrder
           ,@DisplayZoomMin
           ,@DisplayZoomMax
           ,@SecurityUserID
           ,@SecurityUserID
           ,GetDate()
           ,GetDate())
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spSaveGeoSpaLayer] TO [TEUser]
    AS [dbo];

