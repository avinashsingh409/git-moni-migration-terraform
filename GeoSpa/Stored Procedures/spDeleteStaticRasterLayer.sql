﻿

CREATE PROCEDURE [GeoSpa].[spDeleteStaticRasterLayer]
	@StaticRasterLayerID int
	, @DeleteCount int output
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM GeoSpa.tStaticRasterLayer WHERE StaticRasterLayerID = @StaticRasterLayerID
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spDeleteStaticRasterLayer] TO [TEUser]
    AS [dbo];

