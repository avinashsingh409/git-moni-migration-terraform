﻿





CREATE PROCEDURE [GeoSpa].[spSaveStaticRasterLayer]
	@StaticRasterLayerID int = NULL
	, @GeoSpaID int
	, @RasterContentID uniqueidentifier
	, @NewID int output
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT * FROM GeoSpa.tGeoSpa WHERE GeoSpaID = @GeoSpaID)
	BEGIN
		RAISERROR(N'GeoSpa does not exist', 16, 1);
	END

	IF EXISTS (SELECT * FROM GeoSpa.tStaticRasterLayer WHERE StaticRasterLayerID = @StaticRasterLayerID)
	BEGIN
		UPDATE GeoSpa.tStaticRasterLayer
		SET
			GeoSpaID = @GeoSpaID
			, RasterContentID = @RasterContentID
		WHERE GeoSpa.tStaticRasterLayer.StaticRasterLayerID = @StaticRasterLayerID
		SET @NewID = @StaticRasterLayerID
	END
	ELSE
	BEGIN
		INSERT INTO GeoSpa.tStaticRasterLayer (GeoSpaID, RasterContentID)
		VALUES (@GeoSpaID, @RasterContentID)
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spSaveStaticRasterLayer] TO [TEUser]
    AS [dbo];

