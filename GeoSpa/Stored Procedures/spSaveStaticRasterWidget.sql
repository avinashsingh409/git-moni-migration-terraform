﻿

CREATE PROCEDURE [GeoSpa].[spSaveStaticRasterWidget]
	@StaticRasterWidgetID int = NULL
	, @StaticRasterGroupID int
	, @WidgetTypeID int
	, @RelativeXPosition real
	, @RelativeYPosition real
	, @AssetID int = NULL
	, @AssetVariableTypeTagMapID int = NULL
	, @NewID int output
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT * FROM GeoSpa.tStaticRasterGroup WHERE StaticRasterGroupID = @StaticRasterGroupID)
	BEGIN
		RAISERROR(N'StaticRasterGroup does not exist', 16, 1);
	END

	IF EXISTS (SELECT * FROM GeoSpa.tStaticRasterWidget WHERE StaticRasterWidgetID = @StaticRasterWidgetID)
	BEGIN
		UPDATE GeoSpa.tStaticRasterWidget
		SET
			StaticRasterGroupID = @StaticRasterGroupID
			, WidgetTypeID = @WidgetTypeID
			, RelativeXPosition = @RelativeXPosition
			, RelativeYPosition = @RelativeYPosition
			, AssetID = @AssetID
			, AssetVariableTypeTagMapID = @AssetVariableTypeTagMapID
		WHERE GeoSpa.tStaticRasterWidget.StaticRasterWidgetID = @StaticRasterWidgetID
		SET @NewID = @StaticRasterWidgetID
	END
	ELSE
	BEGIN
		INSERT INTO GeoSpa.tStaticRasterWidget (StaticRasterGroupID, WidgetTypeID, RelativeXPosition, RelativeYPosition, AssetID, AssetVariableTypeTagMapID)
		VALUES (@StaticRasterGroupID, @WidgetTypeID, @RelativeXPosition, @RelativeYPosition, @AssetID, @AssetVariableTypeTagMapID)
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spSaveStaticRasterWidget] TO [TEUser]
    AS [dbo];

