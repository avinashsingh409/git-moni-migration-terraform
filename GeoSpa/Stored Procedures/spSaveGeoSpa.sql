﻿CREATE PROCEDURE [GeoSpa].[spSaveGeoSpa]
	@SecurityUserID INT,
	@AssetID INT = NULL,
	@GeoSpaID INT=NULL,
	@Title NVARCHAR(255),
	@GeoSpaDesc NVARCHAR(255),
	@MapTypeID INT,
	@BaseMapID INT,
	@IsPublic BIT,
	@DisplayOrder INT,
	@GeoSpaKey NVARCHAR(30),
	@InitialExtent NVARCHAR(255),
	@ParentGeoSpaID INT = NULL,
	@MapConfig NVARCHAR(MAX) = NULL,
	@TimeBaseID INT = NULL,
	@AggregationTypeID INT = NULL,
	@Decimate BIT = 0,
	@DescendantsInherit BIT = 1,
	@NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User cannot access asset',16, 1);
	END

	IF EXISTS (SELECT * FROM GeoSpa.tGeoSpa WHERE GeoSpaID = @GeoSpaID)
	BEGIN
		UPDATE GeoSpa.tGeoSpa
		SET Title = @Title, 
		GeoSpaDesc = @GeoSpaDesc, 
		MapTypeID = @MapTypeID,
		BaseMapID = @BaseMapID,
		IsPublic = @IsPublic,
		DisplayOrder = @DisplayOrder,
		GeoSpaKey = @GeoSpaKey,
		InitialExtent = @InitialExtent,
		ChangedByUserID = @SecurityUserID,
		ChangeDate = GETDATE(),
		ParentGeoSpaID = @ParentGeoSpaID,
		MapConfig = @MapConfig,
		AssetID = @AssetID,
		TimeBaseID = @TimeBaseID,
		AggregationTypeID = @AggregationTypeID,
		Decimate = @Decimate,
		DescendantsInherit = @DescendantsInherit
		WHERE GeoSpa.tGeoSpa.GeoSpaID = @GeoSpaID 
	END
	ELSE
	BEGIN
		INSERT INTO GeoSpa.tGeoSpa
		(Title, GeoSpaDesc, MapTypeID, BaseMapID, IsPublic, DisplayOrder, GeoSpaKey, InitialExtent,CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate, ParentGeoSpaID, MapConfig,AssetID, TimeBaseID, AggregationTypeID, Decimate, DescendantsInherit)
		VALUES
		(@Title, @GeoSpaDesc, @MapTypeID, @BaseMapID, @IsPublic, @DisplayOrder, @GeoSpaKey, @InitialExtent, @SecurityUserID, @SecurityUserID, GETDATE(), GETDATE(), @ParentGeoSpaID, @MapConfig,@AssetID, @TimeBaseID, @AggregationTypeID, @Decimate, @DescendantsInherit );
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO

GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spSaveGeoSpa] TO [TEUser]
    AS [dbo];

GO

