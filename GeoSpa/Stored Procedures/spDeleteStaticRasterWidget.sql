﻿
CREATE PROCEDURE [GeoSpa].[spDeleteStaticRasterWidget]
	@StaticRasterWidgetID int
	, @DeleteCount int output
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM GeoSpa.tStaticRasterWidget WHERE StaticRasterWidgetID = @StaticRasterWidgetID
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spDeleteStaticRasterWidget] TO [TEUser]
    AS [dbo];

