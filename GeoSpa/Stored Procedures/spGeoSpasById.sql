CREATE PROCEDURE [GeoSpa].[spGeoSpasById] 
	@User int
	, @Asset int 
	, @GeoSpaIDs [Base].[tpIntList] READONLY 
	, @MapTypes [Base].[tpIntList] READONLY
	, @NodeAssetDescendants [Base].[tpIntList] READONLY
	, @CategoryIDs [Base].[tpIntList] READONLY
AS
BEGIN
	SET NOCOUNT ON;

	 select
		g.GeoSpaID
		, g.Title
		, g.GeoSpaDesc
		, g.MapTypeID
		, g.BaseMapID
		, g.IsPublic
		, g.DisplayOrder
		, g.GeoSpaKey
		, g.GeoSpaKeyUnique
		, g.InitialExtent
		, createdBy.Username as CreatedBy
		, changedBy.Username as ChangedBy
		, g.CreateDate
		, g.ChangeDate
		, g.ParentGeoSpaID
		, g.MapConfig
		, g.AssetID
		, g.GlobalID
		, g.TimeBaseID
		, g.AggregationTypeID
		, g.Decimate
		, g.DescendantsInherit

		-- regular layers
		, l.GeoSpaLayerID
		, l.Title as LayerTitle
	 	, l.GeoSpaLayerDesc
	 	, l.IsPublic as LayerIsPublic
	 	, l.DisplayOrder as LayerDisplayOrder
	 	, l.DisplayZoomMin
	 	, l.DisplayZoomMax
	 	, l.CreatedByUserID as LayerCreatedByUserID
	 	, l.ChangedByUserID as LayerChangedByUserID
	 	, l.CreateDate as LayerCreateDate
	 	, l.ChangeDate as LayerChangeDate
		, grp.GeoSpaAssetGroupID
		, grp.AssetClassTypeID
		, grp.GeometryTypeID
		, grp.DisplayOrder as GroupDisplayOrder
		, grp.GeoSpaSourceID
		, grp.Icon
		, grp.SelectedIcon
		, grp.SymbologyProperties
		, grp.AttributeSourceID
		, actyp.AssetClassTypeAbbrev
		, (
			case when @Asset > 0 then
				Asset.ufnAssetHasDescendantsOfAssetClassType(@Asset, grp.AssetClassTypeID)
			else
				case when
				(
				select count (*)
				from @NodeAssetDescendants d
				join Asset.tAsset a on d.id = a.AssetID
				where a.AssetClassTypeID = grp.AssetClassTypeID
				) > 0 then cast(1 as bit) else cast(0 as bit) end
			end
		) as HasDescendants
		, attrsrc.AttributeSourceTypeID
		, attrsrc.AttributeSourceGUID
		, attrsql.AttributesSqlID as AttributesSqlIDattrsql
		, attrsql.TableName as TableNameattrsql
		, attrsql.GeoSpaFK as GeoSpaFKattrsql
		, attrsql.TablePK as TablePKattrsql
		, attrsql.IncludeFields as IncludeFieldsattrsql
		, attrsql.ExcludeFields as ExcludeFieldsattrsql
		, gssrc.GeoSpaSourceTypeID
		, gssrc.GeoSpaSourceGUID
		, gsjson.JsonID as JsonIDgsjson
		, gssql.GeoSpaSqlID as GeoSpaSqlIDgssql
		, gssql.TableName as TableNamegssql
		, gssql.GeoSpaFK as GeoSpaFKgssql
		, gssql.TablePK as TablePKgssql
		, gssql.IncludeFields as IncludeFieldsgssql
		, gssql.ExcludeFields as ExcludeFieldsgssql
		, gsrest.GeoSpaRestID as GeoSpaRestIDgsrest
		, gsrest.RestUrl as RestUrlgsrest
		, gsrest.GeoSpaRestLayerTypeID as GeoSpaRestLayerTypeIDgsrest
		, gsrest.[Credentials] as Credentialsgsrest
		-- regular layers
		-- ancillary layers
		, al.GeoSpaAncillaryLayerID
		, al.LayerIDExtension
		, al.Title as AncillaryLayerTitle
		, al.AncillaryDescription
		, al.IsPublic as AncillaryLayerIsPublic
		, al.DisplayOrder as AncillaryLayerDisplayOrder
		, al.DisplayZoomMax as AncillaryLayerDisplayZoomMax
		, al.DisplayZoomMin as AncillaryLayerDisplayZoomMin
		, al.CreatedByUserID as AncillaryLayerCreatedByUserID
		, al.ChangedByUserID as AncillaryLayerChangedByUserID
		, al.CreateDate as AncillaryLayerCreateDate
		, al.ChangeDate as AncillaryLayerChangeDate
		, am.AncillaryLayerSourceMapID
		, am.GeoSpaSourceID as AncillaryLayerGeoSpaSourceID
		, am.AttributeSourceID as AncillaryLayerAttributeSourceID
		, alattrsrc.AttributeSourceTypeID as AncillaryLayerAttributeSourceTypeID
		, alattrsrc.AttributeSourceGUID as AncillaryLayerAttributeSourceGUID
		, alattrsql.AttributesSqlID as AncillaryLayerAttributesSqlIDattrsql
		, alattrsql.TableName as AncillaryLayerTableNameattrsql
		, alattrsql.GeoSpaFK as AncillaryLayerGeoSpaFKattrsql
		, alattrsql.TablePK as AncillaryLayerTablePKattrsql
		, alattrsql.IncludeFields as AncillaryLayerIncludeFieldsattrsql
		, alattrsql.ExcludeFields as AncillaryLayerExcludeFieldsattrsql
		, algssrc.GeoSpaSourceTypeID as AncillaryLayerGeoSpaSourceTypeID
		, algssrc.GeoSpaSourceGUID as AncillaryLayerGeoSpaSourceGUID
		, algsjson.JsonID as AncillaryLayerJsonIDgsjson
		, algssql.GeoSpaSqlID as AncillaryLayerGeoSpaSqlIDgssql
		, algssql.TableName as AncillaryLayerTableNamegssql
		, algssql.GeoSpaFK as AncillaryLayerGeoSpaFKgssql
		, algssql.TablePK as AncillaryLayerTablePKgssql
		, algssql.IncludeFields as AncillaryLayerIncludeFieldsgssql
		, algssql.ExcludeFields as AncillaryLayerExcludeFieldsgssql
		, algsrest.GeoSpaRestID as AncillaryLayerGeoSpaRestIDgsrest
		, algsrest.RestUrl as AncillaryLayerRestUrlgsrest
		, algsrest.GeoSpaRestLayerTypeID as AncillaryLayerGeoSpaRestLayerTypeIDgsrest
		, algsrest.[Credentials] as AncillaryLayerCredentialsgsrest
		-- ancillary layers
		-- static raster layers
		, sl.StaticRasterLayerID
		, sl.RasterContentID
		, sg.StaticRasterGroupID
		, sg.DisplayOrder as StaticRasterGroupDisplayOrder
		, sw.StaticRasterWidgetID
		, sw.WidgetTypeID
		, sw.RelativeXPosition
		, sw.RelativeYPosition
		, sw.AssetID as StaticRasterWidgetAssetID
		, sw.AssetVariableTypeTagMapID
		, wt.WidgetAbbrev
		, wt.WidgetDesc
		, wt.WidgetBehavior
		-- static raster layers
		-- categories
		, cat.CategoryID
		, cat.ChangeDate as CategoryChangeDate
		, cat.ChangedByUserID as CategoryChangedByUserID
		, cat.ClientId as CategoryClientId
		, cat.CreateDate as CategoryCreateDate
		, cat.CreatedByUserID as CategoryCreatedByUserID
		, cat.Name as CategoryName
		-- categories

	 from GeoSpa.tGeoSpa g 
	 join AccessControl.tUser createdBy on g.CreatedByUserID = createdBy.SecurityUserID  
	 join AccessControl.tUser changedBy on g.ChangedByUserID = changedBy.SecurityUserID  
	 join @GeoSpaIDs a on g.GeoSpaID = a.id  
	 -- regular layers
	 left join GeoSpa.tGeoSpaLayer l on g.GeoSpaID = l.GeoSpaID 
	 left join GeoSpa.tGeoSpaLayerGeoSpaAssetGroupMap map on l.GeoSpaLayerID = map.GeoSpaLayerID 
	 left join GeoSpa.tGeoSpaAssetGroup grp on map.GeoSpaAssetGroupID = grp.GeoSpaAssetGroupID 
	 left join Asset.tAssetClassType actyp on grp.AssetClassTypeID = actyp.AssetClassTypeID  
	 left join GeoSpa.tAttributeSource attrsrc on grp.AttributeSourceID = attrsrc.AttributeSourceID  
	 left join GeoSpa.tGeoSpaSource gssrc on grp.GeoSpaSourceID = gssrc.GeoSpaSourceID  
	 left join GeoSpa.tAttributesSql attrsql on attrsrc.AttributeSourceGUID = attrsql.ContentID 
	 left join GeoSpa.tJson gsjson on gssrc.GeoSpaSourceGUID = gsjson.ContentID  
	 left join GeoSpa.tGeoSpaSql gssql on gssrc.GeoSpaSourceGUID = gssql.ContentID  
	 left join GeoSpa.tGeoSpaRest gsrest on gssrc.GeoSpaSourceGUID = gsrest.ContentID  
	 -- regular layers
	 -- ancillary layers
	 left join GeoSpa.tGeoSpaAncillaryLayer al on g.GeoSpaID = al.GeoSpaID
	 left join GeoSpa.tAncillaryLayerSourceMap am on al.GeoSpaAncillaryLayerID = am.GeoSpaAncillaryLayerID
	 left join GeoSpa.tAttributeSource alattrsrc on am.AttributeSourceID = alattrsrc.AttributeSourceID
	 left join GeoSpa.tGeoSpaSource algssrc on am.GeoSpaSourceID = algssrc.GeoSpaSourceID
	 left join GeoSpa.tAttributesSql alattrsql on alattrsrc.AttributeSourceGUID = alattrsql.ContentID 
	 left join GeoSpa.tJson algsjson on algssrc.GeoSpaSourceGUID = algsjson.ContentID  
	 left join GeoSpa.tGeoSpaSql algssql on algssrc.GeoSpaSourceGUID = algssql.ContentID  
	 left join GeoSpa.tGeoSpaRest algsrest on algssrc.GeoSpaSourceGUID = algsrest.ContentID  
	 -- ancillary layers
	 -- static raster layers
	 left join GeoSpa.tStaticRasterLayer sl on g.GeoSpaID = sl.GeoSpaID
	 left join GeoSpa.tStaticRasterGroup sg on sl.StaticRasterLayerID = sg.StaticRasterLayerID
	 left join GeoSpa.tStaticRasterWidget sw on sg.StaticRasterGroupID = sw.StaticRasterGroupID
	 left join GeoSpa.tWidgetType wt on sw.WidgetTypeID = wt.WidgetTypeID
	 -- static raster layers
	 -- categories
	 left join GeoSpa.tGeoSpaCategoryMap m on g.GeoSpaID = m.GeoSpaID
	 left join Asset.tCategory cat on m.CategoryID = cat.CategoryID
	 where
		((m.GeoSpaID IS NULL
		OR (m.CategoryID NOT IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)) 
		OR (m.CategoryID IN (select categoryID from AccessControl.tRoleCategoryMap r 
							 join AccessControl.tuserRoleMap urm on r.SecurityRoleID = urm.SecurityRoleID
							 where urm.SecurityUserID = @User)))
		AND ((not exists (select id from @CategoryIDs)) or (m.CategoryID in (select id from @CategoryIDs))))
	 -- categories
		and ((not exists (select id from @MapTypes)) or (g.MapTypeID in (select id from @MapTypes)))

	 order by g.DisplayOrder, g.GeoSpaID, l.DisplayOrder, l.GeoSpaLayerID, grp.DisplayOrder, grp.GeoSpaAssetGroupID, al.DisplayOrder, al.GeoSpaAncillaryLayerID, sg.DisplayOrder, sg.StaticRasterGroupID
	 ;

END
GO

GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spGeoSpasById] TO [TEUser]
    AS [dbo];
GO
