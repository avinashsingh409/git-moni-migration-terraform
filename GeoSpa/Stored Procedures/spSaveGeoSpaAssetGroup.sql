﻿

CREATE PROCEDURE [GeoSpa].[spSaveGeoSpaAssetGroup]
	@SecurityUserID INT,
	@GeoSpaAssetGroupID INT = NULL,
	@GeoSpaLayerID INT,
	@AssetClassTypeID INT,
	@GeometryTypeID INT,
	@DisplayOrder INT,
	@GeoSpaSourceID INT,
	@Icon NVARCHAR(50),
	@SelectedIcon NVARCHAR(50),
	@SymbologyProperties NVARCHAR(MAX),
	@AttributeSourceID INT,
	@NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT * FROM GeoSpa.tGeoSpaLayer WHERE GeoSpaLayerID = @GeoSpaLayerID)
	BEGIN
		RAISERROR( 'No such layer',16, 1);
	END

	IF EXISTS (SELECT * FROM GeoSpa.tGeoSpaAssetGroup g 
				INNER JOIN GeoSpa.tGeoSpaLayerGeoSpaAssetGroupMap map ON map.GeoSpaAssetGroupID = g.GeoSpaAssetGroupID
				INNER JOIN GeoSpa.tGeoSpaLayer l ON map.GeoSpaLayerID = l.GeoSpaLayerID
				WHERE g.GeoSpaAssetGroupID = @GeoSpaAssetGroupID AND l.GeoSpaLayerID = @GeoSpaLayerID)
	BEGIN
		UPDATE [GeoSpa].[tGeoSpaAssetGroup]
		SET [AssetClassTypeID] = @AssetClassTypeID
			,[GeometryTypeID] = @GeometryTypeID
			,[DisplayOrder] = @DisplayOrder
			,[GeoSpaSourceID] = @GeoSpaSourceID
			,[Icon] = @Icon
			,[SelectedIcon] = @SelectedIcon
			,[SymbologyProperties] = @SymbologyProperties
			,[AttributeSourceID] = @AttributeSourceID
		WHERE GeoSpaAssetGroupID = @GeoSpaAssetGroupID
	END
	ELSE
	BEGIN
		INSERT INTO [GeoSpa].[tGeoSpaAssetGroup]
			   ([AssetClassTypeID]
			   ,[GeometryTypeID]
			   ,[DisplayOrder]
			   ,[GeoSpaSourceID]
			   ,[Icon]
			   ,[SelectedIcon]
			   ,[SymbologyProperties]
			   ,[AttributeSourceID])
		 VALUES
			   (@AssetClassTypeID
			   ,@GeometryTypeID
			   ,@DisplayOrder
			   ,@GeoSpaSourceID
			   ,@Icon
			   ,@SelectedIcon
			   ,@SymbologyProperties
			   ,@AttributeSourceID)
			   SET @NewID = (SELECT SCOPE_IDENTITY());

			   INSERT INTO GeoSpa.tGeoSpaLayerGeoSpaAssetGroupMap (GeoSpaLayerID, GeoSpaAssetGroupID)
			   VALUES (@GeoSpaLayerID, @NewID)
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spSaveGeoSpaAssetGroup] TO [TEUser]
    AS [dbo];

