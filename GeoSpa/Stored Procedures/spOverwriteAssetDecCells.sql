﻿
CREATE PROCEDURE [GeoSpa].[spOverwriteAssetDecCells]
	@UserID int,
	@AssetID int,
	@AssetDecCells GeoSpa.tpAssetDecCell READONLY,
	@Success bit OUTPUT
AS
BEGIN

	BEGIN TRANSACTION

	BEGIN TRY

	DECLARE @secAssets Base.tpIntList;
	INSERT INTO @secAssets (Id)
	SELECT AssetID FROM Asset.ufnGetAssetIdsForUserStartingAt(@UserID, @AssetID);

	DELETE FROM adc
	FROM GeoSpa.tAssetDecCell adc
	JOIN @AssetDecCells d ON adc.AssetID = d.AssetID
	JOIN @secAssets b on adc.AssetID = b.Id;

	INSERT INTO GeoSpa.tAssetDecCell (AssetID, GeometryTypeID, TileLevel, TileX, TileY)
	SELECT adc.AssetID, adc.GeometryTypeID, adc.TileLevel, adc.TileX, adc.TileY
	FROM @AssetDecCells adc
	JOIN @SecAssets b on adc.AssetID = b.Id;

	SET @Success = 1;

	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		SET @Success = 0;
	END CATCH

END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spOverwriteAssetDecCells] TO [TEUser]
    AS [dbo];

