﻿

CREATE PROCEDURE [GeoSpa].[spSaveStaticRasterGroup]
	@StaticRasterGroupID int = NULL
	, @StaticRasterLayerID int
	, @DisplayOrder int
	, @NewID int output
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT * FROM GeoSpa.tStaticRasterLayer WHERE StaticRasterLayerID = @StaticRasterLayerID)
	BEGIN
		RAISERROR(N'StaticRasterLayer does not exist', 16, 1);
	END

	IF EXISTS (SELECT * FROM GeoSpa.tStaticRasterGroup WHERE StaticRasterGroupID = @StaticRasterGroupID)
	BEGIN
		UPDATE GeoSpa.tStaticRasterGroup
		SET
			StaticRasterLayerID = @StaticRasterLayerID
			, DisplayOrder = @DisplayOrder
		WHERE GeoSpa.tStaticRasterGroup.StaticRasterGroupID = @StaticRasterGroupID
		SET @NewID = @StaticRasterGroupID
	END
	ELSE
	BEGIN
		INSERT INTO GeoSpa.tStaticRasterGroup (StaticRasterLayerID, DisplayOrder)
		VALUES (@StaticRasterLayerID, @DisplayOrder)
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[GeoSpa].[spSaveStaticRasterGroup] TO [TEUser]
    AS [dbo];

