FROM mcr.microsoft.com/dotnet/sdk:latest

RUN apt-get update -qy \
    && apt-get upgrade -y \
    && apt-get install -y msbuild sqlpackage
