﻿Create Table Base.tCertifiedQuery (
	[QueryID] int Identity(1,1) not null,
	[Name] varchar(255) not null,
	[Description] varchar(255) not null,
	[Query] varchar(max) not null
)
GO

Alter Table Base.tCertifiedQuery
Add Constraint [PK_QueryID] Primary Key ([QueryID]);
GO

Alter Table Base.tCertifiedQuery
Add Constraint [UC_Name] Unique ([Name])
GO