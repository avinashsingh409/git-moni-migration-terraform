﻿CREATE TRIGGER [Base].[EventQueueNotify]
   ON  [Base].[tEventQueue]
   AFTER INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	
    IF UPDATE(EventTypeID) OR UPDATE(EventStatusTypeID) 
	BEGIN		
		INSERT INTO Base.tEventQueue (EventID, EventTypeID, EventStatusTypeID, EventQueueDescription, CreatedBy, EventNotificationAddress)
		SELECT EventQueueID, 9, 2, 'Event Queue Notification', 'PPMD', b.EventQueueNotificationEmail
		FROM Inserted a INNER JOIN Base.tEventQueueNotification b 
		ON a.EventTypeID = b.EventTypeID AND a.EventStatusTypeID = b.EventStatusTypeID		
	END
END
