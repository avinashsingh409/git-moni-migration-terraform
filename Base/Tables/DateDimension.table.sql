CREATE TABLE [Base].[DateDimension] (
    [Year]  INT NOT NULL,
    [Month] INT NOT NULL,
    [Day]   INT NOT NULL,
    [Hour]  INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'DateDimension';

