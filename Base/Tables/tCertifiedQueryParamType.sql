﻿Create Table Base.tCertifiedQueryParamType (
	[TypeID] int not null,
	[Description] varchar(50) not null
)
GO

Alter Table Base.tCertifiedQueryParamType
Add Constraint [PK_TypeID] Primary Key ([TypeID]);
GO