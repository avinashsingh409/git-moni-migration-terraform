CREATE TABLE [Base].[tEventType] (
    [EventTypeID]             INT            NOT NULL,
    [EventTypeDesc]           NVARCHAR (255) NOT NULL,
    [EventTypeAbbrev]         NVARCHAR (50)  NOT NULL,
    [Archive]                 BIT            CONSTRAINT [DF_Base_tEventType_Archive] DEFAULT ((0)) NOT NULL,
    [ArchiveDuration_day]     INT            CONSTRAINT [DF_Base_tEventType_ArchiveDuration_day] DEFAULT ((7)) NOT NULL,
    [EventEndPointTypeID]     INT            NULL,
    [EventEndPointConnection] NVARCHAR (MAX) NULL,
    [ArchiveExpiration_day]   INT            NULL,
    CONSTRAINT [PK_Base_tEventType] PRIMARY KEY CLUSTERED ([EventTypeID] ASC),
    CONSTRAINT [FK_Base_tEventType_EventEndPointTypeID_Base_tEventEndPointType] FOREIGN KEY ([EventEndPointTypeID]) REFERENCES [Base].[tEventEndPointType] ([EventEndPointTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventType';

