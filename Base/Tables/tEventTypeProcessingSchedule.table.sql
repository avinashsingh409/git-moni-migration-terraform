CREATE TABLE [Base].[tEventTypeProcessingSchedule] (
    [EventTypeProcessingScheduleID] INT IDENTITY (1, 1) NOT NULL,
    [EventTypeID]                   INT NOT NULL,
    [HourofDay]                     INT NOT NULL,
    CONSTRAINT [FK_Base_tEventTypeProcessingSchedule_EventTypeID_Base_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Base].[tEventType] ([EventTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventTypeProcessingSchedule';

