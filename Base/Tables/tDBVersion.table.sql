﻿CREATE TABLE [Base].[tDBVersion] (
    [DBVersionID]    INT           IDENTITY (1, 1) NOT NULL,
    [DBVersion]      VARCHAR (255) NOT NULL,
    [DBVersionDesc]  VARCHAR (255) NOT NULL,
    [ScriptFileName] VARCHAR (255) NOT NULL,
    [CreatedBy]      VARCHAR (255) CONSTRAINT [DF_tDBVersion_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreateDate]     DATETIME      CONSTRAINT [DF_tDBVersion_CreateDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tDBVersion] PRIMARY KEY CLUSTERED ([DBVersion] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tDBVersion';

