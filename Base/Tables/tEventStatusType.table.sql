CREATE TABLE [Base].[tEventStatusType] (
    [EventStatusTypeID]     INT            NOT NULL,
    [EventStatusTypeDesc]   NVARCHAR (255) NOT NULL,
    [EventStatusTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Base_tEventStatusType] PRIMARY KEY CLUSTERED ([EventStatusTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventStatusType';

