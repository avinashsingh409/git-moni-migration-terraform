CREATE TABLE [Base].[tUnitMeasure] (
    [ID]          INT          NOT NULL,
    [CommonDesc]  VARCHAR (24) NULL,
    [DisplayDesc] VARCHAR (24) NOT NULL
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tUnitMeasure';

