CREATE TABLE [Base].[tDataCacheKeyValue] (
    [DataCacheKeyValueID] INT              IDENTITY (1, 1) NOT NULL,
    [DataCacheID]         UNIQUEIDENTIFIER NOT NULL,
    [Key]                 VARCHAR (50)     NOT NULL,
    [Value]               VARCHAR (255)    NOT NULL,
    CONSTRAINT [FK_tDataCacheKeyValue_DataCacheID_tDataCache] FOREIGN KEY ([DataCacheID]) REFERENCES [Base].[tDataCache] ([DataCacheID]) ON DELETE CASCADE ON UPDATE CASCADE
);






GO

CREATE CLUSTERED INDEX [IDX_tDataCacheKeyValue_Clustered] ON [Base].[tDataCacheKeyValue] 
(
	[DataCacheKeyValueID] ASC,
	[DataCacheID] ASC,
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [PK_DataCacheKeyValueID] ON [Base].[tDataCacheKeyValue] 
(
	[DataCacheKeyValueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IDX_tDataCacheKeyValue_KeyValue
ON [Base].[tDataCacheKeyValue] ([Key],[Value])
INCLUDE ([DataCacheID])
GO
CREATE NONCLUSTERED INDEX IDX_tDataCacheKeyValue_Helper
ON [Base].[tDataCacheKeyValue] ([DataCacheID])
INCLUDE ([DataCacheKeyValueID],[Key],[Value])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tDataCacheKeyValue';

