CREATE TABLE [Base].[tTableChangeLog] (
    [TableChangeLogID] INT           IDENTITY (1, 1) NOT NULL,
    [DateOfChange]     DATETIME      NULL,
    [UserName]         [sysname]     NOT NULL,
    [ddl]              VARCHAR (MAX) NULL,
    CONSTRAINT [PK_TableChangeLog] PRIMARY KEY CLUSTERED ([TableChangeLogID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tTableChangeLog';

