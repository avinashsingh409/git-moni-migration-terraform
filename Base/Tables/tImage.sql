﻿CREATE TABLE [Base].[tImage] (
    [ImageId]        INT              IDENTITY (1, 1) NOT NULL,
    [ImageUniqueId]  UNIQUEIDENTIFIER DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [Name]           VARCHAR (MAX)    NULL,
    [IsSql]          BIT              NOT NULL,
    [IsDisk]         BIT              NOT NULL,
    [IsAWS]          BIT              NOT NULL,
    [IsAzure]        BIT              NOT NULL,
    [CreatedBy]      NVARCHAR (MAX)   NOT NULL,
    [ChangedBy]      NVARCHAR (MAX)   NOT NULL,
    [CreateDate]     DATETIME         NOT NULL,
    [ChangeDate]     DATETIME         NOT NULL,
    [OwningAssetId]  INT              NULL,
    [Format]         VARCHAR (255)    NULL,
    [RepositoryName] NVARCHAR (255)   NULL,
    [UploadComplete] BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tImage] PRIMARY KEY CLUSTERED ([ImageId] ASC),
    CONSTRAINT [FK_tImage_tAsset] FOREIGN KEY ([OwningAssetId]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);




GO
CREATE INDEX [IDX_tImage_ImageUniqueId] ON [Base].[tImage] ([ImageUniqueId])  ;