CREATE TABLE [Base].[tDataCache] (
    [DataCacheID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tDataCache_DataCacheID] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [Catalog]      VARCHAR (255)    NOT NULL,
    [DataKey]      VARCHAR (255)    NOT NULL,
    [DataType]     VARCHAR (50)     NOT NULL,
    [Length]       INT              NOT NULL,
    [Data]         VARBINARY (MAX)  NOT NULL,
    [Hits]         INT              CONSTRAINT [DF_tDataCache_Hits] DEFAULT ((0)) NOT NULL,
    [CreateDate]   DATETIME         CONSTRAINT [DF_tDataCache_CreateDate] DEFAULT (getdate()) NOT NULL,
    [LastAccessed] DATETIME         CONSTRAINT [DF_tDataCache_LastAccessed] DEFAULT (getdate()) NOT NULL,
    [DataLocation] INT              NOT NULL,
    CONSTRAINT [PK_tDataCache] PRIMARY KEY CLUSTERED ([DataCacheID] ASC),
    CONSTRAINT [UC_CatalogDataKey] UNIQUE NONCLUSTERED ([Catalog] ASC, [DataKey] ASC)
);






GO
CREATE NONCLUSTERED INDEX IDX_tDataCache_DataType
ON Base.tDataCache (DataType)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tDataCache';

