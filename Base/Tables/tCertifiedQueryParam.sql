﻿Create Table Base.tCertifiedQueryParam (
	[QueryID] int not null,
	[Name] varchar(50) not null,
	[TypeID] int not null,
	[AllowNull] bit not null default 0,
	[DefaultValue] varchar(255)
)
GO

Alter Table Base.tCertifiedQueryParam
With Check Add Constraint [FK_tCertifiedQueryParam_tCertifiedQuery] Foreign Key([QueryID])
References Base.tCertifiedQuery ([QueryID])
GO

Alter Table Base.tCertifiedQueryParam
With Check Add Constraint [FK_tCertifiedQueryParam_tCertifiedQueryParamType] Foreign Key([TypeID])
References Base.tCertifiedQueryParamType ([TypeID])
GO

Alter Table Base.tCertifiedQueryParam
Add Constraint [UC_QueryID_Name] Unique ([QueryID], [Name])
GO