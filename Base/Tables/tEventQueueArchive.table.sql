CREATE TABLE [Base].[tEventQueueArchive] (
    [EventQueueArchiveID]          INT            IDENTITY (1, 1) NOT NULL,
    [EventQueueArchiveDescription] NVARCHAR (255) NULL,
    [EventTypeID]                  INT            NOT NULL,
    [EventStatusTypeID]            INT            NOT NULL,
    [StartTime]                    DATETIME       NULL,
    [StopTime]                     DATETIME       NULL,
    [ExpirationDate]               DATETIME       NULL,
    [RequestedCompletionDate]      DATETIME       NULL,
    [CreatedBy]                    NVARCHAR (255) NOT NULL,
    [CreateDate]                   DATETIME       NOT NULL,
    [ArchiveDate]                  DATETIME       NOT NULL,
    CONSTRAINT [FK_Base_tEventQueueArchive_EventStatusTypeID_Base_tEventStatusType] FOREIGN KEY ([EventStatusTypeID]) REFERENCES [Base].[tEventStatusType] ([EventStatusTypeID]),
    CONSTRAINT [FK_Base_tEventQueueArchive_EventTypeID_Base_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Base].[tEventType] ([EventTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventQueueArchive';

