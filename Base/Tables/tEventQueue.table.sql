CREATE TABLE [Base].[tEventQueue] (
    [EventQueueID]             INT            IDENTITY (1, 1) NOT NULL,
    [EventID]                  INT            NOT NULL,
    [StartTime]                DATETIME       NULL,
    [StopTime]                 DATETIME       NULL,
    [ExpirationDate]           DATETIME       NULL,
    [RequestedCompletionDate]  DATETIME       NULL,
    [CreatedBy]                NVARCHAR (255) NOT NULL,
    [CreateDate]               DATETIME       CONSTRAINT [DF_Base_tEventQueue_CreateDate] DEFAULT (getdate()) NOT NULL,
    [EventQueueDescription]    NVARCHAR (255) NULL,
    [EventTypeID]              INT            NOT NULL,
    [EventStatusTypeID]        INT            NOT NULL,
    [EventStatusMessage]       VARCHAR (MAX)  NULL,
    [EventNotificationAddress] NVARCHAR (255) NULL,
    [ArchiveTime]              DATETIME       NULL,
    [EventSourceUser]          NVARCHAR (255) NULL,
    CONSTRAINT [PK_Base_tEventQueue] PRIMARY KEY CLUSTERED ([EventQueueID] ASC),
    CONSTRAINT [FK_Base_tEventQueue_EventStatusTypeID_Base_tEventStatusType] FOREIGN KEY ([EventStatusTypeID]) REFERENCES [Base].[tEventStatusType] ([EventStatusTypeID]),
    CONSTRAINT [FK_Base_tEventQueue_EventTypeID_Base_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Base].[tEventType] ([EventTypeID]),
    CONSTRAINT [UK_Base_tEventQueue_EventID_EventTypeID] UNIQUE NONCLUSTERED ([EventID] ASC, [EventTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventQueue';

