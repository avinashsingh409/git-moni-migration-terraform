CREATE TABLE [Base].[tEventEndPointType] (
    [EventEndPointTypeID]     INT            NOT NULL,
    [EventEndPointTypeDesc]   NVARCHAR (255) NOT NULL,
    [EventEndPointTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Base_tEventEndPointType] PRIMARY KEY CLUSTERED ([EventEndPointTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventEndPointType';

