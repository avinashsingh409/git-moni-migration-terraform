CREATE TABLE [Base].[tEventObject] (
    [EventID]          INT             IDENTITY (1, 1) NOT NULL,
    [ObjectByteLength] INT             NOT NULL,
    [EventObject]      VARBINARY (MAX) NOT NULL,
    CONSTRAINT [PK_Base_tObjectEvent] PRIMARY KEY CLUSTERED ([EventID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventObject';

