CREATE TABLE [Base].[tLogEntries] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [Timestamp]        DATETIME       NOT NULL,
    [Application]      NVARCHAR (50)  NOT NULL,
    [LogLevel]         INT            NULL,
    [LevelString]      NVARCHAR (50)  NULL,
    [Message]          NVARCHAR (MAX) NOT NULL,
    [ExceptionMessage] NVARCHAR (MAX) NULL,
    [InnerException]   NVARCHAR (MAX) NULL,
    [TimeDuration]     INT            NOT NULL,
    [ApplicationType]  NVARCHAR (50)  NULL,
    [MachineName]      NVARCHAR (50)  NOT NULL,
    [UserName]         NVARCHAR (50)  NULL,
    [StackTrace]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tLogEntries] PRIMARY KEY CLUSTERED ([ID] ASC)
);







GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tLogEntries';

