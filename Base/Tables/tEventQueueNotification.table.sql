CREATE TABLE [Base].[tEventQueueNotification] (
    [EventQueueNotificationID]    INT            IDENTITY (1, 1) NOT NULL,
    [EventTypeID]                 INT            NOT NULL,
    [EventStatusTypeID]           INT            NOT NULL,
    [EventQueueNotificationEmail] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Base_tEventQueueNotification] PRIMARY KEY CLUSTERED ([EventQueueNotificationID] ASC),
    CONSTRAINT [FK_Base_tEventQueueNotification_EventStatusTypeID_Base_tEventStatusType] FOREIGN KEY ([EventStatusTypeID]) REFERENCES [Base].[tEventStatusType] ([EventStatusTypeID]),
    CONSTRAINT [FK_Base_tEventQueueNotification_EventTypeID_Base_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Base].[tEventType] ([EventTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Base', @level1type = N'TABLE', @level1name = N'tEventQueueNotification';

