﻿
CREATE FUNCTION [Base].[ufnMonthlyTimeRanges]
(
	@startTime DateTime, 
	@endTime DateTime,
	@aggPeriod nvarchar(10),
	@firstDOW int
)
RETURNS @periodTable TABLE
(
   Period int PRIMARY KEY not NULL
  ,TimeStamp datetime not NULL
  ,WeekNumber int not NULL
  ,PeriodStart datetime not NULL
  ,PeriodEnd datetime not NULL
)
AS
BEGIN

	DECLARE @startPeriod datetime = Base.ufnTruncateDate(@startTime, 'MONTH', @firstDOW)
	
	IF (UPPER(@aggPeriod) = 'YTD' OR UPPER(@aggPeriod) = 'MTD' OR UPPER(@aggPeriod) = 'ITD')
	BEGIN
		
		DECLARE @truncation nvarchar(10)
		SELECT @truncation =
			CASE
				--Time range for a given day is from the first day of the year to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'YTD' THEN 'YEAR'
				
				--Time range for a given day is from the first day of the month to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'MTD' THEN 'MONTH'
				
				--Time range for a given day is from 1/1/1900 to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'ITD' THEN 'INCEPTION'
			END
			
		;WITH TimeRanges AS
		(
			SELECT
				 1 PeriodIndex
				,@startPeriod TimeStamp
				,DATEPART(ISO_WEEK, @startPeriod) WeekNumber
				,Base.ufnTruncateDate(@startTime, @truncation, @firstDOW) PeriodStart
				,CASE WHEN @endTime < Base.ufnEndOfMonth(@startTime) THEN @endTime ELSE Base.ufnEndOfMonth(@startTime) END PeriodEnd
				,DATEADD(MONTH, 1, @startPeriod) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,Base.ufnTruncateDate(NextTimeStamp, @truncation, @firstDOW) PeriodStart
				,CASE WHEN @endTime < Base.ufnEndOfMonth(NextTimeStamp) THEN @endTime ELSE Base.ufnEndOfMonth(NextTimeStamp) END PeriodEnd
				,DATEADD(MONTH, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges
	END

	ELSE IF (UPPER(@aggPeriod) = 'MONTHLY')
	
		--Time range for a given day is the previous COMPLETE month.
		WITH TimeRanges
		AS
		(
			SELECT
				 1 PeriodIndex
				,@startPeriod TimeStamp
				,DATEPART(ISO_WEEK, @startPeriod) WeekNumber
				,Base.ufnTruncateDate(@startTime, 'MONTH', @firstDOW) PeriodStart
				,CASE WHEN @endTime < Base.ufnEndOfMonth(@startTime)
				 THEN @endTime ELSE Base.ufnEndOfMonth(@startTime)
				 END PeriodEnd
				,DATEADD(MONTH, 1, @startPeriod) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,Base.ufnTruncateDate(NextTimeStamp, 'MONTH', @firstDOW) PeriodStart
				,CASE WHEN @endTime < Base.ufnEndOfMonth(NextTimeStamp)
				 THEN @endTime ELSE Base.ufnEndOfMonth(NextTimeStamp)
				 END PeriodEnd
				,DATEADD(MONTH, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges

	ELSE IF (UPPER(@aggPeriod) = 'ANNUAL')
	
		--Time range for a given day is the previous COMPLETE year.
		WITH TimeRanges
		AS
		(
			SELECT
				 1 PeriodIndex
				,@startPeriod TimeStamp
				,DATEPART(ISO_WEEK, @startPeriod) WeekNumber
				,Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW) PeriodStart
				,DATEADD(SECOND, -1, DATEADD(YEAR, 1, Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW))) PeriodEnd
				--,CASE WHEN @endTime < DATEADD(SECOND, -1, DATEADD(YEAR, 1, Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW)))
				-- THEN @endTime ELSE DATEADD(SECOND, -1, DATEADD(YEAR, 1, Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW)))
				-- END PeriodEnd
				,DATEADD(MONTH, 1, @startPeriod) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW) PeriodStart
				,DATEADD(SECOND, -1, DATEADD(YEAR, 1, Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW))) PeriodEnd
				--,CASE WHEN @endTime < DATEADD(SECOND, -1, DATEADD(YEAR, 1, Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW)))
				-- THEN @endTime ELSE DATEADD(SECOND, -1, DATEADD(YEAR, 1, Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW)))
				-- END PeriodEnd
				,DATEADD(MONTH, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges

	RETURN 
END