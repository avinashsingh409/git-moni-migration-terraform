﻿
-- =============================================
-- Author:		Brian Schumacher
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [Base].[ufnEndOfWeek] 
(
	-- Add the parameters for the function here
	@dat datetime,
	@firstDOW int
)
RETURNS DateTime
AS
BEGIN
	-- Truncate tothe first day of the month
	DECLARE @firstDayOfWeek DateTime = Base.ufnTruncateDate( @dat, 'WEEK', @firstDOW)
	
	-- Declare the return variable here
	DECLARE @Result DateTime = DATEADD(SECOND, -1, DATEADD(DAY, 7, @firstDayOfWeek))

	RETURN @Result

END