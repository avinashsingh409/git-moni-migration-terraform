﻿
-- =============================================
-- Author:		Brian Schumacher
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [Base].[ufnEndOfMonth] 
(
	-- Add the parameters for the function here
	@dat datetime
)
RETURNS DateTime
AS
BEGIN
	-- Truncate tothe first day of the month
	DECLARE @firstDayOfMonth DateTime = Base.ufnTruncateDate( @dat, 'MONTH', 0)
	
	-- Declare the return variable here
	DECLARE @Result DateTime = DATEADD(SECOND, -1, DATEADD(MONTH, 1, @firstDayOfMonth))

	RETURN @Result

END