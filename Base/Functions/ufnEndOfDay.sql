﻿
-- =============================================
-- Author:		Brian Schumacher
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [Base].[ufnEndOfDay] 
(
	-- Add the parameters for the function here
	@dat datetime
)
RETURNS DateTime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result DateTime = DATEADD(SECOND, -1, DATEADD(DAY, 1, DATEADD(DAY, DATEDIFF(DAY, 0, @dat), 0)))
		
	RETURN @Result

END