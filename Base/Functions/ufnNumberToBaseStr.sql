CREATE FUNCTION [Base].[ufnNumberToBaseStr] (@number int, @base int)
RETURNS varchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @dividend int = @number
	,@remainder int = 0
	,@numberString varchar(MAX) = CASE WHEN @number = 0 THEN '0' ELSE '' END ;
	SET @base = CASE WHEN @base <= 36 THEN @base ELSE 36 END;--The max base is 36, includes the range of [0-9A-Z]
	WHILE (@dividend > 0 OR @remainder > 0)
	BEGIN
		SET @remainder = @dividend % @base ; --The reminder by the division number in base
		SET @dividend = @dividend / @base ; -- The integer part of the division, becomes the new divident for the next loop
		IF(@dividend > 0 OR @remainder > 0)--check that not correspond the last loop when quotient and reminder is 0
		SET @numberString = CHAR( (CASE WHEN @remainder <= 9 THEN ASCII('0') ELSE ASCII('A')-10 END) + @remainder ) + @numberString;
	END;
	RETURN(@numberString);
END
GO
GRANT EXECUTE
	ON OBJECT::[Base].[ufnNumberToBaseStr] TO [TEUser]
	AS [dbo];
GO