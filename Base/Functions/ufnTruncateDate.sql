﻿
-- =============================================
-- Author:		Brian Schumacher
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [Base].[ufnTruncateDate] 
(
	-- Add the parameters for the function here
	@dat datetime,
	@period nvarchar(10),
	@firstDOW int = 0
)
RETURNS DateTime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result DateTime

	IF (UPPER(@period) = 'DAY')
		SET @Result = DATEADD(DAY, DATEDIFF(DAY, 0, @dat), 0)
	ELSE IF  (UPPER(@period) = 'WEEK')
		SET @Result = DATEADD(DAY, (DATEDIFF(DAY, @firstDOW, @dat)/7*7), @firstDOW)
	ELSE IF  (UPPER(@period) = 'MONTH')
		SET @Result = DATEADD(MONTH, (DATEDIFF(MONTH, 0, @dat)), 0)
	ELSE IF  (UPPER(@period) = 'YEAR')
		SET @Result = DATEADD(YEAR, (DATEDIFF(YEAR, 0, @dat)), 0)
	ELSE IF  (UPPER(@period) = 'INCEPTION')
		SET @Result = '1/1/1900'
		
	RETURN @Result

END