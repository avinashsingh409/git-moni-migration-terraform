CREATE FUNCTION [Base].[ufnReplaceEmbeddedImageWithText] (@HTMLText VARCHAR(MAX), @MSGText VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
     BEGIN
         DECLARE @Start INT;
         DECLARE @End INT;
         DECLARE @Length INT;
         SET @Start = CHARINDEX('<img src=', @HTMLText);
         SET @End = CHARINDEX('>', @HTMLText, @Start);
         SET @Length = (@End - @Start) + 1;
         WHILE @Start > 0
               AND @End > 0
               AND @Length > 0
             BEGIN
                 SET @HTMLText = STUFF(@HTMLText, @Start, @Length, @MSGText);
                 SET @Start = CHARINDEX('<img src=', @HTMLText);
                 SET @End = CHARINDEX('>', @HTMLText, @Start);
                 SET @Length = (@End - @Start) + 1;
             END;
         RETURN LTRIM(RTRIM(@HTMLText));
     END
GO
GRANT EXECUTE
	ON OBJECT::[Base].[ufnReplaceEmbeddedImageWithText] TO [TEUser]
	AS [dbo];
GO