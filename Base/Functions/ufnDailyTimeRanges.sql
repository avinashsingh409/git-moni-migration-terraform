﻿
CREATE FUNCTION [Base].[ufnDailyTimeRanges]
(
	@startTime DateTime, 
	@endTime DateTime,
	@aggPeriod nvarchar(10),
	@firstDOW int
)
RETURNS @periodTable TABLE
(
   Period int PRIMARY KEY not NULL
  ,TimeStamp datetime not NULL
  ,WeekNumber int not NULL
  ,PeriodStart datetime not NULL
  ,PeriodEnd datetime not NULL
)
AS
BEGIN

	DECLARE @startDate datetime = Base.ufnTruncateDate(@startTime, 'DAY', @firstDOW)
	
	IF (UPPER(@aggPeriod) = 'YTD' OR UPPER(@aggPeriod) = 'MTD' OR UPPER(@aggPeriod) = 'WTD' OR UPPER(@aggPeriod) = 'ITD' OR UPPER(@aggPeriod) = 'DAILY')
	BEGIN
		
		DECLARE @truncation nvarchar(10)
		SELECT @truncation =
			CASE
				--Time range for a given day is from the beginning of the day to the end of the day.
				WHEN UPPER(@aggPeriod) = 'DAILY' THEN 'DAY'
				
				--Time range for a given day is from the first day of the year to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'YTD' THEN 'YEAR'
				
				--Time range for a given day is from the first day of the month to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'MTD' THEN 'MONTH'
				
				--Time range for a given day is from the first day of the week as defined
				--by @firstDOW (0 = Monday) to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'WTD' THEN 'WEEK'

				--Time range for a given day is from 1/1/1900 to the end of the given day.
				WHEN UPPER(@aggPeriod) = 'ITD' THEN 'INCEPTION'
			END
			
		;WITH TimeRanges AS
		(
			SELECT
				 1 PeriodIndex
				,@startDate TimeStamp
				,DATEPART(ISO_WEEK, @startDate) WeekNumber
				,Base.ufnTruncateDate(@startTime, @truncation, @firstDOW) PeriodStart
				,CASE WHEN @endTime < Base.ufnEndOfDay(@startTime) THEN @endTime ELSE Base.ufnEndOfDay(@startTime) END PeriodEnd
				,DATEADD(DAY, 1, @startDate) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,Base.ufnTruncateDate(NextTimeStamp, @truncation, @firstDOW) PeriodStart
				,CASE WHEN @endTime < DATEADD(DAY, 1, PeriodEnd) THEN @endTime ELSE DATEADD(DAY, 1, PeriodEnd) END PeriodEnd
				,DATEADD(DAY, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges
	END

	ELSE IF (UPPER(@aggPeriod) = 'WEEKLY')
	
		--Time range for a given day is the previous COMPLETE week.
		WITH TimeRanges
		AS
		(
			SELECT
				 1 PeriodIndex
				,@startDate TimeStamp
				,DATEPART(ISO_WEEK, @startDate) WeekNumber
				,DATEADD(DAY, -7, Base.ufnTruncateDate(@startTime, 'WEEK', @firstDOW)) PeriodStart
				,CASE WHEN @endTime < DATEADD(SECOND, -1, Base.ufnTruncateDate(@startTime, 'WEEK', @firstDOW))
				 THEN @endTime ELSE DATEADD(SECOND, -1, Base.ufnTruncateDate(@startTime, 'WEEK', @firstDOW))
				 END PeriodEnd
				,DATEADD(DAY, 1, @startDate) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,DATEADD(DAY, -7, Base.ufnTruncateDate(NextTimeStamp, 'WEEK', @firstDOW)) PeriodStart
				,CASE WHEN @endTime < DATEADD(SECOND, -1, Base.ufnTruncateDate(NextTimeStamp, 'WEEK', @firstDOW))
				 THEN @endTime ELSE DATEADD(SECOND, -1, Base.ufnTruncateDate(NextTimeStamp, 'WEEK', @firstDOW))
				 END PeriodEnd
				,DATEADD(DAY, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges

	ELSE IF (UPPER(@aggPeriod) = 'MONTHLY')
	
		--Time range for a given day is the previous COMPLETE month.
		WITH TimeRanges
		AS
		(
			SELECT
				 1 PeriodIndex
				,@startDate TimeStamp
				,DATEPART(ISO_WEEK, @startDate) WeekNumber
				,DATEADD(MONTH, -1, Base.ufnTruncateDate(@startTime, 'MONTH', @firstDOW)) PeriodStart
				,CASE WHEN @endTime < DATEADD(SECOND, -1, Base.ufnTruncateDate(@startTime, 'MONTH', @firstDOW))
				 THEN @endTime ELSE DATEADD(SECOND, -1, Base.ufnTruncateDate(@startTime, 'MONTH', @firstDOW))
				 END PeriodEnd
				,DATEADD(DAY, 1, @startDate) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,DATEADD(MONTH, -1, Base.ufnTruncateDate(NextTimeStamp, 'MONTH', @firstDOW)) PeriodStart
				,CASE WHEN @endTime < DATEADD(SECOND, -1, Base.ufnTruncateDate(NextTimeStamp, 'MONTH', @firstDOW))
				 THEN @endTime ELSE DATEADD(SECOND, -1, Base.ufnTruncateDate(NextTimeStamp, 'MONTH', @firstDOW))
				 END PeriodEnd
				,DATEADD(DAY, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges

	ELSE IF (UPPER(@aggPeriod) = 'ANNUAL')
	
		--Time range for a given day is the previous COMPLETE year.
		WITH TimeRanges
		AS
		(
			SELECT
				 1 PeriodIndex
				,@startDate TimeStamp
				,DATEPART(ISO_WEEK, @startDate) WeekNumber
				,DATEADD(YEAR, -1, Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW)) PeriodStart
				,CASE WHEN @endTime < DATEADD(SECOND, -1, Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW))
				 THEN @endTime ELSE DATEADD(SECOND, -1, Base.ufnTruncateDate(@startTime, 'YEAR', @firstDOW))
				 END PeriodEnd
				,DATEADD(DAY, 1, @startDate) NextTimeStamp
			UNION ALL
			SELECT
				 PeriodIndex+1 PeriodIndex
				,NextTimeStamp TimeStamp
				,DATEPART(ISO_WEEK, NextTimeStamp) WeekNumber
				,DATEADD(YEAR, -1, Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW)) PeriodStart
				,CASE WHEN @endTime < DATEADD(SECOND, -1, Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW))
				 THEN @endTime ELSE DATEADD(SECOND, -1, Base.ufnTruncateDate(NextTimeStamp, 'YEAR', @firstDOW))
				 END PeriodEnd
				,DATEADD(DAY, 1, NextTimeStamp) NextTimeStamp
			FROM TimeRanges
			WHERE NextTimeStamp <= @endTime
		)
		INSERT INTO @periodTable
		SELECT PeriodIndex, TimeStamp, WeekNumber, PeriodStart, PeriodEnd FROM TimeRanges

	RETURN 
END