﻿CREATE TYPE [Base].[tpIntStringPair] AS TABLE(
	[Id] [int] NOT NULL,
    [Label] NVARCHAR (255) NOT NULL
)
GO

GRANT EXECUTE
    ON TYPE::[Base].[tpIntStringPair] TO [TEUser];
GO
