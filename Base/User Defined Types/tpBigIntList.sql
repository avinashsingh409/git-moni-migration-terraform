﻿CREATE TYPE [Base].[tpBigIntList] AS TABLE (
    [id] BIGINT NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC));


GO
GRANT EXECUTE
    ON TYPE::[Base].[tpBigIntList] TO [TEUser];

