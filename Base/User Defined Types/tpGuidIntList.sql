﻿CREATE TYPE [Base].[tpGuidIntList] AS TABLE (
    [GuidID] UNIQUEIDENTIFIER NOT NULL,
	[IntID] INT NOT NULL
	);

GO

GRANT EXECUTE
    ON TYPE::[Base].[tpGuidIntList] TO [TEUser];
GO
