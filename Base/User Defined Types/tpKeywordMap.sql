﻿CREATE TYPE [Base].[tpKeywordMap] AS TABLE(
	[EntityKey] [nvarchar](255) NOT NULL,
	[KeywordID] [bigint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[TagDate] [datetime] NOT NULL,
	[BackDate] [date] NULL
)
GO

GRANT EXECUTE
    ON TYPE::[Base].[tpKeywordMap] TO [TEUser];

GO