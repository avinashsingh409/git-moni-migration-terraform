﻿CREATE TYPE [Base].[tpIntStringList] AS TABLE (
    [Id]           INT            NOT NULL,
    [Label]        NVARCHAR (255) NOT NULL,
    [DisplayOrder] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC));


GO
GRANT EXECUTE
    ON TYPE::[Base].[tpIntStringList] TO [TEUser];

