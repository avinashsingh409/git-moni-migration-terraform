﻿CREATE TYPE [Base].[tpKVP] AS TABLE (
    [pkey]   NVARCHAR (MAX) NOT NULL,
    [pvalue] NVARCHAR (MAX) NOT NULL);

GO
GRANT EXECUTE
    ON TYPE::[Base].[tpKVP] TO [TEUser];
