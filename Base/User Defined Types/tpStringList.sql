﻿CREATE TYPE [Base].[tpStringList] AS TABLE (
    [value] NVARCHAR (MAX) NULL);


GO
GRANT EXECUTE
    ON TYPE::[Base].[tpStringList] TO [TEUser];

