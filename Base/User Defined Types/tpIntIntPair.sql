﻿CREATE TYPE [Base].[tpIntIntPair] AS TABLE(
	[Id1] [int] NOT NULL,
	[Id2] [int] NOT NULL
)
GO

GRANT EXECUTE
    ON TYPE::[Base].[tpIntIntPair] TO [TEUser];
GO
