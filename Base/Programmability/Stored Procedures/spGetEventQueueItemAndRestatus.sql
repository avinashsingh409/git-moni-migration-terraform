﻿

--New work extraction procedure, to properly synchronize access between workers.
CREATE PROCEDURE [Base].[spGetEventQueueItemAndRestatus]
	@EventTypeID INT
,	@CurrentEventStatusTypeID INT
,	@NewEventStatusTypeID INT
,	@EventQueueID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	
	set @EventQueueID=0
	--Check to see if there are any CQI sets waiting to be run
	IF (SELECT COUNT(*) FROM [Base].[tEventStatusType] WHERE EventStatusTypeID = @NewEventStatusTypeID) >= 1 
	AND (SELECT COUNT(*) FROM [Base].[tEventQueue] WITH (UPDLOCK, READPAST) WHERE EventTypeID = @EventTypeID AND EventStatusTypeID = @CurrentEventStatusTypeID) >= 1
	BEGIN
	    --Select the VistCalcRun and lock the record to prevent it from being selected more than once
		SELECT TOP 1 @EventQueueID = EventQueueID
		FROM [Base].[tEventQueue] WITH (UPDLOCK, READPAST)
		WHERE EventTypeID = @EventTypeID AND EventStatusTypeID = @CurrentEventStatusTypeID
	
	    --Update the RunStatus and allow it to be processed
		UPDATE [Base].[tEventQueue]
		SET EventStatusTypeID = @NewEventStatusTypeID		
		WHERE EventQueueID = @EventQueueID
		
	END

END
;
GO
GRANT EXECUTE
    ON OBJECT::[Base].[spGetEventQueueItemAndRestatus] TO [TEUser]
    AS [dbo];

