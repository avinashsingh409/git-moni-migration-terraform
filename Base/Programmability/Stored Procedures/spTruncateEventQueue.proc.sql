﻿
CREATE PROCEDURE [Base].[spTruncateEventQueue]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CURRENT_TIME DateTime

	SET @CURRENT_TIME = GETDATE()

    DELETE Base.tEventObject
    FROM Base.tEventObject
		INNER JOIN Base.tEventQueue ON Base.tEventObject.EventID = Base.tEventQueue.EventID
		INNER JOIN Base.tEventType ON Base.tEventQueue.EventTypeID = Base.tEventType.EventTypeID
	WHERE Base.tEventQueue.ArchiveTime IS NOT NULL
		AND Base.tEventType.ArchiveExpiration_day IS NOT NULL
		AND @CURRENT_TIME > DATEADD("Day", Base.tEventType.ArchiveExpiration_day, Base.tEventQueue.ArchiveTime)

    DELETE Base.tEventQueue
    FROM Base.tEventQueue
		INNER JOIN Base.tEventType ON Base.tEventQueue.EventTypeID = Base.tEventType.EventTypeID
	WHERE Base.tEventQueue.ArchiveTime IS NOT NULL
		AND Base.tEventType.ArchiveExpiration_day IS NOT NULL
		AND @CURRENT_TIME > DATEADD("Day", Base.tEventType.ArchiveExpiration_day, Base.tEventQueue.ArchiveTime)
END
