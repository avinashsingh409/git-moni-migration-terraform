﻿
CREATE PROCEDURE [Base].[spDeleteCacheRecordsForScenarioID] 	
@ScenarioID int
AS
BEGIN
  DELETE FROM [Base].[tDataCache] WHERE DataCacheID IN (SELECT DataCacheID FROM [Base].[tDataCacheKeyValue] WHERE [Key]='ScenarioID' AND [Value]=@ScenarioID) 
END

