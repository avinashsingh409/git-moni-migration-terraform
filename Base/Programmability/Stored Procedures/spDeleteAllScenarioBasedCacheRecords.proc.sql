﻿
CREATE PROCEDURE [Base].[spDeleteAllScenarioBasedCacheRecords] 	
AS
BEGIN
  DELETE FROM [Base].[tDataCache] WHERE 
  dataType in ('EmissionsValueList','GetAllScenariosResponse','GetQtyDetailsUCDResponse','GetQtyDetailsVOMResponse','GetScenariosResponse','GetScenarioWithResultsResponse')
END

