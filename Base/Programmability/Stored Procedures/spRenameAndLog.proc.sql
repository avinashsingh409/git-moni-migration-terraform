﻿CREATE PROCEDURE [Base].[spRenameAndLog]
	@oldname	NVARCHAR(1035),		-- up to 4-part "old" name
	@name		SYSNAME,			-- one-part new name
	@type		VARCHAR(13) = null	-- identifying the name
AS
BEGIN
	SET NOCOUNT ON 
	SET ROWCOUNT 0
	
	BEGIN TRY
		INSERT INTO dbo.tTableChangeLog (DateOfChange, UserName, DDL)
		SELECT getdate(), system_user, 
		'EXEC sp_rename @objname=''' + @oldname + ''',@newname=''' + @name + ''',@objtype=''' + @type + ''';'
		
		EXEC sp_rename @oldname,@name,@type
	END TRY
	BEGIN CATCH
		IF @@trancount > 0
			ROLLBACK TRANSACTION

		DECLARE @ERROR_MESSAGE varchar(8000)
		SET @ERROR_MESSAGE = ERROR_MESSAGE()
		RAISERROR(@ERROR_MESSAGE,16,1)
	END CATCH
END
