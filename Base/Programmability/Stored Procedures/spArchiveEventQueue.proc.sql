﻿
CREATE PROCEDURE [Base].[spArchiveEventQueue]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CURRENT_TIME DateTime

	SET @CURRENT_TIME = GETDATE()

    UPDATE Base.tEventQueue SET ArchiveTime = @CURRENT_TIME
    FROM Base.tEventQueue
		INNER JOIN Base.tEventType ON Base.tEventQueue.EventTypeID = Base.tEventType.EventTypeID
	WHERE Base.tEventQueue.ArchiveTime IS NULL
		AND Base.tEventQueue.StopTime IS NOT NULL
		AND Base.tEventType.Archive = 1
		AND @CURRENT_TIME > DATEADD("Day", Base.tEventType.ArchiveDuration_day, Base.tEventQueue.StopTime)
END
