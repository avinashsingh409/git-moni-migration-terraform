CREATE TYPE [Base].[tpIntList] AS TABLE (
    [id] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC));


GO
GRANT EXECUTE
    ON TYPE::[Base].[tpIntList] TO [TEUser];

