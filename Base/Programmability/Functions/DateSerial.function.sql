﻿-- =============================================
-- Author:		Brian Schumacher
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [Base].[DateSerial] 
(
	-- Add the parameters for the function here
	@Year int,
	@Month int,
	@Day int,
	@Hour int,
	@Minute int,
	@Second int
)
RETURNS DateTime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result DateTime

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = DATEADD(YEAR, @Year-1900, 0)
	SELECT @Result = DATEADD(MONTH, @Month-1, @Result)
	SELECT @Result = DATEADD(DAY, @Day-1, @Result)
	SELECT @Result = DATEADD(HOUR, @Hour, @Result)
	SELECT @Result = DATEADD(MINUTE, @Minute, @Result)
	SELECT @Result = DATEADD(SECOND, @Second, @Result)

	-- Return the result of the function
	RETURN @Result

END

GO
GRANT EXECUTE
    ON OBJECT::[Base].[DateSerial] TO [TEUser]
    AS [dbo];

