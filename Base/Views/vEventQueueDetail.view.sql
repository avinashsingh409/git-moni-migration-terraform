﻿CREATE VIEW [Base].[vEventQueueDetail]
AS
SELECT a.EventQueueID, a.EventID, a.EventStatusTypeID, d.EventStatusTypeDesc, d.EventStatusTypeAbbrev,
a.StartTime, a.StopTime, a.ExpirationDate, a.RequestedCompletionDate, a.CreatedBy, a.CreateDate,
a.EventQueueDescription, a.EventTypeID, b.EventTypeDesc, b.EventTypeAbbrev,
b.Archive, b.ArchiveDuration_day, b.EventEndPointTypeID, a.EventStatusMessage,
c.EventEndPointTypeDesc, c.EventEndPointTypeAbbrev, b.EventEndPointConnection,
a.EventNotificationAddress
FROM Base.tEventQueue a INNER JOIN Base.tEventType b ON a.EventTypeID = b.EventTypeID
LEFT OUTER JOIN Base.tEventEndPointType c ON b.EventEndPointTypeID = c.EventEndPointTypeID
INNER JOIN Base.tEventStatusType d ON a.EventStatusTypeID = d.EventStatusTypeID
