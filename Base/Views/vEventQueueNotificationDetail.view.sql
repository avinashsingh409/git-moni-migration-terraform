﻿CREATE VIEW Base.vEventQueueNotificationDetail
AS
SELECT a.EventID, a.EventStatusTypeID, d.EventStatusTypeDesc, d.EventStatusTypeAbbrev,
a.StartTime, a.StopTime, a.CreatedBy, a.CreateDate,
a.EventQueueDescription, a.EventTypeID, b.EventTypeDesc, b.EventTypeAbbrev,
b.EventEndPointTypeID, c.EventEndPointTypeDesc, c.EventEndPointTypeAbbrev,
e.EventQueueNotificationEmail
FROM Base.tEventQueue a INNER JOIN Base.tEventQueueNotification e ON a.EventTypeID = e.EventTypeID AND a.EventStatusTypeID = e.EventStatusTypeID
INNER JOIN Base.tEventType b ON a.EventTypeID = b.EventTypeID
LEFT OUTER JOIN Base.tEventEndPointType c ON b.EventEndPointTypeID = c.EventEndPointTypeID
INNER JOIN Base.tEventStatusType d ON a.EventStatusTypeID = d.EventStatusTypeID

