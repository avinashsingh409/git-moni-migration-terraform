﻿CREATE VIEW Base.vEventQueueArchiveDetail
AS
SELECT a.EventStatusTypeID, d.EventStatusTypeDesc, d.EventStatusTypeAbbrev,
a.StartTime, a.StopTime, a.ExpirationDate, a.RequestedCompletionDate, a.CreatedBy, a.CreateDate,
a.EventQueueArchiveDescription, a.ArchiveDate, a.EventTypeID, b.EventTypeDesc, b.EventTypeAbbrev,
b.Archive, b.ArchiveDuration_day, b.EventEndPointTypeID, 
c.EventEndPointTypeDesc, c.EventEndPointTypeAbbrev, b.EventEndPointConnection
FROM Base.tEventQueueArchive a INNER JOIN Base.tEventType b ON a.EventTypeID = b.EventTypeID
LEFT OUTER JOIN Base.tEventEndPointType c ON b.EventEndPointTypeID = c.EventEndPointTypeID
INNER JOIN Base.tEventStatusType d ON a.EventStatusTypeID = d.EventStatusTypeID

