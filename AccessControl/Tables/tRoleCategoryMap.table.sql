CREATE TABLE [AccessControl].[tRoleCategoryMap](
	[RoleCategoryMapID] [int] IDENTITY(1,1) NOT NULL,
	[SecurityRoleID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,	
 CONSTRAINT [PK_tCategoryTypeMap] PRIMARY KEY CLUSTERED 
(
	[RoleCategoryMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tRoleCategoryMap_RoleCategoryMapID] UNIQUE NONCLUSTERED 
(	
	[SecurityRoleID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [AccessControl].[tRoleCategoryMap] ADD  
CONSTRAINT [FK_tRoleCategoryMap_CategoryID_tCategory] FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'AccessControl', @level1type=N'TABLE',@level1name=N'tRoleCategoryMap'
GO