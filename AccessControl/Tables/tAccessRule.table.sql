CREATE TABLE [AccessControl].[tAccessRule] (
    [SecurityPermissionID] INT IDENTITY (1, 1) NOT NULL,
    [SecurityResourceID]   INT NOT NULL,
    [SecurityRoleID]       INT NOT NULL,
    [SecurityAccessID]     INT NOT NULL,
    [Allowed]              BIT CONSTRAINT [DF_tSecurityPermission_Allowed] DEFAULT ((0)) NOT NULL,
	[CreatedBy]			   INT			 NOT NULL,
	[ChangedBy]            INT			 NOT NULL,
	[CreateDate]           DATETIME	 	 NOT NULL CONSTRAINT DF_tAccessRule_CreateDate DEFAULT GETDATE(),
	[ChangeDate]           DATETIME		 NOT NULL CONSTRAINT DF_tAccessRule_ChangeDate DEFAULT GETDATE(),
    CONSTRAINT [PK_tSecurityPermission] PRIMARY KEY CLUSTERED ([SecurityPermissionID] ASC),
	CONSTRAINT [FK_tAccessRule_ChangedBy_tUser_SecurityUserID] FOREIGN KEY([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [FK_tAccessRule_CreatedBy_tUser_SecurityUserID] FOREIGN KEY([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tAccessRule';

