CREATE TABLE [AccessControl].[tAssetAccessRule] (
    [AssetAccessRuleID] INT IDENTITY (1, 1) NOT NULL,
    [SecurityRoleID]    INT NOT NULL,
    [AssetID]           INT NULL,
    [Allowed]           BIT CONSTRAINT [DF_tAssetAccessRule_tSecurityPermission_Allowed] DEFAULT ((0)) NOT NULL,
	CreatedBy				   INT			 NOT NULL,
	ChangedBy                  INT			 NOT NULL,
	CreateDate                 DATETIME	 	 NOT NULL CONSTRAINT DF_tAssetAccessRule_CreateDate DEFAULT GETDATE(),
	ChangeDate                 DATETIME		 NOT NULL CONSTRAINT DF_tAssetAccessRule_ChangeDate DEFAULT GETDATE(),
    CONSTRAINT [PK_tAssetAccessRule] PRIMARY KEY CLUSTERED ([AssetAccessRuleID] ASC),
    CONSTRAINT [FK_AccessControl_tAssetAccessRule_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
	CONSTRAINT [FK_tAssetAccessRule_ChangedBy_tUser_SecurityUserID] FOREIGN KEY([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [FK_tAssetAccessRule_CreatedBy_tUser_SecurityUserID] FOREIGN KEY([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tAssetAccessRule';


GO

CREATE NONCLUSTERED INDEX [IDX_AssetAccessRule_RoleAssetAllowed] ON [AccessControl].[tAssetAccessRule]
(
	[SecurityRoleID] ASC,
	[AssetID] ASC,
	[Allowed] ASC
)

GO
