﻿CREATE TABLE [AccessControl].[tFocalRole] (
    [FocalRoleId]   INT NOT NULL,
    [RelatedRoleId] INT NOT NULL,
    [LevelsAway]    INT NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idxFocalRole]
    ON [AccessControl].[tFocalRole]([FocalRoleId] ASC, [LevelsAway] ASC);

