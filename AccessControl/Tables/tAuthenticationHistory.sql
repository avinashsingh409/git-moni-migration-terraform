﻿CREATE TABLE [AccessControl].[tAuthenticationHistory](
	[AuthenticationHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[Subject] NVARCHAR(100) NOT NULL,
	[AuthenticationTime] int NOT NULL,
	[Email] NVARCHAR(MAX) NOT NULL
 CONSTRAINT [PK_tAuthenticationHistory] PRIMARY KEY
(
	[AuthenticationHistoryID] ASC
)
)
