CREATE TABLE [AccessControl].[tCustomer] (
    [CustID]       INT              IDENTITY (1, 1) NOT NULL,
    [ParentCustID] INT              NULL,
    [CustomerID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tCustomer_CustomerID] DEFAULT (newid()) NOT NULL,
    [Name]         NVARCHAR (255)   NOT NULL,
    [CreatedBy]    INT              NOT NULL,
    [ChangedBy]    INT              NOT NULL,
    [CreateDate]   DATETIME         CONSTRAINT [DF_tCustomer_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]   DATETIME         CONSTRAINT [DF_tCustomer_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [Code]         NVARCHAR (6)     NULL,
    [Path]         NVARCHAR (72)    NULL,
    CONSTRAINT [PK_tCustomer] PRIMARY KEY CLUSTERED ([CustID] ASC),
    CONSTRAINT [FK_tUser_SecurityUserID_tCustomer_ChangedBy] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tUser_SecurityUserID_tCustomer_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCustomer_CustID_tCustomer_ParentCustID] FOREIGN KEY ([ParentCustID]) REFERENCES [AccessControl].[tCustomer] ([CustID]),
    CONSTRAINT [UK_tCustomer_CustomerID] UNIQUE NONCLUSTERED ([CustomerID] ASC),
	CONSTRAINT [UK_tCustomer_ParentCustID_Name] UNIQUE NONCLUSTERED (ParentCustID, [Name])
);
GO

CREATE TRIGGER [AccessControl].[CustomerInsert]
ON [AccessControl].[tCustomer]
AFTER INSERT
AS
BEGIN
   -- Trigger does not support UPDATE for now. More development work is needed to support reparenting of Customer in setting descendant's [Path] on UPDATE
   -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
   SET NOCOUNT ON;

   DECLARE @Code nvarchar(6);
   DECLARE @ParentPath nvarchar(72);
   SELECT @Code = Base.ufnNumberToBaseStr(inserted.CustID, 36) FROM inserted
   SELECT @ParentPath = AccessControl.[tCustomer].Path FROM AccessControl.[tCustomer] JOIN inserted ON AccessControl.[tCustomer].CustID = inserted.ParentCustID;

   UPDATE AccessControl.[tCustomer] SET Code=@Code, Path=ISNULL(@ParentPath, '/') + @Code + '/' FROM AccessControl.[tCustomer] t JOIN inserted i ON t.CustID = i.CustID;
END
GO

CREATE UNIQUE NONCLUSTERED INDEX IDX_tCustomer_Path ON AccessControl.tCustomer
	(
	[Path]
	)
	INCLUDE (CustomerID)
WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO