CREATE TABLE [AccessControl].[tAlias] (
    [AliasID]        INT           IDENTITY (1, 1) NOT NULL,
    [SecurityUserID] INT           NOT NULL,
    [Username]       NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tAlias] PRIMARY KEY CLUSTERED ([AliasID] ASC),
    CONSTRAINT [FK_AccessControl_tAlias_SecurityUserID_AccessControl_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tAlias';

