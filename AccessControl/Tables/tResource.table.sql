CREATE TABLE [AccessControl].[tResource] (
    [SecurityResourceID]       INT           IDENTITY (1, 1) NOT NULL,
    [ParentSecurityResourceID] INT           NOT NULL,
    [Left]                     INT           NOT NULL,
    [Right]                    INT           NOT NULL,
    [BeingMoved]               BIT           CONSTRAINT [DF_tSecurityResource_BeingMoved] DEFAULT ((0)) NOT NULL,
    [Text]                     NVARCHAR (50) NOT NULL,
    [IsReadOnly]               BIT           CONSTRAINT [DF_tResource_IsReadOnly] DEFAULT ((0)) NOT NULL,
    [IsDeleted]                BIT           CONSTRAINT [DF_tResource_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tSecurityResource] PRIMARY KEY CLUSTERED ([SecurityResourceID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tResource';


GO
--Add constraint to force the Text to be unique for a given ParentSecurityResourceID
ALTER TABLE AccessControl.tResource ADD CONSTRAINT
	UX_tResource_ParentID_Text UNIQUE NONCLUSTERED 
	(
	ParentSecurityResourceID,
	Text
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]