﻿CREATE TABLE [AccessControl].[tCustomerRoleMap] (
    [CustomerRoleMapID] INT              IDENTITY (1, 1) NOT NULL,
    [CustomerID]        UNIQUEIDENTIFIER NOT NULL,
    [SecurityRoleID]    INT              NOT NULL,
    [CreatedBy]         INT              NOT NULL,
    [ChangedBy]         INT              NOT NULL,
    [CreateDate]        DATETIME         CONSTRAINT [DF_tCustomerRoleMap_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]        DATETIME         CONSTRAINT [DF_tCustomerRoleMap_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tCustomerRoleMap] PRIMARY KEY CLUSTERED ([CustomerRoleMapID] ASC),
    CONSTRAINT [FK_tCustomerRoleMap_CustomerID_tUser_CustomerID] FOREIGN KEY ([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID]),
    CONSTRAINT [FK_tCustomerRoleMap_SecurityRoleID_tRole_SecurityRoleID] FOREIGN KEY ([SecurityRoleID]) REFERENCES [AccessControl].[tRole] ([SecurityRoleID]),
    CONSTRAINT [FK_tCustomerRoleMap_SecurityUserID_tUser_ChangedByUserID] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCustomerRoleMap_SecurityUserID_tUser_CreatedByUserID] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tCustomerRoleMap_CustomerID_SecurityRoleID] UNIQUE NONCLUSTERED ([CustomerID] ASC, [SecurityRoleID] ASC)
);
GO

