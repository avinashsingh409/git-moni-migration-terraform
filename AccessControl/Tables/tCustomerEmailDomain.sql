CREATE TABLE [AccessControl].[tCustomerEmailDomain] (
    ID int NOT NULL IDENTITY (1, 1),
    CustomerID uniqueidentifier NOT NULL,
	EmailDomain nvarchar(128) NOT NULL,
    CONSTRAINT [PK_tCustomerEmailDomain] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_tCustomer_CustomerID_tCustomerEmailDomain_CustomerID] FOREIGN KEY([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID])
);






GO

