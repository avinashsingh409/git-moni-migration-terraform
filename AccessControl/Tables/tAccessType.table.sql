CREATE TABLE [AccessControl].[tAccessType] (
    [SecurityAccessID]      INT           IDENTITY (1, 1) NOT NULL,
    [SecurityAccessGroupID] INT           NOT NULL,
    [Description]           NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tSecurityAccess] PRIMARY KEY CLUSTERED ([SecurityAccessID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tAccessType';

