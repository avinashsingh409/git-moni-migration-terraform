CREATE TABLE [AccessControl].[tAccessGroup] (
    [SecurityAccessGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]           NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tSecurityAccessGroup] PRIMARY KEY CLUSTERED ([SecurityAccessGroupID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tAccessGroup';

