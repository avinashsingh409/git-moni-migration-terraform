﻿CREATE TABLE [AccessControl].[tRole] (
    [SecurityRoleID]       INT           IDENTITY (1, 1) NOT NULL,
    [ParentSecurityRoleID] INT           NOT NULL,
    [Left]                 INT           NOT NULL,
    [Right]                INT           NOT NULL,
    [BeingMoved]           BIT           CONSTRAINT [DF_tSecurityRole_BeingMoved] DEFAULT ((0)) NOT NULL,
    [Text]                 NVARCHAR (50) NOT NULL,
    [IsReadOnly]           BIT           CONSTRAINT [DF_tRole_IsReadOnly] DEFAULT ((0)) NOT NULL,
    [IsDeleted]            BIT           CONSTRAINT [DF_tRole_IsDeleted] DEFAULT ((0)) NOT NULL,
	[CustomerID]		   UNIQUEIDENTIFIER NOT NULL,
	[CreatedBy]            INT			 NOT NULL,
	[ChangedBy]            INT			 NOT NULL,
	[CreateDate]           DATETIME	 	 NOT NULL CONSTRAINT DF_tRole_CreateDate DEFAULT getdate(),
	[ChangeDate]           DATETIME		 NOT NULL CONSTRAINT DF_tRole_ChangeDate DEFAULT getdate(),
    CONSTRAINT [PK_tSecurityRole] PRIMARY KEY CLUSTERED ([SecurityRoleID] ASC),
	CONSTRAINT [FK_tRole_CustomerID_tCustomer_CustomerID] FOREIGN KEY(CustomerID) REFERENCES [AccessControl].[tCustomer] ([CustomerID]),
	CONSTRAINT [FK_tRole_ChangedBy_tUser_SecurityUserID] FOREIGN KEY([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [FK_tRole_CreatedBy_tUser_SecurityUserID] FOREIGN KEY([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [UK_tRole_CustomerID_Text] UNIQUE NONCLUSTERED (CustomerID, [Text])
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tRole';


GO
CREATE TRIGGER [AccessControl].[RoleInsertUpdate]
   ON  [AccessControl].[tRole]
   FOR INSERT,UPDATE,DELETE
AS
BEGIN

BEGIN TRANSACTION 

BEGIN TRY
	 DECLARE @recalculate Base.tpIntList

	 INSERT INTO @recalculate
		  SELECT i.SecurityRoleID
		  FROM INSERTED i
		  INNER JOIN DELETED d ON i.SecurityRoleID = d.SecurityRoleID
		  WHERE i.ParentSecurityRoleID <> d.ParentSecurityRoleID

 	 INSERT INTO @recalculate
		  SELECT i.SecurityRoleID
		  FROM INSERTED i
		  LEFT JOIN DELETED d ON i.SecurityRoleID = d.SecurityRoleID
		  WHERE d.SecurityRoleID IS NULL

	 INSERT INTO @recalculate
		  SELECT d.SecurityRoleID
		  FROM DELETED d 
		  LEFT JOIN INSERTED i ON i.SecurityRoleID = d.SecurityRoleID
		  WHERE i.SecurityRoleID IS NULL

	 EXEC [AccessControl].[spCalculateFocalRole] @recalculate

	 COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
	PRINT N'Trigger Failed, rolling back';
	PRINT ERROR_MESSAGE();
END CATCH;

END