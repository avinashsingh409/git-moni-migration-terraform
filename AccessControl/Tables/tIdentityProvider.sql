﻿Create Table [AccessControl].[tIdentityProvider] (
	[ID] int Identity(1, 1) not null,
	[CustomerID] uniqueidentifier not null,
	[IdentityProvider] nvarchar(50) not null,
	[Enabled] bit CONSTRAINT [DF_tIdentityProvider_Enabled] default 1 not null,
	[TokenID] uniqueidentifier null,
	[DefaultRoleIDs] nvarchar(50) null, -- Comma separated role IDs. These will be moved into a table once the schema for tenants stabilizes.
	CONSTRAINT [UK_tIdentityProvider_CustomerID] UNIQUE NONCLUSTERED ([CustomerID] ASC),
	CONSTRAINT [FK_tCustomer_CustomerID_tIdentityProvider_CustomerID] FOREIGN KEY([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID]),
	CONSTRAINT [FK_tScimTokenRegistry_TokenID_tIdentityProvider_TokenID] FOREIGN KEY([TokenID]) REFERENCES [AccessControl].[tScimTokenRegistry] ([TokenID]),
	CONSTRAINT [PK_tIdentityProvider_ID] PRIMARY KEY CLUSTERED ([ID] ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO