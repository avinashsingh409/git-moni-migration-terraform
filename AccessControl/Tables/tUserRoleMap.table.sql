﻿CREATE TABLE [AccessControl].[tUserRoleMap] (
    [SecurityUserID] INT NOT NULL,
    [SecurityRoleID] INT NOT NULL,
	[CreatedBy]            INT			 NOT NULL,
	[ChangedBy]            INT			 NOT NULL,
	[CreateDate]           DATETIME	 	 NOT NULL CONSTRAINT DF_tUserRoleMap_CreateDate DEFAULT GETDATE(),
	[ChangeDate]           DATETIME		 NOT NULL CONSTRAINT DF_tUserRoleMap_ChangeDate DEFAULT GETDATE(),
    CONSTRAINT [PK_tSecurityUserRole] PRIMARY KEY CLUSTERED ([SecurityUserID] ASC, [SecurityRoleID] ASC),
    CONSTRAINT [FK_tUserRoleMap_tRole] FOREIGN KEY ([SecurityRoleID]) REFERENCES [AccessControl].[tRole] ([SecurityRoleID]),
    CONSTRAINT [FK_tUserRoleMap_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE,
	CONSTRAINT [FK_tUserRoleMap_ChangedBy_tUser_SecurityUserID] FOREIGN KEY([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [FK_tUserRoleMap_CreatedBy_tUser_SecurityUserID] FOREIGN KEY([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
);










GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AccessControl', @level1type = N'TABLE', @level1name = N'tUserRoleMap';

