﻿CREATE TABLE [AccessControl].[tCustomerTopAssetMap]
(
	[CustomerTopAssetMapID] INT NOT NULL IDENTITY (1, 1),
    [CustomerID] UNIQUEIDENTIFIER NOT NULL,
    [AssetID] INT NOT NULL,
    [CreatedBy] INT NOT NULL,
    [ChangedBy] INT NOT NULL,
    [CreateDate] DATETIME CONSTRAINT [DF_tCustomerTopAssetMap_CreateDate] DEFAULT (GETDATE()) NOT NULL,
    [ChangeDate] DATETIME CONSTRAINT [DF_tCustomerTopAssetMap_ChangeDate] DEFAULT (GETDATE()) NOT NULL,
	CONSTRAINT [PK_tCustomerTopAssetMap] PRIMARY KEY CLUSTERED ([CustomerTopAssetMapID] ASC),
    CONSTRAINT [FK_tCustomerTopAssetMap_CustomerID_tCustomer_CustomerID] FOREIGN KEY ([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID]),    
    CONSTRAINT [FK_tCustomerTopAssetMap_AssetID_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tCustomerTopAssetMap_CreatedBy_tUser_SecurityUserID] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCustomerTopAssetMap_ChangedBy_tUser_SecurityUserID] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tCustomerTopAssetMap_CustomerID_AssetID] UNIQUE NONCLUSTERED (CustomerID, [AssetID])
)
