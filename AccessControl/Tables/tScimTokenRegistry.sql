﻿Create Table [AccessControl].[tScimTokenRegistry] (
	[ID] int Identity(1, 1) not null,
	[CustomerID] uniqueidentifier not null,
	[TokenID] uniqueidentifier not null,
	[StartDate] datetime not null,
	[EndDate] datetime not null,
	[CreatedBy] int not null,
	[CreateDate] datetime CONSTRAINT [DF_tScimTokenRegistry_CreateDate] default (getdate()) not null,
	CONSTRAINT [PK_tScimTokenRegistry_ID] PRIMARY KEY CLUSTERED ([ID] ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	CONSTRAINT [FK_tCustomer_CustomerID_tScimTokenRegistry_CustomerID] FOREIGN KEY([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID]),
	CONSTRAINT [UK_tScimTokenRegistry_TokenID] UNIQUE NONCLUSTERED ([TokenID] ASC)
) ON [PRIMARY]
GO