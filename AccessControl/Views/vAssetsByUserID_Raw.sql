﻿CREATE VIEW [AccessControl].[vAssetsByUserID_Raw]
AS

SELECT DISTINCT SecurityUserID, AssetID FROM AccessControl.tUserRoleMap  CROSS APPLY [AccessControl].[ufnGetAssetIdsAllowedForRole] (
   SecurityRoleID)
   
except
-- exclude any asset that has been explicitly disallowed
SELECT DISTINCT TOP 2000000 SecurityUserID, AssetID FROM AccessControl.tUserRoleMap a
JOIN (
SELECT b.assetid,c.securityroleid
            FROM AccessControl.tAssetAccessRule AS a                    
             INNER JOIN AccessControl.tRole AS c ON a.SecurityRoleID = c.SecurityRoleID 
             CROSS APPLY Asset.ufnGetAssetTreeBranch(a.assetid) b             
             where allowed=0) b ON a.securityroleid=b.securityroleid