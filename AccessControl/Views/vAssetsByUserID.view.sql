﻿
CREATE VIEW AccessControl.vAssetsByUserID AS

-- first get all internal nodes
select SecurityUserID,AssetID from (
select distinct a.SecurityUserID,c.AssetID,c.IsLeafNode from AccessControl.tUserRoleMap a 
join AccessControl.tAssetAccessRule b on a.SecurityRoleID=b.SecurityRoleID
cross apply [Asset].[ufnGetAssetTreeBranchToUnit](b.assetid) c where 
b.allowed=1 
EXCEPT

SELECT DISTINCT TOP 2000000 SecurityUserID, AssetID,IsLeafNode FROM AccessControl.tUserRoleMap a
JOIN (
SELECT b.assetid,c.securityroleid,b.IsLeafNode
            FROM AccessControl.tAssetAccessRule AS a                    
             INNER JOIN AccessControl.tRole AS c ON a.SecurityRoleID = c.SecurityRoleID 
             CROSS APPLY [Asset].[ufnGetAssetTreeBranchToUnit](a.assetid) b                                    
             where allowed=0) b ON a.securityroleid=b.securityroleid) a where a.IsLeafNode=0

UNION 

--now get leaf nodes and their children             

select SecurityUserID,b.AssetID from (
select distinct a.SecurityUserID,c.AssetID,c.IsLeafNode from AccessControl.tUserRoleMap a 
join AccessControl.tAssetAccessRule b on a.SecurityRoleID=b.SecurityRoleID
cross apply [Asset].[ufnGetAssetTreeBranchToUnit](b.assetid) c where 
b.allowed=1 
EXCEPT

SELECT DISTINCT TOP 2000000 SecurityUserID, AssetID,IsLeafNode FROM AccessControl.tUserRoleMap a
JOIN (
SELECT b.assetid,c.securityroleid,b.IsLeafNode
            FROM AccessControl.tAssetAccessRule AS a                    
             INNER JOIN AccessControl.tRole AS c ON a.SecurityRoleID = c.SecurityRoleID 
             CROSS APPLY [Asset].[ufnGetAssetTreeBranchToUnit](a.assetid) b                                    
             where allowed=0) b ON a.securityroleid=b.securityroleid) a 
             CROSS APPLY Asset.ufnGetAssetTreeBranch(a.AssetID) b
             where a.IsLeafNode=1
