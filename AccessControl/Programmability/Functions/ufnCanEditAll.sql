﻿
CREATE FUNCTION [AccessControl].[ufnCanEditAll]
(	
	@securityUserId as int
)
RETURNS int
BEGIN
declare @result int = 0

IF EXISTS (
		SELECT SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID AND IsBVEmployee = 1		
		)
	BEGIN
	SET @result =  1;
	END
ELSE 
  BEGIN
  declare @parentResourceID as int
      select @parentResourceID = SecurityResourceID from AccessControl.tResource where [Text] = 'GUI'
      if @parentResourceID is not null
         begin
         declare @cnt as int
         select @cnt = COUNT(*) from AccessControl.tUserRoleMap a join AccessControl.tAccessRule b on a.SecurityRoleID = b.SecurityRoleID 
                join AccessControl.tResource c on b.SecurityResourceID = c.SecurityResourceID where c.ParentSecurityResourceID = @parentResourceID and c.[Text] = 'CanEditAll' and 
	             a.SecurityUserID = @securityUserID
         if @cnt>0 
           begin
	       set @result = 1
	       end           
         end	  
  END
return @result
END

GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[ufnCanEditAll] TO [TEUser]
    AS [dbo];

