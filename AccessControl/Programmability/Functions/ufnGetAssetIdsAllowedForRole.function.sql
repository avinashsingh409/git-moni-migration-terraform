﻿CREATE FUNCTION [AccessControl].[ufnGetAssetIdsAllowedForRole]
(
   @roleid int
)
RETURNS TABLE
AS
RETURN
(
	SELECT hop.EndingAssetId as AssetID
	FROM AccessControl.tRole tfr
	INNER JOIN AccessControl.tAssetAccessRule taar ON taar.securityroleid = tfr.SecurityRoleID
	INNER JOIN Asset.tAssetHop hop ON hop.HopAssetId = taar.AssetID JOIN Asset.tAsset a on hop.EndingAssetId = a.AssetID
	WHERE tfr.SecurityRoleID=@roleId and a.IsHidden = 0
	GROUP BY hop.EndingAssetId 
	HAVING MIN(CAST(taar.Allowed as INT)) > 0	             
)

GO

GRANT SELECT
    ON OBJECT::[Accesscontrol].[ufnGetAssetIdsAllowedForRole] TO [TEUser]
    AS [dbo];

GO

