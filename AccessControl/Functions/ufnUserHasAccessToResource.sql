﻿CREATE FUNCTION [AccessControl].[ufnUserHasAccessToResource]
(
	@securityUserID int,
	@securityResourceID int
)
RETURNS BIT
AS
BEGIN

DECLARE @allowed BIT = NULL

SELECT @allowed = MIN(CONVERT(INT, r.Allowed)) FROM 
AccessControl.tAccessRule r
INNER JOIN AccessControl.tUserRoleMap map ON r.SecurityRoleID = map.SecurityRoleID
WHERE r.SecurityResourceID = @securityResourceID AND map.SecurityUserID = @securityUserID
GROUP BY r.SecurityResourceID, r.SecurityResourceID

RETURN @allowed 

END
GO 

GRANT EXECUTE
    ON OBJECT::[AccessControl].[ufnUserHasAccessToResource] TO [TEUser]
    AS [dbo];