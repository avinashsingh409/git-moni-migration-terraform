﻿CREATE FUNCTION [AccessControl].[ufnGetRoleSecuritizedCategoriesFromUser] 
(	
	-- Add the parameters for the function here
	@securityUserID int,
	@filterCategoryTypeID int = NULL
)
RETURNS TABLE AS RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		DISTINCT(tRCM.CategoryID)
	FROM
		AccessControl.tRoleCategoryMap tRCM
	INNER JOIN AccessControl.tUserRoleMap tURM
		ON tRCM.SecurityRoleID = tURM.SecurityRoleID
	INNER JOIN Asset.tCategoryTypeMap tCTM
		ON tRCM.CategoryID = tCTM.CategoryID
	WHERE
		tURM.SecurityUserID = @securityUserID
		AND
		(@filterCategoryTypeID IS NULL OR tCTM.CategoryTypeID = @filterCategoryTypeID)
	UNION
	SELECT
		DISTINCT(tC.CategoryID)
	FROM
		Asset.tCategory tC
	INNER JOIN Asset.tCategoryTypeMap tCTM
		ON tC.CategoryID = tCTM.CategoryID
	LEFT JOIN AccessControl.tRoleCategoryMap tRCM
		ON tC.CategoryID = tRCM.CategoryID
	WHERE
		tRCM.CategoryID IS NULL
		AND
		(@filterCategoryTypeID IS NULL OR tCTM.CategoryTypeID = @filterCategoryTypeID)
)
GO

GRANT SELECT
    ON OBJECT::[AccessControl].[ufnGetRoleSecuritizedCategoriesFromUser] TO [TEUser]
    AS [dbo]
GO