﻿CREATE FUNCTION [AccessControl].[ufnGetRoleSecuritizedFilteredCategoriesFromUser] 
(	
	-- Add the parameters for the function here
	@securityUserID INT,
	@filterCategoryIDs Base.tpIntList READONLY,
	@filterCategoryTypeID INT = NULL
)
RETURNS TABLE AS RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		roleSecuritizedCategories.CategoryID
	FROM
		AccessControl.ufnGetRoleSecuritizedCategoriesFromUser(@securityUserID, @filterCategoryTypeID) roleSecuritizedCategories 
	LEFT JOIN @filterCategoryIDs filterCategoryIDs
		ON roleSecuritizedCategories.CategoryID = filterCategoryIDs.id
	WHERE
	(
		(EXISTS(SELECT * FROM @filterCategoryIDs) AND filterCategoryIDs.id IS NOT NULL) 
		OR
		(NOT EXISTS(SELECT * FROM @filterCategoryIDs) AND filterCategoryIDs.id IS NULL)
	)
)
GO

GRANT SELECT
    ON OBJECT::[AccessControl].[ufnGetRoleSecuritizedFilteredCategoriesFromUser] TO [TEUser]
    AS [dbo]
GO
