﻿CREATE FUNCTION [AccessControl].[ufnGetUsersForBranch]
(
   @assetID int
)
RETURNS @users TABLE
(
  SecurityUserID int PRIMARY KEY not NULL
)
AS
BEGIN

declare @roles as table
( 
securityRoleID int
)

insert into @roles select SecurityRoleID from AccessControl.tAssetAccessRule a CROSS APPLY Asset.ufnAssetAncestors(a.AssetID) b where b.AssetID = @assetID and a.Allowed = 1
insert into @roles select SecurityRoleID from AccessControl.tAssetAccessRule a where a.AssetID = @assetID and a.Allowed = 1

insert into @users select distinct a.SecurityUserID from AccessControl.tUser a join AccessControl.tUserRoleMap b on a.SecurityUserID = b.SecurityUserID 
join @roles c on b.SecurityRoleID = c.securityRoleID

RETURN
END


GO

GRANT SELECT
    ON OBJECT::[AccessControl].[ufnGetUsersForBranch] TO [TEUser]
    AS [dbo];
