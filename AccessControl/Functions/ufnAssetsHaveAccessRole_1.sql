﻿create function [AccessControl].[ufnAssetsHaveAccessRole] (
	@UserID int,
	@AssetIDs Base.tpIntList readonly,
	@Allowed int
	)
returns @AssetAccessRoles table
(
	AssetID int primary key not null,
	HasAccessRole bit not null
)
as
begin

	insert into @AssetAccessRoles (AssetID, HasAccessRole)
	select
		a.id,
		case when count (c.SecurityRoleID) > 0 then 1 else 0 end
	from @AssetIDs a
	join Asset.ufnUserHasAccessToAssets(@UserID, @AssetIDs) s on a.id = s.AssetID
	join AccessControl.tAssetAccessRule c on a.id = c.AssetID
	where c.Allowed = @Allowed
	group by a.id;

	return;
end
GO
GRANT SELECT
    ON OBJECT::[AccessControl].[ufnAssetsHaveAccessRole] TO [TEUser]
    AS [dbo];

