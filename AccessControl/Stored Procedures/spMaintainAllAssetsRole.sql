﻿
CREATE PROCEDURE [AccessControl].[spMaintainAllAssetsRole]
	@editingUserID int,
	@customerID uniqueidentifier
AS
BEGIN
	declare @rootRoleID int = null;
	select @rootRoleID = SecurityRoleID
	from AccessControl.tRole
	where Text = 'Root'
	and ParentSecurityRoleID = 0;
	
	if @rootRoleID > 0
	BEGIN 

		declare @clientsRoleID int = null;
		declare @clientsRoleText nvarchar(50) = 'Clients'
		select @clientsRoleID = SecurityRoleID
		From AccessControl.tRole
		where text = @clientsRoleText
		and ParentSecurityRoleID = @rootRoleID; 

		if @clientsRoleID is null
		BEGIN
			exec AccessControl.spInsertRole @rootRoleID, @clientsRoleText, @editingUserID, @customerID;
			select @clientsRoleID = SecurityRoleID
			from AccessControl.tRole
			where [Text] = @clientsRoleText
			and ParentSecurityRoleID = @rootRoleID;
		END
		
		declare @allAssetsRoleID int = null;
		declare @allAssetsRoleText nvarchar(50) = 'All Assets';
		select @allAssetsRoleID = SecurityRoleID
		FROM AccessControl.tRole
		where text = @allAssetsRoleText
		and ParentSecurityRoleID = @clientsRoleID;

		if(@allAssetsRoleID is null)
		BEGIN
			exec AccessControl.spInsertRole @clientsRoleID, @allAssetsRoleText, @editingUserID, @customerID;
			select @allAssetsRoleID = SecurityRoleID
			from AccessControl.tRole
			where [Text] = @allAssetsRoleText
			and ParentSecurityRoleID = @clientsRoleID;
		END
	
		MERGE INTO [AccessControl].[tAssetAccessRule] as [TARGET]
			USING(
			 SELECT @allAssetsRoleID as SecurityRoleID,
					AssetID,
					1 as Allowed
			 FROM	Asset.tAsset
			 WHERE	ParentAssetID is null
			 AND	IsHidden = 0
			) as [SOURCE]
			ON [Target].SecurityRoleID = [Source].SecurityRoleID AND [Target].AssetID = [Source].AssetID
			WHEN MATCHED THEN
				Update SET [Target].Allowed = [Source].Allowed,
						   [Target].ChangedBy = @editingUserID,
						   [Target].ChangeDate = GetDate()
			WHEN NOT MATCHED THEN
				INSERT (SecurityRoleID, AssetID, Allowed, CreatedBy, ChangedBy)
				VALUES (Source.SecurityRoleID, Source.AssetID, Source.Allowed, @editingUserID, @editingUserID)
		;


	END
	RETURN 
END

GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spMaintainAllAssetsRole] TO [TEUser]
    AS [dbo];

