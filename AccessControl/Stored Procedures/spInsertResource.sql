﻿--sp to add to AccessControl.tResource
CREATE PROCEDURE [AccessControl].[spInsertResource]
	--Input parameters
	@parentID int,
	@resourceName nvarchar(50)
AS
BEGIN

	DECLARE @tranName VARCHAR(100);
	SELECT @tranName = 'Add ' + @resourceName + ' to Parent Node ' + CONVERT(VARCHAR(10),@parentID) + ' in tResource.';

	BEGIN TRANSACTION @tranName;

	BEGIN TRY
		DECLARE @result int
		DECLARE @left int
		DECLARE @nodeExists int
		
		IF (@parentID = 0)
		BEGIN
			--adding a new root node
			--check to see if there are any root nodes yet
			SELECT @nodeExists = (SELECT COUNT(SecurityResourceID) FROM AccessControl.tResource WHERE ParentSecurityResourceID = @parentID)
			IF (@nodeExists > 0)
			BEGIN
				--find the left value for a new root node
				SELECT @left = (SELECT MAX([Right]) FROM AccessControl.tResource WHERE ParentSecurityResourceID = 0) + 1
			END
			ELSE
			BEGIN
				--first node on the tree will start with 1
				SELECT @left = 1
			END
		END
		ELSE
		BEGIN
			--cannot add a child if the parent ID is missing
			SELECT @nodeExists = (SELECT COUNT(SecurityResourceID) FROM AccessControl.tResource WHERE SecurityResourceID = @parentID)
			IF (@nodeExists > 0)
			BEGIN
				--adding a child node
				--find the right value for this new entry
				SELECT @left = (SELECT [Right] FROM AccessControl.tResource WHERE SecurityResourceID = @parentID)
				
				--update all of the Left values
				UPDATE AccessControl.tResource SET [Left] = [Left] + 2 WHERE [Left] > @left

				--update all of the Right values
				UPDATE AccessControl.tResource SET [Right] = [Right] + 2 WHERE [Right] >= @left
			END
		END

		IF (@nodeExists > 0 OR @parentID = 0)
		BEGIN
			--insert the new node
			INSERT INTO AccessControl.tResource ([ParentSecurityResourceID], [Left], [Right], [BeingMoved], [Text])
			 VALUES (@parentID, @left, @left + 1, 0, @resourceName)
			 SET @result = @@IDENTITY
		END
		ELSE
		BEGIN
			--message to correct the parentID
			RAISERROR ('Unable to add a child node because the ParentID cannot be found in the table.', 10, 1);
		END
		COMMIT TRANSACTION @tranName;

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION @tranName;
		PRINT N'Script Failed, rolling back';
		PRINT ERROR_MESSAGE();
	END CATCH;
	RETURN @result;
END
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spInsertResource] TO [TEUser]
    AS [dbo];

