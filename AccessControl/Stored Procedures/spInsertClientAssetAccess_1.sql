﻿


create procedure [AccessControl].[spInsertClientAssetAccess]
	@UserID int,
	@AssetID int,
	@Allowed int = 1,
	@AssetTypeConstraint int,
	@EditingUserID int,
	@ErrorMsg varchar(255) output
as
begin

declare @TranName varchar(max) = 'Add access to ' + convert(varchar(10), @AssetID);
declare @ClientAccessRoleId int;
declare @ParentRoleId int;
declare @RoleText nvarchar(50);
declare @CustomerID uniqueidentifier;
begin transaction @TranName
begin try

	set @ErrorMsg = N'';

	-- ensure user has rights to do this
	declare @UserAdminResourceID int = (select top 1 SecurityResourceID from AccessControl.tResource where [Text] = 'UserAdmin');
	declare @Users Base.tpIntList;
	insert into @Users (id)

	-- this is meant to substitute for AccessControlRepository.AccessAllowed
	select distinct m.SecurityUserID from AccessControl.tResource e
	join AccessControl.tAccessRule u on e.SecurityResourceID = u.SecurityResourceID
	join AccessControl.tRole r on u.SecurityRoleID = r.SecurityRoleID
	join AccessControl.tUserRoleMap m on r.SecurityRoleID = m.SecurityRoleID
	where e.SecurityResourceID = @UserAdminResourceID and u.Allowed = 1 and m.SecurityUserID = @UserID;

	if not exists(select id from @Users where id = @UserID)
	begin
		set @ErrorMsg = 'User ' + convert(varchar(10), @UserID) + ' does not have Client Asset Access permissions.';
		raiserror (@ErrorMsg, 13, -1, -1);
	end

	-- ensure user has access to asset
	if Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @UserID, -1) = 0
	begin
		set @ErrorMsg = 'User ' + convert(varchar(10), @UserID) + ' does not have access to asset ' + convert(varchar(10), @AssetID) + '.';
		raiserror (@ErrorMsg, 13, -1, -1);
	end

	-- enforce asset type constraint
	declare @AssetType int = -1;
	select @AssetType = c.AssetTypeID
	from Asset.tAsset a
	join Asset.tAssetClassType c on a.AssetClassTypeID = c.AssetClassTypeID
	where a.AssetID = @AssetID;

	if @AssetType <> @AssetTypeConstraint
	begin
		set @ErrorMsg = 'Asset ' + convert(varchar(10), @AssetID) + ' is type ' + convert(varchar(10), @AssetType) + ' which is not equal to constraint ' + convert(varchar(10), @AssetTypeConstraint) + '.';
		raiserror (@ErrorMsg, 13, -1, -1);
	end

	-- ensure that an existing client access role isn't present
	declare @existingRole bit;
	declare @Assets Base.tpIntList;
	insert into @Assets (id) select @AssetID;
	select @existingRole = HasAccessRole from AccessControl.ufnAssetsHaveAccessRole(@UserID, @Assets, @Allowed) where AssetID = @AssetID;
	if @existingRole = 1
	begin
		set @ErrorMsg = 'Allowed=' + convert(varchar(10), @Allowed) + ' client access rule already exists for asset ' + convert(varchar(10), @AssetID) + '.';
		raiserror (@ErrorMsg, 13, -1, 01);
	end

	-- ensure that the parent role exists
	declare @RootRoleText nvarchar(50) = 'Clients';
	select @ParentRoleId = SecurityRoleID from AccessControl.tRole where [Text] = @RootRoleText;
	if @ParentRoleId is null
	begin
		set @ErrorMsg = 'Parent role does not exist for asset ' + convert(varchar(10), @AssetID) + '.';
		raiserror (@ErrorMsg, 13, -1, 01);
	end

	-- get the new role's text
	select @RoleText = AssetAbbrev from Asset.tAsset where AssetID = @AssetID;

	-- get customer the user is associated with
	select @CustomerID = CustomerID from AccessControl.tUser where SecurityUserID = @UserID;
    if @CustomerID is null
    begin
		set @ErrorMsg = 'User is not associated with a customer ' + convert(varchar(10), @UserID) + '.';
		raiserror (@ErrorMsg, 13, -1, -1);
    end

	-- insert new role
	execute AccessControl.spInsertRole @ParentRoleID, @RoleText, @EditingUserID, @CustomerID;
	select @ClientAccessRoleId = SecurityRoleID from AccessControl.tRole where [Text] = @RoleText;

	-- insert asset access rule
	insert into AccessControl.tAssetAccessRule (SecurityRoleID, AssetID, Allowed, CreatedBy, ChangedBy)
	values (@ClientAccessRoleId, @AssetID, @Allowed, @EditingUserID, @EditingUserID);

	-- confirm asset access rule
	declare @AccessRuleCount int;
	select @AccessRuleCount = count (*) from AccessControl.tAssetAccessRule
	where SecurityRoleID = @ClientAccessRoleId and AssetID = @AssetID and Allowed = @Allowed;

	if @AccessRuleCount <> 1
	begin
		set @ErrorMsg = 'Actual outcome ' + convert(varchar(10), @AccessRuleCount) + ' does not match expectation 1.';
		raiserror (@ErrorMsg, 13, -1, -1);
	end

	commit transaction
end try
begin catch
	rollback transaction @TranName;
	print N'Script Failed, rolling back';
	print error_message();
end catch;

return @ClientAccessRoleId;
end
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spInsertClientAssetAccess] TO [TEUser]
    AS [dbo];

