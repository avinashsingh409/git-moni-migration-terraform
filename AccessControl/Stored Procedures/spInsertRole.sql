﻿
CREATE PROCEDURE [AccessControl].[spInsertRole]
	--Input parameters
	@parentID int,
	@roleName nvarchar(50),
	@editingUserID int,
	@customerID uniqueidentifier
AS
BEGIN

	DECLARE @tranName VARCHAR(100);
	SELECT @tranName = 'Add ' + @roleName + ' to Parent Node ' + CONVERT(VARCHAR(10),@parentID) + ' in trole.';

	BEGIN TRANSACTION @tranName;

	BEGIN TRY
		DECLARE @newRoleID int = -1
		IF EXISTS (SELECT * FROM AccessControl.tRole WHERE [Text] = @roleName AND CustomerID = @customerID)
		BEGIN
			RAISERROR('Unable to insert new role. Role already existed for the customer', 16, 1)
		END

		DECLARE @left int
		DECLARE @nodeExists int
		
		IF (@parentID = 0)
		BEGIN
			--adding a new root node
			--check to see if there are any root nodes yet
			SELECT @nodeExists = (SELECT COUNT(SecurityroleID) FROM AccessControl.trole WHERE ParentSecurityroleID = @parentID)
			IF (@nodeExists > 0)
			BEGIN
				--find the left value for a new root node
				SELECT @left = (SELECT MAX([Right]) FROM AccessControl.trole WHERE ParentSecurityroleID = 0) + 1
			END
			ELSE
			BEGIN
				--first node on the tree will start with 1
				SELECT @left = 1
			END
		END
		ELSE
		BEGIN
			--cannot add a child if the parent ID is missing
			SELECT @nodeExists = (SELECT COUNT(SecurityroleID) FROM AccessControl.trole WHERE SecurityroleID = @parentID)
			IF (@nodeExists > 0)
			BEGIN
				--adding a child node
				--find the right value for this new entry
				SELECT @left = (SELECT [Right] FROM AccessControl.trole WHERE SecurityroleID = @parentID)
				
				--update all of the Left values
				UPDATE AccessControl.trole SET [Left] = [Left] + 2, ChangedBy = @editingUserID, ChangeDate = GetDate()  WHERE [Left] > @left

				--update all of the Right values
				UPDATE AccessControl.trole SET [Right] = [Right] + 2, ChangedBy = @editingUserID, ChangeDate = GetDate() WHERE [Right] >= @left
			END
		END

		IF (@nodeExists > 0 OR @parentID = 0)
		BEGIN
			--insert the new node
			INSERT INTO AccessControl.trole ([ParentSecurityroleID], [Left], [Right], [BeingMoved], [Text], CustomerID, CreatedBy,ChangedBy)
			 VALUES (@parentID, @left, @left + 1, 0, @roleName, @customerID, @editingUserID, @editingUserID)
			SET @newRoleID = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			--message to correct the parentID
			RAISERROR ('Unable to add a child node because the ParentID cannot be found in the table.', 10, 1);
		END
		COMMIT TRANSACTION @tranName;

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION @tranName;
		PRINT N'Script Failed, rolling back';
		PRINT ERROR_MESSAGE();
	END CATCH;
	SELECT @newRoleID as newRoleID;
END
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spInsertRole] TO [TEUser]
    AS [dbo];

