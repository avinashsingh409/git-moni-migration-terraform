﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [AccessControl].[spCalculateFocalRole]
	@focalRoleIds Base.tpIntList READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @focalRoleId int;

	DECLARE cursorFocalRole CURSOR FOR SELECT id FROM @focalRoleIds
	OPEN cursorFocalRole
	FETCH NEXT FROM cursorFocalRole INTO @focalRoleId

	WHILE(@@FETCH_STATUS=0) BEGIN
		  DELETE FROM AccessControl.tFocalRole WHERE AccessControl.tFocalRole.FocalRoleID = @focalRoleId;

		  WITH roles(SecurityRoleID, parentSecurityRoleID, levelsAway)
		  AS
		  (
				SELECT SecurityRoleID, parentSecurityRoleID , 0 AS levelsAway
				FROM AccessControl.tRole WHERE SecurityRoleID=@focalRoleId
				UNION ALL
				SELECT a.SecurityRoleID, a.parentSecurityRoleID, a2.levelsAway - 1
				FROM AccessControl.tRole a
				INNER JOIN roles a2 ON a2.parentSecurityRoleID = a.SecurityRoleID
		  )
		  INSERT INTO AccessControl.tFocalRole
		  (
				FocalRoleId,
				RelatedRoleId,
				LevelsAway
		  )
		  SELECT @focalRoleId, a.SecurityRoleID, a.levelsAway FROM roles a;
		  
		  FETCH NEXT FROM cursorFocalRole INTO @focalRoleId
	END
	CLOSE cursorFocalRole
	DEALLOCATE cursorFocalRole

END
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spCalculateFocalRole] TO [TEUser]
    AS [dbo];

