CREATE PROCEDURE [AccessControl].[spUpsertCustomer] @EditingUserID INT
	,@CustomerID UNIQUEIDENTIFIER = NULL
	,@Name NVARCHAR(255)
	,@ParentCustomerID UNIQUEIDENTIFIER = NULL
	,@NewCustomerID UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
	BEGIN TRY
		DECLARE @ParentCustID INT = NULL;

		SELECT @ParentCustID = CustID
		FROM [AccessControl].[tCustomer]
		WHERE [CustomerID] = @ParentCustomerID;

		IF @ParentCustID IS NULL
		BEGIN
			RAISERROR (
					'The ParentCustomerID supplied is invalid.'
					,16
					,1
					)
		END

		IF @CustomerID IS NOT NULL
		BEGIN
			IF NOT EXISTS (
					SELECT *
					FROM [AccessControl].[tCustomer]
					WHERE CustomerID = @CustomerID
					)
			BEGIN
				RAISERROR (
						'Unable to update customer. The CustomerID supplied is invalid.'
						,16
						,1
						)
			END

			IF EXISTS (
					SELECT *
					FROM [AccessControl].[tCustomer]
					WHERE Name = @Name
						AND ParentCustID = @ParentCustID
						AND CustomerID <> @CustomerID
					)
			BEGIN
				RAISERROR (
						'Unable to update customer. The Name is in use by a different customer.'
						,16
						,1
						)
			END

			UPDATE [AccessControl].[tCustomer]
			SET [Name] = @Name
				,[ParentCustID] = @ParentCustID
				,[ChangedBy] = @EditingUserID
				,[ChangeDate] = GETDATE()
			WHERE [CustomerID] = @CustomerID
		END
		ELSE
		BEGIN
			IF EXISTS (
					SELECT *
					FROM [AccessControl].[tCustomer]
					WHERE Name = @Name
						AND ParentCustID = @ParentCustID
					)
			BEGIN
				RAISERROR (
						'Unable to create new customer. The Name is in use by a different customer.'
						,16
						,1
						)
			END

			SET @CustomerID = NEWID();

			INSERT INTO [AccessControl].[tCustomer] (
				[CustomerID]
				,[ParentCustID]
				,[Name]
				,[CreatedBy]
				,[ChangedBy]
				,[CreateDate]
				,[ChangeDate]
				)
			VALUES (
				@CustomerID
				,@ParentCustID
				,@Name
				,@EditingUserID
				,@EditingUserID
				,GETDATE()
				,GETDATE()
				)
		END

		SET @NewCustomerID = @CustomerID;
	END TRY

	BEGIN CATCH
		THROW
	END CATCH
END
GO

GRANT EXECUTE
	ON OBJECT::[AccessControl].[spUpsertCustomer]
	TO [TEUser] AS [dbo];