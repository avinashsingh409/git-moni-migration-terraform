﻿CREATE PROCEDURE [AccessControl].[spValidateEmailRecipients]
   @Recipients Base.tpStringList READONLY
AS
BEGIN
   DECLARE @ValidRecipients AS TABLE (
	   Recipient VARCHAR(MAX), 
	   IsValid BIT
   )
   INSERT INTO @ValidRecipients  SELECT Email,1 FROM AccessControl.tUser u
   JOIN @Recipients r ON u.Email = r.value
   WHERE u.Enabled = 1 AND u.UserValidated = 1

   SELECT * FROM @ValidRecipients
   UNION ALL
   SELECT value,0 FROM @Recipients WHERE value NOT IN (SELECT Recipient FROM @ValidRecipients)
END
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spValidateEmailRecipients] TO [TEUser]
    AS [dbo];

