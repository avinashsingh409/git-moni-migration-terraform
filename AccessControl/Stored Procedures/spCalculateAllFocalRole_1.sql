﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [AccessControl].[spCalculateAllFocalRole]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 SET NOCOUNT ON;

    -- Insert statements for procedure here
	 DECLARE @allRoleIds Base.tpIntList;

	 INSERT INTO @allRoleIds
		  SELECT ta.SecurityRoleID
		  FROM AccessControl.tRole ta

	 EXEC AccessControl.spCalculateFocalRole @allRoleIds

END
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spCalculateAllFocalRole] TO [TEUser]
    AS [dbo];

