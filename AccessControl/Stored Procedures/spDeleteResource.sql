﻿--sp to delete from AccessControl.tResource
CREATE PROCEDURE [AccessControl].[spDeleteResource]
	--Input parameters
	@securityResourceID int
AS
BEGIN

	DECLARE @tranName VARCHAR(80);
	SELECT @tranName = 'Delete node with ID =' + CONVERT(VARCHAR(10),@securityResourceID) + ' from tResource.';

	BEGIN TRANSACTION @tranName;

	BEGIN TRY

		DECLARE @nodeExists int
		DECLARE @left int, @right int
		DECLARE @spanBeingDeleted int
		
		SELECT @nodeExists = (SELECT COUNT(SecurityResourceID) FROM AccessControl.tResource WHERE SecurityResourceID = @securityResourceID)
		IF (@nodeExists > 0)
		BEGIN
			--find the left and right value for the node to be removed
			SELECT @left = (SELECT [Left] FROM AccessControl.tResource WHERE SecurityResourceID = @securityResourceID)
			SELECT @right = (SELECT [Right] FROM AccessControl.tResource WHERE SecurityResourceID = @securityResourceID)
			SET @spanBeingDeleted = @right - @left + 1
			
			--remove node and all children
			DELETE FROM AccessControl.tResource WHERE [Left] >= @left AND [Right] <= @right
			
			--update left values
			UPDATE AccessControl.tResource SET [Left] = [Left] - @spanBeingDeleted WHERE [Left] > @right
			
			--update right values
			UPDATE AccessControl.tResource SET [Right] = [Right] - @spanBeingDeleted WHERE [Right] > @right
		END
		ELSE
		BEGIN
			RAISERROR ('Unable to find a node with the SecurityResourceID specified.', 10, 1);
		END
		COMMIT TRANSACTION @tranName;

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION @tranName;
		PRINT N'Script Failed, rolling back';
		PRINT ERROR_MESSAGE();
	END CATCH;
END