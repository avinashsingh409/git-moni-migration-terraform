﻿CREATE PROCEDURE [AccessControl].[spSaveUser]
	@EditingUserID int,
	@SecurityUserID int = null,
	@Username nvarchar(254),
	@Email nvarchar(254),
	@Enabled bit,
	@FirstName nvarchar(25),
	@LastName nvarchar(35),
	@IsBVEmployee bit,
	@IsGroupSub bit,
	@AutoGenerated bit,
	@ServiceAccount bit,
	@UserValidated bit,
	@ForcePasswordReset bit,
	@NERCCIP bit,
	@PublicKey nvarchar(1000),
	@CustomerID uniqueidentifier = null,
    @CreateNewUser bit,
    @EnforceEmailDomain bit,
	@NewSecurityUserID int output,
	@NewGlobalID uniqueidentifier output
AS
BEGIN
	BEGIN TRY
	
	DECLARE @ConflictingUsernameUserID int = null;
	DECLARE @ConflictingEmailUserID int = null;
	DECLARE @LastValidated datetime;

	IF @Username IS NULL
	BEGIN
		RAISERROR('The Username cannot be empty.', 16, 1)
	END
	
	IF(@CustomerID IS NULL)
	BEGIN
		--For legacy user admin, set customer based on customer domain.
		SELECT TOP 1 @CustomerID = CustomerID FROM AccessControl.tCustomerEmailDomain WHERE
				 EmailDomain = substring(@Email, charindex('@', @Email) + 1, 254 - 2);
		--If user is not found, set to None Specified
		IF (@CustomerID IS NULL)
		BEGIN
			SELECT @CustomerID = CustomerID FROM AccessControl.tCustomer where Name = 'None Specified';
		END
	END

	IF @EnforceEmailDomain = 1
	BEGIN
		--Enforce email domain checks. 
		--Email and Username must include a valid domain associated with the CustomerID
		--The condition below is to limit domains allowed for Username and Email to those defined in
		--AccessControl.tCustomerEmailDomain for the customer. 
		IF charindex('@', @Email) < 1 
		BEGIN
			RAISERROR('Email must include a domain', 16, 1)
		END
		IF NOT EXISTS (Select 1/0 From AccessControl.tCustomerEmailDomain WHERE CustomerID = @CustomerID
		AND EmailDomain = substring(@Email, charindex('@', @Email) + 1, 254 - 2)) -- Max length 254
		BEGIN
			RAISERROR('Invalid Email domain for customer', 16, 1)		
		END
		IF charindex('@', @Username) < 1 
		BEGIN
			RAISERROR('Username must include a domain', 16, 1)
		END
		IF NOT EXISTS (Select 1/0 From AccessControl.tCustomerEmailDomain WHERE CustomerID = @CustomerID
		AND EmailDomain = substring(@Username, charindex('@', @Username) + 1, 254 - 2)) -- Max length 254
		BEGIN
			RAISERROR('Invalid Username domain for customer', 16, 1)
		END
	END

	SELECT @ConflictingUsernameUserID = SecurityUserID FROM AccessControl.tUser WHERE Username = @Username;
	SELECT @ConflictingEmailUserID = SecurityUserID FROM AccessControl.tUser WHERE Email = @Email;

	IF @CreateNewUser = 1
		BEGIN
			-- Create new user
			IF @ConflictingUsernameUserID IS NOT NULL
			BEGIN
				RAISERROR('Unable to create new user, the username is in use by a different user.', 16, 1)
			END
			IF @ConflictingEmailUserID IS NOT NULL
			BEGIN
				RAISERROR('Unable to create new user, the email is in use by a different user.', 16, 1)
			END
		END
	ELSE
		BEGIN
			-- Update existing user
			IF @SecurityUserID IS NULL
			BEGIN
				RAISERROR('Unable to update user, the securityUserID is undefined.', 16, 1)
			END
			IF NOT EXISTS (SELECT TOP 1  SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID)
			BEGIN
				RAISERROR('Unable to update user, the securityUserID is invalid.', 16, 1)
			END
			IF (@ConflictingUsernameUserID IS NOT NULL) AND (@ConflictingUsernameUserID <> @SecurityUserID)
			BEGIN
				RAISERROR('Unable to update username, the username is in use by a different user.', 16, 1)
			END
			IF (@ConflictingEmailUserID IS NOT NULL) AND (@ConflictingEmailUserID <> @SecurityUserID)
			BEGIN
				RAISERROR('Unable to update email, the email is in use by a different user.', 16, 1)
			END
		END

	IF @UserValidated = 1
	BEGIN
		SET @LastValidated = GETDATE();
	END

	IF @CreateNewUser = 0
		BEGIN
			UPDATE [AccessControl].[tUser]
				SET [Username] = @Username
					,[Email] = @Email
					,[Enabled] = @Enabled
					,[FirstName] = @FirstName
					,[LastName] = @LastName
					,[IsBVEmployee] = @IsBVEmployee
					,[IsGroupSub] = @IsGroupSub
					,[AutoGenerated] = @AutoGenerated
					,[ServiceAccount] = @ServiceAccount
					,[UserValidated] = @UserValidated
					,[LastValidated] = @LastValidated
					,[ForcePasswordReset] = @ForcePasswordReset
					,[NERCCIP] = @NERCCIP
					,[PublicKey] = @PublicKey
					,[ChangedByUserID] = @EditingUserID
					,[ChangeDate] = GETDATE()
					,[CustomerID] = @CustomerID
				WHERE [SecurityUserID] = @SecurityUserID

			SET @NewSecurityUserID = @SecurityUserID;
		END		
	ELSE
		BEGIN
			INSERT INTO [AccessControl].[tUser]
				([Username]
				,[Email]
				,[Enabled]
				,[FirstName]
				,[LastName]
				,[IsBVEmployee]
				,[IsGroupSub]
				,[AutoGenerated]
				,[ServiceAccount]
				,[UserValidated]
				,[LastValidated]
				,[ForcePasswordReset]
				,[NERCCIP]
				,[PublicKey]
				,[CreatedByUserID]
				,[ChangedByUserID]
				,[CustomerID])
			VALUES
				(@Username
				,@Email
				,@Enabled
				,@FirstName
				,@LastName
				,@IsBVEmployee
				,@IsGroupSub
				,@AutoGenerated
				,@ServiceAccount
				,@UserValidated
				,@LastValidated
				,@ForcePasswordReset
				,@NERCCIP
				,@PublicKey
				,@EditingUserID
				,@EditingUserID
				,@CustomerID)

			SET @NewSecurityUserID = CAST(SCOPE_IDENTITY() as int);
		END

	SELECT TOP 1  @NewGlobalID = GlobalID FROM AccessControl.tUser WHERE SecurityUserID = @NewSecurityUserID;
	
	END TRY
	BEGIN CATCH
		THROW		
	END CATCH

END
GO
GRANT EXECUTE
    ON OBJECT::[AccessControl].[spSaveUser] TO [TEUser]
    AS [dbo];

