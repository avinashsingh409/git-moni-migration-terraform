﻿CREATE TABLE [CapPrior].[tScenarioProjectMap] (
    [ScenarioProjectMapID] INT IDENTITY (1, 1) NOT NULL,
    [ScenarioID]           INT NOT NULL,
    [ProjectID]            INT NOT NULL,
    CONSTRAINT [PK_tScenarioProjectMapID] PRIMARY KEY CLUSTERED ([ScenarioProjectMapID] ASC)
);

