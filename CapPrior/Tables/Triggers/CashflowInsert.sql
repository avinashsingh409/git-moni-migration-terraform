﻿
CREATE TRIGGER [CapPrior].CashflowInsert
   ON  CapPrior.tProjectCashFlow
   FOR INSERT
AS
BEGIN
declare @capPriorIds Base.tpIntList
insert into @capPriorIds select ProjectCashFlowID from inserted
exec [CapPrior].[spSyncProcessData] @capPriorIds
END

GO
