﻿CREATE TABLE [CapPrior].[tProjectCashflowAudit] (
    [CashFlowAuditId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [AuditId]           BIGINT         NOT NULL,
    [ProjectCashflowId] INT            NOT NULL,
    [UnitOfMeasure]     NVARCHAR (255) NOT NULL,
    [AssetID]           INT            NOT NULL,
    [TimeStamp]         DATETIME       NOT NULL,
    [Type]              NVARCHAR (255) NOT NULL,
    [PreviousValue]     FLOAT (53)     NOT NULL,
    [PreviousStatus]    INT            NOT NULL,
    CONSTRAINT [PK_tProjectCashflowAudit] PRIMARY KEY CLUSTERED ([CashFlowAuditId] ASC),
    CONSTRAINT [FK_tProjectCashflowAudit_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tProjectCashflowAudit_tAudit] FOREIGN KEY ([AuditId]) REFERENCES [Audit].[tAudit] ([AuditId])
);


GO

CREATE INDEX [IDX_ProjectCashflowId_Includes] ON [CapPrior].[tProjectCashflowAudit] ([ProjectCashflowId])  INCLUDE ([AuditId]) WITH (FILLFACTOR=100);
