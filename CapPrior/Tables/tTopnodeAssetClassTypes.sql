﻿CREATE TABLE [CapPrior].[tTopnodeAssetClassTypes] (
    [AssetClassTypeID] INT NOT NULL,
	ValueLabel varchar(255) NULL,
	StatusLabel varchar(255) NULL,
    CONSTRAINT [PK_tTopnodeAssetClassTypes] PRIMARY KEY CLUSTERED ([AssetClassTypeID] ASC),
    CONSTRAINT [FK_tTopnodeAssetClassTypes_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
);

