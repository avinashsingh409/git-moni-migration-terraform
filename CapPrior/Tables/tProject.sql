﻿CREATE TABLE [CapPrior].[tProject] (
    [ProjectID]          INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]            INT            NOT NULL,
    [ProjectAbbrev]      NVARCHAR (50)  NOT NULL,
    [ProjectDesc]        NVARCHAR (255) NOT NULL,
    [ProjectCapitalCost] REAL           NOT NULL,
    [DisplayOrder]       INT            NOT NULL,
    CONSTRAINT [PK_tProjectID] PRIMARY KEY CLUSTERED ([ProjectID] ASC),
    CONSTRAINT [FK_tProject_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);



