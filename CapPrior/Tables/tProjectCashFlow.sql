﻿CREATE TABLE [CapPrior].[tProjectCashFlow] (
    [ProjectCashFlowID] INT            IDENTITY (1, 1) NOT NULL,
    [UnitOfMeasure]     NVARCHAR (255) NOT NULL,
    [AssetID]           INT            NOT NULL,
    [Timestamp]         DATETIME       NOT NULL,
    [Type]              NVARCHAR (255) NOT NULL,
    [Status]            INT            NOT NULL,
    [Value]             FLOAT (53)     NOT NULL,
    [ValueTagID]        INT            NULL,
    [StatusTagID]       INT            NULL,
    PRIMARY KEY CLUSTERED ([ProjectCashFlowID] ASC),
    CONSTRAINT [FK_tProjectCashFlow_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tProjectCashFlow_tTag_StatusTagID] FOREIGN KEY ([StatusTagID]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tProjectCashFlow_tTag_ValueTagID] FOREIGN KEY ([ValueTagID]) REFERENCES [ProcessData].[tTag] ([PDTagID])
);












GO
CREATE NONCLUSTERED INDEX [IDX_tProjectCashFlow_Type]
    ON [CapPrior].[tProjectCashFlow]([Type] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tProjectCashFlow_AssetID_Type_Timestamp]
    ON [CapPrior].[tProjectCashFlow]([AssetID] ASC, [Type] ASC, [Timestamp] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tProjectCashFlow_AssetID_Type_Timestamp_Status_Value]
    ON [CapPrior].[tProjectCashFlow]([AssetID] ASC, [Timestamp] ASC, [Type] ASC, [Status] ASC, [Value] ASC);


GO
CREATE INDEX [IDX_tProjectCashFlow_ValueTagID_Timestamp_includes] ON [CapPrior].[tProjectCashFlow] ([ValueTagID], [Timestamp])  INCLUDE ([Value]) ;
GO
CREATE INDEX [IDX_tProjectCashFlow_Timestamp_includes] ON [CapPrior].[tProjectCashFlow] ([Timestamp])  INCLUDE ([Value], [ValueTagID]) ;
GO
CREATE INDEX [IDX_tProjectCashFlow_Type_Value_includes] ON [CapPrior].[tProjectCashFlow] ([Type], [Value])  INCLUDE ([AssetID], [Timestamp]) ;
GO
CREATE INDEX [IDX_tProjectCashFlow_Type_Status_includes] ON [CapPrior].[tProjectCashFlow] ([Type], [Status])  INCLUDE ([AssetID], [Timestamp]) ;