﻿CREATE TABLE [CapPrior].[tScenarioProjectResult] (
    [ScenarioProjectResultID] INT IDENTITY (1, 1) NOT NULL,
    [ScenarioProjectMapID]    INT NOT NULL,
    [Result]                  INT NOT NULL,
    CONSTRAINT [PK_tScenarioProjectResultID] PRIMARY KEY CLUSTERED ([ScenarioProjectResultID] ASC)
);

