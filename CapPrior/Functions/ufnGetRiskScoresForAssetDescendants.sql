﻿CREATE FUNCTION [CapPrior].[ufnGetRiskScoresForAssetDescendants]
(
	@securityUserID int,
	@startingAssetID int,
	@xAxisDesc varchar(255),
	@yAxisDesc varchar(255),
	@labelDesc varchar(255)
)
RETURNS TABLE AS
RETURN
(
	SELECT cfYaxis.AssetID, cfYaxis.Timestamp, cfXaxis.Value as [XaxisValue], cfYaxis.Value as [YaxisValue], cfLabel.Value as [LabelValue]
	FROM [CapPrior].[tProjectCashFlow] cfYaxis
	JOIN [Asset].[ufnGetAssetIdsForUserStartingAt](@securityUserID, @startingAssetID) A ON A.AssetID = cfYaxis.AssetID
	JOIN [CapPrior].[tProjectCashFlow] cfXaxis ON cfXaxis.AssetID = cfYaxis.AssetID AND cfXaxis.Timestamp = cfYaxis.Timestamp
		AND cfYaxis.Type = @yAxisDesc AND cfXaxis.Type = @xAxisDesc
	LEFT JOIN [CapPrior].[tProjectCashFlow] cfLabel ON cfLabel.AssetID = cfXaxis.AssetID AND cfLabel.Timestamp = cfXaxis.Timestamp
		AND cfLabel.Type = @labelDesc
)
GO
GRANT SELECT
    ON OBJECT::[CapPrior].[ufnGetRiskScoresForAssetDescendants] TO [TEUser]
    AS [dbo];
GO