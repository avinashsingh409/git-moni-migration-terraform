﻿
CREATE   FUNCTION [CapPrior].[ufnGetLastCashflowValueByDate]
(
	-- Add the parameters for the function here
  @assetID int
 ,@type nvarchar(255) 
 ,@date datetime
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @lastValue float = null;
	DECLARE @lastUpdateTS datetime;

	--It's entirely possible to have multiple updates to a given type for a given asset at a given timestamp.
	--If that happens, we need to take the largest value for the Status at that time, which may not be the 'last' by timestamp order.
	--Ignore 99s - we want to 'see through' them to the last real value, they're fake values to keep the asset on the map.
	select top 1 @lastUpdateTS = [Timestamp]
	from CapPrior.tProjectCashFlow
	where AssetID = @assetID
	and [Type] = @type
	and [Timestamp] <= @date
	and [Value] is not null
	and [Value] not in (-99, 99)
	order by [Timestamp] desc
	
	--Of course if an asset has nothing but 99s, then @lastUpdateTS could well be null still.
	if @lastUpdateTS is not null
	BEGIN
		SELECT @lastValue = max ([Value])
		from CapPrior.tProjectCashFlow
		where AssetID = @assetID
		and [Type] = @type
		and [Timestamp] = @lastUpdateTS
		and [Value] not in (-99,99)
	END

	-- if the last value is null, return -1 to take the asset out of the count.
	IF @lastValue is null
	BEGIN
		SET @lastValue = -1
	END
	-- Return the result of the function
	RETURN @lastValue

END
GO
GRANT EXECUTE
    ON OBJECT::[CapPrior].[ufnGetLastCashflowValueByDate] TO [TEUser]
    AS [dbo];

