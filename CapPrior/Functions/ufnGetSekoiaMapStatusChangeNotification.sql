﻿CREATE Function [CapPrior].[ufnGetSekoiaMapStatusChangeNotification]
(
    @assetId int,
    @mapName varchar(255),	
	@sekoiaMapValueChangeFrom FLOAT,
	@sekoiaMapValueChangeTo FLOAT,
	@appliesToManualChange bit = 1,
	@appliesToImportChange bit = 0
)
RETURNS
@mapStatusChangeTable Table
(       
       [NotificationID] int NOT NULL
	   ,[NotificationTypeID] int NOT NULL
)
    AS 
	BEGIN

	DECLARE @assetClassTypeID int = (select tA.AssetClassTypeID FROM Asset.tAsset tA WHERE tA.AssetID = @assetId);
		
	declare @workingTable TABLE	( CategoryAccessRuleID int);			
	DECLARE @assets TABLE(assetID INT, assetClassTypeID int);
	
	INSERT INTO @assets(assetID, assetClassTypeID)
	
	SELECT a.AssetID, a.AssetClassTypeID FROM Asset.ufnAssetAncestorsIncludeAsset(@assetId) anc
	INNER JOIN Asset.tAsset a ON anc.AssetID = a.AssetID
	Inner join Asset.tAssetClassType act on act.AssetClassTypeID = a.AssetClassTypeID
	Where  act.AssetTypeID >= 2;

INSERT INTO @workingTable (CategoryAccessRuleID)
SELECT DISTINCT CategoryAccessRuleID FROM (
	--This should take care of the situation where the asset matches the search criteria independent of ancestors
	SELECT r.CategoryAccessRuleID FROM Diagnostics.tCategoryAccessRule r
	WHERE ((r.AssetID = @assetId AND (r.AssetClassTypeID IS NULL OR r.AssetClassTypeID = @assetclasstypeid))
	OR
	(r.AssetClassTypeID = @assetclasstypeid AND r.AssetID IS NULL)
	OR 
	(r.AssetClassTypeID IS NULL AND r.AssetID IS NULL)) 	

	UNION

	--Find RuleID where the asset ID matches the ancestors
	SELECT r.CategoryAccessRuleID FROM Diagnostics.tCategoryAccessRule r
	INNER JOIN @assets a ON r.AssetID = a.AssetID 
	WHERE r.AssetDescendants = 1 AND (r.AssetClassTypeID IS NULL OR (r.AssetClassTypeID = a.assetClassTypeID AND r.AssetClassTypeDescendants = 0))

	UNION 

	--Find RuleID where the category matches the ancestors
	SELECT r.CategoryAccessRuleID FROM Diagnostics.tCategoryAccessRule r
	INNER JOIN @assets a ON r.AssetClassTypeID = a.assetClassTypeID
	WHERE r.AssetClassTypeDescendants = 1 AND (r.AssetID IS NULL OR (r.AssetID = a.AssetID AND r.AssetDescendants = 0)) 

	UNION

	--Find RuleID where the asset ID matches and class type id matches the ancestors
	SELECT r.CategoryAccessRuleID FROM Diagnostics.tCategoryAccessRule r
	WHERE r.AssetDescendants = 1 AND r.AssetClassTypeDescendants = 1 AND r.AssetID IS NOT NULL AND r.AssetClassTypeID IS NOT NULL	
	AND EXISTS(SELECT 1 FROM @assets where AssetID = r.AssetID)
	AND EXISTS(SELECT 1 FROM @assets WHERE AssetClassTypeID = r.AssetClassTypeID)
	) a
	
	--
	if exists(select * from @workingTable) 
		begin

		INSERT INTO @mapStatusChangeTable (NotificationID, NotificationTypeID) 

		SELECT n.CategoryNotificationID, n.CategoryNotificationTypeID 
		FROM Diagnostics.tCategoryNotification n
		  join Diagnostics.tCategoryAccessRule tCAR on n.AssetIssueCategoryTypeID = tCAR.AssetIssueCategoryTypeID
		  join @workingTable wT on wT.CategoryAccessRuleID = tCAR.CategoryAccessRuleID
		WHERE 
		    (n.SekoiaMapName = @mapName) 
			AND 
			(n.AppliesToManualChange = @appliesToManualChange) 
			AND
			(n.AppliesToImportChange = @appliesToImportChange) 
			AND
			(	
				(((n.SourceSekoiaMapValue = @sekoiaMapValueChangeFrom) AND (n.TargetSekoiaMapValue = @sekoiaMapValueChangeTo))
					OR ((n.SourceSekoiaMapValue = @sekoiaMapValueChangeFrom) AND (n.TargetSekoiaMapValue IS NULL))
					OR ((n.SourceSekoiaMapValue IS NULL) AND (n.TargetSekoiaMapValue = @sekoiaMapValueChangeTo)))
				AND (ISNULL(@sekoiaMapValueChangeFrom,-1) <> ISNULL(@sekoiaMapValueChangeTo,-1))
			)
		end

		--match on assetclasstype independent of access rule table.
		INSERT INTO @mapStatusChangeTable (NotificationID, NotificationTypeID)
		SELECT n.CategoryNotificationID, n.CategoryNotificationTypeID 
		FROM Diagnostics.tCategoryNotification n
			join @assets asts on asts.AssetClasstypeID = n.AssetClassTypeID

		WHERE 
		    (n.SekoiaMapName = @mapName) 
			AND 
			(n.AppliesToManualChange = @appliesToManualChange) 
			AND
			(n.AppliesToImportChange = @appliesToImportChange) 
			AND
			(	
				(((n.SourceSekoiaMapValue = @sekoiaMapValueChangeFrom) AND (n.TargetSekoiaMapValue = @sekoiaMapValueChangeTo))
					OR ((n.SourceSekoiaMapValue = @sekoiaMapValueChangeFrom) AND (n.TargetSekoiaMapValue IS NULL))
					OR ((n.SourceSekoiaMapValue IS NULL) AND (n.TargetSekoiaMapValue = @sekoiaMapValueChangeTo)))
				AND (ISNULL(@sekoiaMapValueChangeFrom,-1) <> ISNULL(@sekoiaMapValueChangeTo,-1))
			)
			AND n.CategoryNotificationID NOT IN (SELECT CategoryNotificationID FROM @mapStatusChangeTable)

	RETURN
	END
GO
	GRANT SELECT
    ON OBJECT::[CapPrior].[ufnGetSekoiaMapStatusChangeNotification] TO [TEUser]
    AS [dbo];

GO