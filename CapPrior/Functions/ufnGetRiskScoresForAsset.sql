﻿CREATE FUNCTION [CapPrior].[ufnGetRiskScoresForAsset]
(
	@securityUserID int,
	@assetID int,
	@xAxisDesc varchar(255),
	@yAxisDesc varchar(255),
	@labelDesc varchar(255)
)
RETURNS TABLE AS
RETURN
(
	SELECT cfYaxis.AssetID, cfYaxis.Timestamp, cfXaxis.Value as [XaxisValue], cfYaxis.Value as [YaxisValue], cfLabel.Value as [LabelValue]
	FROM [CapPrior].[tProjectCashFlow] cfYaxis
	JOIN [CapPrior].[tProjectCashFlow] cfXaxis ON cfXaxis.AssetID = cfYaxis.AssetID AND cfXaxis.Timestamp = cfYaxis.Timestamp
	JOIN [CapPrior].[tProjectCashFlow] cfLabel ON cfLabel.AssetID = cfXaxis.AssetID AND cfLabel.Timestamp = cfXaxis.Timestamp
	WHERE Asset.ufnDoesUserHaveAccessToAsset(@assetID, @securityUserID, -1) = 1
	AND cfYaxis.AssetID = @assetID
	AND cfYaxis.Type = @yAxisDesc
	AND cfXaxis.Type = @xAxisDesc
	AND cfLabel.Type = @labelDesc
)
GO
GRANT SELECT
    ON OBJECT::[CapPrior].[ufnGetRiskScoresForAsset] TO [TEUser]
    AS [dbo];
GO