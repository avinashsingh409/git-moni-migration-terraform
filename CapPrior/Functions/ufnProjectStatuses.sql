﻿CREATE FUNCTION [CapPrior].[ufnProjectStatuses]
(
	@assets Base.tpIntList READONLY,
	@asOf DateTime, 
	@statusType NVARCHAR(255)
)
RETURNS TABLE
AS
RETURN
(
SELECT x.AssetID, x.Timestamp, x.Status, x.Value FROM (
SELECT AssetID, Timestamp, MAX(Status) as Status, Max(Value) as Value FROM CapPrior.tProjectCashFlow cf 
INNER JOIN @assets a ON cf.AssetID = a.id
WHERE cf.Type = @statusType AND TimeStamp <= @asOf
GROUP BY AssetID, TimeStamp
) x

INNER JOIN 

(
SELECT AssetID, MAX( Timestamp ) as Timestamp
FROM CapPrior.tProjectCashFlow cf
INNER JOIN @assets a ON cf.AssetID = a.id
WHERE cf.Type = @statusType AND TimeStamp <= @asOf
GROUP BY AssetID
) y 

ON x.AssetID = y.AssetID AND x.Timestamp = y.Timestamp
)