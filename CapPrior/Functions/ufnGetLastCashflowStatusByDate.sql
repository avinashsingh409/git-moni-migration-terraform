﻿CREATE   FUNCTION [CapPrior].[ufnGetLastCashflowStatusByDate]
(
	-- Add the parameters for the function here
  @assetID int
 ,@type nvarchar(255) 
 ,@date datetime
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @lastStatus int = null;
	DECLARE @lastUpdateTS datetime;
	
	--It's entirely possible to have multiple updates to a given type for a given asset at a given timestamp.
	--If that happens, we need to take the largest value for the Status at that time, which may not be the 'last' by timestamp order.
	--Ignore 99s - we want to 'see through' them to the last real value, they're fake values to keep the asset on the map.
	select top 1 @lastUpdateTS = [Timestamp]
	from CapPrior.tProjectCashFlow
	where AssetID = @assetID
	and [Type] = @type
	and [Timestamp] <= @date
	and [Status] is not null
	and [Status] not in (-99, 99)
	order by [Timestamp] desc

	--Of course if an asset has nothing but 99s, then @lastUpdateTS could well be null still.
	if @lastUpdateTS is not null
	BEGIN
		SELECT @lastStatus = max ([Status])
		from CapPrior.tProjectCashFlow
		where AssetID = @assetID
		and [Type] = @type
		and [Timestamp] = @lastUpdateTS
		AND [Status] not in (-99,99)
	END

	-- if the last status is null, return -1 to take the asset out of the count.
	IF @lastStatus is null
	BEGIN
		SET @lastStatus = -1
	END
	-- Return the result of the function
	RETURN @lastStatus

END
GO
GRANT EXECUTE
    ON OBJECT::[CapPrior].[ufnGetLastCashflowStatusByDate] TO [TEUser]
    AS [dbo];

