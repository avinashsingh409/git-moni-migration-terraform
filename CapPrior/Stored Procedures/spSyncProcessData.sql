﻿CREATE PROCEDURE [CapPrior].[spSyncProcessData]
	 @capPriorIds Base.tpIntList READONLY
AS
BEGIN

SET NOCOUNT ON;

create table #uniqueTypes 
(
  topAssetId int null,
  topClassTypeID int null,
  assetId int,
  [type] varchar(255),
  UnitOfMeasure varchar(255),
  ValuePDServerID int NULL,
  StatusPDServerID int NULL,
  ValueTagName nvarchar(400),
  StatusTagName nvarchar(400),
  ValueTagDesc nvarchar(400),
  StatusTagDesc nvarchar(400),
  ValueTagID int NULL,
  StatusTagID int NULL
)

CREATE INDEX idx_1 ON #uniqueTypes(assetID);

insert into #uniqueTypes (assetId,type,UnitOfMeasure) 
select b.assetid,[type],MAX(UnitOfMeasure) as UnitOfMeasure from @capPriorIds a join CapPrior.tProjectCashFlow b on a.ID = b.ProjectCashFlowID group by b.AssetID,[type]

update a set topAssetId = b.HopAssetId,topClassTypeID = c.AssetClassTypeID,
ValueTagName = [TYPE] + ';Value;' + CONVERT(varchar,a.assetid),ValueTagDesc = [type] +  case when d.valuelabel IS null then '' else ' - ' + d.valuelabel end,
StatusTagName = case when d.StatusLabel IS null then null else  [TYPE] + ';Status;' + CONVERT(varchar,a.assetid) END,
StatusTagDesc = case when d.StatusLabel IS null then null else [type] + ' - ' + d.StatusLabel end
 from #uniqueTypes a join Asset.tAssetHop b on a.assetId = b.EndingAssetId join Asset.tAsset c on b.HopAssetId = c.AssetID 
join CapPrior.tTopnodeAssetClassTypes d on c.AssetClassTypeID = d.AssetClassTypeID

insert into ProcessData.tServer (PDServerTypeID, ServerDesc, Comment, AssetID,NDPumpAction,Active) select distinct 10,'CapPrior.tProjectCashFlow;Timestamp;Value;0;;ValueTagID',b.AssetAbbrev + ' [Value] PD Server',a.topAssetId,0,1 from 
#uniqueTypes a join asset.tasset b on a.topassetid = b.assetid 
join CapPrior.tTopnodeAssetClassTypes c on a.topClassTypeID = c.AssetClassTypeID
where not exists(select * from ProcessData.tServer x where a.topAssetId = x.AssetID and x.PDServerTypeID=10 and 
x.ServerDesc like '%Value%') 

insert into ProcessData.tServer (PDServerTypeID, ServerDesc, Comment, AssetID,NDPumpAction,Active) select distinct 10,'CapPrior.tProjectCashFlow;Timestamp;Status;0;;StatusTagID',b.AssetAbbrev + ' [Status] PD Server',a.topAssetId,0,1 from 
#uniqueTypes a join asset.tasset b on a.topassetid = b.assetid 
join CapPrior.tTopnodeAssetClassTypes c on a.topClassTypeID = c.AssetClassTypeID
where not exists(select * from ProcessData.tServer x where a.topAssetId = x.AssetID and x.PDServerTypeID=10 and 
x.ServerDesc like '%Status%') 

update a set a.ValuePDServerID = b.PDServerID from #uniqueTypes a join ProcessData.tServer b on a.topAssetId = b.AssetID where b.ServerDesc like '%;Value;%'
update a set a.StatusPDServerID = b.PDServerID from #uniqueTypes a join ProcessData.tServer b on a.topAssetId = b.AssetID where b.ServerDesc like '%;Status;%'

create table #tags
(
    [PDServerID] [int] NOT NULL,
	[TagName] [nvarchar](400) NOT NULL,
	[TagDesc] [nvarchar](400) NULL,
	[EngUnits] [nvarchar](255) NULL
)

CREATE INDEX IDX_1 ON #tags(PDServerID);
CREATE INDEX IDX_2 ON #tags(TagName);

insert into #tags (PDServerID,TagName,TagDesc,EngUnits)
select valuePDserverID,a.ValueTagName,a.ValueTagDesc,UnitOfMeasure from #uniqueTypes a,
	AccessControl.tUser u where u.Username = 'bvDataUser@bv.com' and ValuePDServerID is not null and a.ValueTagName is not null

insert into #tags (PDServerID,TagName,TagDesc,EngUnits)
select StatusPDServerID,a.StatusTagName,a.StatusTagDesc,UnitOfMeasure from #uniqueTypes a,
	AccessControl.tUser u where u.Username = 'bvDataUser@bv.com' and StatusPDServerID is not null and a.StatusTagName is not null

insert into ProcessData.tTag (PDServerID,TagName,TagDesc,EngUnits,ExternalID,ExistsOnServer,CreatedBy,ChangedBy,CreateDate,ChangeDate) 
	select PDServerID,TagName,TagDesc,EngUnits, '',1,u.SecurityUserID,u.SecurityUserID,GETDATE(),GETDATE() from #tags a,
	AccessControl.tUser u where u.Username = 'bvDataUser@bv.com' and not exists (select * from ProcessData.tTag t where t.PDServerID = a.PDServerID and t.TagName = a.TagName)

update a set a.ValueTagID = b.PDTagID from #uniqueTypes a join ProcessData.tTag b on a.ValuePDServerID = b.PDServerID and a.ValueTagName = b.TagName
update a set a.StatusTagID = b.PDTagID from #uniqueTypes a join ProcessData.tTag b on a.StatusPDServerID = b.PDServerID and a.StatusTagName = b.TagName

insert into ProcessData.tAssetVariableTypeTagMap (AssetID,TagID,CreatedBy,ChangedBy,CreateDate,ChangeDate) select a.assetId,a.ValueTagID,SecurityUserID,SecurityUserID,GETDATE(),GETDATE() from #uniqueTypes a,AccessControl.tUser b where not exists (select * from ProcessData.tAssetVariableTypeTagMap x where a.ValueTagID = x.TagID and a.assetId = x.AssetID) and 
b.Username = 'bvDataUser@bv.com' and a.ValueTagID IS NOT NULL

insert into ProcessData.tAssetVariableTypeTagMap (AssetID,TagID,CreatedBy,ChangedBy,CreateDate,ChangeDate) select a.assetId,a.StatusTagID,SecurityUserID,SecurityUserID,GETDATE(),GETDATE() from #uniqueTypes a,AccessControl.tUser b where not exists (select * from ProcessData.tAssetVariableTypeTagMap x where a.StatusTagID = x.TagID and a.assetId = x.AssetID) and 
b.Username = 'bvDataUser@bv.com' and a.StatusTagID IS NOT NULL

update a set a.ValueTagID = b.ValueTagID, a.StatusTagID = b.StatusTagID from CapPrior.tProjectCashFlow a join #uniqueTypes b on a.AssetID = b.assetId and a.Type = b.type

declare @tagIDs as base.tpIntList
insert into @tagIDs select distinct ValueTagID from #uniqueTypes where ValueTagID is not null
insert into @tagIDs select distinct StatusTagID from #uniqueTypes where StatusTagID is not null

exec [CapPrior].[spCreateNewVariablesAndMaps] @tagIDs

IF OBJECT_ID('tempdb..#uniqueTypes') IS NOT NULL DROP TABLE #uniqueTypes;

IF OBJECT_ID('tempdb..#tags') IS NOT NULL DROP TABLE #tags;

END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spSyncProcessData] TO [TEUser]
    AS [dbo];

