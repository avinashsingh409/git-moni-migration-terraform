﻿CREATE PROCEDURE [CapPrior].[spGetSimpleTrends]
@ExportAssetID int,
@SecurityUserID int,
@IncludePDServerTypes Base.tpIntList READONLY

AS BEGIN

	create table #assetSimpleLineTrends 
	(  
	  ID Int Primary Key
	)

	--get the population
	declare @userAssetIDs Base.tpIntList;
	insert into @userAssetIDs
	select distinct m.AssetID from ProcessData.tAssetTrendMap m join Asset.ufnGetAssetIdsForUserStartingAt(@SecurityUserID, @ExportAssetID) b on m.AssetID = b.AssetID

	-- start with trends that are asset-specific simple line trends
	insert into #assetSimpleLineTrends (id)
	select distinct (t.PDTrendID)
	from ProcessData.tTrend t
	join ProcessData.tTrendSeries s on t.PDTrendID = s.PDTrendID
	join ProcessData.tAssetTrendMap m on t.PDTrendID = m.TrendID
	join @userAssetIDs a on m.AssetID = a.id
	where
		t.ChartTypeID is null
		and t.FilterMin is null
		and t.Filtermax is null
		and t.FlatlineThreshold = 0
		and (t.Exclude is null OR t.Exclude = '' OR t.Exclude = '<filter></filter>'
			OR t.Exclude = '<filter><months></months></filter>'
			OR t.Exclude = '<filter><days></days></filter>'
			OR t.Exclude = '<filter><hours></hours></filter>'
			OR t.Exclude = '<filter><months></months><days></days></filter>'
			OR t.Exclude = '<filter><days></days><hours></hours></filter>'
			OR t.Exclude = '<filter><months></months><hours></hours></filter>'
			OR t.Exclude = '<filter><months></months><days></days><hours></hours></filter>')
		and t.PinTypeID = 1
		and t.TrendSource is null
		and t.ProjectId is null
		and t.Path is null
		and t.Math is null
		and t.SummaryTypeID is null
		and s.PDTagID is not null
		and s.PDVariableID is null
		and s.FlatlineThreshold = 0
		and s.IsMain = 0
		--//Allow export of trends with axis display order defined, however display order is
		--//would not be preserved
		--and s.DisplayOrder = 0
		and s.UseDisplayText = 0
		and s.IsXAxis = 0
		and s.IsFiltered = 0
		and s.ShowBestFitLine = 0
		and s.IsZAxis = 0
		and s.ApplyToAll = 0
		and s.UseGlobalDataRetrieval = 1

	-- remove from the starting list those trends that have
	-- features that are not supported in the import template workbook
	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tAssetClassTypeTrendMap acm on t.id = acm.TrendID

	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tTrendArea tarea on t.id = tarea.PDTrendID

	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tTrendBandAxis tbax on t.id = tbax.PDTrendID

	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tTrendBandAxisBar tbar on t.id = tbar.PDTrendID

	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tTrendDataRetrieval tret on t.id = tret.PDTrendID
	where tret.Method !=0

	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tTrendDesignCurve tcurv on t.id = tcurv.PDTrendID

	delete from t
	from #assetSimpleLineTrends t
	join ProcessData.tTrendPin tpin on t.id = tpin.PDTrendID

	--
	-- the next section is to disqualify any cash flow trends that have tags that are orphaned (i.e. aren't represented in the cash flow table)
	--

	create table #tmpID 
	(
	  TrendID int, ValueTagID int null, StatusTagID int null
	)

	create table #servers
	(
	  PDServerID Int Primary Key
	)

	insert into #servers select PDServerID from ProcessData.tServer where ServerDesc like 'CapPrior.tProjectCashFlow%'
	insert into #tmpID
	select distinct t.id,valueTag.ValueTagID,statusTag.StatusTagID from #assetSimpleLineTrends t join ProcessData.tTrendSeries b on t.id = b.PDTrendID
	join ProcessData.tTag c on b.PDTagID = c.PDTagID join #servers d on c.PDServerID = d.PDServerID
	LEFT JOIN CapPrior.tProjectCashFlow valueTag ON c.PDTagID = valueTag.ValueTagID
	LEFT JOIN CapPrior.tProjectCashFlow statusTag ON c.PDTagID = statusTag.StatusTagID

	delete from t
	from #assetSimpleLineTrends t
	join #tmpID tmp on t.id = tmp.TrendID where tmp.ValueTagID IS NULL AND tmp.StatusTagID is NULL

	delete from a from #assetSimpleLineTrends a
		join ProcessData.tTrendSeries b on a.id = b.PDTrendID join ProcessData.tTag c on b.PDTagID = c.PDTagID
		JOIN ProcessData.tServer d on c.PDServerID = d.PDServerID
		left outer join @IncludePDServerTypes ipst on ipst.id = d.PDServerTypeID
		WHERE ipst.id IS NULL

	select distinct ID as PDTrendID from #assetSimpleLineTrends

	END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetSimpleTrends] TO [TEUser]
    AS [dbo];