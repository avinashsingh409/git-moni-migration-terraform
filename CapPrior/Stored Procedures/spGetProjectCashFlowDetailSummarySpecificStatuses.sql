﻿CREATE PROCEDURE [CapPrior].[spGetProjectCashFlowDetailSummarySpecificStatuses]
	@userID int ,
    @assetID int ,
    @valueType nvarchar(255),  
	@assetClassTypeID int,
	@indicator datetime,
	@ignoreIDList bit,
	@idList base.tpIntList READONLY,
	@descendants base.tpIntList READONLY,
	@statusList base.tpIntList READONLY,
	@excludeNinetyNineActuals bit = 1,
	@excludeNinetyNinePlans bit = 1
AS
BEGIN

	
	if (SELECT count(*) from @descendants) = 0 AND @assetID <= 0 
	BEGIN
		RAISERROR ('Either an asset ID or a set of descendents must be specified in order to fetch cash flow detail summaries.', 18, 1)
	END
	
	CREATE TABLE #baseTable 
	(
	  AssetID int,
	  AssetClassTypeID int,
	  TimeStamp DateTime,
	  [Type] varchar(255),
	  [Status] int,
	  [Value] float
	)

	IF @assetID > 0
	  BEGIN
	  INSERT INTO #baseTable (AssetID,AssetClassTypeID,TimeStamp,[Type],Status,Value)
	  SELECT 
		flow.AssetID, 
	    a.AssetClassTypeID, 
		flow.[Timestamp], 
		flow.[type], 
		flow.[status], 
		flow.value
		from CapPrior.tProjectCashFlow flow 
		INNER JOIN Asset.ufnGetAssetIdsForUserStartingAt(@userID, @assetID) branch ON flow.AssetID = branch.AssetID 
		INNER JOIN Asset.tAsset a ON flow.AssetID = a.AssetID 
		WHERE  a.AssetClassTypeID = @assetClassTypeID 
			AND flow.[Type] IN (@valueType) 
			AND (@ignoreIDList = 1 OR a.AssetID IN (SELECT id FROM @idList))
			OPTION (RECOMPILE)
		;
	  END
	ELSE IF (SELECT count(*) from @descendants) > 0 
	  BEGIN
	  INSERT INTO #baseTable (AssetID,AssetClassTypeID,TimeStamp,[Type],Status,Value)
	  SELECT 
		flow.AssetID, 
	    a.AssetClassTypeID, 
		flow.[Timestamp], 
		flow.[type], 
		flow.[status], 
		flow.value
		from CapPrior.tProjectCashFlow flow 
		INNER JOIN @descendants branch ON flow.AssetID = branch.id 
		INNER JOIN Asset.tAsset a ON flow.AssetID = a.AssetID 
		WHERE  a.AssetClassTypeID = @assetClassTypeID 
			AND flow.[Type] IN (@valueType) 
			AND (@ignoreIDList = 1 OR a.AssetID IN (SELECT id FROM @idList))
			OPTION (RECOMPILE)
	  END 
	

	DECLARE @assetIDs Base.tpIntList
	INSERT INTO @assetIDS(id)
	SELECT distinct AssetID FROM #baseTable 

	SELECT 
		AssetID, 
		[Timestamp], 
		[status], 
		value 
	INTO #myTable 
	FROM CapPrior.ufnProjectStatuses(@assetIDs, @indicator, @valueType)
	;
	
	if 1 = @excludeNinetyNinePlans
	BEGIN
		update #myTable
		set [Status] = CapPrior.ufnGetLastCashflowStatusByDate(AssetID,@valueType,[Timestamp])
		where [Status] in (99,-99);
	END
	if 1 = @excludeNinetyNineActuals
	BEGIN 
		update #myTable
		set [Value] = CapPrior.ufnGetLastCashflowValueByDate(AssetID,@valueType,[Timestamp])
		where [Value] in (99,-99);
	END
		
	--count total assets. If an asset has cash flow records at all, count it. Per guidance from Kevin Chael, 12/4/2018
	declare @cumPlanTotal int;
	declare @cumActualTotal int;
	select @cumPlanTotal = count(distinct AssetID) 
	from #baseTable
	;
	select @cumActualTotal = count(distinct AssetID) 
	from #baseTable
	; 

	DECLARE @distinctStatusNums int;
	DECLARE @distinctValueNums int;
	DECLARE @anyPlansAtAll bit = 0;
	DECLARE @anyActualsAtAll bit = 0;

	select @distinctStatusNums = count(distinct [Status]) 
	from #baseTable
	where	[Status] is not null
	AND	[Status] <> 99
	AND	[Status] <> -99;
	select @distinctValueNums = count(distinct [Value]) 
	from #baseTable
	where [Value] is not null
	AND	[Value] <> 99
	AND	[Value] <> -99;

	if @distinctStatusNums > 0
	BEGIN
		SET @anyPlansAtAll = 1;
	END
	if @distinctValueNums > 0
	BEGIN
		SET @anyActualsAtAll = 1;
	END


	DECLARE @total int; 
	SELECT @total = (SELECT COUNT (*) FROM #myTable ); 

	
	select @assetClassTypeID as AssetClassTypeID
		,  @total AS Total 
		,  @cumActualTotal AS CumActualTotal 
		,  @cumPlanTotal as CumPlanTotal
		,  @anyPlansAtAll as AnyPlansAtAll
		,  @anyActualsAtAll as AnyActualsAtAll
	;

	select fulledActuals.id as StatusNumber
		,  fulledActuals.value
		,  fulledActuals.[CountActualInStatus]
		,  fulledActuals.CountPercentOfTotalActual
		,  fulledStatus.[status]
		,  fulledStatus.[CountPlanInStatus]
		,  fulledStatus.CountPercentOfTotalPlan
	from 
	(	SELECT * FROM
		(	SELECT 
			  value 
			, COUNT (*) AS [CountActualInStatus] 
			, CASE WHEN @total = 0.0 THEN 5 ELSE CONVERT(real, COUNT (*)) / CONVERT(real, @total) * 100 END CountPercentOfTotalActual 
			FROM #myTable 
			GROUP BY value --don't need to group by asset class type ID, there's only one asset class type ID in myTable.
		) actuals
		full join   @statusList l  on actuals.value = l.id
	) fulledActuals
	inner join
	(	SELECT * FROM 
		(	SELECT 
 			  [Status] 
			, COUNT (*) AS [CountPlanInStatus] 
			, CASE WHEN @total = 0.0 THEN 5 ELSE CONVERT(real, COUNT (*)) / CONVERT(real, @total) * 100 END CountPercentOfTotalPlan 
			FROM #myTable 
			GROUP BY [Status] --don't need to group by asset class type ID, there's only one asset class type ID in myTable.
		) [plan]
		full join  @statusList l  on [plan].[Status] = l.id
	) fulledStatus 
		on fulledActuals.id = fulledStatus.id
	ORDER BY fulledActuals.id asc; 


	drop table #baseTable;
	DROP TABLE #myTable; 

END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetProjectCashFlowDetailSummarySpecificStatuses] TO [TEUser]
    AS [dbo];

