﻿CREATE PROCEDURE [CapPrior].[spGetLatestMappedRiskScoresForAssetDescendants]
	@securityUserID int,
	@startingAssetID int,
	@assetMapping [Risk].[tpAssetRiskScoreTypeMapping] READONLY,
	@xAxisDesc varchar(255),
	@yAxisDesc varchar(255),
	@labelDesc varchar(255),
	@timeStamp datetime
AS
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tmpDescendantsAssetIDs') IS NOT NULL
		DROP table #tmpDescendantsAssetIDs
	CREATE table #tmpDescendantsAssetIDs
	(
		[AssetID]     INT              NOT NULL UNIQUE
	)

	IF OBJECT_ID('tempdb..#assetRiskScoreTypeMapping') IS NOT NULL
		DROP table #assetRiskScoreTypeMapping
	CREATE table #assetRiskScoreTypeMapping (
		[AssetId]     INT              NOT NULL UNIQUE,
		[XaxisType]   NVARCHAR (255)   NOT NULL,
		[YaxisType]   NVARCHAR (255)   NOT NULL,
		[LabelType]   NVARCHAR (255)
	);

	INSERT INTO #tmpDescendantsAssetIDs
		SELECT [AssetID] FROM [Asset].[ufnGetAssetIdsForUserStartingAt](@securityUserID, @startingAssetID);

	-- assets with mapping
	INSERT INTO #assetRiskScoreTypeMapping([AssetId],[XaxisType],[YaxisType],[LabelType])
		SELECT a.AssetID, m.XaxisType, m.YaxisType, m.LabelType
			FROM Asset.tAsset a
			JOIN #tmpDescendantsAssetIDs d on d.AssetID = a.AssetID
			JOIN Asset.tAssetClassType ct on a.AssetClassTypeId = ct.AssetClassTypeID
			JOIN @assetMapping m ON ct.AssetClassTypeKey = m.AssetClassType

	-- assets with no mapping
	INSERT INTO #assetRiskScoreTypeMapping([AssetId],[XaxisType],[YaxisType],[LabelType])
		SELECT d.AssetID, @xAxisDesc, @yAxisDesc, @labelDesc
			FROM #tmpDescendantsAssetIDs d
			LEFT JOIN #assetRiskScoreTypeMapping mI ON mI.AssetId = d.AssetID
			WHERE mI.AssetId IS NULL

	SELECT s.AssetID, s.Timestamp, s.XaxisValue, s.YaxisValue, s.LabelValue
	FROM (
		SELECT t.AssetID, cfYaxis.Timestamp, cfXaxis.Value as [XaxisValue], cfYaxis.Value as [YaxisValue], cfLabel.Value as [LabelValue]
			, ROW_NUMBER() OVER(PARTITION BY t.AssetID ORDER BY cfYaxis.Timestamp DESC) as [rk]
		FROM #assetRiskScoreTypeMapping t
		JOIN #tmpDescendantsAssetIDs A ON A.AssetID = t.AssetID
		JOIN [CapPrior].[tProjectCashFlow] cfYaxis ON cfYaxis.AssetID = t.AssetID AND cfYaxis.Type=t.YaxisType AND cfYaxis.Timestamp <= @timeStamp
		JOIN [CapPrior].[tProjectCashFlow] cfXaxis ON cfXaxis.AssetID = cfYaxis.AssetID AND cfXaxis.Type=t.XaxisType AND cfXaxis.Timestamp = cfYaxis.Timestamp
		LEFT JOIN [CapPrior].[tProjectCashFlow] cfLabel ON cfLabel.AssetID = cfXaxis.AssetID AND cfLabel.Type=t.LabelType AND cfLabel.Timestamp = cfXaxis.Timestamp
	) s WHERE s.rk = 1;

	RETURN
END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetLatestMappedRiskScoresForAssetDescendants] TO [TEUser]
    AS [dbo];
GO