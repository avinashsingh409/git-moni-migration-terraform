﻿-- =============================================
-- Author:		Ryan Irwin
-- Create date: 4/23/2018
-- Description:	Procedure to fetch cumulative 
--  cash flow status changes for assets of a given
--  class type under the specified asset
-- =============================================
CREATE PROCEDURE [CapPrior].[spGetCumulativeCashFlowStatusChanges]
	-- Add the parameters for the stored procedure here
  @assetID int 
 ,@type nvarchar(255) 
 ,@assetClassTypeID int
 ,@status int 
 ,@startDate datetime	--must be before @currentDate
 ,@currentDate datetime	--can be anywhere - does not have to be between @startDate and @endDate.
 ,@endDate datetime		--must be after @currentDate
 ,@useGreaterThanOrEqual bit = 0
 ,@idList Base.tpIntList READONLY

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	if @assetID is null OR @assetID <= 0 RAISERROR ('@assetID must be >= 0',18,1);
	if @startDate is null OR @endDate is null OR @startDate > @endDate RAISERROR ('@endDate must be after @startDate',18,2);
	if @currentDate is null RAISERROR ('@currentDate cannot be null',18,3);
	if @assetClassTypeID is null OR @assetClassTypeID <= 0 RAISERROR ('@assetClassTypeID must be >= 0',18,4);

	declare @assetIDs table (	--either we'll pass in the asset list, or a set of parameters that'll be used to generate said list.
	assetID int
	);
	
	if (Select count(*) from @idList) = 0 
	BEGIN
			print 'No filter IDs';
			insert into @assetIDs
			SELECT distinct a.AssetID  
            FROM Asset.ufnGetAssetTreeBranch(@assetID) branchIDs
			INNER JOIN asset.tAsset a on branchIDs.AssetID = a.AssetID
			WHERE a.AssetID <> @assetID
			AND a.AssetClassTypeID = @assetClassTypeID		
	end
	else
	begin
			print 'filter IDs';
			SELECT distinct a.AssetID  
            FROM Asset.ufnGetAssetTreeBranch(@assetID) branchIDs
			INNER JOIN asset.tAsset a on branchIDs.AssetID = a.AssetID
			inner join @idList id on a.assetID = id.id
			WHERE a.AssetID <> @assetID
			AND a.AssetClassTypeID = @assetClassTypeID		
	end

	
	--return table
	declare @returnData table(
		[Timestamp] datetime,
		[ActualTotal] int,
		[PlannedTotal] int,
		[ForecastTotal] int
	)

--functional code
	declare @pastTimestamps table (
		ts datetime
		);
	declare @futureTimestamps table (
		ts datetime
		);

	--If the start date is before the current date, include the start date 
	--as one of the past timestamps to fetch data for.
	if @startDate < @currentDate
	BEGIN
		Insert into @pastTimestamps
		values (@startDate)
	END

	--If currentDate is greater than startDate, we have past timestamps to query.
	--If not, then this won't add any timestamps, and that's fine.
	insert into @pastTimestamps
	select distinct [Timestamp] 
	from CapPrior.tProjectCashFlow pcf
	inner join @assetIDs a on pcf.AssetID = a.assetID
	where pcf.[Type] = @type
	AND pcf.Timestamp >= @startDate
	and pcf.Timestamp < @currentDate
	order by [Timestamp]
	;

	--if the end date is also before the current date, include the end date as one 
	--of the past timestamps to fetch data for (as we'll have no future data at all).
	if @endDate < @currentDate
	BEGIN
		Insert into @pastTimestamps
		values (@endDate)
	END
	--if the start date is after the current date, include the start date as one 
	--of the future timestamps to fetch data for (as we'll have no past data at all).
	if @startDate > @currentDate
	BEGIN
		Insert into @futureTimestamps
		values (@startDate)
	END
	
	--If currentDate is less than endDate, we have future timestamps to query.
	--If not, then this won't add any timestamps, and that's fine.
	insert into @futureTimestamps
	select distinct [Timestamp] 
	from CapPrior.tProjectCashFlow pcf
	inner join @assetIDs a on pcf.AssetID = a.assetID
	where pcf.[Type] = @type
	AND pcf.Timestamp > @currentDate
	and pcf.Timestamp <= @endDate
	order by [Timestamp]
	;
	--if the end date is also after the current date, include the end date 
	--as one of the future timestamps to fetch data for.
	if @endDate > @currentDate
	BEGIN
		Insert into @futureTimestamps
		values (@endDate)
	END

	declare @ts datetime;
	DECLARE tsPastCursor CURSOR FOR
	SELECT distinct ts from @pastTimestamps
	order by ts;
	DECLARE tsFutureCursor CURSOR FOR
	SELECT distinct ts from @futureTimestamps
	order by ts;

	declare @totalActual int,
			@totalForecast int,
			@totalPlanned int;

	open tsPastCursor;
	fetch next from tsPastCursor into @ts;

	while @@FETCH_STATUS = 0
	BEGIN
	
		if @useGreaterThanOrEqual = 1 
		BEGIN
			select @totalActual = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowValueByDate](a.assetID,@type,@ts) >= @status
	
			select @totalPlanned = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowStatusByDate](a.assetID,@type,@ts) >= @status
		END
		else
		BEGIN
			select @totalActual = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowValueByDate](a.assetID,@type,@ts) > @status
	
			select @totalPlanned = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowStatusByDate](a.assetID,@type,@ts) > @status
		END

		insert into @returnData values (@ts, @totalActual, @totalPlanned, null)
	
	fetch next from tsPastCursor into @ts;
	END
	close tsPastCursor;
	deallocate tsPastCursor;
	
	--if the current date is between the start and end times, capture it as a distinct data point
	--for both the forecast and actuals series.
	if @currentDate > @startDate AND @currentDate < @endDate
	BEGIN
	--Forecast is actuals into the future, starting with the current date. 
		if @useGreaterThanOrEqual = 1 
		BEGIN
			select @totalActual = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowValueByDate](a.assetID,@type,@currentDate) >= @status
	
			select @totalPlanned = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowStatusByDate](a.assetID,@type,@currentDate) >= @status
		END
		else
		BEGIN
			select @totalActual = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowValueByDate](a.assetID,@type,@currentDate) > @status
	
			select @totalPlanned = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowStatusByDate](a.assetID,@type,@currentDate) > @status
		END
	--save @currentDate actuals as the last actuals and the first forecast.
		insert into @returnData values (@currentDate, @totalActual, @totalPlanned, @totalActual)
	END

	open tsFutureCursor;
	fetch next from tsFutureCursor into @ts;

	while @@FETCH_STATUS = 0
	BEGIN
	
		if @useGreaterThanOrEqual = 1 
		BEGIN
			select @totalForecast = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowValueByDate](a.assetID,@type,@ts) >= @status
	
			select @totalPlanned = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowStatusByDate](a.assetID,@type,@ts) >= @status
		END
		else
		BEGIN
			select @totalForecast = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowValueByDate](a.assetID,@type,@ts) > @status
	
			select @totalPlanned = count(distinct pcf.assetID)
			from CapPrior.tProjectCashFlow pcf
			inner join @assetIDs a on pcf.AssetID = a.assetID
			where pcf.Type = @type
			AND [CapPrior].[ufnGetLastCashflowStatusByDate](a.assetID,@type,@ts) > @status
		END

		insert into @returnData values (@ts, null, @totalPlanned, @totalForecast)
	
	fetch next from tsFutureCursor into @ts;
	END
	close tsFutureCursor;
	deallocate tsFutureCursor;


	select * from @returnData
	order by [Timestamp];
END
GO
GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetCumulativeCashFlowStatusChanges] TO [TEUser]
    AS [dbo];

