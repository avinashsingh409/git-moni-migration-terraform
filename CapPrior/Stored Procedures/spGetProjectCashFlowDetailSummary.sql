﻿


CREATE PROCEDURE [CapPrior].[spGetProjectCashFlowDetailSummary]
	@userID int ,
    @assetID int ,
    @valueType nvarchar(255),  
	@assetClassTypeID int,
	@indicator datetime,
	@ignoreIDList bit,
	@idList base.tpIntList READONLY,
	@descendants base.tpIntList READONLY,
	@excludeNinetyNineActuals bit = 1,
	@excludeNinetyNinePlans bit = 1
AS
BEGIN

	DECLARE @treeassets TABLE (AssetID int PRIMARY KEY not NULL);

	if (SELECT count(*) from @descendants) > 0 
	BEGIN
		INSERT INTO @treeassets
		SELECT id FROM @descendants; 
	END
	ELSE if(@assetID > 0)
	BEGIN
		INSERT INTO @treeassets
		SELECT AssetID FROM Asset.ufnGetAssetIdsForUserStartingAt(@userID, @assetID) ; 
	END
	ELSE
	BEGIN
		RAISERROR ('Either an asset ID or a set of descendents must be specified in order to fetch cash flow detail summaries.', 18, 1)
	END
	
	--only use the idList if we're specifically instructed to.
	if @ignoreIDList = 0 
	BEGIN
		 DELETE FROM @treeAssets WHERE AssetID NOT IN (SELECT id FROM @idList); 
	END

	SELECT 
		ROW_NUMBER() OVER (PARTITION BY flow.AssetID, flow.[type] ORDER BY flow.[Timestamp] DESC, flow.ProjectCashFlowID DESC) AS RowNumber, 
		flow.ProjectCashFlowID, 
		flow.AssetID, 
		a.GlobalID, 
		a.AssetDesc, 
		a.AssetClassTypeID, 
		portfolio.assetID PortfolioID, 
		portfolio.AssetDesc PortfolioDesc, 
		scenario.AssetID ScenarioID, 
		scenario.AssetDesc ScenarioDesc, 
		flow.UnitOfMeasure, 
		flow.[Timestamp], 
		flow.[type], 
		flow.[status], 
		flow.value 
	INTO #myTable 
	from CapPrior.tProjectCashFlow flow 
	INNER JOIN @treeassets branch ON flow.AssetID = branch.AssetID 
	INNER JOIN Asset.tAsset a ON flow.AssetID = a.AssetID 
	INNER JOIN Asset.tAsset portfolio ON a.ParentAssetID = portfolio.AssetID  
	INNER JOIN Asset.tAsset scenario ON portfolio.ParentAssetID = scenario.AssetID  
	WHERE   a.AssetClassTypeID = @assetClassTypeID 
		AND flow.[Type] IN (@valueType) 
		AND flow.Timestamp <= @indicator
		AND (flow.[Value] <> 99 OR @excludeNinetyNineActuals = 0)
		AND (flow.[Status] <> 99 OR @excludeNinetyNinePlans = 0)
	; 



	declare @cumTotal int;
	select @cumTotal = count(distinct flow.AssetID) 
	from CapPrior.tProjectCashFlow flow 
	INNER JOIN @treeassets branch ON flow.AssetID = branch.AssetID 
	INNER JOIN Asset.tAsset a ON flow.AssetID = a.AssetID 
	INNER JOIN Asset.tAsset portfolio ON a.ParentAssetID = portfolio.AssetID  
	INNER JOIN Asset.tAsset scenario ON portfolio.ParentAssetID = scenario.AssetID 
	WHERE a.AssetClassTypeID = @assetClassTypeID 
		AND flow.[Type] IN (@valueType) 
		AND (flow.[Value] <> 99 OR @excludeNinetyNineActuals = 0)
		AND (flow.[Status] <> 99 OR @excludeNinetyNinePlans = 0)
	; 



	DECLARE @total int; 
	SELECT @total = (SELECT COUNT (AssetClassTypeID) FROM #myTable WHERE RowNumber = 1); 



	SELECT 
		AssetClassTypeID 
		, value, 
		COUNT (RowNumber) AS [Count] 
		, @total AS Total 
		, @cumTotal AS CumTotal 
		, CASE WHEN @total = 0.0 THEN 5 ELSE CONVERT(real, COUNT (RowNumber)) / CONVERT(real, @total) * 100 END CountPercentOfTotal 
	FROM #myTable 
	WHERE RowNumber = 1 
	GROUP BY AssetClassTypeID, value 
	ORDER BY AssetClassTypeID, value; 



	DROP TABLE #myTable; 
END
;
GO
GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetProjectCashFlowDetailSummary] TO [TEUser]
    AS [dbo];

