﻿CREATE PROCEDURE [CapPrior].[spUpsertCashFlowData]
	   @rows CapPrior.tpCashFlowFull READONLY
AS
BEGIN
	MERGE INTO CapPrior.tProjectCashFlow as d
	USING @rows as s
		ON ((d.ValueTagID = s.ValueTagID) OR (ISNULL(d.ValueTagID,s.ValueTagID) IS NULL))
		AND ((d.StatusTagID = s.StatusTagID) OR (ISNULL(d.StatusTagID,s.StatusTagID) IS NULL))
		AND (d.Timestamp = s.Timestamp)
	WHEN MATCHED THEN
		UPDATE SET d.Value = s.Value, d.Status = s.Status
	WHEN NOT MATCHED THEN
		INSERT ([UnitOfMeasure], [AssetID], [Timestamp], [Type], [Status], [Value], [ValueTagID], [StatusTagID] )
		VALUES (s.[UnitOfMeasure], s.[AssetID], s.[Timestamp], s.[Type], s.[Status], s.[Value], s.[ValueTagID], s.[StatusTagID]);
END
GO


GRANT EXECUTE
    ON OBJECT::[CapPrior].[spUpsertCashFlowData] TO [TEUser]
    AS [dbo];
