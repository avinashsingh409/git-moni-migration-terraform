﻿-- =============================================
-- Author:		Ryan Irwin
-- Create date: 4/23/2018
-- Description:	Procedure to fetch cumulative 
--  cash flow status changes for assets of a given
--  class type under the specified asset
-- =============================================
CREATE PROCEDURE [CapPrior].[spGetCumulativeCashFlowStatusChanges] 
	-- Add the parameters for the stored procedure here
  @assetID int = -1
 ,@nodeAssetIDs Base.tpIntList READONLY
 ,@type nvarchar(255) 
 ,@assetClassTypeID int
 ,@status int 
 ,@startDate datetime	--must be before @currentDate
 ,@currentDate datetime	--can be anywhere - does not have to be between @startDate and @endDate.
 ,@endDate datetime		--must be after @currentDate
 ,@useGreaterThanOrEqual bit = 0
 ,@idList Base.tpIntList READONLY
 ,@UserId int
 ,@ExcludeNinetyNineActuals bit = 1
 ,@ExcludeNinetyNinePlans bit = 1

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	if @startDate is null OR @endDate is null OR @startDate > @endDate RAISERROR ('@endDate must be after @startDate',18,2);
	if @currentDate is null RAISERROR ('@currentDate cannot be null',18,3);
	if @assetClassTypeID is null OR @assetClassTypeID <= 0 RAISERROR ('@assetClassTypeID must be >= 0',18,4);

	declare @Now datetime = GETDATE(); -- this is used to demarcate actuals and forecasted instead of @currentDate
	
	IF OBJECT_ID('tempdb..#TMP_Cashflow') IS NOT NULL  DROP TABLE #TMP_Cashflow;
	CREATE TABLE #TMP_Cashflow
	(
		[UnitOfMeasure] [nvarchar](255) NOT NULL,
		[AssetID] [int] NOT NULL,
		[Timestamp] [datetime] NOT NULL,
		[Type] [nvarchar](255) NOT NULL,
		[Status] [int] NOT NULL,
		[Value] [float] NOT NULL
	)

	CREATE INDEX idx1 ON #TMP_Cashflow (AssetID,timestamp,type)

	if (@assetID > 0)
	  begin
	  insert into #TMP_Cashflow (UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value])
	  select cf.UnitOfMeasure, cf.AssetID, cf.[Timestamp], cf.[Type], cf.[Status], cf.[Value]	  
	  FROM Asset.ufnGetAssetIdsForUserStartingAt(@UserId, @assetID) branchIDs
	  INNER JOIN asset.tAsset a on branchIDs.AssetID = a.AssetID
	  INNER JOIN CapPrior.tProjectCashFlow cf on a.AssetID = cf.AssetID
	  WHERE a.AssetClassTypeID = @assetClassTypeID OPTION (RECOMPILE)
	  end
	else
	  begin
	  insert into #TMP_Cashflow (UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value])
	  select cf.UnitOfMeasure, cf.AssetID, cf.[Timestamp], cf.[Type], cf.[Status], cf.[Value]	  
	  FROM @nodeAssetIDs branchIDs
	  INNER JOIN asset.tAsset a on branchIDs.id = a.AssetID
	  INNER JOIN CapPrior.tProjectCashFlow cf on a.AssetID = cf.AssetID
	  WHERE a.AssetClassTypeID = @assetClassTypeID OPTION (RECOMPILE)
	  end

	delete from #TMP_Cashflow where AssetID = @assetID;

	if (Select count(*) from @idList) = 0 
	BEGIN
			print 'No filter IDs';	
	end
	else
	begin
			print 'filter IDs';
			delete from #TMP_Cashflow where AssetID not in (select id from @idList);
	end

	if @ExcludeNinetyNineActuals = 1
	begin
		delete from #TMP_Cashflow where [Value] = 99;
	end
	if @ExcludeNinetyNinePlans = 1
	begin
		delete from #TMP_Cashflow where [Status] = 99;
	end

	declare @assetIDs as base.tpintlist
	delete from @assetIDs;
	insert into @assetIDs (id) select distinct AssetID from #TMP_Cashflow;

	--return table
	declare @returnData table(
		[Timestamp] datetime,
		[ActualTotal] int,
		[PlannedTotal] int,
		[ForecastTotal] int
	)

	--functional code
	declare @pastTimestamps table (
		ts datetime
		);
	declare @futureTimestamps table (
		ts datetime
		);

	--If the start date is before the current date, include the start date 
	--as one of the past timestamps to fetch data for.
	if @startDate < @currentDate
	BEGIN
		Insert into @pastTimestamps
		values (@startDate)
	END

	--If currentDate is greater than startDate, we have past timestamps to query.
	--If not, then this won't add any timestamps, and that's fine.
	insert into @pastTimestamps
	select distinct [Timestamp] 
	from CapPrior.tProjectCashFlow pcf
	inner join @assetIDs a on pcf.AssetID = a.id
	where pcf.[Type] = @type
	AND pcf.Timestamp >= @startDate
	and pcf.Timestamp < @currentDate
	and pcf.Timestamp < @endDate	--if @currentDate is after @endTime, we need to trim back the results here accordingly
	order by [Timestamp]
	;

	--if the end date is also before the current date, include the end date as one 
	--of the past timestamps to fetch data for (as we'll have no future data at all).
	if @endDate < @currentDate
	BEGIN
		Insert into @pastTimestamps
		values (@endDate)
	END
	--if the start date is after the current date, include the start date as one 
	--of the future timestamps to fetch data for (as we'll have no past data at all).
	if @startDate > @currentDate
	BEGIN
		Insert into @futureTimestamps
		values (@startDate)
	END
	
	--If currentDate is less than endDate, we have future timestamps to query.
	--If not, then this won't add any timestamps, and that's fine.
	insert into @futureTimestamps
	select distinct [Timestamp] 
	from CapPrior.tProjectCashFlow pcf
	inner join @assetIDs a on pcf.AssetID = a.id
	where pcf.[Type] = @type
	and pcf.Timestamp > @startDate		--if @startDate is later than @currentDate, we need to trim back these results accordingly.
	AND pcf.Timestamp > @currentDate
	and pcf.Timestamp <= @endDate
	order by [Timestamp]
	;

	--if the end date is also after the current date, include the end date 
	--as one of the future timestamps to fetch data for.
	if @endDate > @currentDate
	BEGIN
		Insert into @futureTimestamps
		values (@endDate)
	END

	declare @allTimestamps table(
		ts datetime
	);

	insert into @allTimeStamps (ts) select ts from @pastTimestamps;
	--if the current date is between the start and end times, capture it as a distinct data point
	--for both the forecast and actuals series.
	if @currentDate > @startDate AND @currentDate < @endDate
	begin
		insert into @allTimestamps (ts) select @currentDate;
	end
	insert into @allTimeStamps (ts) select ts from @futureTimestamps;
	-- if now is between the start and end times, capture it as a distinct data point
	-- for both the forecast and actuals series
	if @Now > @startDate AND @Now < @endDate
	begin
		insert into @allTimestamps (ts) select @Now;
	end

	IF OBJECT_ID('tempdb..#tmp_assetIDsAndStuff') IS NOT NULL  DROP TABLE #tmp_assetIDsAndStuff;

	create table #tmp_assetIDsAndStuff
	(
		timeStamp DateTime,
		assetID int,
		lastValue float NULL,
		lastStatus float NULL
	)

	create index idx_1 on #tmp_assetIDsAndStuff (timeStamp,assetID)
	
	insert into #tmp_assetIDsAndStuff(timeStamp,assetID) select ts,id from @assetIDs, @allTimestamps

	CREATE TABLE #tmp_max
	(
	  assetID int,
	  timestamp DATETIME,
	  maxValue float NULL,
	  maxStatus float NULL
	)

	create index idx_1 on #tmp_max (timeStamp,assetID)
	
	insert into #tmp_max
	select a.assetID,a.timestamp,
	MAX(case when [Value] not in (-99,99) then b.[Value] else null end) as maxValue,
	MAX(case when [Status] not in (-99,99) then b.[Status] else null end) as maxStatus
	 from #tmp_assetIDsAndStuff a 
	join CapPrior.tProjectCashFlow b on a.assetID = b.AssetID 
	where 
		[Type] = @type
		and b.[Timestamp] <= a.timestamp
		group by a.assetID,a.timestamp

	update a set
	-- valueTimeStamp = b.maxValueTimeStamp,statusTimeStamp = b.maxStatusTimeStamp,
	lastValue = b.maxValue,lastStatus = b.maxStatus FROM #tmp_assetIDsAndStuff a 
	JOIN #tmp_max b ON a.assetID = b.assetID and a.timeStamp=b.timestamp -- OPTION (RECOMPILE)

	declare @ActualPlanValues table(
		TS datetime not null
		, AssetID int not null
		, Actual int not null
		, [Plan] int not null
	);

	insert into @ActualPlanValues(TS, AssetID, Actual, [Plan]) select x.timeStamp,x.assetID,isnull(x.lastValue,-1),isnull(x.lastStatus,-1) from #tmp_assetIDsAndStuff x
	
	-- get all timestamps into the return table
	insert into @returnData ([Timestamp]) select ts from @allTimestamps order by ts;

	if @useGreaterThanOrEqual = 0
	begin
		-- update actuals
		update r
		set ActualTotal = d.ActualCount
		from @returnData r
		join
		(
		select apv.TS, count (AssetID) as ActualCount
		from @ActualPlanValues apv
		where apv.ts <= @Now and apv.Actual > @status
		group by apv.ts
		) d on r.[Timestamp] = d.TS;

		-- update forecasted
		update r
		set ForecastTotal = d.ForecastCount
		from @returnData r
		join
		(
		select apv.TS, count (AssetID) as ForecastCount
		from @ActualPlanValues apv
		where apv.ts >= @Now and apv.Actual > @status
		group by apv.ts
		) d on r.[Timestamp] = d.TS;

		-- update plans
		update r
		set PlannedTotal = d.PlannedCount
		from @returnData r
		join
		(
		select apv.TS, count (AssetID) as PlannedCount
		from @ActualPlanValues apv
		where apv.[Plan] > @status
		group by apv.ts
		) d on r.[Timestamp] = d.TS;
	end
	else
	begin
	-- update actuals
		update r
		set ActualTotal = d.ActualCount
		from @returnData r
		join
		(
		select apv.TS, count (AssetID) as ActualCount
		from @ActualPlanValues apv
		where apv.ts <= @Now and apv.Actual >= @status
		group by apv.ts
		) d on r.[Timestamp] = d.TS;

		-- update forecasted
		update r
		set ForecastTotal = d.ForecastCount
		from @returnData r
		join
		(
		select apv.TS, count (AssetID) as ForecastCount
		from @ActualPlanValues apv
		where apv.ts >= @Now and apv.Actual >= @status
		group by apv.ts
		) d on r.[Timestamp] = d.TS;

		-- update plans
		update r
		set PlannedTotal = d.PlannedCount
		from @returnData r
		join
		(
		select apv.TS, count (AssetID) as PlannedCount
		from @ActualPlanValues apv
		where apv.[Plan] >= @status
		group by apv.ts
		) d on r.[Timestamp] = d.TS;
	end

	select * from @returnData
	order by [Timestamp];
END

GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetCumulativeCashFlowStatusChanges] TO [TEUser]
    AS [dbo];
GO
