﻿CREATE PROCEDURE [CapPrior].[spCreateNewVariablesAndMaps]
	 @tagIds as base.tpintlist READONLY
AS
BEGIN	

-- 
-- This proc adds new custom variables and applies those variables to tag maps for the list of tags provided.
-- 

SET NOCOUNT ON;

DECLARE @NewVariables AS TABLE
(
   VariableName nvarchar(255)   
)

insert into @NewVariables (VariableName)
select distinct replace(a.TagDesc,' ','') as VariableName from ProcessData.tTag a join ProcessData.tServer b 
on a.PDServerID = b.PDServerID JOIN @tagIds ids on a.PDTagID = ids.id
where b.PDServerTypeID = 10 
and ServerDesc like 'CapPrior%' and 
replace(a.TagDesc,' ','') not in (select a.VariableName from ProcessData.tVariableType a);

INSERT INTO ProcessData.tVariableType (VariableDesc,VariableAbbrev,VariableName,IsExempt) 
SELECT VariableName,VariableName,VariableName,1 FROM @NewVariables

-- update tag maps for CapPrior tags that have not been mapped yet
update C set C.VariableTypeID = d.PDVariableID
from ProcessData.tTag a 
join ProcessData.tServer b on a.PDServerID = b.PDServerID 
join ProcessData.tAssetVariableTypeTagMap c on a.PDTagID = c.TagID 
JOIN ProcessData.tVariableType d on replace(a.TagDesc,' ','') = d.VariableName
JOIN @tagIds ids on a.PDTagID = ids.id
where b.PDServerTypeID = 10 
and ServerDesc like 'CapPrior%' and c.VariableTypeID IS NULL

END

GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spCreateNewVariablesAndMaps] TO [TEUser]
    AS [dbo];
