﻿CREATE PROCEDURE [CapPrior].[spGetCashflowChange]
	@UserID int,
	@AssetID int,
	@CFType nvarchar(255),
	@ProjectTimeStamp datetime

AS BEGIN

declare @cashFlowID as int
declare @timeStamp as datetime

    -- Select the latest change being made to Project Status
	SELECT TOP 1 @cashFlowID = ProjectCashFlowID, @timeStamp = [TimeStamp]
		FROM CapPrior.tProjectCashFlow 
		WHERE AssetID = @AssetID AND [Type] = @CFType AND [TimeStamp] <= @ProjectTimeStamp ORDER BY [Timestamp] Desc

 	-- Select the Value, Status PRIOR to lastest change. 
	-- Note: ProjectCashFlowID and TimeStamp wil be based of the latest change to keep track of change made in tProjectCashflowAudit
	SELECT @cashFlowID as ProjectCashFlowID, a.AssetID, [Type], a.UnitOfMeasure, a.Value,a.Status,@timeStamp as [Timestamp] 
	FROM (
		SELECT TOP 1 ProjectCashFlowID, AssetID, [Type], UnitOfMeasure,Value,Status,[TimeStamp]
		FROM CapPrior.tProjectCashFlow 
		WHERE AssetID = @AssetID AND [Type] = @CFType AND [TimeStamp] < @timeStamp ORDER BY [Timestamp] DESC) a 
END
GO


GRANT EXECUTE
    ON OBJECT::[CapPrior].[spGetCashflowChange] TO [TEUser]
    AS [dbo];

