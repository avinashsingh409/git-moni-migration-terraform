﻿CREATE PROCEDURE [CapPrior].[spSaveProjectCashFlows]
	@UserID int,
	@Flows CapPrior.tpProjectCashFlow READONLY,
	@DeleteExistingRows bit = 0,
	@Delete99Markers bit = 1, --This is to identify if a 99 value be retained in the DB or not.
	@Save99Markers bit = 0 --This is to identify if a 99 value be added in the DB or not. 99 values can only be added through import. Thus, if @Save99Markers = 1, values are from the import.
AS
BEGIN
	declare @assets as base.tpintlist

	insert into @assets select distinct assetid from @Flows
	
	delete from @assets where Asset.ufnDoesUserHaveAccessToAsset(id,@UserID,-1)!=1

	declare @cnt as int
	select @cnt = COUNT(*) from @Flows

	if EXISTS(select * from @assets)  --if any are left after the delete 
	BEGIN
		DECLARE @OK as int

		DECLARE @UnitOfMeasure as NVARCHAR(255)
		DECLARE @AssetID as int
		DECLARE @Timestamp as DATETIME
		DECLARE @Type as NVARCHAR(255)
		DECLARE @Status as int
		DECLARE @Value as float

		DECLARE curUP CURSOR LOCAL	
			FOR SELECT UnitOfMeasure,AssetID,TimeStamp,Type,Status,Value FROM @Flows
		OPEN curUP;
		FETCH NEXT FROM curUP INTO @UnitOfMeasure,@AssetID,@Timestamp,@Type,@Status,@Value		

		SET @OK = 1
		WHILE @OK = 1
		BEGIN
			IF @@FETCH_STATUS =  0
			BEGIN
				BEGIN TRY 
				DECLARE @flow as CapPrior.tpProjectCashFlow
				DELETE FROM @flow
				INSERT INTO @flow (UnitOfMeasure,AssetID,TimeStamp,Type,Status,Value) 
								VALUES (@UnitOfMeasure,@AssetID,@TimeStamp,@Type,@Status,@Value)
			  
				SET XACT_ABORT OFF			--don't abort main transaction on error inside SP
				EXEC [CapPrior].[spSekoiaMapStatusChangeNotification] @flow, @UserID, 1, 0;
				SET XACT_ABORT ON

				END TRY
				BEGIN CATCH
					PRINT 'Caught error in map status change automated event'
					PRINT ERROR_MESSAGE()
				END CATCH
				FETCH NEXT FROM curUP INTO @UnitOfMeasure,@AssetID,@Timestamp,@Type,@Status,@Value		
			END
			ELSE
			BEGIN
				SET @OK = 0
			END	 
		END
	END

	CLOSE curUP;
	DEALLOCATE curUP;

	IF @DeleteExistingRows = 1
	  BEGIN
	  IF @Delete99Markers = 1
	  BEGIN
		DELETE FROM CapPrior.tProjectCashFlow 
		  WHERE EXISTS (SELECT AssetID, [Type] FROM @Flows a JOIN @assets b ON a.AssetID = b.id
		  WHERE CapPrior.tProjectCashFlow.AssetID = a.AssetID AND CapPrior.tProjectCashFlow.Type = a.Type)
	  END
	  ELSE 
	  BEGIN
		DELETE FROM CapPrior.tProjectCashFlow 
		  WHERE EXISTS (SELECT AssetID, [Type] FROM @Flows a JOIN @assets b ON a.AssetID = b.id
		  WHERE CapPrior.tProjectCashFlow.AssetID = a.AssetID AND CapPrior.tProjectCashFlow.Type = a.Type
		  AND NOT (ABS(CapPrior.tProjectCashFlow.Value) = 99 AND ABS(CapPrior.tProjectCashFlow.Status) = 99))
	  END	
	END

	IF @Save99Markers = 1
	BEGIN
		INSERT INTO CapPrior.tProjectCashFlow (UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value])
		SELECT UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value] FROM @Flows a join @assets b on a.AssetID = b.id
	END
	ELSE 
	BEGIN
		INSERT INTO CapPrior.tProjectCashFlow (UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value])
		SELECT UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value] FROM @Flows a join @assets b on a.AssetID = b.id
		WHERE NOT (ABS(a.Value) = 99 AND ABS(a.Status) = 99)
	END	
	
END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spSaveProjectCashFlows] TO [TEUser]
    AS [dbo];
GO