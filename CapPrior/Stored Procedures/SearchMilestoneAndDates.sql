﻿CREATE PROCEDURE [CapPrior].[SearchMilestoneAndDates]
  -- Add the parameters for the stored procedure here
    @type    NVARCHAR(255),
    @assetId INT,
    @lookup  Base.TPINTSTRINGLIST READONLY
AS
  BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    DECLARE @results TABLE(      
      DisplayOrder INT,      
      AssetId   INT,
	  ID INT,
	  Label VARCHAR(255), 
	  PlanDate DateTime null,      
	  ActualDate DateTime null
    )

 DECLARE @statuses TABLE(            
      TimeStamp DATETIME,
      Status    INT   
    )
	
 DECLARE @values TABLE(     
      TimeStamp DATETIME,
      Value    INT  
    )

 DECLARE @values_audit TABLE(            
      TimeStamp DATETIME,
      Value    INT   
    )

insert into @results (DisplayOrder,AssetId,ID,Label) select DisplayOrder,@assetId,Id,Label  from @lookup

-- Set milestones based on the earliest date. This is a legacy method PRIOR to 
-- 1)allowing user to change milestone dates to past, present and future
-- 2)fixing audit log bug whereby changes made to CapPrior.tProjectCashFlow was not properly stored in CapPrior.tProjectCashFlowAudit
insert into @statuses select MIN(Timestamp) as TimeStamp,status from CapPrior.tProjectCashFlow a join @lookup b on a.Status = b.Id where AssetID = @assetId and [Type] = @type group by Status
insert into @values select MIN(Timestamp) as TimeStamp,value from CapPrior.tProjectCashFlow a join @lookup b on a.Value = b.Id where AssetID = @assetId and [Type] = @type group by Value

update A set PlanDate = b.TimeStamp from @results a join @statuses b on a.ID = b.Status
update A set ActualDate = b.TimeStamp from @results a join @values b on a.ID = b.Value

-- If audit log contains record of changes made to Actual status, set milestone dates based on latest change date in audit log
-- No ability to change the Planned status yet, so use the earliest Budget date per legacy code
insert into @values_audit 
SELECT TimeStamp, Value FROM    
	(SELECT a.TimeStamp as TimeStamp, a.Value as Value, c.TimeStamp as ChangeDate
	,ROW_NUMBER() OVER ( PARTITION BY a.Value ORDER BY c.TimeStamp DESC ) RowNum
	FROM CapPrior.tProjectCashFlow a JOIN @lookup map on a.Value = map.Id
	JOIN CapPrior.tProjectCashFlowAudit b ON a.ProjectCashFlowID = b.ProjectCashFlowID 
	JOIN Audit.tAudit c ON b.AuditID = c.AuditID
	where a.AssetID = @assetId and a.Type = @type) X WHERE X.RowNum = 1;

update A set ActualDate = b.TimeStamp from @results a join @values_audit b on a.ID = b.Value

select DisplayOrder,AssetId,ID,Label,PlanDate,ActualDate from @results order by DisplayOrder
END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[SearchMilestoneAndDates] TO [TEUser]
    AS [dbo];

