﻿



CREATE PROCEDURE [CapPrior].[spSaveCashflowChange]
	@UserID int,
	@Audits CapPrior.tpCashFlowChange READONLY
AS BEGIN


  BEGIN TRANSACTION
  BEGIN TRY

	  EXEC Audit.InsertAuditEntry @UserID, @Audits

	  INSERT INTO CapPrior.tProjectCashflowAudit (AuditId, ProjectCashflowId, UnitOfMeasure, AssetID, [TimeStamp], [Type], PreviousValue, PreviousStatus)
	  SELECT b.AuditId, a.ProjectCashFlowID, a.UnitOfMeasure, a.AssetId, a.[TimeStamp], a.[Type], a.PreviousValue, a.PreviousStatus
	  FROM @Audits a
	  JOIN Audit.tAudit b on a.GlobalID = b.GlobalID
	  WHERE Asset.ufnDoesUserHaveAccessToAsset(a.AssetId, @UserID, -1) = 1;

  COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
  ROLLBACK TRANSACTION;
  DECLARE @ErrorMessage NVARCHAR(4000);
  DECLARE @ErrorSeverity INT;
  DECLARE @ErrorState INT;
  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();
  RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH


  SELECT b.AuditId, b.AuditTypeId, b.CreatedBy, a.ProjectCashFlowID, a.UnitOfMeasure, a.AssetId, a.[TimeStamp], a.[Type], a.PreviousValue, a.PreviousStatus, b.GlobalID FROM CapPrior.tProjectCashFlowAudit a JOIN Audit.tAudit b ON a.AuditId = b.AuditId JOIN @Audits c on b.GlobalID = c.GlobalID

END
GO
GRANT EXECUTE
    ON OBJECT::[CapPrior].[spSaveCashflowChange] TO [TEUser]
    AS [dbo];

