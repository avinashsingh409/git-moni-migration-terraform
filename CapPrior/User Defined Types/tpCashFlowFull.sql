﻿
CREATE TYPE [CapPrior].[tpCashFlowFull] AS TABLE(
	[UnitOfMeasure] [nvarchar](255) NOT NULL,
	[AssetID] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Type] [nvarchar](255) NOT NULL,
	[Status] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[ValueTagID]	INT			   NULL,
	[StatusTagID]	INT			   NULL);

GO
GRANT EXECUTE
    ON TYPE::[CapPrior].[tpCashFlowFull] TO [TEUser];

GO
