﻿CREATE TYPE [CapPrior].[tpCashFlowChange] AS TABLE (
    [AuditId]           INT              NOT NULL,
    [AuditTypeId]       INT              NOT NULL,
    [CreatedBy]         INT              NOT NULL,
    [ProjectCashFlowID] INT              NOT NULL,
    [UnitOfMeasure]     NVARCHAR (255)   NOT NULL,
    [AssetId]           INT              NOT NULL,
    [TimeStamp]         DATETIME         NOT NULL,
    [Type]              NVARCHAR (255)   NOT NULL,
    [PreviousValue]     FLOAT (53)       NOT NULL,
    [PreviousStatus]    INT              NOT NULL,
    [GlobalID]          UNIQUEIDENTIFIER NOT NULL);


GO
GRANT EXECUTE
    ON TYPE::[CapPrior].[tpCashFlowChange] TO [TEUser];

