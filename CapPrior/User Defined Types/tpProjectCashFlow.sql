﻿CREATE TYPE [CapPrior].[tpProjectCashFlow] AS TABLE (
    [UnitOfMeasure] NVARCHAR (255) NOT NULL,
    [AssetID]       INT            NOT NULL,
    [Timestamp]     DATETIME       NOT NULL,
    [Type]          NVARCHAR (255) NOT NULL,
    [Status]        INT            NOT NULL,
    [Value]         FLOAT (53)     NOT NULL);


GO
GRANT EXECUTE
    ON TYPE::[CapPrior].[tpProjectCashFlow] TO [TEUser];

