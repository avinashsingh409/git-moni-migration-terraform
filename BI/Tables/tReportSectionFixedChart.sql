﻿CREATE TABLE [BI].[tReportSectionFixedChart] (
    [ReportSectionFixedChartID] UNIQUEIDENTIFIER NOT NULL,
    [Content]                   NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [BI_tReportSectionFixedChart_PK] PRIMARY KEY CLUSTERED ([ReportSectionFixedChartID] ASC)
);

