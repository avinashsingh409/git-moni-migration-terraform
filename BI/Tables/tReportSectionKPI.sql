﻿CREATE TABLE [BI].[tReportSectionKPI] (
    [ReportSectionKPIID]        UNIQUEIDENTIFIER NOT NULL,
    [AssetVariableTypeTagMapID] INT              NOT NULL,
    [Background]                NVARCHAR (255)   NULL,
    [Summary]                   INT              NOT NULL,
    [Scenario] NVARCHAR(255) NULL, 
    CONSTRAINT [BI_tReportSectionKPI_PK] PRIMARY KEY CLUSTERED ([ReportSectionKPIID] ASC),
    CONSTRAINT [FK_BI_tReportSectionKPI_ProcessData_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE
);


