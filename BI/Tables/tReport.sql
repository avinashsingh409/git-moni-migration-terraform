﻿CREATE TABLE [BI].[tReport] (
    [ReportID]        INT              IDENTITY (1, 1) NOT NULL,
    [ReportDesc]      NVARCHAR (MAX)   NOT NULL,
    [ReportAbbrev]    NVARCHAR (255)   NOT NULL,
    [DisplayOrder]    INT              NOT NULL,
    [AssetID]         INT              NULL,
    [Title]           NVARCHAR (MAX)   NOT NULL,
    [Subtitle]        NVARCHAR (MAX)   NOT NULL,
    [CreatedByUserID] INT              NOT NULL,
    [ChangedByUserID] INT              NOT NULL,
    [CreateDate]      DATETIME         NOT NULL,
    [ChangeDate]      DATETIME         NOT NULL,
    [NodeID]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [BI_tReport_PK] PRIMARY KEY CLUSTERED ([ReportID] ASC),
    CONSTRAINT [FK_BI_tReport_AccessControl_tUser_Change] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_BI_tReport_AccessControl_tUser_Create] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_BI_tReport_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tReport_tAdHocNode] FOREIGN KEY ([NodeID]) REFERENCES [UIConfig].[tAdHocNode] ([NodeId])
);


GO
