﻿CREATE TABLE [BI].[tReportSectionChart] (
    [ReportSectionChartID] UNIQUEIDENTIFIER NOT NULL,
    [PDTrendID]            INT              NOT NULL,
    CONSTRAINT [BI_tReportSectionChart_PK] PRIMARY KEY CLUSTERED ([ReportSectionChartID] ASC),
    CONSTRAINT [FK_BI_tReportSectionChart_ProcessData_tTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);


