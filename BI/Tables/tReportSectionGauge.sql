﻿CREATE TABLE [BI].[tReportSectionGauge] (
    [ReportSectionGaugeID]      UNIQUEIDENTIFIER NOT NULL,
    [AssetVariableTypeTagMapID] INT              NOT NULL,
    [Min]                       FLOAT (53)       NULL,
    [Max]                       FLOAT (53)       NULL,
    [Summary]                   INT              NOT NULL,
    [Scenario] NVARCHAR(255) NULL, 
    CONSTRAINT [BI_tReportSectionGauge_PK] PRIMARY KEY CLUSTERED ([ReportSectionGaugeID] ASC),
    CONSTRAINT [FK_BI_tReportSectionChart_ProcessData_tAssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE
);


