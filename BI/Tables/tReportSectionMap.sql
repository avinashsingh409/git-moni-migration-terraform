﻿CREATE TABLE [BI].[tReportSectionMap] (
    [ReportSectionMapID] INT              IDENTITY (1, 1) NOT NULL,
    [ReportSectionID]    INT              NOT NULL,
    [WidgetID]           UNIQUEIDENTIFIER NOT NULL,
    [Title]              NVARCHAR (MAX)   NULL,
    [Subtitle]           NVARCHAR (MAX)   NULL,
    [Width]              INT              NOT NULL,
    [Kind]               NVARCHAR (255)   NOT NULL,
    [AssetID]            INT              NULL,
    [DisplayOrder]       INT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [BI_tReportSectionMap_PK] PRIMARY KEY CLUSTERED ([ReportSectionMapID] ASC),
    CONSTRAINT [FK_BI_tReportSectionMap_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_BI_tReportSectionMap_BI_tReportSection] FOREIGN KEY ([ReportSectionID]) REFERENCES [BI].[tReportSection] ([ReportSectionID]) ON DELETE CASCADE
);




