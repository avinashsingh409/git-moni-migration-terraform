﻿CREATE TABLE [BI].[tReportSection] (
    [ReportSectionID] INT            IDENTITY (1, 1) NOT NULL,
    [ReportID]        INT            NOT NULL,
    [Title]           NVARCHAR (MAX) NULL,
    [Subtitle]        NVARCHAR (MAX) NULL,
    [DisplayOrder]    INT            NOT NULL,
    [Color]           NVARCHAR (255) NULL,
	[Height]          NVARCHAR (6)  NULL,
    CONSTRAINT [BI_tReportSection_PK] PRIMARY KEY CLUSTERED ([ReportSectionID] ASC),
    CONSTRAINT [FK_BI_tReportSection_BI_tReport] FOREIGN KEY ([ReportID]) REFERENCES [BI].[tReport] ([ReportID]) ON DELETE CASCADE
);

