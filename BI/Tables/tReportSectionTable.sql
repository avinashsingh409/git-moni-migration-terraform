﻿CREATE TABLE [BI].[tReportSectionTable] (
    [ReportSectionTableID] UNIQUEIDENTIFIER NOT NULL,
    [PDTrendID]            INT              NOT NULL,
    CONSTRAINT [BI_tReportSectionTable_PK] PRIMARY KEY CLUSTERED ([ReportSectionTableID] ASC),
    CONSTRAINT [FK_BI_tReportSectionTable_ProcessData_tTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);


