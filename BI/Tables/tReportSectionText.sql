﻿CREATE TABLE [BI].[tReportSectionText] (
    [ReportSectionTextID] UNIQUEIDENTIFIER NOT NULL,
    [Content]             NVARCHAR (MAX)   NULL,
	[Editable]			  BIT NOT NULL DEFAULT ((0)),
    CONSTRAINT [BI_tReportSectionText_PK] PRIMARY KEY CLUSTERED ([ReportSectionTextID] ASC)
);


