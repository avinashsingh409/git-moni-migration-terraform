﻿
CREATE PROCEDURE [BI].[spUpsertReportSectionGauge]
	@widgetID UNIQUEIDENTIFIER,
	@reportSectionID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@width int,
	@assetID int,
	@assetVariableTypeTagMapID int,
	@min float,
	@max float,
	@summary int,
	@displayOrder int,
	@scenario NVARCHAR(255)
AS
BEGIN
	IF EXISTS(SELECT * FROM BI.tReportSectionGauge WHERE ReportSectionGaugeID = @widgetID)
	BEGIN
		UPDATE BI.tReportSectionGauge SET
			AssetVariableTypeTagMapID = @assetVariableTypeTagMapID,
			[Min] = @min,
			[Max] = @max,
			Summary = @summary,
			Scenario = @scenario
		WHERE BI.tReportSectionGauge.ReportSectionGaugeID = @widgetID 
		UPDATE BI.tReportSectionMap SET
			Title = @title,
			Subtitle = @subtitle,
			Width = @width,
			AssetID = @assetID,
			DisplayOrder = @displayOrder
		WHERE BI.tReportSectionMap.WidgetID = @widgetID 
	END
	ELSE
	BEGIN
		INSERT INTO BI.tReportSectionGauge(ReportSectionGaugeID, AssetVariableTypeTagMapID, [Min], [Max], Summary,Scenario)
		VALUES(@widgetID, @assetVariableTypeTagMapID, @min, @max,@summary,@scenario);
		INSERT INTO BI.tReportSectionMap(ReportSectionID, WidgetID,Title,Subtitle,Width,Kind,AssetID,DisplayOrder)
		VALUES(@reportSectionID,@widgetID,@title,@subtitle,@width,'Gauge',@assetID,@displayOrder);
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSectionGauge] TO [TEUser]
    AS [dbo];