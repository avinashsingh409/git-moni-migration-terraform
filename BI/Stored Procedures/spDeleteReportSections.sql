﻿--Procedure to delete all of the report sections for a report
CREATE PROCEDURE [BI].[spDeleteReportSections]
	@ReportID int
AS
BEGIN
	DELETE c FROM BI.tReportSectionFixedChart c
	INNER JOIN BI.tReportSectionMap map on map.WidgetID = c.ReportSectionFixedChartID
	INNER JOIN BI.tReportSection section ON section.ReportSectionID = map.ReportSectionID
	WHERE map.Kind = 'FixedChart' AND section.ReportID = @ReportID

	DELETE c FROM BI.tReportSectionChart c
	INNER JOIN BI.tReportSectionMap map on map.WidgetID = c.ReportSectionChartID
	INNER JOIN BI.tReportSection section ON section.ReportSectionID = map.ReportSectionID
	WHERE map.Kind = 'Chart' AND section.ReportID = @ReportID

	DELETE c FROM BI.tReportSectionTable c
	INNER JOIN BI.tReportSectionMap map on map.WidgetID = c.ReportSectionTableID
	INNER JOIN BI.tReportSection section ON section.ReportSectionID = map.ReportSectionID
	WHERE map.Kind = 'Table' AND section.ReportID = @ReportID

	DELETE c FROM BI.tReportSectionGauge c
	INNER JOIN BI.tReportSectionMap map on map.WidgetID = c.ReportSectionGaugeID
	INNER JOIN BI.tReportSection section ON section.ReportSectionID = map.ReportSectionID
	WHERE map.Kind = 'Gauge' AND section.ReportID = @ReportID

	DELETE c FROM BI.tReportSectionKPI c
	INNER JOIN BI.tReportSectionMap map on map.WidgetID = c.ReportSectionKPIID
	INNER JOIN BI.tReportSection section ON section.ReportSectionID = map.ReportSectionID
	WHERE map.Kind = 'KPI' AND section.ReportID = @ReportID

	DELETE c FROM BI.tReportSectionText c
	INNER JOIN BI.tReportSectionMap map on map.WidgetID = c.ReportSectionTextID
	INNER JOIN BI.tReportSection section ON section.ReportSectionID = map.ReportSectionID
	WHERE map.Kind = 'Text' AND section.ReportID = @ReportID

	DELETE map FROM BI.tReportSectionMap map
	INNER JOIN BI.tReportSection section ON map.ReportSectionID = section.ReportSectionID
	WHERE section.ReportID = @ReportID

	DELETE section FROM BI.tReportSection section
	WHERE section.ReportID = @ReportID 

END

GO

GRANT EXECUTE
    ON OBJECT::[BI].[spDeleteReportSections] TO [TEUser]
    AS [dbo];