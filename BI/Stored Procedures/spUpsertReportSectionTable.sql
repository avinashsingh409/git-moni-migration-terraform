﻿--Create Upsert for the Chart Widget
CREATE PROCEDURE [BI].[spUpsertReportSectionTable]
	@widgetID UNIQUEIDENTIFIER,
	@reportSectionID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@width int,
	@assetID int,
	@pdtrendID int,
	@displayOrder int
AS
BEGIN
	IF EXISTS(SELECT * FROM BI.tReportSectionTable WHERE ReportSectionTableID = @widgetID)
	BEGIN
		UPDATE BI.tReportSectionTable SET
			PDTrendID = @pdtrendID
		WHERE BI.tReportSectionTable.ReportSectionTableID = @widgetID 
		UPDATE BI.tReportSectionMap SET
			Title = @title,
			Subtitle = @subtitle,
			Width = @width,
			AssetID = @assetID,
			DisplayOrder = @displayOrder
		WHERE BI.tReportSectionMap.WidgetID = @widgetID 
	END
	ELSE
	BEGIN
		INSERT INTO BI.tReportSectionTable(ReportSectionTableID, PDTrendID)
		VALUES(@widgetID, @pdtrendID);
		INSERT INTO BI.tReportSectionMap(ReportSectionID, WidgetID,Title,Subtitle,Width,Kind,AssetID,DisplayOrder)
		VALUES(@reportSectionID,@widgetID,@title,@subtitle,@width,'Table',@assetID,@displayOrder);
	END
END

GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSectionTable] TO [TEUser]
    AS [dbo];

