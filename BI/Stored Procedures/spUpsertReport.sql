﻿CREATE PROCEDURE [BI].[spUpsertReport]
	@ReportID int = NULL,
	@ReportDesc NVARCHAR(MAX),
	@ReportAbbrev NVARCHAR(255),
	@DisplayOrder int,
	@AssetID int,
	@Title NVARCHAR(MAX),
	@Subtitle NVARCHAR(MAX),
	@SecurityUserID int,
	@ChangeDate datetime,
	@NodeId uniqueidentifier,
	@NewReportID int OUTPUT
    AS 
	BEGIN
		SET @NewReportID = -1

		DECLARE @ProceedWithSave bit = 0;

		IF @AssetID IS NOT NULL
		  BEGIN
		  SELECT @ProceedWithSave = Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@SecurityUserID,-1)
		  END		

		IF (@ProceedWithSave = 0 AND @NodeID IS NOT NULL) BEGIN
		  SET @ProceedWithSave = 1 
		END
	
		IF (@ProceedWithSave = 1) BEGIN
			IF(@ReportID IS NULL) BEGIN
				INSERT INTO BI.tReport(ReportDesc, ReportAbbrev, DisplayOrder, AssetID, Title, Subtitle, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate, NodeId)
				VALUES(@ReportDesc, @ReportAbbrev, @DisplayOrder, @AssetID, @Title, @Subtitle, @SecurityUserID, @SecurityUserID, @ChangeDate, @ChangeDate, @NodeId)
				SET @NewReportID = SCOPE_IDENTITY();
			END ELSE BEGIN 
				UPDATE BI.tReport SET
					ReportDesc = @ReportDesc,
					ReportAbbrev = @ReportAbbrev,
					DisplayOrder = @DisplayOrder,
					AssetID = @AssetID,
					Title = @Title,
					Subtitle = @Subtitle,
					ChangedByUserID = @SecurityUserID,
					ChangeDate = @ChangeDate,
					NodeId = @NodeId
				WHERE BI.tReport.ReportID = @ReportID
				SET @NewReportID = @ReportID; 
			END
		END
		SELECT @NewReportID
	END


GO

GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReport] TO [TEUser]
    AS [dbo];