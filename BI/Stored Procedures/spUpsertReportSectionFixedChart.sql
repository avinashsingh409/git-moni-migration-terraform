﻿
CREATE PROCEDURE [BI].[spUpsertReportSectionFixedChart]
	@widgetID UNIQUEIDENTIFIER,
	@reportSectionID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@width int,
	@assetID int,
	@content NVARCHAR(MAX),
	@displayOrder int
AS
BEGIN
	IF EXISTS(SELECT * FROM BI.tReportSectionFixedChart WHERE ReportSectionFixedChartID = @widgetID)
	BEGIN
		UPDATE BI.tReportSectionFixedChart SET
			Content = @content
		WHERE BI.tReportSectionFixedChart.ReportSectionFixedChartID = @widgetID 
		UPDATE BI.tReportSectionMap SET
			Title = @title,
			Subtitle = @subtitle,
			Width = @width,
			AssetID = @assetID,
			DisplayOrder = @displayOrder
		WHERE BI.tReportSectionMap.WidgetID = @widgetID 
	END
	ELSE
	BEGIN
		INSERT INTO BI.tReportSectionFixedChart(ReportSectionFixedChartID, Content)
		VALUES(@widgetID, @content);
		INSERT INTO BI.tReportSectionMap(ReportSectionID, WidgetID,Title,Subtitle,Width,Kind,AssetID, DisplayOrder)
		VALUES(@reportSectionID,@widgetID,@title,@subtitle,@width,'FixedChart',@assetID,@displayOrder);
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSectionFixedChart] TO [TEUser]
    AS [dbo];