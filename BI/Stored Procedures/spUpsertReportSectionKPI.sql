﻿
CREATE PROCEDURE [BI].[spUpsertReportSectionKPI]
	@widgetID UNIQUEIDENTIFIER,
	@reportSectionID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@width int,
	@assetID int,
	@assetVariableTypeTagMapID int,
	@background NVARCHAR(255),
	@summary int,
	@displayOrder int,
	@scenario NVARCHAR(255)
AS
BEGIN
	IF EXISTS(SELECT * FROM BI.tReportSectionKPI WHERE ReportSectionKPIID = @widgetID)
	BEGIN
		UPDATE BI.tReportSectionKPI SET
			AssetVariableTypeTagMapID = @assetVariableTypeTagMapID,
			Background = @background,
			Summary = @summary,
			Scenario = @scenario
		WHERE BI.tReportSectionKPI.ReportSectionKPIID = @widgetID 
		UPDATE BI.tReportSectionMap SET
			Title = @title,
			Subtitle = @subtitle,
			Width = @width,
			AssetID = @assetID,
			DisplayOrder = @displayOrder
		WHERE BI.tReportSectionMap.WidgetID = @widgetID 
	END
	ELSE
	BEGIN
		INSERT INTO BI.tReportSectionKPI(ReportSectionKPIID, AssetVariableTypeTagMapID, Background, Summary, Scenario)
		VALUES(@widgetID, @assetVariableTypeTagMapID, @background, @summary,@scenario);
		INSERT INTO BI.tReportSectionMap(ReportSectionID, WidgetID,Title,Subtitle,Width,Kind,AssetID,DisplayOrder)
		VALUES(@reportSectionID,@widgetID,@title,@subtitle,@width,'KPI',@assetID,@displayOrder);
	END
END

GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSectionKPI] TO [TEUser]
    AS [dbo];

