﻿CREATE PROCEDURE [BI].[spUpsertReportSection]
	@reportSectionID int=NULL,
	@reportID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@height NVARCHAR(6),
	@displayOrder int,
	@color NVARCHAR(255)
AS
BEGIN
	DECLARE @result int = -1;
	IF @reportSectionID IS NULL
	BEGIN
		INSERT INTO BI.tReportSection(ReportID, Title,Subtitle,DisplayOrder,Color,Height)
		VALUES (@reportID, @title, @subtitle,@displayOrder,@color,@height);
		SET @result = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE BI.tReportSection 
		SET ReportID = @reportID, Title = @title, Subtitle = @subtitle,DisplayOrder = @displayOrder,Color=@color,Height=@height 
		WHERE ReportSectionID = @reportSectionID;
		SET @result = @reportID;
	END
	SELECT @result; 
END
GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSection] TO [TEUser]
    AS [dbo];