﻿
--Create Upsert for the Text Widget
CREATE PROCEDURE [BI].[spUpsertReportSectionText]
	@widgetID UNIQUEIDENTIFIER,
	@reportSectionID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@width int,
	@assetID int,
	@content NVARCHAR(MAX),
	@editable BIT,
	@displayOrder int
AS
BEGIN
	IF EXISTS(SELECT * FROM BI.tReportSectionText WHERE ReportSectionTextID = @widgetID)
	BEGIN
		UPDATE BI.tReportSectionText SET
			Content = @content,
			Editable = @editable
		WHERE BI.tReportSectionText.ReportSectionTextID = @widgetID 
		UPDATE BI.tReportSectionMap SET
			Title = @title,
			Subtitle = @subtitle,
			Width = @width,
			AssetID = @assetID,
			DisplayOrder = @displayOrder
		WHERE BI.tReportSectionMap.WidgetID = @widgetID 
	END
	ELSE
	BEGIN
		INSERT INTO BI.tReportSectionText(ReportSectionTextID, Content,Editable)
		VALUES(@widgetID, @content,@editable);
		INSERT INTO BI.tReportSectionMap(ReportSectionID, WidgetID,Title,Subtitle,Width,Kind,AssetID,DisplayOrder)
		VALUES(@reportSectionID,@widgetID,@title,@subtitle,@width,'Text',@assetID,@displayOrder);
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSectionText] TO [TEUser]
    AS [dbo];