﻿CREATE PROCEDURE [BI].[spDeleteReport]
	@ReportID int,
	@SecurityUserID int
AS
BEGIN
	DECLARE @Result int = 0; 

	--If the user passes in a report that doesn't exist then AssetID will be null and
	--executing the delete statements will have no effect. 
	DECLARE @AssetID int = NULL;
	DECLARE @NodeID uniqueidentifier = NULL;


	SELECT @AssetID = AssetID, @NodeId = NodeId  FROM BI.tReport WHERE ReportID = @ReportID;
	
	DECLARE @continue bit = 0

	IF (@AssetID IS NOT NULL) BEGIN

		SELECT @continue = Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@SecurityUserID,-1)
		
	END ELSE IF (@NodeID IS NOT NULL)  BEGIN
		
		SET @continue = 1

	END

	IF (@continue = 1) 
	BEGIN
		EXEC BI.spDeleteReportSections @ReportID;

		--Delete the report sections and the report.
		--There is a cascade delete so I shouldn't need to execute 
		--separate deletes on the report sections.
		DELETE FROM BI.tReport WHERE ReportID = @ReportID; 
		SET @Result = @@ROWCOUNT;
	END
	ELSE
	BEGIN
		SET @Result = -1;
		RAISERROR ('The user does not have access to this asset.',1,1);
	END 

	SELECT @Result; 

END

GO

GRANT EXECUTE
    ON OBJECT::[BI].[spDeleteReport] TO [TEUser]
    AS [dbo];
