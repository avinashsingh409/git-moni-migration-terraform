﻿
CREATE PROCEDURE [BI].[spUpsertReportSectionChart]
	@widgetID UNIQUEIDENTIFIER,
	@reportSectionID int,
	@title NVARCHAR(MAX),
	@subtitle NVARCHAR(MAX),
	@width int,
	@assetID int,
	@pdtrendID int,
	@displayOrder int
AS
BEGIN
	IF EXISTS(SELECT * FROM BI.tReportSectionChart WHERE ReportSectionChartID = @widgetID)
	BEGIN
		UPDATE BI.tReportSectionChart SET
			PDTrendID = @pdtrendID
		WHERE BI.tReportSectionChart.ReportSectionChartID = @widgetID 
		UPDATE BI.tReportSectionMap SET
			Title = @title,
			Subtitle = @subtitle,
			Width = @width,
			AssetID = @assetID,
			DisplayOrder = @displayOrder
		WHERE BI.tReportSectionMap.WidgetID = @widgetID 
	END
	ELSE
	BEGIN
		INSERT INTO BI.tReportSectionChart(ReportSectionChartID, PDTrendID)
		VALUES(@widgetID, @pdtrendID);
		INSERT INTO BI.tReportSectionMap(ReportSectionID, WidgetID,Title,Subtitle,Width,Kind,AssetID,DisplayOrder)
		VALUES(@reportSectionID,@widgetID,@title,@subtitle,@width,'Chart',@assetID,@displayOrder);
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[BI].[spUpsertReportSectionChart] TO [TEUser]
    AS [dbo];