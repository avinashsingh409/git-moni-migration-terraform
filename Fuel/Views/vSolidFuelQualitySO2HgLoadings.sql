﻿
CREATE VIEW [Fuel].[vSolidFuelQualitySO2HgLoadings]
AS
	select fq.FuelQualityID,
	CONVERT(float(24), (CASE WHEN fq.HHV_btulbm > 0 AND fq.Sulfur_percent > 0 THEN (					1000000.0 / fq.HHV_btulbm	* fq.Sulfur_percent		/ 100.0 * 1.9978) ELSE 0  END )) SO2Loading_lbmmbtu,
	CONVERT(float(24), (CASE WHEN fq.HHV_btulbm > 0 AND fq.Mercury_ppm > 0 AND fq.Moisture_percent > 0 THEN	(					1000000000000.0 / fq.HHV_btulbm		* fq.Mercury_ppm	* ((100.0 - fq.Moisture_percent)	/ 100.0) / 1000000.0) ELSE (0) END)) HgLoading_lbmtbtu
	FROM Fuel.vSolidFuelQuality fq
	;
GO