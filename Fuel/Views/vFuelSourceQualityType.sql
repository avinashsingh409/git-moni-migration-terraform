﻿
CREATE VIEW [Fuel].[vFuelSourceQualityType]
AS
	SELECT fs.FuelSourceID,
	(CASE fqCounter.QualityCount WHEN 1 THEN 'SingleQuality' ELSE 'VariableQuality' END) as FuelModelType
	FROM Fuel.tFuelSource fs
	INNER JOIN (
		SELECT FuelSourceID, Count(*) as QualityCount from Fuel.vSolidFuelQuality GROUP BY FuelSourceID
	) fqCounter on fs.FuelSourceID = fqCounter.FuelSourceID
	;
GO
