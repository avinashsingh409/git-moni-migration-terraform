﻿
CREATE VIEW [Fuel].[vFirstQualityEffectiveDateBySource] AS
SELECT	 FuelSourceID, MIN(EffectiveDate) AS FirstQualityEffectiveDate
FROM	 Fuel.tFuelQuality
GROUP BY FuelSourceID

