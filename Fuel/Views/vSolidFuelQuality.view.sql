﻿CREATE VIEW Fuel.vSolidFuelQuality
AS
SELECT a.[FuelQualityID]
      ,[FuelQualityTypeID]
      ,[FuelQualityDesc]
      ,[FuelSourceID]
      ,a.[EffectiveDate]
      ,[Mine]
      ,[ShipPoint]
      ,[County]
      ,[State]
      ,[Seam]
      ,[TrainNumber]
      ,[ShipDate]
      ,[SampleIdentification]
      ,[SampleLocation]
      ,[SampleDate]
      ,[TypicalMinimumMaximum]
      ,[ProximateBasis]
      ,[HHV_btulbm]
      ,[Moisture_percent]
      ,[InherentMoisture_percent]
      ,[Ash_percent]
      ,[Sulfur_percent]
      ,[VolatileMatter_percent]
      ,[FixedCarbon_percent]
      ,[UltimateBasis]
      ,[Carbon_percent]
      ,[Hydrogen_percent]
      ,[Nitrogen_percent]
      ,[Chlorine_percent]
      ,[Oxygen_percent]
      ,[SiO2_percent]
      ,[Al2O3_percent]
      ,[TiO2_percent]
      ,[Fe2O3_percent]
      ,[CaO_percent]
      ,[MgO_percent]
      ,[K2O_percent]
      ,[Na2O_percent]
      ,[SO3_percent]
      ,[P2O5_percent]
      ,[SrO_percent]
      ,[BaO_percent]
      ,[Mn3O4_percent]
      ,[AshUndetermined_percent]
      ,[AshFusionReducingInitialDeformation_f]
      ,[AshFusionReducingSoftening_f]
      ,[AshFusionReducingHemispherical_f]
      ,[AshFusionReducingFluid_f]
      ,[AshFusionOxidizingInitialDeformation_f]
      ,[AshFusionOxidizingSoftening_f]
      ,[AshFusionOxidizingHemispherical_f]
      ,[AshFusionOxidizingFluid_f]
      ,[T250_f]
      ,[HGI]
      ,[BaseToAcidRatio_fraction]
      ,[EquilibriumMoisture_percent]
      ,[SulfurFormPyritic_percent]
      ,[SulfurFormSulfate_percent]
      ,[SulfurFormOrganic_percent]
      ,[Mercury_ppm]
      ,[ThermoMAFHHV_btulbm]
      ,[ThermoOtherAsh_percent]
      ,[ThermoBoundMoisture_percent]
      ,[CreatedBy]
      ,[ChangedBy]
      ,[CreateDate]
      ,[ChangeDate]
      ,[IsValid]
  FROM Fuel.tFuelQuality a INNER JOIN Fuel.tSolidFuelQuality b ON a.FuelQualityID = b.FuelQualityID
