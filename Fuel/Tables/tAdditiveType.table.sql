CREATE TABLE [Fuel].[tAdditiveType] (
    [AdditiveTypeID]     INT            NOT NULL,
    [AdditiveTypeDesc]   NVARCHAR (255) NOT NULL,
    [AdditiveTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [DefaultForm]        TINYINT        NOT NULL,
    CONSTRAINT [PK_Fuel_tAdditiveType] PRIMARY KEY CLUSTERED ([AdditiveTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tAdditiveType';

