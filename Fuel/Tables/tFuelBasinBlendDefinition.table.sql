CREATE TABLE [Fuel].[tFuelBasinBlendDefinition] (
    [FuelBasinID]    INT NOT NULL,
    [ParentBasin1ID] INT NOT NULL,
    [ParentBasin2ID] INT NOT NULL,
    CONSTRAINT [PK_tFuelBasinBlendDefinition] PRIMARY KEY CLUSTERED ([FuelBasinID] ASC, [ParentBasin1ID] ASC, [ParentBasin2ID] ASC),
    CONSTRAINT [FK_tFuelBasinBlendDefinition_FuelBasinID_tFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID]),
    CONSTRAINT [FK_tFuelBasinBlendDefinition_ParentBasin1ID_tFuelBasin] FOREIGN KEY ([ParentBasin1ID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID]),
    CONSTRAINT [FK_tFuelBasinBlendDefinition_ParentBasin2ID_tFuelBasin] FOREIGN KEY ([ParentBasin2ID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelBasinBlendDefinition';

