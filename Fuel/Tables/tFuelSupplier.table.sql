CREATE TABLE [Fuel].[tFuelSupplier] (
    [FuelSupplierID]   INT            IDENTITY (1, 1) NOT NULL,
    [FuelSupplierDesc] NVARCHAR (255) NOT NULL,
    [CreatedBy]        NVARCHAR (255) NOT NULL,
    [ChangedBy]        NVARCHAR (255) NOT NULL,
    [CreateDate]       DATETIME       CONSTRAINT [DF__tFuelSupp__Creat__297722B6] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]       DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tFuelSupplier] PRIMARY KEY CLUSTERED ([FuelSupplierID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supplier of fuel sources.', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSupplier';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSupplier';

