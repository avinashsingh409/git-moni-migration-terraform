CREATE TABLE [Fuel].[tSolidFuelQualityStatus] (
    [SolidFuelQualityStatusID] INT     IDENTITY (1, 1) NOT NULL,
    [FuelQualityID]            INT     NOT NULL,
    [HHVStatus]                TINYINT NULL,
    [MoistureStatus]           TINYINT NULL,
    [AshStatus]                TINYINT NULL,
    [SulfurStatus]             TINYINT NULL,
    [SiO2Status]               TINYINT NULL,
    [Al2O3Status]              TINYINT NULL,
    [TiO2Status]               TINYINT NULL,
    [Fe2O3Status]              TINYINT NULL,
    [CaOStatus]                TINYINT NULL,
    [MgOStatus]                TINYINT NULL,
    [K2OStatus]                TINYINT NULL,
    [Na2OStatus]               TINYINT NULL,
    CONSTRAINT [PK_tSolidFuelQualityStatus] PRIMARY KEY CLUSTERED ([FuelQualityID] ASC),
    CONSTRAINT [FK_tSolidFuelQualityStatus_FuelQualityID_tSolidFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQualityStatus';

