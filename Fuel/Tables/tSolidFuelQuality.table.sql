CREATE TABLE [Fuel].[tSolidFuelQuality] (
    [Mine]                                   NVARCHAR (255) NULL,
    [ShipPoint]                              NVARCHAR (255) NULL,
    [County]                                 NVARCHAR (255) NULL,
    [State]                                  NVARCHAR (255) NULL,
    [Seam]                                   NVARCHAR (255) NULL,
    [TrainNumber]                            NVARCHAR (255) NULL,
    [ShipDate]                               DATETIME       NULL,
    [SampleIdentification]                   NVARCHAR (255) NULL,
    [SampleLocation]                         NVARCHAR (255) NULL,
    [SampleDate]                             DATETIME       NULL,
    [TypicalMinimumMaximum]                  TINYINT        CONSTRAINT [DF_tFuelQualityTypical] DEFAULT ((1)) NOT NULL,
    [ProximateBasis]                         TINYINT        CONSTRAINT [DF_tFuelQualityIsProximate] DEFAULT ((1)) NOT NULL,
    [HHV_btulbm]                             REAL           NULL,
    [Moisture_percent]                       REAL           NULL,
    [InherentMoisture_percent]               REAL           NULL,
    [Ash_percent]                            REAL           NULL,
    [Sulfur_percent]                         REAL           NULL,
    [VolatileMatter_percent]                 REAL           NULL,
    [FixedCarbon_percent]                    REAL           NULL,
    [UltimateBasis]                          TINYINT        CONSTRAINT [DF_tFuelQualityIsUltimate] DEFAULT ((1)) NOT NULL,
    [Carbon_percent]                         REAL           NULL,
    [Hydrogen_percent]                       REAL           NULL,
    [Nitrogen_percent]                       REAL           NULL,
    [Chlorine_percent]                       REAL           NULL,
    [Oxygen_percent]                         REAL           NULL,
    [SiO2_percent]                           REAL           NULL,
    [Al2O3_percent]                          REAL           NULL,
    [TiO2_percent]                           REAL           NULL,
    [Fe2O3_percent]                          REAL           NULL,
    [CaO_percent]                            REAL           NULL,
    [MgO_percent]                            REAL           NULL,
    [K2O_percent]                            REAL           NULL,
    [Na2O_percent]                           REAL           NULL,
    [SO3_percent]                            REAL           NULL,
    [P2O5_percent]                           REAL           NULL,
    [SrO_percent]                            REAL           NULL,
    [BaO_percent]                            REAL           NULL,
    [Mn3O4_percent]                          REAL           NULL,
    [AshUndetermined_percent]                REAL           NULL,
    [AshFusionReducingInitialDeformation_f]  REAL           NULL,
    [AshFusionReducingSoftening_f]           REAL           NULL,
    [AshFusionReducingHemispherical_f]       REAL           NULL,
    [AshFusionReducingFluid_f]               REAL           NULL,
    [AshFusionOxidizingInitialDeformation_f] REAL           NULL,
    [AshFusionOxidizingSoftening_f]          REAL           NULL,
    [AshFusionOxidizingHemispherical_f]      REAL           NULL,
    [AshFusionOxidizingFluid_f]              REAL           NULL,
    [T250_f]                                 REAL           NULL,
    [HGI]                                    REAL           NULL,
    [BaseToAcidRatio_fraction]               REAL           NULL,
    [EquilibriumMoisture_percent]            REAL           NULL,
    [SulfurFormPyritic_percent]              REAL           NULL,
    [SulfurFormSulfate_percent]              REAL           NULL,
    [SulfurFormOrganic_percent]              REAL           NULL,
    [Mercury_ppm]                            REAL           NULL,
    [ThermoMAFHHV_btulbm]                    REAL           NULL,
    [ThermoOtherAsh_percent]                 REAL           NULL,
    [ThermoBoundMoisture_percent]            REAL           NULL,
    [CreatedBy]                              NVARCHAR (255) NOT NULL,
    [ChangedBy]                              NVARCHAR (255) NOT NULL,
    [CreateDate]                             DATETIME       CONSTRAINT [DF_tFuelQualityCreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                             DATETIME       CONSTRAINT [DF__tFuelQual__Chang__29B719D8] DEFAULT (getdate()) NOT NULL,
    [IsValid]                                BIT            DEFAULT ((0)) NOT NULL,
    [FuelQualityID]                          INT            NOT NULL,
    CONSTRAINT [PK_Fuel_tSolidFuelQuality] PRIMARY KEY CLUSTERED ([FuelQualityID] ASC),
    CONSTRAINT [FK_Fuel_tSolidFuelQuality_FuelQualityID_Fuel_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuel quality data (Proximate, Ultimate, Ash, Fusion Temps, HGI etc...)', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQuality';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuel quality data represents typical values if 1, minimum values if 2 and maximum if 3', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQuality', @level2type = N'COLUMN', @level2name = N'TypicalMinimumMaximum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Proximate analysis data is on an as received basis if 1, dry basis if 2 and air dried basis if 3', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQuality', @level2type = N'COLUMN', @level2name = N'ProximateBasis';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ultimate analysis data is on an as received basis if 1, dry basis if 2 and air dried basis if 3', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQuality', @level2type = N'COLUMN', @level2name = N'UltimateBasis';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQuality';

