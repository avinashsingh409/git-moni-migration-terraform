CREATE TABLE [Fuel].[tFuelBlendDefinition] (
    [FuelBlendDefinitionID] INT  IDENTITY (1, 1) NOT NULL,
    [FuelSourceID]          INT  NOT NULL,
    [ParentSourceID]        INT  NOT NULL,
    [ParentMass_fraction]   REAL NOT NULL,
    PRIMARY KEY CLUSTERED ([FuelBlendDefinitionID] ASC),
    CONSTRAINT [FK_tFuelBlendDefinition_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tFuelBlendDefinition_ParentSourceID_tFuelSource] FOREIGN KEY ([ParentSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Blend definition table for the fuel source.', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelBlendDefinition';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelBlendDefinition';

