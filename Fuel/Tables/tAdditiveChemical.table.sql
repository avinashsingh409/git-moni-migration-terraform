CREATE TABLE [Fuel].[tAdditiveChemical] (
    [AdditiveChemicalID]     INT  IDENTITY (1, 1) NOT NULL,
    [FuelQualityID]          INT  NOT NULL,
    [AdditiveChemicalTypeID] INT  NOT NULL,
    [ChemicalMass_percent]   REAL NOT NULL,
    CONSTRAINT [PK_Fuel_tAdditiveChemical] PRIMARY KEY CLUSTERED ([AdditiveChemicalID] ASC),
    CONSTRAINT [FK_Fuel_tAdditiveChemical_AdditiveChemicalTypeID_Fuel_tAdditiveChemicalType] FOREIGN KEY ([AdditiveChemicalTypeID]) REFERENCES [Fuel].[tAdditiveChemicalType] ([AdditiveChemicalTypeID]),
    CONSTRAINT [FK_Fuel_tAdditiveChemical_FuelQualityID_Fuel_tAdditive] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tAdditive] ([FuelQualityID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tAdditiveChemical';

