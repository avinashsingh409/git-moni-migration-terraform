﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 10/01/2008
-- Description:	Validate the selection of a default fuel quality.
-- =============================================
CREATE TRIGGER [Fuel].[ValidateDefaultFuelQuality]
   ON  [Fuel].[tFuelQuality]
   FOR INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	DECLARE @FuelQualityValid as bit;
	SET @FuelQualityValid = 1;	
	
    IF UPDATE(EffectiveDate) OR UPDATE(FuelSourceID)
	BEGIN		
		--If it is an additive (not solid fuel quality) then default to valid
		SELECT @FuelQualityValid=ISNULL(CONVERT(bit,MIN(CONVERT(tinyint,IsValid))),1) 
		FROM Fuel.tSolidFuelQuality a INNER JOIN INSERTED b ON a.FuelQualityID = b.FuelQualityID 		
		INNER JOIN tFuelSource c ON b.FuelSourceID = c.FuelSourceID
		WHERE b.EffectiveDate is not null and c.IsAdditive = 0;

		IF @FuelQualityValid = 0
		BEGIN
			ROLLBACK TRANSACTION;
			RAISERROR(N'The mapped fuel quality cannot be set as default for the fuel source because it is not valid for the calculations.  Verify the fuel quality is complete and correct.',
				  16, -- Severity.
				  1); -- State.				  
		END	

	    UPDATE tCQIResult SET IsModified = 1 FROM INSERTED WHERE tCQIResult.FuelSourceID = INSERTED.FuelSourceID AND INSERTED.EffectiveDate is not null;

		UPDATE tScenario SET IsModified = 1 
		FROM INSERTED b INNER JOIN tFuelPlanDetail c ON b.FuelSourceID = c.FuelSourceID
		WHERE b.EffectiveDate is not null AND tScenario.FuelPlanSetID = c.FuelPlanSetID
	END
END
