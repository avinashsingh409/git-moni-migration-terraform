﻿/****** Object:  Trigger [Fuel].[ValidateFuelQualityChangeForProjection]    Script Date: 06/12/2013 11:38:44 ******/
CREATE TRIGGER [Fuel].[ValidateFuelQualityChangeForProjection]
   ON  [Fuel].[tSolidFuelQuality]
   AFTER INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	DECLARE @CurrentFuelQualityID as int;
	SET @CurrentFuelQualityID = -1;
	
    IF UPDATE(Moisture_percent) OR UPDATE(Ash_percent) OR UPDATE(VolatileMatter_percent) 
		OR UPDATE(FixedCarbon_percent) OR UPDATE(InherentMoisture_percent)
		OR UPDATE(Carbon_percent) OR UPDATE(Hydrogen_percent) OR UPDATE(Nitrogen_percent) 
		OR UPDATE(Sulfur_percent) OR UPDATE(Chlorine_percent) OR UPDATE(Moisture_percent) 
		OR UPDATE(Oxygen_percent) OR UPDATE(SiO2_percent) OR UPDATE(Al2O3_percent) 
		OR UPDATE(TiO2_percent) OR UPDATE(Fe2O3_percent) OR UPDATE(CaO_percent) 
		OR UPDATE(MgO_percent) OR UPDATE(K2O_percent) OR UPDATE(Na2O_percent) 
		OR UPDATE(SO3_percent) OR UPDATE(P2O5_percent) OR UPDATE(SrO_percent) 
		OR UPDATE(BaO_percent) OR UPDATE(Mn3O4_percent) OR UPDATE(AshUndetermined_percent)
		OR UPDATE(HHV_btulbm) OR UPDATE(Mercury_ppm) 
	BEGIN		
		UPDATE Projection.tCQIResult SET IsModified = 1 FROM INSERTED WHERE Projection.tCQIResult.FuelQualityID = INSERTED.FuelQualityID;

		UPDATE Projection.tScenario SET IsModified = 1 
		FROM INSERTED a INNER JOIN Fuel.tFuelQuality b ON a.FuelQualityID = b.FuelQualityID
		INNER JOIN Projection.tFuelPlanDetail c ON b.FuelSourceID = c.FuelSourceID
		WHERE b.EffectiveDate is not null AND Projection.tScenario.FuelPlanSetID = c.FuelPlanSetID
	END
END
