﻿/****** Object:  Trigger [Fuel].[ValidateFuelQualityChange]    Script Date: 06/12/2013 11:38:34 ******/
CREATE TRIGGER [Fuel].[ValidateFuelQualityChange]
   ON  [Fuel].[tSolidFuelQuality]
   AFTER INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	DECLARE @CurrentFuelQualityID as int;
	SET @CurrentFuelQualityID = -1;
	
    IF UPDATE(Moisture_percent) OR UPDATE(Ash_percent) OR UPDATE(VolatileMatter_percent) 
		OR UPDATE(FixedCarbon_percent) OR UPDATE(InherentMoisture_percent)
		OR UPDATE(Carbon_percent) OR UPDATE(Hydrogen_percent) OR UPDATE(Nitrogen_percent) 
		OR UPDATE(Sulfur_percent) OR UPDATE(Chlorine_percent) OR UPDATE(Moisture_percent) 
		OR UPDATE(Oxygen_percent) OR UPDATE(SiO2_percent) OR UPDATE(Al2O3_percent) 
		OR UPDATE(TiO2_percent) OR UPDATE(Fe2O3_percent) OR UPDATE(CaO_percent) 
		OR UPDATE(MgO_percent) OR UPDATE(K2O_percent) OR UPDATE(Na2O_percent) 
		OR UPDATE(SO3_percent) OR UPDATE(P2O5_percent) OR UPDATE(SrO_percent) 
		OR UPDATE(BaO_percent) OR UPDATE(Mn3O4_percent) OR UPDATE(AshUndetermined_percent)
		OR UPDATE(HHV_btulbm) OR UPDATE(Mercury_ppm) 
	BEGIN		
		WHILE EXISTS(SELECT TOP 1 FuelQualityID FROM INSERTED WHERE FuelQualityID > @CurrentFuelQualityID ORDER BY FuelQualityID ASC)			
		BEGIN
			SELECT TOP 1 @CurrentFuelQualityID=FuelQualityID FROM INSERTED WHERE FuelQualityID > @CurrentFuelQualityID ORDER BY FuelQualityID ASC;		
			EXEC Fuel.spValidateFuelQuality @CurrentFuelQualityID;			
		END		
	END
END

