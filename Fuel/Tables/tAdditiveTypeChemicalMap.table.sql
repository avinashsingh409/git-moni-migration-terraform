CREATE TABLE [Fuel].[tAdditiveTypeChemicalMap] (
    [AdditiveTypeID]         INT  NOT NULL,
    [AdditiveChemicalTypeID] INT  NOT NULL,
    [ChemicalMass_percent]   REAL NOT NULL,
    CONSTRAINT [PK_Fuel_tAdditiveTypeChemicalMap] PRIMARY KEY CLUSTERED ([AdditiveTypeID] ASC, [AdditiveChemicalTypeID] ASC),
    CONSTRAINT [FK_Fuel_tAdditiveTypeChemicalMap_AdditiveChemicalTypeID_Fuel_tAdditiveChemicalType] FOREIGN KEY ([AdditiveChemicalTypeID]) REFERENCES [Fuel].[tAdditiveChemicalType] ([AdditiveChemicalTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Fuel_tAdditiveTypeChemicalMap_AdditiveTypeID_Fuel_tAdditiveType] FOREIGN KEY ([AdditiveTypeID]) REFERENCES [Fuel].[tAdditiveType] ([AdditiveTypeID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tAdditiveTypeChemicalMap';

