CREATE TABLE [Fuel].[tSolidFuelQualityThermoConstant] (
    [FuelQualityThermoConstantID]  INT     IDENTITY (1, 1) NOT NULL,
    [FuelQualityID]                INT     NOT NULL,
    [ThermoSourceIdentifier]       INT     NOT NULL,
    [ThermoSourceNumber]           TINYINT NOT NULL,
    [ThermoSourceQuantity_percent] REAL    NOT NULL,
    [ThermoMAFHHV_btulbm]          REAL    NOT NULL,
    [ThermoOtherAsh_percent]       REAL    NOT NULL,
    [ThermoBoundMoisture_percent]  REAL    NOT NULL,
    CONSTRAINT [PK_FuelQualityThermoConstant] PRIMARY KEY CLUSTERED ([FuelQualityThermoConstantID] ASC),
    CONSTRAINT [FK_tFuelQualityThermoConstant_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tSolidFuelQualityThermoConstant';

