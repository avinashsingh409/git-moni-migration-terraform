CREATE TABLE [Fuel].[tFuelSourceGroupMap] (
    [FuelSourceGroupMapID] INT IDENTITY (1, 1) NOT NULL,
    [FuelSourceGroupID]    INT NOT NULL,
    [FuelSourceID]         INT NOT NULL,
    PRIMARY KEY CLUSTERED ([FuelSourceGroupMapID] ASC),
    CONSTRAINT [FK_tFuelSourceGroupMap_FuelSourceGroupID_tFuelSourceGroup] FOREIGN KEY ([FuelSourceGroupID]) REFERENCES [Fuel].[tFuelSourceGroup] ([FuelSourceGroupID]),
    CONSTRAINT [FK_tFuelSourceGroupMap_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Holds mapping for fuel source records to a group.', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSourceGroupMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSourceGroupMap';

