CREATE TABLE [Fuel].[tFuelBasin] (
    [FuelBasinID]    INT            IDENTITY (1, 1) NOT NULL,
    [FuelBasinDesc]  NVARCHAR (255) NOT NULL,
    [CreatedBy]      NVARCHAR (255) NOT NULL,
    [ChangedBy]      NVARCHAR (255) NOT NULL,
    [CreateDate]     DATETIME       CONSTRAINT [DF__tBasin__CreateDa__24B26D99] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]     DATETIME       DEFAULT (getdate()) NOT NULL,
    [BasinColorARGB] INT            CONSTRAINT [DF_tFuelBasin_BasinColorARGB] DEFAULT ((-16777216)) NOT NULL,
    [OwningAssetID]  INT            NULL,
    CONSTRAINT [PK_tFuelBasin] PRIMARY KEY CLUSTERED ([FuelBasinID] ASC),
    CONSTRAINT [FK_Fuel_tFuelBasin_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [UK_tFuelBasin_BasinColorARGB] UNIQUE NONCLUSTERED ([BasinColorARGB] ASC),
    CONSTRAINT [UK_tFuelBasin_FuelBasinDesc] UNIQUE NONCLUSTERED ([FuelBasinDesc] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Basin or regional data for a fuel.  Provides default data specific to a basin if required.', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelBasin';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelBasin';

