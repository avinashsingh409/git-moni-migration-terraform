CREATE TABLE [Fuel].[tFuelQualityType] (
    [FuelQualityTypeID]     INT           NOT NULL,
    [FuelQualityTypeDesc]   VARCHAR (255) NOT NULL,
    [FuelQualityTypeAbbrev] VARCHAR (50)  NOT NULL,
    [ShowInFuelTracker]     BIT           CONSTRAINT [DF_tFuelQualityType_ShowInFuelTracker] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFuelQualityType] PRIMARY KEY CLUSTERED ([FuelQualityTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelQualityType';

