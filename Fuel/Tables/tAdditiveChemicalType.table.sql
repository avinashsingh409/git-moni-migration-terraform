CREATE TABLE [Fuel].[tAdditiveChemicalType] (
    [AdditiveChemicalTypeID]     INT            NOT NULL,
    [AdditiveChemicalTypeDesc]   NVARCHAR (255) NOT NULL,
    [AdditiveChemicalTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Fuel_tAdditiveChemicalType] PRIMARY KEY CLUSTERED ([AdditiveChemicalTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tAdditiveChemicalType';

