﻿CREATE TABLE [Fuel].[tFuelSourceGroup] (
    [FuelSourceGroupID]     INT            IDENTITY (1, 1) NOT NULL,
    [FuelSourceGroupDesc]   NVARCHAR (255) NOT NULL,
    [FuelSourceGroupAbbrev] NVARCHAR (50)  NOT NULL,
    [CreatedBy]             NVARCHAR (255) NOT NULL,
    [ChangedBy]             NVARCHAR (255) NOT NULL,
    [CreateDate]            DATETIME       CONSTRAINT [DF_FuelSourceGroup_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]            DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tFuelSourceGroup] PRIMARY KEY CLUSTERED ([FuelSourceGroupID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of fuel sources.', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSourceGroup';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSourceGroup';

