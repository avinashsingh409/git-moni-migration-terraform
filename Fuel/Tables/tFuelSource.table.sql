﻿CREATE TABLE [Fuel].[tFuelSource] (
    [FuelSourceID]      INT            IDENTITY (1, 1) NOT NULL,
    [FuelBasinID]       INT            NULL,
    [FuelSupplierID]    INT            NULL,
    [FuelSourceDesc]    NVARCHAR (255) NOT NULL,
    [CreatedBy]         NVARCHAR (255) NOT NULL,
    [ChangedBy]         NVARCHAR (255) NOT NULL,
    [CreateDate]        DATETIME       CONSTRAINT [DF__tFuelSour__Creat__2D47B39A] DEFAULT (getdate()) NOT NULL,
    [IsAdditive]        BIT            CONSTRAINT [DF_tFuelSource_IsAdditive] DEFAULT ((0)) NOT NULL,
    [ChangeDate]        DATETIME       CONSTRAINT [DF__tFuelSour__Chang__2215F810] DEFAULT (getdate()) NOT NULL,
    [FuelSourceAbbrev]  NVARCHAR (255) NULL,
    [IsGas]             BIT            CONSTRAINT [DF_Fuel_tFuelSource_IsGas] DEFAULT ((0)) NOT NULL,
    [ERPQuoteUniqueKey] NVARCHAR (255) NULL,
    [WasAutoCreated]    BIT            CONSTRAINT [DF_tFuelSource_WasAutoCreated] DEFAULT ((0)) NOT NULL,
    [OwningAssetID]     INT            NULL,
    [MarkedForDeletion] BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFuelSource] PRIMARY KEY CLUSTERED ([FuelSourceID] ASC),
    CONSTRAINT [FK_Fuel_tFuelSource_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tFuelSource_FuelBasinID_tFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID]),
    CONSTRAINT [FK_tFuelSource_FuelSupplierID_tFuelSupplier] FOREIGN KEY ([FuelSupplierID]) REFERENCES [Fuel].[tFuelSupplier] ([FuelSupplierID])
);










GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of as delivered or as burned fuels.  Single type of fuel or a blend.', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSource';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelSource';

