CREATE TABLE [Fuel].[tFuelQuality] (
    [FuelQualityID]     INT             IDENTITY (1, 1) NOT NULL,
    [FuelSourceID]      INT             NULL,
    [FuelQualityTypeID] INT             NOT NULL,
    [FuelQualityDesc]   NVARCHAR (1000) NOT NULL,
    [OwningAssetID]     INT             NULL,
    [EffectiveDate]     DATETIME2 (7)   NULL,
    CONSTRAINT [PK_Fuel_tFuelQuality] PRIMARY KEY CLUSTERED ([FuelQualityID] ASC),
    CONSTRAINT [FK_Fuel_tFuelQuality_FuelQualityTypeID_Fuel_tFuelQualityType] FOREIGN KEY ([FuelQualityTypeID]) REFERENCES [Fuel].[tFuelQualityType] ([FuelQualityTypeID]),
    CONSTRAINT [FK_Fuel_tFuelQuality_FuelSourceID_Fuel_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_Fuel_tFuelQuality_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
);






GO

GO
CREATE UNIQUE NONCLUSTERED INDEX uniqueIndex_tFuelQuality_FuelSourceID_EffectiveDate
ON [Fuel].[tFuelQuality](FuelSourceID, EffectiveDate)
WHERE EffectiveDate IS NOT NULL;
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tFuelQuality';

