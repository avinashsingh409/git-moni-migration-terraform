CREATE TABLE [Fuel].[tAdditive] (
    [FuelQualityID]     INT  NOT NULL,
    [AdditiveTypeID]    INT  NOT NULL,
    [Form]              INT  NOT NULL,
    [Location]          INT  NOT NULL,
    [MassFlow_tonhr]    REAL NOT NULL,
    [Density_lbmgal]    REAL NOT NULL,
    [VolumeFlow_galmin] REAL NOT NULL,
    [MassFlow_percent]  REAL NOT NULL,
    CONSTRAINT [PK_Fuel_tAdditive] PRIMARY KEY CLUSTERED ([FuelQualityID] ASC),
    CONSTRAINT [FK_Fuel_tAdditive_AdditiveTypeID_Fuel_tAdditiveType] FOREIGN KEY ([AdditiveTypeID]) REFERENCES [Fuel].[tAdditiveType] ([AdditiveTypeID]),
    CONSTRAINT [FK_Fuel_tAdditive_FuelQualityID_Fuel_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Fuel', @level1type = N'TABLE', @level1name = N'tAdditive';

