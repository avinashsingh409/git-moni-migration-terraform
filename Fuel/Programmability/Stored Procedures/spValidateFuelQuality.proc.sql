﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 10/01/2008
-- Description:	Validate a fuel quality record.
-- =============================================
CREATE PROCEDURE [Fuel].[spValidateFuelQuality]
	@FuelQualityID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MiscValid bit;
	DECLARE @FixedCarbon float;
	DECLARE @ProximateValid bit;
	DECLARE @ProximateBasis tinyint;
	DECLARE @Oxygen float;
	DECLARE @UltimateValid bit;
	DECLARE @UltimateBasis tinyint;
	DECLARE @UltimateBasisAsh float;
	DECLARE @UltimateAshConversion float;
	DECLARE @UndeterminedAsh float;
	DECLARE @AshValid bit;	
	
	SELECT @ProximateBasis = ProximateBasis, @UltimateBasis = UltimateBasis FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;

	--Determine the fixed carbon so we can update the fuel quality record later
	--if it is valid.  Also check that all values except fixed carbon were entered.
	--The values required allow conversion to as received which is used by the calculations.
	IF @ProximateBasis = 1 --As Received
	BEGIN
		SELECT @FixedCarbon = 100.0-ISNULL(Moisture_percent+Ash_percent+VolatileMatter_percent,0), 
		@ProximateValid = (CASE WHEN ISNULL(Moisture_percent,0) > 0 AND 
									 ISNULL(Ash_percent,0) > 0 AND 
									 ISNULL(VolatileMatter_percent,0) > 0 THEN 1 ELSE 0 END)
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;
	END
	ELSE IF @ProximateBasis = 2 --Dry
	BEGIN
		SELECT @FixedCarbon = 100.0-ISNULL(Ash_percent+VolatileMatter_percent,0), 
		@ProximateValid = (CASE WHEN ISNULL(Moisture_percent,0) > 0 AND 
									 ISNULL(Ash_percent,0) > 0 AND 
									 ISNULL(VolatileMatter_percent,0) > 0 THEN 1 ELSE 0 END)
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;
	END
	ELSE --Air Dried
	BEGIN
		SELECT @FixedCarbon = 100.0-ISNULL(InherentMoisture_percent+Ash_percent+VolatileMatter_percent,0), 
		@ProximateValid = (CASE WHEN ISNULL(Moisture_percent,0) > 0 AND 
									 ISNULL(InherentMoisture_percent,0) > 0 AND
									 ISNULL(Ash_percent,0) > 0 AND 
									 ISNULL(VolatileMatter_percent,0) > 0 THEN 1 ELSE 0 END)
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;
	END
 
	--Determine the oxygen so we can update the fuel quality record later
	--if it is valid.  Also check that all values except oxygen were entered.
	--The values required allow conversion to as received which is used by the calculations.
	IF @UltimateBasis = 1 --As Received
	BEGIN
		SELECT @Oxygen = 100.0-ISNULL(Carbon_percent+Hydrogen_percent+Nitrogen_percent+Sulfur_percent+ISNULL(Chlorine_percent,0)+Moisture_percent,0), 
		@UltimateValid = (CASE WHEN ISNULL(Carbon_percent,0) > 0 AND 
									ISNULL(Hydrogen_percent,0) > 0 AND 
									ISNULL(Nitrogen_percent,0) > 0 AND 
									ISNULL(Sulfur_percent,0) > 0 AND 
								    ISNULL(Moisture_percent,0) > 0 THEN 1 ELSE 0 END)
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;
	END
	ELSE IF @UltimateBasis = 2 --Dry
	BEGIN
		SELECT @Oxygen = 100.0-ISNULL(Carbon_percent+Hydrogen_percent+Nitrogen_percent+Sulfur_percent+ISNULL(Chlorine_percent,0),0),
		@UltimateValid = (CASE WHEN ISNULL(Carbon_percent,0) > 0 AND 
									ISNULL(Hydrogen_percent,0) > 0 AND 
									ISNULL(Nitrogen_percent,0) > 0 AND 
									ISNULL(Sulfur_percent,0) > 0 AND 
								    ISNULL(Moisture_percent,0) > 0 THEN 1 ELSE 0 END)
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;
	END
	ELSE --Air Dried
	BEGIN
		SELECT @Oxygen = 100.0-ISNULL(Carbon_percent+Hydrogen_percent+Nitrogen_percent+Sulfur_percent+ISNULL(Chlorine_percent,0)+InherentMoisture_percent,0), 
		@UltimateValid = (CASE WHEN ISNULL(Carbon_percent,0) > 0 AND 
									ISNULL(Hydrogen_percent,0) > 0 AND 
									ISNULL(Nitrogen_percent,0) > 0 AND 
									ISNULL(Sulfur_percent,0) > 0 AND 
									ISNULL(InherentMoisture_percent,0) > 0 AND
								    ISNULL(Moisture_percent,0) > 0 THEN 1 ELSE 0 END)
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;
	END

	--Convert the ash basis if the proximate and ultimate are on different basis
	SET @UltimateAshConversion = 1;
	SELECT @UltimateBasisAsh = ISNULL(Ash_percent,0) FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;	
	IF @ProximateBasis != @UltimateBasis
	BEGIN
		IF @ProximateBasis = 1 AND @UltimateBasis = 2 --Convert As Received to Dry
		BEGIN
			SELECT @UltimateAshConversion = ISNULL(CASE WHEN ISNULL(Moisture_percent,0) < 100.0 THEN 100.0/(100.0-Moisture_percent) ELSE 0 END,0) 
			FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;			
		END
		IF @ProximateBasis = 1 AND @UltimateBasis = 3 --Convert As Received to Air Dried
		BEGIN
			SELECT @UltimateAshConversion = ISNULL(CASE WHEN ISNULL(Moisture_percent,0) < 100.0 THEN (100.0-InherentMoisture_percent)/(100.0-Moisture_percent) ELSE 0 END,0) 
			FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;			
		END
		IF @ProximateBasis = 2 AND @UltimateBasis = 1 --Convert Dry to As Received
		BEGIN
			SELECT @UltimateAshConversion = ISNULL((100.0-Moisture_percent)/100.0,0)
			FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;			
		END
		IF @ProximateBasis = 2 AND @UltimateBasis = 3 --Convert Dry to Air Dried
		BEGIN
			SELECT @UltimateAshConversion = ISNULL((100.0-InherentMoisture_percent)/100.0,0)
			FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;			
		END
		IF @ProximateBasis = 2 AND @UltimateBasis = 1 --Convert Air Dried to As Received
		BEGIN
			SELECT @UltimateAshConversion = ISNULL(CASE WHEN ISNULL(InherentMoisture_percent,0) < 100.0 THEN (100.0-Moisture_percent)/(100.0-InherentMoisture_percent) ELSE 0 END,0)
			FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;			
		END
		IF @ProximateBasis = 2 AND @UltimateBasis = 3 --Convert Air Dried to Dry
		BEGIN
			SELECT @UltimateAshConversion = ISNULL(CASE WHEN ISNULL(InherentMoisture_percent,0) < 100.0 THEN 100.0/(100.0-InherentMoisture_percent) ELSE 0 END,0)
			FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;			
		END
	END

	--Convert the ash to the ultimate basis and add it to the total
	SET @UltimateBasisAsh = @UltimateBasisAsh * @UltimateAshConversion;
	SET @Oxygen = @Oxygen - @UltimateBasisAsh;
	IF @UltimateBasisAsh <= 0
	BEGIN
		SET @UltimateValid = 0;
	END

	--Determine the undetermined ash so we can update the fuel quality record later
	--if it is valid.  Also check that all values except undetermined ash were entered.
	SELECT @UndeterminedAsh = 100.0-ISNULL(SiO2_percent+Al2O3_percent+TiO2_percent+Fe2O3_percent+CaO_percent+MgO_percent+K2O_percent
										   +Na2O_percent+SO3_percent+P2O5_percent+ISNULL(SrO_percent,0)+ISNULL(BaO_percent,0)+ISNULL(Mn3O4_percent,0),0),
	@AshValid = (CASE WHEN ISNULL(SiO2_percent,0) > 0 AND
					      ISNULL(Al2O3_percent,0) > 0 AND
					      ISNULL(TiO2_percent,0) > 0 AND
					      ISNULL(Fe2O3_percent,0) > 0 AND
					      ISNULL(CaO_percent,0) > 0 AND
					      ISNULL(MgO_percent,0) > 0 AND
					      ISNULL(K2O_percent,0) > 0 AND
					      ISNULL(Na2O_percent,0) > 0 AND
					      ISNULL(SO3_percent,0) > 0 AND
					      ISNULL(P2O5_percent,0) > 0 THEN 1 ELSE 0 END)
	FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;

	--A higher heating value and mercury content must be entered to be valid
	SELECT @MiscValid = (CASE WHEN HHV_btulbm > 0 AND Mercury_ppm > 0 THEN 1 ELSE 0 END) 
	FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;	

	--Raise an error with a low severity when the data is invalid so the message is shown
	--but does not abort processing or throw an error to the user.
	IF @FixedCarbon <= 0 OR @ProximateValid = 0	
	BEGIN
		SET @ProximateValid = 0
		RAISERROR(N'The current fuel quality:%d, has an invalid proximate analysis.',
				  0, -- Severity.
				  1, -- State.
				  @FuelQualityID);
	END

	IF @Oxygen <= 0 OR @UltimateValid = 0
	BEGIN
		SET @UltimateValid = 0
		RAISERROR(N'The current fuel quality:%d, has an invalid ultimate analysis.',
				  0, -- Severity.
				  1, -- State.
				  @FuelQualityID);
	END

	IF @UndeterminedAsh < 0 OR @AshValid = 0
	BEGIN
		SET @AshValid = 0
		RAISERROR(N'The current fuel quality:%d, has an invalid ash analysis.',
				  0, -- Severity.
				  1, -- State.
				  @FuelQualityID);
	END

	IF @MiscValid = 0
	BEGIN
		RAISERROR(N'The current fuel quality:%d, is either missing a higher heating value or mercury content.',
				  0, -- Severity.
				  1, -- State.
				  @FuelQualityID);
	END

	

	IF @ProximateValid = 1 AND @UltimateValid = 1 AND @AshValid = 1 AND @MiscValid = 1
	BEGIN 
		UPDATE Fuel.tSolidFuelQuality SET IsValid = 1, 
							    FixedCarbon_percent = @FixedCarbon, 
							    Oxygen_percent = @Oxygen,
								AshUndetermined_percent = @UndeterminedAsh
		FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;		
	END 
	ELSE
	BEGIN 
		UPDATE Fuel.tSolidFuelQuality SET IsValid = 0 FROM Fuel.tSolidFuelQuality WHERE FuelQualityID = @FuelQualityID;		
	END 
END
