﻿CREATE PROCEDURE [Fuel].[spUpdateAutocreatedFuelsAndBasins]
AS
BEGIN
	DECLARE @fuelSourceID int, @fuelPlanSetID int, @lastFuelPlanSetID int, @fuelBasinID int, @fuelPlanSetDesc nvarchar(255), @fuelBasinDesc nvarchar(255);
	DECLARE @fuelPlansReferenced int, @fuelBasinReferenced int
	--This query fetches all autocreated fuel sources that are used by exactly one fuel plan,
	--along with that fuel plan set ID and description.  Ordered by fuel plan set ID.


	DECLARE fsIDfpsIDfpsDesc_cursor CURSOR FOR 
	SELECT countedPlansAndSources.FuelSourceID, countedPlansAndSources.FuelPlanSetID, fps.FuelPlanSetDesc, countedPlansAndSources.FuelPlansReferenced, fs.FuelBasinID
	FROM
	(	SELECT COUNT(FuelPlanSetID) FuelPlansReferenced, FuelSourceID, MAX(FuelPlanSetID) FuelPlanSetID
		FROM
		(	SELECT distinct fps.FuelPlanSetID, autoFuels.FuelSourceID
			FROM
			(	SELECT *
				FROM Fuel.tFuelSource
				WHERE WasAutoCreated = 1
			) autoFuels
			join Projection.tFuelChangeEvent fce ON autoFuels.FuelSourceID = fce.FuelSourceID
			left outer join Projection.tFuelPlanSet fps on fps.FuelPlanSetID = fce.FuelPlanSetID
		) distinctFPSandFSIDs
		group by FuelSourceID
	) countedPlansAndSources
	join Projection.tFuelPlanSet fps on fps.FuelPlanSetID = countedPlansAndSources.FuelPlanSetID
	join Fuel.tFuelSource fs on fs.FuelSourceID = countedPlansAndSources.FuelSourceID
	left join Fuel.tFuelBasin fb on fs.FuelBasinID = fb.FuelBasinID
	WHERE (FuelPlansReferenced > 1 AND FuelBasinDesc != 'Used by Multiple Plans') OR (fs.FuelBasinID is null OR fs.FuelBasinID <= 0)
	ORDER BY FuelPlansReferenced, FuelPlanSetID
	;

	OPEN fsIDfpsIDfpsDesc_cursor;

	set @lastFuelPlanSetID = -1;
	FETCH NEXT FROM fsIDfpsIDfpsDesc_cursor 
	INTO @fuelSourceID, @fuelPlanSetID, @fuelPlanSetDesc, @fuelPlansReferenced, @fuelBasinReferenced;

	declare @newFuelARGB int;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--do the actual work here!
		IF @fuelPlansReferenced > 1
		BEGIN
			set @fuelBasinDesc = (SELECT 'Used by Multiple Plans');
			IF ((SELECT COUNT(*) FROM Fuel.tFuelBasin WHERE FuelBasinDesc = @fuelBasinDesc) < 1)
			BEGIN
				set @newFuelARGB = CONVERT(int, RAND() * 2147483647, 0);
				WHILE (SELECT COUNT(*) FROM Fuel.tFuelBasin WHERE BasinColorARGB = @newFuelARGB) > 0
				BEGIN
					set @newFuelARGB = CONVERT(int, RAND() * 2147483647, 0);
				END
				INSERT INTO [Fuel].[tFuelBasin]
				([FuelBasinDesc] 
				,[BasinColorARGB]
				,[CreatedBy] 
				,[ChangedBy]) 
				VALUES 
				(@fuelBasinDesc, @newFuelARGB, 'spUpdateAutocreatedFuelsAndBasins','spUpdateAutocreatedFuelsAndBasins') ;
			END 
			set @fuelBasinID = (SELECT FuelBasinID FROM Fuel.tFuelBasin WHERE FuelBasinDesc = @fuelBasinDesc);
		END
		ELSE IF @fuelPlansReferenced = 1
		BEGIN
			IF @fuelPlanSetID != @lastFuelPlanSetID
			BEGIN
				--If we've moved to a new fuel plan set, we need to see if a corresponding basin exists,
				--and create said basin if it does not.
				set @fuelBasinDesc = (SELECT 'Fuel Plan: ' + @fuelPlanSetDesc);
				IF ((SELECT COUNT(*) FROM Fuel.tFuelBasin WHERE FuelBasinDesc = @fuelBasinDesc) < 1)
				BEGIN
					set @newFuelARGB = CONVERT(int, RAND() * 2147483647, 0);
					WHILE (SELECT COUNT(*) FROM Fuel.tFuelBasin WHERE BasinColorARGB = @newFuelARGB) > 0
					BEGIN
						set @newFuelARGB = CONVERT(int, RAND() * 2147483647, 0);
					END
					INSERT INTO [Fuel].[tFuelBasin]
					([FuelBasinDesc] 
					,[BasinColorARGB]
					,[CreatedBy] 
					,[ChangedBy]) 
					VALUES 
					(@fuelBasinDesc, @newFuelARGB, 'spUpdateAutocreatedFuelsAndBasins','spUpdateAutocreatedFuelsAndBasins') ;
				END 
				set @fuelBasinID = (SELECT FuelBasinID FROM Fuel.tFuelBasin WHERE FuelBasinDesc = @fuelBasinDesc);
			END ;
		END ;
		UPDATE Fuel.tFuelSource 
		SET FuelBasinID = @fuelBasinID
		WHERE FuelSourceID = @fuelSourceID ;
		
		--once this pass is finished, move on to the next.
		SET @lastFuelPlanSetID = @fuelPlanSetID;
		FETCH NEXT FROM fsIDfpsIDfpsDesc_cursor 
		INTO @fuelSourceID, @fuelPlanSetID, @fuelPlanSetDesc, @fuelPlansReferenced, @fuelBasinReferenced;
	END
	CLOSE fsIDfpsIDfpsDesc_cursor;
	DEALLOCATE fsIDfpsIDfpsDesc_cursor;
 END
 ;
GO
GRANT EXECUTE
    ON OBJECT::[Fuel].[spUpdateAutocreatedFuelsAndBasins] TO [TEUser]
    AS [dbo];

