﻿CREATE TABLE [Event].[tEvent] (
    [EventID]          INT                NOT NULL,
    [Notes]            NVARCHAR (MAX)     NULL,
    [PlannedStartDate] DATETIMEOFFSET (7) NULL,
    [PlannedEndDate]   DATETIMEOFFSET (7) NULL,
    [ActualStartDate]  DATETIMEOFFSET (7) NULL,
    [ActualEndDate]    DATETIMEOFFSET (7) NULL,
    PRIMARY KEY CLUSTERED ([EventID] ASC),
    CONSTRAINT [FK_Event_EventID_Asset_AssetID] FOREIGN KEY ([EventID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);

