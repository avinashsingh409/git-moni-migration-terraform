﻿CREATE   FUNCTION [Event].[ufnGetEventsForAsset]
(
    @securityUserID int,
    @assetID int,
	@eventClassTypeID int = null,
	@startTime datetimeoffset = null,
	@endTime datetimeoffset = null,
	@eventDesc nvarchar(255) = null
)
RETURNS TABLE AS RETURN
(
    SELECT * FROM [Event].ufnGetEvents(@securityUserID, @assetID)
	WHERE AssetID = @assetID
	AND (ISNULL(@eventClassTypeID, -1) <= 0 OR AssetClassTypeID = @eventClassTypeID)
	AND (@startTime is null OR @startTime <= PlannedStartDate OR @startTime <= PlannedEndDate OR @startTime <= ActualStartDate OR @startTime <= ActualEndDate)
	AND (@endTime is null OR @endTime >= PlannedStartDate OR @endTime >= PlannedEndDate OR @endTime >= ActualStartDate OR @endTime >= ActualEndDate)
	AND (@eventDesc is null or EventDesc like @eventDesc)
)
GO
GRANT SELECT
    ON OBJECT::[Event].[ufnGetEventsForAsset] TO [TEUser]
    AS [dbo];

