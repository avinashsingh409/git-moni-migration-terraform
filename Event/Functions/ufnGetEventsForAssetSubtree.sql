﻿
CREATE   FUNCTION [Event].[ufnGetEventsForAssetSubtree]
(
    @securityUserID int,
    @startingAssetID int,
	@eventClassTypeID int = null,
	@startTime datetimeoffset = null,
	@endTime datetimeoffset = null,
	@eventDesc nvarchar(255) = null
)
RETURNS TABLE AS RETURN
(
    SELECT * FROM [Event].ufnGetEvents(@securityUserID, @startingAssetID)
	WHERE (ISNULL(@eventClassTypeID, -1) < 0 OR AssetClassTypeID = @eventClassTypeID)
	AND (@startTime is null OR @startTime <= PlannedStartDate OR @startTime <= PlannedEndDate OR @startTime <= ActualStartDate OR @startTime <= ActualEndDate)
	AND (@endTime is null OR @endTime >= PlannedStartDate OR @endTime >= PlannedEndDate OR @endTime >= ActualStartDate OR @endTime >= ActualEndDate)
	and (@eventDesc is null or EventDesc like @eventDesc)
)
GO
GRANT SELECT
    ON OBJECT::[Event].[ufnGetEventsForAssetSubtree] TO [TEUser]
    AS [dbo];

