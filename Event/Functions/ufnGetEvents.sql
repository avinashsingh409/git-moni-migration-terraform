﻿--Essentially a parameterized view of event data, integrating security handling.
--Will write C# retrievers that supply further WHERE clauses.
--Unless we decide more specific SQL functions/sprocs would be better.
CREATE FUNCTION [Event].[ufnGetEvents]
(
	@securityUserID INT,
	@assetSubtreeRootID INT = null
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		eventAsset.[AssetID] as [EventID],
		eventAsset.[ParentAssetID] as [EventGroupID],
		eventGroupAsset.[ParentAssetID] as [AssetID],
		eventAsset.[GlobalID] AS [EventGuid],
		eventAsset.[AssetAbbrev] AS [EventAbbrev],
		eventAsset.[AssetDesc] AS [EventDesc],
		eventAsset.AssetClassTypeID,
		eventAsset.AssetTreeNodeChildConfigurationTypeID,
		eventAsset.DisplayOrder,
		createdUser.SecurityUserID AS [CreatedBy],
		CAST(eventAsset.[CreateDate] AS DATETIMEOFFSET) AS [CreatedDate],
		changedUser.SecurityUserID AS [ChangedBy],
		CAST(eventAsset.[ChangeDate] AS DATETIMEOFFSET) AS [ChangedDate],
		eventAsset.IsHidden,
		eventAsset.Track,
		tE.Notes [EventNotes],
		tE.PlannedStartDate,
		tE.PlannedEndDate,
		tE.ActualStartDate,
		tE.ActualEndDate
	FROM Asset.tAsset eventAsset
	LEFT JOIN [Event].tEvent tE on tE.EventID = eventAsset.AssetID
	JOIN Asset.tAsset eventGroupAsset on eventGroupAsset.AssetID = eventAsset.ParentAssetID
	JOIN AccessControl.tUser createdUser on createdUser.Username = eventAsset.CreatedBy
	JOIN AccessControl.tUser changedUser on changedUser.Username = eventAsset.ChangedBy
	JOIN Asset.tAssetClassType act on eventAsset.AssetClassTypeID = act.AssetClassTypeID
	INNER JOIN Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @assetSubtreeRootID) validAssets ON eventAsset.AssetID = validAssets.AssetID
	WHERE act.AssetTypeID = 9	--Event AssetType
)
GO
GRANT SELECT
    ON OBJECT::[Event].[ufnGetEvents] TO [TEUser]
    AS [dbo];

