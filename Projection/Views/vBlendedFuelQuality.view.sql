﻿CREATE VIEW [Projection].[vBlendedFuelQuality]
AS
SELECT b.FuelSourceDesc FuelQualityDesc
	  ,b.FuelBasinID
	  ,e.FuelBasinDesc
	  ,a.ScenarioID
      ,a.[UnitID]
      ,d.[Year]
      ,d.[Month]
      ,d.[Day]
	  ,d.[Hour]
      ,c.[HHV_btulbm]/100.0 as Quantity_ton
      ,c.[ProximateBasis]
      ,c.[HHV_btulbm]
      ,c.[Moisture_percent]
      ,c.[InherentMoisture_percent]
      ,c.[Ash_percent]
      ,c.[Sulfur_percent]
      ,c.[VolatileMatter_percent]
      ,c.[FixedCarbon_percent]
      ,c.[UltimateBasis]
      ,c.[Carbon_percent]
      ,c.[Hydrogen_percent]
      ,c.[Nitrogen_percent]
      ,c.[Chlorine_percent]
      ,c.[Oxygen_percent]
      ,c.[SIO2_percent]
      ,c.[AL2O3_percent]
      ,c.[TIO2_percent]
      ,c.[Fe2O3_percent]
      ,c.[CAO_percent]
      ,c.[MGO_percent]
      ,c.[K2O_percent]
      ,c.[NA2O_percent]
      ,c.[SO3_percent]
      ,c.[P2O5_percent]
      ,c.[SRO_percent]
      ,c.[BAO_percent]
      ,c.[MN3O4_percent]
      ,c.[AshUndetermined_percent]
      ,c.[AshFusionReducingInitialDeformation_f]
      ,c.[AshFusionReducingSoftening_f]
      ,c.[AshFusionReducingHemispherical_f]
      ,c.[AshFusionReducingFluid_f]
      ,c.[AshFusionOxidizingInitialDeformation_f]
      ,c.[AshFusionOxidizingSoftening_f]
      ,c.[AshFusionOxidizingHemispherical_f]
      ,c.[AshFusionOxidizingFluid_f]
      ,c.[T250_f]
      ,c.[HGI]
      ,c.[BaseToAcidRatio_fraction]
      ,c.[EquilibriumMoisture_percent]
      ,c.[SulfurFormPyritic_percent]
      ,c.[SulfurFormSulfate_percent]
      ,c.[SulfurFormOrganic_percent]
      ,c.[Mercury_ppm]
	  ,CONVERT(real,CASE WHEN c.[HHV_btulbm] > 0 THEN 1000000.0 / c.[HHV_btulbm] * c.[Sulfur_percent] / 100.0 * 1.9978 ELSE 0 END) as SO2_lbmmbtu
	  ,CONVERT(real,CASE WHEN c.[HHV_btulbm] > 0 THEN 1000000000000.0 / c.[HHV_btulbm] * c.[Mercury_ppm] * ((100.0 - c.[Moisture_percent]) / 100.0) / 1000000.0 ELSE 0 END) as Mercury_lbmtbtu
	  ,CONVERT(real,RAND(c.[SIO2_percent])) as SlaggingIndex
	  ,CONVERT(real,RAND(c.[Sulfur_percent])) as FoulingIndex
 	  ,CONVERT(real,RAND(c.[AL2O3_percent])) as ErosionIndex
FROM Projection.tScenarioFuelBurnSchedule a INNER JOIN Fuel.tFuelSource b ON a.FuelSourceID = b.FuelSourceID
INNER JOIN Fuel.tSolidFuelQuality c ON a.FuelQualityID = c.FuelQualityID
INNER JOIN Base.DateDimension d ON a.[Year] = d.[Year] AND a.[Month] = d.[Month] AND a.[Day] = d.[Day] 
LEFT OUTER JOIN Fuel.tFuelBasin e ON b.FuelBasinID = e.FuelBasinID
