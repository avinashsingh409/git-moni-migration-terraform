﻿CREATE VIEW Projection.vHourlyScenarioConfig AS
select b.ScenarioID,a.UnitID, 
a.year,a.month,a.day,a.hour,a.EffectiveTime,
c.FuelSourceID
,d.FuelQualityID
,e.UnitConfigID 
,a.Generation_mw
FROM
projection.tGenerationDetail_Adjusted a
JOIN Projection.tScenario b on a.GenerationSetID=b.GenerationSetID 
JOIN Projection.tFuelChangeEvent c on b.FuelPlanSetID=c.FuelPlanSetID and a.UnitID=c.UnitID and c.FuelChangeEventID = 
(SELECT TOP 1 FuelChangeEventID FROM Projection.tFuelChangeEvent fce where fce.FuelPlanSetID=b.FuelPlanSetID and fce.UnitID=a.UnitID and 
fce.EventTime<=a.EffectiveTime order by fce.EventTime desc)
JOIN Fuel.tFuelQuality d on 
c.FuelSourceID=d.FuelSourceID 
AND d.FuelQualityID = (SELECT TOP 1 fq.FuelQualityID FROM Fuel.tFuelQuality fq where 
fq.FuelSourceID=d.FuelSourceID AND 
fq.EffectiveDate<=a.EffectiveTime ORDER BY fq.EffectiveDate DESC)
JOIN Projection.tUnitConfigScheduleDetail e ON b.UnitConfigScheduleSetID = e.UnitConfigScheduleSetID 
AND e.UnitConfigScheduleDetailID = (Select TOP 1 UnitConfigScheduleDetailID FROM 
Projection.tUnitConfigScheduleSet ucss JOIN 
Projection.tUnitConfigScheduleDetail ucsd ON ucss.UnitConfigScheduleSetID=b.UnitConfigScheduleSetID  
and ucss.UnitConfigScheduleSetID=ucsd.UnitConfigScheduleSetID
JOIN projection.tUnitConfig uc ON ucsd.UnitConfigID = uc.UnitConfigID 
AND uc.UnitID=a.UnitID 
WHERE ucsd.EffectiveDate<=a.EffectiveTime  order by ucsd.EffectiveDate desc)