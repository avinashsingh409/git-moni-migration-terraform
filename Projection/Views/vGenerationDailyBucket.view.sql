﻿CREATE VIEW [Projection].[vGenerationDailyBucket]
AS
SELECT a.ScenarioID,b.UnitID,b.[Year],b.[Month],b.[Day],
b.LoadBucket_mw,b.Hours
FROM Projection.tScenario a INNER JOIN Projection.tGenerationDailyBucket b 
ON a.GenerationSetID = b.GenerationSetID
