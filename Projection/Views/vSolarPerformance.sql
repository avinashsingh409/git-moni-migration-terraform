﻿CREATE VIEW [Projection].[vSolarPerformance] AS
SELECT sp.SolarPerformanceID, 
   a.AssetID, 
   a.AssetDesc,a.AssetAbbrev,
   wa.[WeatherAssetID],
   sp.SolarPerformanceSetID, 
   sp.TimeStamp, 
   sp.TypicalPOAIrradiance, 
   sp.TypicalPowerOutput,
   sp.ForecastPowerOutput,
   sp.ForecastPowerOutput*1000 as ForecastPowerOutput_kW,
   datepart(year,sp.TimeStamp) as [Year],
   datepart(month,sp.TimeStamp) as [Month],
   datepart(day,sp.TimeStamp) as [Day],
   datepart(hour,sp.TimeStamp) as [Hour]
FROM Projection.tSolarPerformance sp,
   Asset.tAsset a,
   Projection.tWeatherAsset wa 
WHERE a.AssetID = sp.AssetID
  AND a.AssetID = wa.AssetID