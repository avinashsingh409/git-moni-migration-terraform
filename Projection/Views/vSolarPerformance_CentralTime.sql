﻿--------------------------------------------------------------------------------
CREATE VIEW [Projection].[vSolarPerformance_CentralTime] AS
SELECT sp.SolarPerformanceID, 
   sp.AssetID, 
   sp.AssetDesc,
   sp.AssetAbbrev,
   sp.[WeatherAssetID],
   sp.SolarPerformanceSetID,    
   DateAdd(HOUR,-(ISNULL(aat.attribute_int,-5) + 5), sp.TimeStamp) As TimeStamp,
   sp.TypicalPOAIrradiance, 
   sp.TypicalPowerOutput,
   sp.ForecastPowerOutput,
   sp.ForecastPowerOutput*1000 as ForecastPowerOutput_kW,
   datepart(year,DateAdd(HOUR,-(ISNULL(aat.attribute_int,-5) + 5), sp.TimeStamp)) as [Year],
   datepart(month,DateAdd(HOUR,-(ISNULL(aat.attribute_int,-5) + 5), sp.TimeStamp)) as [Month],
   datepart(day,DateAdd(HOUR,-(ISNULL(aat.attribute_int,-5) + 5), sp.TimeStamp)) as [Day],
   datepart(hour,DateAdd(HOUR,-(ISNULL(aat.attribute_int,-5) + 5), sp.TimeStamp)) as [Hour]
FROM 
Projection.vSolarPerformance sp 
LEFT JOIN Asset.tAssetAttribute aat ON sp.AssetID = aat.AssetID 
WHERE aat.AttributeTypeID = (Select attributetypeid from Asset.tAttributeType where AttributeTypeKey='TimeZone')