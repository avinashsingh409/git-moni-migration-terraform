﻿CREATE VIEW [Projection].[vGenerationDetail] AS
SELECT  a.GenerationDetailID,a.GenerationSetID,a.UnitID,a.[Year], a.[Month],a.[Day], a.[Hour],
MAX(ISNULL(c.Adjustment_mw,a.Generation_mw)) as Generation_mw, MAX(a.MinGeneration_mw) AS MinGeneration_mw,
MAX(a.MaxGeneration_mw) AS MaxGeneration_mw,MAX(case when a.OfflineAllowed=1 then 1 else 0 end) AS OfflineAllowed
FROM Projection.tGenerationDetail a
LEFT OUTER JOIN Projection.tGenerationAdjustmentMap b ON a.GenerationSetID=b.GenerationSetID
LEFT OUTER JOIN Projection.tGenerationAdjustmentDetail c ON b.GenerationAdjustmentSetID=b.GenerationAdjustmentSetID
AND a.UnitID=c.UnitID AND
a.[Year] = c.[Year] and a.[Month] = c.[Month] and a.[Day] = c.[Day] and a.Hour = c.Hour
GROUP BY a.GenerationDetailID,a.GenerationSetID,a.UnitID,a.[Year], a.[Month],a.[Day], a.[Hour]




