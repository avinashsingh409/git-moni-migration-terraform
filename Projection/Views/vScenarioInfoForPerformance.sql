﻿
CREATE View Projection.vScenarioInfoForPerformance AS
SELECT a.ScenarioID,a.ScenarioDesc,
a.OwningAssetID,
a.CreateDate,
a.CreatedBy,
a.IsPublic,
a.IsArchived,
a.IsConsensus,
a.PlanYear,
dv.PlanPeriod,
dv.DataVersionDesc,
Base.DateSerial(a.PlanYear,(case when dv.PlanPeriod=12 then 0 else dv.PlanPeriod END)+1,1,0,0,0) as ScenarioStartDate,
Base.DateSerial(CONVERT(int,LEFT(CONVERT(varchar(8),i.MaxDate),4)),
CONVERT(int,SUBSTRING(CONVERT(varchar(8),i.MaxDate),5,2)),
CONVERT(int,SUBSTRING(CONVERT(varchar(8),i.MaxDate),7,2)),0,0,0) as ScenarioEndDate,
a.GenerationSetID,
c.GenerationSetDesc,
isnull(a.DemandSetID,a.GenerationSetID) as DemandSetID,
isnull(d.GenerationSetDesc,c.GenerationSetDesc) as DemandSetDesc,
convert(bit,case when a.DemandSetID is null then 0 else 1 end) as HasDemandSet,
a.FuelPlanSetID,
e.FuelPlanSetDesc,
a.UnitConfigScheduleSetID,
f.UnitConfigScheduleSetDesc,
a.MaintenancePlanSetID,
g.MaintenancePlanSetDesc,
h.ForecastSetID,
h.ForecastSetDesc
 FROM Projection.tScenario a 
 JOIN Projection.tDataVersion dv ON a.DataVersionID=dv.DataVersionID  
JOIN Projection.tGenerationSet c ON a.GenerationSetID=c.GenerationSetID
LEFT JOIN Projection.tGenerationSet d on a.DemandSetID = d.GenerationSetID
JOIN Projection.tFuelPlanSet e ON a.FuelPlanSetID=e.FuelPlanSetID
JOIN Projection.tUnitConfigScheduleSet f ON a.UnitConfigScheduleSetID=f.UnitConfigScheduleSetID
LEFT JOIN PowerRAM.tMaintenancePlanSet g ON a.MaintenancePlanSetID = g.MaintenancePlanSetID
LEFT JOIN Economic.tForecastSet h ON a.ForecastSetID = h.ForecastSetID
JOIN (SELECT GenerationSetID,
MAX(YEAR * 10000 + MONTH * 100 + DAY) AS MaxDate FROM Projection.tGenerationDetail_Adjusted GROUP BY GenerationSetID) i ON a.GenerationSetID=i.GenerationSetID
Where RunStatus=3