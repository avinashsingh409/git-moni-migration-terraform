﻿CREATE VIEW Projection.vCQIKeysByScenario AS
SELECT b.ScenarioID,
c.FuelSourceID
,d.FuelQualityID
,e.UnitConfigID 
,MIN(a.EffectiveTime) as EffectiveTime
FROM

Projection.tScenario b 
join (select generationsetid,min(effectivetime) as minTime,MAX(effectivetime) as maxTime from projection.tGenerationDetail_Adjusted gda group by generationsetid) gen on 
gen.GenerationSetID = b.GenerationSetID

join 
(

select scenarioid,unitid,b.EventTime as effectivetime from Projection.tScenario a join projection.tFuelChangeEvent b on a.FuelPlanSetID=b.FuelPlanSetID 
UNION
select scenarioid,unitid,c.EffectiveDate as effectivetime from Projection.tScenario a join projection.tFuelChangeEvent b on a.FuelPlanSetID=b.FuelPlanSetID JOIN 
Fuel.tFuelQuality c on b.FuelSourceID=c.FuelSourceID 
UNION
select scenarioid,unitid,effectivedate as effectivetime from projection.tScenario a join projection.tUnitConfigScheduleDetail b on a.UnitConfigScheduleSetID = b.UnitConfigScheduleSetID 
join projection.tUnitConfig c on b.UnitConfigID = c.UnitConfigID
UNION
select scenarioid,unitid,min(effectivetime) as effectivetime from projection.tGenerationDetail_Adjusted gda
join projection.tScenario b on gda.GenerationSetID=b.GenerationSetID group by scenarioid,unitid
UNION
select scenarioid,unitid,MAX(effectivetime) as effectivetime from projection.tGenerationDetail_Adjusted gda
join projection.tScenario b on gda.GenerationSetID=b.GenerationSetID group by scenarioid,unitid

) a on a.ScenarioID=b.ScenarioID 

JOIN Projection.tFuelChangeEvent c on b.FuelPlanSetID=c.FuelPlanSetID and a.UnitID=c.UnitID and c.FuelChangeEventID = 
(SELECT TOP 1 FuelChangeEventID FROM Projection.tFuelChangeEvent fce where fce.FuelPlanSetID=b.FuelPlanSetID and fce.UnitID=a.UnitID and 
fce.EventTime<=a.EffectiveTime order by fce.EventTime desc)
JOIN Fuel.tFuelQuality d on 
c.FuelSourceID=d.FuelSourceID 
AND d.FuelQualityID = (SELECT TOP 1 fq.FuelQualityID FROM Fuel.tFuelQuality fq where 
fq.FuelSourceID=d.FuelSourceID AND 
fq.EffectiveDate<=a.EffectiveTime ORDER BY fq.EffectiveDate DESC)
JOIN Projection.tUnitConfigScheduleDetail e ON b.UnitConfigScheduleSetID = e.UnitConfigScheduleSetID 
AND e.UnitConfigScheduleDetailID = (Select TOP 1 UnitConfigScheduleDetailID FROM 
Projection.tUnitConfigScheduleSet ucss JOIN 
Projection.tUnitConfigScheduleDetail ucsd ON ucss.UnitConfigScheduleSetID=b.UnitConfigScheduleSetID  
and ucss.UnitConfigScheduleSetID=ucsd.UnitConfigScheduleSetID
JOIN projection.tUnitConfig uc ON ucsd.UnitConfigID = uc.UnitConfigID 
AND uc.UnitID=a.UnitID 
WHERE ucsd.EffectiveDate<=a.EffectiveTime  order by ucsd.EffectiveDate desc)

where a.EffectiveTime between minTime and maxTime

Group by b.ScenarioID,c.FuelSourceID
,d.FuelQualityID
,e.UnitConfigID