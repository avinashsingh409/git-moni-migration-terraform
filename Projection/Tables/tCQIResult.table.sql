﻿CREATE TABLE [Projection].[tCQIResult] (
    [CQIResultID]     INT              IDENTITY (1, 1) NOT NULL,
    [FuelQualityID]   INT              NOT NULL,
    [UnitConfigID]    INT              NOT NULL,
    [LoadPoint]       INT              NOT NULL,
    [VistaEvaluation] UNIQUEIDENTIFIER NOT NULL,
    [FuelSourceID]    INT              NOT NULL,
    [IsValid]         BIT              DEFAULT ((0)) NOT NULL,
    [IsModified]      BIT              DEFAULT ((0)) NOT NULL,
    [CQIRunTypeID]    INT              NOT NULL,
    [DidIPMConverge]  BIT              CONSTRAINT [DF_tCQIResult_DidIPMConverge] DEFAULT ((0)) NOT NULL,
    [VistaResultID]   BIGINT           CONSTRAINT [DF_tCQIResult_VistaResultID] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCQIResult] PRIMARY KEY CLUSTERED ([CQIResultID] ASC),
    CONSTRAINT [FK_tCQIResult_CQIRunTypeID_tCQIRunType] FOREIGN KEY ([CQIRunTypeID]) REFERENCES [Projection].[tCQIRunType] ([CQIRunTypeID]),
    CONSTRAINT [FK_tCQIResult_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tCQIResult_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tCQIResult_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tCQIResult_FuelQualityID_UnitConfigID_FuelSourceID_CQIRunTypeID_LoadPoint] UNIQUE NONCLUSTERED ([FuelQualityID] ASC, [UnitConfigID] ASC, [FuelSourceID] ASC, [CQIRunTypeID] ASC, [LoadPoint] ASC)
);










GO
CREATE NONCLUSTERED INDEX IDX_tCQIResult_CQIRunType
ON [Projection].[tCQIResult] ([CQIRunTypeID])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResult';

