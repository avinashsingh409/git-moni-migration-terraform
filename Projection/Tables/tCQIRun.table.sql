CREATE TABLE [Projection].[tCQIRun] (
    [CQIRunID]        INT            IDENTITY (1, 1) NOT NULL,
    [FuelSourceID]    INT            NOT NULL,
    [FuelQualityID]   INT            NOT NULL,
    [UnitConfigID]    INT            NOT NULL,
    [RunStatus]       TINYINT        NOT NULL,
    [CQIRunTypeID]    INT            NOT NULL,
    [StartTime]       DATETIME       NULL,
    [StopTime]        DATETIME       NULL,
    [ExecutingOnHost] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tCQIRun] PRIMARY KEY CLUSTERED ([CQIRunID] ASC),
    CONSTRAINT [FK_tCQIRun_CQIRunTypeID_tCQIRunType] FOREIGN KEY ([CQIRunTypeID]) REFERENCES [Projection].[tCQIRunType] ([CQIRunTypeID]),
    CONSTRAINT [FK_tCQIRun_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tCQIRun_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tCQIRun_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIRun';

