﻿CREATE TABLE [Projection].[tGenerationSet] (
    [GenerationSetID]         INT            IDENTITY (1, 1) NOT NULL,
    [GenerationSetDesc]       NVARCHAR (255) NOT NULL,
    [DataVersionID]           INT            NULL,
    [PlanYear]                SMALLINT       CONSTRAINT [DF__tGeneratio__Year__0BE6BFCF] DEFAULT (datepart(year,getdate())) NOT NULL,
    [IsConsensus]             BIT            CONSTRAINT [DF__tGenerati__IsCon__0CDAE408] DEFAULT ((0)) NOT NULL,
    [IsSensitivity]           BIT            CONSTRAINT [DF_tGenerationSet_IsSensitivity] DEFAULT ((0)) NOT NULL,
    [CreatedBy]               NVARCHAR (255) NOT NULL,
    [ChangedBy]               NVARCHAR (255) NOT NULL,
    [CreateDate]              DATETIME       CONSTRAINT [DF__tGenerati__Creat__0DCF0841] DEFAULT (getdate()) NOT NULL,
    [SensitivityImpactTypeID] INT            NULL,
    [ModifiedDate]            DATETIME       CONSTRAINT [DF_tGenerationSet_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]              DATETIME       DEFAULT (getdate()) NOT NULL,
    [ImportDate]              DATETIME       NULL,
    [Comment]                 NVARCHAR (255) NULL,
    [IsPublic]                BIT            CONSTRAINT [DF_tGenerationSet_IsPublic] DEFAULT ((0)) NOT NULL,
    [IsValid]                 BIT            DEFAULT ((1)) NOT NULL,
    [IsArchived]              BIT            CONSTRAINT [DF_tGenerationSet_IsArchived] DEFAULT ((0)) NOT NULL,
    [GenerationSetType]       INT            DEFAULT ((0)) NOT NULL,
    [OwningAssetID]           INT            NULL,
    [GenerationSetAbbrev]     NVARCHAR (50)  CONSTRAINT [GenerationSetAbbrevDefault] DEFAULT ('') NOT NULL,
    [StartDate]               DATETIME2 (7)  NULL,
    [EndDate]                 DATETIME2 (7)  NULL,
    CONSTRAINT [PK_tGenerationSet] PRIMARY KEY CLUSTERED ([GenerationSetID] ASC),
    CONSTRAINT [FK_Projection_tGenerationSet_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tGenerationSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID]),
    CONSTRAINT [FK_tGenerationSet_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of hourly generation data, adjustments and burn and emission assumptions.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationSet';

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationSet';


GO
CREATE TRIGGER [Projection].[tGenerationSet_OnUpdate]
   ON  [Projection].[tGenerationSet] 
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    IF UPDATE(ImportDate)
      BEGIN
      UPDATE Projection.tGenerationSet SET StartDate = a.StartDate,EndDate=a.EndDate FROM 
      (SELECT a.generationsetid,MIN(EffectiveTime) as StartDate, MAX(EffectiveTime) as EndDate FROM 
      inserted a join Projection.tGenerationDetail_Adjusted b on a.GenerationSetID=b.GenerationSetID GROUP BY 
      a.GenerationSetID) a where a.GenerationSetID=Projection.tGenerationSet.GenerationSetID
      END

END