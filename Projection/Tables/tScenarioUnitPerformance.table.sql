CREATE TABLE [Projection].[tScenarioUnitPerformance] (
    [ScenarioUnitPerformanceID]       INT      IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                      INT      NOT NULL,
    [FuelQualityID]                   INT      NOT NULL,
    [UnitID]                          INT      NOT NULL,
    [Year]                            SMALLINT NOT NULL,
    [Month]                           TINYINT  NOT NULL,
    [Day]                             TINYINT  NOT NULL,
    [GrossGeneration_mwh]             REAL     NOT NULL,
    [NetGeneration_mwh]               REAL     NOT NULL,
    [NetPlantHeatRate_btukwh]         REAL     NOT NULL,
    [FuelBurnRate_tonhr]              REAL     NOT NULL,
    [FuelBurnRate_mbtuhr]             REAL     NOT NULL,
    [SO2_ton]                         REAL     NOT NULL,
    [NOx_ton]                         REAL     NOT NULL,
    [Hg_lbm]                          REAL     NOT NULL,
    [CO2_ton]                         REAL     NOT NULL,
    [SO2Rate_lbmmbtu]                 REAL     NOT NULL,
    [NOxRate_lbmmbtu]                 REAL     NOT NULL,
    [CO2Rate_lbmmbtu]                 REAL     NOT NULL,
    [HgRate_lbmtbtu]                  REAL     NOT NULL,
    [Particulate_lbmmbtu]             REAL     NOT NULL,
    [H2SO4Rate_lbmmbtu]               REAL     NOT NULL,
    [BottomAsh_tonhr]                 REAL     NOT NULL,
    [FlyAsh_tonhr]                    REAL     NOT NULL,
    [Gypsum_tonhr]                    REAL     NOT NULL,
    [FGDAdditive_tonhr]               REAL     NOT NULL,
    [FGDFixative_tonhr]               REAL     NOT NULL,
    [FGDWaste_tonhr]                  REAL     NOT NULL,
    [DryFGDWaste_tonhr]               REAL     NOT NULL,
    [FurnaceSorbent_tonhr]            REAL     NOT NULL,
    [SO3Feedstock_tonhr]              REAL     NOT NULL,
    [CoalAdditive_tonhr]              REAL     NOT NULL,
    [SCRReagent_tonhr]                REAL     NOT NULL,
    [SNCRReagent_tonhr]               REAL     NOT NULL,
    [Particulate_ton]                 REAL     NOT NULL,
    [H2SO4Rate_ton]                   REAL     NOT NULL,
    [BottomAsh_ton]                   REAL     NOT NULL,
    [FlyAsh_ton]                      REAL     NOT NULL,
    [Gypsum_ton]                      REAL     NOT NULL,
    [FGDAdditive_ton]                 REAL     NOT NULL,
    [FGDFixative_ton]                 REAL     NOT NULL,
    [FGDWaste_ton]                    REAL     NOT NULL,
    [DryFGDWaste_ton]                 REAL     NOT NULL,
    [FurnaceSorbent_ton]              REAL     NOT NULL,
    [SO3Feedstock_ton]                REAL     NOT NULL,
    [CoalAdditive_ton]                REAL     NOT NULL,
    [SCRReagent_ton]                  REAL     NOT NULL,
    [SNCRReagent_ton]                 REAL     NOT NULL,
    [MaxGrossPower_mw]                REAL     NOT NULL,
    [FuelBurn_ton]                    REAL     NOT NULL,
    [FuelBurn_mbtu]                   REAL     NOT NULL,
    [EAF_percent]                     REAL     NOT NULL,
    [EFOR_hour]                       REAL     NOT NULL,
    [UnitConfigID]                    INT      NOT NULL,
    [GrossLoad_mw]                    REAL     NOT NULL,
    [NetLoad_mw]                      REAL     NOT NULL,
    [AuxLoad_mw]                      REAL     NOT NULL,
    [FuelSourceID]                    INT      NOT NULL,
    [CORate_lbmmbtu]                  REAL     NULL,
    [CO2Rate_tonhr]                   REAL     NULL,
    [SO2InletRate_lbmmbtu]            REAL     NULL,
    [NOxInletRate_lbmmbtu]            REAL     NULL,
    [Opacity_percent]                 REAL     NULL,
    [FGDRemovalEfficiency_percent]    REAL     NULL,
    [SCRRemovalEfficiency_percent]    REAL     NULL,
    [HgAdditive_tonhr]                REAL     NOT NULL,
    [HgAdditive_ton]                  REAL     NOT NULL,
    [CO_lbm]                          REAL     NOT NULL,
    [VOC_lbm]                         REAL     NOT NULL,
    [PM_lbm]                          REAL     NOT NULL,
    [VOCRate_lbmmbtu]                 REAL     NOT NULL,
    [PMRate_lbmmbtu]                  REAL     NOT NULL,
    [SaleableFlyAsh_ton]              REAL     NOT NULL,
    [NonSaleableFlyAsh_ton]           REAL     NOT NULL,
    [FGDWater_gallon]                 REAL     NOT NULL,
    [FlueGasSorbent_ton]              REAL     NOT NULL,
    [SaleableFlyAsh_tonhr]            REAL     NOT NULL,
    [NonSaleableFlyAsh_tonhr]         REAL     NOT NULL,
    [FGDWater_gallonhr]               REAL     NOT NULL,
    [FlueGasSorbent_tonhr]            REAL     NOT NULL,
    [FGDDBAAdditive_gallon]           REAL     NOT NULL,
    [FGDDBAAdditive_gallonhr]         REAL     NOT NULL,
    [CFBBedReagent_ton]               REAL     NOT NULL,
    [CFBBedReagent_tonhr]             REAL     NOT NULL,
    [NH3Feedstock_ton]                REAL     NOT NULL,
    [NH3Feedstock_tonhr]              REAL     NOT NULL,
    [OMFixedCost_day]                 REAL     NOT NULL,
    [OMVarCost_mwh]                   REAL     NOT NULL,
    [Hour]                            TINYINT  NOT NULL,
    [DeratedEnergy_mwh]               REAL     DEFAULT ((0)) NOT NULL,
    [SaleableBottomAsh_tonhr]         REAL     DEFAULT ((0)) NULL,
    [NonSaleableBottomAsh_tonhr]      REAL     DEFAULT ((0)) NULL,
    [Demand_mwh]                      REAL     CONSTRAINT [DF_tScenarioUnitPerformance_Demand_mwh] DEFAULT ((0)) NOT NULL,
    [CalcGypsymSale_ton]              REAL     NULL,
    [CalcScrubberWaste_ton]           REAL     NULL,
    [CalcFlyAshSale_ton]              REAL     NULL,
    [CalcFlyAshDisposal_ton]          REAL     NULL,
    [CalcBottomAshSale_ton]           REAL     NULL,
    [CalcBottomAshDisposal_ton]       REAL     NULL,
    [CalcFGDAsh_ton]                  REAL     NULL,
    [HCL_ton]                         REAL     NULL,
    [MaxVistaNetPower_mw]             REAL     NULL,
    [LostEnergyFromNegMargin_mw]      REAL     NULL,
    [LostEnergyFromFuelDerate_mw]     REAL     NULL,
    [LostEnergyFromUnavailability_mw] REAL     NULL,
    CONSTRAINT [PK_tScenarioUnitPerformance] PRIMARY KEY CLUSTERED ([ScenarioUnitPerformanceID] ASC),
    CONSTRAINT [FK_tScenarioUnitPerformance_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tScenarioUnitPerformance_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tScenarioUnitPerformance_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioUnitPerformance_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tScenarioUnitPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);
















GO
/*
These indices accelerate the Forward Fuel performance data table construction process,
shaving off more than 80% of the query time in some situations.  And that's not the 
only place where these indices could help - the tables below get used in joins on the
CQIResultID field a lot, and these indices will accelerate all of those joins.
*/



GO
CREATE NONCLUSTERED INDEX IDX_tScenarioUnitPerformance_Helper2
ON [Projection].[tScenarioUnitPerformance] ([ScenarioID],[UnitID],[Year],[Month],[Day],[Hour])
GO
/****** Object:  Index [IDX_tScenarioUnitPerformance_Helper3]    Script Date: 06/20/2013 12:00:18 ******/
CREATE NONCLUSTERED INDEX [IDX_tScenarioUnitPerformance_Helper3] ON [Projection].[tScenarioUnitPerformance] 
(
	[ScenarioID], [FuelSourceID], [FuelQualityID], [UnitConfigID], [UnitID], [Year],[Month],[Day],[Hour]
)	
INCLUDE
(
[GrossGeneration_mwh]
      ,[NetGeneration_mwh]
      ,[NetPlantHeatRate_btukwh]
      ,[FuelBurnRate_tonhr]
      ,[FuelBurnRate_mbtuhr]
      ,[SO2_ton]
      ,[NOx_ton]
      ,[Hg_lbm]
      ,[CO2_ton]
      ,[SO2Rate_lbmmbtu]
      ,[NOxRate_lbmmbtu]
      ,[CO2Rate_lbmmbtu]
      ,[HgRate_lbmtbtu]
      ,[Particulate_lbmmbtu]
      ,[H2SO4Rate_lbmmbtu]
      ,[BottomAsh_tonhr]
      ,[FlyAsh_tonhr]
      ,[Gypsum_tonhr]
      ,[FGDAdditive_tonhr]
      ,[FGDFixative_tonhr]
      ,[FGDWaste_tonhr]
      ,[DryFGDWaste_tonhr]
      ,[FurnaceSorbent_tonhr]
      ,[SO3Feedstock_tonhr]
      ,[CoalAdditive_tonhr]
      ,[SCRReagent_tonhr]
      ,[SNCRReagent_tonhr]
      ,[Particulate_ton]
      ,[H2SO4Rate_ton]
      ,[BottomAsh_ton]
      ,[FlyAsh_ton]
      ,[Gypsum_ton]
      ,[FGDAdditive_ton]
      ,[FGDFixative_ton]
      ,[FGDWaste_ton]
      ,[DryFGDWaste_ton]
      ,[FurnaceSorbent_ton]
      ,[SO3Feedstock_ton]
      ,[CoalAdditive_ton]
      ,[SCRReagent_ton]
      ,[SNCRReagent_ton]
      ,[MaxGrossPower_mw]
      ,[FuelBurn_ton]
      ,[FuelBurn_mbtu]
      ,[EAF_percent]
      ,[EFOR_hour]     
      ,[GrossLoad_mw]
      ,[NetLoad_mw]
      ,[AuxLoad_mw]      
      ,[CORate_lbmmbtu]
      ,[CO2Rate_tonhr]
      ,[SO2InletRate_lbmmbtu]
      ,[NOxInletRate_lbmmbtu]
      ,[Opacity_percent]
      ,[FGDRemovalEfficiency_percent]
      ,[SCRRemovalEfficiency_percent]
      ,[HgAdditive_tonhr]
      ,[HgAdditive_ton]
      ,[CO_lbm]
      ,[VOC_lbm]
      ,[PM_lbm]
      ,[VOCRate_lbmmbtu]
      ,[PMRate_lbmmbtu]
      ,[SaleableFlyAsh_ton]
      ,[NonSaleableFlyAsh_ton]
      ,[FGDWater_gallon]
      ,[FlueGasSorbent_ton]
      ,[SaleableFlyAsh_tonhr]
      ,[NonSaleableFlyAsh_tonhr]
      ,[FGDWater_gallonhr]
      ,[FlueGasSorbent_tonhr]
      ,[FGDDBAAdditive_gallon]
      ,[FGDDBAAdditive_gallonhr]
      ,[CFBBedReagent_ton]
      ,[CFBBedReagent_tonhr]
      ,[NH3Feedstock_ton]
      ,[NH3Feedstock_tonhr]
      ,[OMFixedCost_day]
      ,[OMVarCost_mwh]      
      ,[DeratedEnergy_mwh]
      ,[SaleableBottomAsh_tonhr]
      ,[NonSaleableBottomAsh_tonhr]
      ,[Demand_mwh]
      ,[CalcGypsymSale_ton]
      ,[CalcScrubberWaste_ton]
      ,[CalcFlyAshSale_ton]
      ,[CalcFlyAshDisposal_ton]
      ,[CalcBottomAshSale_ton]
      ,[CalcBottomAshDisposal_ton]
      ,[CalcFGDAsh_ton]
      ,[HCL_ton]
)
GO



GO
CREATE NONCLUSTERED INDEX IDX_tScenarioUnitPerformance_Hour
ON [Projection].[tScenarioUnitPerformance] ([Hour])
INCLUDE ([Year],[Month],[Day])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioUnitPerformance';


GO
GRANT ALTER
    ON OBJECT::[Projection].[tScenarioUnitPerformance] TO [TEUser]
    AS [dbo];


GO
--CREATE NONCLUSTERED INDEX [IDX_tScenarioUnitPerformance_UnitConfigID] ON [Projection].[tScenarioUnitPerformance] 
--(
--      [UnitConfigID] ASC
--)
GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioUnitPerformance_RollingEmissions]
    ON [Projection].[tScenarioUnitPerformance]([ScenarioID] ASC, [UnitID] ASC)
    INCLUDE([FuelQualityID], [Year], [Month], [Day], [GrossGeneration_mwh], [NetGeneration_mwh], [SO2_ton], [Hg_lbm], [SO2Rate_lbmmbtu], [HgRate_lbmtbtu], [Particulate_lbmmbtu], [FuelBurn_mbtu], [UnitConfigID], [Hour], [HCL_ton], [CORate_lbmmbtu], [NOxRate_lbmmbtu]);

