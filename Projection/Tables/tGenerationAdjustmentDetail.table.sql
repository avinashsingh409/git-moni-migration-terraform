CREATE TABLE [Projection].[tGenerationAdjustmentDetail] (
    [GenerationAdjustmentDetailID] INT      IDENTITY (1, 1) NOT NULL,
    [GenerationAdjustmentSetID]    INT      NOT NULL,
    [UnitID]                       INT      NOT NULL,
    [Year]                         SMALLINT NOT NULL,
    [Month]                        TINYINT  NOT NULL,
    [Day]                          TINYINT  NOT NULL,
    [Hour]                         TINYINT  NOT NULL,
    [Adjustment_mw]                REAL     NOT NULL,
    CONSTRAINT [PK_tGenerationAdjustmentDetail] PRIMARY KEY CLUSTERED ([GenerationAdjustmentDetailID] ASC),
    CONSTRAINT [FK_tGenerationAdjustmentDetail_GenerationAdjustmentSetID_tGenerationAdjustmentSet] FOREIGN KEY ([GenerationAdjustmentSetID]) REFERENCES [Projection].[tGenerationAdjustmentSet] ([GenerationAdjustmentSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationAdjustmentDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_ProjectiontGenerationAdjustmentDetail_GenerationAdjustmentSetID_UnitID_Year_Month_Day_Hour] UNIQUE NONCLUSTERED ([GenerationAdjustmentSetID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Hourly adjustment overlaid on a generation set to provide a final generation plan.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationAdjustmentDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationAdjustmentDetail';

