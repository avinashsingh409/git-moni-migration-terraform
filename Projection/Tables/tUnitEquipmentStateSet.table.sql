CREATE TABLE [Projection].[tUnitEquipmentStateSet] (
	[UnitEquipmentStateSetID]   INT            IDENTITY (1, 1) NOT NULL,
	[UnitEquipmentStateSetDesc] NVARCHAR (255) NOT NULL,
	[CreatedBy]                 NVARCHAR (255) NOT NULL,
	[ChangedBy]                 NVARCHAR (255) NOT NULL,
	[CreateDate]                DATETIME       DEFAULT (getdate()) NOT NULL,
	[ChangeDate]                DATETIME       DEFAULT (getdate()) NOT NULL,
	[IsArchived]                BIT            CONSTRAINT [DF_tUnitEquipmentStateSet_IsArchived] DEFAULT ((0)) NOT NULL,
	[OwningAssetID]             INT            NULL,
	[BuiltFromAdjustment]		BIT			   Default ((0)) not null
	CONSTRAINT [PK_tUnitEquipmentStateSet] PRIMARY KEY CLUSTERED ([UnitEquipmentStateSetID] ASC),
	CONSTRAINT [FK_Projection_tUnitEquipmentStateSet_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of unit performance overwrites.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitEquipmentStateSet';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitEquipmentStateSet';

