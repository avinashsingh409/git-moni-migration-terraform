﻿CREATE TABLE [Projection].[tVistaModel] (
    [VistaModelID]   INT            IDENTITY (1, 1) NOT NULL,
    [VistaModelDesc] NVARCHAR (255) NOT NULL,
    [UnitModelID]    BIGINT         NOT NULL,
    [MASetID]        BIGINT         NOT NULL,
    [IsArchived]     BIT            CONSTRAINT [DF_tVistaModel_IsArchived] DEFAULT ((0)) NOT NULL,
    [OwningAssetID]  INT            NULL,
    PRIMARY KEY CLUSTERED ([VistaModelID] ASC),
    CONSTRAINT [FK_Projection_tVistaModel_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tVistaModel';

