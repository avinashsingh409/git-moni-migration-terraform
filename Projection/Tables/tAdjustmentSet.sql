﻿CREATE TABLE [Projection].[tAdjustmentSet] (
    [AdjustmentSetID]     INT            IDENTITY (1, 1) NOT NULL,
    [OwningScenarioID]    INT            NULL,
    [AdjustmentSetDesc]   NVARCHAR (50)  NOT NULL,
    [AdjustmentSetAbbrev] NVARCHAR (50)  NOT NULL,
    [CreateDate]          DATETIME       CONSTRAINT [DF_tAdjustmentSet_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]          DATETIME       CONSTRAINT [DF_tAdjustmentSet_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           INT            NOT NULL,
    [ChangedBy]           INT            NOT NULL,
    [StartDate]           DATETIME       NOT NULL,
    [EndDate]             DATETIME       NOT NULL,
    [Note]                NVARCHAR (MAX) NULL,
    [OwningAssetID] INT NULL, 
    CONSTRAINT [PK_tAdjustmentSet] PRIMARY KEY CLUSTERED ([AdjustmentSetID] ASC),
    CONSTRAINT [FK_tAdjustmentSet_tScenario] FOREIGN KEY ([OwningScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]),
	CONSTRAINT [FK_tAdjustmentSet_tAsset] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tAdjustmentSet_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tAdjustmentSet_tUser1] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);

GO
