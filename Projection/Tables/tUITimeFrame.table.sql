CREATE TABLE [Projection].[tUITimeFrame] (
    [UITimeFrameID]     INT            NOT NULL,
    [UITimeFrameDesc]   NVARCHAR (255) NOT NULL,
    [UITimeFrameAbbrev] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tUITimeFrame] PRIMARY KEY CLUSTERED ([UITimeFrameID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table for the EOY, YTD, MTD etc... time frames for the UI meta data tables.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITimeFrame';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITimeFrame';

