﻿CREATE TABLE [Projection].[tScenarioKillRequeues] (
    [KillRequestID] INT NOT NULL,
    [ScenarioID]    INT NOT NULL,
    CONSTRAINT [FK_tScenarioKillRequeues_tScenario_ScenarioID] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]),
    CONSTRAINT [FK_tScenarioKillRequeues_tScenarioKill_KillRequestID] FOREIGN KEY ([KillRequestID]) REFERENCES [Projection].[tScenarioKills] ([KillRequestID])
);

