CREATE TABLE [Projection].[tGenerationSetRoleMap] (
    [GenerationSetID] INT NOT NULL,
    [SecurityRoleID]  INT NOT NULL,
    CONSTRAINT [PK_tGenerationSetRoleMap] PRIMARY KEY CLUSTERED ([GenerationSetID] ASC, [SecurityRoleID] ASC),
    CONSTRAINT [FK_tGenerationSetRoleMap_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationSetRoleMap_SecurityRoleID_tSecurityRole] FOREIGN KEY ([SecurityRoleID]) REFERENCES [AccessControl].[tRole] ([SecurityRoleID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationSetRoleMap';

