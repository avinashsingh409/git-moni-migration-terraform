﻿CREATE TABLE [Projection].[tScenarioFuelQuality] (
    [ScenarioFuelQualityID]                  INT      IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                             INT      NOT NULL,
    [FuelQualityID]                          INT      NOT NULL,
    [UnitID]                                 INT      NOT NULL,
    [Year]                                   SMALLINT NOT NULL,
    [Month]                                  TINYINT  NOT NULL,
    [Day]                                    TINYINT  NOT NULL,
    [ProximateBasis]                         TINYINT  CONSTRAINT [DF_tScenarioFuelQualityIsProximate] DEFAULT ((1)) NOT NULL,
    [HHV_btulbm]                             REAL     NULL,
    [Moisture_percent]                       REAL     NULL,
    [InherentMoisture_percent]               REAL     NULL,
    [Ash_percent]                            REAL     NULL,
    [Sulfur_percent]                         REAL     NULL,
    [VolatileMatter_percent]                 REAL     NULL,
    [FixedCarbon_percent]                    REAL     NULL,
    [UltimateBasis]                          TINYINT  CONSTRAINT [DF_tScenarioFuelQualityIsUltimate] DEFAULT ((1)) NOT NULL,
    [Carbon_percent]                         REAL     NULL,
    [Hydrogen_percent]                       REAL     NULL,
    [Nitrogen_percent]                       REAL     NULL,
    [Chlorine_percent]                       REAL     NULL,
    [Oxygen_percent]                         REAL     NULL,
    [SIO2_percent]                           REAL     NULL,
    [AL2O3_percent]                          REAL     NULL,
    [TIO2_percent]                           REAL     NULL,
    [Fe2O3_percent]                          REAL     NULL,
    [CAO_percent]                            REAL     NULL,
    [MGO_percent]                            REAL     NULL,
    [K2O_percent]                            REAL     NULL,
    [NA2O_percent]                           REAL     NULL,
    [SO3_percent]                            REAL     NULL,
    [P2O5_percent]                           REAL     NULL,
    [SRO_percent]                            REAL     NULL,
    [BAO_percent]                            REAL     NULL,
    [MN3O4_percent]                          REAL     NULL,
    [AshUndetermined_percent]                REAL     NULL,
    [AshFusionReducingInitialDeformation_f]  REAL     NULL,
    [AshFusionReducingSoftening_f]           REAL     NULL,
    [AshFusionReducingHemispherical_f]       REAL     NULL,
    [AshFusionReducingFluid_f]               REAL     NULL,
    [AshFusionOxidizingInitialDeformation_f] REAL     NULL,
    [AshFusionOxidizingSoftening_f]          REAL     NULL,
    [AshFusionOxidizingHemispherical_f]      REAL     NULL,
    [AshFusionOxidizingFluid_f]              REAL     NULL,
    [T250_f]                                 REAL     NULL,
    [HGI]                                    REAL     NULL,
    [BaseToAcidRatio_fraction]               REAL     NULL,
    [EquilibriumMoisture_percent]            REAL     NULL,
    [SulfurFormPyritic_percent]              REAL     NULL,
    [SulfurFormSulfate_percent]              REAL     NULL,
    [SulfurFormOrganic_percent]              REAL     NULL,
    [Mercury_ppm]                            REAL     NULL,
    [UnitConfigID]                           INT      NOT NULL,
    [FuelSourceID]                           INT      NOT NULL,
    [Hour]                                   TINYINT  NOT NULL,
    CONSTRAINT [PK_tScenarioFuelQuality] PRIMARY KEY CLUSTERED ([ScenarioFuelQualityID] ASC),
    CONSTRAINT [FK_tScenarioFuelQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tScenarioFuelQuality_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tScenarioFuelQuality_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioFuelQuality_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tScenarioFuelQuality_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tScenarioFuelQuality_ScenarioID_FuelQualityID_FuelSourceID_UnitConfigID_UnitID_Year_Month_Day_Hour] UNIQUE NONCLUSTERED ([ScenarioID] ASC, [FuelQualityID] ASC, [FuelSourceID] ASC, [UnitConfigID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);










GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioFuelQuality_Helper]
    ON [Projection].[tScenarioFuelQuality]([ScenarioID] ASC, [Year] ASC, [Month] ASC, [Day] ASC)
    INCLUDE([FuelQualityID], [UnitID], [HHV_btulbm], [Sulfur_percent], [Hour]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioFuelQuality';


GO
GRANT ALTER
    ON OBJECT::[Projection].[tScenarioFuelQuality] TO [TEUser]
    AS [dbo];

