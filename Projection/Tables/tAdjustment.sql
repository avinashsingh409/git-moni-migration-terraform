﻿CREATE TABLE [Projection].[tAdjustment] (
    [AdjustmentID]                  INT      IDENTITY (1, 1) NOT NULL,
    [InputTypeInputOptionTypeMapID] INT      NOT NULL,
    [AdjustmentSetID]               INT      NOT NULL,
    [AssetID]                       INT      NOT NULL,
    [CreateDate]                    DATETIME CONSTRAINT [DF_tAdjustment_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                    DATETIME CONSTRAINT [DF_tAdjustment_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                     INT      NOT NULL,
    [ChangedBy]                     INT      NOT NULL,
    CONSTRAINT [PK_tAdjustment] PRIMARY KEY CLUSTERED ([AdjustmentID] ASC),
    CONSTRAINT [FK_tAdjustment_tAdjustmentSet] FOREIGN KEY ([AdjustmentSetID]) REFERENCES [Projection].[tAdjustmentSet] ([AdjustmentSetID]),
    CONSTRAINT [FK_tAdjustment_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tAdjustment_tInputTypeInputOptionTypeMap] FOREIGN KEY ([InputTypeInputOptionTypeMapID]) REFERENCES [Projection].[tInputTypeInputOptionTypeMap] ([InputTypeInputOptionTypeMapID]),
    CONSTRAINT [FK_tAdjustment_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tAdjustment_tUser1] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);

