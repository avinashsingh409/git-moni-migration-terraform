CREATE TABLE [Projection].[tUITargetType] (
    [UITargetTypeID]     INT            NOT NULL,
    [UITargetTypeDesc]   NVARCHAR (255) NOT NULL,
    [UITargetTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_UITargetType] PRIMARY KEY CLUSTERED ([UITargetTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table for the type of emission limit (Annual, rolling average etc...)', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetType';

