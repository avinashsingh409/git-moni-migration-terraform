CREATE TABLE [Projection].[tDataVersion] (
    [DataVersionID]   INT            NOT NULL,
    [DataVersionDesc] NVARCHAR (255) NOT NULL,
    [PlanTypeID]      INT            NOT NULL,
    [PlanPeriod]      TINYINT        NOT NULL,
    CONSTRAINT [PK_tDataVersion] PRIMARY KEY CLUSTERED ([DataVersionID] ASC),
    CONSTRAINT [FK_tDataVersion_PlanTypeID_tPlanType] FOREIGN KEY ([PlanTypeID]) REFERENCES [Projection].[tPlanType] ([PlanTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reusable information that describes an input data set.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tDataVersion';


GO



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tDataVersion';

