﻿/* Create Projection.tWeatherAsset; create FK relationship to tAsset; register with type table */
CREATE TABLE [Projection].[tWeatherAsset] (
    [WeatherAssetID]     INT            IDENTITY (1, 1) NOT NULL,
    [WeatherAssetDesc]   NVARCHAR (255) NOT NULL,
    [WeatherAssetAbbrev] NVARCHAR (55)  NOT NULL,
    [AssetID]            INT            NOT NULL,
    [ADDSStationID]      NVARCHAR (255) NULL,
    [Enabled]            BIT            DEFAULT ((0)) NOT NULL,
    [LastCollectionTime] DATETIME       NULL,
    [CreatedBy]          NVARCHAR (255) NOT NULL,
    [ChangedBy]          NVARCHAR (255) NOT NULL,
    [CreateDate]         DATETIME       NOT NULL,
    [ChangeDate]         DATETIME       NOT NULL,
    CONSTRAINT [PK_tWeatherAssetID] PRIMARY KEY CLUSTERED ([WeatherAssetID] ASC),
    CONSTRAINT [FK_tWeatherAsset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);


GO

GO


GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Projection', @level1type=N'TABLE',@level1name=N'tWeatherAsset'