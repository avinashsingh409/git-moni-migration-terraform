﻿CREATE TABLE [Projection].[tInputTypeDropDownOption] (
    [InputTypeDropDownOptionID]     INT           IDENTITY (1, 1) NOT NULL,
    [InputTypeInputOptionTypeMapID] INT           NOT NULL,
    [DropDownValue]                 FLOAT (53)    NULL,
    [DropDownDesc]                  NVARCHAR (50) NULL,
    CONSTRAINT [FK_tInputTypeDropDownOption_InputTypeInputOptionTypeMapID_tInputTypeInputOptionTypeMap] FOREIGN KEY ([InputTypeInputOptionTypeMapID]) REFERENCES [Projection].[tInputTypeInputOptionTypeMap] ([InputTypeInputOptionTypeMapID]),
    CONSTRAINT [UK_tInputTypeDropDownOption_InputTypeInputOptionTypeMapID_DropDownDesc] UNIQUE NONCLUSTERED ([InputTypeInputOptionTypeMapID] ASC, [DropDownDesc] ASC),
    CONSTRAINT [UK_tInputTypeDropDownOption_InputTypeInputOptionTypeMapID_DropDownValue] UNIQUE NONCLUSTERED ([InputTypeInputOptionTypeMapID] ASC, [DropDownValue] ASC)
);



