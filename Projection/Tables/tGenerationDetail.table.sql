CREATE TABLE [Projection].[tGenerationDetail] (
    [GenerationDetailID] INT      IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]    INT      NOT NULL,
    [UnitID]             INT      NOT NULL,
    [Year]               SMALLINT NOT NULL,
    [Month]              TINYINT  NOT NULL,
    [Day]                TINYINT  NOT NULL,
    [Hour]               TINYINT  NOT NULL,
    [Generation_mw]      REAL     NOT NULL,
    [MinGeneration_mw]   REAL     NOT NULL,
    [MaxGeneration_mw]   REAL     NOT NULL,
    [OfflineAllowed]     BIT      CONSTRAINT [DF__tGenerati__Offli__1387E197] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tGenerationDetail] PRIMARY KEY NONCLUSTERED ([GenerationDetailID] ASC),
    CONSTRAINT [FK_tGenerationDetail_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tGenerationDetail_GenerationSetID_UnitID_Year_Month_Day_Hour] UNIQUE CLUSTERED ([GenerationSetID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Hourly generation data by unit.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationDetail';

