﻿CREATE TABLE [Projection].[tCQIResultScrubberPerformance] (
    [CQIResultScrubberPerformanceID]      INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                         INT  NOT NULL,
    [RemEffActual_pct]                    REAL NOT NULL,
    [RemEffReq_pct]                       REAL NOT NULL,
    [MillThroughputReq_tonhr]             REAL NOT NULL,
    [MillThroughputAvail_tonhr]           REAL NOT NULL,
    [FeedPumpThroughputReq_gpm]           REAL NOT NULL,
    [FeedPumpThroughputAvail_gpm]         REAL NOT NULL,
    [SprayPumpThroughputReq_gpm]          REAL NOT NULL,
    [SprayPumpThroughputAvail_gpm]        REAL NOT NULL,
    [PrimaryDewaterThroughputReq_tonhr]   REAL NOT NULL,
    [PrimaryDewaterThroughputAvail_tonhr] REAL NOT NULL,
    [BlowdownPumpThroughputReq_gpm]       REAL NOT NULL,
    [BlowdownPumpThroughputAvail_gpm]     REAL NOT NULL,
    [UnderflowPumpThroughputReq_gpm]      REAL NOT NULL,
    [UnderflowPumpThroughputAvail_gpm]    REAL NOT NULL,
    [OverflowPumpThroughputReq_gpm]       REAL NOT NULL,
    [OverflowPumpThroughputAvail_gpm]     REAL NOT NULL,
    [SorbentBlowerThroughputReq_tonhr]    REAL NOT NULL,
    [SorbentBlowerThroughputAvail_tonhr]  REAL NOT NULL,
    [RecycleSolidsThroughputReq_tonhr]    REAL NOT NULL,
    [RecycleSolidsThroughputAvail_tonhr]  REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultScrubberPerformance] PRIMARY KEY CLUSTERED ([CQIResultScrubberPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultScrubberPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultScrubberPerformance';

