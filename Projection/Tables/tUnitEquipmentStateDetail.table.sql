CREATE TABLE [Projection].[tUnitEquipmentStateDetail] (
    [UnitEquipmentStateDetailID] INT           IDENTITY (1, 1) NOT NULL,
    [UnitEquipmentStateSetID]    INT           NOT NULL,
    [UnitEquipmentStateTypeID]   INT           NOT NULL,
    [Magnitude]                  FLOAT (53)    NOT NULL,
    [Units]                      NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_tUnitEquipmentStateDetail] PRIMARY KEY CLUSTERED ([UnitEquipmentStateDetailID] ASC),
    CONSTRAINT [FK_tUnitEquipmentStateDetail_UnitEquipmentStateSetID_tUnitEquipmentStateSet] FOREIGN KEY ([UnitEquipmentStateSetID]) REFERENCES [Projection].[tUnitEquipmentStateSet] ([UnitEquipmentStateSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitEquipmentStateDetail_UnitEquipmentStateTypeID_tUnitEquipmentStateType] FOREIGN KEY ([UnitEquipmentStateTypeID]) REFERENCES [Projection].[tUnitEquipmentStateType] ([UnitEquipmentStateTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Set specific unit performance overwrites.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitEquipmentStateDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitEquipmentStateDetail';

