CREATE TABLE [Projection].[tGenerationAdjustmentSet] (
    [GenerationAdjustmentSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [GenerationAdjustmentSetDesc] NVARCHAR (255) NOT NULL,
    [DataVersionID]               INT            NULL,
    [PlanYear]                    SMALLINT       CONSTRAINT [DF__tGeneratio__Year__1B29035F] DEFAULT (datepart(year,getdate())) NOT NULL,
    [IsConsensus]                 BIT            CONSTRAINT [DF__tGenerati__IsCon__1C1D2798] DEFAULT ((0)) NOT NULL,
    [IsSensitivity]               BIT            CONSTRAINT [DF_tGenerationAdjustmentSet_IsSensitivty] DEFAULT ((0)) NOT NULL,
    [CreatedBy]                   NVARCHAR (255) NOT NULL,
    [ChangedBy]                   NVARCHAR (255) NOT NULL,
    [CreateDate]                  DATETIME       CONSTRAINT [DF__tGenerati__Creat__1D114BD1] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                  DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tGenerationAdjustmentSet] PRIMARY KEY CLUSTERED ([GenerationAdjustmentSetID] ASC),
    CONSTRAINT [FK_tGenerationAdjustmentSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of hourly adjustments to generation.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationAdjustmentSet';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationAdjustmentSet';

