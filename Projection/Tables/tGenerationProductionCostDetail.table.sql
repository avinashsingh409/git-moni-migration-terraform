CREATE TABLE [Projection].[tGenerationProductionCostDetail] (
    [GenerationProductionCostDetailID] INT      IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]                  INT      NOT NULL,
    [Year]                             SMALLINT NOT NULL,
    [Month]                            TINYINT  NOT NULL,
    [Day]                              TINYINT  NOT NULL,
    [Hour]                             TINYINT  NOT NULL,
    [FuelBurn_mbtu]                    REAL     NOT NULL,
    [SO2_ton]                          REAL     NOT NULL,
    [NOx_ton]                          REAL     NOT NULL,
    [CO2_ton]                          REAL     NOT NULL,
    [UnitID]                           INT      NOT NULL,
    CONSTRAINT [PK_tGenerationProductionCostDetail] PRIMARY KEY CLUSTERED ([GenerationProductionCostDetailID] ASC),
    CONSTRAINT [FK_tGenerationProductionCostDetail_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationProductionCostDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Hourly burn and emissions data by unit that corresponds with generation data assumptions.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationProductionCostDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationProductionCostDetail';

