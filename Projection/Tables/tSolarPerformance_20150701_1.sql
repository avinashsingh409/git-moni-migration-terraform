﻿CREATE TABLE [Projection].[tSolarPerformance_20150701] (
    [SolarPerformanceID]    INT                IDENTITY (1, 1) NOT NULL,
    [SolarPerformanceSetID] INT                NOT NULL,
    [AssetID]               INT                NOT NULL,
    [TimeStamp]             DATETIMEOFFSET (7) NULL,
    [TypicalPowerOutput]    REAL               NOT NULL,
    [TypicalPOAIrradiance]  REAL               NOT NULL
);

