CREATE TABLE [Projection].[tDataCommentDetail] (
    [DataCommentDetailID]   INT            IDENTITY (1, 1) NOT NULL,
    [DataCommentSetID]      INT            NOT NULL,
    [DataCommentDetailDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tDataComment] PRIMARY KEY CLUSTERED ([DataCommentDetailID] ASC),
    CONSTRAINT [FK_tDataCommentDetail_DataCommentSetID_tDataCommentSet] FOREIGN KEY ([DataCommentSetID]) REFERENCES [Projection].[tDataCommentSet] ([DataCommentSetID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User specified comments for input data sets.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tDataCommentDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tDataCommentDetail';

