CREATE TABLE [Projection].[tUITargetStationGroupDetail] (
    [UITargetStationGroupDetailID] INT           IDENTITY (1, 1) NOT NULL,
    [UITargetSetID]                INT           NOT NULL,
    [StationGroupID]               INT           NOT NULL,
    [UIItemID]                     INT           NOT NULL,
    [UITargetTypeID]               INT           NOT NULL,
    [StartDate]                    DATETIME      NOT NULL,
    [EndDate]                      DATETIME      NOT NULL,
    [Quantity]                     FLOAT (53)    NOT NULL,
    [QuantityUnit]                 NVARCHAR (20) CONSTRAINT [DF_tUITargetStationGroupDetail_QuantityUnit] DEFAULT (N'ton') NOT NULL,
    [Margin]                       FLOAT (53)    NOT NULL,
    [MarginUnit]                   NVARCHAR (20) CONSTRAINT [DF_tUITargetStationGroupDetail_MarginUnit] DEFAULT (N'ton') NOT NULL,
    [IsRegulatory]                 BIT           CONSTRAINT [DF__tEmission__IsReg__473C8FC7] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tUITargetStationGroupDetail] PRIMARY KEY CLUSTERED ([UITargetStationGroupDetailID] ASC),
    CONSTRAINT [FK_tUITargetStationGroupDetail_StationGroupID_tStationGroup] FOREIGN KEY ([StationGroupID]) REFERENCES [Asset].[tStationGroup] ([StationGroupID]),
    CONSTRAINT [FK_tUITargetStationGroupDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]),
    CONSTRAINT [FK_tUITargetStationGroupDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID]),
    CONSTRAINT [FK_tUITargetStationGroupDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'System level emission limits over time.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetStationGroupDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetStationGroupDetail';

