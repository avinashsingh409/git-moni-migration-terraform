﻿CREATE TABLE [Projection].[tScenarioMargin] (
    [ScenarioMarginID]                   INT      IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                         INT      NOT NULL,
    [FuelSourceID]                       INT      NOT NULL,
    [FuelQualityID]                      INT      NOT NULL,
    [UnitID]                             INT      NOT NULL,
    [UnitConfigID]                       INT      NOT NULL,
    [Year]                               SMALLINT NOT NULL,
    [Month]                              TINYINT  NOT NULL,
    [Day]                                TINYINT  NOT NULL,
    [SO2Margin_percent]                  REAL     NOT NULL,
    [NOxMargin_percent]                  REAL     NOT NULL,
    [HgMargin_percent]                   REAL     NOT NULL,
    [CO2Margin_percent]                  REAL     NOT NULL,
    [OpacityMargin_percent]              REAL     NOT NULL,
    [FDFanMargin_percent]                REAL     NOT NULL,
    [PAFanMargin_percent]                REAL     NOT NULL,
    [IDFanMargin_percent]                REAL     NOT NULL,
    [SBFanMargin_percent]                REAL     NOT NULL,
    [MillCapacityMargin_percent]         REAL     NOT NULL,
    [MillDryingMargin_percent]           REAL     NOT NULL,
    [SlaggingMargin_percent]             REAL     NOT NULL,
    [FoulingMargin_percent]              REAL     NOT NULL,
    [CycloneAcceptabilityMargin_percent] REAL     NOT NULL,
    [EAFMargin_percent]                  REAL     NOT NULL,
    [AshMargin_percent]                  REAL     NOT NULL,
    [GypsumMargin_percent]               REAL     NOT NULL,
    [Hour]                               TINYINT  NOT NULL,
    [StokerAcceptabilityMargin_percent]  REAL     CONSTRAINT [DF_tScenarioMargin_StokerAcceptabilityMargin_percent] DEFAULT ((0)) NOT NULL,
    [StokerMargin_percent]               REAL     CONSTRAINT [DF_tScenarioMargin_StokerMargin_percent] DEFAULT ((0)) NOT NULL,
    [CFBMargin_percent]                  REAL     CONSTRAINT [DF_tScenarioMargin_CFBMargin_percent] DEFAULT ((0)) NOT NULL,
    [SCRMargin_percent]                  REAL     CONSTRAINT [DF_tScenarioMargin_SCRMargin_percent] DEFAULT ((0)) NOT NULL,
    [ESPMargin_percent]                  REAL     CONSTRAINT [DF_tScenarioMargin_ESPMargin_percent] DEFAULT ((0)) NOT NULL,
    [FabricFilterMargin_percent]         REAL     CONSTRAINT [DF_tScenarioMargin_FabricFilterMargin_percent] DEFAULT ((0)) NOT NULL,
    [ScrubberMargin_percent]             REAL     CONSTRAINT [DF_tScenarioMargin_ScrubberMargin_percent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tScenarioMargin] PRIMARY KEY CLUSTERED ([ScenarioMarginID] ASC),
    CONSTRAINT [FK_tScenarioMargin_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tScenarioMargin_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tScenarioMargin_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioMargin_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tScenarioMargin_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tScenarioMargin_ScenarioID_FuelQualityID_FuelSourceID_UnitConfigID_UnitID_Year_Month_Day_Hour] UNIQUE NONCLUSTERED ([ScenarioID] ASC, [FuelQualityID] ASC, [FuelSourceID] ASC, [UnitConfigID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);












GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioMargin';


GO
GRANT ALTER
    ON OBJECT::[Projection].[tScenarioMargin] TO [TEUser]
    AS [dbo];

