﻿CREATE TABLE [Projection].[tInputTypeInputOptionTypeMap] (
    [InputTypeInputOptionTypeMapID] INT  NOT NULL,
    [InputTypeID]                   INT  NOT NULL,
    [InputOptionTypeID]             INT  NOT NULL,
    [MinValue]                      REAL NULL,
    [MaxValue]                      REAL NULL,
    [DefaultValue]                  REAL NULL,
    [MinIsInclusive]                BIT  CONSTRAINT [D_tInputTypeInputOptionTypeMap_MinIsInclusive] DEFAULT ((0)) NOT NULL,
    [MaxIsInclusive]                BIT  CONSTRAINT [D_tInputTypeInputOptionTypeMap_MaxIsInclusive] DEFAULT ((0)) NOT NULL,
    [UsesDropDown]                  BIT  CONSTRAINT [D_tInputTypeInputOptionTypeMap_UsesDropDown] DEFAULT ((0)) NOT NULL,
    [SpecialDropDownHandling]       BIT  CONSTRAINT [D_tInputTypeInputOptionTypeMap_SpecialDropDownHandling] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tInputTypeInputOptionTypeMap] PRIMARY KEY CLUSTERED ([InputTypeInputOptionTypeMapID] ASC),
    CONSTRAINT [FK_tInputTypeInputOptionTypeMap_tInputOptionType] FOREIGN KEY ([InputOptionTypeID]) REFERENCES [Projection].[tInputOptionType] ([InputOptionTypeID]),
    CONSTRAINT [FK_tInputTypeInputOptionTypeMap_tInputType] FOREIGN KEY ([InputTypeID]) REFERENCES [Projection].[tInputType] ([InputTypeID])
);





