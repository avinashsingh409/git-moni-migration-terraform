CREATE TABLE [Projection].[tUnitConfigScheduleDetail] (
    [UnitConfigScheduleDetailID] INT      IDENTITY (1, 1) NOT NULL,
    [UnitConfigScheduleSetID]    INT      NOT NULL,
    [UnitConfigID]               INT      NOT NULL,
    [EffectiveDate]              DATETIME NOT NULL,
    PRIMARY KEY CLUSTERED ([UnitConfigScheduleDetailID] ASC),
    CONSTRAINT [FK_tUnitConfigScheduleDetail_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tUnitConfigScheduleDetail_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Definition of the unit configurations over time.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitConfigScheduleDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitConfigScheduleDetail';

