﻿CREATE TABLE [Projection].[tScenarioObjectiveTypeInputTypeMap] (
    [ScenarioObjectiveTypeInputTypeMapID] INT NOT NULL,
    [ScenarioObjectiveTypeID]             INT NOT NULL,
    [InputTypeID]                         INT NOT NULL,
    [IsRequired]                          BIT NOT NULL,
    CONSTRAINT [PK_tScenarioObjectiveTypeInputTypeMap] PRIMARY KEY CLUSTERED ([ScenarioObjectiveTypeInputTypeMapID] ASC),
    CONSTRAINT [FK_tScenarioObjectiveTypeInputTypeMap_tInputType] FOREIGN KEY ([InputTypeID]) REFERENCES [Projection].[tInputType] ([InputTypeID]),
    CONSTRAINT [FK_tScenarioObjectiveTypeInputTypeMap_tScenarioObjectiveType] FOREIGN KEY ([ScenarioObjectiveTypeID]) REFERENCES [Projection].[tScenarioObjectiveType] ([ScenarioObjectiveTypeID])
);

