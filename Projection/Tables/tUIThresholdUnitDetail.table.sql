CREATE TABLE [Projection].[tUIThresholdUnitDetail] (
    [UIThresholdUnitDetailID]     INT        IDENTITY (1, 1) NOT NULL,
    [UITargetSetID]               INT        NOT NULL,
    [UnitID]                      INT        NOT NULL,
    [UIItemID]                    INT        NOT NULL,
    [UITargetTypeID]              INT        NOT NULL,
    [StartDate]                   DATETIME   NOT NULL,
    [EndDate]                     DATETIME   NOT NULL,
    [BadColorThreshold_fraction]  FLOAT (53) NOT NULL,
    [GoodColorThreshold_fraction] FLOAT (53) NOT NULL,
    CONSTRAINT [PK_tUIThresholdUnitDetail] PRIMARY KEY CLUSTERED ([UIThresholdUnitDetailID] ASC),
    CONSTRAINT [FK_tUIThresholdUnitDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]),
    CONSTRAINT [FK_tUIThresholdUnitDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID]),
    CONSTRAINT [FK_tUIThresholdUnitDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID]),
    CONSTRAINT [FK_tUIThresholdUnitDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUIThresholdUnitDetail';

