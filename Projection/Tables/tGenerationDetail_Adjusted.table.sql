CREATE TABLE [Projection].[tGenerationDetail_Adjusted] (
    [GenerationDetailID] INT      IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]    INT      NOT NULL,
    [UnitID]             INT      NOT NULL,
    [Year]               SMALLINT NOT NULL,
    [Month]              TINYINT  NOT NULL,
    [Day]                TINYINT  NOT NULL,
    [Hour]               TINYINT  NOT NULL,
    [Generation_mw]      REAL     NOT NULL,
    [MinGeneration_mw]   REAL     NOT NULL,
    [MaxGeneration_mw]   REAL     NOT NULL,
    [OfflineAllowed]     BIT      CONSTRAINT [DF__tGenerati_adj__Offli__1387E197] DEFAULT ((1)) NOT NULL,
    [EffectiveDate]      AS       (CONVERT([datetime],(CONVERT([varchar](4),[year],(0))+right('0'+CONVERT([varchar](2),[month],(0)),(2)))+right('0'+CONVERT([varchar](2),[day],(0)),(2)),(0))),
    [EffectiveTime]      AS       (CONVERT([datetime],((((CONVERT([varchar](4),[year],(0))+right('0'+CONVERT([varchar](2),[month],(0)),(2)))+right('0'+CONVERT([varchar](2),[day],(0)),(2)))+' ')+right('0'+CONVERT([varchar](2),[hour],(0)),(2)))+':00:00',(0))),
    CONSTRAINT [PK_tGenerationDetail_Adjusted] PRIMARY KEY NONCLUSTERED ([GenerationDetailID] ASC),
    CONSTRAINT [FK_tGenerationDetail_Adjusted_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationDetail_Adjusted_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tGenerationDetail_Adjusted_GenerationSetID_UnitID_Year_Month_Day_Hour] UNIQUE CLUSTERED ([GenerationSetID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);








GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hourly generation data by unit.' , @level0type=N'SCHEMA',@level0name=N'Projection', @level1type=N'TABLE',@level1name=N'tGenerationDetail_Adjusted'


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationDetail_Adjusted';

