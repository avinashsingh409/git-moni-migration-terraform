CREATE TABLE [Projection].[tPollutantType] (
    [PollutantTypeID]     INT            NOT NULL,
    [PollutantTypeDesc]   NVARCHAR (255) NOT NULL,
    [PollutantTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [DisplayOrder]        INT            NOT NULL,
    CONSTRAINT [PK_tPollutant] PRIMARY KEY CLUSTERED ([PollutantTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table providing different pollutant types.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tPollutantType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tPollutantType';

