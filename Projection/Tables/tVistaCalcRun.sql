CREATE TABLE [Projection].[tVistaCalcRun] (
    [VISTACalcRunID]  INT            IDENTITY (1, 1) NOT NULL,
    [EvaluationID]    BIGINT         NOT NULL,
    [BoilerOption]    INT            NOT NULL,
    [ConstantPower]   INT            NOT NULL,
    [RunStatus]       TINYINT        NOT NULL,
    [StartTime]       DATETIME       NULL,
    [StopTime]        DATETIME       NULL,
    [ExecutingOnHost] NVARCHAR (MAX) NULL,
    [ErrorReport]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tVistaCalcRun] PRIMARY KEY CLUSTERED ([VISTACalcRunID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tVistaCalcRun';

