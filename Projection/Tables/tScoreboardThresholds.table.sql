CREATE TABLE [Projection].[tScoreboardThresholds] (
    [UIScoreBoardID]              INT        IDENTITY (1, 1) NOT NULL,
    [UIItemID]                    INT        NOT NULL,
    [UITimeFrameID]               INT        NOT NULL,
    [EditDate]                    DATETIME   NOT NULL,
    [PlanYear]                    SMALLINT   NOT NULL,
    [PlanTypeID]                  INT        NOT NULL,
    [PlanPeriod]                  TINYINT    NOT NULL,
    [BadColorThreshold_fraction]  FLOAT (53) NOT NULL,
    [GoodColorThreshold_fraction] FLOAT (53) NOT NULL,
    [UpArrowThreshold_fraction]   FLOAT (53) NOT NULL,
    [DownArrowThreshold_fraction] FLOAT (53) NOT NULL,
    [StartDate]                   DATETIME   NULL,
    [EndDate]                     DATETIME   NULL,
    CONSTRAINT [PK_tUIScoreBoard] PRIMARY KEY CLUSTERED ([UIScoreBoardID] ASC),
    CONSTRAINT [FK_tUIScoreBoard_PlanTypeID_tPlanType] FOREIGN KEY ([PlanTypeID]) REFERENCES [Projection].[tPlanType] ([PlanTypeID]),
    CONSTRAINT [FK_tUIScoreBoard_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]),
    CONSTRAINT [FK_tUIScoreBoard_UITimeFrameID_tUITimeFrame] FOREIGN KEY ([UITimeFrameID]) REFERENCES [Projection].[tUITimeFrame] ([UITimeFrameID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Meta data that controls the format of the scoreboards in the dashboard.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScoreboardThresholds';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All current margins less than or equal to this value will be displayed using the designated bad color.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScoreboardThresholds', @level2type = N'COLUMN', @level2name = N'BadColorThreshold_fraction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All current margins greater than or equal to this value will be displayed using the designated good color.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScoreboardThresholds', @level2type = N'COLUMN', @level2name = N'GoodColorThreshold_fraction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'If the difference between the current margin and the previous period margin is greater than or equal to this value then an up arrow will be dispalyed in the good color.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScoreboardThresholds', @level2type = N'COLUMN', @level2name = N'UpArrowThreshold_fraction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'If the difference between the current margin and the previous period margin is less than or equal to this value then a down arrow will be dispalyed in the bad color.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScoreboardThresholds', @level2type = N'COLUMN', @level2name = N'DownArrowThreshold_fraction';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScoreboardThresholds';

