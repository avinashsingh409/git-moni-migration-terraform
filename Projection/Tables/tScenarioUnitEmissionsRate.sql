﻿CREATE TABLE [Projection].[tScenarioUnitEmissionsRate] (
    [ScenarioUnitEmissionsRateID] INT            IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                  INT            NOT NULL,
    [UnitID]                      INT            NOT NULL,
    [UIItemID]                    INT            NOT NULL,
    [StartDate]                   DATETIME       NOT NULL,
    [Rate]                        REAL           NOT NULL,
    [Comment]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tScenarioUnitEmissionsRate] PRIMARY KEY CLUSTERED ([ScenarioUnitEmissionsRateID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioUnitEmissionsRate';

