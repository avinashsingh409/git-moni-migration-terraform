CREATE TABLE [Projection].[tScenarioResultStatus] (
    [ScenarioResultStatusID] INT      IDENTITY (1, 1) NOT NULL,
    [ScenarioID]             INT      NOT NULL,
    [FuelQualityID]          INT      NOT NULL,
    [UnitID]                 INT      NOT NULL,
    [Year]                   SMALLINT NOT NULL,
    [Month]                  TINYINT  NOT NULL,
    [Day]                    TINYINT  NOT NULL,
    [Status]                 BIT      NOT NULL,
    [UnitConfigID]           INT      NOT NULL,
    [FuelSourceID]           INT      NOT NULL,
    [Hour]                   TINYINT  NOT NULL,
    CONSTRAINT [PK_tScenarioResultStatus] PRIMARY KEY CLUSTERED ([ScenarioResultStatusID] ASC),
    CONSTRAINT [FK_tScenarioResultStatus_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tScenarioResultStatus_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tScenarioResultStatus_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioResultStatus_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tScenarioResultStatus_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tScenarioResultStatus_ScenarioID_FuelQualityID_FuelSourceID_UnitConfigID_UnitID_Year_Month_Day_Hour] UNIQUE NONCLUSTERED ([ScenarioID] ASC, [FuelQualityID] ASC, [FuelSourceID] ASC, [UnitConfigID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioResultStatus';

