﻿CREATE TABLE [Projection].[tUnitEquipmentStateType] (
    [UnitEquipmentStateTypeID]   INT            NOT NULL,
    [UnitEquipmentStateTypeDesc] NVARCHAR (255) NOT NULL,
    [DefaultUnits]               NVARCHAR (20)  NOT NULL,
    [IsAppliedToFuel]            BIT            CONSTRAINT [DF_Projection_tUnitEquipmentStateType_IsAppliedToFuel] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tUnitEquipmentStateType] PRIMARY KEY CLUSTERED ([UnitEquipmentStateTypeID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Complete list of available unit performance overwrites.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitEquipmentStateType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitEquipmentStateType';

