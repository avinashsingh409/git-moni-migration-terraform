﻿CREATE TABLE [Projection].[tInputTypeUnitEquipmentStateTypeMap] (
    [InputTypeUnitEquipmentStateTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [InputTypeID]                          INT NOT NULL,
    [UnitEquipmentStateTypeID]             INT NOT NULL,
    CONSTRAINT [PK_tInputTypeUnitEquipmentStateTypeMap] PRIMARY KEY CLUSTERED ([InputTypeUnitEquipmentStateTypeMapID] ASC),
    CONSTRAINT [FK_tInputTypeUnitEquipmentStateTypeMap_InputTypeID] FOREIGN KEY ([InputTypeID]) REFERENCES [Projection].[tInputType] ([InputTypeID]),
    CONSTRAINT [FK_tInputTypeUnitEquipmentStateTypeMap_UnitEquipmentStateTypeID] FOREIGN KEY ([UnitEquipmentStateTypeID]) REFERENCES [Projection].[tUnitEquipmentStateType] ([UnitEquipmentStateTypeID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Maps InputTypes to their corresponding UnitEquipmentStateTypes.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tInputTypeUnitEquipmentStateTypeMap';

