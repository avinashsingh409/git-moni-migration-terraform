CREATE TABLE [Projection].[tGenerationAdjustmentMap] (
    [GenerationAdjustmentMapID] INT IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]           INT NOT NULL,
    [GenerationAdjustmentSetID] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([GenerationAdjustmentMapID] ASC),
    CONSTRAINT [FK_tGenerationAdjustmentMap_GenerationAdjustmentSetID_tGenerationAdjustmentSet] FOREIGN KEY ([GenerationAdjustmentSetID]) REFERENCES [Projection].[tGenerationAdjustmentSet] ([GenerationAdjustmentSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationAdjustmentMap_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mapping of adjustment sets to a generation set.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationAdjustmentMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationAdjustmentMap';

