﻿CREATE TABLE [Projection].[tScenarioUnitCost] (
    [ScenarioUnitCostID]   INT      IDENTITY (1, 1) NOT NULL,
    [ScenarioID]           INT      NOT NULL,
    [UnitID]               INT      NOT NULL,
    [Year]                 SMALLINT NOT NULL,
    [Month]                TINYINT  NOT NULL,
    [Day]                  TINYINT  NOT NULL,
    [Hour]                 TINYINT  NULL,
    [Energy]               REAL     NULL,
    [DeratedEnergy]        REAL     NULL,
    [Demand]               REAL     NULL,
    [Fuel]                 REAL     NULL,
    [SaleableBottomAsh]    REAL     NULL,
    [NonSaleableBottomAsh] REAL     NULL,
    [SaleableFlyAsh]       REAL     NULL,
    [NonSaleableFlyAsh]    REAL     NULL,
    [Gypsum]               REAL     NULL,
    [FGDAdditive]          REAL     NULL,
    [FGDFixative]          REAL     NULL,
    [FGDWaste]             REAL     NULL,
    [FGDWater]             REAL     NULL,
    [FGDReagent]           REAL     NULL,
    [FGDAsh]               REAL     NULL,
    [DryFGDWaste]          REAL     NULL,
    [MercuryAdditive]      REAL     NULL,
    [FurnaceSorbent]       REAL     NULL,
    [SO3Feedstock]         REAL     NULL,
    [CoalAdditive]         REAL     NULL,
    [FlueGasSorbent]       REAL     NULL,
    [FGDDBAAdditive]       REAL     NULL,
    [CFBBedReagent]        REAL     NULL,
    [NH3Feedstock]         REAL     NULL,
    [SO2Allowance]         REAL     NULL,
    [NOxAllowance]         REAL     NULL,
    [HgAllowance]          REAL     NULL,
    [CO2Allowance]         REAL     NULL,
    [TotalRevenue]         AS       (isnull([Energy],(0))),
    [TotalCosts]           AS       ((((((((((((((((((((((((isnull([Fuel],(0))+isnull([SaleableBottomAsh],(0)))+isnull([NonSaleableBottomAsh],(0)))+isnull([SaleableFlyAsh],(0)))+isnull([NonSaleableFlyAsh],(0)))+isnull([Gypsum],(0)))+isnull([FGDAdditive],(0)))+isnull([FGDFixative],(0)))+isnull([FGDWaste],(0)))+isnull([FGDWater],(0)))+isnull([FGDReagent],(0)))+isnull([FGDAsh],(0)))+isnull([DryFGDWaste],(0)))+isnull([MercuryAdditive],(0)))+isnull([FurnaceSorbent],(0)))+isnull([SO3FeedStock],(0)))+isnull([CoalAdditive],(0)))+isnull([FlueGasSorbent],(0)))+isnull([FGDDBAAdditive],(0)))+isnull([CFBBedReagent],(0)))+isnull([NH3Feedstock],(0)))+isnull([So2Allowance],(0)))+isnull([NOxAllowance],(0)))+isnull([HgAllowance],(0)))+isnull([CO2Allowance],(0))),
    [GrossMargin]          AS       (isnull([Energy],(0))-((((((((((((((((((((((((isnull([Fuel],(0))+isnull([SaleableBottomAsh],(0)))+isnull([NonSaleableBottomAsh],(0)))+isnull([SaleableFlyAsh],(0)))+isnull([NonSaleableFlyAsh],(0)))+isnull([Gypsum],(0)))+isnull([FGDAdditive],(0)))+isnull([FGDFixative],(0)))+isnull([FGDWaste],(0)))+isnull([FGDWater],(0)))+isnull([FGDReagent],(0)))+isnull([FGDAsh],(0)))+isnull([DryFGDWaste],(0)))+isnull([MercuryAdditive],(0)))+isnull([FurnaceSorbent],(0)))+isnull([SO3FeedStock],(0)))+isnull([CoalAdditive],(0)))+isnull([FlueGasSorbent],(0)))+isnull([FGDDBAAdditive],(0)))+isnull([CFBBedReagent],(0)))+isnull([NH3Feedstock],(0)))+isnull([So2Allowance],(0)))+isnull([NOxAllowance],(0)))+isnull([HgAllowance],(0)))+isnull([CO2Allowance],(0)))),
    CONSTRAINT [PK_tScenarioUnitCost] PRIMARY KEY CLUSTERED ([ScenarioUnitCostID] ASC),
    CONSTRAINT [FK_tScenarioUnitCost_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioUnitCost_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tScenarioUnitCost_ScenarioID_UnitID_Year_Month_Day_Hour] UNIQUE NONCLUSTERED ([ScenarioID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioUnitCost] ON [Projection].[tScenarioUnitCost] 
(
	[ScenarioID],[UnitID], [Year],[Month],[Day],[Hour]
)	
INCLUDE
(
TotalRevenue, TotalCosts,GrossMargin
)
GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioUnitCost_AllCosts]
    ON [Projection].[tScenarioUnitCost]([ScenarioID] ASC)
    INCLUDE([UnitID], [Year], [Month], [Day], [Hour], [Energy], [Fuel], [SaleableBottomAsh], [NonSaleableBottomAsh], [SaleableFlyAsh], [NonSaleableFlyAsh], [Gypsum], [FGDAdditive], [FGDFixative], [FGDWaste], [FGDWater], [FGDReagent], [FGDAsh], [DryFGDWaste], [MercuryAdditive], [FurnaceSorbent], [SO3Feedstock], [CoalAdditive], [FlueGasSorbent], [FGDDBAAdditive], [CFBBedReagent], [NH3Feedstock], [SO2Allowance], [NOxAllowance], [HgAllowance], [CO2Allowance]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioUnitCost';


GO
GRANT ALTER
    ON OBJECT::[Projection].[tScenarioUnitCost] TO [TEUser]
    AS [dbo];

