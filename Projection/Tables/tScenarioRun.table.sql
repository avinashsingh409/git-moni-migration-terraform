﻿CREATE TABLE [Projection].[tScenarioRun] (
    [ScenarioRunID]      INT            IDENTITY (1, 1) NOT NULL,
    [ScenarioID]         INT            NOT NULL,
    [RunStatus]          TINYINT        NOT NULL,
    [StartTime]          DATETIME       NULL,
    [StopTime]           DATETIME       NULL,
    [ExecutingOnHost]    NVARCHAR (MAX) NULL,
    [AdHoc]              BIT            NULL,
    [ScenarioFeedback]   NVARCHAR (MAX) NULL,
    [VISTAFeedback]      NVARCHAR (MAX) NULL,
    [VISTAStatus]        TINYINT        DEFAULT ((0)) NOT NULL,
    [PowerRAMFeedback]   NVARCHAR (MAX) NULL,
    [PowerRAMStatus]     TINYINT        DEFAULT ((0)) NOT NULL,
    [RunPriority]        FLOAT (53)     DEFAULT ((0.0)) NOT NULL,
    [QueuedBySecID]      INT            DEFAULT (NULL) NULL,
    [QueuedDate]         DATETIME       DEFAULT (sysdatetime()) NOT NULL,
    [RunProgressPercent] FLOAT (53)     CONSTRAINT [DF_tScenarioRun_RunProgressPercent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tScenarioRun] PRIMARY KEY CLUSTERED ([ScenarioRunID] ASC),
    CONSTRAINT [FK_tScenarioRun_QueuedBySecID_tUser] FOREIGN KEY ([QueuedBySecID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE SET NULL,
    CONSTRAINT [FK_tScenarioRun_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);










GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioRun';

