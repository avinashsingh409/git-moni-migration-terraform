CREATE TABLE [Projection].[tScenarioResultStatusDetail] (
    [ScenarioResultStatusDetailID] INT             IDENTITY (1, 1) NOT NULL,
    [ScenarioResultStatusID]       INT             NOT NULL,
    [ScenarioResultStatusDesc]     NVARCHAR (2000) NOT NULL,
    CONSTRAINT [PK_tScenarioResultStatusDetail] PRIMARY KEY CLUSTERED ([ScenarioResultStatusDetailID] ASC),
    CONSTRAINT [FK_tScenarioResultStatusDetail_ScenarioResultStatusID_tScenarioResultStatus] FOREIGN KEY ([ScenarioResultStatusID]) REFERENCES [Projection].[tScenarioResultStatus] ([ScenarioResultStatusID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioResultStatusDetail';

