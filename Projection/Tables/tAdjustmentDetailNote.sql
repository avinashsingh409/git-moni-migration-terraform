﻿CREATE TABLE [Projection].[tAdjustmentDetailNote] (
    [AdjustmentDetailNotesID] INT            IDENTITY (1, 1) NOT NULL,
    [AdjustmentDetailID]      INT            NOT NULL,
    [Note]                    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tAdjustmentDetailNote] PRIMARY KEY CLUSTERED ([AdjustmentDetailNotesID] ASC),
    CONSTRAINT [FK_tAdjustmentDetailNote_tAdjustmentDetail] FOREIGN KEY ([AdjustmentDetailID]) REFERENCES [Projection].[tAdjustmentDetail] ([AdjustmentDetailID])
);

