CREATE TABLE [Projection].[tFuelPlanSetRoleMap] (
    [FuelPlanSetID]  INT NOT NULL,
    [SecurityRoleID] INT NOT NULL,
    CONSTRAINT [PK_tFuelPlanSetRoleMap] PRIMARY KEY CLUSTERED ([FuelPlanSetID] ASC, [SecurityRoleID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFuelPlanSetRoleMap';

