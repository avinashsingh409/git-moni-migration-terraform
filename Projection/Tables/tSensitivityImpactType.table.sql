CREATE TABLE [Projection].[tSensitivityImpactType] (
    [SensitivityImpactTypeID]     INT            NOT NULL,
    [SensitivityImpactTypeDesc]   NVARCHAR (255) NOT NULL,
    [SensitivityImpactTypeAbbrev] NVARCHAR (2)   NOT NULL,
    CONSTRAINT [PK_tSensitivityImpactType] PRIMARY KEY CLUSTERED ([SensitivityImpactTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table providing different sensitivity impact types.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tSensitivityImpactType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tSensitivityImpactType';

