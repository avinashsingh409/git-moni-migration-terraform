CREATE TABLE [Projection].[tUITargetStationDetail] (
    [UITargetStationDetailID] INT           IDENTITY (1, 1) NOT NULL,
    [UITargetSetID]           INT           NOT NULL,
    [StationID]               INT           NOT NULL,
    [UIItemID]                INT           NOT NULL,
    [UITargetTypeID]          INT           NOT NULL,
    [StartDate]               DATETIME      NOT NULL,
    [EndDate]                 DATETIME      NOT NULL,
    [Quantity]                FLOAT (53)    NOT NULL,
    [QuantityUnit]            NVARCHAR (20) CONSTRAINT [DF_tUITargetStationDetail_QuantityUnit] DEFAULT (N'ton') NOT NULL,
    [Margin]                  FLOAT (53)    NOT NULL,
    [MarginUnit]              NVARCHAR (20) CONSTRAINT [DF_tUITargetStationDetail_MarginUnit] DEFAULT (N'ton') NOT NULL,
    [IsRegulatory]            BIT           CONSTRAINT [DF_tUITargetStationDetail_IsRegulatory] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tUITargetStationDetail] PRIMARY KEY CLUSTERED ([UITargetStationDetailID] ASC),
    CONSTRAINT [FK_tUITargetStationDetail_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID]),
    CONSTRAINT [FK_tUITargetStationDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]),
    CONSTRAINT [FK_tUITargetStationDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID]),
    CONSTRAINT [FK_tUITargetStationDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'System level emission limits over time.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetStationDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetStationDetail';

