﻿CREATE TABLE [Projection].[tInputOptionType] (
    [InputOptionTypeID]     INT           NOT NULL,
    [InputOptionTypeAbbrev] NVARCHAR (50) NOT NULL,
    [InputOptionTypeDesc]   NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tInputOptionType] PRIMARY KEY CLUSTERED ([InputOptionTypeID] ASC)
);

