﻿CREATE TABLE [Projection].[tCQIResultFabricFilterPerformance] (
    [CQIResultFabricFilterPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                        INT  NOT NULL,
    [InletGasFlow_cfm]                   REAL DEFAULT ((0)) NOT NULL,
    [MaxGasFlow_cfm]                     REAL DEFAULT ((0)) NOT NULL,
    [InletAshLoad_tonhr]                 REAL DEFAULT ((0)) NOT NULL,
    [MaxAshLoad_tonhr]                   REAL DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCQIResultFabricFilterPerformance] PRIMARY KEY CLUSTERED ([CQIResultFabricFilterPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultFabricFilterPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX IDX_tCQIResultFabricFilterPerformance_CQIResultID
ON [Projection].[tCQIResultFabricFilterPerformance] ([CQIResultID])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultFabricFilterPerformance';

