﻿CREATE TABLE [Projection].[tUIItem] (
    [UIItemID]     INT            NOT NULL,
    [UIItemDesc]   NVARCHAR (255) NOT NULL,
    [UIItemAbbrev] NVARCHAR (255) NOT NULL,
    [DisplayOrder] INT            DEFAULT ((100)) NOT NULL,
    CONSTRAINT [PK_tUIItem] PRIMARY KEY CLUSTERED ([UIItemID] ASC)
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table for items used in the UI meta data tables.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUIItem';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUIItem';

