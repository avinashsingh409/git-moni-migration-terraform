CREATE TABLE [Projection].[tCQIRunType] (
    [CQIRunTypeID]   INT            NOT NULL,
    [CQIRunTypeDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tCQIRunType] PRIMARY KEY CLUSTERED ([CQIRunTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIRunType';

