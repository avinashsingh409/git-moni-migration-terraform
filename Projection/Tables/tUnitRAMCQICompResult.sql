--Create mapping table for storing monthly CQI factor
CREATE TABLE [PowerRAM].[tUnitRAMCQICompResult] (
    [UnitRAMCQICompResultID] INT           IDENTITY (1, 1) NOT NULL,
    [RAMModelID]             INT           NOT NULL,
    [SystemNum]              INT           NOT NULL,
    [ComponentID]            NVARCHAR (10) NOT NULL,
    [Year]                   INT           NOT NULL,
    [Month]                  INT           NOT NULL,
    [CQIFactor]              FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_tUnitRAMCQICompResult] PRIMARY KEY CLUSTERED ([UnitRAMCQICompResultID] ASC),
    CONSTRAINT [FK_tUnitRAMCQICompResult_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMCQICompResult';

