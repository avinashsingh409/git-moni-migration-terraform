﻿
CREATE TABLE [Projection].[tEmissionRollingData](
	[EmissionRollingDataID] [int] IDENTITY(1,1) NOT NULL,
	[ScenarioID] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[UIItemID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Quantity] float(53) NULL, --no date gaps are to be allowed, so we may need some null entries occasionally.
CONSTRAINT [PK_Scenario_tEmissionRollingData] PRIMARY KEY NONCLUSTERED 
(
	[EmissionRollingDataID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
,
CONSTRAINT [UK_Scenario_tEmissionRollingData_Scenario_Unit_UIItem_Date] UNIQUE CLUSTERED 
(
	[ScenarioID] ASC,
	[UnitID] ASC,
	[UIItemID] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,  IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
,
CONSTRAINT [FK_Scenario_tEmissionRollingData_tScenario] FOREIGN KEY([ScenarioID])
REFERENCES [Projection].[tScenario] ([ScenarioID])
ON DELETE CASCADE,
CONSTRAINT [FK_Scenario_tEmissionRollingData_tUnit] FOREIGN KEY([UnitID])
REFERENCES [Asset].[tUnit] ([UnitID])
ON DELETE CASCADE,
CONSTRAINT [FK_Scenario_tEmissionRollingData_tUIItem] FOREIGN KEY([UIItemID])
REFERENCES [Projection].[tUIItem] ([UIItemID])
ON DELETE CASCADE
 )