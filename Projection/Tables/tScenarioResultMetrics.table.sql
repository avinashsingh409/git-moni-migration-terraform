CREATE TABLE [Projection].[tScenarioResultMetrics] (
    [ID]                         INT            IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                 INT            NOT NULL,
    [AssetID]                    INT            NOT NULL,
    [Year]                       INT            NULL,
    [Quarter]                    INT            NULL,
    [Month]                      INT            NULL,
    [Day]                        INT            NULL,
    [OperatingHours]             INT            CONSTRAINT [DF_tScenarioResultMetrics_OperatingHours] DEFAULT ((0)) NOT NULL,
    [FuelBurn_mbtu]              REAL           CONSTRAINT [DF_tScenarioResultMetrics_FuelBurn_mbtu] DEFAULT ((0)) NOT NULL,
    [FuelBurn_tons]              REAL           CONSTRAINT [DF_tScenarioResultMetrics_FuelBurn_tons] DEFAULT ((0)) NOT NULL,
    [Particulate_tons]           REAL           CONSTRAINT [DF_tScenarioResultMetrics_Particulate_tons] DEFAULT ((0)) NOT NULL,
    [GrossMargin_dollars]        REAL           CONSTRAINT [DF_tScenarioResultMetrics_GrossMargin_dollars] DEFAULT ((0)) NOT NULL,
    [TotalRevenue_dollars]       REAL           CONSTRAINT [DF_tScenarioResultMetrics_TotalRevenue_dollars] DEFAULT ((0)) NOT NULL,
    [FuelCost_dollars]           REAL           CONSTRAINT [DF_tScenarioResultMetrics_FuelCost_dollars] DEFAULT ((0)) NOT NULL,
    [TotalVariableOandM_dollars] REAL           CONSTRAINT [DF_tScenarioResultMetrics_TotalVariableOandM_dollars] DEFAULT ((0)) NOT NULL,
    [Generation_mwh]             REAL           CONSTRAINT [DF_tScenarioResultMetrics_Generation_mwh] DEFAULT ((0)) NOT NULL,
    [MaximumAchievable_Load_mw]  REAL           CONSTRAINT [DF_tScenarioResultMetrics_MaximumAchievable_Load_mw] DEFAULT ((0)) NOT NULL,
    [EAF_pct]                    REAL           CONSTRAINT [DF_tScenarioResultMetrics_EAF_pct] DEFAULT ((0)) NOT NULL,
    [ReliabilityIndex_pct]       REAL           CONSTRAINT [DF_tScenarioResultMetrics_ReliabilityIndex_pct] DEFAULT ((0)) NOT NULL,
    [PlannedMaintenance_days]    REAL           CONSTRAINT [DF_tScenarioResultMetrics_PlannedMaintenance_days] DEFAULT ((0)) NOT NULL,
    [BalanceOfPlant_pct]         REAL           CONSTRAINT [DF_tScenarioResultMetrics_BalanceOfPlant_pct] DEFAULT ((0)) NOT NULL,
    [BoilerTubeFailureRate_pct]  REAL           CONSTRAINT [DF_tScenarioResultMetrics_BoilerTubeFailureRate_pct] DEFAULT ((0)) NOT NULL,
    [SO2Emissions_tons]          REAL           CONSTRAINT [DF_tScenarioResultMetrics_SO2Emissions_tons] DEFAULT ((0)) NOT NULL,
    [NOxEmissions_tons]          REAL           CONSTRAINT [DF_tScenarioResultMetrics_NOxEmissions_tons] DEFAULT ((0)) NOT NULL,
    [HgEmissions_lbm]            REAL           CONSTRAINT [DF_tScenarioResultMetrics_HgEmissions_lbm] DEFAULT ((0)) NOT NULL,
    [CO2Emissions_ktons]         REAL           CONSTRAINT [DF_tScenarioResultMetrics_CO2Emissions_ktons] DEFAULT ((0)) NOT NULL,
    [SO2InletEmissions_tons]     REAL           CONSTRAINT [DF_tScenarioResultMetrics_SO2InletEmissions_tons] DEFAULT ((0)) NOT NULL,
    [HHV_btu]                    REAL           CONSTRAINT [DF_tScenarioResultMetrics_HHV_btu] DEFAULT ((0)) NOT NULL,
    [CreateDate]                 DATETIME       CONSTRAINT [DF_tScenarioResultMetrics_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                  NVARCHAR (255) NOT NULL,
    [ChangeDate]                 DATETIME       CONSTRAINT [DF_tScenarioResultMetrics_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [ChangedBy]                  NVARCHAR (255) NOT NULL,
    [MATS_Margin_pct]            REAL           NULL,
    [MATS_SO2_HCl_Margin_pct]    REAL           NULL,
    [MATS_Hg_Margin_pct]         REAL           NULL,
    [MATS_PM_Margin_pct]         REAL           NULL,
    [MATS_SO2_Margin_pct]        REAL           NULL,
    [MATS_HCl_Margin_pct]        REAL           NULL,
    CONSTRAINT [PK_tScenarioResultMetrics] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tScenarioResultMetrics_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioResultMetrics_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);





 



GO
CREATE NONCLUSTERED INDEX IDX_tScenarioResultMetrics_1
ON [Projection].[tScenarioResultMetrics] (ScenarioID,AssetID,Year,Month,Day)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioResultMetrics';

