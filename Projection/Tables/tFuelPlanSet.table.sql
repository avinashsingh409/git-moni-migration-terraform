CREATE TABLE [Projection].[tFuelPlanSet] (
    [FuelPlanSetID]           INT            IDENTITY (1, 1) NOT NULL,
    [FuelPlanSetDesc]         NVARCHAR (255) NOT NULL,
    [DataVersionID]           INT            NULL,
    [Comment]                 NVARCHAR (255) NULL,
    [PlanYear]                SMALLINT       CONSTRAINT [DF__tFuelSuppl__Year__320C68B7] DEFAULT (datepart(year,getdate())) NOT NULL,
    [IsConsensus]             BIT            CONSTRAINT [DF__tFuelSupp__IsCon__33008CF0] DEFAULT ((0)) NOT NULL,
    [IsSensitivity]           BIT            CONSTRAINT [DF_tFuelBurnSet_IsSensitivity] DEFAULT ((0)) NOT NULL,
    [CreatedBy]               NVARCHAR (255) NOT NULL,
    [ChangedBy]               NVARCHAR (255) NOT NULL,
    [CreateDate]              DATETIME       CONSTRAINT [DF__tFuelSupp__Creat__33F4B129] DEFAULT (getdate()) NOT NULL,
    [SensitivityImpactTypeID] INT            NULL,
    [ModifiedDate]            DATETIME       CONSTRAINT [DF_tFuelPlanSet_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]              DATETIME       CONSTRAINT [DF__tFuelPlan__Chang__2C938683] DEFAULT (getdate()) NOT NULL,
    [ImportDate]              DATETIME       NULL,
    [IsPublic]                BIT            CONSTRAINT [DF_tFuelPlanSet_IsPublic] DEFAULT ((0)) NOT NULL,
    [IsValid]                 BIT            DEFAULT ((1)) NOT NULL,
    [IsArchived]              BIT            CONSTRAINT [DF_tFuelPlanSet_IsArchived] DEFAULT ((0)) NOT NULL,
    [OwningAssetID]           INT            NULL,
    [FuelPlanSetAbbrev]       NVARCHAR (50)  CONSTRAINT [FuelPlanSetAbbrevDefault] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_tFuelPlanSet] PRIMARY KEY CLUSTERED ([FuelPlanSetID] ASC),
    CONSTRAINT [FK_Projection_tFuelPlanSet_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tFuelPlanSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID]),
    CONSTRAINT [FK_tFuelPlanSet_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of monthly fuel burn plan data.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFuelPlanSet';

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFuelPlanSet';

