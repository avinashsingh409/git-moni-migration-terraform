﻿CREATE TABLE [Projection].[tInputType] (
    [InputTypeID]             INT           NOT NULL,
    [InputTypeDesc]           NVARCHAR (50) NOT NULL,
    [InputTypeAbbrev]         NVARCHAR (50) NOT NULL,
    [VariableTypeID]          INT           NULL,
    [AssetClassTypeID]        INT           NOT NULL,
    [ValueTypeID]             INT           NULL,
    [InputTypeLegacyDomainID] INT           NULL,
    CONSTRAINT [PK_tInputType] PRIMARY KEY CLUSTERED ([InputTypeID] ASC),
    CONSTRAINT [FK_Projection_tInputType_InputTypeLegacyDomainID_tInputTypeLegacyDomain_InputTypeLegacyDomainID] FOREIGN KEY ([InputTypeLegacyDomainID]) REFERENCES [Projection].[tInputTypeLegacyDomain] ([InputTypeLegacyDomainID]),
    CONSTRAINT [FK_tInputType_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tInputType_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tInputType_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
);



