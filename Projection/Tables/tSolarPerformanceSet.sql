﻿CREATE TABLE [Projection].[tSolarPerformanceSet] (
    [SolarPerformanceSetID]  INT            IDENTITY (1, 1) NOT NULL,
    [SolarPeformanceSetDesc] NVARCHAR (255) NOT NULL,
    [SolarPerformanceAbbrev] NVARCHAR (55)  NOT NULL,
    [IsConsensus]            BIT            NOT NULL,
    [CreatedBy]              NVARCHAR (255) NOT NULL,
    [ChangedBy]              NVARCHAR (255) NOT NULL,
    [CreateDate]             DATETIME       NOT NULL,
    [ChangeDate]             DATETIME       NOT NULL,
    CONSTRAINT [PK_tSolarPerformanceSetID] PRIMARY KEY CLUSTERED ([SolarPerformanceSetID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tSolarPerformanceSet';

