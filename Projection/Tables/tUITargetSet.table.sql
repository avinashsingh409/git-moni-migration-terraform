CREATE TABLE [Projection].[tUITargetSet] (
    [UITargetSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [UITargetSetDesc] NVARCHAR (255) NOT NULL,
    [DataVersionID]   INT            NULL,
    [PlanYear]        SMALLINT       CONSTRAINT [DF__tEmissionS__Year__3F9B6DFF] DEFAULT (datepart(year,getdate())) NOT NULL,
    [EditDate]        DATETIME       NULL,
    [IsConsensus]     BIT            CONSTRAINT [DF__tEmission__IsCon__408F9238] DEFAULT ((0)) NOT NULL,
    [CreatedBy]       NVARCHAR (255) NOT NULL,
    [ChangedBy]       NVARCHAR (255) NOT NULL,
    [CreateDate]      DATETIME       CONSTRAINT [DF__tEmission__Creat__4183B671] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tUITargetSet] PRIMARY KEY CLUSTERED ([UITargetSetID] ASC),
    CONSTRAINT [FK_tUITargetSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of system level emission limits over time', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetSet';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetSet';

