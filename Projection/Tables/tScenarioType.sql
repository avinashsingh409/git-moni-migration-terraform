﻿CREATE TABLE [Projection].[tScenarioType] (
    [ScenarioTypeID]   INT            NOT NULL,
    [ScenarioTypeDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tScenarioType] PRIMARY KEY CLUSTERED ([ScenarioTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table providing different scenario types.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioType';

