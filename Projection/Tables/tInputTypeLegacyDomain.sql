﻿CREATE TABLE [Projection].[tInputTypeLegacyDomain] (
    [InputTypeLegacyDomainID]   INT           NOT NULL,
    [InputTypeLegacyDomainDesc] NVARCHAR (63) NOT NULL,
    CONSTRAINT [PK_tInputTypeLegacyDomain] PRIMARY KEY CLUSTERED ([InputTypeLegacyDomainID] ASC)
);

