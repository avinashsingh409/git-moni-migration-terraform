CREATE TABLE [Projection].[tVistaModelConfidence] (
    [VistaModelConfidenceID] INT            IDENTITY (1, 1) NOT NULL,
    [VistaModelID]           INT            NOT NULL,
    [ConfidenceTypeID]       INT            NOT NULL,
    [ConfidenceValue]        REAL           NOT NULL,
    [ConfidenceComment]      NVARCHAR (255) DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_tVistaModelConfidence] PRIMARY KEY CLUSTERED ([VistaModelConfidenceID] ASC),
    CONSTRAINT [FK_tVistaModelConfidence_ConfidenceTypeID_tConfidenceType] FOREIGN KEY ([ConfidenceTypeID]) REFERENCES [Projection].[tConfidenceType] ([ConfidenceTypeID]),
    CONSTRAINT [FK_tVistaModelConfidence_VistaModelID_tVistaModel] FOREIGN KEY ([VistaModelID]) REFERENCES [Projection].[tVistaModel] ([VistaModelID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tVistaModelConfidence';

