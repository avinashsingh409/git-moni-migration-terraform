﻿CREATE TABLE [Projection].[tCQIResultESPPerformance] (
    [CQIResultESPPerformanceID]  INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                INT  NOT NULL,
    [ESPInletGasFlow_cfm]        REAL DEFAULT ((0)) NOT NULL,
    [ESPMaxGasFlow_cfm]          REAL DEFAULT ((0)) NOT NULL,
    [ESPFFInletAshLoad_tonhr]    REAL DEFAULT ((0)) NOT NULL,
    [ESPFFMaxAshLoad_tonhr]      REAL DEFAULT ((0)) NOT NULL,
    [ESPFFBosFanFlowReq_cfm]     REAL DEFAULT ((0)) NOT NULL,
    [ESPFFBosFanFlowAvail_cfm]   REAL DEFAULT ((0)) NOT NULL,
    [ESPFFBosFanPressReq_inwg]   REAL DEFAULT ((0)) NOT NULL,
    [ESPFFBosFanPressAvail_inwg] REAL DEFAULT ((0)) NOT NULL,
    [ESPFFBosFanPowerReq_hp]     REAL DEFAULT ((0)) NOT NULL,
    [ESPFFBosFanPowerAvail_hp]   REAL DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCQIResultESPPerformance] PRIMARY KEY CLUSTERED ([CQIResultESPPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultESPPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX IDX_tCQIResultESPPerformance_CQIResultID
ON [Projection].[tCQIResultESPPerformance] ([CQIResultID])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultESPPerformance';

