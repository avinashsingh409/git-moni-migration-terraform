CREATE TABLE [Projection].[tSensitivityType] (
    [SensitivityTypeID]   INT            NOT NULL,
    [SensitivityTypeDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tSensitivityType] PRIMARY KEY CLUSTERED ([SensitivityTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table providing different sensitivity types.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tSensitivityType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tSensitivityType';

