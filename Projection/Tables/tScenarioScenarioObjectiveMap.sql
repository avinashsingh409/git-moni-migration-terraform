﻿CREATE TABLE [Projection].[tScenarioScenarioObjectiveMap] (
    [ScenarioScenarioObjectiveTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                         INT NOT NULL,
    [ScenarioObjectiveTypeID]            INT NOT NULL,
    CONSTRAINT [FK_tScenarioScenarioObjectiveMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioScenarioObjectiveMap_tScenarioObjectiveType] FOREIGN KEY ([ScenarioObjectiveTypeID]) REFERENCES [Projection].[tScenarioObjectiveType] ([ScenarioObjectiveTypeID])
);



