﻿CREATE TABLE [Projection].[tScenarioUnitEmissionsRateHgMismatchIDBackup] (
    [ScenarioUnitEmissionsRateID] INT            NOT NULL,
    [ScenarioID]                  INT            NOT NULL,
    [UnitID]                      INT            NOT NULL,
    [UIItemID]                    INT            NOT NULL,
    [StartDate]                   DATETIME       NOT NULL,
    [Rate]                        REAL           NOT NULL,
    [Comment]                     NVARCHAR (MAX) NULL
);

