﻿CREATE TABLE [Projection].[tWeatherForecast] (
    [WeatherForecastID]           INT            IDENTITY (1, 1) NOT NULL,
    [WeatherAssetID]              INT            NOT NULL,
    [TimeStamp]                   DATETIME       NULL,
    [DailyMax_degF]               REAL           NULL,
    [DailyMin_degF]               REAL           NULL,
    [Temperature_degF]            REAL           NULL,
    [Precipitation_in]            REAL           NULL,
    [CloudCover_pct]              REAL           NULL,
    [WindSpeed_MPH]               REAL           NULL,
    [WindDirection_deg]           REAL           NULL,
    [WindSpeedGust_MPH]           REAL           NULL,
    [RelativeHumidity_pct]        REAL           NULL,
    [DewPointTemperature_degF]    REAL           NULL,
    [WeatherIcon]                 NVARCHAR (MAX) NULL,
    [MaxRelativeHumidity_pct]     REAL           NULL,
    [MinRelativeHumidity_pct]     REAL           NULL,
    [ApparentTemperature_degF]    REAL           NULL,
    [Precipitation12HourProb_pct] REAL           NULL,
    [Snowfall_in]                 REAL           NULL,
    [IceAccumulation_in]          REAL           NULL,
    CONSTRAINT [PK_tWeatherForecastID] PRIMARY KEY CLUSTERED ([WeatherForecastID] ASC),
    CONSTRAINT [FK_tWeatherForecast_tWeatherAsset] FOREIGN KEY ([WeatherAssetID]) REFERENCES [Projection].[tWeatherAsset] ([WeatherAssetID]) ON DELETE CASCADE
);






GO
/* Create FK between tWeatherForecast and tWeatherAsset */

GO

