CREATE TABLE [Projection].[tUITargetClientDetail] (
    [UITargetClientDetailID] INT           IDENTITY (1, 1) NOT NULL,
    [UITargetSetID]          INT           NOT NULL,
    [UIItemID]               INT           NOT NULL,
    [UITargetTypeID]         INT           NOT NULL,
    [StartDate]              DATETIME      NOT NULL,
    [EndDate]                DATETIME      NOT NULL,
    [Quantity]               FLOAT (53)    NOT NULL,
    [QuantityUnit]           NVARCHAR (20) CONSTRAINT [DF_tUITargetClientDetail_QuantityUnit] DEFAULT (N'ton') NOT NULL,
    [Margin]                 FLOAT (53)    NOT NULL,
    [MarginUnit]             NVARCHAR (20) CONSTRAINT [DF_tUITargetClientDetail_MarginUnit] DEFAULT (N'ton') NOT NULL,
    [IsRegulatory]           BIT           CONSTRAINT [DF_tUITargetClientDetail_IsReg] DEFAULT ((0)) NOT NULL,
    [OwningAssetID]          INT           NULL,
	[AssetID]				 int		   null,
    CONSTRAINT [PK_tUITargetClientDetail] PRIMARY KEY CLUSTERED ([UITargetClientDetailID] ASC),
    CONSTRAINT [FK_Projection_tUITargetClientDetail_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_Projection_tUITargetClientDetail_AssetID_Asset_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tUITargetClientDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]),
    CONSTRAINT [FK_tUITargetClientDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID]),
    CONSTRAINT [FK_tUITargetClientDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUITargetClientDetail';

