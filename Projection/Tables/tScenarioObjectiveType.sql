﻿CREATE TABLE [Projection].[tScenarioObjectiveType] (
    [ScenarioObjectiveTypeID]     INT           NOT NULL,
    [ScenarioObjectiveTypeDesc]   NVARCHAR (50) NOT NULL,
    [ScenarioObjectiveTypeAbbrev] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tScenarioObjectiveType] PRIMARY KEY CLUSTERED ([ScenarioObjectiveTypeID] ASC)
);

