CREATE TABLE [Projection].[tGenerationDailyBucket] (
    [GenerationDailyBucketID] INT        IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]         INT        NOT NULL,
    [UnitID]                  INT        NOT NULL,
    [Year]                    SMALLINT   NOT NULL,
    [Month]                   TINYINT    NOT NULL,
    [Day]                     TINYINT    NOT NULL,
    [LoadBucket_mw]           FLOAT (53) NOT NULL,
    [Hours]                   FLOAT (53) NOT NULL,
    CONSTRAINT [PK_tGenerationDailyBucketID] PRIMARY KEY CLUSTERED ([GenerationDailyBucketID] ASC),
    CONSTRAINT [FK_tGenerationDailyBucket_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationDailyBucket_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationDailyBucket';

