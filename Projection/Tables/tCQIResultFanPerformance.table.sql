CREATE TABLE [Projection].[tCQIResultFanPerformance] (
    [CQIResultFanPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]               INT  NOT NULL,
    [FlowRequired_cfm]          REAL NOT NULL,
    [FlowAvailable_cfm]         REAL NOT NULL,
    [PressureRequired_inwg]     REAL NOT NULL,
    [PressureAvailable_inwg]    REAL NOT NULL,
    [PowerRequired_hp]          REAL NOT NULL,
    [PowerAvailable_hp]         REAL NOT NULL,
    [FanTypeID]                 INT  NOT NULL,
    CONSTRAINT [PK_tCQIResultFanPerformance] PRIMARY KEY CLUSTERED ([CQIResultFanPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultFanPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tCQIResultFanPerformance_FanTypeID_tFanType] FOREIGN KEY ([FanTypeID]) REFERENCES [Projection].[tFanType] ([FanTypeID])
);






GO
CREATE NONCLUSTERED INDEX IDX_tCQIResultFanPerformance_FanTypeID
ON [Projection].[tCQIResultFanPerformance] ([CQIResultID],[FanTypeID])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultFanPerformance';

