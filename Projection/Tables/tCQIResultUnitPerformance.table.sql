﻿CREATE TABLE [Projection].[tCQIResultUnitPerformance] (
    [CQIResultUnitPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                INT  NOT NULL,
    [GrossLoad_mw]               REAL NOT NULL,
    [NetLoad_mw]                 REAL NOT NULL,
    [AuxLoad_mw]                 REAL NOT NULL,
    [MaxGrossLoad_mw]            REAL NOT NULL,
    [NetPlantHeatrate_btukwh]    REAL NOT NULL,
    [FuelBurnRate_tonhr]         REAL NOT NULL,
    [FuelBurnRate_mbtuhr]        REAL NOT NULL,
    [SO2Rate_lbmmbtu]            REAL NOT NULL,
    [NOxRate_lbmmbtu]            REAL NOT NULL,
    [CO2Rate_lbmmbtu]            REAL NOT NULL,
    [HgRate_lbmtbtu]             REAL NOT NULL,
    [H2SO4Rate_lbmmbtu]          REAL NOT NULL,
    [EAF_percent]                REAL NOT NULL,
    [Particulate_lbmmbtu]        REAL NOT NULL,
    [BottomAsh_tonhr]            REAL NOT NULL,
    [FlyAsh_tonhr]               REAL NOT NULL,
    [Gypsum_tonhr]               REAL NOT NULL,
    [FGDAdditive_tonhr]          REAL NOT NULL,
    [FGDFixative_tonhr]          REAL NOT NULL,
    [FGDWaste_tonhr]             REAL NOT NULL,
    [DryFGDWaste_tonhr]          REAL NOT NULL,
    [FurnaceSorbent_tonhr]       REAL NOT NULL,
    [SO3Feedstock_tonhr]         REAL NOT NULL,
    [CoalAdditive_tonhr]         REAL NOT NULL,
    [SCRReagent_tonhr]           REAL NOT NULL,
    [SNCRReagent_tonhr]          REAL NOT NULL,
    [EFOR_hour]                  REAL NOT NULL,
    [HgAdditive_tonhr]           REAL NOT NULL,
    [SaleableFlyAsh_tonhr]       REAL NOT NULL,
    [NonSaleableFlyAsh_tonhr]    REAL NOT NULL,
    [FGDWater_gallonhr]          REAL NOT NULL,
    [FlueGasSorbent_tonhr]       REAL NOT NULL,
    [FGDDBAAdditive_gallonhr]    REAL NOT NULL,
    [CFBBedReagent_tonhr]        REAL NOT NULL,
    [NH3Feedstock_tonhr]         REAL NOT NULL,
    [SaleableBottomAsh_tonhr]    REAL DEFAULT ((0)) NULL,
    [NonSaleableBottomAsh_tonhr] REAL DEFAULT ((0)) NULL,
    [HCL_tonhr]                  REAL NULL,
    [Opacity_percent]            REAL DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCQIResultUnitPerformance] PRIMARY KEY CLUSTERED ([CQIResultUnitPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultUnitPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);












GO
CREATE NONCLUSTERED INDEX [IDX_tCQIResultUnitPerformance_CQIResultID_INCLUDEALL] ON [Projection].[tCQIResultUnitPerformance] (
[CQIResultID]
) 
INCLUDE (
[GrossLoad_mw],
[NetLoad_mw],
[AuxLoad_mw],
[MaxGrossLoad_mw],
[NetPlantHeatrate_btukwh],
[FuelBurnRate_tonhr],
[FuelBurnRate_mbtuhr],
[SO2Rate_lbmmbtu],
[NOxRate_lbmmbtu],
[CO2Rate_lbmmbtu],
[HgRate_lbmtbtu],
[H2SO4Rate_lbmmbtu],
[EAF_percent],
[Particulate_lbmmbtu],
[BottomAsh_tonhr],
[FlyAsh_tonhr],
[Gypsum_tonhr],
[FGDAdditive_tonhr],
[FGDFixative_tonhr],
[FGDWaste_tonhr],
[DryFGDWaste_tonhr],
[FurnaceSorbent_tonhr],
[SO3Feedstock_tonhr],
[CoalAdditive_tonhr],
[SCRReagent_tonhr],
[SNCRReagent_tonhr],
[EFOR_hour],
[HgAdditive_tonhr],
[SaleableFlyAsh_tonhr],
[NonSaleableFlyAsh_tonhr],
[FGDWater_gallonhr],
[FlueGasSorbent_tonhr],
[FGDDBAAdditive_gallonhr],
[CFBBedReagent_tonhr],
[NH3Feedstock_tonhr]
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultUnitPerformance';

