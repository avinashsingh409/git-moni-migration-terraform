﻿CREATE TRIGGER [Projection].[tScenario_CheckInheritanceChanges]
   ON  [Projection].[tScenario]
   FOR UPDATE
AS
BEGIN    
	DECLARE @pScenarioID int;
	DECLARE @pDemandSetChanged bit;
	DECLARE @pForecastSetChanged bit;
	DECLARE @pFuelSetChanged bit;
	DECLARE @pMaintenanceSetChanged bit;
	DECLARE @pUConfigSetChanged bit;
	DECLARE @pBaseGenerationSetID int;
	DECLARE @pDemandSetID int;
	DECLARE @pGenerationSetID int;
	DECLARE @pForecastSetID int;
	DECLARE @pFuelPlanSetID int;
	DECLARE @pBaseFuelPlanSetID int;
	DECLARE @pMaintenancePlanSetID int;
	DECLARE @pBaseUnitConfigScheduleSetID int;
	DECLARE @pUnitConfigScheduleSetID int;
	
	select 
		CONVERT(bit, (CASE tI.BaseGenerationSetID - tD.BaseGenerationSetID WHEN 0 then 0 Else 1 END)) as BaseGenerationSetIDChanged, 
		CONVERT(bit, (CASE ISNULL(tI.DemandSetID, -1) - ISNULL(tD.DemandSetID, -1) WHEN 0 then 0 Else 1 END)) as DemandSetIDChanged,
		CONVERT(bit, (CASE tI.GenerationSetID - tD.GenerationSetID WHEN 0 then 0 Else 1 END)) as GenerationSetIDChanged,
		CONVERT(bit, (CASE ISNULL(tI.ForecastSetID, -1) - ISNULL(tD.ForecastSetID, -1) WHEN 0 THEN 0 ELSE 1 END)) as ForecastSetIDChanged,
		CONVERT(bit, (CASE tI.BaseFuelPlanSetID - tD.BaseFuelPlanSetID WHEN 0 then 0 Else 1 END)) as BaseFuelPlanSetIDChanged,
		CONVERT(bit, (CASE tI.FuelPlanSetID - tD.FuelPlanSetID WHEN 0 then 0 Else 1 END)) as FuelPlanSetIDChanged,
		CONVERT(bit, (CASE ISNULL(tI.MaintenancePlanSetID, -1) - ISNULL(tD.MaintenancePlanSetID, -1) WHEN 0 THEN 0 ELSE 1 END)) as MaintenancePlanSetIDChanged,
		CONVERT(bit, (CASE tI.BaseUnitConfigScheduleSetID - tD.BaseUnitConfigScheduleSetID WHEN 0 then 0 Else 1 END)) as BaseUnitConfigScheduleSetIDChanged,
		CONVERT(bit, (CASE tI.UnitConfigScheduleSetID - tD.UnitConfigScheduleSetID WHEN 0 then 0 Else 1 END)) as UnitConfigScheduleSetIDChanged,
		tI.ScenarioID,
		tI.BaseGenerationSetID, tI.DemandSetID, tI.GenerationSetID, tI.ForecastSetID, tI.BaseFuelPlanSetID,
		tI.FuelPlanSetID, tI.MaintenancePlanSetID, tI.BaseUnitConfigScheduleSetID, tI.UnitConfigScheduleSetID
	INTO #tChangeData
	from inserted tI INNER JOIN deleted tD ON tI.ScenarioID = tD.ScenarioID
	;
	
	DECLARE changeCursor CURSOR LOCAL
	FOR Select ScenarioID, 
		BaseGenerationSetIDChanged | DemandSetIDChanged | GenerationSetIDChanged, ForecastSetIDChanged, BaseFuelPlanSetIDChanged | 		
		FuelPlanSetIDChanged, MaintenancePlanSetIDChanged, BaseUnitConfigScheduleSetIDChanged | UnitConfigScheduleSetIDChanged,
		BaseGenerationSetID, DemandSetID, GenerationSetID, ForecastSetID, BaseFuelPlanSetID,
		FuelPlanSetID, MaintenancePlanSetID, BaseUnitConfigScheduleSetID, UnitConfigScheduleSetID
		FROM #tChangeData;

	DECLARE @tOldChildScenarioData TABLE(
	ChildScenarioID int not null,
	BaseGenerationSetID int not null, DemandSetID int null, GenerationSetID int not null,
	ForecastSetID int null, BaseFuelPlanSetID int not null, 
	FuelPlanSetID int not null, MaintenancePlanSetID int null,
	BaseUnitConfigScheduleSetID int not null, UnitConfigScheduleSetID int not null
	)
	;
	
	DECLARE @recordsLeft int;
	SELECT @recordsLeft = COUNT(*) FROM #tChangeData;
	
	WHILE @recordsLeft > 0
	BEGIN 
	
		OPEN changeCursor;
		FETCH NEXT FROM changeCursor 
		INTO @pScenarioID, @pDemandSetChanged, @pForecastSetChanged, @pFuelSetChanged, @pMaintenanceSetChanged, @pUConfigSetChanged,
			@pBaseGenerationSetID, @pDemandSetID, @pGenerationSetID, @pForecastSetID, @pBaseFuelPlanSetID,
			@pFuelPlanSetID, @pMaintenancePlanSetID, @pBaseUnitConfigScheduleSetID, @pUnitConfigScheduleSetID;
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO @tOldChildScenarioData
			SELECT ScenarioID as ChildScenarioID,
			BaseGenerationSetID, DemandSetID, GenerationSetID, ForecastSetID, BaseFuelPlanSetID,
			FuelPlanSetID, MaintenancePlanSetID, BaseUnitConfigScheduleSetID, UnitConfigScheduleSetID
			FROM Projection.tScenario
			WHERE ParentScenarioID = @pScenarioID AND 
			(
				((@pDemandSetChanged & [DemandIDsAreInherited]) = 1)
				OR
				((@pForecastSetChanged & [ForecastSetIDIsInherited]) = 1)
				OR
				((@pFuelSetChanged & [FuelPlanSetIDIsInherited]) = 1)
				OR
				((@pMaintenanceSetChanged & [MaintenancePlanSetIDIsInherited]) = 1)
				OR
				((@pUConfigSetChanged & [UnitConfigScheduleSetIDIsInherited]) = 1)
			)
			;
			
			
			
			UPDATE Projection.tScenario
			SET RunStatus = NULL, RunDate = NULL, ChangeDate = DEFAULT,
			ForecastSetID = (CASE @pForecastSetChanged & [ForecastSetIDIsInherited] WHEN 1 THEN @pForecastSetID ELSE ForecastSetID END),
			BaseFuelPlanSetID = (CASE @pFuelSetChanged & [FuelPlanSetIDIsInherited] WHEN 1 THEN @pFuelPlanSetID ELSE BaseFuelPlanSetID END),
			FuelPlanSetID = (CASE @pFuelSetChanged & [FuelPlanSetIDIsInherited] WHEN 1 THEN @pFuelPlanSetID ELSE FuelPlanSetID END),
			MaintenancePlanSetID = (CASE @pMaintenanceSetChanged & [MaintenancePlanSetIDIsInherited] WHEN 1 THEN @pMaintenancePlanSetID ELSE MaintenancePlanSetID END),
			--If the demand plan is null, we'll be using a Generation plan.
			--if the demand plan isn't null, we'll be adjusting a demand plan before building a generation plan. 
			BaseGenerationSetID = (CASE @pDemandSetChanged & [DemandIDsAreInherited] WHEN 1 THEN ISNULL(@pDemandSetID, @pGenerationSetID) ELSE BaseGenerationSetID END),
			DemandSetID = (CASE @pDemandSetChanged & [DemandIDsAreInherited] WHEN 1 THEN @pDemandSetID ELSE DemandSetID END),
			GenerationSetID = (CASE @pDemandSetChanged & [DemandIDsAreInherited] WHEN 1 THEN ISNULL(@pDemandSetID, @pGenerationSetID) ELSE GenerationSetID END),
			BaseUnitConfigScheduleSetID = (CASE @pUConfigSetChanged & [UnitConfigScheduleSetIDIsInherited] WHEN 1 THEN @pUnitConfigScheduleSetID ELSE BaseUnitConfigScheduleSetID END),
			UnitConfigScheduleSetID = (CASE @pUConfigSetChanged & [UnitConfigScheduleSetIDIsInherited] WHEN 1 THEN @pUnitConfigScheduleSetID ELSE UnitConfigScheduleSetID END)
			WHERE ParentScenarioID = @pScenarioID AND 
			(
				((@pDemandSetChanged & [DemandIDsAreInherited]) = 1)
				OR
				((@pForecastSetChanged & [ForecastSetIDIsInherited]) = 1)
				OR
				((@pFuelSetChanged & [FuelPlanSetIDIsInherited]) = 1)
				OR
				((@pMaintenanceSetChanged & [MaintenancePlanSetIDIsInherited]) = 1)
				OR
				((@pUConfigSetChanged & [UnitConfigScheduleSetIDIsInherited]) = 1)
			)
			;
						
			FETCH NEXT FROM changeCursor 
			INTO @pScenarioID, @pDemandSetChanged, @pForecastSetChanged, @pFuelSetChanged, @pMaintenanceSetChanged, @pUConfigSetChanged,
				@pBaseGenerationSetID, @pDemandSetID, @pGenerationSetID, @pForecastSetID, @pBaseFuelPlanSetID,
				@pFuelPlanSetID, @pMaintenancePlanSetID, @pBaseUnitConfigScheduleSetID, @pUnitConfigScheduleSetID;

		END
		CLOSE changeCursor;
		
		TRUNCATE TABLE #tChangeData;
		
		INSERT INTO #tChangeData
		select 
			CONVERT(bit, (CASE tI.BaseGenerationSetID - tD.BaseGenerationSetID WHEN 0 then 0 Else 1 END)) as BaseGenerationSetIDChanged, 
			CONVERT(bit, (CASE ISNULL(tI.DemandSetID, -1) - ISNULL(tD.DemandSetID, -1) WHEN 0 then 0 Else 1 END)) as DemandSetIDChanged,
			CONVERT(bit, (CASE tI.GenerationSetID - tD.GenerationSetID WHEN 0 then 0 Else 1 END)) as GenerationSetIDChanged,
			CONVERT(bit, (CASE ISNULL(tI.ForecastSetID, -1) - ISNULL(tD.ForecastSetID, -1) WHEN 0 THEN 0 ELSE 1 END)) as ForecastSetIDChanged,
			CONVERT(bit, (CASE tI.BaseFuelPlanSetID - tD.BaseFuelPlanSetID WHEN 0 then 0 Else 1 END)) as BaseFuelPlanSetIDChanged,
			CONVERT(bit, (CASE tI.FuelPlanSetID - tD.FuelPlanSetID WHEN 0 then 0 Else 1 END)) as FuelPlanSetIDChanged,
			CONVERT(bit, (CASE ISNULL(tI.MaintenancePlanSetID, -1) - ISNULL(tD.MaintenancePlanSetID, -1) WHEN 0 THEN 0 ELSE 1 END)) as MaintenancePlanSetIDChanged,
			CONVERT(bit, (CASE tI.BaseUnitConfigScheduleSetID - tD.BaseUnitConfigScheduleSetID WHEN 0 then 0 Else 1 END)) as BaseUnitConfigScheduleSetIDChanged,
			CONVERT(bit, (CASE tI.UnitConfigScheduleSetID - tD.UnitConfigScheduleSetID WHEN 0 then 0 Else 1 END)) as UnitConfigScheduleSetIDChanged,
			tI.ScenarioID,
			tI.BaseGenerationSetID, tI.DemandSetID, tI.GenerationSetID, tI.ForecastSetID, tI.BaseFuelPlanSetID,
			tI.FuelPlanSetID, tI.MaintenancePlanSetID, tI.BaseUnitConfigScheduleSetID, tI.UnitConfigScheduleSetID
		from Projection.tScenario tI INNER JOIN @tOldChildScenarioData tD ON tI.ScenarioID = tD.ChildScenarioID
		;
		
		DELETE FROM @tOldChildScenarioData;		
		
		SELECT @recordsLeft = COUNT(*) FROM #tChangeData;
		
	END
	
	DROP TABLE #tChangeData;

END


GO

