﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 10/01/2008
-- Description:	Set when calcs should be rerun.
-- =============================================
CREATE TRIGGER [Projection].[UnitEquipmentStateDetailSetModifiedForCalcs]
   ON  [Projection].[tUnitEquipmentStateDetail]
   FOR INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
    IF UPDATE(Magnitude) OR UPDATE(Units)
	BEGIN		
		UPDATE tCQIResult SET IsModified = 1 
		FROM INSERTED a INNER JOIN tUnitConfig b ON a.UnitEquipmentStateSetID = b.UnitEquipmentStateSetID
		WHERE tCQIResult.UnitConfigID = b.UnitConfigID;

		UPDATE tScenario SET IsModified = 1 
		FROM INSERTED a INNER JOIN tUnitConfig b ON a.UnitEquipmentStateSetID = b.UnitEquipmentStateSetID
		INNER JOIN tUnitConfigScheduleDetail c ON b.UnitConfigID = c.UnitConfigID
		WHERE tScenario.UnitConfigScheduleSetID = c.UnitConfigScheduleSetID;
	END
END
