﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 12/01/2010
-- Description:	Set when calcs should be rerun.
-- =============================================
CREATE TRIGGER [Projection].[LoadBucketModifiedForCalcs]
   ON  [Projection].[tCQILoadBucket]
   FOR INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
    IF UPDATE(LoadPercent)
	BEGIN		
		UPDATE tCQIResult SET IsModified = 1 
		FROM INSERTED a INNER JOIN Projection.tUnitConfig b ON a.UnitID = b.UnitID
		WHERE tCQIResult.UnitConfigID = b.UnitConfigID;
	END
END
