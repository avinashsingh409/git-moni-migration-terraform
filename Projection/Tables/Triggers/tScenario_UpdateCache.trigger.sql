﻿
CREATE TRIGGER [Projection].[tScenario_UpdateCache]
   ON  [Projection].[tScenario]
   FOR INSERT,UPDATE,DELETE
AS
BEGIN    
	DELETE FROM [Base].[tDataCache] 
	WHERE DataType in ('GetScenariosResponse','GetAllScenariosResponse')
END
