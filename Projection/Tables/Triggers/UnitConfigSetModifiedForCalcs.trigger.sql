﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 10/01/2008
-- Description:	Set when calcs should be rerun.
-- =============================================
CREATE TRIGGER [Projection].[UnitConfigSetModifiedForCalcs]
   ON  [Projection].[tUnitConfig]
   FOR INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
    IF UPDATE(VistaModelID)
	BEGIN		
		UPDATE tCQIResult SET IsModified = 1 FROM INSERTED WHERE tCQIResult.UnitConfigID = INSERTED.UnitConfigID;

		UPDATE tScenario SET IsModified = 1 
		FROM INSERTED a INNER JOIN tUnitConfigScheduleDetail b ON a.UnitConfigID = b.UnitConfigID
		WHERE tScenario.UnitConfigScheduleSetID = b.UnitConfigScheduleSetID;
	END
END
