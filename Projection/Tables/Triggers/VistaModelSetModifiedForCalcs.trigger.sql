﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 10/01/2008
-- Description:	Set when calcs should be rerun.
-- =============================================
CREATE TRIGGER [Projection].[VistaModelSetModifiedForCalcs]
   ON  [Projection].[tVistaModel]
   FOR INSERT,UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
    IF UPDATE(UnitModelID) OR UPDATE(MASetID)
	BEGIN		
		UPDATE tCQIResult SET IsModified = 1 
		FROM INSERTED a INNER JOIN tUnitConfig b ON a.VistaModelID = b.VistaModelID
		WHERE tCQIResult.UnitConfigID = b.UnitConfigID;

		UPDATE tScenario SET IsModified = 1 
		FROM INSERTED a INNER JOIN tUnitConfig b ON a.VistaModelID = b.VistaModelID
		INNER JOIN tUnitConfigScheduleDetail c ON b.UnitConfigID = c.UnitConfigID
		WHERE tScenario.UnitConfigScheduleSetID = c.UnitConfigScheduleSetID;
	END
END
