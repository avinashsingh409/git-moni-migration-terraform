﻿CREATE TABLE [Projection].[tUIItemUnitMapHgMismatchIDBackup] (
    [UIItemUnitMapID] INT NOT NULL,
    [UIItemID]        INT NOT NULL,
    [UnitID]          INT NOT NULL,
    [AppContextID]    INT NOT NULL
);

