CREATE TABLE [Projection].[tDataCommentSet] (
    [DataCommentSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [DataCommentSetDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tDataCommentSet] PRIMARY KEY CLUSTERED ([DataCommentSetID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A collection of user specified comments for input data sets.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tDataCommentSet';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tDataCommentSet';

