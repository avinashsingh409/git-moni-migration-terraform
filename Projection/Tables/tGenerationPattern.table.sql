CREATE TABLE [Projection].[tGenerationPattern] (
    [GenerationPatternID]                  INT      IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]                      INT      NOT NULL,
    [UnitID]                               INT      NOT NULL,
    [StartDate]                            DATETIME CONSTRAINT [DF__tGenerationPattern__StartDate] DEFAULT (getdate()) NOT NULL,
    [EndDate]                              DATETIME CONSTRAINT [DF__tGenerationPattern__EndDate] DEFAULT (getdate()+(365)) NOT NULL,
    [WeekdayPeakStartHour]                 INT      CONSTRAINT [DF__tGenerationPattern__WeekdayPeakStartHour] DEFAULT ((6)) NOT NULL,
    [WeekdayPeakEndHour]                   INT      CONSTRAINT [DF__tGenerationPattern__WeekdayPeakEndHour] DEFAULT ((18)) NOT NULL,
    [WeekdayPeakCapacityFactor_percent]    REAL     CONSTRAINT [DF__tGenerationPattern__WeekdayPeakCapacityFactor_percent] DEFAULT ((95.0)) NOT NULL,
    [WeekdayOffPeakCapacityFactor_percent] REAL     CONSTRAINT [DF__tGenerationPattern__WeekdayOffPeakCapacityFactor_percent] DEFAULT ((60.0)) NOT NULL,
    [WeekendPeakStartHour]                 INT      CONSTRAINT [DF__tGenerationPattern__WeekendPeakStartHour] DEFAULT ((8)) NOT NULL,
    [WeekendPeakEndHour]                   INT      CONSTRAINT [DF__tGenerationPattern__WeekendPeakEndHour] DEFAULT ((20)) NOT NULL,
    [WeekendPeakCapacityFactor_percent]    REAL     CONSTRAINT [DF__tGenerationPattern__WeekendPeakCapacityFactor_percent] DEFAULT ((80.0)) NOT NULL,
    [WeekendOffPeakCapacityFactor_percent] REAL     CONSTRAINT [DF__tGenerationPattern__WeekendOffPeakCapacityFactor_percent] DEFAULT ((50.0)) NOT NULL,
    CONSTRAINT [PK_tGenerationPattern] PRIMARY KEY CLUSTERED ([GenerationPatternID] ASC),
    CONSTRAINT [FK_tGenerationPattern_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationPattern_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationPattern';

