﻿CREATE TABLE [Projection].[tUnitConfig] (
    [UnitConfigID]                      INT            IDENTITY (1, 1) NOT NULL,
    [UnitID]                            INT            NOT NULL,
    [VistaModelID]                      INT            NULL,
    [UnitConfigDesc]                    NVARCHAR (255) NOT NULL,
    [CreatedBy]                         NVARCHAR (255) NOT NULL,
    [ChangedBy]                         NVARCHAR (255) NOT NULL,
    [CreateDate]                        DATETIME       CONSTRAINT [DF__tUnitConf__Creat__5E1FF51F] DEFAULT (getdate()) NOT NULL,
    [SensitivityImpactTypeID]           INT            NULL,
    [ChangeDate]                        DATETIME       DEFAULT (getdate()) NOT NULL,
    [UnitConfigAbbrev]                  NVARCHAR (255) NOT NULL,
    [UnitEquipmentStateSetID]           INT            NULL,
    [FlyAshSold_percent]                REAL           CONSTRAINT [DF_tUnitConfig_FlyAshSold_percent] DEFAULT ((0)) NOT NULL,
    [BottomAshSold_percent]             REAL           CONSTRAINT [DF_tUnitConfig_BottomAshSold_percent] DEFAULT ((0)) NOT NULL,
    [GypsumSold_percent]                REAL           CONSTRAINT [DF_tUnitConfig_GypsumSold_percent] DEFAULT ((0)) NOT NULL,
    [MaxFlyAshSold_ton]                 REAL           CONSTRAINT [DF_tUnitConfig_MaxFlyAshSold_ton] DEFAULT ((-1)) NOT NULL,
    [MaxBottomAshSold_ton]              REAL           CONSTRAINT [DF_tUnitConfig_MaxBottomAshSold_ton] DEFAULT ((-1)) NOT NULL,
    [MaxGypsumSold_ton]                 REAL           CONSTRAINT [DF_tUnitConfig_MaxGypsumSold_ton] DEFAULT ((-1)) NOT NULL,
    [IsBaseConfig]                      BIT            DEFAULT ((0)) NOT NULL,
    [IsArchived]                        BIT            CONSTRAINT [DF_tUnitConfig_IsArchived] DEFAULT ((0)) NOT NULL,
    [SaleableFlyAshMoisture_percent]    REAL           CONSTRAINT [DF_ProjectiontUnitConfigSaleableFlyAshMoisture_percent] DEFAULT ((0)) NOT NULL,
    [NonSaleableFlyAshMoisture_percent] REAL           CONSTRAINT [DF_ProjectiontUnitConfigNonSaleableFlyAshMoisture_percent] DEFAULT ((0)) NOT NULL,
    [FGDAshMoisture_percent]            REAL           CONSTRAINT [DF_ProjectiontUnitConfigFGDAshMoisture_percent] DEFAULT ((0)) NOT NULL,
    [BottomAshMoisture_percent]         REAL           CONSTRAINT [DF_ProjectiontUnitConfigBottomAshMoisture_percent] DEFAULT ((0)) NOT NULL,
    [RAMModelID]                        INT            NULL,
    [IsHidden]                          BIT            CONSTRAINT [DF_Projection_tUnitConfig_IsHidden] DEFAULT ((0)) NOT NULL,
    [BaseFuelSourceID]                  INT            NULL,
    [BuiltFromAdjustment]               BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tUnitConfig] PRIMARY KEY NONCLUSTERED ([UnitConfigID] ASC),
    CONSTRAINT [FK_tUnitConfig_BaseFuelSourceID_tFuelSource] FOREIGN KEY ([BaseFuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tUnitConfig_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitConfig_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID]),
    CONSTRAINT [FK_tUnitConfig_UnitEquipmentStateSetID_tUnitEquipmentStateSet] FOREIGN KEY ([UnitEquipmentStateSetID]) REFERENCES [Projection].[tUnitEquipmentStateSet] ([UnitEquipmentStateSetID]),
    CONSTRAINT [FK_tUnitConfig_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitConfig_VistModelID_tVistaModel] FOREIGN KEY ([VistaModelID]) REFERENCES [Projection].[tVistaModel] ([VistaModelID])
);










GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Definition of how the unit is configured.  Physical equipment plus operating parameters.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitConfig';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitConfig';

