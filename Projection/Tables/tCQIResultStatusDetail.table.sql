CREATE TABLE [Projection].[tCQIResultStatusDetail] (
    [CQIResultStatusDetailID]   INT             IDENTITY (1, 1) NOT NULL,
    [CQIResultID]               INT             NOT NULL,
    [CQIResultStatusDetailDesc] NVARCHAR (2000) NOT NULL,
    CONSTRAINT [PK_tCQIResultStatusDetail] PRIMARY KEY CLUSTERED ([CQIResultStatusDetailID] ASC),
    CONSTRAINT [FK_tCQIResultStatusDetail_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultStatusDetail';

