﻿CREATE TABLE [Projection].[tScenarioKills] (
    [KillRequestID]     INT            IDENTITY (1, 1) NOT NULL,
    [KillRequestTime]   DATETIME       DEFAULT (sysdatetime()) NOT NULL,
    [KillProgress]      TINYINT        DEFAULT ((0)) NOT NULL,
    [KillCompletedTime] DATETIME       DEFAULT (NULL) NULL,
    [RequestedBy]       NVARCHAR (255) NOT NULL,
    [StatusInformation] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tScenarioKills] PRIMARY KEY CLUSTERED ([KillRequestID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioKills_KillRequest_KillRequestTime_KillProcessedTime]
    ON [Projection].[tScenarioKills]([KillRequestID] ASC, [KillRequestTime] ASC, [KillProgress] ASC);

