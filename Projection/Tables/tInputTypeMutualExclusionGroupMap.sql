﻿CREATE TABLE [Projection].[tInputTypeMutualExclusionGroupMap] (
    [InputTypeMutualExclusionGroupMapID] INT IDENTITY (1, 1) NOT NULL,
    [InputTypeID]                        INT NOT NULL,
    [MutualExclusionGroupID]             INT NOT NULL,
    CONSTRAINT [FK_tInputTypeMutualExclusionGroupMap_InputTypeID_tInputType] FOREIGN KEY ([InputTypeID]) REFERENCES [Projection].[tInputType] ([InputTypeID])
);

