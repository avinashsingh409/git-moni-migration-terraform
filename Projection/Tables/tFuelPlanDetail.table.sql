CREATE TABLE [Projection].[tFuelPlanDetail] (
    [FuelPlanDetailID]    INT      IDENTITY (1, 1) NOT NULL,
    [FuelPlanSetID]       INT      NOT NULL,
    [UnitID]              INT      NOT NULL,
    [Year]                SMALLINT NOT NULL,
    [Month]               TINYINT  NOT NULL,
    [FuelSourceID]        INT      NOT NULL,
    [MonthlyTon_fraction] REAL     NOT NULL,
    [FuelSourceOrder]     INT      NOT NULL,
    CONSTRAINT [PK_tFuelPlanDetail] PRIMARY KEY CLUSTERED ([FuelPlanDetailID] ASC),
    CONSTRAINT [FK_tFuelPlanDetail_FuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanDetail_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tFuelPlanDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Monthly fuel burn plan.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFuelPlanDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFuelPlanDetail';

