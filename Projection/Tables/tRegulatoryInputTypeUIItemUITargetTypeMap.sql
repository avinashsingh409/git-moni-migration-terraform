
CREATE TABLE [Projection].[tRegulatoryInputTypeUIItemUITargetTypeMap] (
    [RegulatoryInputTypeUIItemUITargetTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [InputTypeID]                                INT NOT NULL,
    [UIItemID]                                   INT NOT NULL,
    [UITargetTypeID]                             INT NOT NULL,
    CONSTRAINT [PK_tRegulatoryInputTypeUIItemUITargetTypeMap] PRIMARY KEY CLUSTERED ([RegulatoryInputTypeUIItemUITargetTypeMapID] ASC),
    CONSTRAINT [FK_tRegulatoryInputTypeUIItemUITargetTypeMap_InputTypeID] FOREIGN KEY ([InputTypeID]) REFERENCES [Projection].[tInputType] ([InputTypeID]),
    CONSTRAINT [FK_tRegulatoryInputTypeUIItemUITargetTypeMap_UIItemID] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]),
    CONSTRAINT [FK_tRegulatoryInputTypeUIItemUITargetTypeMap_UITargetTypeID] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
);


GO
ALTER TABLE [Projection].[tRegulatoryInputTypeUIItemUITargetTypeMap] CHECK CONSTRAINT [FK_tRegulatoryInputTypeUIItemUITargetTypeMap_InputTypeID]
GO
ALTER TABLE [Projection].[tRegulatoryInputTypeUIItemUITargetTypeMap] CHECK CONSTRAINT [FK_tRegulatoryInputTypeUIItemUITargetTypeMap_UIItemID]
GO
ALTER TABLE [Projection].[tRegulatoryInputTypeUIItemUITargetTypeMap] CHECK CONSTRAINT [FK_tRegulatoryInputTypeUIItemUITargetTypeMap_UITargetTypeID]
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Maps InputTypes to their corresponding UIItems.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tRegulatoryInputTypeUIItemUITargetTypeMap';

