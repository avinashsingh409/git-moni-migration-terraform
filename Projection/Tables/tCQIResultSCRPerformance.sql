﻿CREATE TABLE [Projection].[tCQIResultSCRPerformance] (
    [CQIResultSCRPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]               INT  NOT NULL,
    [RemEffActual_pct]          REAL NOT NULL,
    [RemEffReq_pct]             REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultSCRPerformance] PRIMARY KEY CLUSTERED ([CQIResultSCRPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultSCRPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultSCRPerformance';

