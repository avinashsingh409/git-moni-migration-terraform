﻿CREATE TABLE [Projection].[tScenarioFuelBurnSchedule] (
    [ScenarioFuelBurnScheduleID] INT      IDENTITY (1, 1) NOT NULL,
    [ScenarioID]                 INT      NOT NULL,
    [UnitID]                     INT      NOT NULL,
    [Year]                       SMALLINT NOT NULL,
    [Month]                      TINYINT  NOT NULL,
    [Day]                        TINYINT  NOT NULL,
    [FuelQualityID]              INT      NOT NULL,
    [Quantity_ton]               REAL     NOT NULL,
    [UnitConfigID]               INT      NOT NULL,
    [FuelSourceID]               INT      NOT NULL,
    [Quantity_mbtu]              REAL     NOT NULL,
    [Hour]                       TINYINT  NOT NULL,
    CONSTRAINT [PK_tScenarioFuelBurnSchedule] PRIMARY KEY CLUSTERED ([ScenarioFuelBurnScheduleID] ASC),
    CONSTRAINT [FK_tScenarioFuelBurnSchedule_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]),
    CONSTRAINT [FK_tScenarioFuelBurnSchedule_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tScenarioFuelBurnSchedule_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tScenarioFuelBurnSchedule_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tScenarioFuelBurnSchedule_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tScenarioFuelBurnSchedule_ScenarioID_FuelQualityID_FuelSourceID_UnitConfigID_UnitID_Year_Month_day] UNIQUE NONCLUSTERED ([ScenarioID] ASC, [FuelQualityID] ASC, [FuelSourceID] ASC, [UnitConfigID] ASC, [UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
);










GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Hourly fuel tons burned by unit with coal quality.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioFuelBurnSchedule';


GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioFuelBurnSchedule_ScenarioID_UnitID_Year_Month_Day_Hour_INCLUDE_FuelQualityID_FuelSourceID_UnitConfigID] ON [Projection].[tScenarioFuelBurnSchedule] 
(
	[ScenarioID] ASC,
	[UnitID] ASC,
	[Year] ASC,
	[Month] ASC,
	[Day] ASC,
	[Hour] ASC
)
INCLUDE ( [FuelQualityID],
[UnitConfigID],
[FuelSourceID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IDX_tScenarioFuelBurnSchedule_UnitID_Etc
ON [Projection].[tScenarioFuelBurnSchedule] ([ScenarioID],[UnitID])
INCLUDE ([Year],[Month],[Day],[FuelQualityID],[FuelSourceID],[Quantity_mbtu],[Hour])
GO
CREATE NONCLUSTERED INDEX [IDX_tScenarioFuelBurnSchedule_Helper]
    ON [Projection].[tScenarioFuelBurnSchedule]([UnitID] ASC, [Year] ASC, [Month] ASC, [Day] ASC, [Hour] ASC)
    INCLUDE([FuelQualityID], [Quantity_ton], [UnitConfigID], [FuelSourceID], [Quantity_mbtu], [ScenarioFuelBurnScheduleID], [ScenarioID]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioFuelBurnSchedule';


GO
GRANT ALTER
    ON OBJECT::[Projection].[tScenarioFuelBurnSchedule] TO [TEUser]
    AS [dbo];

