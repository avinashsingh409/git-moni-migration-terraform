CREATE TABLE [Projection].[tCQIResultConfidence] (
    [CQIResultConfidenceID] INT            IDENTITY (1, 1) NOT NULL,
    [CQIResultID]           INT            NOT NULL,
    [ConfidenceTypeID]      INT            NOT NULL,
    [ConfidenceValue]       REAL           NOT NULL,
    [ConfidenceComment]     NVARCHAR (255) DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_tCQIResultConfidence] PRIMARY KEY CLUSTERED ([CQIResultConfidenceID] ASC),
    CONSTRAINT [FK_tCQIResultConfidence_ConfidenceTypeID_tConfidenceType] FOREIGN KEY ([ConfidenceTypeID]) REFERENCES [Projection].[tConfidenceType] ([ConfidenceTypeID]),
    CONSTRAINT [FK_tCQIResultConfidence_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [IDX_tCQIResultConfidence_CQIResultID] ON [Projection].[tCQIResultConfidence] (
[CQIResultID]
) 
INCLUDE (	[ConfidenceTypeID],
[ConfidenceValue]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IDX_tCQIResultConfidence_ConfidenceTypeID
ON [Projection].[tCQIResultConfidence] ([ConfidenceTypeID])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultConfidence';

