CREATE TABLE [Projection].[tGenerationRegulationSchedule] (
    [GenerationRegulationScheduleID] INT        IDENTITY (1, 1) NOT NULL,
    [GenerationSetID]                INT        NOT NULL,
    [UnitID]                         INT        NOT NULL,
    [EffectiveDate]                  DATETIME   NOT NULL,
    [TimeInRegulation_fraction]      FLOAT (53) NOT NULL,
    PRIMARY KEY CLUSTERED ([GenerationRegulationScheduleID] ASC),
    CONSTRAINT [FK_tRegulationSchedule_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tRegulationSchedule_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Specifies a schedule of when the unit is in regulation that is overlaid over the hourly generation data.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationRegulationSchedule';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tGenerationRegulationSchedule';

