﻿CREATE TABLE [Projection].[tCSAPRAlternateProjection] (
    [CSAPRAlternateProjectionID] INT        IDENTITY (1, 1) NOT NULL,
    [DataVersionID]              INT        NOT NULL,
    [UnitID]                     INT        NOT NULL,
    [Year]                       INT        NOT NULL,
    [Month]                      INT        NOT NULL,
    [SO2_ton]                    FLOAT (53) NOT NULL,
    [NOX_ton]                    FLOAT (53) NOT NULL,
    [NetGeneration_MWh]          FLOAT (53) NOT NULL,
    CONSTRAINT [PK_tCSAPRAlternateProjection] PRIMARY KEY CLUSTERED ([CSAPRAlternateProjectionID] ASC),
    CONSTRAINT [FK_tCSAPRAlternateProjection_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID]),
    CONSTRAINT [FK_tCSAPRAlternateProjection_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);

