CREATE TABLE [Projection].[tCQIResultBoilerPerformance] (
    [CQIResultBoilerPerformanceID]       INT           IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                        INT           NOT NULL,
    [SlaggingIndex]                      NVARCHAR (50) NOT NULL,
    [SlaggingMargin_percent]             REAL          NOT NULL,
    [FoulingIndex]                       NVARCHAR (50) NOT NULL,
    [FoulingMargin_percent]              REAL          NOT NULL,
    [ErosionPotential]                   REAL          NOT NULL,
    [CycloneAcceptability]               NVARCHAR (50) NOT NULL,
    [FurnaceSootblowsPerDay]             REAL          NOT NULL,
    [UpperFurnaceSootblowsPerDay]        REAL          NOT NULL,
    [PendentSootblowsPerDay]             REAL          NOT NULL,
    [ConvectiveSootblowsPerDay]          REAL          NOT NULL,
    [ExcessO2_percent]                   REAL          NOT NULL,
    [BoilerEfficiency_percent]           REAL          NOT NULL,
    [TemperatureAtEconomizerExit_f]      REAL          NOT NULL,
    [FurnaceExitGasTemperature_f]        REAL          NOT NULL,
    [CycloneAcceptabilityMargin_percent] REAL          NOT NULL,
    [BoilerNOxRate_lbmmbtu]              REAL          NOT NULL,
    [BoilerEfficiencyLHV_percent]        REAL          CONSTRAINT [DF_tCQIResultBoilerPerformance_BoilerEfficiencyLHV_percent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCQIResultBoilerPerformance] PRIMARY KEY CLUSTERED ([CQIResultBoilerPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultBoilerPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX IDX_tCQIResultBoilerPerformance_CQIResultID
ON [Projection].[tCQIResultBoilerPerformance] ([CQIResultID])
INCLUDE ([SlaggingIndex],[SlaggingMargin_percent],[FoulingIndex],[FoulingMargin_percent],[ErosionPotential],[CycloneAcceptability],[FurnaceSootblowsPerDay],[UpperFurnaceSootblowsPerDay],[PendentSootblowsPerDay],[ConvectiveSootblowsPerDay])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultBoilerPerformance';

