CREATE TABLE [Projection].[tFanType] (
    [FanTypeID]     INT            NOT NULL,
    [FanTypeDesc]   NVARCHAR (255) NOT NULL,
    [FanTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_tFanType] PRIMARY KEY CLUSTERED ([FanTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFanType';

