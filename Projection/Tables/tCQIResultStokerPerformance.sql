﻿CREATE TABLE [Projection].[tCQIResultStokerPerformance] (
    [CQIResultStokerPerformanceID]      INT           IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                       INT           NOT NULL,
    [StokerAcceptability]               NVARCHAR (50) NOT NULL,
    [StokerAcceptabilityMargin_percent] REAL          CONSTRAINT [DF_tCQIResultStokerPerformance_StokerAcceptabilityMargin_percent] DEFAULT ((0)) NOT NULL,
    [GrateReleaseReq_kBtuhrft2]         REAL          DEFAULT ((0)) NOT NULL,
    [GrateReleaseAvail_kBtuhrft2]       REAL          DEFAULT ((0)) NOT NULL,
    [GrateBurnRateReq_lbmhrft2]         REAL          DEFAULT ((0)) NOT NULL,
    [GrateBurnRateAvail_lbmhrft2]       REAL          DEFAULT ((0)) NOT NULL,
    [FeederThroughputReq_tonhr]         REAL          DEFAULT ((0)) NOT NULL,
    [FeederThroughputAvail_tonhr]       REAL          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [FK_tCQIResultStokerPerformance_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultStokerPerformance';

