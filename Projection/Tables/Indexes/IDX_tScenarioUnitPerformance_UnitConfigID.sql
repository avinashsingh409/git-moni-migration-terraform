﻿CREATE NONCLUSTERED INDEX [IDX_tScenarioUnitPerformance_UnitConfigID] ON [Projection].[tScenarioUnitPerformance] 
(
      [UnitConfigID] ASC
)