﻿CREATE NONCLUSTERED INDEX [IDX_tScenarioFuelQuality_Aggregate]
    ON [Projection].[tScenarioFuelQuality]([ScenarioID] ASC)
    INCLUDE([FuelQualityID], [UnitID], [Year], [Month], [Day], [HHV_btulbm], [Sulfur_percent]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

