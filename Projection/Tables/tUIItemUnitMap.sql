﻿CREATE TABLE [Projection].[tUIItemUnitMap] (
    [UIItemUnitMapID] INT IDENTITY (1, 1) NOT NULL,
    [UIItemID]        INT NOT NULL,
    [UnitID]          INT NOT NULL,
	[AppContextID]	  INT NOT NULL,
    CONSTRAINT [PK_UIItemUnitMapID] PRIMARY KEY CLUSTERED ([UIItemUnitMapID] ASC),
    CONSTRAINT [FK_tUIItemUnitMap_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUIItemUnitMap_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE,
	CONSTRAINT [FK_Projection_tUIItemUnitMap_UIConfig_tAppContext] FOREIGN KEY([AppContextID]) REFERENCES [UIConfig].[tAppContext] ([AppContextID]) ON DELETE CASCADE
);

