CREATE TABLE [Projection].[tUnitConfigScheduleSet] (
    [UnitConfigScheduleSetID]     INT            IDENTITY (1, 1) NOT NULL,
    [UnitConfigScheduleSetDesc]   NVARCHAR (255) NOT NULL,
    [DataVersionID]               INT            NULL,
    [PlanYear]                    SMALLINT       CONSTRAINT [DF__tUnitConfi__Year__731B1205] DEFAULT (datepart(year,getdate())) NOT NULL,
    [IsConsensus]                 BIT            CONSTRAINT [DF__tUnitConf__IsCon__740F363E] DEFAULT ((0)) NOT NULL,
    [IsSensitivity]               BIT            CONSTRAINT [DF_tUnitConfigScheduleSet_IsSensitivity] DEFAULT ((0)) NOT NULL,
    [Comment]                     NVARCHAR (255) NULL,
    [CreatedBy]                   NVARCHAR (255) NOT NULL,
    [ChangedBy]                   NVARCHAR (255) NOT NULL,
    [CreateDate]                  DATETIME       CONSTRAINT [DF__tUnitConf__Creat__75035A77] DEFAULT (getdate()) NOT NULL,
    [SensitivityImpactTypeID]     INT            NULL,
    [ModifiedDate]                DATETIME       CONSTRAINT [DF_tUnitConfigScheduleSet_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                  DATETIME       DEFAULT (getdate()) NOT NULL,
    [ImportDate]                  DATETIME       NULL,
    [IsPublic]                    BIT            CONSTRAINT [DF_tUnitConfigScheduleSet_IsPublic] DEFAULT ((0)) NOT NULL,
    [IsValid]                     BIT            DEFAULT ((1)) NOT NULL,
    [IsArchived]                  BIT            CONSTRAINT [DF_tUnitConfigScheduleSet_IsArchived] DEFAULT ((0)) NOT NULL,
    [OwningAssetID]               INT            NULL,
    [UnitConfigScheduleSetAbbrev] NVARCHAR (50)  CONSTRAINT [UnitConfigScheduleSetAbbrevDefault] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_tUnitConfigScheduleSet] PRIMARY KEY CLUSTERED ([UnitConfigScheduleSetID] ASC),
    CONSTRAINT [FK_Projection_tUnitConfigScheduleSet_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tUnitConfigScheduleSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID]),
    CONSTRAINT [FK_tUnitConfigScheduleSet_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of unit configurations over time.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitConfigScheduleSet';

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitConfigScheduleSet';

