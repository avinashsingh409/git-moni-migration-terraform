CREATE TABLE [Projection].[tCQIResultMillPerformance] (
    [CQIResultMillPerformanceID]      INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                     INT  NOT NULL,
    [ThroughputRequired_tonhr]        REAL NOT NULL,
    [ThroughputAvailable_tonhr]       REAL NOT NULL,
    [PowerRequired_hp]                REAL NOT NULL,
    [PowerAvailable_hp]               REAL NOT NULL,
    [FeederThroughputRequired_tonhr]  REAL NOT NULL,
    [FeederThroughputAvailable_tonhr] REAL NOT NULL,
    [InletTemperature_f]              REAL NOT NULL,
    [OutletTemperature_f]             REAL NOT NULL,
    [MinimumOutletTemperature_f]      REAL NOT NULL,
    [NumberRequired]                  INT  NOT NULL,
    [NumberAvailable]                 INT  NOT NULL,
    CONSTRAINT [PK_tCQIResultMillPerformance] PRIMARY KEY CLUSTERED ([CQIResultMillPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultMillPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX IDX_tCQIResultMillPerformance_CQIResultID
ON [Projection].[tCQIResultMillPerformance] ([CQIResultID])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultMillPerformance';

