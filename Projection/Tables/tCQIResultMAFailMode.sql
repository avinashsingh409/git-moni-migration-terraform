﻿--Create table for storing Vista MA Failure Mode Results
CREATE TABLE [Projection].[tCQIResultMAFailMode] (
    [CQIResultMAFailModeID]   INT          IDENTITY (1, 1) NOT NULL,
    [CQIResultID]             INT          NOT NULL,
    [SystemID]                INT          NOT NULL,
    [SubSysID]                INT          NOT NULL,
    [CompID]                  INT          NOT NULL,
    [FailureModeID]           INT          NOT NULL,
    [FailureModeDescription]  VARCHAR (50) NULL,
    [MeanTimeBetweenFailures] FLOAT (53)   NULL,
    [ScheduledOutages]        FLOAT (53)   NULL,
    [ForcedOutages]           FLOAT (53)   NULL,
    [CTTR_hours]              FLOAT (53)   NULL,
    CONSTRAINT [PK_tCQIResultMAFailMode] PRIMARY KEY CLUSTERED ([CQIResultMAFailModeID] ASC),
    CONSTRAINT [FK_tCQIResultMAFailMode_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);










GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultMAFailMode';


GO
CREATE NONCLUSTERED INDEX [IDX_CQIResultMAFailMode]
    ON [Projection].[tCQIResultMAFailMode]([CQIResultID] ASC)
    INCLUDE([SystemID], [SubSysID], [CompID], [FailureModeID]);

