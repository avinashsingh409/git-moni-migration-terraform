﻿CREATE TABLE [Projection].[tEmissionRollingWindowData](
	[EmissionRollingDataID] [int] NOT NULL,
	[DateWithinWindow] [date] NOT NULL,
	[DailySum] [float] NULL,
	[SummedHours] [tinyint] NOT NUll
CONSTRAINT [PK_Scenario_tEmissionRollingWindowData] PRIMARY KEY CLUSTERED 
(
	[EmissionRollingDataID] ASC,
	[DateWithinWindow] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Projection].[tEmissionRollingWindowData]  ADD  CONSTRAINT [FK_Scenario_tEmissionRollingWindowData_tEmissionRollingData] FOREIGN KEY([EmissionRollingDataID])
REFERENCES [Projection].[tEmissionRollingData] ([EmissionRollingDataID])
ON DELETE CASCADE
GO

ALTER TABLE [Projection].[tEmissionRollingWindowData] CHECK CONSTRAINT [FK_Scenario_tEmissionRollingWindowData_tEmissionRollingData]
GO