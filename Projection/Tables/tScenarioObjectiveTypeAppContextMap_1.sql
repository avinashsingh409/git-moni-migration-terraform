﻿CREATE TABLE [Projection].[tScenarioObjectiveTypeAppContextMap] (
    [ScenarioObjectiveTypeAppContextMapID] INT IDENTITY (1, 1) NOT NULL,
    [ScenarioObjectiveTypeID]              INT NOT NULL,
    [AppContextID]                         INT NOT NULL,
    CONSTRAINT [PK_tScenarioObjectiveTypeAppContextMap] PRIMARY KEY CLUSTERED ([ScenarioObjectiveTypeAppContextMapID] ASC),
    CONSTRAINT [FK_tScenarioObjectiveTypeAppContextMap_tAppContext] FOREIGN KEY ([AppContextID]) REFERENCES [UIConfig].[tAppContext] ([AppContextID]),
    CONSTRAINT [FK_tScenarioObjectiveTypeAppContextMap_tScenarioObjectiveType] FOREIGN KEY ([ScenarioObjectiveTypeID]) REFERENCES [Projection].[tScenarioObjectiveType] ([ScenarioObjectiveTypeID])
);

