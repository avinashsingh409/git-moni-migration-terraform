﻿CREATE TABLE [Projection].[tAdjustmentDetail] (
    [AdjustmentDetailID] INT      IDENTITY (1, 1) NOT NULL,
    [AdjustmentID]       INT      NOT NULL,
    [Value]              REAL     NOT NULL,
    [EffectiveDate]      DATETIME NOT NULL,
    [Duration]           DATETIME NOT NULL,
    CONSTRAINT [PK_tAdjustmentDetail] PRIMARY KEY CLUSTERED ([AdjustmentDetailID] ASC),
    CONSTRAINT [FK_tAdjustmentDetail_tAdjustment] FOREIGN KEY ([AdjustmentID]) REFERENCES [Projection].[tAdjustment] ([AdjustmentID])
);

