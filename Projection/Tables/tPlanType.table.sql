CREATE TABLE [Projection].[tPlanType] (
    [PlanTypeID]   INT            NOT NULL,
    [PlanTypeDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tPlanType] PRIMARY KEY CLUSTERED ([PlanTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Look-up table providing different input data plan types.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tPlanType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tPlanType';

