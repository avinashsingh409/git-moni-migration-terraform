﻿CREATE TABLE [Projection].[tCQILoadBucket] (
    [CQILoadBucketID] INT IDENTITY (1, 1) NOT NULL,
    [UnitID]          INT NOT NULL,
    [LoadPercent]     INT NOT NULL,
    CONSTRAINT [PK_tCQILoadBucket] PRIMARY KEY CLUSTERED ([CQILoadBucketID] ASC),
    CONSTRAINT [FK_tCQILoadBucket_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Used by the Coal Quality Impact calculations to specify the loads the calculations should be run at for each unit.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQILoadBucket';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQILoadBucket';

