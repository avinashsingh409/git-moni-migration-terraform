CREATE TABLE [Projection].[tScenarioRoleMap] (
    [ScenarioID]     INT NOT NULL,
    [SecurityRoleID] INT NOT NULL,
    CONSTRAINT [PK_tScenarioRoleMap] PRIMARY KEY CLUSTERED ([ScenarioID] ASC, [SecurityRoleID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenarioRoleMap';

