﻿CREATE TABLE [Projection].[tCQIResultCFBPerformance] (
    [CQIResultCFBPerformanceID]           INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                         INT  NOT NULL,
    [FuelFeederThroughputReq_tonhr]       REAL NOT NULL,
    [FuelFeederThroughputAvail_tonhr]     REAL NOT NULL,
    [FuelMillThroughputReq_tonhr]         REAL NOT NULL,
    [FuelMillThroughputAvail_tonhr]       REAL NOT NULL,
    [AdditiveFeederThroughputReq_tonhr]   REAL NOT NULL,
    [AdditiveFeederThroughputAvail_tonhr] REAL NOT NULL,
    [AdditiveMillThroughputReq_tonhr]     REAL NOT NULL,
    [AdditiveMillThroughputAvail_tonhr]   REAL NOT NULL,
    [AdditiveBlowerFlowReq_cfm]           REAL NOT NULL,
    [AdditiveBlowerFlowAvail_cfm]         REAL NOT NULL,
    [AdditiveBlowerPressureReq_inwg]      REAL NOT NULL,
    [AdditiveBlowerPressureAvail_inwg]    REAL NOT NULL,
    [AdditiveBlowerPowerReq_hp]           REAL NOT NULL,
    [AdditiveBlowerPowerAvail_hp]         REAL NOT NULL,
    [CycloneCapacityReq_tonhr]            REAL NOT NULL,
    [CycloneCapacityAvail_tonhr]          REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultCFBPerformance] PRIMARY KEY CLUSTERED ([CQIResultCFBPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultCFBPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tCQIResultCFBPerformance';

