CREATE TABLE [Projection].[tConfidenceType] (
    [ConfidenceTypeID]     INT            NOT NULL,
    [ConfidenceTypeDesc]   NVARCHAR (255) NOT NULL,
    [ConfidenceTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_tConfidenceType] PRIMARY KEY CLUSTERED ([ConfidenceTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tConfidenceType';

