CREATE TABLE [Projection].[tFuelChangeEvent] (
    [FuelChangeEventID] INT           IDENTITY (1, 1) NOT NULL,
    [FuelPlanSetID]     INT           NOT NULL,
    [UnitID]            INT           NOT NULL,
    [EventTime]         DATETIME2 (7) NOT NULL,
    [FuelSourceID]      INT           NOT NULL,
    CONSTRAINT [PK_tFuelChangeEvent] PRIMARY KEY CLUSTERED ([FuelChangeEventID] ASC),
    CONSTRAINT [FK_tFuelChangeEvent_FuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelChangeEvent_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [UK_tFuelChangeEvent] UNIQUE NONCLUSTERED ([FuelPlanSetID] ASC, [UnitID] ASC, [EventTime] ASC)
);





 


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fuel burn plan - fuel change events.' , @level0type=N'SCHEMA',@level0name=N'Projection', @level1type=N'TABLE',@level1name=N'tFuelChangeEvent'


GO
CREATE NONCLUSTERED INDEX [IDX_tFuelChangeEvent_FuelPlanSetID]
    ON [Projection].[tFuelChangeEvent]([FuelPlanSetID] ASC)
    INCLUDE([FuelChangeEventID], [UnitID], [EventTime], [FuelSourceID]);


GO
CREATE NONCLUSTERED INDEX [IDX_FuelChangeEvent_FuelPlanSetID]
    ON [Projection].[tFuelChangeEvent]([FuelPlanSetID] ASC)
    INCLUDE([FuelChangeEventID], [UnitID], [EventTime], [FuelSourceID]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tFuelChangeEvent';

