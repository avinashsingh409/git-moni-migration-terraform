﻿CREATE TABLE [Projection].[tSolarPerformance] (
    [SolarPerformanceID]    INT      IDENTITY (1, 1) NOT NULL,
    [SolarPerformanceSetID] INT      NOT NULL,
    [AssetID]               INT      NOT NULL,
    [TimeStamp]             DATETIME NULL,
    [TypicalPowerOutput]    REAL     NOT NULL,
    [TypicalPOAIrradiance]  REAL     NOT NULL,
    [ForecastPowerOutput]   REAL     NULL,
    CONSTRAINT [PK_tSolarPerformanceID] PRIMARY KEY CLUSTERED ([SolarPerformanceID] ASC),
    CONSTRAINT [FK_tSolarPerformance_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSolarPerformance_tSolarPerformanceSet] FOREIGN KEY ([SolarPerformanceSetID]) REFERENCES [Projection].[tSolarPerformanceSet] ([SolarPerformanceSetID])
);




GO
CREATE INDEX [IX_SolarPerformanceSetID_TimeStamp_Includes] ON [Projection].[tSolarPerformance] ([SolarPerformanceSetID], [TimeStamp])  INCLUDE ([AssetID]) WITH (FILLFACTOR=100);

GO
CREATE INDEX [IX_SolarPerformanceSetID_AssetID_TimeStamp_Includes] ON [Projection].[tSolarPerformance] ([SolarPerformanceSetID], [AssetID], [TimeStamp])  INCLUDE ([TypicalPOAIrradiance], [TypicalPowerOutput]) WITH (FILLFACTOR=100);

GO
