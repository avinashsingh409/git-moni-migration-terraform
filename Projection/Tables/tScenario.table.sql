﻿CREATE TABLE [Projection].[tScenario] (
    [ScenarioID]                         INT            IDENTITY (1, 1) NOT NULL,
    [ScenarioDesc]                       NVARCHAR (255) NOT NULL,
    [DataVersionID]                      INT            NOT NULL,
    [PlanYear]                           SMALLINT       CONSTRAINT [DF__tScenario__Year__63A3C44B] DEFAULT (datepart(year,getdate())) NOT NULL,
    [IsConsensus]                        BIT            CONSTRAINT [DF__tScenario__IsCon__6497E884] DEFAULT ((0)) NOT NULL,
    [IsSensitivity]                      BIT            CONSTRAINT [DF_tScenario_IsSensitivity] DEFAULT ((0)) NOT NULL,
    [SensitivityTypeID]                  INT            NULL,
    [GenerationSetID]                    INT            NOT NULL,
    [FuelPlanSetID]                      INT            NOT NULL,
    [UnitConfigScheduleSetID]            INT            NOT NULL,
    [CreatedBy]                          NVARCHAR (255) NOT NULL,
    [ChangedBy]                          NVARCHAR (255) NOT NULL,
    [CreateDate]                         DATETIME       CONSTRAINT [DF__tScenario__Creat__695C9DA1] DEFAULT (getdate()) NOT NULL,
    [SensitivityImpactTypeID]            INT            NULL,
    [RunDate]                            DATETIME       NULL,
    [RunStatus]                          TINYINT        NULL,
    [IsModified]                         BIT            CONSTRAINT [DF_tScenario_IsModified] DEFAULT ((0)) NOT NULL,
    [ChangeDate]                         DATETIME       DEFAULT (getdate()) NOT NULL,
    [IsPublic]                           BIT            CONSTRAINT [DF_tScenario_IsPublic] DEFAULT ((0)) NOT NULL,
    [ForecastSetID]                      INT            NULL,
    [IsArchived]                         BIT            CONSTRAINT [DF_tScenario_IsArchived] DEFAULT ((0)) NOT NULL,
    [MaintenancePlanSetID]               INT            NULL,
    [ScenarioComment]                    NVARCHAR (MAX) NULL,
    [MetricsCalcDate]                    DATETIME       NULL,
    [UseDeratedPower]                    BIT            DEFAULT ((0)) NOT NULL,
    [DemandSetID]                        INT            NULL,
    [OwningAssetID]                      INT            NULL,
    [RecalculateMaintenancePlan]         BIT            DEFAULT ((1)) NOT NULL,
    [ElimNegMarginHrs]                   BIT            DEFAULT ((0)) NOT NULL,
    [ParentScenarioID]                   INT            NULL,
    [UseVistaMTBFAdj]                    BIT            CONSTRAINT [DF_tScenario_UseVistaMTBFAdj] DEFAULT ((0)) NOT NULL,
    [CalcRateBasedEmis]                  BIT            CONSTRAINT [DF_tScenario_CalcRateBasedEmis] DEFAULT ((1)) NOT NULL,
    [UseStartAndEndDate]                 BIT            DEFAULT ((0)) NOT NULL,
    [StartDate]                          DATETIME2 (7)  NULL,
    [EndDate]                            DATETIME2 (7)  NULL,
    [OmitAvailabilityCalcs]              BIT            DEFAULT ((0)) NOT NULL,
    [OmitEconomicCalcs]                  BIT            DEFAULT ((0)) NOT NULL,
    [ApplyUnavailability]                BIT            DEFAULT ((1)) NOT NULL,
    [ScenarioTypeID]                     INT            CONSTRAINT [DF_tScenario_ScenarioType] DEFAULT ((1)) NOT NULL,
    [ENMHParentScenarioID]               INT            NULL,
    [BaseUnitConfigScheduleSetID]        INT            NOT NULL,
    [BaseGenerationSetID]                INT            NOT NULL,
    [DemandIDsAreInherited]              BIT            CONSTRAINT [DF_tScenario_DemandIDsAreInherited] DEFAULT ((1)) NOT NULL,
    [ForecastSetIDIsInherited]           BIT            CONSTRAINT [DF_tScenario_ForecastSetIDIsInherited] DEFAULT ((1)) NOT NULL,
    [FuelPlanSetIDIsInherited]           BIT            CONSTRAINT [DF_tScenario_FuelPlanSetIDIsInherited] DEFAULT ((1)) NOT NULL,
    [MaintenancePlanSetIDIsInherited]    BIT            CONSTRAINT [DF_tScenario_MaintenancePlanSetIDIsInherited] DEFAULT ((1)) NOT NULL,
    [UnitConfigScheduleSetIDIsInherited] BIT            CONSTRAINT [DF_tScenario_UnitConfigScheduleSetIDIsInherited] DEFAULT ((1)) NOT NULL,
    [InheritanceChanged]                 BIT            CONSTRAINT [DF_tScenario_InheritanceChanged] DEFAULT ((0)) NULL,
    [BaseFuelPlanSetID]                  INT            NOT NULL,
    CONSTRAINT [PK_tScenario] PRIMARY KEY CLUSTERED ([ScenarioID] ASC),
    FOREIGN KEY ([ScenarioTypeID]) REFERENCES [Projection].[tScenarioType] ([ScenarioTypeID]),
    CONSTRAINT [FK_Projection_tScenario_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tScenario_BaseFuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([BaseFuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]),
    CONSTRAINT [FK_tScenario_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID]),
    CONSTRAINT [FK_tScenario_DemandSetID_tGenerationSet] FOREIGN KEY ([DemandSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]),
    CONSTRAINT [FK_tScenario_ForecastSetID_tForecastSet] FOREIGN KEY ([ForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID]),
    CONSTRAINT [FK_tScenario_FuelBurnSetID_tFuelBurnSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]),
    CONSTRAINT [FK_tScenario_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]),
    CONSTRAINT [FK_tScenario_ParentScenarioID_tScenario] FOREIGN KEY ([ParentScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]),
    CONSTRAINT [FK_tScenario_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID]),
    CONSTRAINT [FK_tScenario_SensitivityTypeID_tSensitivityType] FOREIGN KEY ([SensitivityTypeID]) REFERENCES [Projection].[tSensitivityType] ([SensitivityTypeID]),
    CONSTRAINT [FK_tScenario_tGenerationSet_BaseGenerationSetID] FOREIGN KEY ([BaseGenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]),
    CONSTRAINT [FK_tScenario_tUnitConfigScheduleSet] FOREIGN KEY ([BaseUnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]),
    CONSTRAINT [FK_tScenario_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]),
    CONSTRAINT [FK_tScenarioMaintenancePlanSetID_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE SET NULL
);






















GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User defined collection of data sets that defines a specific plan.', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenario';


GO
--would like to make this ON DELETE CASCADE, but it won't let me--apparently having two foreign keys in one table against the same field can cause cycles if it cascades at all.

GO


GO

GO


CREATE TRIGGER [Projection].[tScenario_CheckForMaintenancePlanChange]
   ON  [Projection].[tScenario]
   FOR UPDATE
AS
BEGIN    
	--old rows are copied to DELETED first, then new rows are added to both the target 
	--table and INSERTED.  So, if I do a primary key join between the target table and
	--the DELETED table, I should be able to compare new maintenance plan IDs with old.
	--If they're different, and the new ID isn't null, I need to set IsModified to 1.
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 1
	where ScenarioID in
	(
		Select a.ScenarioID 
		from [Projection].[tScenario] a join DELETED as b
		on a.ScenarioID = b.ScenarioID
		where a.MaintenancePlanSetID != b.MaintenancePlanSetID
		and a.MaintenancePlanSetID is not null
	)
	
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 0
	where ScenarioID in
	(
		Select a.ScenarioID 
		from [Projection].[tScenario] a join DELETED as b
		on a.ScenarioID = b.ScenarioID
		where a.MaintenancePlanSetID != b.MaintenancePlanSetID
		and a.MaintenancePlanSetID is null
	)
	
	--if UseVistaMTBFAdj has changed, that also justifies recalculating the plan.
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 1
	where ScenarioID in
	(
		Select a.ScenarioID 
		from [Projection].[tScenario] a join DELETED as b
		on a.ScenarioID = b.ScenarioID
		where a.UseVistaMTBFAdj != b.UseVistaMTBFAdj
	)
	
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 1
	where ScenarioID in
	(
		Select a.ScenarioID 
		from [Projection].[tScenario] a join INSERTED as b
		on a.ScenarioID = b.ScenarioID
		where a.UseVistaMTBFAdj != b.UseVistaMTBFAdj
	)
	
	--If the changed scenario is a partial scenario, and the start or end date changed,
	--that also will require a recalculation of the maintenance plan (if it doesn't
	--omit availability calcs, anyway).
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 1
	where ScenarioID in
	(
		Select a.ScenarioID 
		from [Projection].[tScenario] a join INSERTED as b
		on a.ScenarioID = b.ScenarioID
		where a.UseStartAndEndDate = 1
		and a.OmitAvailabilityCalcs = 0
		and a.MaintenancePlanSetID is not null
		and 
		(
			a.StartDate != b.StartDate 
			or a.EndDate != b.EndDate
		)
	)
	
END
;
GO


GO
CREATE INDEX IDX_tScenario_ParentScenarioID ON Projection.tScenario (ParentScenarioID);
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tScenario';

