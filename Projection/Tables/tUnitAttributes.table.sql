CREATE TABLE [Projection].[tUnitAttributes] (
    [UnitID]       INT NOT NULL,
    [FuelSourceID] INT NOT NULL,
    CONSTRAINT [PK_tUnitAttributes] PRIMARY KEY CLUSTERED ([UnitID] ASC),
    CONSTRAINT [FK_Projection_tUnitAttributes_FuelSourceID_Fuel_FuelSource_FuelSourceID] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_Projection_tUnitAttributes_UnitID_Asset_tUnit_UnitID] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Projection', @level1type = N'TABLE', @level1name = N'tUnitAttributes';

