﻿CREATE TYPE [Projection].[tpSolarPerformance] AS TABLE (
    [SolarPerformanceSetID] INT        NULL,
    [AssetID]               INT        NULL,
    [WeatherAssetID]        INT        NULL,
    [StartDate]             DATETIME   NULL,
    [ForecastPowerOutput]   FLOAT (53) NULL);




GO
GRANT EXECUTE
    ON TYPE::[Projection].[tpSolarPerformance] TO [TEUser];



