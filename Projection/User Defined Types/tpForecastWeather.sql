﻿CREATE TYPE [Projection].[tpForecastWeather] AS TABLE (
    [WeatherAssetID]              INT            NULL,
    [TimeStamp]                   DATETIME       NULL,
    [DailyMax_degF]               FLOAT (53)     NULL,
    [DailyMin_degF]               FLOAT (53)     NULL,
    [Temperature_degF]            FLOAT (53)     NULL,
    [Precipitation_in]            FLOAT (53)     NULL,
    [CloudCover_pct]              FLOAT (53)     NULL,
    [WindSpeed_MPH]               FLOAT (53)     NULL,
    [WindDirection_deg]           FLOAT (53)     NULL,
    [WindSpeedGust_MPH]           FLOAT (53)     NULL,
    [RelativeHumidity_pct]        FLOAT (53)     NULL,
    [DewPointTemperature_degF]    FLOAT (53)     NULL,
    [WeatherIcon]                 NVARCHAR (MAX) NULL,
    [MaxRelativeHumidity_pct]     FLOAT (53)     NULL,
    [MinRelativeHumidity_pct]     FLOAT (53)     NULL,
    [ApparentTemperature_degF]    FLOAT (53)     NULL,
    [Precipitation12HourProb_pct] FLOAT (53)     NULL,
    [Snowfall_in]                 FLOAT (53)     NULL,
    [IceAccumulation_in]          FLOAT (53)     NULL);






GO
GRANT EXECUTE
    ON TYPE::[Projection].[tpForecastWeather] TO [TEUser];



