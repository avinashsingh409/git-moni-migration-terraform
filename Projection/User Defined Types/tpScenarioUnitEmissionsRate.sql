﻿CREATE TYPE [Projection].[tpScenarioUnitEmissionsRate] AS TABLE (
    [UnitID]    INT            NULL,
    [UIItemID]  INT            NOT NULL,
    [StartDate] DATETIME       NOT NULL,
    [Rate]      REAL           NOT NULL,
    [Comment]   NVARCHAR (MAX) NULL);




GO
GRANT EXECUTE
    ON TYPE::[Projection].[tpScenarioUnitEmissionsRate] TO [TEUser];

