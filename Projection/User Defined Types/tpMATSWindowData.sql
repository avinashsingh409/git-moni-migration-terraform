﻿/****** Object:  UserDefinedTableType [Projection].[tpMATSData]    Script Date: 04/13/2015 14:42:00 ******/
CREATE TYPE [Projection].[tpMATSWindowData] AS TABLE(
	[EndOfWindow] [date] NOT NULL,
	[DateWithinWindow] [date] NOT NULL,
	[DailySum] [float] NULL,
	[SummedHours] [tinyint] NOT NUll
)