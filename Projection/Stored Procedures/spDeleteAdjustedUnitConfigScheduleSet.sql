-- =============================================
-- Author:		Irwin, Ryan
-- Create date: 2015-05-07
-- Description:	Deletes a constructed unit config
-- schedule set, presumably built from a base set
-- then altered with unit equipment adjustments.
-- This deletes all the adjustment-derived UnitConfig
-- and UnitEquipmentStateSets/Details, along with
-- all of the UnitConfigScheduleDetails, and the
-- UnitConfigScheduleSet itself.
-- =============================================
CREATE PROCEDURE [Projection].[spDeleteAdjustedUnitConfigScheduleSet]
	-- Add the parameters for the stored procedure here
	@UnitConfigScheduleSetID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @UCIDs as TABLE 
	(
		UnitConfigID int
	);
	DECLARE @UESSIDs as TABLE
	(
		UnitEquipmentStateSetID int
	);

	-- Insert statements for procedure here
	INSERT INTO @UCIDs
	SELECT uc.UnitConfigID FROM Projection.tUnitConfigScheduleDetail ucsd
	INNER JOIN Projection.tUnitConfig uc ON ucsd.UnitConfigID = uc.UnitConfigID
	WHERE ucsd.UnitConfigScheduleSetID = @UnitConfigScheduleSetID
	AND uc.BuiltFromAdjustment = 1;
	
	INSERT INTO @UESSIDs
	SELECT uess.UnitEquipmentStateSetID FROM Projection.tUnitEquipmentStateSet uess
	INNER JOIN Projection.tUnitConfig uc ON uess.UnitEquipmentStateSetID = uc.UnitEquipmentStateSetID
	INNER JOIN @UCIDs ucids on uc.UnitConfigID = ucids.UnitConfigID
	WHERE uess.BuiltFromAdjustment = 1;
	
	BEGIN TRANSACTION
	
	DELETE FROM Projection.tUnitEquipmentStateDetail
	WHERE UnitEquipmentStateSetID IN (SELECT UnitEquipmentStateSetID FROM @UESSIDs);
	
	DELETE FROM Projection.tUnitConfigScheduleDetail
	WHERE UnitConfigScheduleSetID = @UnitConfigScheduleSetID;
	
	DELETE FROM Projection.tCQIResult
	WHERE UnitConfigID in (SELECT UnitConfigID FROM @UCIDs);
	
	DELETE FROM Projection.tScenarioFuelBurnSchedule
	WHERE UnitConfigID in (SELECT UnitConfigID from @UCIDs);
	
	DELETE FROM Projection.tScenarioFuelQuality
	WHERE UnitConfigID in (SELECT UnitConfigID from @UCIDs);
	
	DELETE FROM Projection.tScenarioMargin
	WHERE UnitConfigID in (SELECT UnitConfigID from @UCIDs);
	
	DELETE FROM Projection.tScenarioUnitPerformance
	WHERE UnitConfigID in (SELECT UnitConfigID from @UCIDs);
	
	DELETE FROM Projection.tUnitConfig
	WHERE UnitConfigID in (SELECT UnitConfigID FROM @UCIDs);
	
	DELETE FROM Projection.tUnitEquipmentStateSet
	WHERE UnitEquipmentStateSetID IN (SELECT UnitEquipmentStateSetID FROM @UESSIDs);
	
	DELETE FROM Projection.tUnitConfigScheduleSet
	WHERE UnitConfigScheduleSetID = @UnitConfigScheduleSetID;
	
	COMMIT TRANSACTION
END
;
GO


GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteAdjustedUnitConfigScheduleSet] TO [TEUser]
    AS [dbo];