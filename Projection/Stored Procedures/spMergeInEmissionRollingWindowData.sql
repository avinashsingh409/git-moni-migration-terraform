﻿




CREATE PROCEDURE [Projection].[spMergeInEmissionRollingWindowData]
	@ScenarioID [int],
	@UnitID [int],
	@UIItemID [int],
	@NewEmissionRollingWindowData Projection.[tpEmissionRollingWindowData] READONLY	
AS
BEGIN    		
	SELECT b.EmissionRollingDataID, a.DateWithinWindow, a.DailySum, a.SummedHours
	INTO #Temp
	FROM @NewEmissionRollingWindowData a INNER JOIN Projection.tEmissionRollingData b
	ON b.ScenarioID = @ScenarioID AND b.UIItemID = @UIItemID AND b.UnitID = @UnitID AND a.EndOfWindow = b.[Date]
		
	DELETE FROM [Projection].[tEmissionRollingWindowData] WHERE EmissionRollingDataID IN (SELECT EmissionRollingDataID FROM #Temp)
	INSERT INTO [Projection].[tEmissionRollingWindowData] SELECT * FROM #Temp	
	DROP TABLE #Temp;
END


GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spMergeInEmissionRollingWindowData] TO [TEUser]
    AS [dbo];

