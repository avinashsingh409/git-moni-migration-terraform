﻿
CREATE PROCEDURE [Projection].[spUpdateSolarPerformanceForecastedPower]
	@SolarPerf Projection.tpSolarPerformance READONLY
AS

	UPDATE Projection.vSolarPerformance
	   SET ForecastPowerOutput = sp.ForecastPowerOutput
	   FROM @SolarPerf sp,
			[Projection].[vSolarPerformance] vsp
	  WHERE (vsp.AssetID = sp.AssetID OR sp.AssetID IS NULL)
	    AND (vsp.WeatherAssetID = sp.WeatherAssetID OR sp.WeatherAssetID IS NULL)
	    AND vsp.[TimeStamp] = sp.StartDate 
	    AND vsp.SolarPerformanceSetID = sp.SolarPerformanceSetID
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spUpdateSolarPerformanceForecastedPower] TO [TEUser]
    AS [dbo];

