﻿CREATE PROCEDURE [Projection].[spUpsertGenAdjustmentMap]
	@GenerationAdjustmentMapID int = NULL,
	@GenerationSetID int,
	@GenerationAdjustmentSetID int,
	@NewID int OUTPUT
AS
BEGIN
	IF (@GenerationAdjustmentMapID IS NULL OR @GenerationAdjustmentMapID <= 0)
		BEGIN
		INSERT INTO Projection.tGenerationAdjustmentMap (GenerationSetID,GenerationAdjustmentSetID) 
			VALUES (@GenerationSetID,@GenerationAdjustmentSetID);
		
		SET @NewID = SCOPE_IDENTITY();
		END
	ELSE
		BEGIN
		UPDATE Projection.tGenerationAdjustmentMap SET GenerationSetID = @GenerationSetID, GenerationAdjustmentSetID = @GenerationAdjustmentSetID
			WHERE GenerationAdjustmentMapID = @GenerationAdjustmentMapID;
		 
		 SET @NewID = @GenerationAdjustmentMapID
		END
	SELECT @NewID
END
GO

GRANT EXECUTE
    ON OBJECT::[Projection].[spUpsertGenAdjustmentMap] TO [TEUser]
    AS [dbo];
GO
