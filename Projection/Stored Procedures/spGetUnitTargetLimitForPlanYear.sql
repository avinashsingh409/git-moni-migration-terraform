﻿
create proc Projection.spGetUnitTargetLimitForPlanYear
 @UnitID int,
 @PlanYear int,
 @UIItemID int
AS

select TOP 1 unitTargetSet.UITargetSetID, UnitID, UIItemID, uTD.UITargetTypeID,
StartDate, EndDate, Quantity, QuantityUnit,
Margin, MarginUnit, IsRegulatory, PlanYear
 from Projection.tUITargetUnitDetail uTD, Projection.tUITargetType unitTargetType,
 Projection.tUITargetSet unitTargetSet
 where uTD.UnitID = @UnitID
  and unitTargetSet.PlanYear = @PlanYear
  and uTD.UITargetTypeID =unitTargetType.UITargetTypeID
  and unitTargetType.UITargetTypeID = 3 
  and unitTargetSet.UITargetSetID = uTD.UITargetSetID
  and uTD.UIItemID = @UIItemID
  ORDER by StartDate desc
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetUnitTargetLimitForPlanYear] TO [TEUser]
    AS [dbo];

