﻿


CREATE PROCEDURE [Projection].[spDeleteAdjustmentDetailsByAdjustmentID]
	-- Add the parameters for the stored procedure here
	@adjustmentID int,
	@adjustmentDetailIDsToKeep Base.tpIntList READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ADNIDs as TABLE 
	(
		AdjustmentDetailNotesID int
	);
	DECLARE @ADIDs as TABLE
	(
		AdjustmentDetailID int
	);
	
	INSERT INTO @ADIDs
	SELECT tA.AdjustmentDetailID from Projection.tAdjustmentDetail tA
	WHERE tA.AdjustmentID = @adjustmentID 
	AND tA.AdjustmentDetailID not in (SELECT * From @adjustmentDetailIDsToKeep)
	
	INSERT INTO @ADNIDs
	SELECT tADN.AdjustmentDetailNotesID FROM Projection.tAdjustmentDetailNote tADN
	INNER JOIN @ADIDs tAD on tAD.AdjustmentDetailID = tADN.AdjustmentDetailID
	
	DELETE FROM Projection.tAdjustmentDetailNote
	WHERE AdjustmentDetailNotesID in (SELECT * FROM @ADNIDs)
	
	DELETE FROM Projection.tAdjustmentDetail
	WHERE AdjustmentDetailID in (SELECT * FROM @ADIDs)
	
END
;
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteAdjustmentDetailsByAdjustmentID] TO [TEUser]
    AS [dbo];

