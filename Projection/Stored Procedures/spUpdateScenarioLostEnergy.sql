﻿
--Update the Stored Procedure to user the renamed column
CREATE PROCEDURE [Projection].[spUpdateScenarioLostEnergy]

@ScenarioID int

AS

BEGIN

SET NOCOUNT ON;

DECLARE @UseDeratedPower as bit
select @UseDeratedPower = UseDeratedPower from Projection.tScenario where ScenarioID = @ScenarioID

update perf set LostEnergyFromNegMargin_mw = perf.Demand_mwh  from projection.tScenarioUnitPerformance perf 
join projection.tScenario child on perf.ScenarioID = child.ScenarioID
join projection.tScenario parent on child.ENMHParentScenarioID = parent.ScenarioID
join projection.tGenerationDetail_Adjusted childGen on child.GenerationSetID = childgen.GenerationSetID and 
perf.UnitID=childGen.unitid and perf.Year=childGen.year and perf.Month=childGen.Month and perf.Day=childGen.Day  and perf.Hour=childGen.Hour
join projection.tGenerationDetail_Adjusted parentGen on parent.GenerationSetID = parentGen.GenerationSetID and 
perf.UnitID=parentGen.unitid and perf.Year=parentGen.year and perf.Month=parentGen.Month and perf.Day=parentGen.Day  and perf.Hour=parentGen.Hour
where childGen.Generation_mw=0 and parentGen.Generation_mw<>0 and child.ScenarioID = @ScenarioID

update projection.tScenarioUnitPerformance set LostEnergyFromFuelDerate_mw = 
case when @UseDeratedPower=1 and MaxVistaNetPower_mw is not null and isnull(LostEnergyFromNegMargin_mw,0) = 0 and Demand_mwh > MaxVistaNetPower_mw then Demand_mwh - MaxVistaNetPower_mw else 0 end,

LostEnergyFromUnavailability_mw = 
(case when MaxVistaNetPower_mw is null or isnull(LostEnergyFromNegMargin_mw,0)>0 then 0 else 
case when (@UseDeratedPower = 0 or Demand_mwh<=MaxVistaNetPower_mw) then Demand_mwh-(NetGeneration_mwh+Deratedenergy_mwh) else 
case when MaxVistaNetPower_mw-(NetGeneration_mwh+Deratedenergy_mwh) > 0 then
MaxVistaNetPower_mw-(NetGeneration_mwh+Deratedenergy_mwh) else 0 end end end)
where ScenarioID=@ScenarioID
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spUpdateScenarioLostEnergy] TO [TEUser]
    AS [dbo];

