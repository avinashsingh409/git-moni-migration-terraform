﻿


CREATE PROCEDURE [Projection].[spDeleteAdjustmentSet]
	@adjustmentSet int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ADNIDs as TABLE 
	(
		AdjustmentDetailNotesID int
	);
	DECLARE @ADIDs as TABLE
	(
		AdjustmentDetailID int
	);
	DECLARE @AIDs as TABLE
	(
		AdjustmentID int
	);
	
	INSERT INTO @AIDs
	SELECT tA.AdjustmentID from Projection.tAdjustment tA
	WHERE tA.AdjustmentSetID = @adjustmentSet
	
	INSERT INTO @ADIDs
	SELECT tAD.AdjustmentDetailID FROM Projection.tAdjustmentDetail tAD
	INNER JOIN @AIDs tA on tAD.AdjustmentID = tA.AdjustmentID
	
	INSERT INTO @ADNIDs
	SELECT tADN.AdjustmentDetailNotesID FROM Projection.tAdjustmentDetailNote tADN
	INNER JOIN @ADIDs tAD on tAD.AdjustmentDetailID = tADN.AdjustmentDetailID
	
	DELETE FROM Projection.tAdjustmentDetailNote
	WHERE AdjustmentDetailNotesID in (SELECT * FROM @ADNIDs)
	
	DELETE FROM Projection.tAdjustmentDetail
	WHERE AdjustmentDetailID in (SELECT * FROM @ADIDs)
	
	DELETE FROM Projection.tAdjustment
	WHERE AdjustmentID in (SELECT * FROM @AIDs)
	
	DELETE FROM Projection.tAdjustmentSet
	WHERE AdjustmentSetID = @adjustmentSet
	
END
;
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteAdjustmentSet] TO [TEUser]
    AS [dbo];

