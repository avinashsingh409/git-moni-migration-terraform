﻿
CREATE PROCEDURE [Projection].[spMergeInEmissionRollingData]
	@ScenarioID [int],
	@UnitID [int],
	@UIItemID [int],
	@NewEmissionRollingData Projection.[tpEmissionRollingData] READONLY	
AS
BEGIN

    DELETE a FROM Projection.tEmissionRollingData a JOIN @NewEmissionRollingData b ON a.ScenarioID = b.ScenarioID and 
    a.UnitID= b.UnitID and a.UIItemID=b.UIItemID 
      AND a.Date=b.Date
    INSERT INTO Projection.tEmissionRollingData SELECT * FROM @NewEmissionRollingData
    
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spMergeInEmissionRollingData] TO [TEUser]
    AS [dbo];
