CREATE PROCEDURE PROJECTION.spUpsertInputType
@InputTypeID INT
,@InputTypeDesc VARCHAR(255)
,@InputTypeAbbrev VARCHAR(100)
,@VariableTypeID INT = NULL
,@AssetClassTypeID INT
,@ValueTypeID INT = NULL
,@InputTypeLegacyDomainID INT = NULL
AS
BEGIN

IF EXISTS(SELECT Projection.tInputType.InputTypeID FROM Projection.tInputType 
			WHERE Projection.tInputType.InputTypeID = @InputTypeID)
	BEGIN
	UPDATE [Projection].[tInputType]
	SET			
			   [InputTypeDesc] = @InputTypeDesc
			   ,[InputTypeAbbrev] = @InputTypeAbbrev
			   ,[VariableTypeID] = @VariableTypeID
			   ,[AssetClassTypeID] = @AssetClassTypeID
			   ,[ValueTypeID] = @ValueTypeID
			   ,[InputTypeLegacyDomainID] = @InputTypeLegacyDomainID
			   where [InputTypeID] = @InputTypeID
	END
	ELSE
	BEGIN
	INSERT INTO [Projection].[tInputType]
			   ([InputTypeID]
			   ,[InputTypeDesc]
			   ,[InputTypeAbbrev]
			   ,[VariableTypeID]
			   ,[AssetClassTypeID]
			   ,[ValueTypeID]
			   ,[InputTypeLegacyDomainID])
			VALUES
				(@InputTypeID
				,@InputTypeDesc
				,@InputTypeAbbrev
				,@VariableTypeID
				,@AssetClassTypeID
				,@ValueTypeID
				,@InputTypeLegacyDomainID)
	END
END
GO

GRANT EXECUTE
    ON OBJECT::[Projection].[spUpsertInputType] TO [TEUser]
    AS [dbo];
GO


