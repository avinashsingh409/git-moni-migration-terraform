﻿

CREATE PROCEDURE [Projection].[spGetMappedUIItemsForUnit]
	@UnitID int	
AS
BEGIN
	SELECT u.*, map.AppContextID from [Projection].[tUIItemUnitMap] map
	INNER JOIN [Projection].[tUIItem] u ON map.UIItemID = u.UIItemID
	WHERE map.UnitID = @UnitID
	ORDER BY u.DisplayOrder
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetMappedUIItemsForUnit] TO [TEUser]
    AS [dbo];

