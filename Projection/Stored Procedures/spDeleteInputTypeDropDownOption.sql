﻿CREATE PROCEDURE [Projection].[spDeleteInputTypeDropDownOption]
	@InputTypeDropDownOptionID int
AS
BEGIN
	DELETE FROM Projection.tInputTypeDropDownOption WHERE InputTypeDropDownOptionID = @InputTypeDropDownOptionID;
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteInputTypeDropDownOption] TO [TEUser]
    AS [dbo];

