﻿CREATE PROCEDURE [Projection].[spGetEmissionRollingData]
	@ScenarioID [int],
	@UnitID [int],
	@UIItemID [int],
	@FirstDate [date] = null,
	@LastDate [date] = null
AS
BEGIN

	IF @FirstDate IS NULL  
	BEGIN
		SELECT @FirstDate = MIN([Date]) from Projection.tEmissionRollingData 
	END
	
	IF @LastDate IS NULL  
	BEGIN
		SELECT @LastDate = MAX([Date]) from Projection.tEmissionRollingData 
	END

	SELECT [Date], Quantity FROM Projection.tEmissionRollingData
	WHERE ScenarioID = @ScenarioID 
	AND UnitID = @UnitID
	AND UIItemID = @UIItemID
	AND @FirstDate <= [Date]
	AND @LastDate >= [Date]
	ORDER BY [Date]
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetEmissionRollingData] TO [TEUser]
    AS [dbo];

