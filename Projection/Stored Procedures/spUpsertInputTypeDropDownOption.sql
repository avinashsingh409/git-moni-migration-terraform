﻿CREATE PROCEDURE [Projection].[spUpsertInputTypeDropDownOption]
	@InputTypeDropDownOptionID int = null,
	@InputTypeInputOptionTypeMapID int,
	@DropDownValue real,
	@DropDownDesc nvarchar(50),
	@NewInputTypeDropDownOptionID int OUTPUT
AS
BEGIN
	IF(@InputTypeDropDownOptionID IS NULL OR @InputTypeDropDownOptionID <= 0)
	BEGIN
		INSERT INTO Projection.tInputTypeDropDownOption(
			InputTypeInputOptionTypeMapID
			, DropDownValue
			, DropDownDesc
		)
		VALUES (
			@InputTypeInputOptionTypeMapID
			, @DropDownValue
			, @DropDownDesc
		)
		SET @NewInputTypeDropDownOptionID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE Projection.tInputTypeDropDownOption
		SET
			InputTypeInputOptionTypeMapID = @InputTypeInputOptionTypeMapID
			, DropDownValue = @DropDownValue
			, DropDownDesc = @DropDownDesc
		WHERE InputTypeDropDownOptionID = @InputTypeDropDownOptionID
		SET @NewInputTypeDropDownOptionID = @InputTypeDropDownOptionID
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spUpsertInputTypeDropDownOption] TO [TEUser]
    AS [dbo];

