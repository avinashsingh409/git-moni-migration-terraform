﻿



-- =============================================
-- Author:		Irwin, Ryan
-- Create date: 2015-06-09
-- Description:	Deletes all adjustment sets with
--  an OwningAssetID equal to the provided 
--  scenario ID, except for those in the provided
--  adjustment set ID list.
-- =============================================
CREATE PROCEDURE [Projection].[spDeleteAdjustmentSetsByScenarioID]
	-- Add the parameters for the stored procedure here
	@ScenarioID int,
	@AdjustmentSetIDsToKeep Base.tpIntList READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ADNIDs as TABLE 
	(
		AdjustmentDetailNotesID int
	);
	DECLARE @ADIDs as TABLE
	(
		AdjustmentDetailID int
	);
	DECLARE @AIDs as TABLE
	(
		AdjustmentID int
	);
	DECLARE @ASIDs as Table
	(
		AdjustmentSetID int
	);
	
	INSERT INTO @ASIDs
	SELECT tAS.AdjustmentSetID from Projection.tAdjustmentSet tAS
	WHERE tAS.OwningScenarioID = @ScenarioID 
	AND tAS.AdjustmentSetID not in (SELECT * From @AdjustmentSetIDsToKeep)
	
	INSERT INTO @AIDs
	SELECT tA.AdjustmentID from Projection.tAdjustment tA
	INNER JOIN @ASIDs tAS ON tA.AdjustmentSetID = tAS.AdjustmentSetID
	
	INSERT INTO @ADIDs
	SELECT tAD.AdjustmentDetailID FROM Projection.tAdjustmentDetail tAD
	INNER JOIN @AIDs tA on tAD.AdjustmentID = tA.AdjustmentID
	
	INSERT INTO @ADNIDs
	SELECT tADN.AdjustmentDetailNotesID FROM Projection.tAdjustmentDetailNote tADN
	INNER JOIN @ADIDs tAD on tAD.AdjustmentDetailID = tADN.AdjustmentDetailID
	
	DELETE FROM Projection.tAdjustmentDetailNote
	WHERE AdjustmentDetailNotesID in (SELECT * FROM @ADNIDs)
	
	DELETE FROM Projection.tAdjustmentDetail
	WHERE AdjustmentDetailID in (SELECT * FROM @ADIDs)
	
	DELETE FROM Projection.tAdjustment
	WHERE AdjustmentID in (SELECT * FROM @AIDs)
	
	DELETE FROM Projection.tAdjustmentSet
	WHERE AdjustmentSetID in (SELECT * FROM @ASIDs)
	
END
;
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteAdjustmentSetsByScenarioID] TO [TEUser]
    AS [dbo];

