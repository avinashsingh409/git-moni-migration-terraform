CREATE Procedure [Projection].[spDeleteInputType]
	@InputTypeID INT
AS
BEGIN
	DELETE FROM Projection.tInputTypeInputOptionTypeMap where InputTypeID = @InputTypeID
	DELETE FROM Projection.tRegulatoryInputTypeUIItemUITargetTypeMap where InputTypeID = @InputTypeID
	DELETE FROM Projection.tInputType WHERE InputTypeID = @InputTypeID
END
GO

GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteInputType] TO [TEUser]
    AS [dbo];
GO