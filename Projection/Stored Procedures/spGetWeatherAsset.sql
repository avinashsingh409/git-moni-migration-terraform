﻿
CREATE PROCEDURE [Projection].[spGetWeatherAsset]
	@AssetID int = NULL,
	@Enabled bit = NULL
AS
BEGIN

SELECT WeatherAssetID,
       WeatherAssetAbbrev,
       WeatherAssetDesc,
       LastCollectionTime,
       Enabled,
       AssetID,
       ADDSStationID,
       ChangeDate,
       ChangedBy,
       CreateDate,
       CreatedBy
  FROM Projection.tWeatherAsset
 WHERE (AssetID = @AssetID OR @AssetID IS NULL)
   AND (Enabled = @Enabled OR @Enabled IS NULL)

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetWeatherAsset] TO [TEUser]
    AS [dbo];

