﻿
CREATE PROCEDURE Projection.spGetScenarioMonthlyUnitCosts

@ScenarioID int, @MinYear int, @MaxYear int

AS

BEGIN

SET NOCOUNT ON
select UnitID,YEAR,MONTH,
SUM(case when datepart(dw,base.dateserial(a.YEAR,a.MONTH,a.DAY,a.Hour,0,0)) between 2 and 6 and a.[Hour] between 7 and 21 then ISNULL(a.energy,0) else 0 end) as Energy_WeekdayPeak_dollars,
SUM(case when datepart(dw,base.dateserial(a.YEAR,a.MONTH,a.DAY,a.Hour,0,0)) between 2 and 6 and not (a.[Hour] between 7 and 21)then ISNULL(a.energy,0) else 0 end) as Energy_WeekdayOffPeak_dollars,
SUM(case when not (datepart(dw,base.dateserial(a.YEAR,a.MONTH,a.DAY,a.Hour,0,0)) between 2 and 6) and a.[Hour] between 7 and 21 then ISNULL(a.energy,0) else 0 end)as Energy_WeekendPeak_dollars,
SUM(case when not (datepart(dw,base.dateserial(a.YEAR,a.MONTH,a.DAY,a.Hour,0,0)) between 2 and 6) and not (a.[Hour] between 7 and 21) then ISNULL(a.energy,0) else 0 end) as Energy_WeekendOffPeak_dollars,
SUM(ISNULL(Fuel,0)) as Fuel_dollars,
SUM(ISNULL(SaleableBottomAsh,0)) as SaleableBottomAsh_dollars,
SUM(ISNULL(NonSaleableBottomAsh,0)) as NonSaleableBottomAsh_dollars,
SUM(ISNULL(SaleableFlyAsh,0)) as SaleableFlyAsh_dollars,
SUM(ISNULL(NonSaleableFlyAsh,0)) as NonSaleableFlyAsh_dollars,
SUM(ISNULL(Gypsum,0)) as Gypsum_dollars,
SUM(ISNULL(FGDAdditive,0)) as FGDAdditive_dollars,
SUM(ISNULL(FGDFixative,0)) asFGDFixative_dollars,
SUM(ISNULL(FGDWaste,0)) as FGDWaste_dollars,
SUM(ISNULL(FGDWater,0)) as FGDWater_dollars,
SUM(ISNULL(FGDReagent,0)) as FGDReagent_dollars,
SUM(ISNULL(FGDAsh,0)) as FGDAsh_dollars,
SUM(ISNULL(DryFGDWaste,0)) as DryFGDWaste_dollars,
SUM(ISNULL(MercuryAdditive,0)) as MercuryAdditive_dollars,
SUM(ISNULL(FurnaceSorbent,0)) as FurnaceSorbent_dollars,
SUM(ISNULL(SO3Feedstock,0)) as SO3Feedstock_dollars,
SUM(ISNULL(CoalAdditive,0)) as CoalAdditive_dollars,
SUM(ISNULL(FlueGasSorbent,0)) as FlueGasSorbent_dollars,
SUM(ISNULL(FGDDBAAdditive,0)) as FGDDBAAdditive_dollars,
SUM(ISNULL(CFBBedReagent,0)) as CFBBedReagent_dollars,
SUM(ISNULL(NH3Feedstock,0)) as NH3Feedstock_dollars,
SUM(ISNULL(SO2Allowance,0)) as SO2Allowance_dollars,
SUM(ISNULL(NOxAllowance,0)) as NOxAllowance_dollars,
SUM(ISNULL(HgAllowance,0)) as HgAllowance_dollars,
SUM(ISNULL(CO2Allowance,0)) as CO2Allowance_dollars

From projection.tScenarioUnitCost a where ScenarioID=@ScenarioID AND Year>=@MinYear and Year<=@MaxYear GROUP BY UnitID,Year,Month

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetScenarioMonthlyUnitCosts] TO [TEUser]
    AS [dbo];

