﻿CREATE PROCEDURE [Projection].[spGetBackdown]

@ScenarioID int

AS

BEGIN


SET NOCOUNT ON;

select a.UnitID,a.Year,a.Month,sUP.netgeneration_mwh as Generation_MW, b.UseDeratedPower, b.applyunavailability, case when b.MaintenancePlanSetID IS null then 0 else 1 end as HasMaintenancePlan,
case when u.RatedNetCapacity_mw-sup.LostEnergyFromFuelDerate_mw-sup.LostEnergyFromUnavailability_mw-a.Generation_mw > 0 then 
             u.RatedNetCapacity_mw-sup.LostEnergyFromFuelDerate_mw-sup.LostEnergyFromUnavailability_mw-a.Generation_mw else 0 end as Backdown_MWh, 
             a.Generation_mw as Demand_MWh
             ,
              
              (case when b.UseDeratedPower=1 then sup.MaxVistaNetPower_mw else u.RatedNetCapacity_mw end)
                         as MaxVistaNetPower_mw,
              
              (case when b.DemandSetID is not null then gen.MaxGeneration_mw else u.RatedNetCapacity_mw end)
                         as MaxRAMNetPower_mw,
              u.RatedNetCapacity_mw as RatedNetCapacity_mw, CONVERT(float,0.00) as EnergyAvail_mw
              into #tmp_backdown
              from projection.tGenerationDetail_Adjusted a join 
            projection.tScenario b on a.GenerationSetID=isnull(b.DemandSetID,b.generationsetid)
            JOIN projection.tGenerationDetail_Adjusted gen on gen.GenerationSetID = b.GenerationSetID and a.UnitID=gen.UnitID and a.Year=gen.Year and a.Month=gen.Month and a.Day=gen.Day and a.Hour=gen.hour
             JOIN Asset.tUnit u on a.UnitID=u.UnitID 
             JOIN Projection.tScenarioUnitPerformance sUP on b.ScenarioID = sUP.ScenarioID and a.UnitID = sUP.UnitID and a.Year = sUP.year and a.Month = sUP.Month and a.Day = sUP.Day and a.Hour = sUP.Hour 
            where b.ScenarioID=@scenarioid and b.DemandSetID is not null

UPDATE #tmp_backdown SET   EnergyAvail_mw = case when MaxVistaNetPower_mw < MaxRAMNetPower_mw and MaxVistaNetPower_mw < RatedNetCapacity_mw then MaxVistaNetPower_mw 
when MaxRAMNetPower_mw < MaxVistaNetPower_mw and MaxRAMNetPower_mw < RatedNetCapacity_mw then MaxRAMNetPower_mw 
else RatedNetCapacity_mw
end

select UnitID,Year,Month,SUM(Generation_MW) as Generation_MWh, SUM(Demand_MWh) as Demand_MWh,SUM(backdown_mwh) as OldBackdown_mwh,
sum(
case when EnergyAvail_mw > Demand_MWh then EnergyAvail_mw - Demand_MWh else 0 end
) as Backdown_MWh
             
              from #tmp_backdown
            GROUP BY UnitID,Year,Month
            
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetBackdown] TO [TEUser]
    AS [dbo];

