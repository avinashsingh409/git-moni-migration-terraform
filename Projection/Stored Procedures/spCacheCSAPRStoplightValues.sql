﻿
CREATE PROCEDURE [Projection].[spCacheCSAPRStoplightValues] 
(
  @DateAsOf as DateTime = null,
  @ProjectionScenarioID as int = null
)
AS
BEGIN
DECLARE @RC int
DECLARE @UnitAssetID int

DECLARE @TryToRetrieveFromCache bit
DECLARE @MTDSO2StopLightValue int
DECLARE @CurrentSO2StopLightValue int
DECLARE @ProjectedSO2StopLightValue int
DECLARE @MTDSO2MarginPct real
DECLARE @CurrentSO2MarginPct real
DECLARE @ProjectedSO2MarginPct real
DECLARE @MTDSO2ActualValue real
DECLARE @YTDSO2ActualValue real
DECLARE @EOYSO2ProjectedValue real
DECLARE @MTDSO2PlanValue real
DECLARE @YTDSO2PlanValue real
DECLARE @FullYearSO2PlanValue real
DECLARE @SO2EPAAllocation real
DECLARE @SO2AccountBalance real
DECLARE @YTDSO2AlternatePlanValue real
DECLARE @EOYSO2AlternatePlanValue real
DECLARE @YTDSO2AlternateProjectedValue real
DECLARE @EOYSO2AlternateProjectedValue real
DECLARE @MTDNOxStopLightValue int
DECLARE @CurrentNOxStopLightValue int
DECLARE @ProjectedNOxStopLightValue int
DECLARE @MTDNOxMarginPct real
DECLARE @CurrentNOxMarginPct real
DECLARE @ProjectedNOxMarginPct real
DECLARE @MTDNOxActualValue real
DECLARE @YTDNOxActualValue real
DECLARE @EOYNOxProjectedValue real
DECLARE @MTDNOxPlanValue real
DECLARE @YTDNOxPlanValue real
DECLARE @FullYearNOxPlanValue real
DECLARE @NOxEPAAllocation real
DECLARE @NOxAccountBalance real
DECLARE @YTDNOxAlternatePlanValue real
DECLARE @EOYNOxAlternatePlanValue real
DECLARE @YTDNOxAlternateProjectedValue real
DECLARE @EOYNOxAlternateProjectedValue real
DECLARE @MTDNOxSStopLightValue int
DECLARE @CurrentNOxSStopLightValue int
DECLARE @ProjectedNOxSStopLightValue int
DECLARE @MTDNOxSMarginPct real
DECLARE @CurrentNOxSMarginPct real
DECLARE @ProjectedNOxSMarginPct real
DECLARE @MTDNOxSActualValue real
DECLARE @YTDNOxSActualValue real
DECLARE @EOYNOxSProjectedValue real
DECLARE @MTDNOxSPlanValue real
DECLARE @YTDNOxSPlanValue real
DECLARE @FullYearNOxSPlanValue real
DECLARE @NOxSEPAAllocation real
DECLARE @NOxSAccountBalance real
DECLARE @YTDNOxSAlternatePlanValue real
DECLARE @EOYNOxSAlternatePlanValue real
DECLARE @YTDNOxSAlternateProjectedValue real
DECLARE @EOYNOxSAlternateProjectedValue real
DECLARE @MTDNetGenStopLightValue int
DECLARE @CurrentNetGenStopLightValue int
DECLARE @ProjectedNetGenStopLightValue int
DECLARE @MTDNetGenMarginPct real
DECLARE @CurrentNetGenMarginPct real
DECLARE @ProjectedNetGenMarginPct real
DECLARE @MTDNetGenActualValue real
DECLARE @YTDNetGenActualValue real
DECLARE @EOYNetGenProjectedValue real
DECLARE @MTDNetGenPlanValue real
DECLARE @YTDNetGenPlanValue real
DECLARE @FullYearNetGenPlanValue real
DECLARE @NetGenEPAAllocation real
DECLARE @NetGenAccountBalance real
DECLARE @YTDNetGenAlternatePlanValue real
DECLARE @EOYNetGenAlternatePlanValue real
DECLARE @YTDNetGenAlternateProjectedValue real
DECLARE @EOYNetGenAlternateProjectedValue real
DECLARE @LastActualsDate datetime

if @dateAsOf is null
  begin
  set @lastActualsDate = [Actuals].[ufnLastActualsDateForAssetId](null)
  end
else
  begin
  set @lastActualsDate = @dateAsOf
  end
  
set @lastActualsDate = base.DateSerial(YEAR(@lastActualsDate),MONTH(@lastActualsDate),DAY(@lastActualsDate),23,0,0)
set @dateAsOf =  @lastActualsDate

DECLARE curUP INSENSITIVE CURSOR FOR SELECT AssetID FROM Asset.tUnit

OPEN curUP;
FETCH NEXT FROM curUP INTO @UnitAssetID
WHILE @@FETCH_STATUS =  0
  BEGIN		

EXECUTE @RC = [Projection].[spGetCSAPRStoplights] 
   @UnitAssetID
  ,@dateAsOf
  ,@ProjectionScenarioID
  ,0
  ,@MTDSO2StopLightValue OUTPUT
  ,@CurrentSO2StopLightValue OUTPUT
  ,@ProjectedSO2StopLightValue OUTPUT
  ,@MTDSO2MarginPct OUTPUT
  ,@CurrentSO2MarginPct OUTPUT
  ,@ProjectedSO2MarginPct OUTPUT
  ,@MTDSO2ActualValue OUTPUT
  ,@YTDSO2ActualValue OUTPUT
  ,@EOYSO2ProjectedValue OUTPUT
  ,@MTDSO2PlanValue OUTPUT
  ,@YTDSO2PlanValue OUTPUT
  ,@FullYearSO2PlanValue OUTPUT
  ,@SO2EPAAllocation OUTPUT
  ,@SO2AccountBalance OUTPUT
  ,@YTDSO2AlternatePlanValue OUTPUT
  ,@EOYSO2AlternatePlanValue OUTPUT
  ,@YTDSO2AlternateProjectedValue OUTPUT
  ,@EOYSO2AlternateProjectedValue OUTPUT
  ,@MTDNOxStopLightValue OUTPUT
  ,@CurrentNOxStopLightValue OUTPUT
  ,@ProjectedNOxStopLightValue OUTPUT
  ,@MTDNOxMarginPct OUTPUT
  ,@CurrentNOxMarginPct OUTPUT
  ,@ProjectedNOxMarginPct OUTPUT
  ,@MTDNOxActualValue OUTPUT
  ,@YTDNOxActualValue OUTPUT
  ,@EOYNOxProjectedValue OUTPUT
  ,@MTDNOxPlanValue OUTPUT
  ,@YTDNOxPlanValue OUTPUT
  ,@FullYearNOxPlanValue OUTPUT
  ,@NOxEPAAllocation OUTPUT
  ,@NOxAccountBalance OUTPUT
  ,@YTDNOxAlternatePlanValue OUTPUT
  ,@EOYNOxAlternatePlanValue OUTPUT
  ,@YTDNOxAlternateProjectedValue OUTPUT
  ,@EOYNOxAlternateProjectedValue OUTPUT
  ,@MTDNOxSStopLightValue OUTPUT
  ,@CurrentNOxSStopLightValue OUTPUT
  ,@ProjectedNOxSStopLightValue OUTPUT
  ,@MTDNOxSMarginPct OUTPUT
  ,@CurrentNOxSMarginPct OUTPUT
  ,@ProjectedNOxSMarginPct OUTPUT
  ,@MTDNOxSActualValue OUTPUT
  ,@YTDNOxSActualValue OUTPUT
  ,@EOYNOxSProjectedValue OUTPUT
  ,@MTDNOxSPlanValue OUTPUT
  ,@YTDNOxSPlanValue OUTPUT
  ,@FullYearNOxSPlanValue OUTPUT
  ,@NOxSEPAAllocation OUTPUT
  ,@NOxSAccountBalance OUTPUT
  ,@YTDNOxSAlternatePlanValue OUTPUT
  ,@EOYNOxSAlternatePlanValue OUTPUT
  ,@YTDNOxSAlternateProjectedValue OUTPUT
  ,@EOYNOxSAlternateProjectedValue OUTPUT
  ,@MTDNetGenStopLightValue OUTPUT
  ,@CurrentNetGenStopLightValue OUTPUT
  ,@ProjectedNetGenStopLightValue OUTPUT
  ,@MTDNetGenMarginPct OUTPUT
  ,@CurrentNetGenMarginPct OUTPUT
  ,@ProjectedNetGenMarginPct OUTPUT
  ,@MTDNetGenActualValue OUTPUT
  ,@YTDNetGenActualValue OUTPUT
  ,@EOYNetGenProjectedValue OUTPUT
  ,@MTDNetGenPlanValue OUTPUT
  ,@YTDNetGenPlanValue OUTPUT
  ,@FullYearNetGenPlanValue OUTPUT
  ,@NetGenEPAAllocation OUTPUT
  ,@NetGenAccountBalance OUTPUT
  ,@YTDNetGenAlternatePlanValue OUTPUT
  ,@EOYNetGenAlternatePlanValue OUTPUT
  ,@YTDNetGenAlternateProjectedValue OUTPUT
  ,@EOYNetGenAlternateProjectedValue OUTPUT
  ,@LastActualsDate OUTPUT

  FETCH NEXT FROM curUP INTO @unitassetid
  END
CLOSE curUP;
DEALLOCATE curUP;
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spCacheCSAPRStoplightValues] TO [TEUser]
    AS [dbo];

