﻿CREATE PROCEDURE [Projection].[spInsertUpdateWeatherForecast]
	@ForecastData Projection.tpForecastWeather READONLY
AS
BEGIN

BEGIN TRANSACTION

DELETE Projection.tWeatherForecast
FROM Projection.tWeatherForecast wf
INNER JOIN @ForecastData wfType on wf.WeatherAssetID = wfType.WeatherAssetID
                and wf.TimeStamp = wfType.TimeStamp
  
INSERT INTO Projection.tWeatherForecast 
  (WeatherAssetID, 
  TimeStamp, 
  WeatherIcon,
  DailyMax_degF, 
  DailyMin_degF, 
  Temperature_degF, 
  ApparentTemperature_degF, 
  DewPointTemperature_degF, 
  WindDirection_deg, 
  WindSpeed_MPH, 
  WindSpeedGust_MPH, 
  CloudCover_pct, 
  Precipitation_in, 
  Precipitation12HourProb_pct, 
  Snowfall_in, 
  IceAccumulation_in, 
  RelativeHumidity_pct, 
  MaxRelativeHumidity_pct, 
  MinRelativeHumidity_pct)
SELECT WeatherAssetID, 
  TimeStamp, 
  WeatherIcon,
  DailyMax_degF, 
  DailyMin_degF, 
  Temperature_degF, 
  ApparentTemperature_degF, 
  DewPointTemperature_degF, 
  WindDirection_deg, 
  WindSpeed_MPH, 
  WindSpeedGust_MPH, 
  CloudCover_pct, 
  Precipitation_in, 
  Precipitation12HourProb_pct, 
  Snowfall_in, 
  IceAccumulation_in, 
  RelativeHumidity_pct, 
  MaxRelativeHumidity_pct, 
  MinRelativeHumidity_pct
FROM @ForecastData

COMMIT TRANSACTION

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spInsertUpdateWeatherForecast] TO [TEUser]
    AS [dbo];

