﻿




CREATE PROCEDURE [Projection].[spExtractUITargetDataFromAllRegAdminAdjustmentSets] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	--Start by making sure we have all the plan years necessary covered in tUITargetSet.
	BEGIN
		CREATE TABLE #PlanYearsNeeded (PlanYear smallint not null);
		Insert into #PlanYearsNeeded
		SELECT DISTINCT YEAR(StartDate)
		from Projection.tAdjustmentSet adjSet
		WHERE adjSet.OwningScenarioID is null
		
		SELECT PlanYear Into #PlanYearsToAdd from #PlanYearsNeeded
		WHERE PlanYear not in (SELECT PlanYear FROM Projection.tUITargetSet);
		
		IF (SELECT COUNT(*) FROM #PlanYearsToAdd) > 0
		BEGIN
			DECLARE PlanYearCursor CURSOR
			FOR Select * FROM #PlanYearsToAdd;
			DECLARE @PlanYear smallint;
			OPEN PlanYearCursor;

			FETCH NEXT FROM PlanYearCursor 
			INTO @PlanYear;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				INSERT INTO [Projection].[tUITargetSet]
					   ([UITargetSetDesc]
					   ,[DataVersionID]
					   ,[PlanYear]
					   ,[EditDate]
					   ,[IsConsensus]
					   ,[CreatedBy]
					   ,[ChangedBy]
					   ,[CreateDate]
					   ,[ChangeDate])
				 VALUES
					   (CONVERT(varchar(4), @PlanYear) + ' Limits and Targets'
					   ,1 --annual budget
					   ,@PlanYear
					   ,SYSDATETIME()
					   ,1
					   ,USER
					   ,USER
					   ,SYSDATETIME()
					   ,SYSDATETIME())
				
				
				FETCH NEXT FROM PlanYearCursor 
				INTO @PlanYear;
			END
		END
	END
	
	
	--At this point, every year covered by the supplied adjustment sets is now 
	--represented in tUITargetSet. Which means we should now be able to add in
	--all the requisite detail records.
	
	
	SELECT [UITargetSetID]
      , u.[UnitID]
      , itui.[UIItemID]
      , itui.[UITargetTypeID]
      , adjDet.EffectiveDate as [StartDate]
      , DateAdd(HOUR, DATEDIFF(Hour,'1900-01-01', adjDet.Duration), adjDet.EffectiveDate) as [EndDate]
      , adjDet.Value [Quantity]
      , isnull(vt.EngUnits,'') as [QuantityUnit]
      , 0 as [Margin] --all the detail records I've seen have had a margin of 0 specified.
      , isnull(vt.EngUnits,'') as [MarginUnit]
      , 1 as [IsRegulatory]
    INTO #NewUITargetUnitDetailsToAdd
    from  Projection.tAdjustmentSet adjSet
	INNER JOIN Projection.tAdjustment adj on adj.AdjustmentSetID = adjSet.AdjustmentSetID
	INNER JOIN Projection.tAdjustmentDetail adjDet on adj.AdjustmentID = adjDet.AdjustmentID
	INNER JOIN Projection.tInputTypeInputOptionTypeMap itiotm on itiotm.InputTypeInputOptionTypeMapID = adj.InputTypeInputOptionTypeMapID
	INNER JOIN Projection.tInputType it on itiotm.InputTypeID = it.InputTypeID
	INNER JOIN Asset.tUnit u on u.AssetID = adj.AssetID --for UnitID
	INNER JOIN Projection.tRegulatoryInputTypeUIItemUITargetTypeMap itui ON it.InputTypeID = itui.InputTypeID
	LEFT JOIN ProcessData.tVariableType vt ON vt.PDVariableID = it.VariableTypeID
	INNER JOIN Projection.tUITargetSet uits on uits.PlanYear = YEAR(adjSet.StartDate)
    WHERE adjSet.OwningScenarioID is null
    
	
	SELECT [UITargetSetID]
      , adj.AssetID
	  , adjSet.OwningAssetID
      , itui.[UIItemID]
      , itui.[UITargetTypeID]
      , adjDet.EffectiveDate as [StartDate]
      , DateAdd(HOUR, DATEDIFF(Hour,'1900-01-01', adjDet.Duration), adjDet.EffectiveDate) as [EndDate]
      , adjDet.Value [Quantity]
      , isnull(vt.EngUnits,'') as [QuantityUnit]
      , 0 as [Margin] --all the detail records I've seen have had a margin of 0 specified.
      , isnull(vt.EngUnits,'') as [MarginUnit]
      , 1 as [IsRegulatory]
    INTO #NewUITargetClientDetailsToAdd
    from  Projection.tAdjustmentSet adjSet
	INNER JOIN Projection.tAdjustment adj on adj.AdjustmentSetID = adjSet.AdjustmentSetID
	INNER JOIN Projection.tAdjustmentDetail adjDet on adj.AdjustmentID = adjDet.AdjustmentID
	INNER JOIN Projection.tInputTypeInputOptionTypeMap itiotm on itiotm.InputTypeInputOptionTypeMapID = adj.InputTypeInputOptionTypeMapID
	INNER JOIN Projection.tInputType it on itiotm.InputTypeID = it.InputTypeID
	--INNER JOIN Asset.tUnit u on u.AssetID = adj.AssetID --for UnitID
	INNER JOIN Projection.tRegulatoryInputTypeUIItemUITargetTypeMap itui ON it.InputTypeID = itui.InputTypeID
	LEFT JOIN ProcessData.tVariableType vt ON vt.PDVariableID = it.VariableTypeID
	INNER JOIN Projection.tUITargetSet uits on uits.PlanYear = YEAR(adjSet.StartDate)
    WHERE adjSet.OwningScenarioID is null AND it.AssetClassTypeID = 1000 --asset class type 1000 is 'client'
    


	--Delete existing records of relevance
	DECLARE @CSAPREPAAlloc_UITargetTypeID int;
	SELECT @CSAPREPAAlloc_UITargetTypeID = UITargetTypeID FROM Projection.tUITargetType WHERE UITargetTypeAbbrev = 'CSAPR Allocation';
	DECLARE @CSAPRAcctBal_UITargetTypeID int;
	SELECT @CSAPRAcctBal_UITargetTypeID = UITargetTypeID FROM Projection.tUITargetType WHERE UITargetTypeAbbrev = 'CSAPR Account Balance';
	DECLARE @1MonthRate_UITargetTypeID int;
	SELECT @1MonthRate_UITargetTypeID = UITargetTypeID FROM Projection.tUITargetType WHERE UITargetTypeAbbrev = '1-Month Rate';
	--The relevant UIItemIDs are for SO2Out, SO2Out2, PMOut1, PMOut2, HgOut1 (low rank), HgOut2 (low rank), HgOut1 (high rank), HgOut2 (low rank), HCl_lbmmbtu, HCl_lbmmwh)
	--I'm not going to spend 20 lines setting up explicit IDs for them, since I know those ID aren't going to change.
	DECLARE @AirPermits_UITargetTypeID int;
	SELECT @AirPermits_UITargetTypeID = UITargetTypeID FROM Projection.tUITargetType WHERE UITargetTypeAbbrev = 'Air Permits';
		
	BEGIN TRY
		BEGIN TRANSACTION;
		SET XACT_ABORT ON;
		DELETE FROM Projection.tUITargetUnitDetail WHERE (
			UITargetTypeID = @CSAPREPAAlloc_UITargetTypeID
			OR UITargetTypeID = @CSAPRAcctBal_UITargetTypeID 
			OR UITargetTypeID = @AirPermits_UITargetTypeID
		) OR (
			UITargetTypeID = @1MonthRate_UITargetTypeID
			and IsRegulatory = 1
			and UIItemID IN (9,42,43,44,45,46,47,48,51,52)
		)
		;

		INSERT INTO [Projection].[tUITargetUnitDetail]
			   ([UITargetSetID]
			   ,[UnitID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory])
		SELECT [UITargetSetID]
			   ,[UnitID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory]
		 FROM #NewUITargetUnitDetailsToAdd;
		 print 'Unit Delete and Insert Succeeded'


		 DELETE FROM Projection.tUITargetClientDetail
		 WHERE UITargetTypeID = @CSAPRAcctBal_UITargetTypeID
		 ;

		 INSERT INTO Projection.[tUITargetClientDetail]
			   ([UITargetSetID]
			   ,[AssetID]
			   ,[OwningAssetID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory])
		SELECT [UITargetSetID]
			   ,[AssetID]
			   ,[OwningAssetID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory]
		 FROM #NewUITargetClientDetailsToAdd;

		 ;
		 print 'Client Delete and Insert Succeeded'
		 print 'Committing Transaction.'
		COMMIT TRANSACTION;	    
	END TRY
	BEGIN CATCH
		 print 'Delete and Insert Failed; Rolling Back Transaction.'
		ROLLBACK TRANSACTION;
	END CATCH;
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spExtractUITargetDataFromAllRegAdminAdjustmentSets] TO [TEUser]
    AS [dbo];

