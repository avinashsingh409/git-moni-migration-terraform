﻿


CREATE PROCEDURE [Projection].[spGetScenarioUnitEmissionsRate]
	@scenarioID int
AS
SELECT scenarioUnitEmissionsRate.ScenarioUnitEmissionsRateID
      ,scenarioUnitEmissionsRate.ScenarioID
      ,scenarioUnitEmissionsRate.UnitID
      ,a.AssetID
      ,scenarioUnitEmissionsRate.UIItemID
      ,scenarioUnitEmissionsRate.Rate
      ,UIItem.UIItemDesc
      ,UIItem.UIItemAbbrev
      --,UIItem.IsMatsLite
      ,scenarioUnitEmissionsRate.StartDate
      ,scenarioUnitEmissionsRate.Comment
  FROM Projection.tScenarioUnitEmissionsRate scenarioUnitEmissionsRate
  INNER JOIN Projection.tUIItem UIItem
  ON scenarioUnitEmissionsRate.UIItemID = UIItem.UIItemID
  INNER JOIN Asset.tUnit u
  ON scenarioUnitEmissionsRate.UnitID = u.UnitID
  INNER JOIN Asset.tAsset a 
  ON u.AssetID = a.AssetID
  WHERE scenarioUnitEmissionsRate.ScenarioID = @scenarioID
RETURN 0
GO



GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetScenarioUnitEmissionsRate] TO [TEUser]
    AS [dbo];

