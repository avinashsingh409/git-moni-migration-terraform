﻿
CREATE PROCEDURE [Projection].[spGetCSAPRStoplights]
	@UnitAssetID int,
	@DateAsOf datetime = null,
	@ProjectionScenarioID int = null,	
	@TryToRetrieveFromCache bit = 1,
	
	@MTDSO2StopLightValue int OUTPUT,
	@CurrentSO2StopLightValue int OUTPUT,
	@ProjectedSO2StopLightValue int OUTPUT,
	@MTDSO2MarginPct real OUTPUT,
	@CurrentSO2MarginPct real OUTPUT,
	@ProjectedSO2MarginPct real OUTPUT,
	@MTDSO2ActualValue real OUTPUT,
	@YTDSO2ActualValue real OUTPUT,
	@EOYSO2ProjectedValue real OUTPUT,
	@MTDSO2PlanValue real OUTPUT,
	@YTDSO2PlanValue real OUTPUT,
	@FullYearSO2PlanValue real OUTPUT,
	@SO2EPAAllocation real OUTPUT,
	@SO2AccountBalance real OUTPUT,	
	@YTDSO2AlternatePlanValue real OUTPUT,
	@EOYSO2AlternatePlanValue real OUTPUT,	
	@YTDSO2AlternateProjectedValue real OUTPUT,
	@EOYSO2AlternateProjectedValue real OUTPUT,		
	
	@MTDNOxStopLightValue int OUTPUT,
	@CurrentNOxStopLightValue int OUTPUT,
	@ProjectedNOxStopLightValue int OUTPUT,
	@MTDNOxMarginPct real OUTPUT,
	@CurrentNOxMarginPct real OUTPUT,
	@ProjectedNOxMarginPct real OUTPUT,
	@MTDNOxActualValue real OUTPUT,
	@YTDNOxActualValue real OUTPUT,
	@EOYNOxProjectedValue real OUTPUT,
	@MTDNOxPlanValue real OUTPUT,
	@YTDNOxPlanValue real OUTPUT,
	@FullYearNOxPlanValue real OUTPUT,
	@NOxEPAAllocation real OUTPUT,
	@NOxAccountBalance real OUTPUT,	
	@YTDNOxAlternatePlanValue real OUTPUT,
	@EOYNOxAlternatePlanValue real OUTPUT,	
	@YTDNOxAlternateProjectedValue real OUTPUT,
	@EOYNOxAlternateProjectedValue real OUTPUT,		
	
	@MTDNOxSStopLightValue int OUTPUT,
	@CurrentNOxSStopLightValue int OUTPUT,
	@ProjectedNOxSStopLightValue int OUTPUT,
	@MTDNOxSMarginPct real OUTPUT,
	@CurrentNOxSMarginPct real OUTPUT,
	@ProjectedNOxSMarginPct real OUTPUT,
	@MTDNOxSActualValue real OUTPUT,
	@YTDNOxSActualValue real OUTPUT,
	@EOYNOxSProjectedValue real OUTPUT,
	@MTDNOxSPlanValue real OUTPUT,
	@YTDNOxSPlanValue real OUTPUT,
	@FullYearNOxSPlanValue real OUTPUT,
	@NOxSEPAAllocation real OUTPUT,
	@NOxSAccountBalance real OUTPUT,	
	@YTDNOxSAlternatePlanValue real OUTPUT,
	@EOYNOxSAlternatePlanValue real OUTPUT,	
	@YTDNOxSAlternateProjectedValue real OUTPUT,
	@EOYNOxSAlternateProjectedValue real OUTPUT,		
	
	@MTDNetGenStopLightValue int OUTPUT,
	@CurrentNetGenStopLightValue int OUTPUT,
	@ProjectedNetGenStopLightValue int OUTPUT,
	@MTDNetGenMarginPct real OUTPUT,
	@CurrentNetGenMarginPct real OUTPUT,
	@ProjectedNetGenMarginPct real OUTPUT,
	@MTDNetGenActualValue real OUTPUT,
	@YTDNetGenActualValue real OUTPUT,
	@EOYNetGenProjectedValue real OUTPUT,
	@MTDNetGenPlanValue real OUTPUT,
	@YTDNetGenPlanValue real OUTPUT,
	@FullYearNetGenPlanValue real OUTPUT,
	@NetGenEPAAllocation real OUTPUT,
	@NetGenAccountBalance real OUTPUT,	
	@YTDNetGenAlternatePlanValue real OUTPUT,
	@EOYNetGenAlternatePlanValue real OUTPUT,	
	@YTDNetGenAlternateProjectedValue real OUTPUT,
	@EOYNetGenAlternateProjectedValue real OUTPUT,
		
	@LastActualsDate DateTime OUTPUT
	
AS
BEGIN

    set @lastActualsDate = [Actuals].[ufnLastActualsDateForAssetId](null)
    set @lastActualsDate = base.DateSerial(YEAR(@lastActualsDate),MONTH(@lastActualsDate),DAY(@lastActualsDate),23,0,0)
    
    if @DateAsOf is null
      begin
      set @DateAsOf = @lastActualsDate
      end
    
    set @DateAsOf = base.DateSerial(YEAR(@DateAsOf),MONTH(@DateAsOf),DAY(@DateAsOf),23,0,0)    
    
    if @ProjectionScenarioID is null
      begin   
      set @ProjectionScenarioID = [Projection].[ufnMostRecentConsensusScenarioID](YEAR(@dateAsOf),MONTH(@dateAsOf))
      end
          
    DECLARE @SO2_ton_UIItemID int = 1;
    DECLARE @NOx_ton_UIItemID int = 2;
    DECLARE @NOxS_ton_UIItemID int = 3;
    DECLARE @NetGen_MWh_UIItemID int = 6;
    DECLARE @UITargetTypeID int = 6; 
    DECLARE @CSAPR_Allocation_TargetTypyeID int = 8
    DECLARE @CSAPR_Balance_TargetTypyeID int = 9
	
	--Need to get the UITargetSetID by extracting the Year from @DateAsOf, and searching for the matching PlanYear in tUITargetSet.
	DECLARE @UITargetSetID int;
	DECLARE @DateAsOfYear int = Year(@DateAsOf);
	DECLARE @AsOfYearLastDay datetime = Base.DateSerial(@DateAsOfYear, 12, 31, 0, 0, 0);
	SELECT @UITargetSetID = a.UITargetSetID FROM Projection.tUITargetSet a WHERE a.PlanYear = @DateAsOfYear
		
	DECLARE @UnitID int;
	SELECT @UnitID = UnitID from Asset.tUnit WHERE AssetID = @UnitAssetID
	
	declare @MTDSO2AlternateProjectionValue as real
	declare @MTDNOxAlternateProjectionValue as real
	declare @MTDNOxSAlternateProjectionValue as real
	declare @MTDNetGenAlternateProjectionValue as real
	
	declare @MTDSO2AlternatePlanValue as real
	declare @MTDNOxAlternatePlanValue as real
	declare @MTDNOxSAlternatePlanValue as real
	declare @MTDNetGenAlternatePlanValue as real
	
	declare @RestOfMonthSO2AlternateProjectionValue as real
	declare @RestOfMonthNOxAlternateProjectionValue as real
	declare @RestOfMonthNOxSAlternateProjectionValue as real
	declare @RestOfMonthNetGenAlternateProjectionValue as real
	
	declare @RestOfMonthSO2AlternatePlanValue as real
	declare @RestOfMonthNOxAlternatePlanValue as real
	declare @RestOfMonthNOxSAlternatePlanValue as real
	declare @RestOfMonthNetGenAlternatePlanValue as real
	
	declare @RestOfYearSO2AlternateProjectionValue as real
	declare @RestOfYearNOxAlternateProjectionValue as real
	declare @RestOfYearNOxSAlternateProjectionValue as real
	declare @RestOfYearNetGenAlternateProjectionValue as real
	
	declare @RestOfYearSO2AlternatePlanValue as real
	declare @RestOfYearNOxAlternatePlanValue as real
	declare @RestOfYearNOxSAlternatePlanValue as real
	declare @RestOfYearNetGenAlternatePlanValue as real
	
	declare @daysinmonth as int
	set @daysinmonth = datediff(day, dateadd(day, 1-day(@DateAsOf), @DateAsOf),
              dateadd(month, 1, dateadd(day, 1-day(@DateAsOf), @DateAsOf)))
	
	declare @ratio float 
	set @ratio = DAY(@DateAsOf) * 1.0/@daysinmonth
		
	select @MTDSO2AlternatePlanValue = ISNULL(SO2_ton * @ratio,0),
	       @MTDNOxAlternatePlanValue = ISNULL(NOx_ton * @ratio,0),
	       @MTDNOxSAlternatePlanValue = ISNULL(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * @ratio ELSE 0 END,0),
	       @MTDNetGenAlternatePlanValue = ISNULL(NetGeneration_MWh * @ratio,0),
	       
	       @RestOfMonthSO2AlternatePlanValue = ISNULL(SO2_ton * (1-@ratio),0),
	       @RestOfMonthNOxAlternatePlanValue = ISNULL(NOx_ton * (1-@ratio),0),
	       @RestOfMonthNOxSAlternatePlanValue = ISNULL(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * (1-@ratio) ELSE 0 END,0),
	       @RestOfMonthNetGenAlternatePlanValue = ISNULL(NetGeneration_MWh * (1-@ratio),0)
	       
	 From projection.tcsapralternateprojection where unitid=@UnitID and YEAR=YEAR(@DateAsOf) AND MONTH=MONTH(@DateAsOf) and DataVersionID=1
		
	 declare @ProjectionDataVersionID as int
	 
	 select top 1 @ProjectionDataVersionID = MAX(DataVersionID) from Projection.tcsapralternateprojection where year=YEAR(@DateAsOf) and month<=MONTH(@DateAsOf)
	 
	 select @MTDSO2AlternateProjectionValue = ISNULL(SO2_ton * @ratio,0),
	       @MTDNOxAlternateProjectionValue = ISNULL(NOx_ton * @ratio,0),
	       @MTDNOxSAlternateProjectionValue = ISNULL(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * @ratio ELSE 0 END,0),
	       @MTDNetGenAlternateProjectionValue = ISNULL(NetGeneration_MWh * @ratio,0),
	       
	       @RestOfMonthSO2AlternateProjectionValue = ISNULL(SO2_ton * (1-@ratio),0),
	       @RestOfMonthNOxAlternateProjectionValue = ISNULL(NOx_ton * (1-@ratio),0),
	       @RestOfMonthNOxSAlternateProjectionValue = ISNULL(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * (1-@ratio) ELSE 0 END,0),
	       @RestOfMonthNetGenAlternateProjectionValue = ISNULL(NetGeneration_MWh * (1-@ratio),0)
	       
	 From projection.tcsapralternateprojection where unitid=@UnitID and YEAR=YEAR(@DateAsOf) AND MONTH=MONTH(@DateAsOf) and DataVersionID=@ProjectionDataVersionID
	 
	 select 
	 @YTDSO2AlternatePlanValue = ISNULL(SUM(SO2_ton),0) + isnull(@MTDSO2AlternatePlanValue,0),
	 @YTDNOxAlternatePlanValue = ISNULL(SUM(NOx_ton),0) + isnull(@MTDNOxAlternatePlanValue,0),
	 @YTDNOxSAlternatePlanValue = ISNULL(SUM(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * @ratio ELSE 0 END),0) + isnull(@MTDNOxSAlternatePlanValue,0),
	 @YTDNetGenAlternatePlanValue = ISNULL(SUM(NetGeneration_MWh),0) + isnull(@MTDNetGenAlternatePlanValue,0),
	 
	 @RestOfYearSO2AlternatePlanValue = ISNULL(SUM(SO2_ton),0) + isnull(@RestOfMonthSO2AlternatePlanValue,0),
	 @RestOfYearNOxAlternatePlanValue = ISNULL(SUM(NOx_ton),0) + isnull(@RestOfMonthNOxAlternatePlanValue,0),
	 @RestOfYearNOxSAlternatePlanValue = ISNULL(SUM(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * @ratio ELSE 0 END),0) + isnull(@RestOfMonthNOxSAlternatePlanValue,0),
	 @RestOfYearNetGenAlternatePlanValue = ISNULL(SUM(NetGeneration_MWh),0) + isnull(@RestOfMonthNetGenAlternatePlanValue,0)
	  
	 FROM Projection.tcsapralternateprojection where unitid=@UnitID and year=year(@DateAsOf) and month<MONTH(@DateAsOf) and dataversionid=1
	 
	 select 
	 @YTDSO2AlternateProjectedValue = ISNULL(SUM(SO2_ton),0) + isnull(@MTDSO2AlternateProjectionValue,0),
	 @YTDNOxAlternateProjectedValue = ISNULL(SUM(NOx_ton),0) + isnull(@MTDNOxAlternateProjectionValue,0),
	 @YTDNOxSAlternateProjectedValue = ISNULL(SUM(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * @ratio ELSE 0 END),0) + isnull(@MTDNOxSAlternateProjectionValue,0),
	 @YTDNetGenAlternateProjectedValue = ISNULL(SUM(NetGeneration_MWh),0) + isnull(@MTDNetGenAlternateProjectionValue,0),
	 
	 @RestOfYearSO2AlternateProjectionValue = ISNULL(SUM(SO2_ton),0) + isnull(@RestOfMonthSO2AlternateProjectionValue,0),
	 @RestOfYearNOxAlternateProjectionValue = ISNULL(SUM(NOx_ton),0) + isnull(@RestOfMonthNOxAlternateProjectionValue,0),
	 @RestOfYearNOxSAlternateProjectionValue = ISNULL(SUM(CASE WHEN Month BETWEEN 5 AND 9 THEN NOx_ton * @ratio ELSE 0 END),0) + isnull(@RestOfMonthNOxSAlternateProjectionValue,0),
	 @RestOfYearNetGenAlternateProjectionValue = ISNULL(SUM(NetGeneration_MWh),0) + isnull(@RestOfMonthNetGenAlternateProjectionValue,0)
	   
	 FROM Projection.tcsapralternateprojection where unitid=@UnitID and year=year(@DateAsOf) and month<MONTH(@DateAsOf) and dataversionid=@ProjectionDataVersionID
	 
	SELECT Top 1 @SO2EPAAllocation = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Allocation_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @SO2_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc

    SELECT Top 1 @NOxEPAAllocation = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Allocation_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @NOx_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @NOxSEPAAllocation = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Allocation_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @NOxS_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @NetGenEPAAllocation = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Allocation_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @NetGen_MWh_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @SO2AccountBalance = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Balance_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @SO2_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc

    SELECT Top 1 @NOxAccountBalance = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Balance_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @NOx_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @NOxSAccountBalance = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Balance_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @NOxS_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @NetGenAccountBalance = Quantity		
	FROM [Projection].tUITargetUnitDetail
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @CSAPR_Balance_TargetTypyeID
	AND UnitID = @unitID AND UIItemID = @NetGen_MWh_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	if @TryToRetrieveFromCache=1
	  begin
	  
	  declare @tryDate as datetime
	  select 
	  @tryDate = DateAsOf,
	  @MTDSO2StopLightValue = MTDSO2StopLightValue,
	  @CurrentSO2StopLightValue = CurrentSO2StopLightValue,
	  @ProjectedSO2StopLightValue = ProjectedSO2StopLightValue,
	  @MTDSO2MarginPct = MTDSO2MarginPct,
	  @CurrentSO2MarginPct = CurrentSO2MarginPct,
	  @ProjectedSO2MarginPct = ProjectedSO2MarginPct,	  
	  @MTDSO2ActualValue = MTDSO2ActualValue,
	  @YTDSO2ActualValue = YTDSO2ActualValue,
	  @EOYSO2ProjectedValue = EOYSO2ProjectedValue,
	  @MTDSO2PlanValue = MTDSO2PlanValue,
	  @YTDSO2PlanValue = YTDSO2PlanValue,
	  @FullYearSO2PlanValue = FullYearSO2PlanValue,
	  
	  @MTDNOxStopLightValue = MTDNOxStopLightValue,
	  @CurrentNOxStopLightValue = CurrentNOxStopLightValue,
	  @ProjectedNOxStopLightValue = ProjectedNOxStopLightValue,
	  @MTDNOxMarginPct = MTDNOxMarginPct,
	  @CurrentNOxMarginPct = CurrentNOxMarginPct,
	  @ProjectedNOxMarginPct = ProjectedNOxMarginPct,	  
	  @MTDNOxActualValue = MTDNOxActualValue,
	  @YTDNOxActualValue = YTDNOxActualValue,
	  @EOYNOxProjectedValue = EOYNOxProjectedValue,
	  @MTDNOxPlanValue = MTDNOxPlanValue,
	  @YTDNOxPlanValue = YTDNOxPlanValue,
	  @FullYearNOxPlanValue = FullYearNOxPlanValue,
	  
	  @MTDNOxSStopLightValue = MTDNOxSStopLightValue,
	  @CurrentNOxSStopLightValue = CurrentNOxSStopLightValue,
	  @ProjectedNOxSStopLightValue = ProjectedNOxSStopLightValue,
	  @MTDNOxSMarginPct = MTDNOxSMarginPct,
	  @CurrentNOxSMarginPct = CurrentNOxSMarginPct,
	  @ProjectedNOxSMarginPct = ProjectedNOxSMarginPct,	  
	  @MTDNOxSActualValue = MTDNOxSActualValue,
	  @YTDNOxSActualValue = YTDNOxSActualValue,
	  @EOYNOxSProjectedValue = EOYNOxSProjectedValue,
	  @MTDNOxSPlanValue = MTDNOxSPlanValue,
	  @YTDNOxSPlanValue = YTDNOxSPlanValue,
	  @FullYearNOxSPlanValue = FullYearNOxSPlanValue,
	  
	  @MTDNetGenStopLightValue = MTDNetGenStopLightValue,
	  @CurrentNetGenStopLightValue = CurrentNetGenStopLightValue,
	  @ProjectedNetGenStopLightValue = ProjectedNetGenStopLightValue,
	  @MTDNetGenMarginPct = MTDNetGenMarginPct,
	  @CurrentNetGenMarginPct = CurrentNetGenMarginPct,
	  @ProjectedNetGenMarginPct = ProjectedNetGenMarginPct,	  
	  @MTDNetGenActualValue = MTDNetGenActualValue,
	  @YTDNetGenActualValue = YTDNetGenActualValue,
	  @EOYNetGenProjectedValue = EOYNetGenProjectedValue,
	  @MTDNetGenPlanValue = MTDNetGenPlanValue,
	  @YTDNetGenPlanValue = YTDNetGenPlanValue,
	  @FullYearNetGenPlanValue = FullYearNetGenPlanValue
	  
	  from Projection.[tCSAPRCachedStopLightResults] where DateAsOf = @DateAsOf and UnitAssetID = @UnitAssetID and ProjectionScenarioID = @ProjectionScenarioID
	  
	  if @tryDate is not null
	    begin
	    
	    UPDATE Projection.[tCSAPRCachedStopLightResults] SET LastAccessed = GETDATE(), AccessCount = ISNULL(AccessCount,0) + 1 where DateAsOf = @DateAsOf and UnitAssetID = @UnitAssetID
		SET @EOYSO2AlternateProjectedValue = @YTDSO2ActualValue + ISNULL(@RestOfYearSO2AlternateProjectionValue,0)
		SET @EOYNOxAlternateProjectedValue = @YTDNOxActualValue + ISNULL(@RestOfYearNOxAlternateProjectionValue,0)
		SET @EOYNOxSAlternateProjectedValue = @YTDNOxSActualValue + ISNULL(@RestOfYearNOxSAlternateProjectionValue,0)
		SET @EOYNetGenAlternateProjectedValue = @YTDNetGenActualValue + ISNULL(@RestOfYearNetGenAlternateProjectionValue,0)
		 
		SET @EOYSO2AlternatePlanValue = @YTDSO2ActualValue + ISNULL(@RestOfYearSO2AlternatePlanValue,0)
		SET @EOYNOxAlternatePlanValue = @YTDNOxActualValue + ISNULL(@RestOfYearNOxAlternatePlanValue,0)
		SET @EOYNOxSAlternatePlanValue = @YTDNOxSActualValue + ISNULL(@RestOfYearNOxSAlternatePlanValue,0)
		SET @EOYNetGenAlternatePlanValue = @YTDNetGenActualValue + ISNULL(@RestOfYearNetGenAlternatePlanValue,0)
		  
	    return
	    end
	  end
	
	select                      
       @EOYSO2ProjectedValue = SUM(CASE WHEN IsActual = 1 or IsProjection=1 THEN SO2_ton ELSE 0 END),
       @FullYearSO2PlanValue = SUM(CASE WHEN IsBudget=1 THEN SO2_ton ELSE 0 END),
          
       @EOYNOxProjectedValue = SUM(CASE WHEN IsActual = 1 or IsProjection=1 THEN NOx_ton ELSE 0 END),
       @FullYearNOxPlanValue = SUM(CASE WHEN IsBudget=1 THEN NOx_ton ELSE 0 END),
       
       @EOYNOxSProjectedValue = SUM(CASE WHEN IsActual = 1 or IsProjection=1 THEN NOx_S_ton ELSE 0 END),
       @FullYearNOxSPlanValue = SUM(CASE WHEN IsBudget=1 THEN NOx_S_ton ELSE 0 END),
       
       @EOYNetGenProjectedValue = SUM(CASE WHEN IsActual = 1 or IsProjection=1 THEN GEN_MWh ELSE 0 END),
       @FullYearNetGenPlanValue = SUM(CASE WHEN IsBudget=1 THEN GEN_MWh ELSE 0 END)
       
       from [Projection].[ufnCSAPRMonthlyValues](@ProjectionScenarioID,@DateAsOf,YEAR(@DateAsOf),YEAR(@DateAsOf),@UnitAssetID)
 
	
	select 
	   @MTDSO2ActualValue = SUM(CASE WHEN IsActual = 1 AND [MONTH]=MONTH(@DateAsOf) THEN SO2_ton ELSE 0 END),
       @MTDSO2PlanValue = SUM(CASE WHEN IsBudget=1 AND [MONTH]=MONTH(@DateAsOf) THEN SO2_ton ELSE 0 END),
       @YTDSO2ActualValue = SUM(CASE WHEN IsActual = 1 AND [YEAR]=YEAR(@DateAsOf) THEN SO2_ton ELSE 0 END),
       @YTDSO2PlanValue = SUM(CASE WHEN IsBudget=1 AND [YEAR]=YEAR(@DateAsOf) THEN SO2_ton ELSE 0 END),
       
       @MTDNOxActualValue = SUM(CASE WHEN IsActual = 1 AND [MONTH]=MONTH(@DateAsOf) THEN NOx_ton ELSE 0 END),
       @MTDNOxPlanValue = SUM(CASE WHEN IsBudget=1 AND [MONTH]=MONTH(@DateAsOf) THEN NOx_ton ELSE 0 END),
       @YTDNOxActualValue = SUM(CASE WHEN IsActual = 1 AND [YEAR]=YEAR(@DateAsOf) THEN NOx_ton ELSE 0 END),
       @YTDNOxPlanValue = SUM(CASE WHEN IsBudget=1 AND [YEAR]=YEAR(@DateAsOf) THEN NOx_ton ELSE 0 END),
       
       @MTDNOxSActualValue = SUM(CASE WHEN IsActual = 1 AND [MONTH]=MONTH(@DateAsOf) THEN NOx_S_ton ELSE 0 END),
       @MTDNOxSPlanValue = SUM(CASE WHEN IsBudget=1 AND [MONTH]=MONTH(@DateAsOf) THEN NOx_S_ton ELSE 0 END),
       @YTDNOxSActualValue = SUM(CASE WHEN IsActual = 1 AND [YEAR]=YEAR(@DateAsOf) THEN NOx_S_ton ELSE 0 END),
       @YTDNOxSPlanValue = SUM(CASE WHEN IsBudget=1 AND [YEAR]=YEAR(@DateAsOf) THEN NOx_S_ton ELSE 0 END),
       
       @MTDNetGenActualValue = SUM(CASE WHEN IsActual = 1 AND [MONTH]=MONTH(@DateAsOf) THEN Gen_MWh ELSE 0 END),
       @MTDNetGenPlanValue = SUM(CASE WHEN IsBudget=1 AND [MONTH]=MONTH(@DateAsOf) THEN Gen_MWh ELSE 0 END),
       @YTDNetGenActualValue = SUM(CASE WHEN IsActual = 1 AND [YEAR]=YEAR(@DateAsOf) THEN Gen_MWh ELSE 0 END),
       @YTDNetGenPlanValue = SUM(CASE WHEN IsBudget=1 AND [YEAR]=YEAR(@DateAsOf) THEN Gen_MWh ELSE 0 END)
       
        from [Projection].[ufnCSAPRDailyValues](@ProjectionScenarioID,@DateAsOf,YEAR(@DateAsOf),YEAR(@DateAsOf),@UnitAssetID,1)
        
	--Thresholds	
	DECLARE @SO2_YellowLimit_MTD real, @SO2_RedLimit_MTD real, @SO2_YellowLimit_YTD real, @SO2_RedLimit_YTD real,  @SO2_YellowLimit_EOY real, @SO2_RedLimit_EOY real
	DECLARE @NOx_YellowLimit_MTD real, @NOx_RedLimit_MTD real, @NOx_YellowLimit_YTD real, @NOx_RedLimit_YTD real,  @NOx_YellowLimit_EOY real, @NOx_RedLimit_EOY real
	DECLARE @NOxS_YellowLimit_MTD real, @NOxS_RedLimit_MTD real, @NOxS_YellowLimit_YTD real, @NOxS_RedLimit_YTD real,  @NOxS_YellowLimit_EOY real, @NOxS_RedLimit_EOY real
	DECLARE @NetGen_YellowLimit_MTD real, @NetGen_RedLimit_MTD real, @NetGen_YellowLimit_YTD real, @NetGen_RedLimit_YTD real,  @NetGen_YellowLimit_EOY real, @NetGen_RedLimit_EOY real
	
	SELECT Top 1 @SO2_RedLimit_YTD = [BadColorThreshold_fraction],
		@SO2_YellowLimit_YTD = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @SO2_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SET @SO2_RedLimit_MTD = @SO2_RedLimit_YTD
	SET @SO2_YellowLimit_MTD = @SO2_YellowLimit_YTD
	
	SELECT Top 1 @NOx_RedLimit_YTD = [BadColorThreshold_fraction],
		@NOx_YellowLimit_YTD = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @NOx_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SET @NOx_RedLimit_MTD = @NOx_RedLimit_YTD
	SET @NOx_YellowLimit_MTD = @NOx_YellowLimit_YTD
		
	SELECT Top 1 @NOxS_RedLimit_YTD = [BadColorThreshold_fraction],
		@NOxS_YellowLimit_YTD = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @NOxS_ton_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SET @NOxS_RedLimit_MTD = @NOxS_RedLimit_YTD
	SET @NOxS_YellowLimit_MTD = @NOxS_YellowLimit_YTD
	
	SELECT Top 1 @NetGen_RedLimit_YTD = [BadColorThreshold_fraction],
		@NetGen_YellowLimit_YTD = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @NetGen_MWh_UIItemID
	AND StartDate <= @DateAsOf AND EndDate >= @DateAsOf
	ORDER BY StartDate Desc, EndDate Asc
	
	SET @NetGen_RedLimit_MTD = @NetGen_RedLimit_YTD
	SET @NetGen_YellowLimit_MTD = @NetGen_YellowLimit_YTD
	
	SELECT Top 1 @SO2_RedLimit_EOY = [BadColorThreshold_fraction],
		@SO2_YellowLimit_EOY = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @SO2_ton_UIItemID
	AND StartDate <= @AsOfYearLastDay AND EndDate >= @AsOfYearLastDay
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @NOx_RedLimit_EOY = [BadColorThreshold_fraction],
		@NOx_YellowLimit_EOY = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @NOx_ton_UIItemID
	AND StartDate <= @AsOfYearLastDay AND EndDate >= @AsOfYearLastDay
	ORDER BY StartDate Desc, EndDate Asc
		
	SELECT Top 1 @NOxS_RedLimit_EOY = [BadColorThreshold_fraction],
		@NOxS_YellowLimit_EOY = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @NOxS_ton_UIItemID
	AND StartDate <= @AsOfYearLastDay AND EndDate >= @AsOfYearLastDay
	ORDER BY StartDate Desc, EndDate Asc
	
	SELECT Top 1 @NetGen_RedLimit_EOY = [BadColorThreshold_fraction],
		@NetGen_YellowLimit_EOY = [GoodColorThreshold_fraction]
	FROM [Projection].[tUIThresholdUnitDetail]
	WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @UITargetTypeID
	AND UnitID = @unitID AND UIItemID = @NetGen_MWh_UIItemID
	AND StartDate <= @AsOfYearLastDay AND EndDate >= @AsOfYearLastDay
	ORDER BY StartDate Desc, EndDate Asc
	
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @MTDSO2ActualValue, @MTDSO2PlanValue,       @SO2_RedLimit_YTD, @SO2_YellowLimit_YTD, 0,      @MTDSO2MarginPct output, @MTDSO2StopLightValue output
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @MTDNOxActualValue, @MTDNOxPlanValue,       @NOx_RedLimit_YTD, @NOx_YellowLimit_YTD, 0,      @MTDNOxMarginPct output, @MTDNOxStopLightValue output
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @MTDNOxSActualValue, @MTDNOxSPlanValue,     @NOxS_RedLimit_YTD, @NOxS_YellowLimit_YTD, 0,    @MTDNOxSMarginPct output, @MTDNOxSStopLightValue output
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @MTDNetGenActualValue, @MTDNetGenPlanValue, @NetGen_RedLimit_YTD, @NetGen_YellowLimit_YTD,1, @MTDNetGenMarginPct output, @MTDNetGenStopLightValue output
		
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @YTDSO2ActualValue, @YTDSO2PlanValue,       @SO2_RedLimit_YTD, @SO2_YellowLimit_YTD,  0,     @CurrentSO2MarginPct output, @CurrentSO2StopLightValue output
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @YTDNOxActualValue, @YTDNOxPlanValue,       @NOx_RedLimit_YTD, @NOx_YellowLimit_YTD, 0,      @CurrentNOxMarginPct output, @CurrentNOxStopLightValue output
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @YTDNOxSActualValue, @YTDNOxSPlanValue,     @NOxS_RedLimit_YTD, @NOxS_YellowLimit_YTD,  0,   @CurrentNOxSMarginPct output, @CurrentNOxSStopLightValue output
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @YTDNetGenActualValue, @YTDNetGenPlanValue, @NetGen_RedLimit_YTD, @NetGen_YellowLimit_YTD,1, @CurrentNetGenMarginPct output, @CurrentNetGenStopLightValue output
		
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @EOYSO2ProjectedValue, @FullYearSO2PlanValue,    @SO2_RedLimit_EOY, @SO2_YellowLimit_EOY,  0,     @ProjectedSO2MarginPct output, @ProjectedSO2StopLightValue output								   
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @EOYNOxProjectedValue, @FullYearNOxPlanValue,    @NOx_RedLimit_EOY, @NOx_YellowLimit_EOY,   0,    @ProjectedNOxMarginPct output, @ProjectedNOxStopLightValue output	
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @EOYNOxSProjectedValue, @FullYearNOxSPlanValue,   @NOxS_RedLimit_EOY, @NOxS_YellowLimit_EOY, 0,    @ProjectedNOxSMarginPct output, @ProjectedNOxSStopLightValue output							   
	EXEC [Projection].[spComputeSingleCSAPRStoplightValue] @EOYNetGenProjectedValue, @FullYearNetGenPlanValue, @NetGen_RedLimit_EOY, @NetGen_YellowLimit_EOY,1, @ProjectedNetGenMarginPct output, @ProjectedNetGenStopLightValue output

    SET @EOYSO2AlternateProjectedValue = @YTDSO2ActualValue + ISNULL(@RestOfYearSO2AlternateProjectionValue,0)
	SET @EOYNOxAlternateProjectedValue = @YTDNOxActualValue + ISNULL(@RestOfYearNOxAlternateProjectionValue,0)
	SET @EOYNOxSAlternateProjectedValue = @YTDNOxSActualValue + ISNULL(@RestOfYearNOxSAlternateProjectionValue,0)
	SET @EOYNetGenAlternateProjectedValue = @YTDNetGenActualValue + ISNULL(@RestOfYearNetGenAlternateProjectionValue,0)
	  
	SET @EOYSO2AlternatePlanValue = @YTDSO2ActualValue + ISNULL(@RestOfYearSO2AlternatePlanValue,0)
	SET @EOYNOxAlternatePlanValue = @YTDNOxActualValue + ISNULL(@RestOfYearNOxAlternatePlanValue,0)
	SET @EOYNOxSAlternatePlanValue = @YTDNOxSActualValue + ISNULL(@RestOfYearNOxSAlternatePlanValue,0)
	SET @EOYNetGenAlternatePlanValue = @YTDNetGenActualValue + ISNULL(@RestOfYearNetGenAlternatePlanValue,0)	
	
	DELETE FROM [Projection].[tCSAPRCachedStopLightResults] WHERE UnitAssetID = @UnitAssetID AND DateAsOf = @DateAsOf AND ProjectionScenarioID = @ProjectionScenarioID
	
	INSERT INTO [Projection].[tCSAPRCachedStopLightResults]
           ([UnitAssetID]
           ,[DateAsOf]
           ,[ProjectionScenarioID]
           ,[MTDSO2StopLightValue]
           ,[CurrentSO2StopLightValue]
           ,[ProjectedSO2StopLightValue]
           ,[MTDSO2MarginPct]
           ,[CurrentSO2MarginPct]
           ,[ProjectedSO2MarginPct]
           ,[MTDSO2ActualValue]
           ,[YTDSO2ActualValue]
           ,[EOYSO2ProjectedValue]
           ,[MTDSO2PlanValue]
           ,[YTDSO2PlanValue]
           ,[FullYearSO2PlanValue]
           ,[MTDNOxStopLightValue]
           ,[CurrentNOxStopLightValue]
           ,[ProjectedNOxStopLightValue]
           ,[MTDNOxMarginPct]
           ,[CurrentNOxMarginPct]
           ,[ProjectedNOxMarginPct]
           ,[MTDNOxActualValue]
           ,[YTDNOxActualValue]
           ,[EOYNOxProjectedValue]
           ,[MTDNOxPlanValue]
           ,[YTDNOxPlanValue]
           ,[FullYearNOxPlanValue]
           ,[MTDNOxSStopLightValue]
           ,[CurrentNOxSStopLightValue]
           ,[ProjectedNOxSStopLightValue]
           ,[MTDNOxSMarginPct]
           ,[CurrentNOxSMarginPct]
           ,[ProjectedNOxSMarginPct]
           ,[MTDNOxSActualValue]
           ,[YTDNOxSActualValue]
           ,[EOYNOxSProjectedValue]
           ,[MTDNOxSPlanValue]
           ,[YTDNOxSPlanValue]
           ,[FullYearNOxSPlanValue]
           ,[MTDNetGenStopLightValue]
           ,[CurrentNetGenStopLightValue]
           ,[ProjectedNetGenStopLightValue]
           ,[MTDNetGenMarginPct]
           ,[CurrentNetGenMarginPct]
           ,[ProjectedNetGenMarginPct]
           ,[MTDNetGenActualValue]
           ,[YTDNetGenActualValue]
           ,[EOYNetGenProjectedValue]
           ,[MTDNetGenPlanValue]
           ,[YTDNetGenPlanValue]
           ,[FullYearNetGenPlanValue])
     VALUES
           (@UnitAssetID
           ,@DateAsOf
           ,@ProjectionScenarioID
           ,@MTDSO2StopLightValue
           ,@CurrentSO2StopLightValue
           ,@ProjectedSO2StopLightValue
           ,@MTDSO2MarginPct
           ,@CurrentSO2MarginPct
           ,@ProjectedSO2MarginPct
           ,@MTDSO2ActualValue
           ,@YTDSO2ActualValue
           ,@EOYSO2ProjectedValue
           ,@MTDSO2PlanValue
           ,@YTDSO2PlanValue
           ,@FullYearSO2PlanValue
           ,@MTDNOxStopLightValue
           ,@CurrentNOxStopLightValue
           ,@ProjectedNOxStopLightValue
           ,@MTDNOxMarginPct
           ,@CurrentNOxMarginPct
           ,@ProjectedNOxMarginPct
           ,@MTDNOxActualValue
           ,@YTDNOxActualValue
           ,@EOYNOxProjectedValue
           ,@MTDNOxPlanValue
           ,@YTDNOxPlanValue
           ,@FullYearNOxPlanValue
           ,@MTDNOxSStopLightValue
           ,@CurrentNOxSStopLightValue
           ,@ProjectedNOxSStopLightValue
           ,@MTDNOxSMarginPct
           ,@CurrentNOxSMarginPct
           ,@ProjectedNOxSMarginPct
           ,@MTDNOxSActualValue
           ,@YTDNOxSActualValue
           ,@EOYNOxSProjectedValue
           ,@MTDNOxSPlanValue
           ,@YTDNOxSPlanValue
           ,@FullYearNOxSPlanValue
           ,@MTDNetGenStopLightValue
           ,@CurrentNetGenStopLightValue
           ,@ProjectedNetGenStopLightValue
           ,@MTDNetGenMarginPct
           ,@CurrentNetGenMarginPct
           ,@ProjectedNetGenMarginPct
           ,@MTDNetGenActualValue
           ,@YTDNetGenActualValue
           ,@EOYNetGenProjectedValue
           ,@MTDNetGenPlanValue
           ,@YTDNetGenPlanValue
           ,@FullYearNetGenPlanValue)
	
	
END
;
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetCSAPRStoplights] TO [TEUser]
    AS [dbo];

