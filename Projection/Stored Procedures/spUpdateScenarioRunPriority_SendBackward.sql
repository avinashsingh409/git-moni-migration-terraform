﻿

CREATE PROCEDURE [Projection].[spUpdateScenarioRunPriority_SendBackward]
	@ScenarioID INT
	
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ScenarioIDToMoveForward INT
	DECLARE @ScenarioRunToMoveBackInitialPriority FLOAT
	DECLARE @ScenarioRunToMoveForwardInitialPriority FLOAT
	
	--Check to see if there are any scenarios waiting to be run
	IF (SELECT COUNT(*) FROM Projection.tScenarioRun WITH (UPDLOCK, READPAST) WHERE RunStatus=0 AND ScenarioID=@ScenarioID) >= 1	
	BEGIN
		--Get the current priority of the scenario we're delaying. if for some reason there's 
		--more than one run request, get the priority of the last one in the queue. 
		SELECT @ScenarioRunToMoveBackInitialPriority = MAX(RunPriority) 
		FROM Projection.tScenarioRun 
		WHERE ScenarioID = @ScenarioID AND RunStatus = 0
		
		--if there's more than one run request for that scenario, set them all to the same priority, 
		--so we can find the next one in line with some kind of consistency. We'll set this again later.
		if(SELECT COUNT(*) FROM Projection.tScenarioRun WHERE ScenarioID = @ScenarioID AND RunStatus = 0) > 1
		BEGIN
			UPDATE Projection.tScenarioRun SET RunPriority = @ScenarioRunToMoveBackInitialPriority 
			WHERE ScenarioID = @ScenarioID AND RunStatus = 0
		END
		
		--Find the scenario currently after the scenario we want to move, along with its priority.
		--If there's no such scenario...I guess we're done.
		if(SELECT COUNT(*) FROM Projection.tScenarioRun WHERE RunPriority > @ScenarioRunToMoveBackInitialPriority AND RunStatus = 0) >= 1
		BEGIN
			SELECT top 1 @ScenarioRunToMoveForwardInitialPriority = RunPriority, @ScenarioIDToMoveForward = ScenarioID
			FROM Projection.tScenarioRun 
			WHERE RunPriority > @ScenarioRunToMoveBackInitialPriority AND RunStatus = 0
			ORDER BY RunPriority ASC
			
			--Swap the priorities of the run requests for the two scenarios.
			
			UPDATE Projection.tScenarioRun SET RunPriority = @ScenarioRunToMoveForwardInitialPriority WHERE ScenarioID = @ScenarioID AND RunStatus = 0
		
			UPDATE Projection.tScenarioRun SET RunPriority = @ScenarioRunToMoveBackInitialPriority WHERE ScenarioID = @ScenarioIDToMoveForward AND RunStatus = 0
		END
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spUpdateScenarioRunPriority_SendBackward] TO [TEUser]
    AS [dbo];

