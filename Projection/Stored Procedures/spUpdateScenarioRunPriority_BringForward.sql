﻿

CREATE PROCEDURE [Projection].[spUpdateScenarioRunPriority_BringForward]
	@ScenarioID INT
	
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ScenarioIDToMoveBackward INT
	DECLARE @ScenarioRunToMoveBackInitialPriority FLOAT
	DECLARE @ScenarioRunToMoveForwardInitialPriority FLOAT
	
	--Check to see if there are any scenarios waiting to be run
	IF (SELECT COUNT(*) FROM Projection.tScenarioRun WITH (UPDLOCK, READPAST) WHERE RunStatus=0 AND ScenarioID=@ScenarioID) >= 1	
	BEGIN
		--Get the current priority of the scenario we're promoting. if for some reason there's 
		--more than one run request, get the priority of the first one in the queue. 
		SELECT @ScenarioRunToMoveForwardInitialPriority = MIN(RunPriority) 
		FROM Projection.tScenarioRun 
		WHERE ScenarioID = @ScenarioID AND RunStatus = 0
		
		--if there's more than one run request for that scenario, set them all to the same priority, 
		--so we can find the prior one in line with some kind of consistency. We'll set this again later.
		if(SELECT COUNT(*) FROM Projection.tScenarioRun WHERE ScenarioID = @ScenarioID AND RunStatus = 0) > 1
		BEGIN
			UPDATE Projection.tScenarioRun SET RunPriority = @ScenarioRunToMoveForwardInitialPriority 
			WHERE ScenarioID = @ScenarioID AND RunStatus = 0
		END
		
		--Find the scenario currently before the scenario we want to move, along with its priority.
		--If there's no such scenario...I guess we're done.
		if(SELECT COUNT(*) FROM Projection.tScenarioRun WHERE RunPriority < @ScenarioRunToMoveForwardInitialPriority AND RunStatus = 0) >= 1
		BEGIN
			SELECT top 1 @ScenarioRunToMoveBackInitialPriority = RunPriority, @ScenarioIDToMoveBackward = ScenarioID
			FROM Projection.tScenarioRun 
			WHERE RunPriority < @ScenarioRunToMoveForwardInitialPriority AND RunStatus = 0
			ORDER BY RunPriority DESC
			
			--Swap the priorities of the run requests for the two scenarios.
			
			UPDATE Projection.tScenarioRun SET RunPriority = @ScenarioRunToMoveForwardInitialPriority WHERE ScenarioID = @ScenarioIDToMoveBackward AND RunStatus = 0
		
			UPDATE Projection.tScenarioRun SET RunPriority = @ScenarioRunToMoveBackInitialPriority WHERE ScenarioID = @ScenarioID AND RunStatus = 0
		END
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spUpdateScenarioRunPriority_BringForward] TO [TEUser]
    AS [dbo];

