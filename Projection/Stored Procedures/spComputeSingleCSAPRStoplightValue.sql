﻿

CREATE PROCEDURE [Projection].[spComputeSingleCSAPRStoplightValue] 
(
	-- Add the parameters for the function here
	@Value real,
	@Limit real,	
	@RedLimit real,
	@YellowLimit real,
	@ReverseMargin bit,
	@Margin real output,
	@StoplightValue int output,	
	@GrayValue int = 0,
	@GreenValue int = 3,
	@YellowValue int = 2,
	@RedValue int = 1	
)
AS	
BEGIN
	IF @Value is null OR @Limit is null 
	BEGIN
		Set @Margin = null;
		set @StoplightValue = @GrayValue;
		return
	END
	else if @Limit = 0 and @Value = 0
	begin
		set @Margin = null
	    set @StoplightValue = @GrayValue
	    return
	end
	else if @Limit = 0
	begin
		if (@Value>0)
	    begin
			set @Margin = CAST('-3.40E+38' AS real)
	    end
		else
	    begin
			set @Margin = CAST('3.40E+38' AS real)
	    end	  
	end
	else
	begin
		Set @Margin  = (1.0 - (@Value / @Limit) )* 100.0;
	end
	
	if @Margin is not null and @ReverseMargin=1
	  begin
	    set @Margin = -1.0 * @Margin
	  end
	  
	--a positive margin is good--that means the emission is under the limit.
	--a negative margin is bad--that means the emission is over the limit.
	if @margin >= @YellowLimit*100 set @StoplightValue =  @GreenValue
	ELSE  if @margin >= @RedLimit*100 set @StoplightValue =  @YellowValue
	ELSE set @StoplightValue = @RedValue

END
;
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spComputeSingleCSAPRStoplightValue] TO [TEUser]
    AS [dbo];

