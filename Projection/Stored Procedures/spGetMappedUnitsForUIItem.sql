﻿


CREATE PROCEDURE [Projection].[spGetMappedUnitsForUIItem]
	@UIItemID int
AS
BEGIN
	SELECT u.*, map.AppContextID from [Projection].[tUIItemUnitMap] map
	INNER JOIN [Asset].[tUnit] u on map.UnitID = u.UnitID
	WHERE map.UIItemID = @UIItemID
END
;
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetMappedUnitsForUIItem] TO [TEUser]
    AS [dbo];

