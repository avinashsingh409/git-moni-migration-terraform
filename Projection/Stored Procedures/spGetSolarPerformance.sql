﻿CREATE PROCEDURE [Projection].[spGetSolarPerformance]
	@AssetID int = null, 
	@WeatherAssetID int = null,
	@SolarPerformanceSetID int, 
	@StartDate DateTime, 
	@EndDate DateTime
AS
SELECT SolarPerformanceID,
       AssetID,
       AssetDesc,
       WeatherAssetID,
       SolarPerformanceSetID,
       TimeStamp,
       TypicalPOAIrradiance,
       TypicalPowerOutput,
       ForecastPowerOutput
  FROM Projection.vSolarPerformance
  WHERE (AssetID = @AssetID OR @AssetID IS NULL)
    AND (WeatherAssetID = @WeatherAssetID OR @WeatherAssetID IS NULL)
    AND TimeStamp >= @StartDate 
    AND TimeStamp <= @EndDate 
    AND SolarPerformanceSetID = @SolarPerformanceSetID
RETURN 0
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetSolarPerformance] TO [TEUser]
    AS [dbo];

