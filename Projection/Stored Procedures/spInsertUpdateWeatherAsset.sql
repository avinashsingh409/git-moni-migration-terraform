﻿
CREATE PROCEDURE [Projection].spInsertUpdateWeatherAsset
   @WeatherAssetID int,
   @WeatherAssetDesc nvarchar(255),
   @WeatherAssetAbbrev nvarchar(255), 
   @LastCollectionTime DateTime,
   @Enabled bit, 
   @ADDSStationID nvarchar(255),
   @AssetID int,
   @ChangedBy nvarchar(255), 
   @CreatedBy nvarchar(255)
AS
BEGIN

UPDATE Projection.tWeatherAsset
SET LastCollectionTime = @LastCollectionTime,
    ADDSStationID = @ADDSStationID,
    ChangeDate = GETDATE(),
    ChangedBy = @ChangedBy,
    WeatherAssetAbbrev = @WeatherAssetAbbrev,
    WeatherAssetDesc = @WeatherAssetDesc,
    AssetID = @AssetID,
    Enabled = @Enabled
WHERE WeatherAssetID = @WeatherAssetID

if (@@ROWCOUNT = 0)
 BEGIN
  INSERT INTO Projection.tWeatherAsset
  (WeatherAssetAbbrev, 
  WeatherAssetDesc, 
  Enabled, 
  AssetID, 
  CreatedBy, 
  CreateDate,
  ChangedBy, 
  ChangeDate, 
  LastCollectionTime, 
  ADDSStationID)
  values (@WeatherAssetAbbrev, 
    @WeatherAssetDesc, 
    @Enabled, 
    @AssetID, 
    @CreatedBy, 
    GETDATE(),
    @CreatedBy, 
    GETDATE(), 
    @LastCollectionTime, 
    @ADDSStationID)
 END

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spInsertUpdateWeatherAsset] TO [TEUser]
    AS [dbo];

