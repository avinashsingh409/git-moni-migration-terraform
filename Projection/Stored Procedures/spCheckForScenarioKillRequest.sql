﻿

CREATE PROCEDURE [Projection].[spCheckForScenarioKillRequest]
	@KillRequested BIT OUTPUT
,	@KillRequestID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	
	set @KillRequested = 0
	set @KillRequestID = -1
	--Check to see if there are any pending kill requests
	if (SELECT COUNT(*) FROM Projection.tScenarioKills WITH (UPDLOCK, READPAST) WHERE KillProgress = 0) >= 1
	BEGIN
		set @KillRequested = 1
	    --Select the latest kill request and return its ID, so progress and data can be written to it.
		SELECT TOP 1 @KillRequestID = KillRequestID
		FROM Projection.tScenarioKills WITH (UPDLOCK, READPAST)
		WHERE KillProgress = 0
		order by KillRequestID Desc
	
	    --Update the RunStatus and allow it to be processed
		UPDATE Projection.tScenarioKills
		SET KillProgress = 1
		WHERE KillProgress = 0
		
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spCheckForScenarioKillRequest] TO [TEUser]
    AS [dbo];

