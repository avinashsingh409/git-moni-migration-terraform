﻿
CREATE PROCEDURE [Projection].[spGetWeatherForecast]
    @WeatherAssetID int = NULL,
    @StartTime datetime = NULL,
    @EndTime datetime = NULL
AS
BEGIN

SELECT WeatherAssetID, 
       TimeStamp, 
       WeatherIcon,
       DailyMax_degF, 
       DailyMin_degF, 
       Temperature_degF, 
       ApparentTemperature_degF, 
       DewPointTemperature_degF, 
       WindDirection_deg, 
       WindSpeed_MPH, 
       WindSpeedGust_MPH, 
       CloudCover_pct, 
       Precipitation_in, 
       Precipitation12HourProb_pct, 
       Snowfall_in, 
       IceAccumulation_in, 
       RelativeHumidity_pct, 
       MaxRelativeHumidity_pct, 
       MinRelativeHumidity_pct
  FROM [Projection].[tWeatherForecast]
 WHERE (WeatherAssetID = @WeatherAssetID OR @WeatherAssetID IS NULL)
   AND (TimeStamp >= @StartTime OR @StartTime IS NULL)
   AND (TimeStamp <= @EndTime OR @EndTime IS NULL)

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetWeatherForecast] TO [TEUser]
    AS [dbo];

