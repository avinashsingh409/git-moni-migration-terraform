﻿


-- =============================================
-- Author:		Ryan Irwin
-- Create date: May, 2015
-- Description:	Analyzes a set of adjustment sets 
--   with regulatory limit data and extracts that 
--   data into the appropriate legacy tables.
-- =============================================
CREATE PROCEDURE [Projection].[spExtractUITargetDataFromAdjustmentSets] 
	-- Add the parameters for the stored procedure here
	@AdjustmentSetIDs [Base].[tpIntList] READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--DECLARE @AdjustmentSetIDs as Base.tpIntList;
	--INSERT INTO @AdjustmentSetIDs Values (12);
	
	--Start by making sure we have all the plan years necessary covered in tUITargetSet.
	BEGIN
		CREATE TABLE #PlanYearsNeeded (PlanYear smallint not null);
		Insert into #PlanYearsNeeded
		SELECT DISTINCT YEAR(StartDate)
		from @AdjustmentSetIDs adsIDs
		INNER JOIN Projection.tAdjustmentSet adjSet on adjSet.AdjustmentSetID = adsIDs.id
		
		SELECT PlanYear Into #PlanYearsToAdd from #PlanYearsNeeded
		WHERE PlanYear not in (SELECT PlanYear FROM Projection.tUITargetSet);
		
		IF (SELECT COUNT(*) FROM #PlanYearsToAdd) > 0
		BEGIN
			DECLARE PlanYearCursor CURSOR
			FOR Select * FROM #PlanYearsToAdd;
			DECLARE @PlanYear smallint;
			OPEN PlanYearCursor;

			FETCH NEXT FROM PlanYearCursor 
			INTO @PlanYear;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				INSERT INTO [Projection].[tUITargetSet]
					   ([UITargetSetDesc]
					   ,[DataVersionID]
					   ,[PlanYear]
					   ,[EditDate]
					   ,[IsConsensus]
					   ,[CreatedBy]
					   ,[ChangedBy]
					   ,[CreateDate]
					   ,[ChangeDate])
				 VALUES
					   (CONVERT(varchar(4), @PlanYear) + ' Limits and Targets'
					   ,1 --annual budget
					   ,@PlanYear
					   ,SYSDATETIME()
					   ,1
					   ,USER
					   ,USER
					   ,SYSDATETIME()
					   ,SYSDATETIME())
				
				
				FETCH NEXT FROM PlanYearCursor 
				INTO @PlanYear;
			END
		END
	END
	
	
	--At this point, every year covered by the supplied adjustment sets is now 
	--represented in tUITargetSet. Which means we should now be able to add in
	--all the requisite detail records.
	
	
	SELECT [UITargetSetID]
      , u.[UnitID]
      , itui.[UIItemID]
      , itui.[UITargetTypeID]
      , adjDet.EffectiveDate as [StartDate]
      , DateAdd(HOUR, DATEDIFF(Hour,'1900-01-01', adjDet.Duration), adjDet.EffectiveDate) as [EndDate]
      , adjDet.Value [Quantity]
      , isnull(vt.EngUnits,'') as [QuantityUnit]
      , 0 as [Margin] --all the detail records I've seen have had a margin of 0 specified.
      , isnull(vt.EngUnits,'') as [MarginUnit]
      , 1 as [IsRegulatory]
    INTO #NewUITargetUnitDetailsToAdd
    from @AdjustmentSetIDs adsIDs
	INNER JOIN Projection.tAdjustmentSet adjSet on adjSet.AdjustmentSetID = adsIDs.id
	INNER JOIN Projection.tAdjustment adj on adj.AdjustmentSetID = adsIDs.id
	INNER JOIN Projection.tAdjustmentDetail adjDet on adj.AdjustmentID = adjDet.AdjustmentID
	INNER JOIN Projection.tInputTypeInputOptionTypeMap itiotm on itiotm.InputTypeInputOptionTypeMapID = adj.InputTypeInputOptionTypeMapID
	INNER JOIN Projection.tInputType it on itiotm.InputTypeID = it.InputTypeID
	INNER JOIN Asset.tUnit u on u.AssetID = adj.AssetID --for UnitID
	INNER JOIN Projection.tRegulatoryInputTypeUIItemUITargetTypeMap itui ON it.InputTypeID = itui.InputTypeID
	LEFT JOIN ProcessData.tVariableType vt ON vt.PDVariableID = it.VariableTypeID
	INNER JOIN Projection.tUITargetSet uits on uits.PlanYear = YEAR(adjSet.StartDate)
      
	
	SELECT [UITargetSetID]
      , adj.AssetID
	  , adjSet.OwningAssetID
      , itui.[UIItemID]
      , itui.[UITargetTypeID]
      , adjDet.EffectiveDate as [StartDate]
      , DateAdd(HOUR, DATEDIFF(Hour,'1900-01-01', adjDet.Duration), adjDet.EffectiveDate) as [EndDate]
      , adjDet.Value [Quantity]
      , isnull(vt.EngUnits,'') as [QuantityUnit]
      , 0 as [Margin] --all the detail records I've seen have had a margin of 0 specified.
      , isnull(vt.EngUnits,'') as [MarginUnit]
      , 1 as [IsRegulatory]
    INTO #NewUITargetClientDetailsToAdd
    from @AdjustmentSetIDs adsIDs
	INNER JOIN Projection.tAdjustmentSet adjSet on adjSet.AdjustmentSetID = adsIDs.id
	INNER JOIN Projection.tAdjustment adj on adj.AdjustmentSetID = adsIDs.id
	INNER JOIN Projection.tAdjustmentDetail adjDet on adj.AdjustmentID = adjDet.AdjustmentID
	INNER JOIN Projection.tInputTypeInputOptionTypeMap itiotm on itiotm.InputTypeInputOptionTypeMapID = adj.InputTypeInputOptionTypeMapID
	INNER JOIN Projection.tInputType it on itiotm.InputTypeID = it.InputTypeID
	--INNER JOIN Asset.tUnit u on u.AssetID = adj.AssetID --for UnitID
	INNER JOIN Projection.tRegulatoryInputTypeUIItemUITargetTypeMap itui ON it.InputTypeID = itui.InputTypeID
	LEFT JOIN ProcessData.tVariableType vt ON vt.PDVariableID = it.VariableTypeID
	INNER JOIN Projection.tUITargetSet uits on uits.PlanYear = YEAR(adjSet.StartDate)
    WHERE it.AssetClassTypeID = 1000 --asset class type 1000 is 'client'


	SELECT uitud.[UITargetUnitDetailID]
      ,uitud.[UITargetSetID]
      ,uitud.[UnitID]
      ,uitud.[UIItemID]
      ,uitud.[UITargetTypeID]
      ,uitud.[StartDate]
      ,uitud.[EndDate]
      ,toAdd.StartDate NewRecStartDate
      ,toAdd.EndDate NewRecEndDate
      ,uitud.[Quantity]
      ,uitud.[QuantityUnit]
      ,uitud.[Margin]
      ,uitud.[MarginUnit]
      ,uitud.[IsRegulatory]
      INTO #UnitDetailsInConflict
	  FROM [Projection].[tUITargetUnitDetail] uitud
	  INNER JOIN #NewUITargetUnitDetailsToAdd toAdd on toAdd.UITargetSetID = uitud.UITargetSetID
	  WHERE toAdd.UIItemID = uitud.UIItemID
	  AND toAdd.UITargetTypeID = uitud.UITargetTypeID
	  AND toadd.UITargetSetID = uitud.UITargetSetID
	  AND toAdd.UnitID = uitud.UnitID
	  AND toAdd.StartDate < uitud.EndDate
	  AND toAdd.EndDate > uitud.StartDate
  
	SELECT uitcd.[UITargetClientDetailID]
      ,uitcd.[UITargetSetID]
      ,uitcd.AssetID
	  ,toAdd.OwningAssetID
      ,uitcd.[UIItemID]
      ,uitcd.[UITargetTypeID]
      ,uitcd.[StartDate]
      ,uitcd.[EndDate]
      ,toAdd.StartDate NewRecStartDate
      ,toAdd.EndDate NewRecEndDate
      ,uitcd.[Quantity]
      ,uitcd.[QuantityUnit]
      ,uitcd.[Margin]
      ,uitcd.[MarginUnit]
      ,uitcd.[IsRegulatory]
      INTO #ClientDetailsInConflict
	  FROM [Projection].[tUITargetClientDetail] uitcd
	  INNER JOIN #NewUITargetClientDetailsToAdd toAdd on toAdd.UITargetSetID = uitcd.UITargetSetID
	  WHERE toAdd.UIItemID = uitcd.UIItemID
	  AND toAdd.UITargetTypeID = uitcd.UITargetTypeID
	  AND toadd.UITargetSetID = uitcd.UITargetSetID
	  AND toAdd.AssetID = uitcd.AssetID
	  AND toAdd.StartDate < uitcd.EndDate
	  AND toAdd.EndDate > uitcd.StartDate
  
	DECLARE ExistingUnitDetailsInConflictCursor CURSOR
	FOR SELECT * FROM #UnitDetailsInConflict;
	
	DECLARE 
	@UITargetUnitDetailID int,
	@UITargetClientDetailID int,
	@UITargetSetID int,
	@UnitID int,
	@AssetID int,
	@OwningAssetID int,
	@UIItemID int, 
	@UITargetTypeID int,
	@StartDate datetime,
	@EndDate datetime, 
	@NewRecStartDate datetime,
	@NewRecEndDate datetime, 
	@Quantity float, 
	@QuantityUnit nvarchar(20),
	@Margin float,
	@MarginUnit nvarchar(20),
	@IsRegulatory bit;
	
	DECLARE @TargetUnitDetailsToDelete TABLE(ID int not null);
	
	OPEN ExistingUnitDetailsInConflictCursor;
	
	FETCH NEXT FROM ExistingUnitDetailsInConflictCursor 
	INTO @UITargetUnitDetailID,
	@UITargetSetID ,
	@UnitID ,
	@UIItemID , 
	@UITargetTypeID ,
	@StartDate ,
	@EndDate , 	
	@NewRecStartDate ,
	@NewRecEndDate ,	
	@Quantity ,
	@QuantityUnit ,
	@Margin ,
	@MarginUnit ,
	@IsRegulatory ;
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @NewRecStartDate > @StartDate AND @NewRecEndDate < @EndDate 
		BEGIN	--need to split the record into two.
			INSERT INTO #NewUITargetUnitDetailsToAdd
			   ([UITargetSetID],[UnitID],[UIItemID],[UITargetTypeID],[StartDate],[EndDate],[Quantity],[QuantityUnit],[Margin],[MarginUnit],[IsRegulatory])
			VALUES
			   (@UITargetSetID,@UnitID,@UIItemID,@UITargetTypeID,@StartDate,DATEADD(hour, -1, @NewRecStartDate)
			   ,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory)
			   ,
			   (@UITargetSetID,@UnitID,@UIItemID,@UITargetTypeID,DATEADD(hour, 1, @NewRecEndDate)
			   ,@EndDate,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory )	
			   ;
		END
		ELSE IF @NewRecStartDate > @StartDate AND @NewRecEndDate >= @EndDate
		BEGIN 
			INSERT INTO #NewUITargetUnitDetailsToAdd
			   ([UITargetSetID],[UnitID],[UIItemID],[UITargetTypeID],[StartDate],[EndDate],[Quantity],[QuantityUnit],[Margin],[MarginUnit],[IsRegulatory])
			VALUES
			   (@UITargetSetID,@UnitID,@UIItemID,@UITargetTypeID,@StartDate,DATEADD(hour, -1, @NewRecStartDate)
			   ,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory);
		END
		ELSE IF @NewRecStartDate <= @StartDate AND @NewRecEndDate < @EndDate
		BEGIN
			INSERT INTO #NewUITargetUnitDetailsToAdd
			   ([UITargetSetID],[UnitID],[UIItemID],[UITargetTypeID],[StartDate],[EndDate],[Quantity],[QuantityUnit],[Margin],[MarginUnit],[IsRegulatory])
			VALUES
			   (@UITargetSetID,@UnitID,@UIItemID,@UITargetTypeID,DATEADD(hour, 1, @NewRecEndDate)
			   ,@EndDate,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory );
		END
		ELSE IF @NewRecStartDate <= @StartDate AND @NewRecEndDate >= @EndDate
		BEGIN
			DECLARE @noop int; --nothing to do here, the new record completely encompasses the old. We'll just need to delete it.
		END
		ELSE --This clause should never execute, 
		BEGIN
			DECLARE @errmsg varchar(max);
			SET @errmsg ='Detected supposed conflicting target unit detail, but the date logic did not apply properly. ';
			SET @errmsg += 'Supposed conflicting record had UITargetSetID ' + Convert(varchar(5), @UITargetSetID );
			SET @errmsg += ', UnitID ' + Convert(varchar(5), @UnitID );
			SET @errmsg += ', UIItemID ' + Convert(varchar(5), @UIItemID );
			SET @errmsg += ', UITargetTypeID ' + Convert(varchar(5), @UITargetTypeID );
			SET @errmsg += ', StartDate ' + Convert(varchar(12), @StartDate, 101 );
			SET @errmsg += ', EndDate ' + Convert(varchar(12), @EndDate, 101);
			SET @errmsg += ', NewRecStartDate ' + Convert(varchar(12), @NewRecStartDate, 101 );
			SET @errmsg += ', NewRecEndDate ' + Convert(varchar(12), @NewRecEndDate, 101);
			    RAISERROR ( @errmsg, -- Message text.
               16, -- Severity.
               1 -- State.
               );

		END
		
		INSERT INTO @TargetUnitDetailsToDelete Values (@UITargetUnitDetailID);
		
		FETCH NEXT FROM ExistingUnitDetailsInConflictCursor 
		INTO @UITargetUnitDetailID,
		@UITargetSetID ,
		@UnitID ,
		@UIItemID , 
		@UITargetTypeID ,
		@StartDate ,
		@EndDate , 
		@NewRecStartDate ,
		@NewRecEndDate ,
		@Quantity ,
		@QuantityUnit ,
		@Margin ,
		@MarginUnit ,
		@IsRegulatory ;
	END
	
	DECLARE ExistingClientDetailsInConflictCursor CURSOR
	FOR SELECT * FROM #ClientDetailsInConflict;
	
	DECLARE @TargetClientDetailsToDelete TABLE(ID int not null);
	
	OPEN ExistingClientDetailsInConflictCursor;
	
	FETCH NEXT FROM ExistingClientDetailsInConflictCursor 
	INTO @UITargetClientDetailID,
	@UITargetSetID ,
	@AssetID ,
	@OwningAssetID ,
	@UIItemID , 
	@UITargetTypeID ,
	@StartDate ,
	@EndDate , 	
	@NewRecStartDate ,
	@NewRecEndDate ,	
	@Quantity ,
	@QuantityUnit ,
	@Margin ,
	@MarginUnit ,
	@IsRegulatory ;
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @NewRecStartDate > @StartDate AND @NewRecEndDate < @EndDate 
		BEGIN	--need to split the record into two.
			INSERT INTO #NewUITargetClientDetailsToAdd
			   ([UITargetSetID],[AssetID],[OwningAssetID],[UITargetTypeID],[StartDate],[EndDate],[Quantity],[QuantityUnit],[Margin],[MarginUnit],[IsRegulatory])
			VALUES
			   (@UITargetSetID,@AssetID,@OwningAssetID,@UIItemID,@UITargetTypeID,@StartDate,DATEADD(hour, -1, @NewRecStartDate)
			   ,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory)
			   ,
			   (@UITargetSetID,@AssetID,@OwningAssetID,@UIItemID,@UITargetTypeID,DATEADD(hour, 1, @NewRecEndDate)
			   ,@EndDate,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory )	
			   ;
		END
		ELSE IF @NewRecStartDate > @StartDate AND @NewRecEndDate >= @EndDate
		BEGIN 
			INSERT INTO #NewUITargetClientDetailsToAdd
			   ([UITargetSetID],[AssetID],[OwningAssetID],[UITargetTypeID],[StartDate],[EndDate],[Quantity],[QuantityUnit],[Margin],[MarginUnit],[IsRegulatory])
			VALUES
			   (@UITargetSetID,@AssetID,@OwningAssetID,@UIItemID,@UITargetTypeID,@StartDate,DATEADD(hour, -1, @NewRecStartDate)
			   ,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory);
		END
		ELSE IF @NewRecStartDate <= @StartDate AND @NewRecEndDate < @EndDate
		BEGIN
			INSERT INTO #NewUITargetClientDetailsToAdd
			   ([UITargetSetID],[AssetID],[OwningAssetID],[UIItemID],[UITargetTypeID],[StartDate],[EndDate],[Quantity],[QuantityUnit],[Margin],[MarginUnit],[IsRegulatory])
			VALUES
			   (@UITargetSetID,@AssetID,@OwningAssetID,@UIItemID,@UITargetTypeID,DATEADD(hour, 1, @NewRecEndDate)
			   ,@EndDate,@Quantity,@QuantityUnit,@Margin,@MarginUnit,@IsRegulatory );
		END
		ELSE IF @NewRecStartDate <= @StartDate AND @NewRecEndDate >= @EndDate
		BEGIN
			DECLARE @noop2 int; --nothing to do here, the new record completely encompasses the old. We'll just need to delete it.
		END
		ELSE --This clause should never execute, 
		BEGIN
			SET @errmsg ='Detected supposed conflicting target client detail, but the date logic did not apply properly. ';
			SET @errmsg += 'Supposed conflicting record had UITargetSetID ' + Convert(varchar(5), @UITargetSetID );
			SET @errmsg += ', AssetID ' + Convert(varchar(5), @AssetID );
			SET @errmsg += ', UIItemID ' + Convert(varchar(5), @UIItemID );
			SET @errmsg += ', UITargetTypeID ' + Convert(varchar(5), @UITargetTypeID );
			SET @errmsg += ', StartDate ' + Convert(varchar(12), @StartDate, 101 );
			SET @errmsg += ', EndDate ' + Convert(varchar(12), @EndDate, 101);
			SET @errmsg += ', NewRecStartDate ' + Convert(varchar(12), @NewRecStartDate, 101 );
			SET @errmsg += ', NewRecEndDate ' + Convert(varchar(12), @NewRecEndDate, 101);
			    RAISERROR ( @errmsg, -- Message text.
               16, -- Severity.
               1 -- State.
               );

		END
		
		INSERT INTO @TargetClientDetailsToDelete Values (@UITargetClientDetailID);
		
		FETCH NEXT FROM ExistingClientDetailsInConflictCursor 
		INTO @UITargetClientDetailID,
		@UITargetSetID ,
		@AssetID ,
		@OwningAssetID ,
		@UIItemID , 
		@UITargetTypeID ,
		@StartDate ,
		@EndDate , 	
		@NewRecStartDate ,
		@NewRecEndDate ,	
		@Quantity ,
		@QuantityUnit ,
		@Margin ,
		@MarginUnit ,
		@IsRegulatory ;
	END
	
	--SELECT * FROM #NewUITargetUnitDetailsToAdd;
	--SELECT * FROM #DetailsInConflict
	--SELECT * FROM @TargetUnitDetailsToDelete;
	
	BEGIN TRY
		BEGIN TRANSACTION;
		SET XACT_ABORT ON;
		DELETE FROM Projection.tUITargetUnitDetail WHERE UITargetUnitDetailID in (SELECT * FROM @TargetUnitDetailsToDelete);
		INSERT INTO [Projection].[tUITargetUnitDetail]
			   ([UITargetSetID]
			   ,[UnitID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory])
		SELECT [UITargetSetID]
			   ,[UnitID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory]
		 FROM #NewUITargetUnitDetailsToAdd;
		 print 'Unit Delete and Insert Succeeded'

		DELETE FROM Projection.tUITargetClientDetail WHERE UITargetClientDetailID in (SELECT * FROM @TargetClientDetailsToDelete);
		INSERT INTO [Projection].[tUITargetClientDetail]
			   ([UITargetSetID]
			   ,[AssetID]
			   ,[OwningAssetID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory])
		SELECT [UITargetSetID]
			   ,[AssetID]
			   ,[OwningAssetID]
			   ,[UIItemID]
			   ,[UITargetTypeID]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Quantity]
			   ,[QuantityUnit]
			   ,[Margin]
			   ,[MarginUnit]
			   ,[IsRegulatory]
		 FROM #NewUITargetClientDetailsToAdd;
		 print 'Client Delete and Insert Succeeded'
		 print 'Committing Transaction.'
		COMMIT TRANSACTION;	    
	END TRY
	BEGIN CATCH
		 print 'Delete and Insert Failed; Rolling Back Transaction.'
		ROLLBACK TRANSACTION;
	END CATCH;
END
GO

GRANT EXECUTE
    ON OBJECT::[Projection].[spExtractUITargetDataFromAdjustmentSets] TO [TEUser]
    AS [dbo];


GO

