﻿

CREATE PROCEDURE [Projection].[spGetAveragedSolarPerformance]
	@AssetID int = null, 
	@WeatherAssetID int = null,
	@SolarPerformanceSetID int, 
	@StartDate DateTime, 
	@EndDate DateTime
AS
WITH SolarPerformanceRange AS (
    SELECT [AssetID]
        ,[WeatherAssetID]
        ,[SolarPerformanceSetID]
        ,[TimeStamp]
        ,DATEPART(hh, [Timestamp]) as hour
        ,[TypicalPOAIrradiance]
        ,[TypicalPowerOutput]
    FROM [Projection].[vSolarPerformance]
)
SELECT vsp.[SolarPerformanceID]
      ,vsp.[AssetID]
      ,vsp.[AssetDesc]
      ,vsp.[WeatherAssetID]
      ,vsp.[SolarPerformanceSetID]
      ,vsp.[TimeStamp]
      ,TypicalPOAIrradiance = ( SELECT CAST (AVG(spr.[TypicalPOAIrradiance]) AS real)
					  FROM SolarPerformanceRange spr 
					  WHERE vsp.[AssetID] = spr.[AssetID]
						AND vsp.[WeatherAssetID] = spr.[WeatherAssetID]
						AND vsp.[SolarPerformanceSetID] = spr.[SolarPerformanceSetID]
						AND DATEPART(hh, vsp.[Timestamp]) = spr.[hour] 
						AND spr.[Timestamp] BETWEEN DATEADD(dd, -3, vsp.[Timestamp]) 
                            AND DATEADD(dd, 3, vsp.[Timestamp]) )
      ,TypicalPowerOutput = ( SELECT CAST (AVG(spr.[TypicalPowerOutput]) AS real )
					  FROM SolarPerformanceRange spr 
					  WHERE vsp.[AssetID] = spr.[AssetID]
						AND vsp.[WeatherAssetID] = spr.[WeatherAssetID]
						AND vsp.[SolarPerformanceSetID] = spr.[SolarPerformanceSetID]
						AND DATEPART(hh, vsp.[Timestamp]) = spr.[hour] 
						AND spr.[Timestamp] BETWEEN DATEADD(dd, -3, vsp.[Timestamp]) 
                            AND DATEADD(dd, 3, vsp.[Timestamp]) )
      ,vsp.[ForecastPowerOutput]
  FROM [Projection].[vSolarPerformance] vsp
  WHERE (AssetID = @AssetID OR @AssetID IS NULL)
    AND (WeatherAssetID = @WeatherAssetID OR @WeatherAssetID IS NULL)
    AND TimeStamp >= @StartDate 
    AND TimeStamp <= @EndDate 
    AND SolarPerformanceSetID = @SolarPerformanceSetID
RETURN 0
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetAveragedSolarPerformance] TO [TEUser]
    AS [dbo];

