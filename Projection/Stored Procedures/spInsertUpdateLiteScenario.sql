﻿CREATE PROCEDURE [Projection].[spInsertUpdateLiteScenario]
	@ScenarioID int, 
	@OwningAssetID int,
	@ChangedBy nvarchar(255),
	@DataVersionID int,
	@ScenarioDesc nvarchar(255),
	@ScenarioComment nvarchar(max),
	@StartDate datetime2(7),
	@EndDate datetime2(7),
	@IsSensitivity bit,
	@IsConsensus bit,
	@EmissionsRateTable Projection.tpScenarioUnitEmissionsRate READONLY,
	@OutputScenarioID int OUTPUT
	
	
AS
BEGIN
	DECLARE @UnitConfigScheduleSetID int = -1
	DECLARE @FuelPlanSetID int = -1
	DECLARE @GenerationSetID int = -1
	DECLARE @ScenarioTypeID int =  -1
	DECLARE @IsArchived bit = 0
	DECLARE @IsModified  bit = 0
	DECLARE @IsPublic bit =1
	DECLARE @UseDeratedPower bit =0
	DECLARE @RecalculateMaintenancePlan bit= 0
	DECLARE @ElimNegMarginHrs bit =0
	DECLARE @UseVistaMTBFAdj bit =0
	DECLARE @CalcRateBasedEmis bit =0
	DECLARE @UseStartAndEndDate bit =1
	DECLARE @OmitAvailabilityCalcs bit =1
	DECLARE @OmitEconomicCalcs bit =1
	DECLARE @ApplyUnavailability bit =0
	DECLARE @scenarioCount  int = 0
	DECLARE @countRates int =0
	DECLARE @PlanYear int 

	BEGIN TRANSACTION

		SET @PlanYear = YEAR(GETDATE())
		 SELECT @countRates = COUNT(*) 
		 FROM
		 @EmissionsRateTable

		SET @OutputScenarioID = -1
		Select @UnitConfigScheduleSetID= UnitConfigScheduleSetID 
		from Projection.tUnitConfigScheduleSet 
		where OwningAssetID = @OwningAssetID
		and UnitConfigScheduleSetAbbrev = 'N/A'

		IF (@UnitConfigScheduleSetID = -1)
		BEGIN 
		   Insert into Projection.tUnitConfigScheduleSet
		   (UnitConfigScheduleSetDesc, PlanYear,
		   IsConsensus, IsSensitivity, CreatedBy,
		   ChangedBy, CreateDate, ChangeDate,
		   ModifiedDate, IsPublic, IsValid, isarchived,
		   OwningAssetID, UnitConfigScheduleSetAbbrev)
		   values ('No Unit Config', YEAR(GETDATE()), 
		   0, 0, 'BV',
		   'BV', GETDATE(), GETDATE(), 
		   GETDATE(), 1, 1, 0, 
		   @OwningAssetID, 'N/A')
		   Select @UnitConfigScheduleSetID= @@IDENTITY 
		END  
 
  
		Select @FuelPlanSetID = FuelPlanSetID 
		from Projection.tFuelPlanSet
		where OwningAssetID = @OwningAssetID
		and FuelPlanSetAbbrev = 'N/A'

		IF (@FuelPlanSetID = -1)
		BEGIN

		  Insert into Projection.tFuelPlanSet
		  (FuelPlanSetDesc, Comment, PlanYear,
		   IsConsensus, IsSensitivity, CreatedBy,
		   CreateDate, ChangedBy, ChangeDate,
		   ModifiedDate, IsPublic, IsValid,
		   isarchived, OwningAssetID, FuelPlanSetAbbrev)
		   values ('No Fuel Plan', 'No Fuel Plan',YEAR(GETDATE()),
		   0, 0, 'BV', GETDATE(), 'BV', GETDATE(),
		   GETDATE(), 1, 1, 0, @OwningAssetID, 'N/A')
   
		   Select @FuelPlanSetID = @@IDENTITY

		END

		Select @GenerationSetID = GenerationSetID 
		from Projection.tGenerationSet
		where OwningAssetID = @OwningAssetID 
		and GenerationSetAbbrev = 'N/A'

		IF (@GenerationSetID = -1)
		BEGIN
		 insert into Projection.tGenerationSet 
		  (generationsetdesc, DataVersionID, 
		  PlanYear, IsConsensus, IsSensitivity, 
		  CreatedBy, ChangedBy, CreateDate, ModifiedDate,
		  ChangeDate, Comment, IsPublic, isvalid, 
		  IsArchived, GenerationSetType, OwningAssetID, GenerationSetAbbrev)
		  values ('No Generation Set', 1, YEAR(GETDATE()), 
		  1, 0, 'BV', 'BV',
		  GETDATE(), GETDATE(),
		  GETDATE(), 'No Generation Set', 1, 
		  1, 0, 1, @OwningAssetID, 'N/A')
  
		  Select @GenerationSetID = @@IDENTITY
		 END

		Select @ScenarioTypeID = ScenarioTypeID
		from Projection.tScenarioType
		where ScenarioTypeDesc = 'Lite'

		if (@ScenarioID = -1)
		BEGIN
		  Insert into Projection.tScenario 
		  (ScenarioDesc, ScenarioComment,
		  DataVersionID, OwningAssetID, ScenarioTypeID,
		  CreatedBy, ChangedBy,CreateDate, ChangeDate,
		  PlanYear, StartDate, EndDate, 
		  GenerationSetID, UnitConfigScheduleSetID, FuelPlanSetID,
		  IsArchived, IsConsensus, IsModified, IsPublic,
		  IsSensitivity, UseDeratedPower, RecalculateMaintenancePlan,
		  ElimNegMarginHrs, UseVistaMTBFAdj, CalcRateBasedEmis,
		  UseStartAndEndDate, OmitAvailabilityCalcs,
		  OmitEconomicCalcs, ApplyUnavailability,
		  BaseUnitConfigScheduleSetID,BaseGenerationSetID, BaseFuelPlanSetID)
		  values (@ScenarioDesc, @ScenarioComment,
		  @DataVersionID,@OwningAssetID, @ScenarioTypeID,
		  @ChangedBy, @ChangedBy, GETDATE(), GETDATE(),
		  DATEPART(YEAR, @startDate), @StartDate, @EndDate, 
		  @GenerationSetID, @UnitConfigScheduleSetID, @FuelPlanSetID,
		  @IsArchived, @IsConsensus, @IsModified, @IsPublic,
		  @IsSensitivity, @UseDeratedPower, @RecalculateMaintenancePlan,
		  @ElimNegMarginHrs, @UseVistaMTBFAdj, @CalcRateBasedEmis,
		  @UseStartAndEndDate, @OmitAvailabilityCalcs,
		  @OmitEconomicCalcs,@ApplyUnavailability,
		  @UnitConfigScheduleSetID,@GenerationSetID, @FuelPlanSetID)
		  Select @OutputScenarioID = @@IDENTITY
		  SET @ScenarioID = @OutputScenarioID
		 END

		 ELSE
		  BEGIN
			Select @scenarioCount= COUNT(*) from Projection.tScenario
			where ScenarioID = @ScenarioID
			IF (@scenarioCount = 1)
			  BEGIN
			  UPDATE Projection.tScenario 
			  SET 
				ScenarioDesc = @ScenarioDesc, 
				ScenarioComment = @ScenarioComment,
				DataVersionID = @DataVersionID, 
				OwningAssetID = @OwningAssetID, 
				ScenarioTypeID = @ScenarioTypeID,
				ChangedBy = @ChangedBy,
				ChangeDate = GETDATE(),
				PlanYear = DATEPART(YEAR, @startDate), 
				StartDate = @StartDate, 
				EndDate = @EndDate, 
				GenerationSetID = @GenerationSetID, 
				UnitConfigScheduleSetID = @UnitConfigScheduleSetID, 
				FuelPlanSetID = @FuelPlanSetID,
				IsArchived = @IsArchived, 
				IsConsensus = @IsConsensus, 
				IsModified = @IsModified, 
				IsPublic = @IsPublic,
				IsSensitivity = @IsSensitivity, 
				UseDeratedPower = @UseDeratedPower, 
				RecalculateMaintenancePlan = @RecalculateMaintenancePlan,
				ElimNegMarginHrs = @ElimNegMarginHrs, 
				UseVistaMTBFAdj = @UseVistaMTBFAdj, 
				CalcRateBasedEmis = @CalcRateBasedEmis,
				UseStartAndEndDate = @UseStartAndEndDate, 
				OmitAvailabilityCalcs = @OmitAvailabilityCalcs,
				OmitEconomicCalcs = @OmitEconomicCalcs, 
				ApplyUnavailability = @ApplyUnavailability,
				BaseUnitConfigScheduleSetID = @UnitConfigScheduleSetID,
				BaseGenerationSetID = @GenerationSetID,
				BaseFuelPlanSetID = @FuelPlanSetID
				Where ScenarioID = @ScenarioID
        
				SET @OutputScenarioID = @ScenarioID
        
				IF (@countRates > 0)
				BEGIN 
				   DELETE FROM Projection.tScenarioUnitEmissionsRate
				   WHERE ScenarioID = @ScenarioID
				   SET @OutputScenarioID = @ScenarioID
				END
			END
			if (@IsConsensus = 1)
			BEGIN
			UPDATE Projection.tScenario 
			SET IsConsensus = 0
			WHERE OwningAssetID = @OwningAssetID
			  AND PlanYear = @PlanYear
			  AND DataVersionID = 1
			  AND ScenarioID != @OutputScenarioID
			END
		  END
 
 

		 if (@countRates > 0)
		 BEGIN
		   INSERT INTO Projection.tScenarioUnitEmissionsRate 
		   (ScenarioID, UnitID, UIItemID, Rate, Comment, StartDate)
		   SELECT
			@OutputScenarioID,
			E2.UnitID,
			E2.UIItemID,
			E2.Rate,
			E2.Comment,
			E2.StartDate
		   FROM 
			@EmissionsRateTable E2
	
			DELETE FROM Projection.tScenarioUnits 
			WHERE ScenarioID = @OutputScenarioID
	
			Insert Into Projection.tScenarioUnits 
			(ScenarioID, UnitID) 
			Select Distinct @OutputScenarioID, UnitID 
			from @EmissionsRateTable 
		 END

	COMMIT TRANSACTION

	RETURN 1
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spInsertUpdateLiteScenario] TO [TEUser]
    AS [dbo];

