﻿CREATE PROCEDURE [Projection].[spDeleteGenAdjustmentMap]
	@GenerationAdjustmentMapID int
AS
BEGIN
  	DELETE FROM Projection.tGenerationAdjustmentMap WHERE GenerationAdjustmentMapID = @GenerationAdjustmentMapID;
END
GO

GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteGenAdjustmentMap] TO [TEUser]
    AS [dbo];
GO
