﻿CREATE TYPE [Projection].[tpEmissionRollingData] AS TABLE(
	[ScenarioID] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[UIItemID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Quantity] [float] NULL
)




GO
GRANT EXECUTE
    ON TYPE::[Projection].[tpEmissionRollingData] TO [TEUser];



