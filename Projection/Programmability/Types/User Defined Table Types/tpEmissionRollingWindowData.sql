﻿CREATE TYPE [Projection].[tpEmissionRollingWindowData] AS TABLE(
	[EndOfWindow] [date] NOT NULL,
	[DateWithinWindow] [date] NOT NULL,
	[DailySum] [float] NULL,
	[SummedHours] [tinyint] NOT NULL
)



GO
GRANT EXECUTE
    ON TYPE::[Projection].[tpEmissionRollingWindowData] TO [TEUser];



