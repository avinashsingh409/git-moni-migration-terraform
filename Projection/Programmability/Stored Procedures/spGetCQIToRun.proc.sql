﻿CREATE PROCEDURE Projection.spGetCQIToRun
	@CQIRunID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	
	Set @CQIRunID=0
	
	--Check to see if there are any CQI sets waiting to be run
	IF (SELECT COUNT(*) FROM Projection.tCQIRun WITH (UPDLOCK, READPAST) WHERE RunStatus=0) >= 1
	
	BEGIN
	    --Select the CQIRun and lock the record to prevent it from being selected more than once
		SELECT TOP 1 @CQIRunID = CQIRunID 
		FROM Projection.tCQIRun WITH (UPDLOCK, READPAST)
		WHERE RunStatus = 0
	
	    --Update the RunStatus and allow it to be processed
		UPDATE Projection.tCQIRun
		SET RunStatus = 1		
		WHERE CQIRunID = @CQIRunID
		
	END

END



GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetCQIToRun] TO [TEUser]
    AS [dbo];

