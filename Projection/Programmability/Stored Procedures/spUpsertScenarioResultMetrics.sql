﻿CREATE PROCEDURE [Projection].[spUpsertScenarioResultMetrics]
	@SecurityUserID INT
	,@ID INT = NULL
	,@ScenarioID INT
    ,@AssetID INT
    ,@Year INT = NULL
    ,@Quarter INT = NULL
    ,@Month INT = NULL
    ,@Day INT = NULL
    ,@OperatingHours INT 
    ,@FuelBurn_mbtu REAL
    ,@FuelBurn_tons REAL
    ,@Particulate_tons REAL
    ,@GrossMargin_dollars REAL
    ,@TotalRevenue_dollars REAL
    ,@FuelCost_dollars REAL
    ,@TotalVariableOandM_dollars REAL
    ,@Generation_mwh REAL
    ,@MaximumAchievable_Load_mw REAL
    ,@EAF_pct REAL
    ,@ReliabilityIndex_pct REAL
    ,@PlannedMaintenance_days REAL
    ,@BalanceOfPlant_pct REAL
    ,@BoilerTubeFailureRate_pct REAL
    ,@SO2Emissions_tons REAL
    ,@NOxEmissions_tons REAL
    ,@HgEmissions_lbm REAL
    ,@CO2Emissions_ktons REAL
    ,@SO2InletEmissions_tons REAL
    ,@HHV_btu REAL
    ,@MATS_Margin_pct REAL = NULL 
    ,@MATS_SO2_HCl_Margin_pct REAL = NULL
    ,@MATS_Hg_Margin_pct REAL = NULL
    ,@MATS_PM_Margin_pct REAL = NULL
    ,@MATS_SO2_Margin_pct REAL = NULL
    ,@MATS_HCl_Margin_pct REAL = NULL
	,@newID int OUTPUT
AS
BEGIN
	SET @newID = -1

	--Make sure that the convention of treating a -1 as no value is respected
	IF @ID IS NOT NULL AND @ID <= 0
	BEGIN
		SET @ID = NULL; 
	END

	--The table has not been updated to use the securityuser id instead of the string name yet. 
	DECLARE @name NVARCHAR(255);
	if(@securityUserID < 0)
	BEGIN
		SET @name = 'BV'
	END
	ELSE
	BEGIN
		SET @name = (SELECT TOP 1 Username FROM AccessControl.tUser WHERE SecurityUserID = @securityUserID);
	END

	IF @securityUserID < 0 OR 1 = Asset.ufnDoesUserHaveAccessToAsset(@assetID,@securityUserID,-1)
	BEGIN

		IF @ID IS NULL
		BEGIN
		INSERT INTO [Projection].[tScenarioResultMetrics]
			   ([ScenarioID]
			   ,[AssetID]
			   ,[Year]
			   ,[Quarter]
			   ,[Month]
			   ,[Day]
			   ,[OperatingHours]
			   ,[FuelBurn_mbtu]
			   ,[FuelBurn_tons]
			   ,[Particulate_tons]
			   ,[GrossMargin_dollars]
			   ,[TotalRevenue_dollars]
			   ,[FuelCost_dollars]
			   ,[TotalVariableOandM_dollars]
			   ,[Generation_mwh]
			   ,[MaximumAchievable_Load_mw]
			   ,[EAF_pct]
			   ,[ReliabilityIndex_pct]
			   ,[PlannedMaintenance_days]
			   ,[BalanceOfPlant_pct]
			   ,[BoilerTubeFailureRate_pct]
			   ,[SO2Emissions_tons]
			   ,[NOxEmissions_tons]
			   ,[HgEmissions_lbm]
			   ,[CO2Emissions_ktons]
			   ,[SO2InletEmissions_tons]
			   ,[HHV_btu]
			   ,[CreateDate]
			   ,[CreatedBy]
			   ,[ChangeDate]
			   ,[ChangedBy]
			   ,[MATS_Margin_pct]
			   ,[MATS_SO2_HCl_Margin_pct]
			   ,[MATS_Hg_Margin_pct]
			   ,[MATS_PM_Margin_pct]
			   ,[MATS_SO2_Margin_pct]
			   ,[MATS_HCl_Margin_pct])
		 VALUES
			   (@ScenarioID
			   ,@AssetID
			   ,@Year
			   ,@Quarter
			   ,@Month
			   ,@Day
			   ,@OperatingHours
			   ,@FuelBurn_mbtu
			   ,@FuelBurn_tons
			   ,@Particulate_tons
			   ,@GrossMargin_dollars
			   ,@TotalRevenue_dollars
			   ,@FuelCost_dollars
			   ,@TotalVariableOandM_dollars
			   ,@Generation_mwh
			   ,@MaximumAchievable_Load_mw
			   ,@EAF_pct
			   ,@ReliabilityIndex_pct
			   ,@PlannedMaintenance_days
			   ,@BalanceOfPlant_pct
			   ,@BoilerTubeFailureRate_pct
			   ,@SO2Emissions_tons
			   ,@NOxEmissions_tons
			   ,@HgEmissions_lbm
			   ,@CO2Emissions_ktons
			   ,@SO2InletEmissions_tons
			   ,@HHV_btu
			   ,GETDATE()
			   ,@name
			   ,GETDATE()
			   ,@name
			   ,@MATS_Margin_pct
			   ,@MATS_SO2_HCl_Margin_pct
			   ,@MATS_Hg_Margin_pct
			   ,@MATS_PM_Margin_pct
			   ,@MATS_SO2_Margin_pct
			   ,@MATS_HCl_Margin_pct)
			   SET @newID = SCOPE_IDENTITY();
		END
		ELSE
		BEGIN
		UPDATE [Projection].[tScenarioResultMetrics]
	   SET [ScenarioID] = @ScenarioID
		  ,[AssetID] = @AssetID
		  ,[Year] = @Year
		  ,[Quarter] = @Quarter
		  ,[Month] = @Month
		  ,[Day] = @Day
		  ,[OperatingHours] = @OperatingHours
		  ,[FuelBurn_mbtu] = @FuelBurn_mbtu
		  ,[FuelBurn_tons] = @FuelBurn_tons
		  ,[Particulate_tons] = @Particulate_tons
		  ,[GrossMargin_dollars] = @GrossMargin_dollars
		  ,[TotalRevenue_dollars] = @TotalRevenue_dollars
		  ,[FuelCost_dollars] = @FuelCost_dollars
		  ,[TotalVariableOandM_dollars] = @TotalVariableOandM_dollars
		  ,[Generation_mwh] = @Generation_mwh
		  ,[MaximumAchievable_Load_mw] = @MaximumAchievable_Load_mw
		  ,[EAF_pct] = @EAF_pct
		  ,[ReliabilityIndex_pct] = @ReliabilityIndex_pct
		  ,[PlannedMaintenance_days] = @PlannedMaintenance_days
		  ,[BalanceOfPlant_pct] = @BalanceOfPlant_pct
		  ,[BoilerTubeFailureRate_pct] = @BoilerTubeFailureRate_pct
		  ,[SO2Emissions_tons] = @SO2Emissions_tons
		  ,[NOxEmissions_tons] = @NOxEmissions_tons
		  ,[HgEmissions_lbm] = @HgEmissions_lbm
		  ,[CO2Emissions_ktons] = @CO2Emissions_ktons
		  ,[SO2InletEmissions_tons] = @SO2InletEmissions_tons
		  ,[HHV_btu] = @HHV_btu
		  ,[ChangeDate] = GETDATE()
		  ,[ChangedBy] = @name
		  ,[MATS_Margin_pct] = @MATS_Margin_pct
		  ,[MATS_SO2_HCl_Margin_pct] = @MATS_SO2_HCl_Margin_pct
		  ,[MATS_Hg_Margin_pct] = @MATS_Hg_Margin_pct
		  ,[MATS_PM_Margin_pct] = @MATS_PM_Margin_pct
		  ,[MATS_SO2_Margin_pct] = @MATS_SO2_Margin_pct
		  ,[MATS_HCl_Margin_pct] = @MATS_HCl_Margin_pct
		WHERE ID = @ID

	SET @newID = @ID;
	END

	END

	SELECT @newID; 

END
GO


GRANT EXECUTE
    ON OBJECT::[Projection].[spUpsertScenarioResultMetrics] TO [TEUser]
    AS [dbo];