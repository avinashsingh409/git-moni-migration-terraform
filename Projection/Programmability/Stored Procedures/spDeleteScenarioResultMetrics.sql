﻿CREATE PROCEDURE [Projection].[spDeleteScenarioResultMetrics]
	@SecurityUserID INT
	,@ID INT
AS
BEGIN
	DECLARE @assetID INT;
	SELECT @assetID = AssetID from Projection.tScenarioResultMetrics
	WHERE ID = @ID;

	IF @SecurityUserID < 0 OR 1 = Asset.ufnDoesUserHaveAccessToAsset(@assetID,@securityUserID,-1)
	BEGIN
		DELETE FROM Projection.tScenarioResultMetrics WHERE ID = @ID
	END
END
GO

GRANT EXECUTE
    ON OBJECT::[Projection].[spDeleteScenarioResultMetrics] TO [TEUser]
    AS [dbo];
GO