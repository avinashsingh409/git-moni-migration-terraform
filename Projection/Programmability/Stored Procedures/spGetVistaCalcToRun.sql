﻿
CREATE PROCEDURE [Projection].[spGetVistaCalcToRun]
	@EvaluationID BIGINT OUTPUT
,	@BoilerOption INT OUTPUT
,	@ConstantPower INT OUTPUT	
AS
BEGIN
	SET NOCOUNT ON
	
	set @EvaluationID=0
	set @BoilerOption=0
	set @ConstantPower=0
	--Check to see if there are any CQI sets waiting to be run
	IF (SELECT COUNT(*) FROM Projection.tVistaCalcRun WITH (UPDLOCK, READPAST) WHERE RunStatus=0) >= 1
	
	BEGIN
	    --Select the VistCalcRun and lock the record to prevent it from being selected more than once
		SELECT TOP 1 @EvaluationID = EvaluationID, @BoilerOption = BoilerOption, @ConstantPower = ConstantPower
		FROM Projection.tVistaCalcRun WITH (UPDLOCK, READPAST)
		WHERE RunStatus = 0
	
	    --Update the RunStatus and allow it to be processed
		UPDATE Projection.tVistaCalcRun
		SET RunStatus = 1		
		WHERE EvaluationID = @EvaluationID
		
	END

END

GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetVistaCalcToRun] TO [TEUser]
    AS [dbo];

