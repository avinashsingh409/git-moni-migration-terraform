﻿
-- create a stored procedure to update the quantity fields 

CREATE PROC Projection.UpdateQuantities
  @scenarioid int,
  @unitPerfTable varchar(80) = 'projection.tscenariounitperformance'
AS

BEGIN

declare @isLuminant as bit
set @isLuminant = 1
-- later
--,CalcFGDAsh_ton = a.FGDAsh_ton

declare @sql as varchar(4000)
set @sql = 'UPDATE ' + @unitPerfTable + ' SET CalcGypsymSale_ton = a.GypsumSale_ton,CalcScrubberWaste_ton = a.ScrubberWaste_ton,CalcFlyAshSale_ton = a.FlyAshSale_ton,'
set @sql = @sql + ' CalcFlyAshDisposal_ton = a.FlyAshDisposal_ton,CalcBottomAshSale_ton = a.BottomAshSale_ton,CalcBottomAshDisposal_ton = a.BottomAshDisposal_ton'
set @sql = @sql + ' FROM (SELECT a.ScenarioID,a.UnitID,a.Year,a.Month,a.Day,a.Hour, '

-- Luminant

if @isLuminant=1
  begin
  set @sql = @sql + ' Projection.ufnVOMApplySalesMoisture(ISNULL(Gypsum_ton,0),ISNULL(0,0),GypsumSold_percent,0, 1) as GypsumSale_ton,'
  set @sql = @sql + ' Projection.ufnVOMApplySalesMoisture(ISNULL(Gypsum_ton,0),ISNULL(FGDWaste_ton,0),GypsumSold_percent,0, 0) as ScrubberWaste_ton,'
  set @sql = @sql + ' Projection.ufnVOMApplySalesMoisture(ISNULL(SaleableFlyAsh_ton + NonSaleableFlyAsh_ton,0),ISNULL(0,0),FlyAshSold_percent,SaleableFlyAshMoisture_percent, 1) as FlyAshSale_ton,'
  set @sql = @sql + ' Projection.ufnVOMApplySalesMoisture(ISNULL(SaleableFlyAsh_ton + NonSaleableFlyAsh_ton,0),ISNULL(0,0),FlyAshSold_percent,NonSaleableFlyAshMoisture_percent, 0) as FlyAshDisposal_ton,'
  set @sql = @sql + ' Projection.ufnVOMApplySalesMoisture(ISNULL(BottomAsh_ton,0),ISNULL(0,0),BottomAshSold_percent,BottomAshMoisture_percent, 1) as BottomAshSale_ton,'
  set @sql = @sql + ' Projection.ufnVOMApplySalesMoisture(ISNULL(BottomAsh_ton,0),ISNULL(0,0),BottomAshSold_percent,BottomAshMoisture_percent, 0) as BottomAshDisposal_ton'
  end
else
  begin
--Projection.ufnVOMApplySalesMoisture(ISNULL(NonSaleableFlyAsh_ton,0),ISNULL(0,0),0,FGDAshMoisture_percent, 0) as FGDAsh_ton
  
-- OTHER CLIENTS

--Projection.ufnVOMApplySalesMoisture(ISNULL(Gypsum_ton,0),ISNULL(0,0),GypsumSold_percent,0, 1) as GypsumSale_ton,
--Projection.ufnVOMApplySalesMoisture(ISNULL(Gypsum_ton,0),ISNULL(FGDWaste_ton,0),GypsumSold_percent,0, 0) as ScrubberWaste_ton,

--Projection.ufnVOMApplySalesMoisture(ISNULL(SaleableFlyAsh_ton,0),ISNULL(0,0),FlyAshSold_percent,SaleableFlyAshMoisture_percent, 1) as FlyAshSale_ton,
--Projection.ufnVOMApplySalesMoisture(ISNULL(SaleableFlyAsh_ton,0),ISNULL(0,0),FlyAshSold_percent,NonSaleableFlyAshMoisture_percent, 0) as FlyAshDisposal_ton,

--Projection.ufnVOMApplySalesMoisture(ISNULL(BottomAsh_ton,0),ISNULL(0,0),BottomAshSold_percent,BottomAshMoisture_percent, 1) as BottomAshSale_ton,
--Projection.ufnVOMApplySalesMoisture(ISNULL(BottomAsh_ton,0),ISNULL(0,0),BottomAshSold_percent,BottomAshMoisture_percent, 0) as BottomAshDisposal_ton,

--Projection.ufnVOMApplySalesMoisture(ISNULL(NonSaleableFlyAsh_ton,0),ISNULL(0,0),0,FGDAshMoisture_percent, 0) as FGDAsh_ton
print 'none'
  end
  
  set @sql = @sql + ' from ' + @unitPerfTable + ' a join  projection.tUnitConfig b on a.UnitConfigID = b.UnitConfigID) a '
  set @sql = @sql + '   WHERE ' + @unitPerfTable + '.ScenarioID = a.ScenarioID and ' + @unitPerfTable + '.UnitID=a.UnitID and'
  set @sql = @sql + '    ' + @unitPerfTable + '.Year = a.Year and ' + @unitPerfTable + '.Month=a.Month and '
  set @sql = @sql + '   ' + @unitPerfTable + '.Day=a.Day and ' + @unitPerfTable + '.Hour=a.Hour'
  set @sql = @sql + '   AND a.ScenarioID = ' + convert(varchar(50),@ScenarioID)
END

exec (@sql)
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[UpdateQuantities] TO [TEUser]
    AS [dbo];

