﻿-- =============================================
-- Author:		David Gillespie
-- Create date: 06/02/2008
-- Description:	Convert hourly load to daily load buckets with equiv hours.
-- =============================================
CREATE PROCEDURE [Projection].[spGenerateDailyLoadBucket] 
	@GenerationSetID int
AS
BEGIN
	SET NOCOUNT ON;

	--Remove all of the results so they can be rebuilt
	DELETE FROM Projection.tGenerationDailyBucket WHERE GenerationSetID = @GenerationSetID;

	--Initialize the temp table with the base data we need to calculate
	--our load buckets from
	SELECT a.GenerationSetID,a.UnitID,a.[Year],a.[Month],a.[Day],a.[Hour]
	,b.RatedNetCapacity_mw,a.Generation_mw,convert(float(53),0.0) as LoadBucket_mw
	INTO #tmpGenerationBucket
	FROM Projection.tGenerationDetail a INNER JOIN Asset.tUnit b ON a.UnitID = b.UnitID
	WHERE a.GenerationSetID = @GenerationSetID	
	
	--Update the generation with any adjustments (adjustments overwrite)
	UPDATE #tmpGenerationBucket SET Generation_mw = sub.adjust
	FROM (SELECT MAX(b.Adjustment_mw) as adjust,b.UnitID,b.[Year],b.[Month],b.[Day],b.[Hour]
		  FROM Projection.tGenerationAdjustmentMap a INNER JOIN Projection.tGenerationAdjustmentDetail b
 		      ON a.GenerationAdjustmentSetID = b.GenerationAdjustmentSetID
		  WHERE a.GenerationSetID = @GenerationSetID
		  GROUP BY b.UnitID,b.[Year],b.[Month],b.[Day],b.[Hour]) as sub		   
	WHERE #tmpGenerationBucket.UnitID = sub.UnitID
	AND #tmpGenerationBucket.[Year] = sub.[Year]
	AND #tmpGenerationBucket.[Month] = sub.[Month]
	AND #tmpGenerationBucket.[Day] = sub.[Day]
	AND #tmpGenerationBucket.[Hour] = sub.[Hour];

    --Set the generation to the rated net capacity for the unit so 
	--the insert below will work as designed and further calculations will be valid
	--UPDATE #tmpGenerationBucket SET Generation_mw = RatedNetCapacity_mw WHERE Generation_mw > RatedNetCapacity_mw;

	----Calculate the load bucket MW
	/*UPDATE #tmpGenerationBucket SET LoadBucket_mw = sub.bucket
	FROM (SELECT MIN(a.LoadPercent/100.0*b.RatedNetCapacity_mw) as bucket
		  ,b.UnitID,b.[Year],b.[Month],b.[Day],b.[Hour]
		  FROM tCQILoadBucket a INNER JOIN #tmpGenerationBucket b
 		      ON a.UnitID = b.UnitID
		  WHERE b.GenerationSetID = @GenerationSetID
		  AND b.Generation_mw <= (a.LoadPercent/100.0*b.RatedNetCapacity_mw)
		  GROUP BY b.UnitID,b.[Year],b.[Month],b.[Day],b.[Hour]) as sub		   
	WHERE #tmpGenerationBucket.UnitID = sub.UnitID
	AND #tmpGenerationBucket.[Year] = sub.[Year]
	AND #tmpGenerationBucket.[Month] = sub.[Month]
	AND #tmpGenerationBucket.[Day] = sub.[Day]
	AND #tmpGenerationBucket.[Hour] = sub.[Hour];*/ -- Changed to handle loads greater than rated
	UPDATE #tmpGenerationBucket SET LoadBucket_mw = sub.bucket
	FROM (SELECT MIN(a.LoadPercent/100.0*b.RatedNetCapacity_mw) as bucket
		  ,b.UnitID,b.[Year],b.[Month],b.[Day],b.[Hour]
		  FROM Projection.tCQILoadBucket a INNER JOIN #tmpGenerationBucket b ON a.UnitID = b.UnitID
		  WHERE b.GenerationSetID = @GenerationSetID
		  AND b.Generation_mw <= (a.LoadPercent/100.0*b.RatedNetCapacity_mw)
		  GROUP BY b.UnitID,b.[Year],b.[Month],b.[Day],b.[Hour]
		  UNION 
		  SELECT MIN(b.MaxLoadPercent/100.0*a.RatedNetCapacity_mw)
		  ,a.UnitID,a.[Year],a.[Month],a.[Day],a.[Hour]
		  FROM #tmpGenerationBucket a INNER JOIN (SELECT UnitID,MAX(LoadPercent) MaxLoadPercent FROM Projection.tCQILoadBucket GROUP BY UnitID) b ON a.UnitID = b.UnitID
		  WHERE a.GenerationSetID = @GenerationSetID
		  AND a.Generation_mw > (b.MaxLoadPercent/100.0*a.RatedNetCapacity_mw)
		  GROUP BY a.UnitID,a.[Year],a.[Month],a.[Day],a.[Hour]) as sub		   
	WHERE #tmpGenerationBucket.UnitID = sub.UnitID
	AND #tmpGenerationBucket.[Year] = sub.[Year]
	AND #tmpGenerationBucket.[Month] = sub.[Month]
	AND #tmpGenerationBucket.[Day] = sub.[Day]
	AND #tmpGenerationBucket.[Hour] = sub.[Hour];

	--Insert the final values into the permanent table
	INSERT INTO Projection.tGenerationDailyBucket 
		(GenerationSetID,UnitID,[Year],[Month],[Day],LoadBucket_mw,Hours)
	SELECT GenerationSetID,UnitID,[Year],[Month],[Day],
	LoadBucket_mw,SUM(CASE WHEN LoadBucket_mw > 0 THEN Generation_mw/LoadBucket_mw ELSE 0 END) 
	FROM #tmpGenerationBucket
	GROUP BY GenerationSetID,UnitID,[Year],[Month],[Day],LoadBucket_mw;	

	--Drop the temp table
	DROP TABLE #tmpGenerationBucket;
END
