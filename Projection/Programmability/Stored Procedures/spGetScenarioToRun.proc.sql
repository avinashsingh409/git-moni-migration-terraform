﻿



CREATE PROCEDURE [Projection].[spGetScenarioToRun]
	@IsAdHoc BIT = 0,
	@ScenarioID INT OUTPUT
	
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ScenarioRunID INT
	Set @ScenarioID=0
	
	--Check to see if there are any scenarios waiting to be run
	IF (SELECT COUNT(*) FROM Projection.tScenarioRun WITH (UPDLOCK, READPAST) WHERE RunStatus=0 AND AdHoc=@IsAdHoc) >= 1
	
	BEGIN
	    --Select the scenario and lock the record to prevent it from being selected more than once
		SELECT TOP 1 @ScenarioID = ScenarioID, @ScenarioRunID = ScenarioRunID 
		FROM Projection.tScenarioRun WITH (UPDLOCK, READPAST)
		WHERE RunStatus = 0 AND AdHoc = @IsAdHoc
		ORDER BY RunPriority asc, ScenarioRunID asc
	
	    --Update the RunStatus and allow it to be processed
		UPDATE Projection.tScenarioRun
		SET RunStatus = 1, RunPriority = 0.0	
		WHERE ScenarioRunID = @ScenarioRunID
		
		UPDATE Projection.tScenario
		SET RunStatus = 1
		WHERE ScenarioID = @ScenarioID
	END

END









GO
GRANT EXECUTE
    ON OBJECT::[Projection].[spGetScenarioToRun] TO [TEUser]
    AS [dbo];

