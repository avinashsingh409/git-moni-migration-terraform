﻿CREATE FUNCTION [Projection].[ufnGetFlueGasMassFlow]
(
	@FuelBurn_ton real,
	@Moisture_percent real, 
	@Carbon_percent real, 
	@Hydrogen_percent real, 
	@Nitrogen_percent real, 
	@Sulfur_percent real, 
	@Oxygen_percent real,
	@ExcessAir_percent real,
	@Humidity_lbmMlbmDA real
)
RETURNS real
AS
BEGIN
	DECLARE @FlueGasMassFlow_lbmhr real;
	SELECT @FlueGasMassFlow_lbmhr = (@FuelBurn_ton*2000.0*SUM(lbm100lbmFuel)/100.0) 
		FROM ufnGetCombustionTable(@Moisture_percent,@Carbon_percent,@Hydrogen_percent,
		@Nitrogen_percent,@Sulfur_percent,@Oxygen_percent,@ExcessAir_percent,@Humidity_lbmMlbmDA);
	RETURN @FlueGasMassFlow_lbmhr;
END;

GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnGetFlueGasMassFlow] TO [TEUser]
    AS [dbo];

