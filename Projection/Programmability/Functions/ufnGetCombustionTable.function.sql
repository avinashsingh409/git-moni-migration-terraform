﻿CREATE FUNCTION [Projection].[ufnGetCombustionTable]
(
	@Moisture_percent real, 
	@Carbon_percent real, 
	@Hydrogen_percent real, 
	@Nitrogen_percent real, 
	@Sulfur_percent real, 
	@Oxygen_percent real,
	@ExcessAir_percent real,
	@Humidity_lbmMlbmDA real
)
RETURNS @retFlueGas TABLE 
(    
    FGConstituent nvarchar(5) PRIMARY KEY NOT NULL, 
    lbm100lbmFuel real NULL, 
    MolecularWeight real NULL
)
AS 
-- Returns the composition of the resulting flue gas.
BEGIN
    DECLARE 
		@StoichO2 real,
        @StoichAir real,
		@MWC real,
		@MWS real,
		@MWH2 real, 
        @FGConstituent nvarchar(5), 
        @lbmO2100lbmFuel real,
		@MWO2 real, 
        @lbmN2100lbmFuel real, 
		@MWN2 real,
		@lbmCO2100lbmFuel real, 
		@MWCO2 real,
		@lbmSO2100lbmFuel real, 
		@MWSO2 real,
		@lbmH2O100lbmFuel real,
		@MWH2O real  

	SET @MWC = 12.01115;
	SET @MWS = 32.064;
	SET @MWH2 = 2.01594;
	SET @MWN2 = 28.22;
	SET @MWO2 = 31.9988;	
	SET @MWCO2 = 44.00995;
	SET @MWSO2 = 64.0628;
	SET @MWH2O = 18.01534;
	
	--Determine the amount of O2 required for ideal combustion
	SET @StoichO2 = @MWO2 * (@Carbon_percent/@MWC +
					@Hydrogen_percent/@MWH2/2.0 +
					@Sulfur_percent/@MWS -
					@Oxygen_percent/@MWO2);

	--Determine the amount of air required for ideal combustion
	SET @StoichAir = @StoichO2 + @StoichO2 * 0.7685 / 0.2315;

	--Determine the amount of O2 in the flue gas assuming 23.15% of air is O2
	SET @FGConstituent = 'O2';
	SET @lbmO2100lbmFuel = @StoichAir * (@ExcessAir_percent/100.0) * 0.2315;
	INSERT @retFlueGas SELECT @FGConstituent, @lbmO2100lbmFuel, @MWO2;

	--Determine the amount of N2 in the flue gas assuming 76.85% of air is N2
	SET @FGConstituent = 'N2';
	SET @lbmN2100lbmFuel = @Nitrogen_percent + @StoichAir * 0.7685 + @StoichAir * (@ExcessAir_percent/100.0) * 0.7685;
	INSERT @retFlueGas SELECT @FGConstituent, @lbmN2100lbmFuel, @MWN2

	--Determine the amount of CO2 in the flue gas assuming complete combustion
	SET @FGConstituent = 'CO2';
	SET @lbmCO2100lbmFuel = @Carbon_percent + @MWO2 * @Carbon_percent/@MWC;
	INSERT @retFlueGas SELECT @FGConstituent, @lbmCO2100lbmFuel, @MWCO2

	--Determine the amount of SO2 in the flue gas assuming complete combustion
	SET @FGConstituent = 'SO2';
	SET @lbmSO2100lbmFuel = @Sulfur_percent + @MWO2 * @Sulfur_percent/@MWS;
	INSERT @retFlueGas SELECT @FGConstituent, @lbmSO2100lbmFuel, @MWSO2

	--Determine the amount of H2O in the flue gas assuming complete combustion
	SET @FGConstituent = 'H2O';
	SET @lbmH2O100lbmFuel = @StoichAir * @Humidity_lbmMlbmDA +
							@StoichAir * (@ExcessAir_percent/100.0) * @Humidity_lbmMlbmDA +
							@MWO2 * @Hydrogen_percent/@MWH2/2.0 +							
							@Moisture_percent;
	INSERT @retFlueGas SELECT @FGConstituent, @lbmH2O100lbmFuel, @MWH2O	
    RETURN;
END;
