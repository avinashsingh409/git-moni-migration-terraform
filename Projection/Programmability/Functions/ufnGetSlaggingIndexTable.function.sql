﻿CREATE FUNCTION [Projection].[ufnGetSlaggingIndexTable]
(
	@Ash_percent real,
	@NA2O_percent real,
	@CAO_percent real,
	@MGO_percent real,
	@Fe2O3_percent real,	
	@K2O_percent real,
	@SIO2_percent real,
	@AL2O3_percent real,
	@TIO2_percent real,
	@BaseToAcidRatio_fraction real,
	@AshFusionHem_f real,
	@AshFusionID_f real,
	@Sulfur_percent real,
	@Moisture_percent real
)
RETURNS @retSlagging TABLE 
(    
    SlaggingIndex tinyint PRIMARY KEY NOT NULL, 
    SlaggingPotential real NULL 
)
AS 
-- Returns the individual Slagging Index results.
BEGIN
	DECLARE
		@BaseAcidWestern real,
		@IronCalciumWestern real,
		@AshFusionWestern real,
		@T250DolmiteWestern real,
		@BaseAcidEastern real,
		@SilicaFactorEastern real,
		@AttigDuzyEastern real,
		@T250BWEastern real,
		@AEPAll real,
		@MultiViscosityAll real,
		@T250WattFerrAll real,
		@T250HoyRobertsAll real,
		@T250BCURAAll real,
		@HanselHalfingerAll real,
		@Base_percent real,
		@Acid_percent real,
		@ViscosityTemperatureM real,
		@ViscosityTemperatureC real,
		@TempFactor real		

	SET @BaseAcidWestern = 0	
	SET @IronCalciumWestern = 0
	SET @AshFusionWestern = 0
	SET @T250DolmiteWestern = 0 --Not implemented
	SET @BaseAcidEastern = 0
	SET @SilicaFactorEastern = 0
	SET @AttigDuzyEastern = 0
	SET @T250BWEastern = 0 --Not implemented
	SET @AEPAll = 0 --Not implemented
	SET @MultiViscosityAll = 0 --Not implemented
	SET @T250WattFerrAll = 0
	SET @T250HoyRobertsAll = 0
	SET @T250BCURAAll = 0
	SET @HanselHalfingerAll = 0	--Not implemented		

	SET @Base_percent = @Fe2O3_percent + @CAO_percent + @MGO_percent + @NA2O_percent + @K2O_percent
	SET @Acid_percent = @SIO2_percent + @AL2O3_percent + @TIO2_percent
	SET @TempFactor = @SIO2_percent + @AL2O3_percent + @Fe2O3_percent + @CAO_percent + @MGO_percent
	SET @ViscosityTemperatureM = 0.00835 * @SIO2_percent / @TempFactor * 100.0 + 0.00601 * @AL2O3_percent / @TempFactor * 100.0 - 0.109
	SET @ViscosityTemperatureC = 0.04150 * @SIO2_percent / @TempFactor * 100.0 + 0.01920 * @AL2O3_percent / @TempFactor * 100.0 - 3.920
							   + 0.0276 * @Fe2O3_percent / @TempFactor * 100.0 + 0.016 * @CAO_percent / @TempFactor * 100.0
	SET @TempFactor = 0

	--Base Acid Western
	IF @BaseToAcidRatio_fraction is not null AND @BaseToAcidRatio_fraction > 0	
	BEGIN
		SET @BaseAcidWestern = CASE WHEN @BaseToAcidRatio_fraction >= 0.5 AND @BaseToAcidRatio_fraction < 1.0 THEN 4
								    WHEN @BaseToAcidRatio_fraction >= 1.0 THEN 7
								    ELSE 2 END 
	END

	--Iron Calcium Western
	IF @CAO_percent > 0
	BEGIN
		SET @TempFactor = @Fe2O3_percent / @CAO_percent
		SET @IronCalciumWestern = 1
		IF @TempFactor >= 0.15 AND @TempFactor < 5.00
			SET @IronCalciumWestern = 2 
		IF @TempFactor >= 0.30 AND @TempFactor < 3.00
			SET @IronCalciumWestern = 5
		IF @TempFactor >= 0.90 AND @TempFactor < 1.10
			SET @IronCalciumWestern = 8
		IF @TempFactor >= 0.98 AND @TempFactor < 1.02
			SET @IronCalciumWestern = 9						 
	END

	--Ash Fusion Western
	IF @AshFusionID_f > 1000 AND @AshFusionHem_f >= @AshFusionID_f
	BEGIN
		SET @TempFactor = (@AshFusionHem_f + 4 * @AshFusionID_f) / 5
		SET @AshFusionWestern = CASE WHEN @TempFactor < 1900 THEN 2
									 WHEN @TempFactor < 2075 AND @TempFactor >= 1900 THEN 3
								     WHEN @TempFactor < 2125 AND @TempFactor >= 2075 THEN 4
								     WHEN @TempFactor < 2200 AND @TempFactor >= 2125 THEN 5
								     WHEN @TempFactor < 2300 AND @TempFactor >= 2200 THEN 6
								     WHEN @TempFactor < 2400 AND @TempFactor >= 2300 THEN 7
								     WHEN @TempFactor < 2500 AND @TempFactor >= 2400 THEN 8
								     WHEN @TempFactor < 2650 AND @TempFactor >= 2500 THEN 9		   					       
								     ELSE 1 END
	END

	--Base Acid Eastern
	IF @BaseToAcidRatio_fraction is not null AND @BaseToAcidRatio_fraction > 0	
	BEGIN
		SET @BaseAcidEastern = CASE WHEN @BaseToAcidRatio_fraction >= 0.26 AND @BaseToAcidRatio_fraction < 0.28 THEN 5
								    WHEN @BaseToAcidRatio_fraction >= 0.28 THEN 7
								    ELSE 3 END 
	END

	--Silica Factor Eastern
	IF (@SIO2_percent + @Fe2O3_percent + @CAO_percent + @MGO_percent) > 0
	BEGIN
		SET @TempFactor = 100.0 * @SIO2_percent/(@SIO2_percent + @Fe2O3_percent + @CAO_percent + @MGO_percent)		
		SET @SilicaFactorEastern = CASE WHEN @TempFactor < 30 THEN 9 
										WHEN @TempFactor >= 30 AND @TempFactor < 50 THEN 8 
										WHEN @TempFactor >= 50 AND @TempFactor < 65 THEN 6 
										WHEN @TempFactor >= 65 AND @TempFactor < 72 THEN 4 	
										WHEN @TempFactor >= 72 AND @TempFactor < 80 THEN 2 	
										ELSE 1 END 
	END

	--Attig Duzy Eastern
	IF (1.0-@Moisture_percent/100.0) > 0 AND @BaseToAcidRatio_fraction is not null
	BEGIN
		SET @TempFactor = @Sulfur_percent/(1.0-@Moisture_percent/100.0) * @BaseToAcidRatio_fraction
		SET @AttigDuzyEastern = CASE WHEN @TempFactor >= 0.40 AND @TempFactor < 0.55 THEN 2 
									 WHEN @TempFactor >= 0.55 AND @TempFactor < 0.70 THEN 3 
									 WHEN @TempFactor >= 0.70 AND @TempFactor < 1.80 THEN 4 	
									 WHEN @TempFactor >= 1.80 AND @TempFactor < 2.10 THEN 5 	
									 WHEN @TempFactor >= 2.10 AND @TempFactor < 2.50 THEN 6 	
									 WHEN @TempFactor >= 2.50 AND @TempFactor < 2.65 THEN 7 	
									 WHEN @TempFactor >= 2.65 AND @TempFactor < 3.00 THEN 8 	
									 WHEN @TempFactor >= 3.00 THEN 9 	
									 ELSE 1 END 
	END

	--Watt Ferr All
	IF (LOG10(250.0) - @ViscosityTemperatureC) > 0
	BEGIN
		SET @TempFactor = 32.0 + 1.8 * (150.0 + SQRT(10000000.0 * @ViscosityTemperatureM / (LOG10(250.0) - @ViscosityTemperatureC)))
		SET @T250WattFerrAll = CASE WHEN @TempFactor < 2000 THEN 9
									WHEN @TempFactor < 2100 AND @TempFactor >= 2000 THEN 8
									WHEN @TempFactor < 2175 AND @TempFactor >= 2100 THEN 7
									WHEN @TempFactor < 2250 AND @TempFactor >= 2175 THEN 6
									WHEN @TempFactor < 2325 AND @TempFactor >= 2250 THEN 5
									WHEN @TempFactor < 2450 AND @TempFactor >= 2325 THEN 4
									WHEN @TempFactor < 2550 AND @TempFactor >= 2450 THEN 3
									WHEN @TempFactor < 2700 AND @TempFactor >= 2550 THEN 2		   					       
									ELSE 1 END
	END

	--T250 Hoy Roberts All
	IF @AL2O3_percent > 0
	BEGIN
		IF (@SIO2_percent/@AL2O3_percent) <= 2.5
		BEGIN
			SET @TempFactor = 32.0 + 1.8 * (2990.0 - 1470.0 * (@SIO2_percent/@AL2O3_percent) + 360.0 
							* (POWER((@SIO2_percent/@AL2O3_percent), 2)) - 14.7 
							* (@Fe2O3_percent + @CAO_percent + @MGO_percent) 
							+ 0.15 * (POWER((@Fe2O3_percent + @CAO_percent + @MGO_percent), 2)))
			SET @T250HoyRobertsAll = CASE WHEN @TempFactor < 2000 THEN 9
										  WHEN @TempFactor < 2100 AND @TempFactor >= 2000 THEN 8
										  WHEN @TempFactor < 2175 AND @TempFactor >= 2100 THEN 7
										  WHEN @TempFactor < 2250 AND @TempFactor >= 2175 THEN 6
										  WHEN @TempFactor < 2325 AND @TempFactor >= 2250 THEN 5
										  WHEN @TempFactor < 2450 AND @TempFactor >= 2325 THEN 4
										  WHEN @TempFactor < 2550 AND @TempFactor >= 2450 THEN 3
										  WHEN @TempFactor < 2700 AND @TempFactor >= 2550 THEN 2		   					       
										  ELSE 1 END
		END
	END

	--T250 BCURA All
	IF (@SIO2_percent + @Fe2O3_percent + @CAO_percent + @MGO_percent) > 0
	BEGIN		
		SET @TempFactor = (12650.0 / ((LOG10(250.0)) - 4.468
                         * (POWER(@SIO2_percent/(@SIO2_percent + @Fe2O3_percent + @CAO_percent + @MGO_percent), 2)) + 7.44))
                         * 1.8 - 460.0;
		SET @T250BCURAAll = CASE WHEN @TempFactor < 2000 THEN 9
								 WHEN @TempFactor < 2100 AND @TempFactor >= 2000 THEN 8
								 WHEN @TempFactor < 2175 AND @TempFactor >= 2100 THEN 7
								 WHEN @TempFactor < 2250 AND @TempFactor >= 2175 THEN 6
								 WHEN @TempFactor < 2325 AND @TempFactor >= 2250 THEN 5
								 WHEN @TempFactor < 2450 AND @TempFactor >= 2325 THEN 4
								 WHEN @TempFactor < 2550 AND @TempFactor >= 2450 THEN 3
								 WHEN @TempFactor < 2700 AND @TempFactor >= 2550 THEN 2		   					       
								 ELSE 1 END		
	END

	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential) 
	VALUES (1,@BaseAcidWestern)	
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (3,@IronCalciumWestern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (4,@AshFusionWestern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (5,@T250DolmiteWestern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (7,@BaseAcidEastern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (8,@SilicaFactorEastern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (9,@AttigDuzyEastern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (10,@T250BWEastern)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (11,@AEPAll)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (12,@MultiViscosityAll)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (13,@T250WattFerrAll)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (14,@T250HoyRobertsAll)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (15,@T250BCURAAll)
	INSERT INTO @retSlagging (SlaggingIndex,SlaggingPotential)
	VALUES (16,@HanselHalfingerAll)

	RETURN;
END
