﻿CREATE FUNCTION [Projection].[ufnGetFoulingIndexTable]
(
	@Ash_percent real,
	@NA2O_percent real,
	@CAO_percent real,
	@MGO_percent real,
	@Fe2O3_percent real,	
	@K2O_percent real,
	@SIO2_percent real,
	@AL2O3_percent real,
	@TIO2_percent real
)
RETURNS @retFouling TABLE 
(    
    FoulingIndex tinyint PRIMARY KEY NOT NULL, 
    FoulingPotential real NOT NULL 
)
AS 
-- Returns the individual Fouling Index results.
BEGIN
	DECLARE
		@MaxSodiumWestern real,
		@SodiumCaOWestern real,
		@MaxAshSodiumWestern real,
		@SodiumContentEastern real,
		@SodiumK2OEastern real,
		@BaseAcidSodiumEastern real,
		@AlkaliFactorEastern real,
		@TempFactor real		

	SET @MaxSodiumWestern = 0
	SET @SodiumCaOWestern = 0
	SET @MaxAshSodiumWestern = 0
	SET @SodiumContentEastern = 0
	SET @SodiumK2OEastern = 0
	SET @BaseAcidSodiumEastern = 0
	SET @AlkaliFactorEastern = 0
	SET @TempFactor = 0		

	--Max Sodium Western
	IF (@CAO_percent + @MGO_percent + @Fe2O3_percent) >= 20 
	BEGIN
		SET @MaxSodiumWestern = CASE WHEN @NA2O_percent >= 1.0 AND @NA2O_percent < 1.8 THEN 2
								     WHEN @NA2O_percent >= 1.8 AND @NA2O_percent < 2.2 THEN 3
								     WHEN @NA2O_percent >= 2.2 AND @NA2O_percent < 2.8 THEN 4
								     WHEN @NA2O_percent >= 2.8 AND @NA2O_percent < 3.6 THEN 5
								     WHEN @NA2O_percent >= 3.6 AND @NA2O_percent < 5.6 THEN 6
								     WHEN @NA2O_percent >= 5.6 AND @NA2O_percent < 7.0 THEN 7
								     WHEN @NA2O_percent >= 7.0 AND @NA2O_percent < 8.0 THEN 8
								     WHEN @NA2O_percent >= 8.0 THEN 8
								     ELSE 1 END 
	END
	ELSE
	BEGIN
		SET @MaxSodiumWestern = CASE WHEN @NA2O_percent >= 0.6 AND @NA2O_percent < 0.9 THEN 2
								     WHEN @NA2O_percent >= 0.9 AND @NA2O_percent < 1.1 THEN 3
								     WHEN @NA2O_percent >= 1.1 AND @NA2O_percent < 1.5 THEN 4
								     WHEN @NA2O_percent >= 1.5 AND @NA2O_percent < 2.4 THEN 5
								     WHEN @NA2O_percent >= 2.4 AND @NA2O_percent < 3.6 THEN 6
								     WHEN @NA2O_percent >= 3.6 AND @NA2O_percent < 5.0 THEN 7
								     WHEN @NA2O_percent >= 5.0 AND @NA2O_percent < 6.5 THEN 8
								     WHEN @NA2O_percent >= 6.5 THEN 9
								     ELSE 1 END 
	END

	--Sodium CaO Western
	IF @CAO_percent < 15 
	BEGIN
		SET @TempFactor = @NA2O_percent + 0.05
	END
	ELSE
	BEGIN
		SET @TempFactor = @NA2O_percent - 0.05
	END

	SET @SodiumCaOWestern = CASE WHEN @TempFactor >= 0.50 AND @TempFactor < 0.75 THEN 2
							     WHEN @TempFactor >= 0.75 AND @TempFactor < 1.25 THEN 3
							     WHEN @TempFactor >= 1.25 AND @TempFactor < 1.75 THEN 4
							     WHEN @TempFactor >= 1.75 AND @TempFactor < 2.25 THEN 5
							     WHEN @TempFactor >= 2.25 AND @TempFactor < 2.75 THEN 6
							     WHEN @TempFactor >= 2.75 AND @TempFactor < 3.25 THEN 7
							     WHEN @TempFactor >= 3.25 AND @TempFactor < 4.00 THEN 8
							     WHEN @TempFactor >= 4.00 THEN 9
							     ELSE 1 END 

	--Max Ash Sodium Western
	SET @MaxAshSodiumWestern = CASE WHEN @Ash_percent >= 5  AND @Ash_percent < 10 AND @NA2O_percent < 1.0 THEN 2
									WHEN @Ash_percent >= 10 AND @Ash_percent < 14 AND @NA2O_percent < 1.0 THEN 3
									WHEN @Ash_percent >= 14 AND @Ash_percent < 18 AND @NA2O_percent < 1.0 THEN 4
									WHEN @Ash_percent >= 18 AND @Ash_percent < 23 AND @NA2O_percent < 1.0 THEN 5
									WHEN @Ash_percent >= 23 AND @Ash_percent < 28 AND @NA2O_percent < 1.0 THEN 6
									WHEN @Ash_percent >= 28 AND @Ash_percent < 34 AND @NA2O_percent < 1.0 THEN 7
									WHEN @Ash_percent >= 34 AND @Ash_percent < 41 AND @NA2O_percent < 1.0 THEN 8
									WHEN @Ash_percent >= 41 AND @NA2O_percent < 1.0 THEN 9
									WHEN @Ash_percent >= 4  AND @Ash_percent < 7  AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 2
									WHEN @Ash_percent >= 7  AND @Ash_percent < 10 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 3
									WHEN @Ash_percent >= 10 AND @Ash_percent < 13 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 4
									WHEN @Ash_percent >= 13 AND @Ash_percent < 17 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 5
									WHEN @Ash_percent >= 17 AND @Ash_percent < 21 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 6
									WHEN @Ash_percent >= 21 AND @Ash_percent < 26 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 7
									WHEN @Ash_percent >= 26 AND @Ash_percent < 32 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 8
									WHEN @Ash_percent >= 32 AND @NA2O_percent >= 1.0 AND @NA2O_percent <= 3.0 THEN 9
									WHEN @Ash_percent >= 3  AND @Ash_percent < 5  AND @NA2O_percent > 3.0 THEN 2						
									WHEN @Ash_percent >= 5  AND @Ash_percent < 7  AND @NA2O_percent > 3.0 THEN 3
									WHEN @Ash_percent >= 7  AND @Ash_percent < 9  AND @NA2O_percent > 3.0 THEN 4
									WHEN @Ash_percent >= 9  AND @Ash_percent < 11 AND @NA2O_percent > 3.0 THEN 5
									WHEN @Ash_percent >= 11 AND @Ash_percent < 14 AND @NA2O_percent > 3.0 THEN 6
									WHEN @Ash_percent >= 14 AND @Ash_percent < 17 AND @NA2O_percent > 3.0 THEN 7
									WHEN @Ash_percent >= 17 AND @Ash_percent < 21 AND @NA2O_percent > 3.0 THEN 8
									WHEN @Ash_percent >= 21 AND @NA2O_percent > 3.0 THEN 9
									ELSE 1 END 
	
	--Sodium Content Eastern
	SET @SodiumContentEastern = CASE WHEN @NA2O_percent >= 0.2 AND @NA2O_percent < 0.4 THEN 2
								     WHEN @NA2O_percent >= 0.4 AND @NA2O_percent < 0.6 THEN 3
								     WHEN @NA2O_percent >= 0.6 AND @NA2O_percent < 0.9 THEN 4
								     WHEN @NA2O_percent >= 0.9 AND @NA2O_percent < 1.5 THEN 5
								     WHEN @NA2O_percent >= 1.5 AND @NA2O_percent < 2.2 THEN 6
								     WHEN @NA2O_percent >= 2.2 AND @NA2O_percent < 2.9 THEN 7
								     WHEN @NA2O_percent >= 2.9 AND @NA2O_percent < 3.7 THEN 8
								     WHEN @NA2O_percent >= 3.7 THEN 9
								     ELSE 1 END 

	--Sodium K2O Eastern
	SET @TempFactor = 0	
	IF (@NA2O_percent+@K2O_percent) > 0 
	BEGIN
		SET @TempFactor = @NA2O_percent/(@NA2O_percent+@K2O_percent)
	END
	SET @SodiumK2OEastern = CASE WHEN @TempFactor >= 0.50 AND @TempFactor < 0.75 THEN 2
							     WHEN @TempFactor >= 0.75 AND @TempFactor < 1.25 THEN 3
							     WHEN @TempFactor >= 1.25 AND @TempFactor < 1.75 THEN 4
							     WHEN @TempFactor >= 1.75 AND @TempFactor < 2.25 THEN 5
							     WHEN @TempFactor >= 2.25 AND @TempFactor < 2.75 THEN 6
							     WHEN @TempFactor >= 2.75 AND @TempFactor < 3.25 THEN 7
							     WHEN @TempFactor >= 3.25 AND @TempFactor < 4.00 THEN 8
							     WHEN @TempFactor >= 4.00 THEN 9
							     ELSE 1 END 
	
	--Base Acid Sodium Eastern	
	SET @TempFactor = 0
	IF (@SIO2_percent +	@AL2O3_percent + @TIO2_percent) > 0 
	BEGIN
		SET @TempFactor = ((@NA2O_percent+@CAO_percent+@MGO_percent+@Fe2O3_percent+@K2O_percent)/(@SIO2_percent +	@AL2O3_percent + @TIO2_percent)) * @NA2O_percent 
	END
	SET @BaseAcidSodiumEastern = CASE WHEN @TempFactor >= 0.05 AND @TempFactor < 0.13 THEN 2
								      WHEN @TempFactor >= 0.13 AND @TempFactor < 0.28 THEN 3
									  WHEN @TempFactor >= 0.28 AND @TempFactor < 0.45 THEN 4
									  WHEN @TempFactor >= 0.45 AND @TempFactor < 0.65 THEN 5
								      WHEN @TempFactor >= 0.65 AND @TempFactor < 0.87 THEN 6
									  WHEN @TempFactor >= 0.87 AND @TempFactor < 1.10 THEN 7
								      WHEN @TempFactor >= 1.10 AND @TempFactor < 1.50 THEN 8
								      WHEN @TempFactor >= 1.50 THEN 9
								      ELSE 1 END 

	--Base Acid Sodium Eastern	
	SET @TempFactor = (@Ash_percent * (@NA2O_percent + 0.659*@K2O_percent))/100.0
	SET @AlkaliFactorEastern = CASE WHEN @TempFactor >= 0.1500 AND @TempFactor < 0.2625 THEN 2
							        WHEN @TempFactor >= 0.2625 AND @TempFactor < 0.3375 THEN 3
							        WHEN @TempFactor >= 0.3375 AND @TempFactor < 0.4125 THEN 4
							        WHEN @TempFactor >= 0.4125 AND @TempFactor < 0.4875 THEN 5
							        WHEN @TempFactor >= 0.4875 AND @TempFactor < 0.5625 THEN 6
							        WHEN @TempFactor >= 0.5625 AND @TempFactor < 0.6375 THEN 7
							        WHEN @TempFactor >= 0.6375 AND @TempFactor < 0.7500 THEN 8
							        WHEN @TempFactor >= 0.7500 THEN 9
									ELSE 1 END 

	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (2,@MaxSodiumWestern)
	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (3,@SodiumCaOWestern)
	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (4,@MaxAshSodiumWestern)
	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (7,@SodiumContentEastern)
	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (8,@SodiumK2OEastern)
	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (9,@BaseAcidSodiumEastern)
	INSERT INTO @retFouling (FoulingIndex,FoulingPotential) 
	VALUES (10,@AlkaliFactorEastern)
    RETURN;
END
