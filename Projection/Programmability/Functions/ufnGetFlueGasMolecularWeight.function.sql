﻿CREATE FUNCTION [Projection].[ufnGetFlueGasMolecularWeight]
(	
	@Moisture_percent real, 
	@Carbon_percent real, 
	@Hydrogen_percent real, 
	@Nitrogen_percent real, 
	@Sulfur_percent real, 
	@Oxygen_percent real,
	@ExcessAir_percent real,
	@Humidity_lbmMlbmDA real
)
RETURNS real
AS
BEGIN
	DECLARE @FlueGasMW real;
	SELECT @FlueGasMW = SUM(CASE WHEN Moles > 0 THEN MolecularWeight*(lbm100lbmFuel*MolecularWeight/Moles) ELSE 0 END)
		FROM ufnGetCombustionTable(@Moisture_percent,@Carbon_percent,@Hydrogen_percent,
		@Nitrogen_percent,@Sulfur_percent,@Oxygen_percent,@ExcessAir_percent,@Humidity_lbmMlbmDA)
		CROSS JOIN
		(SELECT SUM(lbm100lbmFuel*MolecularWeight) Moles
		FROM ufnGetCombustionTable(@Moisture_percent,@Carbon_percent,@Hydrogen_percent,
		@Nitrogen_percent,@Sulfur_percent,@Oxygen_percent,@ExcessAir_percent,@Humidity_lbmMlbmDA)) SUB
	RETURN @FlueGasMW;
END;

GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnGetFlueGasMolecularWeight] TO [TEUser]
    AS [dbo];

