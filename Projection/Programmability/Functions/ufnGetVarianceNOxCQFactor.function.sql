﻿CREATE FUNCTION [Projection].[ufnGetVarianceNOxCQFactor]
(
	@FuelBurn_ton real,
	@HHV_btulbm real,
	@Ash_percent real,
	@Moisture_percent real, 
	@Carbon_percent real, 
	@Hydrogen_percent real, 
	@Nitrogen_percent real, 
	@Sulfur_percent real, 
	@Oxygen_percent real,
	@VolatileMatter_percent real,
	@ExcessAir_percent real,
	@Humidity_lbmMlbmDA real
)
RETURNS real
AS
BEGIN
	DECLARE @FlueGasMassFlow_lbmhr real;
	DECLARE @FlueGasMW real;
	DECLARE @NOx_ppm real;
	DECLARE @NOx_lbmmbtu real;
	DECLARE @DAFVM real;
	DECLARE @DAFN2 real;

	SELECT @FlueGasMassFlow_lbmhr = dbo.ufnGetFlueGasMassFlow(@FuelBurn_ton,@Moisture_percent, 
		@Carbon_percent,@Hydrogen_percent,@Nitrogen_percent,@Sulfur_percent,@Oxygen_percent,
		@ExcessAir_percent,@Humidity_lbmMlbmDA);

	SELECT @FlueGasMW = dbo.ufnGetFlueGasMolecularWeight(@Moisture_percent, 
		@Carbon_percent,@Hydrogen_percent,@Nitrogen_percent,@Sulfur_percent,@Oxygen_percent,
		@ExcessAir_percent,@Humidity_lbmMlbmDA);

	IF @FlueGasMassFlow_lbmhr > 0 AND @FlueGasMW > 0 AND (@Moisture_percent + @Ash_percent) < 100.0 
	   AND @FuelBurn_ton > 0 AND @HHV_btulbm > 0
	BEGIN
		SET @DAFN2 = @Nitrogen_percent / (100.0 - @Moisture_percent - @Ash_percent) / 100.0;
		SET @DAFVM = @VolatileMatter_percent / (100.0 - @Moisture_percent - @Ash_percent) / 100.0;
		SET @NOx_ppm = (@Nitrogen_percent/100.0) * (@FuelBurn_ton * 2000.0) * 1000000.0 * @FlueGasMW /
					   (@FlueGasMassFlow_lbmhr * (14.11)); --1 mole of N = 1 mole of NOx
		SET @NOx_ppm = 150.0 + 80.0 * (@DAFN2 / 1.5) + (166.67 - 96.67 * (@DAFVM / 40.0)) * (@NOx_ppm / 3200.0);
		SET @NOx_lbmmbtu = (((@NOx_ppm * (@FlueGasMassFlow_lbmhr/@FlueGasMW)) / 1000000.0)*(14.11+31.9988)) / 
						   ((@FuelBurn_ton * 2000.0)*@HHV_btulbm/1000000.0); --Assume all NO2
	END	
	ELSE
	BEGIN
		SET @NOx_lbmmbtu = 0.0;
	END	

	RETURN @NOx_lbmmbtu;
END;

GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnGetVarianceNOxCQFactor] TO [TEUser]
    AS [dbo];

