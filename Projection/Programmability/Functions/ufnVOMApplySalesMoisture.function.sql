﻿
CREATE FUNCTION [Projection].[ufnVOMApplySalesMoisture]
(
	@SaleData real,
	@DisposalData real,
	@Sales_percent real,
	@Moisture_percent real,
	@ReturnSaleData bit 
)
RETURNS real
AS
BEGIN
	DECLARE @FinalReturnData real;
	
	IF @ReturnSaleData = 1
	BEGIN
		SET @FinalReturnData = @SaleData * @Sales_percent / 100.0		
	END
	ELSE 
	BEGIN
		SET @FinalReturnData = @DisposalData + (@SaleData * (100.0 - @Sales_percent)/100.0) 		
	END

	SET @FinalReturnData = @FinalReturnData * (100.0 + @Moisture_percent)/100.0
	
	RETURN @FinalReturnData
END


GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnVOMApplySalesMoisture] TO [TEUser]
    AS [dbo];

