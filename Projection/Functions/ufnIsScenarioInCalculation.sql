﻿
-- =============================================
-- Author:		Ryan Irwin
-- Create date: 3 November 2014
-- Description:	Checks the supplied scenario ID
-- against the set of running scenarios, to see
-- if that scenario is currently calculating or
-- in the queue for calculation.
-- =============================================
CREATE FUNCTION [Projection].[ufnIsScenarioInCalculation]
(
	-- Add the parameters for the function here
	@ScenarioID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit
	SET @result = 0 --until otherwise determined.
	
	DECLARE @count int
	SELECT @count = COUNT(*) 
	FROM Projection.tScenario s
	JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
	WHERE s.ScenarioID = @ScenarioID
	AND sr.RunStatus >= 0 AND sr.RunStatus < 3
	
	if @count > 0
	BEGIN
		set @result = 1
	END
		
	-- Return the result of the function
	RETURN @result

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnIsScenarioInCalculation] TO [TEUser]
    AS [dbo];

