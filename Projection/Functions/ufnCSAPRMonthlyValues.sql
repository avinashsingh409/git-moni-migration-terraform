﻿
CREATE FUNCTION [Projection].[ufnCSAPRMonthlyValues]
(
 @scenarioId as int,
 @dateAsOf as DateTime,
 @startYear as int,
 @endYear as int,
 @assetId as int
)
RETURNS @results TABLE
(  
  [year] int,
  [month] int,  
  SO2_ton float,
  NOx_ton float,
  NOx_S_ton float,
  Gen_MWh float,
  SO2_limit_ton float,
  NOx_limit_ton float,
  NOx_S_limit_ton float,  
  SO2_balance_ton float,
  NOx_balance_ton float,
  NOx_S_balance_ton float,
  IsActual bit,
  IsBudget bit,
  IsProjection bit,
  IsR7D bit,
  IsR30D bit
)
BEGIN

insert into @results
select YEAR,MONTH,SUM(ISNULL(So2_ton,0)) as SO2_ton,SUM(ISNULL(NOX_ton,0)) AS NOX_ton, 
SUM(ISNULL(NOX_S_ton,0)) AS NOX_S_ton,SUM(ISNULL(Gen_MWh,0)) AS Gen_MWh,
MAX(SO2_Limit_ton) as SO2_Limit_ton,MAX(NOx_Limit_ton) as NOx_Limit_ton,MAX(NOx_S_Limit_ton) as NOx_S_Limit_ton,
MAX(SO2_Balance_ton) as SO2_Balance_ton,MAX(NOx_Balance_ton) as NOx_Balance_ton,MAX(NOx_S_Balance_ton) as NOx_S_Balance_ton,
IsActual,IsBudget,IsProjection,IsR7D,IsR30D from [Projection].[ufnCSAPRDailyValues](@scenarioId,@dateAsOf,@startYear, @endYear, @assetId,0) group by YEAR,month,IsActual,IsBudget,IsProjection,IsR7D,IsR30D
RETURN
END