﻿
CREATE FUNCTION [Projection].[ufnCSAPRDailyValues]
(
 @scenarioId as int,
 @dateAsOf as DateTime,
 @startYear as int, 
 @endYear as int, 
 @assetId as int,
 @justYearToDate bit = 0
)
RETURNS @results TABLE
(
  isActual bit,
  isBudget bit,
  isProjection bit,
  isR7D bit,
  isR30D bit,
  scenarioId int,
  [year] int,
  [month] int,
  [day] int,
  SO2_ton float,
  NOx_ton float,
  NOx_S_ton float,
  Gen_MWh float,
  SO2_limit_ton float,
  NOx_limit_ton float,
  NOx_S_limit_ton float,
  SO2_balance_ton float,
  NOx_balance_ton float,
  NOx_S_balance_ton float
)

BEGIN

DECLARE @SO2_ton_UIItemID int = 1;
DECLARE @NOx_ton_UIItemID int = 2;
DECLARE @NOxS_ton_UIItemID int = 3;
DECLARE @LimitTargetTypeID int = 8; 
DECLARE @BalanceTargetTypeID int = 9; 

declare @scenarioStartDate as DateTime
declare @scenarioEndDate as DateTime

declare @unitid as int
declare @stationid as int
declare @stationgroupid as int
declare @clientAssetID as int

if @assetId is not null
  begin
  declare @assettypeid as int
  select @assettypeid = assettypeid from asset.tAsset a join asset.tAssetClassType b on a.AssetClassTypeID = b.AssetClassTypeID where AssetID = @assetId
  if @assettypeid = 1 -- client level
    begin
	declare @stationGroupChildCount as int
	select @stationGroupChildCount = count(*) from asset.tAsset a join Asset.tStationGroup b on a.AssetID = b.AssetID where a.ParentAssetID = @assetId
	if @stationGroupChildCount = 0
		begin
		set @assetId = null  --  this will force a rollup to the client level, or above it, potentially
		end
    else
		begin
		set @clientAssetID = @assetId
		end
	end
  else
    begin
    select @unitid=unitid from asset.tunit where assetid=@assetid
    if @unitid is null
      begin
      select @stationid = stationid from asset.tStation where AssetID=@assetId
      if @stationid is null
        begin
        select @stationgroupid = StationGroupid from asset.tStationGroup where AssetID=@assetId
        end
      end
    end
  end

declare @units table (unitid int, assetid int)

declare @clientsAndStationGroups table (clientAssetID int, stationGroupID int)

insert into @clientsAndStationGroups select a.ParentAssetID, b.StationGroupID from asset.tAsset a join asset.tStationGroup b on a.AssetID = b.AssetID

insert into @units select b.unitid,b.assetid from asset.tUnit b join asset.tStation c on b.StationID = c.StationID join @clientsAndStationGroups d on c.StationGroupID = d.stationGroupID

			where ((@unitid is not null and b.unitid=@unitid) or (@stationid is not null and b.StationID=@stationid) or 
			  (@stationgroupid is not null and c.StationGroupID = @stationgroupid) or 
			  (@clientAssetID is not null and d.clientAssetID = @clientAssetID) or
			  (@assetId is null))
			    
declare @lastActualsDate as DateTime
declare @lastActualsHour as float
declare @projFraction as float

if @unitid is null
  begin
  set @lastActualsDate = [Actuals].[ufnLastActualsDateForAssetId](null)
  end
else
  begin
  set @lastActualsDate = [Actuals].[ufnLastActualsDateForAssetId](@assetid)
  end

set @lastActualsDate = base.DateSerial(YEAR(@lastActualsDate),MONTH(@lastActualsDate),DAY(@lastActualsDate),DATEPART(HOUR,@lastActualsDate),0,0)  --TJC added hour
set @lastActualsHour = DATEPART(HOUR,@lastActualsDate) --TJC
set @projFraction = (24 - @lastActualsHour) / 24       --TJC

if @dateAsOf is null
  begin
  set @dateAsOf = @lastActualsDate
  end

set @dateAsOf = base.DateSerial(year(@dateasof),month(@dateasof),day(@dateasof),DATEPART(HOUR,@dateasof),0,0)  --TJC added hour
  
if @dateAsOf < @lastActualsDate
  begin
  set @lastActualsDate = @dateAsOf
  end

declare @defaultScenarioid as int

set @defaultScenarioid = [Projection].[ufnMostRecentConsensusScenarioID](YEAR(@dateAsOf),MONTH(@dateAsOf))

if @scenarioId is null
  begin
  set @scenarioId = @defaultScenarioid
  end
  
if @scenarioId is not null
  begin
  select @scenarioStartDate = a.startDate,@scenarioEndDate = a.enddate from [Projection].[ufnScenarioStartAndEndDate](@scenarioId) a
  end
    
if @startYear is null
  begin
  set @startYear = YEAR(@dateAsof)
  end
  
if @endYear is null
  begin
  set @endYear = @startYear
  end

declare @last7date as datetime = convert(datetime,convert(char,dateadd(d,-7,@lastActualsDate),102)) --TJC use full calendar days by stripping time
declare @last30date as datetime = convert(datetime,convert(char,dateadd(d,-30,@lastActualsDate),102)) --TJC

declare @Last7_Daily_SO2_ton as float
declare @Last7_Daily_NOx_ton as float
declare @Last7_Daily_NetGen_MWh as float
declare @Last30_Daily_SO2_ton as float
declare @Last30_Daily_NOx_ton as float
declare @Last30_Daily_NetGen_MWh as float

declare @r7rates table
(assetid int,so2_ton float,nox_ton float,netgen_mwh float)

declare @r30rates table
(assetid int,so2_ton float,nox_ton float,netgen_mwh float)
			  	
insert into @r7rates			  
select assetid,SUM(case when so2_ton > 0 then SO2_ton else 0 end)/7,SUM(case when Nox_ton > 0 then NOx_ton else 0 end)/7
  ,SUM(case when NetGeneration_MWh> 0 then NetGeneration_mwh else 0 end)/7 
from Actuals.vUnitPerformance a join @units b on a.UnitID = b.unitid and Base.DateSerial(YEAR,MONTH,DAY,Hour,0,0) > @last7date group by assetid

select @Last7_Daily_SO2_ton = SUM(so2_ton),@Last7_Daily_NOx_ton = SUM(NOx_ton),@Last7_Daily_NetGen_MWh = SUM(netgen_mwh) from @r7rates a join @units b on a.assetid = b.assetid

insert into @r30rates	
select assetid,SUM(case when so2_ton > 0 then SO2_ton else 0 end)/30,SUM(case when Nox_ton > 0 then NOx_ton else 0 end)/30
  ,SUM(case when NetGeneration_MWh> 0 then NetGeneration_mwh else 0 end)/30 
from Actuals.vUnitPerformance a join @units b on a.UnitID=b.unitid and Base.DateSerial(YEAR,MONTH,DAY,Hour,0,0) > @last30date group by assetid	

select @Last30_Daily_SO2_ton = SUM(so2_ton),@Last30_Daily_NOx_ton = SUM(NOx_ton),@Last30_Daily_NetGen_MWh = SUM(netgen_mwh) from @r30rates a join @units b on a.assetid = b.assetid

declare @y as int
set @y = @startYear
while @y <= @endYear
  begin
  
  declare @UITargetSetID as int
  SELECT @UITargetSetID = a.UITargetSetID FROM Projection.tUITargetSet a WHERE a.PlanYear = @y
        
  declare @budgetScenarioId as int
  set @budgetScenarioId = [Projection].[ufnMostRecentConsensusBudgetScenarioID](@y)
    
  declare @m as int
  set @m = 1
    
  declare @lastMonth as int
  if @justYearToDate = 1
    begin
    set @lastMonth = MONTH(@dateAsOf)
    end
  else
    begin
    set @lastMonth = 12
    end
    
  while @m <= @lastMonth
    begin
    
    declare @days as int
    set @days = DATEPART(day, DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,base.DateSerial(@y,@m,1,0,0,0))+1,0))) --get days in month
            
    declare @origProjId as int
    set @origProjId = [Projection].[ufnMostRecentConsensusScenarioID](@y,@m)
    
    declare @day as int
    set @day = 1
    while @day <= @days
      begin
		declare @d as DateTime
		set @d = base.DateSerial(@y,@m,@day,0,0,0)		--TJC was hour=23

        declare @So2Limit as float, @NoxLimit as float, @NoxSLimit as float
        
        set @So2Limit = null
        select @So2Limit = SUM(a.Quantity) from [Projection].tUITargetUnitDetail a
			join 
		(SELECT  max(StartDate) as StartDate,a.UnitID,UITargetSetID,UITargetTypeID,UIItemID
			FROM [Projection].tUITargetUnitDetail a JOIN @units b on a.UnitID = b.unitid
			WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @LimitTargetTypeID AND UIItemID = @SO2_ton_UIItemID
				AND @d BETWEEN StartDate AND EndDate                                                                        --TJC
				group by a.UnitID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.UnitID = b.UnitID 
				and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
	
	   set @NoxLimit = null
	   select @NOxLimit = SUM(a.Quantity) from [Projection].tUITargetUnitDetail a
			join 
		(SELECT  max(StartDate) as StartDate,a.UnitID,UITargetSetID,UITargetTypeID,UIItemID
			FROM [Projection].tUITargetUnitDetail a JOIN @units b on a.UnitID = b.unitid
			WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @LimitTargetTypeID AND UIItemID = @NOx_ton_UIItemID
				AND @d BETWEEN StartDate AND EndDate																		--TJC
				group by a.UnitID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.UnitID = b.UnitID 
				and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
		
		set @NOxSLimit = null
		select @NOxSLimit = SUM(a.Quantity) from [Projection].tUITargetUnitDetail a
			join 
		(SELECT  max(StartDate) as StartDate,a.UnitID,UITargetSetID,UITargetTypeID,UIItemID
			FROM [Projection].tUITargetUnitDetail a JOIN @units b on a.UnitID = b.unitid
			WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @LimitTargetTypeID AND UIItemID = @NOxS_ton_UIItemID
				AND @d BETWEEN StartDate AND EndDate																		--TJC
				group by a.UnitID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.UnitID = b.UnitID 
				and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
				
		declare @So2Balance as float, @NoxBalance as float, @NoxSBalance as float
        
        set @So2Balance = null
		select @So2Balance = SUM(a.Quantity) from [Projection].tUITargetUnitDetail a
			join 
		(SELECT  max(StartDate) as StartDate,a.UnitID,UITargetSetID,UITargetTypeID,UIItemID
			FROM [Projection].tUITargetUnitDetail a JOIN @units b on a.UnitID = b.unitid
			WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @BalanceTargetTypeID AND UIItemID = @So2_ton_UIItemID
				AND @d BETWEEN StartDate AND EndDate																		--TJC
				group by a.UnitID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.UnitID = b.UnitID 
				and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
	
	   set @NoxBalance = null
	   select @NoxBalance = SUM(a.Quantity) from [Projection].tUITargetUnitDetail a
			join 
		(SELECT  max(StartDate) as StartDate,a.UnitID,UITargetSetID,UITargetTypeID,UIItemID
			FROM [Projection].tUITargetUnitDetail a JOIN @units b on a.UnitID = b.unitid
			WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @BalanceTargetTypeID AND UIItemID = @Nox_ton_UIItemID
				AND @d BETWEEN StartDate AND EndDate																		--TJC
				group by a.UnitID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.UnitID = b.UnitID 
				and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
		
		set @NoxSBalance = null
		select @NoxSBalance = SUM(a.Quantity) from [Projection].tUITargetUnitDetail a
			join 
		(SELECT  max(StartDate) as StartDate,a.UnitID,UITargetSetID,UITargetTypeID,UIItemID
			FROM [Projection].tUITargetUnitDetail a JOIN @units b on a.UnitID = b.unitid
			WHERE UITargetSetID = @UITargetSetID AND UITargetTypeID = @BalanceTargetTypeID AND UIItemID = @NoxS_ton_UIItemID
				AND @d BETWEEN StartDate AND EndDate																		--TJC
				group by a.UnitID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.UnitID = b.UnitID 
				and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
		
		if @clientAssetID is not null
		BEGIN
			DECLARE @UnallocNox float, @UnallocSo2 float, @UnallocNoxS float
			
			select @UnallocSo2 = SUM(a.Quantity) from [Projection].[tUITargetClientDetail] a
				join 
			(SELECT  max(StartDate) as StartDate,a.AssetID,UITargetSetID,UITargetTypeID,UIItemID
				FROM [Projection].[tUITargetClientDetail] a 
				WHERE UITargetSetID = @UITargetSetID AND AssetID = @ClientAssetID AND UITargetTypeID = @BalanceTargetTypeID AND UIItemID = @So2_ton_UIItemID
					AND @d BETWEEN StartDate AND EndDate																		--TJC
					group by a.AssetID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.AssetID = b.AssetID 
					and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
	
		   select @UnallocNox = SUM(a.Quantity) from [Projection].tUITargetClientDetail a
				join 
			(SELECT  max(StartDate) as StartDate,a.AssetID,UITargetSetID,UITargetTypeID,UIItemID
				FROM [Projection].tUITargetClientDetail a 
				WHERE UITargetSetID = @UITargetSetID AND AssetID = @ClientAssetID AND UITargetTypeID = @BalanceTargetTypeID AND UIItemID = @Nox_ton_UIItemID
					AND @d BETWEEN StartDate AND EndDate																		--TJC
					group by a.AssetID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.AssetID = b.AssetID 
					and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
		
			select @UnallocNoxS = SUM(a.Quantity) from [Projection].tUITargetClientDetail a
				join 
			(SELECT  max(StartDate) as StartDate,a.AssetID,UITargetSetID,UITargetTypeID,UIItemID
				FROM [Projection].tUITargetClientDetail a 
				WHERE UITargetSetID = @UITargetSetID AND AssetID = @ClientAssetID AND UITargetTypeID = @BalanceTargetTypeID AND UIItemID = @NoxS_ton_UIItemID
					AND @d BETWEEN StartDate AND EndDate																		--TJC
					group by a.AssetID,UITargetSetID,UITargetTypeID,UIItemID) b on a.StartDate = b.StartDate and a.AssetID = b.AssetID 
					and a.UITargetSetID = b.UITargetSetID and a.UITargetTypeID = b.UITargetTypeID and a.UIItemID = b.UIItemID
			
			if @UnallocSo2 is not null 
				BEGIN
				if @So2Balance is null  
					BEGIN 
					set @So2Balance = @UnallocSo2
					END	
				ELSE 
					BEGIN 
					set @So2Balance =  @So2Balance  + @UnallocSo2
					END
				END
			if @UnallocNox is not null 
				BEGIN
				if @NoxBalance is null  
					BEGIN 
					set @NoxBalance = @UnallocNox   
					END	
				ELSE 
					BEGIN 
					set @NoxBalance =  @NoxBalance  + @UnallocNox	
					END
				END
			if @UnallocNoxS is not null 
				BEGIN
				if @NoxSBalance is null 
					BEGIN 
					set @NoxSBalance = @UnallocNoxS 
					END	
				ELSE 
					BEGIN 
					set @NoxSBalance = @NoxSBalance + @UnallocNoxS	
					END
				END
			END
						
		declare @projScenarioId as int
		
		if @d between @scenarioStartDate and @scenarioEndDate
		  begin
		  set @projScenarioId = @scenarioId
		  end
		else
		  begin
		  set @projScenarioId = @origProjId
		  end
				  
		if @lastActualsDate >= @d  
		  begin
		  --actual
		  insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
			select 1,0,0,0,0,@y,@m,@day,
			SUM(case when SO2_Ton > 0 then SO2_ton else 0 end),SUM(case when NOX_TON > 0 then NOx_ton else 0 end),
			SUM(case when month between 5 and 9 and Nox_ton>0 then NOx_ton else 0 end), SUM(case when NetGeneration_MWh > 0 then NetGeneration_MWh else 0 end ),
			@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance
			 from Actuals.vUnitPerformance a join @units b on a.UnitID=b.UnitID 
			where year = @y and month = @m and [day] = @day and base.DateSerial(year,month,day,hour,0,0) <= @lastActualsDate
		
			--TJC add projection points for same day actuals end; prorate daily rate for final hours
			if ((YEAR(@lastActualsDate) = @y) and (MONTH(@lastActualsDate) = @m) and (DAY(@lastActualsDate) = @day))
				begin
				insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
				select 0,0,1,0,0,@projScenarioId,@y,@m,@day,SUM(case when SO2_Ton > 0 then SO2_ton * @projFraction else 0 end),SUM(case when NOX_TON > 0 then NOx_ton * @projFraction else 0 end),
				SUM(case when month between 5 and 9 and Nox_ton > 0 then NOx_ton * @projFraction else 0 end), SUM(case when NetGeneration_MWh > 0 then NetGeneration_MWh * @projFraction else 0 end ),
				@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance
				from Projection.tScenarioUnitPerformance a join @units b on a.UnitID = b.UnitID 
				where year = @y and month = @m and [DAY] = @day and ScenarioID = @projScenarioId --	and base.DateSerial(year,month,day,Hour,0,0) > @lastActualsDate
				
				--7D 
			  insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton
				,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
				values (0,0,0,1,0,@projScenarioId,@y,@m,@day,@Last7_Daily_SO2_ton * @projFraction,@Last7_Daily_NOx_ton * @projFraction,
				case when @m between 5 and 9 then @Last7_Daily_NOx_ton * @projFraction else 0 end, @Last7_Daily_NetGen_MWh * @projFraction,
				@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance)
			
			  --30D		
			  insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton
				,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
				values (0,0,0,0,1,@projScenarioId,@y,@m,@day,@Last30_Daily_SO2_ton * @projFraction,@Last30_Daily_NOx_ton * @projFraction,
				case when @m between 5 and 9 then @Last30_Daily_NOx_ton * @projFraction else 0 end, @Last30_Daily_NetGen_MWh * @projFraction,
				@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance)
				
				end
		  end
		else
		  begin
		  --projection		  		  
		  insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton
		    ,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
			select 0,0,1,0,0,@projScenarioId,@y,@m,@day,SUM(case when SO2_Ton > 0 then SO2_ton else 0 end),SUM(case when NOX_TON > 0 then NOx_ton else 0 end),
			SUM(case when month between 5 and 9 and Nox_ton>0 then NOx_ton else 0 end), SUM(case when NetGeneration_MWh > 0 then NetGeneration_MWh else 0 end ),
			@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance
			 from Projection.tScenarioUnitPerformance a join @units b on a.UnitID=b.UnitID 
			where year = @y and month = @m and [DAY] = @day and ScenarioID=@projScenarioId and base.DateSerial(year,month,day,Hour,0,0) > @lastActualsDate
			
          --7D 
          insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton
            ,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
			values (0,0,0,1,0,@projScenarioId,@y,@m,@day,@Last7_Daily_SO2_ton,@Last7_Daily_NOx_ton,
			case when @m between 5 and 9 then @Last7_Daily_NOx_ton else 0 end, @Last7_Daily_NetGen_MWh,
			@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance)
		
		  --30D		
          insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton
            ,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
			values (0,0,0,0,1,@projScenarioId,@y,@m,@day,@Last30_Daily_SO2_ton,@Last30_Daily_NOx_ton,
			case when @m between 5 and 9 then @Last30_Daily_NOx_ton else 0 end, @Last30_Daily_NetGen_MWh,
			@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance)
									 		
		  end
		  
	     --budget
		 insert into @results (isActual,isbudget,isprojection,isR7D,isR30D,scenarioId,year,month,day,SO2_ton,NOX_ton,NOX_S_ton,Gen_MWh,SO2_limit_ton
		   ,NOx_limit_ton,NOx_S_limit_ton,SO2_balance_ton,NOx_balance_ton,NOx_S_balance_ton) 
			select 0,1,0,0,0,@budgetScenarioId,@y,@m,@day,
			SUM(case when SO2_Ton > 0 then SO2_ton else 0 end),SUM(case when NOX_TON > 0 then NOx_ton else 0 end),
			SUM(case when month between 5 and 9 and Nox_ton>0 then NOx_ton else 0 end), SUM(case when NetGeneration_MWh > 0 then NetGeneration_MWh else 0 end ),
			@So2Limit,@NoxLimit,@NoxSLimit,@So2Balance,@NoxBalance,@NoxSBalance from Projection.tScenarioUnitPerformance a join @units b on a.UnitID=b.UnitID 
			where year = @y and month = @m and [DAY] = @day and ScenarioID = @budgetScenarioId and (@justYearToDate = 0 or base.DateSerial(year,month,day,hour,0,0) <= @dateAsOf)
			
      set @day = @day + 1
	  end
    set @m = @m + 1
    end
  set @y = @y + 1
  end
RETURN
END

