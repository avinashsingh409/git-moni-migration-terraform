﻿
-- =============================================
-- Author:		Ryan Irwin
-- Create date: 3 November 2014
-- Description:	Checks the supplied scenario ID
-- against the set of running scenarios, to see
-- if that scenario is currently calculating or
-- in the queue for calculation.
-- =============================================
CREATE FUNCTION [Projection].[ufnIsFuelSourceInCalculation]
(
	-- Add the parameters for the function here
	@FuelSourceID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit
	SET @result = 0 --until otherwise determined.
	
	DECLARE @count int
	SELECT @count = COUNT(*) 
	FROM Projection.tScenarioRun sr
	JOIN Projection.tScenario s ON sr.ScenarioID = s.ScenarioID
	JOIN Projection.tFuelChangeEvent fce ON s.FuelPlanSetID = fce.FuelPlanSetID
	WHERE sr.RunStatus >= 0 AND sr.RunStatus < 3
	AND fce.FuelSourceID = @FuelSourceID
	
	if @count > 0
	BEGIN
		set @result = 1
	END
		
	-- Return the result of the function
	RETURN @result

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnIsFuelSourceInCalculation] TO [TEUser]
    AS [dbo];

