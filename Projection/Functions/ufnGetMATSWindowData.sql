﻿CREATE FUNCTION [Projection].[ufnGetEmissionRollingWindowData]
(
 @ScenarioID as int,
 @Date as DateTime,
 @UnitID as int,
 @UIItemID as int
)
RETURNS @results TABLE
(  
  DateWithinWindow Date,
  DailySum float null,  
  SummedHours tinyint
)
BEGIN

insert into @results
select b.DateWithinWindow, b.DailySum, b.SummedHours
from [Projection].[tEmissionRollingWindowData] b 
INNER JOIN Projection.tEmissionRollingData a
ON a.EmissionRollingDataID = b.EmissionRollingDataID
WHERE a.[Date] = @Date 
AND a.UnitID = @UnitID
AND a.UIItemID = @UIItemID
AND a.ScenarioID = @ScenarioID
ORDER BY b.DateWithinWindow
RETURN
END