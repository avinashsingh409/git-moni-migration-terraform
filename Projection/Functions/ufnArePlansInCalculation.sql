﻿
-- =============================================
-- Author:		Ryan Irwin
-- Create date: 30 October 2014
-- Description:	Checks each supplied plan ID against
-- the currently calculating scenarios, and returns
-- 1 if any supplied plan ID is linked to a currently
-- calculating scenario. If no IDs were supplied, or
-- no supplied ID is linked to an actively running 
-- scenario, then 0 will be returned.
-- =============================================
CREATE FUNCTION [Projection].[ufnArePlansInCalculation]
(
	-- Add the parameters for the function here
	@GenerationSetID int = null
	,@DemandSetID int = null
	,@FuelPlanSetID int = null
	,@MaintenancePlanSetID int = null
	,@unitConfigScheduleSetID int = null
	,@ForecastSetID int = null
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit
	SET @result = 0 --until otherwise determined.
	
	DECLARE @count int
	-- Add the T-SQL statements to compute the return value here
	if  @GenerationSetID is not null
	BEGIN
		SELECT @count = COUNT(*) 
		FROM Projection.tScenario s
		JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
		WHERE s.GenerationSetID = @GenerationSetID
		AND sr.RunStatus >= 0 AND sr.RunStatus < 3
		
		if @count > 0
		BEGIN
			set @result = 1
		END
	END
	
	if  @DemandSetID is not null
	BEGIN
		SELECT @count = COUNT(*) 
		FROM Projection.tScenario s
		JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
		WHERE s.DemandSetID = @DemandSetID
		AND sr.RunStatus >= 0 AND sr.RunStatus < 3
		
		if @count > 0
		BEGIN
			set @result = 1
		END
	END
	
	if  @FuelPlanSetID is not null
	BEGIN
		SELECT @count = COUNT(*) 
		FROM Projection.tScenario s
		JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
		WHERE s.FuelPlanSetID = @FuelPlanSetID
		AND sr.RunStatus >= 0 AND sr.RunStatus < 3
		
		if @count > 0
		BEGIN
			set @result = 1
		END
	END
	
	if  @MaintenancePlanSetID is not null
	BEGIN
		SELECT @count = COUNT(*) 
		FROM Projection.tScenario s
		JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
		WHERE s.MaintenancePlanSetID = @MaintenancePlanSetID
		AND sr.RunStatus >= 0 AND sr.RunStatus < 3
		
		if @count > 0
		BEGIN
			set @result = 1
		END
	END
	
	if  @unitConfigScheduleSetID is not null
	BEGIN
		SELECT @count = COUNT(*) 
		FROM Projection.tScenario s
		JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
		WHERE s.UnitConfigScheduleSetID = @unitConfigScheduleSetID
		AND sr.RunStatus >= 0 AND sr.RunStatus < 3
		
		if @count > 0
		BEGIN
			set @result = 1
		END
	END
	
	if  @ForecastSetID is not null
	BEGIN
		SELECT @count = COUNT(*) 
		FROM Projection.tScenario s
		JOIN Projection.tScenarioRun sr on s.ScenarioID = sr.ScenarioID
		WHERE s.ForecastSetID = @ForecastSetID
		AND sr.RunStatus >= 0 AND sr.RunStatus < 3
		
		if @count > 0
		BEGIN
			set @result = 1
		END
	END
	
	
	
	
	-- Return the result of the function
	RETURN @result

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnArePlansInCalculation] TO [TEUser]
    AS [dbo];

