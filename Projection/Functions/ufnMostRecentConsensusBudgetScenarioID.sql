﻿CREATE FUNCTION [Projection].[ufnMostRecentConsensusBudgetScenarioID]
(	
	@planYear as int
)
RETURNS int
AS
BEGIN

declare @result as int

SELECT TOP 1 @result = ScenarioID FROM (select a.ScenarioID,a.ScenarioDesc, a.PlanYear,case when b.PlanPeriod=12 then 0 else b.PlanPeriod END as PlanPeriod,
			YEAR(isnull(a.StartDate,c.StartDate)) as MinYear, YEAR(isnull(a.enddate,c.enddate)) as MaxYear
			from Projection.tScenario a JOIN Projection.tDataVersion b ON a.DataVersionID=b.DataVersionID
			join Projection.tGenerationSet c on a.GenerationSetID = c.GenerationSetID where a.isconsensus=1 and a.DataVersionID=1
			AND (a.ScenarioTypeID = 3 OR a.RunStatus = 3)
			) a WHERE
			(PlanYear=@planYear) OR (MinYear<@planYear and MaxYear>@planYear)
			ORDER BY PlanYear DESC,PlanPeriod DESC, ScenarioID DESC

return @result
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnMostRecentConsensusBudgetScenarioID] TO [TEUser]
    AS [dbo];

