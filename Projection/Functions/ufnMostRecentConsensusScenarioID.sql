﻿

CREATE FUNCTION [Projection].[ufnMostRecentConsensusScenarioID]
(	
	@planYear as int,
	@planMonth as int
)
RETURNS int
AS
BEGIN

declare @result as int

SELECT TOP 1 @result = ScenarioID FROM (select a.ScenarioID,a.ScenarioDesc, a.PlanYear,case when b.PlanPeriod=12 then 0 else b.PlanPeriod END as PlanPeriod,
			YEAR(c.StartDate) as MinYear, YEAR(c.EndDate) as MaxYear
			from Projection.tScenario a JOIN Projection.tDataVersion b ON a.DataVersionID=b.DataVersionID cross apply [Projection].[ufnScenarioStartAndEndDate](a.scenarioID) c
			where a.isconsensus=1 AND (a.ScenarioTypeID = 3 OR a.RunStatus = 3)
			) a WHERE
			(PlanYear=@planYear and PlanPeriod<@planMonth) OR (MinYear<@planYear and MaxYear>@planYear)
			ORDER BY PlanYear DESC,PlanPeriod DESC, ScenarioID DESC

return @result
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnMostRecentConsensusScenarioID] TO [TEUser]
    AS [dbo];

