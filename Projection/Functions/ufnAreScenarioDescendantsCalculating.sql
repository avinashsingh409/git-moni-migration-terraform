﻿-- =============================================
-- Author:		Ryan Irwin
-- Create date: 10/2/2015
-- Description:	Function to check recursively if
-- any of a given scenario's descendents are in
-- calculation at the moment.
-- =============================================
CREATE FUNCTION Projection.ufnAreScenarioDescendantsCalculating 
(
	-- Add the parameters for the function here
	@ScenarioID  int 
)
RETURNS bit
AS
BEGIN
	DECLARE @runningChildrenCount int;
	SELECT @runningChildrenCount = COUNT(*)
	FROM Projection.tScenario
	WHERE ParentScenarioID = @ScenarioID
	AND RunStatus > 0 AND RunStatus < 3;

	if @runningChildrenCount > 0 RETURN 1;
	ELSE
	BEGIN
		DECLARE childScenarioIDCursor CURSOR LOCAL
		FOR 
		SELECT ScenarioID from Projection.tScenario
		WHERE ParentScenarioID = @ScenarioID;
		
		DECLARE @childScenarioID int;
		OPEN childScenarioIDCursor;
  
		FETCH NEXT FROM childScenarioIDCursor INTO @childScenarioID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF Projection.ufnAreScenarioDescendantsCalculating(@childScenarioID) = 1 RETURN 1;
			FETCH NEXT FROM childScenarioIDCursor INTO @childScenarioID
		END
	END
	
	--If we get here, none of the descendents are calculating. Return 0.
	RETURN 0
END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnAreScenarioDescendantsCalculating] TO [TEUser]
    AS [dbo];

