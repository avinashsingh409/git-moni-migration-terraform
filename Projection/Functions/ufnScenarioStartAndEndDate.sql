﻿
CREATE FUNCTION [Projection].[ufnScenarioStartAndEndDate]
(	
	@scenarioId as int
)
RETURNS @table TABLE
(
StartDate DateTime,
EndDate DateTime
)
AS
BEGIN

insert into @table


select 		isnull(isnull(case when a.UseStartAndEndDate=1 then a.StartDate else null end,c.StartDate),base.DateSerial(a.planyear,1,1,0,0,0)) as startDate, 
			isnull(isnull(case when a.UseStartAndEndDate=1 then a.EndDate else null end,c.EndDate),base.DateSerial(a.planyear,12,31,0,0,0)) as endDate			
			from Projection.tScenario a JOIN Projection.tDataVersion b ON a.DataVersionID=b.DataVersionID
			join Projection.tGenerationSet c on a.GenerationSetID = c.GenerationSetID where a.ScenarioID=@scenarioId
return 
END