﻿
-- ===============================================
-- Author:		Ryan Irwin
-- Create date: 3 November 2014
-- Description:	Checks the supplied unit equipment 
-- state ID against the set of running scenarios, 
-- to see if that equipment state is tied to any 
-- scenario currently calculating or in the queue 
-- for calculation.
-- ===============================================
CREATE FUNCTION [Projection].[ufnIsUnitEquipStateInCalculation]
(
	-- Add the parameters for the function here
	@UnitEquipmentStateSetID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit
	SET @result = 0 --until otherwise determined.
	
	DECLARE @count int
	SELECT @count = COUNT(*) 
	FROM Projection.tUnitEquipmentStateSet uess
	JOIN Projection.tUnitConfig uc ON uc.UnitEquipmentStateSetID = uess.UnitEquipmentStateSetID
	JOIN Projection.tUnitConfigScheduleDetail ucsd ON ucsd.UnitConfigID = uc.UnitConfigID
	JOIN Projection.tScenario s on s.UnitConfigScheduleSetID = ucsd.UnitConfigScheduleSetID
	JOIN Projection.tScenarioRun sr on sr.ScenarioID = s.ScenarioID
	WHERE sr.RunStatus >= 0 AND sr.RunStatus < 3
	AND uess.UnitEquipmentStateSetID = @UnitEquipmentStateSetID 
	
	if @count > 0
	BEGIN
		set @result = 1
	END
		
	-- Return the result of the function
	RETURN @result

END
GO
GRANT EXECUTE
    ON OBJECT::[Projection].[ufnIsUnitEquipStateInCalculation] TO [TEUser]
    AS [dbo];

