﻿CREATE PROCEDURE [Scenario].[spUpsertScenario]
    @ScenarioID INT = -1,
    @ScenarioGuid UNIQUEIDENTIFIER = NULL,
    @ScenarioTypeID INT,
	@ScenarioClassTypeID INT,
    @ParentAssetID INT,
    @ScenarioDesc NVARCHAR(255),
	@LastRunJobExtID UNIQUEIDENTIFIER = NULL,
    @Notes NVARCHAR(MAX) = NULL,
    @SecurityUserID INT,
    @AdHocInputs VARCHAR(MAX) = NULL,
    @UpsertedScenarioID INT OUTPUT
AS
BEGIN
    IF Asset.[ufnDoesUserHaveAccessToAsset](@ParentAssetID, @SecurityUserID, DEFAULT) = 0
    BEGIN
        RAISERROR('User does not have access to this scenario', 13, -1, -1)
    END
	DECLARE @username NVARCHAR(MAX) = NULL
	SELECT TOP 1 @username = Username FROM AccessControl.tUser WHERE @SecurityUserID = SecurityUserID
	IF @username IS NULL
	BEGIN
        RAISERROR('User not found', 13, -1, -1)
	END

	DECLARE @scenarioGroupAssetID INT = NULL
	SELECT TOP 1 @scenarioGroupAssetID = AssetID FROM Asset.tAsset WHERE ParentAssetID = @ParentAssetID AND AssetClassTypeID = 7001

	-- if it's a new scenario, add it to the asset table
	IF @ScenarioID < 0
	BEGIN
	    IF @ScenarioGuid IS NULL
	    BEGIN
	        SELECT @ScenarioGuid = NEWID()
	    END
		DECLARE @Out TABLE (id int)
		DECLARE @outGroupId TABLE (id INT)
		BEGIN TRY
			BEGIN TRANSACTION
			IF @scenarioGroupAssetID IS NULL
			BEGIN
				INSERT INTO Asset.tAsset (
					ParentAssetID,
					AssetAbbrev,
					AssetDesc,
					AssetClassTypeID,
					DisplayOrder,
					CreatedBy,
					ChangedBy,
					CreateDate,
					ChangeDate,
					GlobalID,
					IsHidden,
					Track
				)
				OUTPUT inserted.AssetID INTO @outGroupId(id)
				VALUES (
					@ParentAssetID,
					'Scenarios',
					'Scenarios',
					7001,
					0,
					@username,
					@username,
					CURRENT_TIMESTAMP,
					CURRENT_TIMESTAMP,
					NEWID(),
					0,
					0
				)
				SELECT TOP 1 @scenarioGroupAssetID = id FROM @outGroupId
			END
			INSERT INTO Asset.tAsset (
				ParentAssetID,
				AssetAbbrev,
				AssetDesc,
				AssetClassTypeID,
				DisplayOrder,
				CreatedBy,
				ChangedBy,
				CreateDate,
				ChangeDate,
				GlobalID,
				IsHidden,
				Track
			)
			OUTPUT INSERTED.AssetID INTO @Out(id)
			VALUES (
				@scenarioGroupAssetID,
				@ScenarioDesc,
				@ScenarioDesc,
				@ScenarioClassTypeID,
				0,
				@username,
				@username,
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP,
				@ScenarioGuid,
				0,
				0
			)
			SELECT TOP 1 @UpsertedScenarioID = id FROM @Out
			
			-- Asset trigger will create scenario record
			UPDATE Scenario.tScenario SET ScenarioTypeID = @ScenarioTypeID,Notes = @Notes, LastRunJobExtID = @LastRunJobExtID, AdHocInputs = @AdHocInputs where ScenarioID = @UpsertedScenarioID

			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
	END
	ELSE -- existing asset, update
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 AssetID FROM Asset.tAsset WHERE AssetID = @ScenarioID)
		BEGIN
			RAISERROR('Scenario does not exist', 13, -1, -1)
		END

	    IF Asset.[ufnDoesUserHaveAccessToAsset](@ScenarioID, @SecurityUserID, -1) = 0
		BEGIN
			RAISERROR('User does not have access to this scenario', 13, -1, -1)
		END

	    DECLARE @existingOwningAssetID INT
	    DECLARE @existingScenarioTypeID INT
	    SELECT @existingOwningAssetID = ParentAssetID FROM Asset.tAsset WHERE AssetID = @ScenarioID
		SELECT @existingScenarioTypeID = ScenarioTypeID FROM Scenario.tScenario WHERE ScenarioID = @ScenarioID
	    IF @existingOwningAssetID <> @scenarioGroupAssetID OR @scenarioGroupAssetID IS NULL
	    BEGIN
	        RAISERROR('Scenario parent asset ID does not match existing scenario data', 13, -1, -1)
	    END
	    IF @existingScenarioTypeID <> @ScenarioTypeID
	    BEGIN
	        RAISERROR('ScenarioTypeID does not match existing scenario data', 13, -1, -1)
	    END

		BEGIN TRY
			BEGIN TRANSACTION
			UPDATE Asset.tAsset SET
			    AssetDesc = @ScenarioDesc,
				AssetAbbrev = @ScenarioDesc,
			    ChangedBy = @username,
			    ChangeDate = CURRENT_TIMESTAMP
			    WHERE AssetID = @ScenarioID
			UPDATE Scenario.tScenario SET
				Notes = @Notes,
				AdHocInputs = @AdHocInputs,
				LastRunJobExtID = @LastRunJobExtID
				WHERE ScenarioID = @ScenarioID
			SET @UpsertedScenarioID = @ScenarioID
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
	END
END
GO
 
GRANT EXECUTE
    ON OBJECT::[Scenario].[spUpsertScenario] TO [TEUser]
    AS [dbo];
GO
