﻿CREATE PROCEDURE [Scenario].[spInsertScenarioRunJob]
    @scenarioID INT,
	@securityUserID INT,
    @jobExtId UNIQUEIDENTIFIER,
    @insertedScenarioRunJob INT OUTPUT
AS
BEGIN
	IF Asset.ufnDoesUserHaveAccessToAsset(@scenarioID, @securityUserID, DEFAULT) = 0
	BEGIN
        RAISERROR('User does not have access to this scenario', 13, -1, -1)
	END

	DECLARE @Out TABLE (id INT)
	INSERT INTO Scenario.tScenarioRunJobs
		(ScenarioID, JobExtID)
		OUTPUT INSERTED.ScenarioRunJobID INTO @Out(id)
		VALUES
		(@scenarioID, @JobExtId)
	SELECT TOP 1 @insertedScenarioRunJob = id FROM @Out
	UPDATE Scenario.tScenario SET LastRunJobExtID = @jobExtId WHERE ScenarioID = @scenarioID
END
GO

GRANT EXECUTE
    ON OBJECT::[Scenario].[spInsertScenarioRunJob] TO [TEUser]
    AS [dbo];
GO
