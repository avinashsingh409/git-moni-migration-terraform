﻿CREATE TABLE [Scenario].[tScenarioRunJobs]
(
    [ScenarioRunJobID] INT NOT NULL PRIMARY KEY IDENTITY (1, 1),
    [ScenarioID] INT NOT NULL,
    [JobExtID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [FK_ScenarioRunJobs_Scenario_ScenarioID] FOREIGN KEY ([ScenarioID]) REFERENCES [Scenario].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ScenarioRunJobs_Job_JobExtID] FOREIGN KEY ([JobExtID]) REFERENCES [Jobs].[tJob] ([JobExtID]) ON DELETE CASCADE
)
