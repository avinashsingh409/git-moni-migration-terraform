﻿CREATE TABLE [Scenario].[tScenarioType]
(
    [ScenarioTypeID] INT NOT NULL PRIMARY KEY, 
    [ScenarioTypeDesc] NVARCHAR (255) NOT NULL,
    [ScenarioInputTemplate] VARCHAR(MAX) NULL
)
GO

EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Scenario', @level1type = N'TABLE', @level1name = N'tScenarioType';
GO
