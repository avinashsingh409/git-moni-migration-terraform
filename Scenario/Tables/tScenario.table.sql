﻿CREATE TABLE [Scenario].[tScenario]
(
    [ScenarioID] INT PRIMARY KEY,
    [ScenarioTypeID] INT NOT NULL,
    [Notes] NVARCHAR(MAX) NULL,
    [LastRunJobExtID] UNIQUEIDENTIFIER NULL,
	[AdHocInputs] NVARCHAR(MAX) NULL, 
	CONSTRAINT [FK_Scenario_ScenarioID_AssetID] FOREIGN KEY ([ScenarioID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Scenario_ScenarioType_ScenarioTypeID] FOREIGN KEY ([ScenarioTypeID]) REFERENCES [Scenario].[tScenarioType] ([ScenarioTypeID]),
    CONSTRAINT [FK_Scenario_Job_LastRunJobExtID] FOREIGN KEY ([LastRunJobExtID]) REFERENCES [Jobs].[tJob] ([JobExtId])
)
