﻿
CREATE FUNCTION [Scenario].[ufnGetAllScenariosForAssetId]
(
	@assetID INT,
	@securityUserID INT,
	@scenarioTypeID INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		s.ScenarioID,
		s.ScenarioTypeID,
		a.AssetClassTypeID AS [ScenarioClassTypeID],
		a.GlobalID AS [ScenarioGuid],
		ancestry.AssetID AS [ParentAssetID],
		createdUser.Username AS [CreatedBy],
		changedUser.Username AS [ChangedBy],
		a.AssetAbbrev AS [ScenarioDesc],
		s.Notes,
		s.LastRunJobExtID,
		s.AdHocInputs
	FROM Scenario.tScenario s
			INNER JOIN Asset.tAsset a ON a.AssetID = s.ScenarioID
			INNER JOIN Asset.tAssetHop ah on s.ScenarioID = ah.EndingAssetId
			INNER JOIN Asset.tAsset ancestry ON ah.HopAssetId = ancestry.AssetID
			INNER JOIN AccessControl.tUser createdUser ON createdUser.Username = a.CreatedBy
			INNER JOIN AccessControl.tUser changedUser ON changedUser.Username = a.ChangedBy
	WHERE Asset.ufnDoesUserHaveAccessToAsset(s.ScenarioID, @securityUserID, -1) = 1
			AND ah.HopAssetId = Asset.ufnGetClientAssetIDForAsset(@assetID)
			AND s.ScenarioTypeID = @scenarioTypeID
			AND ah.AncestryLevel <= 0
			AND a.AssetClassTypeID = 8001
			AND ancestry.ParentAssetID IS NOT NULL
	ORDER BY ah.AncestryLevel ASC
	OFFSET 0 ROWS -- bypass to keep this sql function inline
)
GO

GRANT SELECT
    ON OBJECT::[Scenario].[ufnGetAllScenariosForAssetId] TO [TEUser]
    AS [dbo];
GO
