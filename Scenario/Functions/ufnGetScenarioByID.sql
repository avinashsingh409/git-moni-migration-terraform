﻿CREATE FUNCTION [Scenario].[ufnGetScenarioByID]
(
	@scenarioID INT,
	@securityUserID INT
)
--    [ScenarioID] INT NOT NULL,
--    [ScenarioGuid] UNIQUEIDENTIFIER NOT NULL,
--    [ScenarioTypeID] INT NOT NULL,
--    [ParentAssetID] INT NOT NULL,
--    [ScenarioDesc] NVARCHAR(255) NOT NULL,
--    [Notes] NVARCHAR(MAX) NULL,
--    [CreatedBy] INT NOT NULL,
--    [CreatedDate] DATETIMEOFFSET NOT NULL,
--    [ChangedBy] INT NOT NULL,
--    [ChangedDate] DATETIMEOFFSET NOT NULL,
--    [LastRunJobExtID] UNIQUEIDENTIFIER NULL,
--    [AdHocInputs] NVARCHAR(MAX) NULL
RETURNS TABLE
AS
RETURN
(
	SELECT
		s.[ScenarioID],
		a.[GlobalID] AS [ScenarioGuid],
		s.[ScenarioTypeID],
		a.[AssetClassTypeID] AS [ScenarioClassTypeID],
		g.[ParentAssetID],
		a.[AssetDesc] AS [ScenarioDesc],
		s.[Notes],
		createdUser.SecurityUserID AS [CreatedBy],
		CAST(a.[CreateDate] AS DATETIMEOFFSET) AS [CreatedDate],
		changedUser.SecurityUserID AS [ChangedBy],
		CAST(a.[ChangeDate] AS DATETIMEOFFSET) AS [ChangedDate],
		s.[LastRunJobExtID],
		s.[AdHocInputs]
	FROM Asset.tAsset a
	JOIN Scenario.tScenario s on a.AssetID = s.ScenarioID
	JOIN Asset.tAsset g on g.AssetID = a.ParentAssetID
	JOIN AccessControl.tUser createdUser on createdUser.Username = a.CreatedBy
	JOIN AccessControl.tUser changedUser on changedUser.Username = a.ChangedBy
	WHERE a.AssetID = @scenarioID
	AND Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @securityUserID, default) = 1
	AND Asset.ufnDoesUserHaveAccessToAsset(g.ParentAssetID, @securityUserID, default) = 1
)
GO

GRANT SELECT
    ON OBJECT::[Scenario].[ufnGetScenarioByID] TO [TEUser]
    AS [dbo];
GO
