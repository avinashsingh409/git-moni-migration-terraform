﻿
CREATE VIEW [Meter].[vAverageMonthlyAreaConsumption]
WITH SCHEMABINDING
AS

WITH cteRouteMultiples
(
	AssetDesc, AssetID, ParentAssetID, rn
)
AS
(
SELECT
	AssetDesc, AssetID, ParentAssetID,
	ROW_NUMBER() OVER (Partition BY AssetDesc ORDER BY AssetID DESC) as rn
FROM Asset.tAsset
WHERE
	AssetClassTypeID = 6286
)
,
cteRouteAndParent
(
	AssetID,
	AssetDesc,
	ParentAssetID,
	ParentAssetDesc
)
AS
(
	SELECT rts.AssetID, rts.AssetDesc, ars.AssetID, ars.AssetDesc
	FROM cteRouteMultiples rts
	INNER JOIN Asset.tAsset ars
	ON rts.ParentAssetID = ars.AssetID
	WHERE rn = 1
)
,
cteAreaAverage
(
	MeterSize,
	AreaID,
	ConsYear,
	ConsMonth,
	AreaAvgCons
)
AS
(
	SELECT
		ms.MeterSize,
		rtprnt.ParentAssetID,
		YEAR(cons.ReadingDate) AS ConsYear, 
		MONTH(cons.ReadingDate) AS ConsMonth,
        ROUND(AVG(cons.Consumption), 0) AS AreaAvgCons
    FROM   Meter.tConsumption AS cons
    INNER JOIN Meter.tMeterSerialInfo AS ms
    ON cons.EquipmentID = ms.MeterNumber
    INNER JOIN cteRouteAndParent rtprnt
    ON cons.RouteAssetID = rtprnt.AssetID
    WHERE 
		(cons.DoNotUseMultReadNotClosestToAvgRead = 0)
	GROUP BY
		ms.MeterSize,
		rtprnt.ParentAssetID,
		YEAR(cons.ReadingDate),
		MONTH(cons.ReadingDate)
)
SELECT
	MeterSize,
	AreaID,
	ConsYear,
	ConsMonth,
	AreaAvgCons
FROM cteAreaAverage