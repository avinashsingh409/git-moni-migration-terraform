﻿
CREATE VIEW [Meter].[vConsumptionYearsMonths]
WITH SCHEMABINDING
AS

SELECT DISTINCT YEAR(ReadingDate) AS ConsYear, MONTH(ReadingDate) AS ConsMonth
FROM Meter.tConsumption