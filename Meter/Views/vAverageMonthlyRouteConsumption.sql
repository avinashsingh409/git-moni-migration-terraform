﻿
CREATE VIEW [Meter].[vAverageMonthlyRouteConsumption]
WITH SCHEMABINDING
AS

WITH cteRouteAverage
AS
(
SELECT ms.MeterSize, cons.RouteID, YEAR(cons.ReadingDate) AS ConsYear, MONTH(cons.ReadingDate) AS ConsMonth, ROUND(AVG(cons.Consumption), 0) AS RouteAvgCons
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.DoNotUseMultReadNotClosestToAvgRead = 0
GROUP BY ms.MeterSize, cons.RouteID, YEAR(cons.ReadingDate), MONTH(cons.ReadingDate)
)

SELECT MeterSize, RouteID, ConsYear, ConsMonth, RouteAvgCons
FROM cteRouteAverage