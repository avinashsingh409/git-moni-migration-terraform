﻿CREATE TABLE [Meter].[tWaterLossPeerMetrics] (
    [WaterLossPeerMetricsID] INT            IDENTITY (1, 1) NOT NULL,
    [NameOfCityOrUtility]    NVARCHAR (255) NOT NULL,
    [ApparentLossesSCPerDay] FLOAT (53)     NULL,
    [RealLossesSCPerDay]     FLOAT (53)     NULL,
    [PeerAssetID]            INT            NOT NULL,
    CONSTRAINT [PK_tWaterLossPeerMetrics] PRIMARY KEY CLUSTERED ([WaterLossPeerMetricsID] ASC),
    CONSTRAINT [FK_tWaterLossPeerMetrics_Asset_tAsset] FOREIGN KEY ([PeerAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
);

