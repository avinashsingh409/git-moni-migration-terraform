﻿CREATE TABLE [Meter].[tMainsLength] (
    [MainsLengthID]         INT        IDENTITY (1, 1) NOT NULL,
    [OwningAssetID]         INT        NULL,
    [MasterTapSerialInfoID] INT        NULL,
    [MainsLength]           FLOAT (53) NOT NULL,
    CONSTRAINT [PK_tMainsLength] PRIMARY KEY CLUSTERED ([MainsLengthID] ASC),
    CONSTRAINT [FK_tMainsLength_tAsset] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tMainsLength_tMasterTapSerialInfo] FOREIGN KEY ([MasterTapSerialInfoID]) REFERENCES [Meter].[tMasterTapSerialInfo] ([ID])
);

