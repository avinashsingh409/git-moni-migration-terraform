﻿CREATE TABLE [Meter].[tMasterTapSerialInfo] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [OwningAssetID] INT             NOT NULL,
    [RouteAssetID]  INT             NULL,
    [TapNumber]     INT             NOT NULL,
    [MasterMeter]   NVARCHAR (255)  NOT NULL,
    [MM]            DECIMAL (18, 1) NULL,
    [Size]          NVARCHAR (50)   NULL,
    [MeterSize]     INT             NULL,
    [TapOff]        NVARCHAR (50)   NULL,
    [MeterNumber]   NVARCHAR (255)  NULL,
    [InstallDate]   SMALLDATETIME   NULL,
    [WTRClass]      NVARCHAR (50)   NULL,
    [Area]          NVARCHAR (255)  NULL,
    CONSTRAINT [PK_tMasterTapSerialInfoID] PRIMARY KEY CLUSTERED ([ID] ASC)
);

