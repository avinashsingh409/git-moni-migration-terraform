﻿CREATE TABLE [Meter].[tAverageMonthlyConsumption] (
    [ID]            INT        IDENTITY (1, 1) NOT NULL,
    [OwningAssetID] INT        NULL,
    [AreaAssetID]   INT        NOT NULL,
    [RouteAssetID]  INT        NOT NULL,
    [MeterSize]     FLOAT (53) NULL,
    [ConsYear]      INT        NULL,
    [ConsMonth]     INT        NULL,
    [RouteAvgCons]  FLOAT (53) NULL,
    CONSTRAINT [PK_tAverageMonthlyConsumption] PRIMARY KEY CLUSTERED ([ID] ASC)
);

