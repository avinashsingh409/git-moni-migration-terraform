﻿CREATE TABLE [Meter].[tMasterTapMeterSerialInfoMap] (
    [MasterTapMeterSerialInfoMapID] INT            IDENTITY (1, 1) NOT NULL,
    [TapNumber]                     INT            NOT NULL,
    [MIU]                           NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tMasterTapMIUMapID] PRIMARY KEY CLUSTERED ([MasterTapMeterSerialInfoMapID] ASC)
);

