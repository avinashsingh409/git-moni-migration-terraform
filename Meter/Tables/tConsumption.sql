﻿CREATE TABLE [Meter].[tConsumption] (
    [ID]                                    INT             IDENTITY (1, 1) NOT NULL,
    [OwningAssetID]                         INT             NOT NULL,
    [RouteAssetID]                          INT             NULL,
    [Location]                              NVARCHAR (255)  NOT NULL,
    [ServiceAddress]                        NVARCHAR (1024) NOT NULL,
    [EquipmentID]                           NVARCHAR (255)  NOT NULL,
    [RouteID]                               NVARCHAR (50)   NOT NULL,
    [NewRouteID]                            NVARCHAR (50)   NOT NULL,
    [DocumentNumber]                        NVARCHAR (50)   NOT NULL,
    [ServiceType]                           NVARCHAR (50)   NOT NULL,
    [ReadingDate]                           SMALLDATETIME   NOT NULL,
    [CurrentReading]                        FLOAT (53)      NOT NULL,
    [PreviousReading]                       FLOAT (53)      NOT NULL,
    [Consumption]                           FLOAT (53)      NOT NULL,
    [BilledConsumption]                     FLOAT (53)      NOT NULL,
    [MeterChargeAmount]                     FLOAT (53)      NOT NULL,
    [ConnectionDate]                        SMALLDATETIME   NOT NULL,
    [ConnectionSequence]                    INT             NOT NULL,
    [ConnectionStatus]                      NVARCHAR (50)   NOT NULL,
    [EquipmentClass]                        NVARCHAR (50)   NOT NULL,
    [Digits]                                INT             NOT NULL,
    [Size]                                  FLOAT (53)      NOT NULL,
    [MIU]                                   NVARCHAR (255)  NOT NULL,
    [ReadingType]                           NVARCHAR (50)   NOT NULL,
    [FromTable]                             NVARCHAR (50)   NOT NULL,
    [DoNotUseMultReadNotClosestToAvgRead]   BIT             CONSTRAINT [DF_tConsumption_MultReadDoNotUse] DEFAULT ((0)) NOT NULL,
    [DoNotUseMultReadNotEarliestRead]       BIT             CONSTRAINT [DF_tConsumption_DoNotUseMultReadNotEarliestRead] DEFAULT ((0)) NOT NULL,
    [DoNotUseMultReadNotLatestRead]         BIT             CONSTRAINT [DF_tConsumption_DoNotUseMultReadNotLatestRead] DEFAULT ((0)) NOT NULL,
    [DoNotUseMultReadNotHighestConsumption] BIT             CONSTRAINT [DF_tConsumption_DoNotUseMultReadNotHighestConsumption] DEFAULT ((0)) NOT NULL,
    [DeclineFromLastYear]                   BIT             CONSTRAINT [DF_MetertConsumption_DeclineFromLastYear] DEFAULT ((0)) NOT NULL,
    [DecliningUsage]                        BIT             CONSTRAINT [DF_MetertConsumption_DecliningUsage] DEFAULT ((0)) NOT NULL,
    [VarianceFromTypical]                   BIT             CONSTRAINT [DF_MetertConsumption_VarianceFromTypical] DEFAULT ((0)) NOT NULL,
    [LrgMeterVarianceFromTypical]           BIT             CONSTRAINT [DF_MetertConsumption_LrgMeterVarianceFromTypical] DEFAULT ((0)) NOT NULL,
    [MasterTap]                             BIT             CONSTRAINT [DF_MetertConsumption_MasterTap] DEFAULT ((0)) NOT NULL,
    [MIUEqualsMeterNumber]                  BIT             CONSTRAINT [DF_MetertConsumption_MIUEqualsMeterNumber] DEFAULT ((0)) NOT NULL,
    [ZeroRead]                              BIT             CONSTRAINT [DF_MetertConsumption_ZeroRead] DEFAULT ((0)) NOT NULL,
    [AnomalyCount]                          INT             CONSTRAINT [DF_MetertConsumption_AnomalyCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tConsumption_1] PRIMARY KEY CLUSTERED ([ID] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_RouteID]
    ON [Meter].[tConsumption]([RouteID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_RouteAssetID]
    ON [Meter].[tConsumption]([RouteAssetID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_ReadingDate]
    ON [Meter].[tConsumption]([ReadingDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_MIU]
    ON [Meter].[tConsumption]([MIU] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_Location]
    ON [Meter].[tConsumption]([Location] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_EquipmentID]
    ON [Meter].[tConsumption]([EquipmentID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMConsumption_DoNotUseMultReadNotClosestToAvgRead]
    ON [Meter].[tConsumption]([DoNotUseMultReadNotClosestToAvgRead] ASC);

