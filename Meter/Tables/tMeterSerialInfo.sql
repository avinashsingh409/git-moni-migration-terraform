﻿CREATE TABLE [Meter].[tMeterSerialInfo] (
    [Location]               NVARCHAR (255) NULL,
    [MeterNumber]            NVARCHAR (255) NULL,
    [MIU]                    NVARCHAR (255) NULL,
    [MeterSize]              FLOAT (53)     NULL,
    [EquipmentClass]         NVARCHAR (255) NULL,
    [RouteID]                NVARCHAR (255) NULL,
    [AssessorID]             NVARCHAR (255) NULL,
    [Customer]               NVARCHAR (255) NULL,
    [CustomerName]           NVARCHAR (255) NULL,
    [ServiceAddress]         NVARCHAR (255) NULL,
    [Owner]                  NVARCHAR (255) NULL,
    [OwnerName]              NVARCHAR (255) NULL,
    [OwnerAddress]           NVARCHAR (255) NULL,
    [ConnectionDate]         DATETIME       NULL,
    [DisconnectionDate]      DATETIME       NULL,
    [LocationClass]          NVARCHAR (255) NULL,
    [NumberResidentialUnits] FLOAT (53)     NULL,
    [ServiceType]            NVARCHAR (255) NULL,
    [Subdivision]            NVARCHAR (255) NULL,
    [Area]                   NVARCHAR (255) NULL,
    [InstallationDate]       DATETIME       NULL,
    [ConnectStatus]          NVARCHAR (255) NULL,
    [Diff]                   NVARCHAR (255) NULL,
    [Size]                   FLOAT (53)     NULL,
    [umuserdefined1]         NVARCHAR (255) NULL,
    [metertype]              NVARCHAR (255) NULL,
    [AgeYears]               FLOAT (53)     NULL,
    [OwningAssetID]          INT            CONSTRAINT [DF_tMeterSerialInfo_OwningAssetID] DEFAULT ((0)) NOT NULL,
    [RouteAssetID]           INT            NULL,
    [ID]                     INT            IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_tMeterSerialInfo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_tMeterSerialInfo_MeterSize]
    ON [Meter].[tMeterSerialInfo]([MeterSize] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMeterSerialInfo_MeterNumber]
    ON [Meter].[tMeterSerialInfo]([MeterNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMeterSerialInfo_Location]
    ON [Meter].[tMeterSerialInfo]([Location] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMeterSerialInfo_Area]
    ON [Meter].[tMeterSerialInfo]([Area] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tMeterSerialInfo_1]
    ON [Meter].[tMeterSerialInfo]([MeterSize] ASC, [OwningAssetID] ASC, [RouteAssetID] ASC);

