﻿CREATE TABLE [Meter].[tMasterTapConsumption] (
    [ID]                    INT            IDENTITY (1, 1) NOT NULL,
    [OwningAssetID]         INT            NOT NULL,
    [RouteAssetID]          INT            NULL,
    [TapNumber]             INT            NOT NULL,
    [MeterNumber]           NVARCHAR (255) NULL,
    [ClayPSI1]              NVARCHAR (50)  NULL,
    [ClayPSI2]              NVARCHAR (50)  NULL,
    [Rollover1]             INT            NULL,
    [ThisMonthsReading]     INT            NULL,
    [Unknown1]              INT            NULL,
    [Rollover2]             INT            NULL,
    [LastMonthsReading]     INT            NULL,
    [Unknown2]              INT            NULL,
    [ThisMonthsConsumption] INT            NULL,
    [Unknown3]              INT            NULL,
    [LastMonthsConsumption] INT            NULL,
    [Unknown4]              INT            NULL,
    [IncreaseDecrease]      NVARCHAR (50)  NULL,
    [ThisMonthsDate]        SMALLDATETIME  NOT NULL,
    CONSTRAINT [PK_tMasterTapConsumptionID] PRIMARY KEY CLUSTERED ([ID] ASC)
);

