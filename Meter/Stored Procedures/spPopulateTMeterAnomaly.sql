﻿

CREATE PROCEDURE [Meter].[spPopulateTMeterAnomaly] 
	@anomalyStartTime datetime,
	@anomalyEndTime datetime,
	@routeActiveDeclineFromLastYearThreshold float,
	@routeActiveDecliningUsageThreshold float,
	@routeActiveVarianceFromTypical float,
	@routeActiveMtrSizeThreshold float,
	@routeActiveMtrSizeLowerFloor float,
	@routeActiveMtrSizeLowerCeiling float,
	@routeActiveMtrSizeUpperFloor float,
	@routeActiveMtrSizeUpperCeiling float
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @meter TABLE(
		MeterNumber nvarchar(255)
	)

	INSERT INTO @meter (MeterNumber)
	SELECT DISTINCT ms.MeterNumber
	FROM Meter.tMeterSerialInfo ms
	JOIN Meter.tConsumption cons ON ms.MeterNumber = cons.EquipmentID
	WHERE
		ms.ConnectStatus = 'Active'
		AND cons.DoNotUseMultReadNotClosestToAvgRead = 0

	DECLARE @meterNumber as nvarchar(255);
	DECLARE @OK as int;

	DECLARE @owningAssetID as int = null;
	DECLARE @routeAssetID as int = null;
	DECLARE @routeID as nvarchar(255) = null;

	DECLARE @decFromLastYearETL TABLE(
		EquipmentID nvarchar(255)
		, MeterSize float
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, Consumption float
		, LastYearConsumption float
		, PercentDecline float
	)

	DECLARE @decUsageETL TABLE(
		EquipmentID nvarchar(255)
		, MeterSize float
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, Gradient float
	)

	DECLARE @decVarianceETL TABLE(
		EquipmentID nvarchar(255)
		, MeterSize float
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, Consumption float
		, RouteAverageConsumption float
		, Variance float
	)

	DECLARE @decLrgMtrVarianceETL TABLE(
		EquipmentID nvarchar(255)
		, MeterSize float
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, Consumption float
		, RouteAverageCons float
		, Variance float
	)

	DECLARE @decMIUETL TABLE(
		MIU nvarchar(255)
		, MeterNumber nvarchar(255)
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, CurrentReading float
		, PreviousReading float
		, Consumption float
	)

	DECLARE @decZeroETL TABLE(
		EquipmentID nvarchar(255)
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, CurrentReading float
		, PreviousReading float
	)

	DECLARE @decTapETL TABLE(
		TapNumber int
		, MIU nvarchar(255)
		, MeterNumber nvarchar(255)
		, ConnectionDate datetime
		, InstallationDate datetime
		, Customer nvarchar(255)
		, ReadingDate datetime
	
		, CurrentReading float
		, PreviousReading float
		, Consumption float
	)


	DECLARE curMtr INSENSITIVE CURSOR
		FOR
			SELECT MeterNumber
			FROM @meter
	OPEN curMtr;
	FETCH NEXT FROM curMtr INTO @meterNumber


	SET @OK = 1
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN
		
				SET @owningAssetID = null;
				SET @routeAssetID = null;
				SET @routeID = null;
			
				SELECT
					@owningAssetID = ms.OwningAssetID
					, @routeAssetID = ms.RouteAssetID
					, @routeID = ms.RouteID
				FROM Meter.tMeterSerialInfo ms
				JOIN Meter.tConsumption cons ON ms.MeterNumber = cons.EquipmentID
				WHERE
					ms.MeterNumber = @meterNumber
					AND ms.ConnectStatus = 'Active'
					AND cons.DoNotUseMultReadNotClosestToAvgRead = 0;			
			
				-- decline from last year start
				DELETE FROM @decFromLastYearETL;
				INSERT INTO @decFromLastYearETL
				EXEC Meter.spAnomalyRouteActiveDeclineFromLastYear
					@routeID, @anomalyStartTime, @anomalyEndTime, @routeActiveDeclineFromLastYearThreshold, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.DeclineFromLastYear = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decFromLastYearETL etl ON (cons.EquipmentID = etl.EquipmentID) AND (cons.ReadingDate = etl.ReadingDate)
				-- decline from last year end
			
				-- declining usage start
				DELETE FROM @decUsageETL;
				INSERT INTO @decUsageETL
				EXEC [Meter].[spAnomalyRouteActiveDecliningUsage]
					@routeID, @anomalyStartTime, @anomalyEndTime, @routeActiveDecliningUsageThreshold, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.DecliningUsage = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decUsageETL etl ON (cons.EquipmentID = etl.EquipmentID) AND (cons.ReadingDate = etl.ReadingDate)
				-- declining usage end
			
				-- variance from typical start
				DELETE FROM @decVarianceETL;
				INSERT INTO @decVarianceETL
				EXEC [Meter].[spAnomalyRouteActiveVarianceFromTypical]
					@routeID, @anomalyStartTime, @anomalyEndTime, @routeActiveVarianceFromTypical, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.VarianceFromTypical = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decVarianceETL etl ON (cons.EquipmentID = etl.EquipmentID) AND (cons.ReadingDate = etl.ReadingDate)
				-- variance from typcial end
			
				-- lrg meter variance from typical start
				DELETE FROM @decLrgMtrVarianceETL;
				INSERT INTO @decLrgMtrVarianceETL
				EXEC [Meter].[spAnomalyRouteActiveLrgMtrVariTyp]
					@routeID, @anomalyStartTime, @anomalyEndTime, @routeActiveMtrSizeThreshold, @routeActiveMtrSizeLowerFloor, @routeActiveMtrSizeLowerCeiling, @routeActiveMtrSizeUpperFloor, @routeActiveMtrSizeUpperCeiling, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.LrgMeterVarianceFromTypical = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decLrgMtrVarianceETL etl ON (cons.EquipmentID = etl.EquipmentID) AND (cons.ReadingDate = etl.ReadingDate)
				-- lrg meter variance from typcial end
			
				-- miu equals meter number start
				DELETE FROM @decMIUETL;
				INSERT INTO @decMIUETL
				EXEC [Meter].[spAnomalyRouteActiveMIUEqualsMeterNumber]
					@routeID, @anomalyStartTime, @anomalyEndTime, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.MIUEqualsMeterNumber = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decMIUETL etl ON (cons.EquipmentID = etl.MeterNumber) AND (cons.ReadingDate = etl.ReadingDate)
				-- miu equals meter number end
			
				-- zero reads start
				DELETE FROM @decZeroETL;
				INSERT INTO @decZeroETL
				EXEC [Meter].[spAnomalyRouteActiveZeroRead]
					@routeID, @anomalyStartTime, @anomalyEndTime, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.ZeroRead = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decZeroETL etl ON (cons.EquipmentID = etl.EquipmentID) AND (cons.ReadingDate = etl.ReadingDate)
				-- zero reads end
			
				-- master tap start
				DELETE FROM @decTapETL;
				INSERT INTO @decTapETL
				EXEC [Meter].[spAnomalyRouteActiveMasterTap]
					@routeID, @anomalyStartTime, @anomalyEndTime, @meterNumber;
			
				UPDATE Meter.tConsumption
				SET
					Meter.tConsumption.MasterTap = 1
					, Meter.tConsumption.AnomalyCount = cons.AnomalyCount + 1
				FROM Meter.tConsumption cons
				JOIN @decTapETL etl ON (cons.EquipmentID = etl.MeterNumber) AND (cons.ReadingDate = etl.ReadingDate)
				-- master tap end

				FETCH NEXT FROM curMtr INTO @meterNumber
			END
		ELSE
			BEGIN
				SET @OK = 0
			END
		
	CLOSE curMtr;
	DEALLOCATE curMtr;

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spPopulateTMeterAnomaly] TO [TEUser]
    AS [dbo];

