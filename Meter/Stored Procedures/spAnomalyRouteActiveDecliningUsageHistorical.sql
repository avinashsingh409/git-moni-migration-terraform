﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAnomalyRouteActiveDecliningUsageHistorical] 
	-- Add the parameters for the stored procedure here
	@route nvarchar(255),
	@start datetime,
	@end datetime,
	@declinethreshold float

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteYMCrossJoinMeterSize
(
	ConsYear
	, ConsMonth
	, MeterSize
)
AS
(
	SELECT
		cons.ConsYear
		, cons.ConsMonth
		, allsizes.MeterSize
	FROM Meter.vConsumptionYearsMonths cons
	CROSS JOIN
	(
		SELECT DISTINCT MeterSize
		FROM Meter.tMeterSerialInfo
	) allsizes
	WHERE
		cons.ConsYear >= YEAR(@start)
		AND cons.ConsMonth >= MONTH(@start)
		AND cons.ConsYear <= YEAR(@end)
		AND cons.ConsMonth <= MONTH(@end)
)

, cteReads
(
	RID
	, EID
	, RDate
	, Consumption
	, MeterSize
	, rn
)
AS
(
SELECT
	COALESCE(NULLIF(cons.RouteID, ''), @route)
	, cons.EquipmentID
	, cons.ReadingDate
	, cons.Consumption
	, ms.MeterSize
	, ROW_NUMBER() OVER (PARTITION BY cons.EquipmentID ORDER BY cons.ReadingDate DESC) as rn
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.ReadingDate <= @end
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
)

, cteCrtAndPrev
(
	RID
	, EID
	, RDate
	, MeterSize
	, Rn
	, Gradient
)
AS
(
SELECT
	crt.RID
	, crt.EID
	, crt.RDate
	, crt.MeterSize
	, crt.rn
	, (SUM(prev.Consumption - crt.Consumption) OVER (PARTITION BY crt.EID) / (0 - (COUNT(crt.EID) OVER (PARTITION BY crt.EID) -1))) AS Gradient
FROM cteReads crt
LEFT JOIN cteReads prev
ON
	crt.EID = prev.EID
	AND crt.rn = (prev.rn - 1)
)

, cteDeclines
(
	RID
	, ConsYear
	, ConsMonth
	, MeterSize
	, Declines
)
AS
(
SELECT
	RID
	, YEAR(RDate)
	, MONTH(RDate)
	, MeterSize
	, COUNT(EID) AS Declines
FROM cteCrtAndPrev
WHERE
	Rn = 1
	AND Gradient <= @declinethreshold
GROUP BY
	RID
	, YEAR(RDate)
	, MONTH(RDate)
	, MeterSize
)

SELECT yms.ConsYear, yms.ConsMonth, yms.MeterSize, COALESCE(decl.RID, @route) AS RouteID, COALESCE(decl.Declines, 0) AS Declines
FROM cteYMCrossJoinMeterSize yms
LEFT OUTER JOIN cteDeclines decl
ON
	yms.ConsYear = decl.ConsYear
	AND yms.ConsMonth = decl.ConsMonth
	AND yms.MeterSize = decl.MeterSize
ORDER BY yms.ConsYear, yms.ConsMonth, yms.MeterSize

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveDecliningUsageHistorical] TO [TEUser]
    AS [dbo];

