﻿

CREATE PROCEDURE [Meter].[spAnomalyRouteActiveDecliningUsage] 
	@route nvarchar(255),
	@start datetime,
	@end datetime,
	@declinethreshold float,
	@meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

WITH cteReads
(
	EquipmentID
	, MeterSize
	, ConnectionDate
	, InstallationDate
	, Customer
	, ReadingDate
	, Consumption
	, rn
)
AS
(
SELECT
	cons.EquipmentID
	, MeterSize
	, ms.ConnectionDate
	, ms.InstallationDate
	, ms.Customer
	, cons.ReadingDate
	, cons.Consumption
	, ROW_NUMBER() OVER (PARTITION BY cons.EquipmentID ORDER BY cons.ReadingDate DESC) as rn
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.ReadingDate <= @end
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
)
, cteCrtAndPrev
(
	EquipmentID
	, MeterSize
	, ConnectionDate
	, InstallationDate
	, Customer
	, ReadingDate
	, Rn
	, Gradient
)
AS
(
SELECT
	crt.EquipmentID
	, crt.MeterSize
	, crt.ConnectionDate
	, crt.InstallationDate
	, crt.Customer
	, crt.ReadingDate
	, crt.rn
	, (SUM(prev.Consumption - crt.Consumption) OVER (PARTITION BY crt.EquipmentID) / (0 - (COUNT(crt.EquipmentID) OVER (PARTITION BY crt.EquipmentID) -1))) AS Gradient
FROM cteReads crt
LEFT JOIN cteReads prev
ON
	crt.EquipmentID = prev.EquipmentID
	AND crt.rn = (prev.rn - 1)
)

SELECT
	cp.EquipmentID
	, cp.MeterSize
	, cp.ConnectionDate
	, cp.InstallationDate
	, cp.Customer
	, cp.ReadingDate
	, ROUND(cp.Gradient, 2) AS Gradient
FROM cteCrtAndPrev cp
WHERE
	cp.Rn = 1
	AND cp.ReadingDate BETWEEN @start AND @end
	AND Gradient <= @declinethreshold
ORDER BY
	cp.MeterSize
	, cp.ReadingDate
	, cp.EquipmentID

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveDecliningUsage] TO [TEUser]
    AS [dbo];

