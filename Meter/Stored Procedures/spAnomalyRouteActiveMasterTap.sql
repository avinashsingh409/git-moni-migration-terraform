﻿


CREATE PROCEDURE [Meter].[spAnomalyRouteActiveMasterTap] 
	@route nvarchar(255)
	, @start datetime
	, @end datetime
	, @meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

WITH cteTaps
(
	TapNumber
)
AS
(
	SELECT
		DISTINCT map.TapNumber
	FROM Meter.tMeterSerialInfo ms
	JOIN Meter.tMasterTapMeterSerialInfoMap map
	ON ms.MIU = map.MIU
	WHERE
		ms.RouteID = @route
		AND ms.MIU <> ''
		AND ms.ConnectStatus = 'Active'
		AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
)
, cteTapMeters
(
	MeterNumber
	, MIU
	, ConnectionDate
	, InstallationDate
	, Customer
	, TapNumber
)
AS
(
	SELECT
		ms.MeterNumber
		, ms.MIU
		, ms.ConnectionDate
		, ms.InstallationDate
		, ms.Customer
		, map.TapNumber
	FROM Meter.tMeterSerialInfo ms
	JOIN Meter.tMasterTapMeterSerialInfoMap map
	ON ms.MIU = map.MIU
	JOIN cteTaps taps
	ON map.TapNumber = taps.TapNumber
	WHERE
		ms.MIU <> ''
		AND ms.ConnectStatus = 'Active'
		AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
)
,
cteMtrCons
(
	MIU
	, ReadingDate
	, CurrentReading
	, Previousreading
	, Consumption
)
AS
(
	SELECT
		tapmtrs.MIU
		, cons.ReadingDate
		, cons.CurrentReading
		, cons.PreviousReading
		, cons.Consumption
	FROM Meter.tConsumption cons
	JOIN cteTapMeters tapmtrs
	ON cons.EquipmentID = tapmtrs.MeterNumber
	WHERE
		cons.ReadingDate BETWEEN @start AND @end
		AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
)

SELECT
	allmtrs.TapNumber
	, allmtrs.MIU
	, allmtrs.MeterNumber
	, allmtrs.ConnectionDate
	, allmtrs.InstallationDate
	, allmtrs.Customer
	, consmtrs.ReadingDate
	, consmtrs.CurrentReading
	, consmtrs.Previousreading
	, consmtrs.Consumption
FROM cteTapMeters allmtrs
LEFT OUTER JOIN cteMtrCons consmtrs
ON allmtrs.MIU = consmtrs.MIU
ORDER BY
	allmtrs.TapNumber
	, consmtrs.ReadingDate
	, allmtrs.MIU
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveMasterTap] TO [TEUser]
    AS [dbo];

