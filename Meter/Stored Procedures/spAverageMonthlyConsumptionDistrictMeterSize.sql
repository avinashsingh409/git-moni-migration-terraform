﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAverageMonthlyConsumptionDistrictMeterSize]
	-- Add the parameters for the stored procedure here
	@districtassetid int,
	@metersize float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT OwningAssetID AS AssetID, MeterSize, ConsYear, ConsMonth, ROUND(AVG(RouteAvgCons), 0) AS AssetAvgCons
FROM Meter.tAverageMonthlyConsumption
WHERE OwningAssetID = @districtassetid
AND MeterSize = @metersize
GROUP BY OwningAssetID, MeterSize, ConsYear, ConsMonth
ORDER BY MeterSize, ConsYear, ConsMonth
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAverageMonthlyConsumptionDistrictMeterSize] TO [TEUser]
    AS [dbo];

