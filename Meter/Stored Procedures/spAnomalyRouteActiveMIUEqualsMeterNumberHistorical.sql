﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAnomalyRouteActiveMIUEqualsMeterNumberHistorical] 
	-- Add the parameters for the stored procedure here
	@route nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteYMCrossJoinMeterSize
(
	ConsYear,
	ConsMonth,
	MeterSize
)
AS
(
	SELECT yms.ConsYear, yms.ConsMonth, allsizes.MeterSize
	FROM Meter.vConsumptionYearsMonths yms
	CROSS JOIN
	(
		SELECT DISTINCT MeterSize
		FROM Meter.tMeterSerialInfo
	) allsizes
)
,
cteEqualMIUMeterNumber
(
	RouteID,
	ConsYear,
	ConsMonth,
	MeterSize,
	EqualReads
)
AS
(
	SELECT cons.RouteID, YEAR(cons.ReadingDate) AS ConsYear, MONTH(cons.ReadingDate) AS ConsMonth, ms.MeterSize, COUNT(cons.EquipmentID)
	FROM Meter.tConsumption cons
	INNER JOIN Meter.tMeterSerialInfo ms
	ON cons.EquipmentID = ms.MeterNumber
	WHERE
		cons.RouteID = @route
		AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
		AND ms.ConnectStatus = 'Active'
		AND ms.MIU = ms.MeterNumber
	GROUP BY cons.RouteID, YEAR(cons.ReadingDate), MONTH(cons.ReadingDate), ms.MeterSize
)

SELECT yms.ConsYear, yms.ConsMonth, yms.MeterSize, eqs.RouteID, eqs.EqualReads
FROM cteYMCrossJoinMeterSize yms
LEFT OUTER JOIN cteEqualMIUMeterNumber eqs
ON yms.ConsYear = eqs.ConsYear AND yms.ConsMonth = eqs.ConsMonth AND yms.MeterSize = eqs.MeterSize
ORDER BY yms.ConsYear, yms.ConsMonth, yms.MeterSize
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveMIUEqualsMeterNumberHistorical] TO [TEUser]
    AS [dbo];

