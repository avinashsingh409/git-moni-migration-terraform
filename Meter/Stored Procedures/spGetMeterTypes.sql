﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spGetMeterTypes] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT LTRIM(RTRIM(metertype)) AS metertype
	FROM Meter.tMeterSerialInfo
	WHERE metertype IS NOT NULL
	ORDER BY metertype
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spGetMeterTypes] TO [TEUser]
    AS [dbo];

