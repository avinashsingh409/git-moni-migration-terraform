﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAnomalyRouteActiveVarianceFromTypicalHistorical] 
	-- Add the parameters for the stored procedure here
	@route nvarchar(255),
	@variancethreshold float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteYMCrossJoinMeterSize
(
	ConsYear
	, ConsMonth
	, MeterSize
)
AS
(
SELECT
	cons.ConsYear
	, cons.ConsMonth
	, allsizes.MeterSize
FROM Meter.vConsumptionYearsMonths cons
CROSS JOIN
	(
	SELECT DISTINCT MeterSize
	FROM Meter.tMeterSerialInfo
	) allsizes
)

, cteVariances
(
	RID
	, ConsYear
	, ConsMonth
	, MeterSize
	, Variances
)
AS
(
SELECT
	cons.RouteID
	, YEAR(cons.ReadingDate)
	, MONTH(cons.ReadingDate)
	, typs.MeterSize
	, COUNT(cons.EquipmentID)
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
INNER JOIN Meter.vAverageMonthlyRouteConsumption typs
ON
	cons.RouteID = typs.RouteID
	AND YEAR(cons.ReadingDate) = typs.ConsYear
	AND MONTH(cons.ReadingDate) = typs.ConsMonth
	AND ms.MeterSize = typs.MeterSize
WHERE
	cons.RouteID = @route
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND ABS((((cons.Consumption - typs.RouteAvgCons) / COALESCE(NULLIF(typs.RouteAvgCons, 0),1)) * 100)) > ABS(@variancethreshold)
GROUP BY cons.RouteID, YEAR(cons.ReadingDate), MONTH(cons.ReadingDate), typs.MeterSize
)

SELECT
	yms.ConsYear
	, yms.ConsMonth
	, yms.MeterSize
	, COALESCE(vrs.RID, @route) AS RouteID
	, COALESCE(vrs.Variances, 0) AS Variances
FROM cteYMCrossJoinMeterSize yms
LEFT OUTER JOIN cteVariances vrs
ON
	yms.ConsYear = vrs.ConsYear
	AND yms.ConsMonth = vrs.ConsMonth
	AND yms.MeterSize = vrs.MeterSize
ORDER BY yms.ConsYear, yms.ConsMonth, yms.MeterSize

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveVarianceFromTypicalHistorical] TO [TEUser]
    AS [dbo];

