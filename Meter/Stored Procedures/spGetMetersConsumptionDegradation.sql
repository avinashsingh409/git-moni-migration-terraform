﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spGetMetersConsumptionDegradation] 
	-- Add the parameters for the stored procedure here
	@datestart date,
	@dateend date,
	@meterarea nvarchar(255),
	@route nvarchar(255),
	@metersize float,
	@svctype nvarchar(255),
	@cnctstatus nvarchar(255),
	@mtrtype nvarchar(255),
	@clstoavg bit,
	@clstoerlrd bit,
	@clstolatrd bit,
	@clstohicons bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteReads (MNum, MIU, MSize, RID, SvcType, Area, Install, CnctSt, MType, RDate, CuRead, PrRead, Cons)
AS
(
SELECT ms.MeterNumber, ms.MIU, ms.MeterSize, ms.RouteID, ms.ServiceType, ms.[Area], ms.InstallationDate, ms.ConnectStatus, ms.metertype, co.ReadingDate, co.CurrentReading, co.PreviousReading, co.Consumption
FROM Meter.tMeterSerialInfo as ms
LEFT JOIN Meter.tConsumption as co
ON ms.MeterNumber = co.EquipmentID
WHERE
	co.ReadingDate BETWEEN @datestart AND @dateend
	AND (@meterarea IS NULL OR ms.Area = @meterarea)
	AND (@route IS NULL OR ms.RouteID = @route)
	AND (@metersize IS NULL OR ms.MeterSize = @metersize)
	AND (@svctype IS NULL OR ms.ServiceType = @svctype)
	AND (@cnctstatus IS NULL OR ms.ConnectStatus = @cnctstatus)
	AND (@mtrtype IS NULL OR ms.metertype = @mtrtype)
	AND (@clstoavg IS NULL OR co.DoNotUseMultReadNotClosestToAvgRead = @clstoavg)
	AND (@clstoerlrd IS NULL OR co.DoNotUseMultReadNotEarliestRead = @clstoerlrd)
	AND (@clstolatrd IS NULL OR co.DoNotUseMultReadNotLatestRead = @clstolatrd)
	AND (@clstohicons IS NULL OR co.DoNotUseMultReadNotHighestConsumption = @clstohicons)
),

cteMostRecent (MeterNumber, MIU, MeterSize, RouteID, ServiceType, Area, InstallationDate, ConnectStatus, metertype, ReadingDate, CurrentReading, PreviousReading, TotCons, rn)
AS
(
SELECT
	*,
	ROW_NUMBER() OVER (PARTITION BY MNum ORDER BY RDate DESC) AS rn
FROM cteReads
)

SELECT cm.MeterNumber, cm.MIU, cm.MeterSize, cm.RouteID, cm.ServiceType, cm.Area, cm.InstallationDate, cm.ConnectStatus, cm.metertype, cm.ReadingDate, cm.CurrentReading, cm.PreviousReading, SUM(cr.Cons) AS TotCons, COUNT(cr.Cons) AS PeriodReads
FROM cteMostRecent AS cm
INNER JOIN cteReads AS cr
ON cm.MeterNumber = cr.MNum
WHERE cm.rn = 1
GROUP BY cm.MeterNumber, cm.MIU, cm.MeterSize, cm.RouteID, cm.ServiceType, cm.Area, cm.InstallationDate, cm.ConnectStatus, cm.metertype, cm.ReadingDate, cm.CurrentReading, cm.PreviousReading
ORDER BY cm.MeterNumber

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spGetMetersConsumptionDegradation] TO [TEUser]
    AS [dbo];

