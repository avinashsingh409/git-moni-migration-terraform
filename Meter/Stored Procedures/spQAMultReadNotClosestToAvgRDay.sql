﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spQAMultReadNotClosestToAvgRDay] 
	-- Add the parameters for the stored procedure here
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH cteMultReads (EID, YR, MO, NumReads)
	AS
	(
		SELECT EquipmentID, YEAR(ReadingDate) AS ConsumptionYear, MONTH(ReadingDate) AS ConsumptionMonth, COUNT(ReadingDate) AS NumReads
		FROM Meter.tConsumption
		GROUP BY EquipmentID, YEAR(ReadingDate), MONTH(ReadingDate)
		HAVING COUNT(*) > 1
	)
	,
	cteAvgReadDay (EID, AvgRDay)
	AS
	(
		SELECT EquipmentID, AVG(DAY(ReadingDate))
		FROM Meter.tConsumption
		GROUP BY EquipmentID
	)
	,
	cteGetID (ID, EID, YR, MO, NumReads, ReadingDate, PrevRead, CurRead, Consumption, RType)
	AS
	(
	SELECT c.ID, cmr.EID, cmr.YR, cmr.MO, cmr.NumReads, c.ReadingDate, c.PreviousReading, c.CurrentReading, c.Consumption, c.ReadingType
	FROM cteMultReads AS cmr
	INNER JOIN Meter.tConsumption AS C
	ON cmr.EID = c.EquipmentID AND cmr.YR = YEAR(c.ReadingDate) AND cmr.MO = MONTH(c.ReadingDate)
	)
	,
	cteCompare (ID, EID, RYR, RMO, NumReads, RDate, PrevRead, CurRead, Cons, RType, AvgRDay, RDayDiffToAvgRDay)
	AS
	(
	SELECT gi.ID, gi.EID, gi.YR, gi.MO, gi.NumReads, gi.ReadingDate, gi.PrevRead, gi.CurRead, gi.Consumption, gi.RType, cavg.AvgRDay, ABS(DAY(gi.ReadingDate) - cavg.AvgRDay) AS RDayDiffToAvgRDay
	FROM cteGetID AS gi
	INNER JOIN cteAvgReadDay AS cavg
	ON gi.EID = cavg.EID
	)

	SELECT cc.ID, cc.EID, cc.RYR, cc.RMO, cc.NumReads, cc.RDate, cc.PrevRead, cc.CurRead, cc.Cons, cc.RType, cc.AvgRDay, cc.RDayDiffToAvgRDay, cc.RowNumber
	FROM
	(
		SELECT ID, EID, RYR, RMO, NumReads, RDate, PrevRead, CurRead, Cons, RType, AvgRDay, RDayDiffToAvgRDay,
			ROW_NUMBER() OVER (PARTITION BY EID, RMO ORDER BY RDayDiffToAvgRDay, Cons DESC) AS RowNumber
		FROM cteCompare
	) AS cc
	WHERE RowNumber > 1
	ORDER BY EID, RYR, RMO, RDayDiffToAvgRDay
	;

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spQAMultReadNotClosestToAvgRDay] TO [TEUser]
    AS [dbo];

