﻿

CREATE PROCEDURE [Meter].[spAnomalyRouteActiveZeroRead] 
	@route nvarchar(255),
	@start datetime,
	@end datetime,
	@meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

SELECT cons.EquipmentID, ms.ConnectionDate, ms.InstallationDate, ms.Customer, cons.ReadingDate, cons.CurrentReading, cons.PreviousReading
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND cons.ReadingDate BETWEEN @start AND @end
	AND cons.Consumption = 0
	AND ms.ConnectStatus = 'Active'
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveZeroRead] TO [TEUser]
    AS [dbo];

