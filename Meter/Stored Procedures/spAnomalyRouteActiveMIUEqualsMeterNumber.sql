﻿

CREATE PROCEDURE [Meter].[spAnomalyRouteActiveMIUEqualsMeterNumber] 
	@route nvarchar(255),
	@start datetime,
	@end datetime,
	@meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

SELECT ms.MIU, ms.MeterNumber, ms.ConnectionDate, ms.InstallationDate, ms.Customer, cons.ReadingDate, cons.CurrentReading, cons.PreviousReading, cons.Consumption
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND cons.ReadingDate BETWEEN @start AND @end
	AND ms.MIU = ms.MeterNumber
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveMIUEqualsMeterNumber] TO [TEUser]
    AS [dbo];

