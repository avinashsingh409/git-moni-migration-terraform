﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAverageMonthlyConsumptionRouteMeterSize]
	-- Add the parameters for the stored procedure here
	@routeassetid int, 
	@metersize float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT OwningAssetID, RouteAssetID AS AssetID, MeterSize, ConsYear, ConsMonth, RouteAvgCons AS AssetAvgCons
FROM Meter.tAverageMonthlyConsumption
WHERE RouteAssetID = @routeassetid
AND MeterSize = @metersize
ORDER BY MeterSize, ConsYear, ConsMonth
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAverageMonthlyConsumptionRouteMeterSize] TO [TEUser]
    AS [dbo];

