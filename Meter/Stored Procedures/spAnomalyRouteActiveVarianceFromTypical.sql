﻿

CREATE PROCEDURE [Meter].[spAnomalyRouteActiveVarianceFromTypical] 
	@route nvarchar(255),
	@start datetime,
	@end datetime,
	@variancethreshold float,
	@meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

SELECT
	cons.EquipmentID
	, ms.MeterSize
	, ms.ConnectionDate
	, ms.InstallationDate
	, ms.Customer
	, cons.ReadingDate
	, cons.Consumption
	, typs.RouteAvgCons
	, ROUND((((cons.Consumption - typs.RouteAvgCons) / COALESCE(NULLIF(typs.RouteAvgCons, 0),1)) * 100), 2) AS Variance
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
INNER JOIN Meter.vAverageMonthlyRouteConsumption typs
ON
	cons.RouteID = typs.RouteID
	AND YEAR(cons.ReadingDate) = typs.ConsYear
	AND MONTH(cons.ReadingDate) = typs.ConsMonth
	AND ms.MeterSize = typs.MeterSize
WHERE
	cons.RouteID = @route
	AND cons.ReadingDate BETWEEN @start AND @end
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND ABS((((cons.Consumption - typs.RouteAvgCons) / COALESCE(NULLIF(typs.RouteAvgCons, 0),1)) * 100)) > ABS(@variancethreshold)
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
ORDER BY ms.MeterSize, cons.ReadingDate, cons.EquipmentID
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveVarianceFromTypical] TO [TEUser]
    AS [dbo];

