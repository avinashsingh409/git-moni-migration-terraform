﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAnomalyRouteActiveZeroReadHistorical]
	-- Add the parameters for the stored procedure here
	@route nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteYMCrossJoinMeterSize
(
	ConsYear, ConsMonth, MeterSize
)
AS
(
	SELECT cons.ConsYear, cons.ConsMonth, allsizes.MeterSize
	FROM Meter.vConsumptionYearsMonths cons
	CROSS JOIN
	(
		SELECT DISTINCT MeterSize
		FROM Meter.tMeterSerialInfo
	) allsizes
)
,
cteZeros
(
	RouteID,
	ConsYear,
	ConsMonth,
	MeterSize,
	ZeroReads
)
AS
(
SELECT cons.RouteID, YEAR(cons.ReadingDate) AS ConsYear, MONTH(cons.ReadingDate) AS ConsMonth, ms.MeterSize, COUNT(cons.EquipmentID)
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND cons.Consumption = 0
	AND ms.ConnectStatus = 'Active'
GROUP BY cons.RouteID, YEAR(cons.ReadingDate), MONTH(cons.ReadingDate), ms.MeterSize
)

SELECT yms.ConsYear, yms.ConsMonth, yms.MeterSize, COALESCE(zeros.RouteID, @route) AS RouteID, COALESCE(zeros.ZeroReads, 0) AS ZeroReads
FROM cteYMCrossJoinMeterSize yms
LEFT OUTER JOIN cteZeros zeros
ON yms.ConsYear = zeros.ConsYear AND yms.ConsMonth = zeros.ConsMonth AND yms.MeterSize = zeros.MeterSize
ORDER BY yms.ConsYear, yms.ConsMonth
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveZeroReadHistorical] TO [TEUser]
    AS [dbo];

