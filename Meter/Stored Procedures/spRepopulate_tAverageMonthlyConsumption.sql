﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spRepopulate_tAverageMonthlyConsumption] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DELETE
FROM Meter.tAverageMonthlyConsumption
;

WITH cteRouteMultiples
(
	AssetDesc, AssetID, ParentAssetID, rn
)
AS
(
SELECT
	AssetDesc, AssetID, ParentAssetID,
	ROW_NUMBER() OVER (Partition BY AssetDesc ORDER BY AssetID DESC) as rn
FROM Asset.tAsset
WHERE
	AssetClassTypeID = 6286
)
,
cteRouteAndArea
(
	AssetID,
	AssetDesc,
	ParentAssetID,
	ParentAssetDesc,
	OwningAssetID
)
AS
(
	SELECT rts.AssetID, rts.AssetDesc, ars.AssetID, ars.AssetDesc, ars.ParentAssetID
	FROM cteRouteMultiples rts
	INNER JOIN Asset.tAsset ars
	ON rts.ParentAssetID = ars.AssetID
	WHERE rn = 1
)
,
cteYMCrossJoinMeterSize
(
	ConsYear, ConsMonth, MeterSize
)
AS
(
	SELECT cons.ConsYear, cons.ConsMonth, allsizes.MeterSize
	FROM Meter.vConsumptionYearsMonths cons
	CROSS JOIN
	(
		SELECT DISTINCT MeterSize
		FROM Meter.tMeterSerialInfo
	) allsizes
)
,
cteTypicals
(
	OwningAssetID,
	AreaAssetID,
	RouteAssetID,
	MeterSize,
	ConsYear,
	ConsMonth
)
AS
(

	SELECT ra.OwningAssetID, ra.ParentAssetID, ra.AssetID, ymms.MeterSize, ymms.ConsYear, ymms.ConsMonth
	FROM cteYMCrossJoinMeterSize ymms
	CROSS JOIN cteRouteAndArea ra
)
,
cteRouteAverage
(
	MeterSize,
	RouteAssetID,
	ConsYear,
	ConsMonth,
	RouteAvgCons
)
AS
(
SELECT ms.MeterSize, cons.RouteAssetID, YEAR(cons.ReadingDate) AS ConsYear, MONTH(cons.ReadingDate) AS ConsMonth, ROUND(AVG(cons.Consumption), 0) AS RouteAvgCons
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.DoNotUseMultReadNotClosestToAvgRead = 0
GROUP BY ms.MeterSize, cons.RouteAssetID, YEAR(cons.ReadingDate), MONTH(cons.ReadingDate)
)

INSERT INTO [Meter].tAverageMonthlyConsumption
(
	OwningAssetID
	, AreaAssetID
	, RouteAssetID
	, MeterSize
	, ConsYear
	, ConsMonth
	, RouteAvgCons
)
SELECT
	typs.OwningAssetID,
	typs.AreaAssetID,
	typs.RouteAssetID,
	typs.MeterSize,
	typs.ConsYear,
	typs.ConsMonth,
	COALESCE(av.RouteAvgCons, 0) AS RouteAvgCons
FROM cteTypicals typs
LEFT OUTER JOIN cteRouteAverage av
ON
	typs.ConsYear = av.ConsYear
	AND typs.ConsMonth = av.ConsMonth
	AND typs.MeterSize = av.MeterSize
	AND typs.RouteAssetID = av.RouteAssetID

END