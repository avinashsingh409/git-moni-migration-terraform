﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spGetRouteAssetFromRouteDesc] 
	-- Add the parameters for the stored procedure here
	@route nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteRouteMultiples
(
	AssetDesc, AssetID, ParentAssetID, rn
)
AS
(
SELECT
	AssetDesc, AssetID, ParentAssetID,
	ROW_NUMBER() OVER (Partition BY AssetDesc ORDER BY AssetID DESC) as rn
FROM Asset.tAsset
WHERE
	AssetClassTypeID = 6286
	AND AssetDesc = @route
)

SELECT ars.ParentAssetID AS OwningAssetID, ars.AssetID AS AreaAssetID, ars.AssetDesc AS AreaAssetDesc,rts.AssetID AS RouteAssetID, rts.AssetDesc AS RouteAssetDesc 
FROM cteRouteMultiples rts
INNER JOIN Asset.tAsset ars
ON rts.ParentAssetID = ars.AssetID
WHERE rn = 1
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spGetRouteAssetFromRouteDesc] TO [TEUser]
    AS [dbo];

