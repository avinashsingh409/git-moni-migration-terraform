﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spGetMeterConsumption] 
	-- Add the parameters for the stored procedure here
	@meternumber nvarchar(255),
	@clstoavg bit,
	@clstoerlrd bit,
	@clstolatrd bit,
	@clstohicons bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
	yms.ConsYear,
	yms.ConsMonth,
	cons.EquipmentID,
	cons.ReadingDate,
	COALESCE(cons.CurrentReading, 0) AS CurrentReading,
	COALESCE(cons.PreviousReading, 0) AS PreviousReading,
	COALESCE(cons.Consumption, 0) AS Consumption
FROM Meter.vConsumptionYearsMonths yms
LEFT OUTER JOIN
(
	SELECT EquipmentID, ReadingDate, CurrentReading, PreviousReading, Consumption
	FROM Meter.tConsumption
	WHERE
		EquipmentID = @meternumber
		AND (@clstoavg IS NULL OR DoNotUseMultReadNotClosestToAvgRead = @clstoavg)
		AND (@clstoerlrd IS NULL OR DoNotUseMultReadNotEarliestRead = @clstoerlrd)
		AND (@clstolatrd IS NULL OR DoNotUseMultReadNotLatestRead = @clstolatrd)
		AND (@clstohicons IS NULL OR DoNotUseMultReadNotHighestConsumption = @clstohicons)
) cons
ON yms.ConsYear = YEAR(cons.ReadingDate) AND yms.ConsMonth = MONTH(cons.ReadingDate)
ORDER BY yms.ConsYear, yms.ConsMonth
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spGetMeterConsumption] TO [TEUser]
    AS [dbo];

