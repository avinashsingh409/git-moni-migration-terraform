﻿


CREATE PROCEDURE [Meter].[spAnomalyRouteActiveDeclineFromLastYear] 
	@route nvarchar(255)
	, @start datetime
	, @end datetime
	, @declinethreshold float
	, @meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

WITH cteReads
(
	EquipmentID
	, MeterSize
	, ConnectionDate
	, InstallationDate
	, Customer
	, ReadingDate
	, Consumption)
AS
(
SELECT
	cons.EquipmentID
	, ms.MeterSize
	, ms.ConnectionDate
	, ms.InstallationDate
	, ms.Customer
	, cons.ReadingDate
	, cons.Consumption
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.ReadingDate BETWEEN @start AND @end
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
),
cteOneYearAgoReads
(
	EquipmentID
	, ReadingDate
	, Consumption
)
AS
(
SELECT
	cons.EquipmentID
	, cons.ReadingDate
	, cons.Consumption
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND cons.ReadingDate BETWEEN DATEADD(YEAR, -1, @start) AND DATEADD(YEAR, -1, @end)
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
)

SELECT
	crt.EquipmentID
	, crt.MeterSize
	, crt.ConnectionDate
	, crt.InstallationDate
	, crt.Customer
	, crt.ReadingDate
	, crt.Consumption
	, oya.Consumption AS LastYearConsumption
	, ROUND((((crt.Consumption - oya.Consumption) / COALESCE(NULLIF(oya.Consumption, 0),1)) * 100), 2) AS PercentDecline
FROM cteReads crt
INNER JOIN cteOneYearAgoReads oya
ON
	crt.EquipmentID = oya.EquipmentID
	AND YEAR(DATEADD(YEAR, -1, crt.ReadingDate)) = YEAR(oya.ReadingDate)
	AND MONTH(crt.ReadingDate) = MONTH(oya.ReadingDate)
WHERE
	ROUND((((crt.Consumption - oya.Consumption) / COALESCE(NULLIF(oya.Consumption, 0),1)) * 100), 2) <= @declinethreshold
ORDER BY crt.MeterSize, crt.ReadingDate, crt.EquipmentID

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveDeclineFromLastYear] TO [TEUser]
    AS [dbo];

