﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spAnomalyRouteActiveMasterTapHistorical] 
	-- Add the parameters for the stored procedure here
	@route nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteTaps
(
	TapNumber
)
AS
(
	SELECT
		DISTINCT map.TapNumber
	FROM Meter.tMeterSerialInfo ms
	JOIN Meter.tMasterTapMeterSerialInfoMap map
	ON ms.MIU = map.MIU
	WHERE ms.RouteID = @route AND ms.MIU <> ''
)
,
cteTapCons
(
	ConsYear
	, ConsMonth
	, TapNumber
	, ThisMonthsConsumption
)
AS
(
	SELECT
		YEAR(DATEADD(mm, -1, tapcons.ThisMonthsDate))
		, MONTH(DATEADD(mm, -1, tapcons.ThisMonthsDate))
		, taps.TapNumber
		, tapcons.ThisMonthsConsumption
	FROM Meter.tMasterTapConsumption tapcons
	JOIN cteTaps taps
	ON tapcons.TapNumber = taps.TapNumber
)

,
cteMtrCons
(
	MeterReadYear
	, MeterReadMonth
	, TapNumber
	, MeterConsumption
)
AS
(
SELECT
	YEAR(cons.ReadingDate)
	, MONTH(cons.ReadingDate)
	, map.TapNumber
	, SUM(cons.Consumption)
FROM Meter.tConsumption cons
JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
JOIN Meter.tMasterTapMeterSerialInfoMap map
ON ms.MIU = map.MIU
JOIN cteTaps taps
ON map.TapNumber = taps.TapNumber
WHERE
	ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND map.MIU <> ''
GROUP BY
	YEAR(cons.ReadingDate), MONTH(cons.ReadingDate), map.TapNumber
)

SELECT
	yms.ConsYear
	, yms.ConsMonth
	, taps.TapNumber
	, COALESCE(mtrcons.MeterConsumption, 0) AS MeterConsumption
	, COALESCE(tapcons.ThisMonthsConsumption, 0) AS TapConsumption
FROM Meter.vConsumptionYearsMonths yms
CROSS JOIN cteTaps taps
LEFT OUTER JOIN cteTapCons tapcons
ON
	yms.ConsYear = tapcons.ConsYear
	AND yms.ConsMonth = tapcons.ConsMonth
	AND taps.TapNumber = tapcons.TapNumber
-- toggle between these two joins to include / exclude meter consumption
-- for months that dont have tap consumption
JOIN cteMtrCons mtrcons
ON
	yms.ConsYear = mtrcons.MeterReadYear
	AND yms.ConsMonth = mtrcons.MeterReadMonth
	AND taps.TapNumber = mtrcons.TapNumber
--LEFT OUTER JOIN cteMtrCons mtrcons
--ON
--	tapcons.ConsYear = mtrcons.MeterReadYear
--	AND tapcons.ConsMonth = mtrcons.MeterReadMonth
--	AND taps.TapNumber = mtrcons.TapNumber
ORDER BY yms.ConsYear, yms.ConsMonth, taps.TapNumber
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveMasterTapHistorical] TO [TEUser]
    AS [dbo];

