﻿

CREATE PROCEDURE [Meter].[spAnomalyRouteActiveLrgMtrVariTyp] 
	@route nvarchar(255),
	@start datetime,
	@end datetime,
	@metersizethreshold float,
	@variancethresholdlowerfloor float,
	@variancethresholdlowerceiling float,
	@variancethresholdupperfloor float,
	@variancethresholdupperceiling float,
	@meterNumber nvarchar(255) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
	cons.EquipmentID
	, ms.MeterSize
	, ms.ConnectionDate
	, ms.InstallationDate
	, ms.Customer
	, cons.ReadingDate
	, cons.Consumption
	, typs.RouteAvgCons
	, ROUND((((cons.Consumption - typs.RouteAvgCons) / COALESCE(NULLIF(typs.RouteAvgCons, 0),1)) * 100), 2) AS Variance
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
INNER JOIN Meter.vAverageMonthlyRouteConsumption typs
ON
	cons.RouteID = typs.RouteID
	AND YEAR(cons.ReadingDate) = typs.ConsYear
	AND MONTH(cons.ReadingDate) = typs.ConsMonth
	AND ms.MeterSize = typs.MeterSize
WHERE
	cons.RouteID = @route
	AND cons.ReadingDate BETWEEN @start AND @end
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND ms.MeterSize >= @metersizethreshold
	AND
	(
		ABS((((cons.Consumption - typs.RouteAvgCons) / COALESCE(NULLIF(typs.RouteAvgCons, 0),1)) * 100)) BETWEEN ABS(@variancethresholdlowerfloor) AND ABS(@variancethresholdlowerceiling)
		OR ABS((((cons.Consumption - typs.RouteAvgCons) / COALESCE(NULLIF(typs.RouteAvgCons, 0),1)) * 100)) BETWEEN ABS(@variancethresholdupperfloor) AND ABS(@variancethresholdupperceiling)
	)
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
ORDER BY ms.MeterSize, cons.ReadingDate, cons.EquipmentID

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveLrgMtrVariTyp] TO [TEUser]
    AS [dbo];

