﻿


CREATE PROCEDURE [Meter].[spAnomalyRouteActiveDeclineFromLastYearHistorical] 
	@route nvarchar(255),
	@declinethreshold float,
	@meterNumber nvarchar(255) = null
AS
BEGIN
	SET NOCOUNT ON;

WITH cteYMCrossJoinMeterSize
(
	ConsYear, ConsMonth, MeterSize
)
AS
(
	SELECT cons.ConsYear, cons.ConsMonth, allsizes.MeterSize
	FROM Meter.vConsumptionYearsMonths cons
	CROSS JOIN
	(
		SELECT DISTINCT MeterSize
		FROM Meter.tMeterSerialInfo
	) allsizes
),
cteReads(RID, EID, RDate, Cons, MeterSize)
AS
(
SELECT cons.RouteID, cons.EquipmentID, cons.ReadingDate, cons.Consumption, ms.MeterSize
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
),
cteOneYearAgoReads (RID, EID, RDate, Cons)
AS
(
SELECT cons.RouteID, cons.EquipmentID, cons.ReadingDate, cons.Consumption
FROM Meter.tConsumption cons
INNER JOIN Meter.tMeterSerialInfo ms
ON cons.EquipmentID = ms.MeterNumber
WHERE
	cons.RouteID = @route
	AND ms.ConnectStatus = 'Active'
	AND cons.DoNotUseMultReadNotClosestToAvgRead = 0
	AND (@meterNumber IS NULL OR (ms.MeterNumber = @meterNumber))
)
,
cteDeclines
(
	RID,
	ConsYear,
	ConsMonth,
	MeterSize,
	Declines
)
AS
(
SELECT
	crt.RID,
	YEAR(crt.RDate),
	MONTH(crt.RDate),
	crt.MeterSize,
	COUNT(crt.EID)
FROM cteReads crt
INNER JOIN cteOneYearAgoReads oya
ON
	crt.EID = oya.EID
	AND YEAR(DATEADD(year, -1, crt.RDate)) = YEAR(oya.RDate)
	AND MONTH(crt.RDate) = MONTH(oya.RDate)
WHERE ROUND((((crt.Cons - oya.Cons) / COALESCE(NULLIF(oya.Cons, 0),1)) * 100), 2) <= @declinethreshold
GROUP BY crt.RID, YEAR(crt.RDate), MONTH(crt.RDate), crt.MeterSize
)

SELECT yms.ConsYear, yms.ConsMonth, yms.MeterSize, COALESCE(decl.RID, @route) AS RouteID, COALESCE(decl.Declines, 0) AS Declines
FROM cteYMCrossJoinMeterSize yms
LEFT OUTER JOIN cteDeclines decl
ON yms.ConsYear = decl.ConsYear AND yms.ConsMonth = decl.ConsMonth AND yms.MeterSize = decl.MeterSize
ORDER BY yms.ConsYear, yms.ConsMonth

END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spAnomalyRouteActiveDeclineFromLastYearHistorical] TO [TEUser]
    AS [dbo];

