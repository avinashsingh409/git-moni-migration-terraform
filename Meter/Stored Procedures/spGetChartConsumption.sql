﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spGetChartConsumption] 
	-- Add the parameters for the stored procedure here
	@datestart date,
	@dateend date,
	@meterarea nvarchar(255),
	@route nvarchar(255),
	@metersize float,
	@svctype nvarchar(255),
	@cnctstatus nvarchar(255),
	@mtrtype nvarchar(255),
	@clstoavg bit,
	@clstoerlrd bit,
	@clstolatrd bit,
	@clstohicons bit
WITH RECOMPILE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--get consumption for each EquipmentID within the date and route parameters
	WITH cteECons (EID, RY, RM, CT)
	AS
	(
		SELECT c.EquipmentID, YEAR(c.ReadingDate) AS ReadYear, MONTH(c.ReadingDate) AS ReadMonth, SUM(c.Consumption) AS EquipmentConsumption
		FROM Meter.tConsumption AS c
		WHERE
			c.ReadingDate BETWEEN @datestart AND @dateend
			AND (@route IS NULL OR c.RouteID = @route)
			AND (@clstoavg IS NULL OR DoNotUseMultReadNotClosestToAvgRead = @clstoavg)
			AND (@clstoerlrd IS NULL OR DoNotUseMultReadNotEarliestRead = @clstoerlrd)
			AND (@clstolatrd IS NULL OR DoNotUseMultReadNotLatestRead = @clstolatrd)
			AND (@clstohicons IS NULL or DoNotUseMultReadNotHighestConsumption = @clstohicons)
		GROUP BY c.EquipmentID, YEAR(c.ReadingDate), MONTH(c.ReadingDate)
	)
	,
	--get the sizes for all EquipmentIDs and set unknown size (no match in MeterSerialInfo) to -1
	cteAllSizes (MeterSize, FieldA)
	AS
	(
		SELECT DISTINCT COALESCE(s.MeterSize, -1), 13 AS FieldA
		FROM cteECons AS ce
		LEFT JOIN Meter.tMeterSerialInfo AS s
		ON ce.EID = s.MeterNumber
		WHERE 
			(@meterarea IS NULL OR s.Area = @meterarea)
			AND (@metersize IS NULL OR s.MeterSize = @metersize)
			AND (@svctype IS NULL or s.ServiceType = @svctype)
			AND (@cnctstatus IS NULL OR s.ConnectStatus = @cnctstatus)
			AND (@mtrtype IS NULL OR s.metertype = @mtrtype)
	),
	--get all month periods within date parameters
	cteAllMonths (ConsumptionYear, ConsumptionMonth)
	AS
	(
		SELECT DISTINCT YEAR(ReadingDate) AS ConsumptionYear, MONTH(ReadingDate) AS ConsumptionMonth
		FROM Meter.tConsumption
		WHERE
			ReadingDate BETWEEN @datestart AND @dateend
			AND (@route IS NULL OR RouteID = @route)
	),
	--get all possible combos of month period and size
	cteAllCombos (MeterSize, ConsumptionYear, ConsumptionMonth)
	AS
	(
		SELECT cs.MeterSize, am.ConsumptionYear, am.ConsumptionMonth
		FROM cteAllMonths AS am
		INNER JOIN cteAllSizes AS cs
		ON am.ConsumptionMonth <> cs.FieldA
	)

	--get consumption for all possible combos and set to 0 for no month period-size matches
	SELECT ac.ConsumptionYear, ac.ConsumptionMonth, ac.MeterSize, COALESCE(cons.ConsumptionTotal, 0) AS ConsumptionTotal
	FROM cteAllCombos AS ac
	LEFT JOIN
		(
			SELECT COALESCE(s.MeterSize, -1) AS MeterSize, ce.RY, ce.RM, SUM(ce.CT) AS ConsumptionTotal
			FROM cteECons AS ce
			LEFT JOIN Meter.tMeterSerialInfo AS s
			ON ce.EID = s.MeterNumber
			GROUP BY s.MeterSize, ce.RY, ce.RM
		) AS cons
	ON (ac.ConsumptionYear = cons.RY) AND (ac.ConsumptionMonth = cons.RM) AND (ac.MeterSize = cons.MeterSize)
	ORDER BY ConsumptionYear, ConsumptionMonth, MeterSize
	;
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spGetChartConsumption] TO [TEUser]
    AS [dbo];

