﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Meter].[spGetRouteMetersConsumptionAll] 
	-- Add the parameters for the stored procedure here
	@route nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH cteYMCrossJoinEID
(
	ConsYear
	, ConsMonth
	, EquipmentID
)
AS
(
	SELECT
		yms.ConsYear
		, yms.ConsMonth
		, consEIDs.EquipmentID
	FROM Meter.vConsumptionYearsMonths yms
	CROSS JOIN
	(
		SELECT DISTINCT EquipmentID
		FROM Meter.tConsumption
		WHERE RouteID = @route
	) AS consEIDs
)

SELECT
	@route
	, yms.ConsYear
	, yms.ConsMonth
	, ReadingDate
	, yms.EquipmentID
	, routeCons.MeterSize
	, routeCons.Consumption
	, routeCons.ConnectionDate
	, routeCons.InstallationDate
	, routeCons.Customer
FROM
	cteYMCrossJoinEID yms
LEFT OUTER JOIN
	(
		SELECT
			COALESCE(NULLIF(cons.RouteID, ''), @route) RouteID
			, cons.EquipmentID
			, YEAR(cons.ReadingDate) AS ConsYear
			, MONTH(cons.ReadingDate) AS ConsMonth
			, ReadingDate
			, ms.MeterSize
			, cons.Consumption
			, ms.ConnectionDate
			, ms.InstallationDate
			, ms.Customer
		FROM Meter.tConsumption AS cons
		INNER JOIN Meter.tMeterSerialInfo AS ms

		ON cons.EquipmentID = ms.MeterNumber
		WHERE
			cons.RouteID = @route
			AND ms.ConnectStatus = 'Active'
			AND cons.DoNotUseMultReadNotClosestToAvgRead = 0	
	) AS routeCons
ON
	yms.ConsYear = routeCons.ConsYear
	AND yms.ConsMonth = routeCons.ConsMonth
	AND yms.EquipmentID = routeCons.EquipmentID
ORDER BY yms.EquipmentID, yms.ConsYear, yms.ConsMonth
	
END
GO
GRANT EXECUTE
    ON OBJECT::[Meter].[spGetRouteMetersConsumptionAll] TO [TEUser]
    AS [dbo];

