CREATE TABLE [Administration].[tUserEditTable] (
    [UserEditTableID] INT            IDENTITY (1, 1) NOT NULL,
    [TableName]       NVARCHAR (255) NOT NULL,
    [TableDesc]       NVARCHAR (255) NOT NULL,
    [CreatedBy]       NVARCHAR (255) NOT NULL,
    [ChangedBy]       NVARCHAR (255) NOT NULL,
    [CreateDate]      DATETIME       CONSTRAINT [DF_tUserEditTable_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME       CONSTRAINT [DF_tUserEditTable_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tUserEditTable] PRIMARY KEY CLUSTERED ([UserEditTableID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of tables that can be edited in the editing utility.', @level0type = N'SCHEMA', @level0name = N'Administration', @level1type = N'TABLE', @level1name = N'tUserEditTable';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Administration', @level1type = N'TABLE', @level1name = N'tUserEditTable';

