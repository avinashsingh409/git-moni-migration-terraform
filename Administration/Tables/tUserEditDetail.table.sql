CREATE TABLE [Administration].[tUserEditDetail] (
    [UserEditDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [UserEditTableID]  INT            NOT NULL,
    [FieldName]        NVARCHAR (255) NOT NULL,
    [LookupSelect]     VARCHAR (MAX)  NOT NULL,
    [CreatedBy]        NVARCHAR (255) NOT NULL,
    [ChangedBy]        NVARCHAR (255) NOT NULL,
    [CreateDate]       DATETIME       CONSTRAINT [DF_tUserEditDetail_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]       DATETIME       CONSTRAINT [DF_tUserEditDetail_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tUserEditDetail] PRIMARY KEY CLUSTERED ([UserEditDetailID] ASC),
    CONSTRAINT [FK_tUserEditDetail_UserEditTableID_tUserEditTable] FOREIGN KEY ([UserEditTableID]) REFERENCES [Administration].[tUserEditTable] ([UserEditTableID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of table fields (and their select statements) that will appear as combo boxes in the editing utility.', @level0type = N'SCHEMA', @level0name = N'Administration', @level1type = N'TABLE', @level1name = N'tUserEditDetail';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Administration', @level1type = N'TABLE', @level1name = N'tUserEditDetail';

