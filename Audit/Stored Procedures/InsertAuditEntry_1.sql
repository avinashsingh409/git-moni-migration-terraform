﻿CREATE PROCEDURE [Audit].[InsertAuditEntry]
	@UserID int,
	@Audits  CapPrior.tpCashFlowChange READONLY
AS
  BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	
	INSERT INTO Audit.tAudit (AuditTypeId, [TimeStamp], CreatedBy, GlobalID)
	SELECT AuditTypeID, GETDATE(), CreatedBy, GlobalID FROM @Audits	

    COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
    ROLLBACK TRANSACTION;
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    SELECT
      @ErrorMessage = ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE();
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
    END CATCH

  END
GO

GRANT EXECUTE
    ON OBJECT::[Audit].[InsertAuditEntry] TO [TEUser]
    AS [dbo];

