﻿CREATE PROCEDURE [Audit].[spFileUploadAudit]
    @AuditTypeId INT,
    @CreatedBy   INT,
    @Filename    NVARCHAR(255),
    @SizeInKb    BIGINT,
    @Checksum    NVARCHAR(255),
    @Url         NVARCHAR(255),
    @AssetId     INT = null,
    @AuditId     BIGINT OUTPUT
AS BEGIN
  BEGIN TRANSACTION
  BEGIN TRY

  DECLARE @Audits CapPrior.tpCashFlowChange;
  INSERT INTO @Audits (AuditId, AuditTypeID, CreatedBy, ProjectCashFlowID, UnitOfMeasure, AssetId, [TimeStamp], [Type], PreviousValue, PreviousStatus, GlobalID)
  VALUES (-1, @AuditTypeId, @CreatedBy, -1, '', COALESCE(@AssetId, -1), GETDATE(), '', 0, -1, NEWID())

  EXEC Audit.InsertAuditEntry @CreatedBy, @Audits

  INSERT INTO Audit.tFileuploadAudit (AuditId, Filename, SizeInKb, Checksum, Url, AssetID)
  SELECT b.AuditId, @Filename, @SizeInKb, @Checksum, @Url, @AssetId
  FROM @Audits a
  JOIN Audit.tAudit b on a.GlobalID = b.GlobalID

  COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
  ROLLBACK TRANSACTION;
  DECLARE @ErrorMessage NVARCHAR(4000);
  DECLARE @ErrorSeverity INT;
  DECLARE @ErrorState INT;
  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();
  RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH

  SET @AuditId = ISNULL((SELECT b.AuditId FROM Audit.tFileuploadAudit a JOIN Audit.tAudit b ON a.AuditId = b.AuditId JOIN @Audits c ON b.GlobalID = c.GlobalID),-1);
  
END

GO

GRANT EXECUTE
    ON OBJECT::[Audit].[spFileUploadAudit] TO [TEUser]
    AS [dbo];

