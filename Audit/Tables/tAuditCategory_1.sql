﻿CREATE TABLE [Audit].[tAuditCategory] (
    [AuditCategoryId]          INT            IDENTITY (1, 1) NOT NULL,
    [AuditCategoryDescription] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAuditCategory] PRIMARY KEY CLUSTERED ([AuditCategoryId] ASC)
);

