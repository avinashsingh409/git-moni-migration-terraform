﻿CREATE TABLE [Audit].[tAuditType] (
    [AuditTypeId]     INT            IDENTITY (1, 1) NOT NULL,
    [AuditCategoryId] INT            NOT NULL,
    [AuditTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAuditType] PRIMARY KEY CLUSTERED ([AuditTypeId] ASC),
    CONSTRAINT [FK_tAuditType_tAuditCategory] FOREIGN KEY ([AuditCategoryId]) REFERENCES [Audit].[tAuditCategory] ([AuditCategoryId])
);

