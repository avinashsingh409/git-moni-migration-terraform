﻿CREATE TABLE [Audit].[tFileuploadAudit] (
    [FileuploadAuditId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [AuditId]           BIGINT         NOT NULL,
    [Filename]          NVARCHAR (255) NOT NULL,
    [SizeInKb]          BIGINT         NOT NULL,
    [Checksum]          NVARCHAR (255) NOT NULL,
    [Url]               NVARCHAR (255) NOT NULL,
    [AssetID]           INT            NULL,
    [Date]              DATETIME       CONSTRAINT [DF_tFileuploadAudit_Date] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tFileuploadAudit] PRIMARY KEY CLUSTERED ([FileuploadAuditId] ASC),
    CONSTRAINT [FK_tFileuploadAudit_tAudit] FOREIGN KEY ([AuditId]) REFERENCES [Audit].[tAudit] ([AuditId])
);



