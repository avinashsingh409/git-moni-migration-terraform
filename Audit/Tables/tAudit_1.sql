﻿CREATE TABLE [Audit].[tAudit] (
    [AuditId]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [AuditTypeId] INT              NOT NULL,
    [TimeStamp]   DATETIME         NOT NULL,
    [CreatedBy]   INT              NOT NULL,
    [GlobalID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tAudit_GlobalID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tAudit] PRIMARY KEY CLUSTERED ([AuditId] ASC),
    CONSTRAINT [FK_tAudit_tAuditType] FOREIGN KEY ([AuditTypeId]) REFERENCES [Audit].[tAuditType] ([AuditTypeId])
);

GO

CREATE INDEX [IDX_GlobalID_Includes] ON [Audit].[tAudit] ([GlobalID])  INCLUDE ([AuditTypeId], [CreatedBy]) WITH (FILLFACTOR=100);
GO
