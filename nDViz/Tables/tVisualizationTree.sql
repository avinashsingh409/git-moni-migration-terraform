﻿CREATE TABLE [nDViz].[tVisualizationTree] (
    [HierarchyID]  INT          IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (80) NULL,
    [DisplayOrder] INT          CONSTRAINT [DF_VisualizationTree_DisplayOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VisualizationTree] PRIMARY KEY CLUSTERED ([HierarchyID] ASC)
);

