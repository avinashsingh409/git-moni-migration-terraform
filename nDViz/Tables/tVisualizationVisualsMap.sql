﻿CREATE TABLE [nDViz].[tVisualizationVisualsMap] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [HierarchyID]  INT           NOT NULL,
    [NodeID]       INT           NOT NULL,
    [Display]      VARCHAR (255) NOT NULL,
    [nDExpression] VARCHAR (MAX) NOT NULL,
    [DisplayOrder] INT           CONSTRAINT [DF_VisualizationVisualsMap_DisplayOrder] DEFAULT ((0)) NOT NULL,
    [StartTime]    DATETIME      NULL,
    [EndTime]      DATETIME      NULL,
    [nDPath]       VARCHAR (50)  CONSTRAINT [DF_VisualizationVisualsMap_nDPath] DEFAULT ('/') NOT NULL,
    [nDProjectID]  VARCHAR (255) NULL,
    CONSTRAINT [PK_VisualizationVisualsMap] PRIMARY KEY CLUSTERED ([ID] ASC)
);

