﻿CREATE TABLE [nDViz].[tVisualizationHierarchy] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [HierarchyID]  INT           NOT NULL,
    [NodeID]       INT           NOT NULL,
    [NodeType]     VARCHAR (50)  NOT NULL,
    [Display]      VARCHAR (255) NULL,
    [Utility]      VARCHAR (50)  NOT NULL,
    [Asset]        VARCHAR (50)  NOT NULL,
    [WorldView]    VARCHAR (255) NOT NULL,
    [ParentNodeID] INT           NULL,
    [DisplayOrder] INT           CONSTRAINT [DF_VisualizationHierarchy_DisplayOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VisualizationHierarchy] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_VisualizationHierarchy_VisualizationTree] FOREIGN KEY ([HierarchyID]) REFERENCES [nDViz].[tVisualizationTree] ([HierarchyID]) ON DELETE CASCADE
);

