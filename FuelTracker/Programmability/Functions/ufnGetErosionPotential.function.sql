﻿CREATE FUNCTION [FuelTracker].[ufnGetErosionPotential]
(
	@HHV_btulbm real,
	@Ash_percent real,
	@SIO2_percent real,
	@AL2O3_percent real 	
)
RETURNS real
AS
BEGIN	
	DECLARE 
		@ErosionFactor real,
		@ErosionPotential real

	SET @ErosionFactor = 0.0
	IF @HHV_btulbm > 0 
	BEGIN
		SET @ErosionFactor = @SIO2_percent/100.0 + @AL2O3_percent/100.0 * @Ash_percent/100.0 / @HHV_btulbm * 1000000.0
	END
	SET @ErosionPotential = CASE WHEN @ErosionFactor >= 0.0  AND @ErosionFactor < 8.0  THEN 2
							     WHEN @ErosionFactor >= 8.0  AND @ErosionFactor < 12.0 THEN 4
							     WHEN @ErosionFactor >= 12.0 AND @ErosionFactor < 17.0 THEN 6							     
							     ELSE 8 END 
	RETURN @ErosionPotential
END

GO
GRANT EXECUTE
    ON OBJECT::[FuelTracker].[ufnGetErosionPotential] TO [TEUser]
    AS [dbo];

