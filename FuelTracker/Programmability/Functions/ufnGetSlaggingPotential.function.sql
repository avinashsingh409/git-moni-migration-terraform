﻿CREATE FUNCTION [FuelTracker].[ufnGetSlaggingPotential]
(
	@Ash_percent real,
	@NA2O_percent real,
	@CAO_percent real,
	@MGO_percent real,
	@Fe2O3_percent real,	
	@K2O_percent real,
	@SIO2_percent real,
	@AL2O3_percent real,
	@TIO2_percent real,
	@BaseToAcidRatio_fraction real,
	@AshFusionHem_f real,
	@AshFusionID_f real,
	@Sulfur_percent real,
	@Moisture_percent real 	
)
RETURNS real
AS
BEGIN	
	DECLARE @SlaggingPotential real
	SET @SlaggingPotential = 0
	IF @Fe2O3_percent > 0
	BEGIN
		--If the ash is lignitic use the western indexes if not use the eastern
		IF (@CAO_percent + @MGO_percent)/@Fe2O3_percent > 1
		BEGIN 
			SELECT @SlaggingPotential = CEILING(AVG(SlaggingPotential))
			FROM [Projection].[ufnGetSlaggingIndexTable] (@Ash_percent,@NA2O_percent,@CAO_percent,@MGO_percent,@Fe2O3_percent,@K2O_percent,@SIO2_percent,@AL2O3_percent,@TIO2_percent,@BaseToAcidRatio_fraction,@AshFusionHem_f,@AshFusionID_f,@Sulfur_percent,@Moisture_percent)
			WHERE SlaggingIndex in (0,1,2,3,4,5,11,12,13,14,15,16) AND SlaggingPotential > 0
		END
		ELSE
		BEGIN
			SELECT @SlaggingPotential = CEILING(AVG(SlaggingPotential))
			FROM [Projection].[ufnGetSlaggingIndexTable] (@Ash_percent,@NA2O_percent,@CAO_percent,@MGO_percent,@Fe2O3_percent,@K2O_percent,@SIO2_percent,@AL2O3_percent,@TIO2_percent,@BaseToAcidRatio_fraction,@AshFusionHem_f,@AshFusionID_f,@Sulfur_percent,@Moisture_percent)
			WHERE SlaggingIndex in (6,7,8,9,10,11,12,13,14,15,16) AND SlaggingPotential > 0
		END
	END
	RETURN @SlaggingPotential
END

GO
GRANT EXECUTE
    ON OBJECT::[FuelTracker].[ufnGetSlaggingPotential] TO [TEUser]
    AS [dbo];

