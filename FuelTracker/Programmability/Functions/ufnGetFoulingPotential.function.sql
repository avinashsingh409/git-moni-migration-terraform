﻿CREATE FUNCTION [FuelTracker].[ufnGetFoulingPotential]
(
	@Ash_percent real,
	@NA2O_percent real,
	@CAO_percent real,
	@MGO_percent real,
	@Fe2O3_percent real,	
	@K2O_percent real,
	@SIO2_percent real,
	@AL2O3_percent real,
	@TIO2_percent real	
)
RETURNS real
AS
BEGIN
	DECLARE @FoulingPotential real
	SET @FoulingPotential = 0
	IF @Fe2O3_percent > 0
	BEGIN
		--If the ash is lignitic use the western indexes if not use the eastern
		IF (@CAO_percent + @MGO_percent)/@Fe2O3_percent > 1
		BEGIN 
			SELECT @FoulingPotential = CEILING(AVG(FoulingPotential))
			FROM [Projection].[ufnGetFoulingIndexTable] (@Ash_percent,@NA2O_percent,@CAO_percent,@MGO_percent,@Fe2O3_percent,@K2O_percent,@SIO2_percent,@AL2O3_percent,@TIO2_percent)
			WHERE FoulingIndex in (0,1,2,3,4) AND FoulingPotential > 0
		END
		ELSE
		BEGIN
			SELECT @FoulingPotential = CEILING(AVG(FoulingPotential))
			FROM [Projection].[ufnGetFoulingIndexTable] (@Ash_percent,@NA2O_percent,@CAO_percent,@MGO_percent,@Fe2O3_percent,@K2O_percent,@SIO2_percent,@AL2O3_percent,@TIO2_percent)
			WHERE FoulingIndex in (5,6,7,8,9,10) AND FoulingPotential > 0
		END
	END
	RETURN @FoulingPotential
END

GO
GRANT EXECUTE
    ON OBJECT::[FuelTracker].[ufnGetFoulingPotential] TO [TEUser]
    AS [dbo];

