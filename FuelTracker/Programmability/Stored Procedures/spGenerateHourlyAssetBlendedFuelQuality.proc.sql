﻿CREATE PROCEDURE [FuelTracker].[spGenerateHourlyAssetBlendedFuelQuality] 
	@StartDate DateTime,
	@EndDate DateTime
AS
BEGIN
	SET NOCOUNT ON;

	--Round the start date up to the next hour
	--Round the end date down to the last full hour	
	SELECT DATEADD(mi,60-DATEPART(mi,@StartDate),@StartDate)
	SELECT DATEADD(mi,DATEPART(mi,@EndDate)*-1,@EndDate)

	--Get the fuels moved through each asset from the fuel tracker transactions
	SELECT b.AssetID, CASE WHEN d.IsUsedForNodeContent = 0 OR d.IsUsedForNodeContent is null THEN e.FuelQualityID ELSE d.FuelQualityID END as FuelQualityID, 
	c.FuelSourceID, c.Quantity_percent, YEAR(a.CurrentTimestamp) [Year], MONTH(a.CurrentTimestamp) [Month], 
	DAY(a.CurrentTimestamp) [Day], DATEPART(hh,a.CurrentTimestamp) [Hour], a.CurrentTimestamp
	,a.Quantity_ton
	,CONVERT(tinyint,0) ProximateBasis				
	,CONVERT(real,0.0) HHV_btulbm					
	,CONVERT(real,0.0) Moisture_percent			
	,CONVERT(real,0.0) InherentMoisture_percent	
	,CONVERT(real,0.0) Ash_percent				
	,CONVERT(real,0.0) Sulfur_percent				
	,CONVERT(real,0.0) VolatileMatter_percent		
	,CONVERT(real,0.0) FixedCarbon_percent		
	,CONVERT(tinyint,0) UltimateBasis				
	,CONVERT(real,0.0) Carbon_percent				
	,CONVERT(real,0.0) Hydrogen_percent			
	,CONVERT(real,0.0) Nitrogen_percent			
	,CONVERT(real,0.0) Chlorine_percent			
	,CONVERT(real,0.0) Oxygen_percent				
	,CONVERT(real,0.0) SIO2_percent				
	,CONVERT(real,0.0) AL2O3_percent				
	,CONVERT(real,0.0) TIO2_percent				
	,CONVERT(real,0.0) Fe2O3_percent				
	,CONVERT(real,0.0) CAO_percent				
	,CONVERT(real,0.0) MGO_percent				
	,CONVERT(real,0.0) K2O_percent				
	,CONVERT(real,0.0) NA2O_percent				
	,CONVERT(real,0.0) SO3_percent				
	,CONVERT(real,0.0) P2O5_percent				
	,CONVERT(real,0.0) SRO_percent				
	,CONVERT(real,0.0) BAO_percent				
	,CONVERT(real,0.0) MN3O4_percent				
	,CONVERT(real,0.0) AshUndetermined_percent	
	,CONVERT(real,0.0) AshFusionReducingInitialDeformation_f		
	,CONVERT(real,0.0) AshFusionReducingSoftening_f				
	,CONVERT(real,0.0) AshFusionReducingHemispherical_f			
	,CONVERT(real,0.0) AshFusionReducingFluid_f					
	,CONVERT(real,0.0) AshFusionOxidizingInitialDeformation_f		
	,CONVERT(real,0.0) AshFusionOxidizingSoftening_f				
	,CONVERT(real,0.0) AshFusionOxidizingHemispherical_f			
	,CONVERT(real,0.0) AshFusionOxidizingFluid_f					
	,CONVERT(real,0.0) T250_f										
	,CONVERT(real,0.0) HGI										
	,CONVERT(real,0.0) BaseToAcidRatio_fraction					
	,CONVERT(real,0.0) EquilibriumMoisture_percent				
	,CONVERT(real,0.0) SulfurFormPyritic_percent					
	,CONVERT(real,0.0) SulfurFormSulfate_percent					
	,CONVERT(real,0.0) SulfurFormOrganic_percent					
	,CONVERT(real,0.0) Mercury_ppm								
	INTO #tmpAssetTransactionDetails
	FROM FuelTracker.tTransaction a INNER JOIN FuelTracker.tNode b ON a.FTNodeID = b.FTNodeID
	INNER JOIN FuelTracker.tTransactionSource c ON a.FTTransactionID = c.FTTransactionID
	LEFT OUTER JOIN FuelTracker.tTransactionQuality d ON a.FTTransactionID = d.FTTransactionID AND c.FTTransactionSourceID = d.FTTransactionSourceID 
	LEFT OUTER JOIN FuelTracker.tTransactionQuality e ON a.FTTransactionID = e.FTTransactionID AND e.FTTransactionSourceID is null 
	WHERE b.AssetID is not null AND a.Quantity_ton > 0
	AND c.IsUsedForNodeContent = 1 
	AND (d.IsUsedForNodeContent = 1 OR e.IsUsedForNodeContent = 1)
	AND a.CurrentTimestamp BETWEEN @StartDate AND @EndDate

	--Setup the Fuel Quality
	UPDATE #tmpAssetTransactionDetails SET
		 ProximateBasis	= b.ProximateBasis
		,HHV_btulbm	= b.HHV_btulbm
		,Moisture_percent = b.Moisture_percent
		,InherentMoisture_percent =	b.InherentMoisture_percent
		,Ash_percent = b.Ash_percent
		,Sulfur_percent = b.Sulfur_percent
		,VolatileMatter_percent = b.VolatileMatter_percent
		,FixedCarbon_percent = b.FixedCarbon_percent
		,UltimateBasis = b.UltimateBasis
		,Carbon_percent = b.Carbon_percent
		,Hydrogen_percent = b.Hydrogen_percent
		,Nitrogen_percent =	b.Nitrogen_percent
		,Chlorine_percent =	b.Chlorine_percent
		,Oxygen_percent = b.Oxygen_percent
		,SIO2_percent = b.SIO2_percent
		,AL2O3_percent = b.AL2O3_percent
		,TIO2_percent =	b.TIO2_percent
		,Fe2O3_percent = b.Fe2O3_percent
		,CAO_percent = b.CAO_percent
		,MGO_percent = b.MGO_percent
		,K2O_percent = b.K2O_percent
		,NA2O_percent = b.NA2O_percent
		,SO3_percent = b.SO3_percent
		,P2O5_percent = b.P2O5_percent
		,SRO_percent = b.SRO_percent
		,BAO_percent = b.BAO_percent
		,MN3O4_percent = b.MN3O4_percent
		,AshUndetermined_percent = b.AshUndetermined_percent
		,AshFusionReducingInitialDeformation_f = b.AshFusionReducingInitialDeformation_f
		,AshFusionReducingSoftening_f = b.AshFusionReducingSoftening_f
		,AshFusionReducingHemispherical_f = b.AshFusionReducingHemispherical_f
		,AshFusionReducingFluid_f =	b.AshFusionReducingFluid_f
		,AshFusionOxidizingInitialDeformation_f = b.AshFusionOxidizingInitialDeformation_f
		,AshFusionOxidizingSoftening_f = b.AshFusionOxidizingSoftening_f
		,AshFusionOxidizingHemispherical_f = b.AshFusionOxidizingHemispherical_f
		,AshFusionOxidizingFluid_f = b.AshFusionOxidizingFluid_f
		,T250_f = b.T250_f
		,HGI = b.HGI
		,BaseToAcidRatio_fraction = b.BaseToAcidRatio_fraction
		,EquilibriumMoisture_percent = b.EquilibriumMoisture_percent
		,SulfurFormPyritic_percent = b.SulfurFormPyritic_percent
		,SulfurFormSulfate_percent = b.SulfurFormSulfate_percent
		,SulfurFormOrganic_percent = b.SulfurFormOrganic_percent
		,Mercury_ppm = b.Mercury_ppm
	FROM #tmpAssetTransactionDetails a INNER JOIN Fuel.tSolidFuelQuality b ON a.FuelQualityID = b.FuelQualityID
	
	--Clear any data we should be replacing
	DELETE FROM Actuals.tAssetBlendedFuelQuality
	WHERE Base.DateSerial([Year],[Month],[Day],[Hour],0,0) BETWEEN @StartDate AND @EndDate;

	DELETE FROM Actuals.tAssetBlendedFuelQualityStatus
	WHERE Base.DateSerial([Year],[Month],[Day],[Hour],0,0) BETWEEN @StartDate AND @EndDate;

	SELECT ROW_NUMBER() OVER (ORDER BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) ID
		,ROW_NUMBER() OVER (PARTITION BY a.AssetID, a.[Year], a.[Month], a.[Day], a.[Hour] ORDER BY a.AssetID, a.[Year], a.[Month], a.[Day], a.[Hour], a.Quantity_percent DESC ) SEQ		
		,COUNT(1) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) BlendNum
		,a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]
		,b.FuelBasinID
		,CONVERT(int,NULL) BasinID		
		,b.FuelSourceDesc FuelQualityDesc
		,CONVERT(NVARCHAR(255),'') BlendDescription 
		,Quantity_ton
		,ProximateBasis
		,HHV_btulbm
		,SUM(CASE WHEN ISNULL(HHV_btulbm,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) HHVMass_fraction
		,Moisture_percent
		,SUM(CASE WHEN ISNULL(Moisture_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) MoistureMass_fraction
		,InherentMoisture_percent
		,SUM(CASE WHEN ISNULL(InherentMoisture_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) InherentMoistureMass_fraction
		,Ash_percent
		,SUM(CASE WHEN ISNULL(Ash_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) AshMass_fraction
		,Sulfur_percent
		,SUM(CASE WHEN ISNULL(Sulfur_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SulfurMass_fraction
		,VolatileMatter_percent
		,SUM(CASE WHEN ISNULL(VolatileMatter_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) VolatileMatterMass_fraction
		,FixedCarbon_percent
		,SUM(CASE WHEN ISNULL(FixedCarbon_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) FixedCarbonMass_fraction
		,UltimateBasis	
		,Carbon_percent
		,SUM(CASE WHEN ISNULL(Carbon_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) CarbonMass_fraction
		,Hydrogen_percent
		,SUM(CASE WHEN ISNULL(Hydrogen_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) HydrogenMass_fraction
		,Nitrogen_percent
		,SUM(CASE WHEN ISNULL(Nitrogen_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) NitrogenMass_fraction
		,Chlorine_percent
		,SUM(CASE WHEN ISNULL(Chlorine_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) ChlorineMass_fraction
		,Oxygen_percent
		,SUM(CASE WHEN ISNULL(Oxygen_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) OxygenMass_fraction
		,SiO2_percent
		,SUM(CASE WHEN ISNULL(SiO2_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SiO2Mass_fraction
		,Al2O3_percent
		,SUM(CASE WHEN ISNULL(Al2O3_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) Al2O3Mass_fraction
		,TiO2_percent
		,SUM(CASE WHEN ISNULL(TiO2_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) TiO2Mass_fraction
		,Fe2O3_percent
		,SUM(CASE WHEN ISNULL(Fe2O3_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) Fe2O3Mass_fraction
		,CaO_percent
		,SUM(CASE WHEN ISNULL(CaO_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) CaOMass_fraction
		,MgO_percent
		,SUM(CASE WHEN ISNULL(MgO_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) MgOMass_fraction
		,K2O_percent
		,SUM(CASE WHEN ISNULL(K2O_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) K2OMass_fraction
		,Na2O_percent
		,SUM(CASE WHEN ISNULL(Na2O_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) Na2OMass_fraction
		,SO3_percent
		,SUM(CASE WHEN ISNULL(SO3_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SO3Mass_fraction
		,P2O5_percent
		,SUM(CASE WHEN ISNULL(P2O5_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) P2O5Mass_fraction
		,SrO_percent
		,SUM(CASE WHEN ISNULL(SrO_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SrOMass_fraction
		,BaO_percent
		,SUM(CASE WHEN ISNULL(BaO_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) BaOMass_fraction
		,Mn3O4_percent
		,SUM(CASE WHEN ISNULL(Mn3O4_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) Mn3O4Mass_fraction
		,AshUndetermined_percent
		,SUM(CASE WHEN ISNULL(AshUndetermined_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0*Ash_percent ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) AshUndeterminedMass_fraction
		,AshFusionReducingInitialDeformation_f
		,SUM(CASE WHEN ISNULL(AshFusionReducingInitialDeformation_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) ReducingInitialDeformationMass_fraction
		,AshFusionReducingSoftening_f
		,SUM(CASE WHEN ISNULL(AshFusionReducingSoftening_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) ReducingSofteningMass_fraction
		,AshFusionReducingHemispherical_f
		,SUM(CASE WHEN ISNULL(AshFusionReducingHemispherical_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) ReducingHemisphericalMass_fraction
		,AshFusionReducingFluid_f
		,SUM(CASE WHEN ISNULL(AshFusionReducingFluid_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) ReducingFluidMass_fraction
		,AshFusionOxidizingInitialDeformation_f
		,SUM(CASE WHEN ISNULL(AshFusionOxidizingInitialDeformation_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) OxidizingInitialDeformationMass_fraction
		,AshFusionOxidizingSoftening_f
		,SUM(CASE WHEN ISNULL(AshFusionOxidizingSoftening_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) OxidizingSofteningMass_fraction
		,AshFusionOxidizingHemispherical_f
		,SUM(CASE WHEN ISNULL(AshFusionOxidizingHemispherical_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) OxidizingHemisphericalMass_fraction
		,AshFusionOxidizingFluid_f
		,SUM(CASE WHEN ISNULL(AshFusionOxidizingFluid_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) OxidizingFluidMass_fraction
		,T250_f
		,SUM(CASE WHEN ISNULL(T250_f,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) T250Mass_fraction
		,HGI
		,SUM(CASE WHEN ISNULL(HGI,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) HGIMass_fraction
		,BaseToAcidRatio_fraction
		,SUM(CASE WHEN ISNULL(BaseToAcidRatio_fraction,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) BaseToAcidRatioMass_fraction
		,EquilibriumMoisture_percent
		,SUM(CASE WHEN ISNULL(EquilibriumMoisture_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) EquilibriumMoistureMass_fraction
		,SulfurFormPyritic_percent
		,SUM(CASE WHEN ISNULL(SulfurFormPyritic_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SulfurPyriticMass_fraction
		,SulfurFormSulfate_percent
		,SUM(CASE WHEN ISNULL(SulfurFormSulfate_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SulfurSulfateMass_fraction
		,SulfurFormOrganic_percent
		,SUM(CASE WHEN ISNULL(SulfurFormOrganic_percent,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) SulfurOrganicMass_fraction
		,Mercury_ppm
		,SUM(CASE WHEN ISNULL(Mercury_ppm,0) > 0 THEN ROUND(a.Quantity_percent,0)/100.0 ELSE 0 END) OVER (PARTITION BY a.AssetID,a.[Year],a.[Month],a.[Day],a.[Hour]) MercuryMass_fraction
		,ROUND(ROUND(a.Quantity_percent,0),0)/100.0 ParentMass_fraction	
		,ROUND(a.Quantity_percent,0)/100.0*Ash_percent AshParentMass_fraction	
		,CASE WHEN ISNULL(InherentMoisture_percent,0) < 100.0 THEN (100.0-Moisture_percent)/(100.0-InherentMoisture_percent) ELSE 1 END AirDriedConversion
		,(100.0-Moisture_percent)/100.0 DryConversion
	INTO #tmpFractionFuelQuality
	FROM #tmpAssetTransactionDetails a INNER JOIN Fuel.tFuelSource b ON a.FuelSourceID = b.FuelSourceID	

	--Set all of the basis for the quality to wet
	UPDATE #tmpFractionFuelQuality SET
	HHV_btulbm = sub.HHV,
	Ash_percent = sub.Ash,
	Sulfur_percent = sub.Sulfur,
	VolatileMatter_percent = sub.VM,
	FixedCarbon_percent = sub.FC,
	Carbon_percent = sub.Carbon,
	Hydrogen_percent = sub.Hydrogen,
	Nitrogen_percent = sub.Nitrogen,
	Chlorine_percent = sub.Chlorine,
	Oxygen_percent = sub.Oxygen
	FROM (SELECT ID
		,CASE ProximateBasis WHEN 3 THEN HHV_btulbm*AirDriedConversion WHEN 2 THEN HHV_btulbm*DryConversion ELSE HHV_btulbm END HHV
		,CASE ProximateBasis WHEN 3 THEN Ash_percent*AirDriedConversion WHEN 2 THEN Ash_percent*DryConversion ELSE Ash_percent END Ash
		,CASE ProximateBasis WHEN 3 THEN Sulfur_percent*AirDriedConversion WHEN 2 THEN Sulfur_percent*DryConversion ELSE Sulfur_percent END Sulfur
		,CASE ProximateBasis WHEN 3 THEN VolatileMatter_percent*AirDriedConversion WHEN 2 THEN VolatileMatter_percent*DryConversion ELSE VolatileMatter_percent END VM
		,CASE ProximateBasis WHEN 3 THEN FixedCarbon_percent*AirDriedConversion WHEN 2 THEN FixedCarbon_percent*DryConversion ELSE FixedCarbon_percent END FC
		,CASE UltimateBasis WHEN 3 THEN Carbon_percent*AirDriedConversion WHEN 2 THEN Carbon_percent*DryConversion ELSE Carbon_percent END Carbon
		,CASE UltimateBasis WHEN 3 THEN Hydrogen_percent*AirDriedConversion WHEN 2 THEN Hydrogen_percent*DryConversion ELSE Hydrogen_percent END Hydrogen
		,CASE UltimateBasis WHEN 3 THEN Nitrogen_percent*AirDriedConversion WHEN 2 THEN Nitrogen_percent*DryConversion ELSE Nitrogen_percent END Nitrogen
		,CASE UltimateBasis WHEN 3 THEN Chlorine_percent*AirDriedConversion WHEN 2 THEN Chlorine_percent*DryConversion ELSE Chlorine_percent END Chlorine
		,CASE UltimateBasis WHEN 3 THEN Oxygen_percent*AirDriedConversion WHEN 2 THEN Oxygen_percent*DryConversion ELSE Oxygen_percent END Oxygen
	FROM #tmpFractionFuelQuality) as sub
	WHERE #tmpFractionFuelQuality.ID = sub.ID;	

	--Set the blend name but only the top 2 are included
	UPDATE #tmpFractionFuelQuality SET BlendDescription = sub.FuelDesc FROM
	(SELECT AssetID,[Year],[Month],[Day],[Hour],
           MAX( CASE SEQ WHEN 1 THEN CONVERT(nvarchar(20),ParentMass_fraction*100.0) + '% ' + FuelQualityDesc ELSE '' END ) + '/' +
           MAX( CASE SEQ WHEN 2 THEN CONVERT(nvarchar(20),ParentMass_fraction*100.0) + '% ' + FuelQualityDesc ELSE '' END ) + ' ' FuelDesc           
    FROM #tmpFractionFuelQuality
	WHERE BlendNum > 1
    GROUP BY AssetID,[Year],[Month],[Day],[Hour]) as sub
	WHERE #tmpFractionFuelQuality.AssetID = sub.AssetID
	AND #tmpFractionFuelQuality.[Year] = sub.[Year]
	AND #tmpFractionFuelQuality.[Month] = sub.[Month]
	AND #tmpFractionFuelQuality.[Day] = sub.[Day]
	AND #tmpFractionFuelQuality.[Hour] = sub.[Hour];

	--Set the unblended name for all of the other fuels
	UPDATE #tmpFractionFuelQuality SET BlendDescription = FuelQualityDesc WHERE BlendNum = 1;

	--Set the basin where we can find a basin match with the blend basin definition
	UPDATE #tmpFractionFuelQuality SET BasinID = sub1.FuelBasinID FROM
	(SELECT sub.AssetID,sub.[Year],sub.[Month],sub.[Day],sub.[Hour],a.FuelBasinID
	 FROM
	(SELECT AssetID,[Year],[Month],[Day],[Hour], 
	 MAX( CASE SEQ WHEN 1 THEN FuelBasinID ELSE NULL END ) Basin1,
	 MAX( CASE SEQ WHEN 2 THEN FuelBasinID ELSE NULL END ) Basin2             
     FROM #tmpFractionFuelQuality
	 WHERE BlendNum > 1 
     GROUP BY AssetID,[Year],[Month],[Day],[Hour]) as sub INNER JOIN tFuelBasinBlendDefinition a 
	 ON sub.Basin1 = a.ParentBasin1ID AND sub.Basin2 = a.ParentBasin2ID) sub1
	WHERE #tmpFractionFuelQuality.AssetID = sub1.AssetID
	AND #tmpFractionFuelQuality.[Year] = sub1.[Year]
	AND #tmpFractionFuelQuality.[Month] = sub1.[Month]
	AND #tmpFractionFuelQuality.[Day] = sub1.[Day]
	AND #tmpFractionFuelQuality.[Hour] = sub1.[Hour];

    --If we couldn't find a match then set the dominant basin
	UPDATE #tmpFractionFuelQuality SET BasinID = sub.Basin FROM
	(SELECT AssetID,[Year],[Month],[Day],[Hour], MAX( CASE SEQ WHEN 1 THEN FuelBasinID ELSE NULL END ) Basin           
     FROM #tmpFractionFuelQuality
	 WHERE BlendNum > 1 AND BasinID is null
     GROUP BY AssetID,[Year],[Month],[Day],[Hour]) as sub
	WHERE #tmpFractionFuelQuality.AssetID = sub.AssetID
	AND #tmpFractionFuelQuality.[Year] = sub.[Year]
	AND #tmpFractionFuelQuality.[Month] = sub.[Month]
	AND #tmpFractionFuelQuality.[Day] = sub.[Day]
	AND #tmpFractionFuelQuality.[Hour] = sub.[Hour];
	
	--Set the basin for all of the non blended fuels
	UPDATE #tmpFractionFuelQuality SET BasinID = FuelBasinID WHERE BlendNum = 1;

	INSERT INTO Actuals.tAssetBlendedFuelQuality (AssetID,[Year],[Month],[Day],[Hour],FuelBasinID,
		AssetBlendedFuelQualityDesc,Quantity_ton,ProximateBasis,HHV_btulbm,Moisture_percent,
		InherentMoisture_percent,Ash_percent,Sulfur_percent,VolatileMatter_percent,
		FixedCarbon_percent,UltimateBasis,Carbon_percent,Hydrogen_percent,Nitrogen_percent,
		Chlorine_percent,Oxygen_percent,SIO2_percent,AL2O3_percent,TIO2_percent,Fe2O3_percent,
		CAO_percent,MGO_percent,K2O_percent,NA2O_percent,SO3_percent,P2O5_percent,
		SRO_percent,BAO_percent,MN3O4_percent,AshUndetermined_percent,AshFusionReducingInitialDeformation_f,
		AshFusionReducingSoftening_f,AshFusionReducingHemispherical_f,AshFusionReducingFluid_f,
		AshFusionOxidizingInitialDeformation_f,AshFusionOxidizingSoftening_f,AshFusionOxidizingHemispherical_f,
		AshFusionOxidizingFluid_f,T250_f,HGI,BaseToAcidRatio_fraction,EquilibriumMoisture_percent,
		SulfurFormPyritic_percent,SulfurFormSulfate_percent,SulfurFormOrganic_percent,Mercury_ppm,
		SlaggingIndex,FoulingIndex,ErosionIndex)
	SELECT AssetID,[Year],[Month],[Day],[Hour],MAX(BasinID),MAX(BlendDescription),SUM(Quantity_ton),1 
		,SUM(CASE WHEN HHVMass_fraction > 0 THEN HHV_btulbm*(ParentMass_fraction/HHVMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN MoistureMass_fraction > 0 THEN Moisture_percent*(ParentMass_fraction/MoistureMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN InherentMoistureMass_fraction > 0 THEN InherentMoisture_percent*(ParentMass_fraction/InherentMoistureMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN AshMass_fraction > 0 THEN Ash_percent*(ParentMass_fraction/AshMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SulfurMass_fraction > 0 THEN Sulfur_percent*(ParentMass_fraction/SulfurMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN VolatileMatterMass_fraction > 0 THEN VolatileMatter_percent*(ParentMass_fraction/VolatileMatterMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN FixedCarbonMass_fraction > 0 THEN FixedCarbon_percent*(ParentMass_fraction/FixedCarbonMass_fraction) ELSE 0 END)
		,1
		,SUM(CASE WHEN CarbonMass_fraction > 0 THEN Carbon_percent*(ParentMass_fraction/CarbonMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN HydrogenMass_fraction > 0 THEN Hydrogen_percent*(ParentMass_fraction/HydrogenMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN NitrogenMass_fraction > 0 THEN Nitrogen_percent*(ParentMass_fraction/NitrogenMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN ChlorineMass_fraction > 0 THEN Chlorine_percent*(ParentMass_fraction/ChlorineMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN OxygenMass_fraction > 0 THEN Oxygen_percent*(ParentMass_fraction/OxygenMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SiO2Mass_fraction > 0 THEN SiO2_percent*(AshParentMass_fraction/SiO2Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN Al2O3Mass_fraction > 0 THEN Al2O3_percent*(AshParentMass_fraction/Al2O3Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN TiO2Mass_fraction > 0 THEN TiO2_percent*(AshParentMass_fraction/TiO2Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN Fe2O3Mass_fraction > 0 THEN Fe2O3_percent*(AshParentMass_fraction/Fe2O3Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN CaOMass_fraction > 0 THEN CaO_percent*(AshParentMass_fraction/CaOMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN MgOMass_fraction > 0 THEN MgO_percent*(AshParentMass_fraction/MgOMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN K2OMass_fraction > 0 THEN K2O_percent*(AshParentMass_fraction/K2OMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN Na2OMass_fraction > 0 THEN Na2O_percent*(AshParentMass_fraction/Na2OMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SO3Mass_fraction > 0 THEN SO3_percent*(AshParentMass_fraction/SO3Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN P2O5Mass_fraction > 0 THEN P2O5_percent*(AshParentMass_fraction/P2O5Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SrOMass_fraction > 0 THEN SrO_percent*(AshParentMass_fraction/SrOMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN BaOMass_fraction > 0 THEN BaO_percent*(AshParentMass_fraction/BaOMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN Mn3O4Mass_fraction > 0 THEN Mn3O4_percent*(AshParentMass_fraction/Mn3O4Mass_fraction) ELSE 0 END)
		,SUM(CASE WHEN AshUndeterminedMass_fraction > 0 THEN AshUndetermined_percent*(AshParentMass_fraction/AshUndeterminedMass_fraction) ELSE 0 END)
		,MIN(AshFusionReducingInitialDeformation_f)
		,MIN(AshFusionReducingSoftening_f)
		,MIN(AshFusionReducingHemispherical_f)
		,MIN(AshFusionReducingFluid_f)
		,MIN(AshFusionOxidizingInitialDeformation_f)
		,MIN(AshFusionOxidizingSoftening_f)
		,MIN(AshFusionOxidizingHemispherical_f)
		,MIN(AshFusionOxidizingFluid_f)
		,MAX(T250_f)
		,MIN(HGI)
		,SUM(CASE WHEN BaseToAcidRatioMass_fraction > 0 THEN BaseToAcidRatio_fraction*(ParentMass_fraction/BaseToAcidRatioMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN EquilibriumMoistureMass_fraction > 0 THEN EquilibriumMoisture_percent*(ParentMass_fraction/EquilibriumMoistureMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SulfurPyriticMass_fraction > 0 THEN SulfurFormPyritic_percent*(ParentMass_fraction/SulfurPyriticMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SulfurSulfateMass_fraction > 0 THEN SulfurFormSulfate_percent*(ParentMass_fraction/SulfurSulfateMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN SulfurOrganicMass_fraction > 0 THEN SulfurFormOrganic_percent*(ParentMass_fraction/SulfurOrganicMass_fraction) ELSE 0 END)
		,SUM(CASE WHEN MercuryMass_fraction > 0 THEN Mercury_ppm*(ParentMass_fraction/MercuryMass_fraction) ELSE 0 END)
		,0,0,0
		FROM #tmpFractionFuelQuality
		GROUP BY AssetID,[Year],[Month],[Day],[Hour];

	--Insert the fuel quality status values
	INSERT INTO [Actuals].[tAssetBlendedFuelQualityStatus] ([AssetID],[Year],[Month],
				[Day],[Hour],[HHVStatus],[MoistureStatus],[AshStatus],[SulfurStatus],
				[SiO2Status],[Al2O3Status],[TiO2Status],[Fe2O3Status],[CaOStatus],
				[MgOStatus],[K2OStatus],[Na2OStatus]) 
	SELECT AssetID,[Year],[Month],[Day],[Hour],MAX([HHVStatus]),MAX([MoistureStatus]),
		  MAX([AshStatus]),MAX([SulfurStatus]),MAX([SiO2Status]),MAX([Al2O3Status]),MAX([TiO2Status]), 
		  MAX([Fe2O3Status]),MAX([CaOStatus]),MAX([MgOStatus]),MAX([K2OStatus]),MAX([Na2OStatus]) 
	FROM #tmpAssetTransactionDetails a INNER JOIN Fuel.tSolidFuelQualityStatus b ON a.FuelQualityID = b.FuelQualityID
	GROUP BY AssetID,[Year],[Month],[Day],[Hour]

	--Drop the temp tables
	DROP TABLE #tmpAssetTransactionDetails;
	DROP TABLE #tmpFractionFuelQuality;	

	--Calculate the slagging, fouling and erosion indexes
	UPDATE Actuals.tAssetBlendedFuelQuality SET SlaggingIndex = 
		[FuelTracker].[ufnGetSlaggingPotential] (Ash_percent,
												NA2O_percent,
												CAO_percent,
												MGO_percent,
												Fe2O3_percent,	
												K2O_percent,
												SIO2_percent,
												AL2O3_percent,
												TIO2_percent,
												BaseToAcidRatio_fraction,
												AshFusionReducingHemispherical_f,
												AshFusionReducingInitialDeformation_f,
												Sulfur_percent,
												Moisture_percent)
	WHERE Base.DateSerial([Year],[Month],[Day],[Hour],0,0) BETWEEN @StartDate AND @EndDate

	UPDATE Actuals.tAssetBlendedFuelQuality SET FoulingIndex = 
		[FuelTracker].[ufnGetFoulingPotential] (Ash_percent,
												NA2O_percent,
												CAO_percent,
												MGO_percent,
												Fe2O3_percent,	
												K2O_percent,
												SIO2_percent,
												AL2O3_percent,
												TIO2_percent)
	WHERE Base.DateSerial([Year],[Month],[Day],[Hour],0,0) BETWEEN @StartDate AND @EndDate

	UPDATE Actuals.tAssetBlendedFuelQuality SET ErosionIndex = 
		[FuelTracker].[ufnGetErosionPotential] (HHV_btulbm,
												Ash_percent,
												SIO2_percent,
												AL2O3_percent)
	WHERE Base.DateSerial([Year],[Month],[Day],[Hour],0,0) BETWEEN @StartDate AND @EndDate
END
