﻿CREATE PROCEDURE [FuelTracker].[spGenerateTransactionSummary]
	@IsWithdraw bit,
	@StartTimestamp datetime 	
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #tmpTransaction
	(
		ID int not null,
		FTTransactionID int not null,
	    FTNodeID int not null, 
		CurrentTimestamp datetime not null, 
		Quantity_ton real not null, 
		TimeSinceLast real null,
		Step int null,
		PRIMARY KEY CLUSTERED (FTNodeID,CurrentTimestamp)
	)
	
	IF @IsWithdraw = 1
	BEGIN
		--Setup the base data with a row number so we can tell which row comes after another
		INSERT INTO #tmpTransaction (ID,FTTransactionID,FTNodeID,CurrentTimestamp,Quantity_ton,TimeSinceLast,Step)
		SELECT ROW_NUMBER() OVER (ORDER BY FTNodeID, CurrentTimestamp), 
		FTTransactionID, FTNodeID, CurrentTimestamp, Quantity_ton, 0.0,	0 
		FROM FuelTracker.tTransaction
		WHERE Quantity_ton < 0 AND CurrentTimestamp >= @StartTimestamp
	END
	ELSE
	BEGIN
		--Setup the base data with a row number so we can tell which row comes after another
		INSERT INTO #tmpTransaction (ID,FTTransactionID,FTNodeID,CurrentTimestamp,Quantity_ton,TimeSinceLast,Step)
		SELECT ROW_NUMBER() OVER (ORDER BY FTNodeID, CurrentTimestamp), 
		FTTransactionID, FTNodeID, CurrentTimestamp, Quantity_ton, 0.0,	0 
		FROM FuelTracker.tTransaction
		WHERE Quantity_ton > 0 AND CurrentTimestamp >= @StartTimestamp
	END

	--Calcualte the time between movements
	UPDATE #tmpTransaction SET TimeSincelast = sub.SinceLast
	FROM 
	(SELECT nRow.ID, DateDiff(mi,fRow.CurrentTimestamp,nRow.CurrentTimestamp) SinceLast
	FROM #tmpTransaction fRow, #tmpTransaction nRow
	WHERE fRow.FTNodeID = nRow.FTNodeID AND fRow.ID = (nRow.ID-1)) as Sub
	WHERE #tmpTransaction.ID = Sub.ID

	--Anything less than a two hour gap should be part of the same movement summary
	UPDATE #tmpTransaction SET TimeSinceLast = 1 WHERE TimeSincelast <= 120;

	--Any gap larger than a two hour should be moved to a new summary
	UPDATE #tmpTransaction SET TimeSinceLast = FTTransactionID WHERE TimeSincelast > 120;

	--Set the last record for a path so it get's included as the end of the summary
	UPDATE #tmpTransaction SET TimeSinceLast = FTTransactionID WHERE ID = 
	(SELECT a.ID FROM #tmpTransaction a WHERE a.FTNodeID = #tmpTransaction.FTNodeID AND a.CurrentTimestamp = 
	(SELECT MAX(b.CurrentTimestamp) FROM #tmpTransaction b WHERE b.FTNodeID = #tmpTransaction.FTNodeID));

	--Set the first record for a path so it get's included in the summary
	UPDATE #tmpTransaction SET TimeSinceLast = FTTransactionID WHERE ID = 
	(SELECT a.ID FROM #tmpTransaction a WHERE a.FTNodeID = #tmpTransaction.FTNodeID AND a.CurrentTimestamp = 
	(SELECT MIN(b.CurrentTimestamp) FROM #tmpTransaction b WHERE b.FTNodeID = #tmpTransaction.FTNodeID));

	--Set the midnight record for all non-accumulating end node paths 
	UPDATE #tmpTransaction SET TimeSinceLast = FTTransactionID WHERE FTNodeID in 
	(SELECT FTNodeID FROM FuelTracker.tNode a WHERE a.FTNodeID = #tmpTransaction.FTNodeID 
	AND a.IsAccumulating = 0 AND a.IsUnloadingNode = 0)
	AND DatePart(hh,CurrentTimestamp) = 0 AND DatePart(mi,CurrentTimestamp) = 0; 

	--Set the midnight record for all outgoing transactions from silo/bunker nodes
	UPDATE #tmpTransaction SET TimeSinceLast = FTTransactionID WHERE FTNodeID in 
	(SELECT FTNodeID FROM FuelTracker.tNode a WHERE a.FTNodeID = #tmpTransaction.FTNodeID AND FTNodeTypeID = 7 )
	AND DatePart(hh,CurrentTimestamp) = 0 AND DatePart(mi,CurrentTimestamp) = 0
	AND Quantity_ton < 0; 	

	--Set the places where there is a change in which summary we're using
	UPDATE #tmpTransaction SET step = sub.step
	FROM
	(SELECT ROW_NUMBER() OVER (ORDER BY FTNodeID, CurrentTimestamp) step, ID 
	FROM #tmpTransaction 
	WHERE FTTransactionID = TimeSinceLast) as sub
	WHERE #tmpTransaction.ID = sub.ID;

	--Set the final summary data
	UPDATE #tmpTransaction SET TimeSinceLast = sub.TimeSinceLast
	FROM
	(SELECT cur.ID, fStep.TimeSinceLast
	FROM #tmpTransaction fStep INNER JOIN #tmpTransaction nStep ON fStep.FTNodeID = nStep.FTNodeID 
	INNER JOIN #tmpTransaction cur ON cur.FTNodeID = fStep.FTNodeID	
	WHERE cur.ID > fStep.ID
	AND cur.ID < nStep.ID
	AND (fStep.step = (nStep.step-1))) as sub
	WHERE #tmpTransaction.ID = sub.ID;

	--Set the last record for a paths data
	UPDATE #tmpTransaction SET TimeSinceLast = sub.TimeSinceLast
	FROM (SELECT nStep.ID, fStep.TimeSinceLast
	FROM #tmpTransaction fStep INNER JOIN #tmpTransaction nStep ON fStep.FTNodeID = nStep.FTNodeID 
	WHERE nStep.ID = (SELECT a.ID FROM #tmpTransaction a WHERE a.FTNodeID = nStep.FTNodeID AND a.CurrentTimestamp = 
	(SELECT MAX(b.CurrentTimestamp) FROM #tmpTransaction b WHERE b.FTNodeID = nStep.FTNodeID))
	AND fStep.ID = (SELECT a.ID-1 FROM #tmpTransaction a WHERE a.FTNodeID = fStep.FTNodeID AND a.CurrentTimestamp = 
	(SELECT MAX(b.CurrentTimestamp) FROM #tmpTransaction b WHERE b.FTNodeID = fStep.FTNodeID))) as sub
	WHERE #tmpTransaction.ID = sub.ID;

	--Insert the transaction summaries (add an hour so the duration is correct without using minutes)
	INSERT INTO FuelTracker.tTransactionSummary (FTTransactionSummaryKey, StartTime, StopTime)
	SELECT TimeSinceLast, MIN(CurrentTimeStamp), DATEADD(hh,1,MAX(CurrentTimeStamp)) 
	FROM #tmpTransaction
	GROUP BY TimeSinceLast;

	--Update the transactions with their summary ID's
	UPDATE FuelTracker.tTransaction SET FTTransactionSummaryID = sub.FTTransactionSummaryID
	FROM
	(SELECT FTTransactionSummaryID, FTTransactionID 
	 FROM FuelTracker.tTransactionSummary a INNER JOIN #tmpTransaction b ON a.FTTransactionSummaryKey = b.TimeSinceLast) as sub
	WHERE FuelTracker.tTransaction.FTTransactionID = sub.FTTransactionID

	--Reset the summary key
	UPDATE FuelTracker.tTransactionSummary SET FTTransactionSummarykey = NULL;

	--Remove the transaction summary records which are no longer used
	DELETE FROM FuelTracker.tTransactionSummary 
	FROM FuelTracker.tTransactionSummary a LEFT OUTER JOIN FuelTracker.tTransaction b ON a.FTTransactionSummaryID = b.FTTransactionSummaryID
	WHERE b.FTTransactionSummaryID is null;

	--Clean Up
	DROP TABLE #tmpTransaction;
END
