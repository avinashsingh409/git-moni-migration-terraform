﻿CREATE PROCEDURE [FuelTracker].[spGetNodeContent]
	@UIFTSetID int,
	@CurrentTimestamp datetime = NULL	
AS
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #tmpTimeNode
	(
		MaxTimestamp datetime not null,
		FTNodeID int not null,
		PRIMARY KEY CLUSTERED (MaxTimestamp,FTNodeID)
	)

	IF @CurrentTimestamp is not null
	BEGIN
		INSERT INTO #tmpTimeNode (MaxTimestamp, FTNodeID)
		SELECT MAX(CurrentTimestamp) MaxTimestamp, FTNodeID
		FROM tFTNodeContent 
		WHERE CurrentTimestamp <= @CurrentTimestamp
		GROUP BY FTNodeID
	END
	ELSE
	BEGIN
		INSERT INTO #tmpTimeNode (MaxTimestamp, FTNodeID)
		SELECT MAX(CurrentTimestamp) MaxTimestamp, FTNodeID
		FROM tFTNodeContent 
		GROUP BY FTNodeID
	END	

	SELECT a.FTTransactionID, a.FTNodeID, a.CurrentTimestamp, a.Quantity_ton, a.FuelSourceID, a.FuelQualityID 
	INTO #tmpNodeContent
	FROM FuelTracker.tNodeContent a INNER JOIN #tmpTimeNode b ON a.FTNodeID = b.FTNodeID and a.CurrentTimestamp = b.MaxTimestamp

	SELECT a.ParentUIFTNavTreeID, a.UIFTNavTreeID, a.UIFTNavTreeDesc, ISNULL(f.FuelBasinID,0) AS FuelBasinID, 
	ISNULL(f.FuelBasinDesc,'Unspecified') AS FuelBasinDesc, e.FuelSourceID, e.FuelSourceDesc, a.IsExpanded, 
	SUM(c.Quantity_ton) AS SourceQuantity_tons, 
	CASE WHEN SUM(c.Quantity_ton) > 0 
		 THEN SUM(h.HHV_btulbm * c.Quantity_ton)/SUM(c.Quantity_ton) ELSE 0 END AS HHV_btulbm, 
	SUM(c.Quantity_ton / 0.0005 * h.HHV_btulbm / 1000000.0) AS SourceEnergy_MBtu, 
	CASE WHEN SUM(c.Quantity_ton) > 0 
		 THEN SUM(h.Moisture_percent * c.Quantity_ton)/SUM(c.Quantity_ton) ELSE 0 END AS Moisture_percent, 
	CASE WHEN SUM(c.Quantity_ton) > 0 
		 THEN SUM(h.Ash_percent * c.Quantity_ton)/SUM(c.Quantity_ton) ELSE 0 END AS Ash_percent, 
	CASE WHEN SUM(c.Quantity_ton) > 0 
		 THEN SUM(h.Sulfur_percent * c.Quantity_ton)/SUM(c.Quantity_ton) ELSE 0 END AS Sulfur_percent, 
	CASE WHEN SUM(c.Quantity_ton) > 0 
		 THEN SUM(h.Mercury_ppm * c.Quantity_ton)/SUM(c.Quantity_ton) ELSE 0 END AS Mercury_ppm 
	FROM tUIFTNavTree a INNER JOIN tUIFTNavTreeNodeMap b ON a.UIFTNavTreeID = b.UIFTNavTreeID 
	INNER JOIN #tmpNodeContent c ON b.FTNodeID = c.FTNodeID
	INNER JOIN tFuelSource e ON c.FuelSourceID = e.FuelSourceID 
	LEFT OUTER JOIN tFuelBasin f ON e.FuelBasinID = f.FuelBasinID 	
	INNER JOIN tFuelQuality h ON c.FuelQualityID = h.FuelQualityID 
	WHERE a.UIFTNavTreeSetID = @UIFTSetID 
	GROUP BY a.UIFTNavTreeSortOrder,a.ParentUIFTNavTreeID,a.UIFTNavTreeID,a.UIFTNavTreeDesc,
	a.IsExpanded,f.FuelBasinID,f.FuelBasinDesc,e.FuelSourceID, e.FuelSourceDesc 
	HAVING SUM(c.Quantity_ton) <> 0 
	ORDER BY a.UIFTNavTreeSortOrder, a.UIFTNavTreeID, SourceQuantity_tons DESC 

	DROP TABLE #tmpTimeNode
	DROP TABLE #tmpNodeContent
END
