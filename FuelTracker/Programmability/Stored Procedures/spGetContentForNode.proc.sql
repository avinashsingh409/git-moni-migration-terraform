﻿CREATE PROCEDURE [FuelTracker].[spGetContentForNode]
	@FTNodeID int,
	@CurrentTimestamp datetime = NULL	
AS
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #tmpTimeNode
	(
		MaxTimestamp datetime not null,
		FTNodeID int not null,
		PRIMARY KEY CLUSTERED (MaxTimestamp,FTNodeID)
	)

	IF @CurrentTimestamp is not null
	BEGIN
		INSERT INTO #tmpTimeNode (MaxTimestamp, FTNodeID)
		SELECT MAX(CurrentTimestamp) MaxTimestamp, FTNodeID
		FROM tFTNodeContent 
		WHERE CurrentTimestamp <= @CurrentTimestamp AND FTNodeID = @FTNodeID
		GROUP BY FTNodeID
	END
	ELSE
	BEGIN
		INSERT INTO #tmpTimeNode (MaxTimestamp, FTNodeID)
		SELECT MAX(CurrentTimestamp) MaxTimestamp, FTNodeID
		FROM tFTNodeContent 
		WHERE FTNodeID = @FTNodeID
		GROUP BY FTNodeID
	END	

	SELECT a.FTTransactionID, a.FTNodeID, a.CurrentTimestamp, a.Quantity_ton, a.FuelSourceID 
	INTO #tmpNodeContent
	FROM FuelTracker.tNodeContent a INNER JOIN #tmpTimeNode b ON a.FTNodeID = b.FTNodeID and a.CurrentTimestamp = b.MaxTimestamp
	WHERE a.FTNodeID = @FTNodeID

	SELECT c.FTNodeID, g.FTNodeDesc, g.FTNodeAbbrev, ISNULL(f.FuelBasinID,0) AS FuelBasinID, 
	ISNULL(f.FuelBasinDesc,'Unspecified') AS FuelBasinDesc, e.FuelSourceID, e.FuelSourceDesc, 
	SUM(c.Quantity_ton) AS SourceQuantity_tons
	FROM #tmpNodeContent c INNER JOIN tFuelSource e ON c.FuelSourceID = e.FuelSourceID 
	LEFT OUTER JOIN tFuelBasin f ON e.FuelBasinID = f.FuelBasinID 
	INNER JOIN FuelTracker.tNode g ON c.FTNodeID = g.FTNodeID
	WHERE c.FTNodeID = @FTNodeID 
	GROUP BY c.FTNodeID,g.FTNodeDesc,g.FTNodeAbbrev,f.FuelBasinID,f.FuelBasinDesc,e.FuelSourceID, e.FuelSourceDesc 
	HAVING SUM(c.Quantity_ton) <> 0 
	ORDER BY SourceQuantity_tons DESC 

	DROP TABLE #tmpTimeNode
	DROP TABLE #tmpNodeContent
END
