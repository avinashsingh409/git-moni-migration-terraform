﻿--CREATE PROCEDURE [FuelTracker].[spProcessRightAngleData]
--AS
--BEGIN
--	SET NOCOUNT ON

--	--Only process the last three months worth of data
--	DELETE FROM #tmpRightAngleMovement WHERE ActualStartDate < DATEADD(m,-3,getdate());
--	--Don't process plant consumption records
--	DELETE FROM #tmpRightAngleMovement WHERE ProductID = 34;
--	--Don't process invalid records.  OrderID, ActualStartDate and MovementID are required because
--	--they uniquely identify a right angle record.
--	DELETE FROM #tmpRightAngleMovement WHERE OrderID is null OR ActualStartDate is null OR MovementID is null;

--	--Get all of the mine to pier or plant stockpile records
--	INSERT INTO FuelTracker.tInTransit (FTNodePathID, StartDate, EndDate, Quantity_ton, RightAngleRowKey)
--	SELECT d.FTNodePathID,a.ActualStartDate,
--	CASE WHEN a.MovementTypeID = 'C' THEN DATEADD(d,5,a.ActualStartDate) ELSE DATEADD(d,3,a.ActualStartDate) END EndDate,
--	a.MovementQuantity_ton, CONVERT(varchar,a.OrderID)+CONVERT(varchar,a.ActualStartDate)+CONVERT(varchar,a.MovementID) 
--	FROM #tmpRightAngleMovement a INNER JOIN FuelTracker.tRightAngleNodeMap b ON a.DestinationID = b.LocaleID
--	INNER JOIN FuelTracker.tRightAngleNodeMap c ON a.OriginID = c.LocaleID
--	INNER JOIN FuelTracker.tNodePath d ON b.FTNodeID = d.ToFTNodeID AND c.FTNodeID = d.FromFTNodeID
--	WHERE a.OriginTypeID = 106
--	AND (CONVERT(varchar,a.OrderID)+CONVERT(varchar,a.ActualStartDate)+CONVERT(varchar,a.MovementID)) not in (SELECT RightAngleRowKey FROM FuelTracker.tInTransit);

--	--Get all of the pier to plant stockpile records
--	INSERT INTO FuelTracker.tInTransit (FTNodePathID, StartDate, EndDate, Quantity_ton, RightAngleRowKey)
--	SELECT d.FTNodePathID,a.ActualStartDate,
--	CASE WHEN a.MovementTypeID = 'C' THEN DATEADD(d,5,a.ActualStartDate) ELSE DATEADD(d,3,a.ActualStartDate) END EndDate,
--	a.MovementQuantity_ton, CONVERT(varchar,OrderID)+CONVERT(varchar,ActualStartDate)+CONVERT(varchar,MovementID) AS UK
--	FROM #tmpRightAngleMovement a INNER JOIN FuelTracker.tRightAngleNodeMap b ON a.DestinationID = b.LocaleID
--	INNER JOIN FuelTracker.tRightAngleNodeMap c ON a.OriginID = c.LocaleID
--	INNER JOIN FuelTracker.tNodePath d ON b.FTNodeID = d.ToFTNodeID AND c.FTNodeID = d.FromFTNodeID
--	WHERE a.OriginTypeID = 108
--	AND (CONVERT(varchar,OrderID)+CONVERT(varchar,ActualStartDate)+CONVERT(varchar,MovementID)) not in (SELECT RightAngleRowKey FROM FuelTracker.tInTransit);

--	--Get all of the sources set for the movements
--	INSERT INTO FuelTracker.tInTransitSource (FTInTransitID, FuelSourceID, Quantity_percent)
--	SELECT b.FTInTransitID, d.FuelSourceID, 100.0
--	FROM #tmpRightAngleMovement a INNER JOIN FuelTracker.tInTransit b ON b.RightAngleRowKey = (CONVERT(varchar,a.OrderID)+CONVERT(varchar,a.ActualStartDate)+CONVERT(varchar,a.MovementID))
--	INNER JOIN FuelTracker.tNodePath c ON b.FTNodePathID = c.FTNodePathID
--	INNER JOIN FuelTracker.tNodeContentSourceDefault d ON c.FromFTNodeID = d.FTNodeID 	
--	AND d.CurrentTimeStamp = (SELECT MAX(CurrentTimeStamp) FROM tFTNodeContentSourceDefault subc WHERE c.FromFTNodeID = subc.FTNodeID) 
--	LEFT OUTER JOIN FuelTracker.tInTransitSource e ON b.FTInTransitID = e.FTInTransitID
--	WHERE a.OriginTypeID = 106 AND e.FTInTransitID is NULL;
	
--	--Get all of the lab qualities set for the movements
--	DECLARE @FTInTransitID int;
--	DECLARE @MovementStartDate nvarchar(20);
--	DECLARE @LabNumber nvarchar(20);
--	DECLARE @FuelQualityID int;
--	DECLARE @HHV real;
--	DECLARE @Moisture real;
--	DECLARE @Ash real;
--	DECLARE @Sulfur real;
--	DECLARE @FixedCarbon real;
--	DECLARE @VolatileMatter real;
--	DECLARE @HGI real;
--	DECLARE curFuelQuality INSENSITIVE CURSOR
--	FOR SELECT c.FTInTransitID, CONVERT(nvarchar(20),c.StartDate,101), CONVERT(nvarchar(20),a.LabAnalysisID),
--	MAX(CASE WHEN PropertyDesc = 'ActualBTU' THEN PropertyValue ELSE 0 END) as HHV,
--	MAX(CASE WHEN PropertyDesc = 'ActualMoisturePercentage' THEN PropertyValue ELSE 0 END) as Moisture,
--	MAX(CASE WHEN PropertyDesc = 'ActualAshPercentage' THEN PropertyValue ELSE 0 END) as Ash,
--	MAX(CASE WHEN PropertyDesc = 'ActualSulfurPercentage' THEN PropertyValue ELSE 0 END) as Sulfur,
--	MAX(CASE WHEN PropertyDesc = 'FixedCarbonPercentage' THEN PropertyValue ELSE 0 END) as FixedCarbon,
--	MAX(CASE WHEN PropertyDesc = 'ActualVolatileMatterPercent' THEN PropertyValue ELSE 0 END) as VolatileMatter,
--	MAX(CASE WHEN PropertyDesc = 'HGIActual' THEN PropertyValue ELSE 0 END) as HGI
--	FROM #tmpRightAngleMovement a INNER JOIN #tmpRightAngleLabAnalysis b ON a.LabAnalysisID = b.LabAnalysisID
--	INNER JOIN FuelTracker.tInTransit c ON c.RightAngleRowKey = (CONVERT(varchar,a.OrderID)+CONVERT(varchar,a.ActualStartDate)+CONVERT(varchar,a.MovementID))
--	LEFT OUTER JOIN FuelTracker.tInTransitQuality d ON c.FTInTransitID = d.FTInTransitID
--	WHERE d.FTInTransitID is NULL
--	GROUP BY c.FTInTransitID,c.StartDate,a.LabAnalysisID
--	HAVING MAX(CASE WHEN PropertyDesc = 'ActualBTU' THEN PropertyValue ELSE 0 END) > 0
--	ORDER BY c.FTInTransitID;

--	OPEN curFuelQuality;
--	FETCH NEXT FROM curFuelQuality INTO @FTInTransitID,@MovementStartDate,@LabNumber,@HHV,@Moisture,@Ash,@Sulfur,@FixedCarbon,@VolatileMatter,@HGI;
--	WHILE @@FETCH_STATUS =  0
--	BEGIN
--		INSERT INTO Fuel.tFuelQuality (FuelQualityTypeID, FuelQualityDesc) VALUES (3,'RA ' + @MovementStartDate)
--		SELECT @FuelQualityID = SCOPE_IDENTITY();
--		INSERT INTO Fuel.tSolidFuelQuality (FuelQualityID, SampleIdentification, ProximateBasis,
--			HHV_btulbm, Moisture_percent, Ash_percent, Sulfur_percent, FixedCarbon_percent, 
--			VolatileMatter_percent, UltimateBasis, HGI, CreatedBy, ChangedBy) VALUES
--			(@FuelQualityID,@LabNumber,1,@HHV,@Moisture,@Ash,@Sulfur,@FixedCarbon,@VolatileMatter,1,@HGI,'FT','FT');		
--		INSERT INTO FuelTracker.tInTransitQuality (FTInTransitID,FuelQualityID) VALUES (@FTInTransitID,@FuelQualityID);
--		FETCH NEXT FROM curFuelQuality INTO @FTInTransitID,@MovementStartDate,@LabNumber,@HHV,@Moisture,@Ash,@Sulfur,@FixedCarbon,@VolatileMatter,@HGI;
--	END
--	CLOSE curFuelQuality;
--	DEALLOCATE curFuelQuality;

--	--Add all unmapped locale's to the unmapped table
--	--Unmapped origins
--	INSERT INTO FuelTracker.tRightAngleUnmappedLocale (LocaleID, LocaleDesc, PlantAssociationDesc, FirstMovementDate, IsMine)
--	SELECT OriginID, OriginDesc, PlantDesc, MIN(ActualStartDate), 0
--	FROM #tmpRightAngleMovement a LEFT OUTER JOIN FuelTracker.tRightAngleNodeMap b ON a.OriginID = b.LocaleID
--	LEFT OUTER JOIN FuelTracker.tRightAngleUnmappedLocale c ON a.OriginID = c.LocaleID AND a.OriginDesc = c.LocaleDesc AND a.PlantDesc = c.PlantAssociationDesc 
--	WHERE b.LocaleID is null AND c.LocaleID is null AND a.OriginTypeID <> 106
--	GROUP BY OriginID, OriginDesc, PlantDesc
--	--Unmapped destinations
--	INSERT INTO FuelTracker.tRightAngleUnmappedLocale (LocaleID, LocaleDesc, PlantAssociationDesc, FirstMovementDate, IsMine)
--	SELECT DestinationID, DestinationDesc, PlantDesc, MIN(ActualStartDate), 0
--	FROM #tmpRightAngleMovement a LEFT OUTER JOIN FuelTracker.tRightAngleNodeMap b ON a.DestinationID = b.LocaleID
--	LEFT OUTER JOIN FuelTracker.tRightAngleUnmappedLocale c ON a.DestinationID = c.LocaleID AND a.DestinationDesc = c.LocaleDesc AND a.PlantDesc = c.PlantAssociationDesc 
--	WHERE b.LocaleID is null AND c.LocaleID is null AND a.OriginTypeID <> 106
--	GROUP BY DestinationID, DestinationDesc, PlantDesc
--	--Unmapped mines
--	INSERT INTO FuelTracker.tRightAngleUnmappedLocale (LocaleID, LocaleDesc, PlantAssociationDesc, FirstMovementDate, IsMine)
--	SELECT OriginID, OriginDesc, PlantDesc, MIN(ActualStartDate), 1
--	FROM #tmpRightAngleMovement a LEFT OUTER JOIN FuelTracker.tRightAngleNodeMap b ON a.OriginID = b.LocaleID
--	LEFT OUTER JOIN FuelTracker.tRightAngleUnmappedLocale c ON a.OriginID = c.LocaleID AND a.OriginDesc = c.LocaleDesc AND a.PlantDesc = c.PlantAssociationDesc 
--	WHERE b.LocaleID is null AND c.LocaleID is null AND a.OriginTypeID = 106
--	GROUP BY OriginID, OriginDesc, PlantDesc

--	--Add all of the unknown origin source records to the unknown origin table
--	INSERT INTO FuelTracker.tRightAngleUnknownOriginSource (OriginID, OriginDesc, DestinationID, DestinationDesc,
--	MovementID, OrderID, ActualStartDate, Quantity_ton)
--	SELECT a.OriginID, a.OriginDesc, a.DestinationID, a.DestinationDesc, a.MovementID, a.OrderID,
--	a.ActualStartDate, a.MovementQuantity_ton  
--	FROM #tmpRightAngleMovement a LEFT OUTER JOIN FuelTracker.tRightAngleUnknownOriginSource b ON 
--		a.OriginID = b.OriginID AND a.DestinationID = b.DestinationID AND a.MovementID = b.MovementID AND
--		a.OrderID = b.OrderID AND a.ActualStartDate = b.ActualStartDate AND a.MovementQuantity_ton = b.Quantity_ton
--	WHERE a.OriginTypeID not in (106,108) AND b.OriginID is null
--END
