﻿CREATE PROCEDURE [FuelTracker].[spGenerateHourlyActualFuelQuality] 
	@StartDate DateTime,
	@EndDate DateTime
AS
BEGIN
	SET NOCOUNT ON;

	--Round the start date up to the next hour
	--Round the end date down to the last full hour	
	SELECT DATEADD(mi,60-DATEPART(mi,@StartDate),@StartDate)
	SELECT DATEADD(mi,DATEPART(mi,@EndDate)*-1,@EndDate)

	--Clear any data we should be replacing
	DELETE FROM Actuals.tFuelQuality 
	WHERE Base.DateSerial([Year],[Month],[Day],[Hour],0,0) BETWEEN @StartDate AND @EndDate;

	--Get the fuels burned at each unit from the fuel tracker transactions
	SELECT b.UnitID, CASE WHEN d.IsUsedForNodeContent = 0 OR d.IsUsedForNodeContent is null THEN e.FuelQualityID ELSE d.FuelQualityID END as FuelQualityID, 
	c.FuelSourceID, c.Quantity_percent, YEAR(a.CurrentTimestamp) [Year], MONTH(a.CurrentTimestamp) [Month], 
	DAY(a.CurrentTimestamp) [Day], DATEPART(hh,a.CurrentTimestamp) [Hour], a.CurrentTimestamp
	INTO #tmpBurnedTransactionDetails
	FROM FuelTracker.tTransaction a INNER JOIN FuelTracker.tNode b ON a.FTNodeID = b.FTNodeID
	INNER JOIN FuelTracker.tTransactionSource c ON a.FTTransactionID = c.FTTransactionID
	LEFT OUTER JOIN FuelTracker.tTransactionQuality d ON a.FTTransactionID = d.FTTransactionID AND c.FTTransactionSourceID = d.FTTransactionSourceID 
	LEFT OUTER JOIN FuelTracker.tTransactionQuality e ON a.FTTransactionID = e.FTTransactionID AND e.FTTransactionSourceID is null 
	WHERE b.FTNodeTypeID = 9 AND a.Quantity_ton > 0
	AND c.IsUsedForNodeContent = 1 
	AND (d.IsUsedForNodeContent = 1 OR e.IsUsedForNodeContent = 1)
	AND a.CurrentTimestamp BETWEEN @StartDate AND @EndDate

	INSERT INTO Actuals.tFuelQuality (UnitID,FuelQualityID,FuelSourceID,
		FuelSourceQuantity_percent,[Year],[Month],[Day],[Hour])
	SELECT DISTINCT UnitID, FuelQualityID, FuelSourceID, Quantity_percent, [Year], [Month], [Day], [Hour]
	FROM #tmpBurnedTransactionDetails	

	--Setup the Fuel Quality
	UPDATE Actuals.tFuelQuality SET
		 ProximateBasis	= b.ProximateBasis
		,HHV_btulbm	= b.HHV_btulbm
		,Moisture_percent = b.Moisture_percent
		,InherentMoisture_percent =	b.InherentMoisture_percent
		,Ash_percent = b.Ash_percent
		,Sulfur_percent = b.Sulfur_percent
		,VolatileMatter_percent = b.VolatileMatter_percent
		,FixedCarbon_percent = b.FixedCarbon_percent
		,UltimateBasis = b.UltimateBasis
		,Carbon_percent = b.Carbon_percent
		,Hydrogen_percent = b.Hydrogen_percent
		,Nitrogen_percent =	b.Nitrogen_percent
		,Chlorine_percent =	b.Chlorine_percent
		,Oxygen_percent = b.Oxygen_percent
		,SIO2_percent = b.SIO2_percent
		,AL2O3_percent = b.AL2O3_percent
		,TIO2_percent =	b.TIO2_percent
		,Fe2O3_percent = b.Fe2O3_percent
		,CAO_percent = b.CAO_percent
		,MGO_percent = b.MGO_percent
		,K2O_percent = b.K2O_percent
		,NA2O_percent = b.NA2O_percent
		,SO3_percent = b.SO3_percent
		,P2O5_percent = b.P2O5_percent
		,SRO_percent = b.SRO_percent
		,BAO_percent = b.BAO_percent
		,MN3O4_percent = b.MN3O4_percent
		,AshUndetermined_percent = b.AshUndetermined_percent
		,AshFusionReducingInitialDeformation_f = b.AshFusionReducingInitialDeformation_f
		,AshFusionReducingSoftening_f = b.AshFusionReducingSoftening_f
		,AshFusionReducingHemispherical_f = b.AshFusionReducingHemispherical_f
		,AshFusionReducingFluid_f =	b.AshFusionReducingFluid_f
		,AshFusionOxidizingInitialDeformation_f = b.AshFusionOxidizingInitialDeformation_f
		,AshFusionOxidizingSoftening_f = b.AshFusionOxidizingSoftening_f
		,AshFusionOxidizingHemispherical_f = b.AshFusionOxidizingHemispherical_f
		,AshFusionOxidizingFluid_f = b.AshFusionOxidizingFluid_f
		,T250_f = b.T250_f
		,HGI = b.HGI
		,BaseToAcidRatio_fraction = b.BaseToAcidRatio_fraction
		,EquilibriumMoisture_percent = b.EquilibriumMoisture_percent
		,SulfurFormPyritic_percent = b.SulfurFormPyritic_percent
		,SulfurFormSulfate_percent = b.SulfurFormSulfate_percent
		,SulfurFormOrganic_percent = b.SulfurFormOrganic_percent
		,Mercury_ppm = b.Mercury_ppm
	FROM Actuals.tFuelQuality a INNER JOIN Fuel.tSolidFuelQuality b ON a.FuelQualityID = b.FuelQualityID
	INNER JOIN #tmpBurnedTransactionDetails c ON a.[Year] = c.[Year] AND a.[Month] = c.[Month] AND a.[Day] = c.[Day] 
		AND a.[Hour] = c.[Hour] AND a.FuelQualityID = c.FuelQualityID AND a.FuelSourceID = c.FuelSourceID 
		AND a.UnitID = c.UnitID;	

	--Drop the temp tables
	DROP TABLE #tmpBurnedTransactionDetails;

	--Generate a single quality record for each performance record
	EXEC FuelTracker.spGenerateHourlyActualBlendedFuelQuality @StartDate, @EndDate;
END
