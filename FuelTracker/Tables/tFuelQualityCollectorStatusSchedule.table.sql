CREATE TABLE [FuelTracker].[tFuelQualityCollectorStatusSchedule] (
    [FTFuelQualityCollectorStatusScheduleID] INT      IDENTITY (1, 1) NOT NULL,
    [FTFuelQualityCollectorID]               INT      NOT NULL,
    [DateTimestamp]                          DATETIME NOT NULL,
    [IsOn]                                   BIT      CONSTRAINT [DF_tFTFuelQualityCollectorStatusSchedule_IsOn] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTFuelQualityCollectorStatusSchedule] PRIMARY KEY CLUSTERED ([FTFuelQualityCollectorStatusScheduleID] ASC),
    CONSTRAINT [FK_tFTFuelQualityCollectorStatusSchedule_FTFuelQualityCollectorID_tFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tFuelQualityCollectorStatusSchedule';

