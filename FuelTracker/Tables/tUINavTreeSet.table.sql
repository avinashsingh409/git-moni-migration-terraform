CREATE TABLE [FuelTracker].[tUINavTreeSet] (
    [UIFTNavTreeSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [UIFTNavTreeSetDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tUIFTNavTreeSet] PRIMARY KEY CLUSTERED ([UIFTNavTreeSetID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tUINavTreeSet';

