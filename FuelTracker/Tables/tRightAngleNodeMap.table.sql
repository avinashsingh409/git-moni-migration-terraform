CREATE TABLE [FuelTracker].[tRightAngleNodeMap] (
    [FTNodeID] INT NOT NULL,
    [LocaleID] INT NOT NULL,
    CONSTRAINT [PK_tFTRightAngleNodeMap_FTNodeID_LocaleID] PRIMARY KEY CLUSTERED ([FTNodeID] ASC, [LocaleID] ASC),
    CONSTRAINT [FK_tFTRightAngleNodeMap_FTNodeID_LocaleID] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mapping of Locale id''s from the Right Angle system to nodes in TE.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tRightAngleNodeMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tRightAngleNodeMap';

