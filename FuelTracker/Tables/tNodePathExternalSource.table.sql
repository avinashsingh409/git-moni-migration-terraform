CREATE TABLE [FuelTracker].[tNodePathExternalSource] (
    [FTNodePathExternalSourceID] INT      IDENTITY (1, 1) NOT NULL,
    [FTNodePathID]               INT      NOT NULL,
    [CurrentTimestamp]           DATETIME NOT NULL,
    [Expiration_hr]              INT      NOT NULL,
    [FuelSourceID]               INT      NOT NULL,
    [Quantity_percent]           REAL     NOT NULL,
    [IsManual]                   BIT      DEFAULT ((0)) NOT NULL,
    [IsLogic]                    BIT      DEFAULT ((0)) NOT NULL,
    [IsImport]                   BIT      CONSTRAINT [DF_tNodePathExternalSource_IsImport] DEFAULT ((0)) NOT NULL,
    [IsTrueUp]                   BIT      CONSTRAINT [DF_FuelTracker_tNodePathExternalSource_IsTrueUp] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTNodePathExternalSource] PRIMARY KEY CLUSTERED ([FTNodePathExternalSourceID] ASC),
    CONSTRAINT [FK_tFTNodePathExternalSource_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID]),
    CONSTRAINT [FK_tFTNodePathExternalSource_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodePathExternalSource';

