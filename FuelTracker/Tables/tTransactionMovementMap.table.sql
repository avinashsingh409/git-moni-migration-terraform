CREATE TABLE [FuelTracker].[tTransactionMovementMap] (
    [FTTransactionMovementMapID] INT IDENTITY (1, 1) NOT NULL,
    [FTTransactionID]            INT NOT NULL,
    [FTMovementID]               INT NOT NULL,
    CONSTRAINT [PK_tFTTransactionMovementMap] PRIMARY KEY CLUSTERED ([FTTransactionMovementMapID] ASC),
    CONSTRAINT [FK_tFTTransactionMovementMap_FTMovementID_tFTMovement] FOREIGN KEY ([FTMovementID]) REFERENCES [FuelTracker].[tMovement] ([FTMovementID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFTTransactionMovementMap_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tFTTransactionMovementMap_FTMovementID_FTTransactionID] UNIQUE NONCLUSTERED ([FTMovementID] ASC, [FTTransactionID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mapping of transactions to movements.  Maybe multiple movements compiled into a single transaction.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionMovementMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionMovementMap';

