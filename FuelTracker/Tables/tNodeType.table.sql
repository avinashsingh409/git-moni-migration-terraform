CREATE TABLE [FuelTracker].[tNodeType] (
    [FTNodeTypeID]        INT            IDENTITY (1, 1) NOT NULL,
    [FTNodeTypeDesc]      NVARCHAR (255) NULL,
    [FTNodeTypeSortOrder] REAL           NULL,
    CONSTRAINT [PK_tFTNodeType] PRIMARY KEY CLUSTERED ([FTNodeTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodeType';

