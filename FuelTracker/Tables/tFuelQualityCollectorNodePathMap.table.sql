CREATE TABLE [FuelTracker].[tFuelQualityCollectorNodePathMap] (
    [FTFuelQualityCollectorNodePathMapID] INT IDENTITY (1, 1) NOT NULL,
    [FTFuelQualityCollectorID]            INT NOT NULL,
    [FTNodePathID]                        INT NOT NULL,
    CONSTRAINT [PK_tFTFuelQualityCollectorNodePathMap] PRIMARY KEY CLUSTERED ([FTFuelQualityCollectorNodePathMapID] ASC),
    CONSTRAINT [FK_tFTFuelQualityCollectorNodePathMap_FTFuelQualityCollectorID_tFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID]),
    CONSTRAINT [FK_tFTFuelQualityCollectorNodePathMap_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tFuelQualityCollectorNodePathMap';

