CREATE TABLE [FuelTracker].[tTransactionSummary] (
    [FTTransactionSummaryID]  INT      IDENTITY (1, 1) NOT NULL,
    [StartTime]               DATETIME NULL,
    [StopTime]                DATETIME NULL,
    [FTTransactionSummaryKey] INT      NULL,
    CONSTRAINT [PK_tFTTransactionSummary] PRIMARY KEY CLUSTERED ([FTTransactionSummaryID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionSummary';

