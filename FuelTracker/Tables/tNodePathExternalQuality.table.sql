CREATE TABLE [FuelTracker].[tNodePathExternalQuality] (
    [FTNodePathExternalQualityID] INT      IDENTITY (1, 1) NOT NULL,
    [FTNodePathID]                INT      NOT NULL,
    [CurrentTimestamp]            DATETIME NOT NULL,
    [Expiration_hr]               INT      NOT NULL,
    [FuelQualityID]               INT      NOT NULL,
    [IsManual]                    BIT      DEFAULT ((0)) NOT NULL,
    [IsLogic]                     BIT      DEFAULT ((0)) NOT NULL,
    [IsImport]                    BIT      CONSTRAINT [DF_tNodePathExternalQuality_IsImport] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTNodePathExternalQuality] PRIMARY KEY CLUSTERED ([FTNodePathExternalQualityID] ASC),
    CONSTRAINT [FK_tFTNodePathExternalQuality_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID]),
    CONSTRAINT [FK_tFTNodePathExternalQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodePathExternalQuality';

