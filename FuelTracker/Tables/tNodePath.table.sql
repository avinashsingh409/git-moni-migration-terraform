CREATE TABLE [FuelTracker].[tNodePath] (
    [FTNodePathID]        INT            IDENTITY (1, 1) NOT NULL,
    [PathAddedTimestamp]  DATETIME       CONSTRAINT [DF_tFTNodePath_PathAddedTimestamp] DEFAULT (getdate()) NOT NULL,
    [FTNodePathSortOrder] REAL           NOT NULL,
    [IsOn]                BIT            CONSTRAINT [DF_tFTNodePath_IsOn] DEFAULT ((0)) NOT NULL,
    [FTNodePathAbbrev]    NVARCHAR (255) NOT NULL,
    [FTNodePathDesc]      NVARCHAR (255) NOT NULL,
    [FromFTNodeID]        INT            NOT NULL,
    [ToFTNodeID]          INT            NOT NULL,
    [PathMovementTag]     NVARCHAR (255) NULL,
    [IsTrueUpAddPath]     BIT            CONSTRAINT [DF_FuelTracker_tNodePath_IsTrueUpAddPath] DEFAULT ((0)) NOT NULL,
    [IsTrueUpRemovePath]  BIT            CONSTRAINT [DF_FuelTracker_tNodePath_IsTrueUpRemovePath] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTNodePath] PRIMARY KEY CLUSTERED ([FTNodePathID] ASC),
    CONSTRAINT [FK_tFTNodePath_FromFTNodeID_tFTNode] FOREIGN KEY ([FromFTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID]),
    CONSTRAINT [FK_tFTNodePath_ToFTNodeID_tFTNode] FOREIGN KEY ([ToFTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodePath';

