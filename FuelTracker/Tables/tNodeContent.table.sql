CREATE TABLE [FuelTracker].[tNodeContent] (
    [FTNodeContentID]      INT      IDENTITY (1, 1) NOT NULL,
    [FTTransactionID]      INT      NOT NULL,
    [FTNodeID]             INT      NOT NULL,
    [FuelSourceID]         INT      NOT NULL,
    [FuelQualityID]        INT      NOT NULL,
    [CurrentTimestamp]     DATETIME NOT NULL,
    [TransactionSortOrder] INT      NOT NULL,
    [Quantity_ton]         REAL     NOT NULL,
    CONSTRAINT [PK_tFTNodeContent] PRIMARY KEY CLUSTERED ([FTNodeContentID] ASC),
    CONSTRAINT [FK_tFTNodeContent_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID]),
    CONSTRAINT [FK_tFTNodeContent_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Specific content at a given node at a given time.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodeContent';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodeContent';

