CREATE TABLE [FuelTracker].[tInTransit] (
    [FTInTransitID]    INT            IDENTITY (1, 1) NOT NULL,
    [FTNodePathID]     INT            NOT NULL,
    [StartDate]        DATETIME       NOT NULL,
    [EndDate]          DATETIME       NOT NULL,
    [Quantity_ton]     REAL           NOT NULL,
    [RightAngleRowKey] NVARCHAR (255) NOT NULL,
    [HasProcessed]     BIT            CONSTRAINT [DF_tFTInTransit_HasProcessed] DEFAULT ((0)) NOT NULL,
    [EndDateVerified]  BIT            CONSTRAINT [DF_tFTInTransit_EndDateVerified] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FTInTransitID] PRIMARY KEY CLUSTERED ([FTInTransitID] ASC),
    CONSTRAINT [FK_tFTInTransit_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Store Right Angle data that represents movements that are in-transit and not ready for delivery.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tInTransit';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tInTransit';

