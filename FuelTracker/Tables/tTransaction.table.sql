CREATE TABLE [FuelTracker].[tTransaction] (
    [FTTransactionID]        INT      IDENTITY (1, 1) NOT NULL,
    [FTNodeID]               INT      NOT NULL,
    [CurrentTimestamp]       DATETIME NOT NULL,
    [Quantity_ton]           REAL     NOT NULL,
    [FuelSourceConfidence]   INT      CONSTRAINT [DF_tFTTransactionSource_FuelSourceConfidence] DEFAULT ((0)) NOT NULL,
    [FuelQualityConfidence]  INT      CONSTRAINT [DF_tFTTransactionQuality_FuelQualityConfidence] DEFAULT ((0)) NOT NULL,
    [FTTransactionSummaryID] INT      NULL,
    [IsWithdraw]             BIT      CONSTRAINT [DF_tFTTransaction_IsWithdraw] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTTransaction] PRIMARY KEY CLUSTERED ([FTTransactionID] ASC),
    CONSTRAINT [FK_tFTTransaction_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID]),
    CONSTRAINT [FK_tFTTransaction_FTTransactionSummaryID_tFTTransactionSummary] FOREIGN KEY ([FTTransactionSummaryID]) REFERENCES [FuelTracker].[tTransactionSummary] ([FTTransactionSummaryID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Individual transactions into or out of a node.  Negative quantity transactions are withdraws while positive quantity transactions are deposits.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransaction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Value which specifies the confidence in the source defintion.  This is based on the source data which is availabe.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransaction', @level2type = N'COLUMN', @level2name = N'FuelSourceConfidence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Value which specifies the confidence in the fuel quality.  This is based on the quality data which is availabe.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransaction', @level2type = N'COLUMN', @level2name = N'FuelQualityConfidence';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransaction';

