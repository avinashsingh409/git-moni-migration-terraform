CREATE TABLE [FuelTracker].[tTransactionQuality] (
    [FTTransactionQualityID]              INT IDENTITY (1, 1) NOT NULL,
    [FTTransactionID]                     INT NOT NULL,
    [FTTransactionSourceID]               INT NULL,
    [FuelQualityID]                       INT NOT NULL,
    [IsFuelQualityCollectorQuality]       BIT CONSTRAINT [DF_tFTTransactionQuality_IsFuelQualityCollectorQuality] DEFAULT ((0)) NOT NULL,
    [IsNodeContentQuality]                BIT CONSTRAINT [DF_tFTTransactionQuality_IsNodeContentQuality] DEFAULT ((0)) NOT NULL,
    [IsFuelQualityCollectorSourceQuality] BIT CONSTRAINT [DF_tFTTransactionQuality_IsFuelQualityCollectorSourceQuality] DEFAULT ((0)) NOT NULL,
    [IsDefaultedQuality]                  BIT CONSTRAINT [DF_tFTTransactionQuality_IsDefaultedQuality] DEFAULT ((0)) NOT NULL,
    [IsUsedForNodeContent]                BIT CONSTRAINT [DF_tFTTransactionQuality_IsUsedForNodeContent] DEFAULT ((0)) NOT NULL,
    [IsInTransitQuality]                  BIT CONSTRAINT [DF_tFTTransactionQuality_IsInTransit] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FTTransactionQuality] PRIMARY KEY CLUSTERED ([FTTransactionQualityID] ASC),
    CONSTRAINT [FK_tFTTransactionQuality_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFTTransactionQuality_FTTransactionSourceID_tFTTransactionSource] FOREIGN KEY ([FTTransactionSourceID]) REFERENCES [FuelTracker].[tTransactionSource] ([FTTransactionSourceID]),
    CONSTRAINT [FK_tFTTransactionQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuel qualities associated with a given transaction', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the quality came from one of the installed fuel quality collectors.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality', @level2type = N'COLUMN', @level2name = N'IsFuelQualityCollectorQuality';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the quality came from the node content data.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality', @level2type = N'COLUMN', @level2name = N'IsNodeContentQuality';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the quality came from the default quality for the source specified in the installed fuel quality collector.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality', @level2type = N'COLUMN', @level2name = N'IsFuelQualityCollectorSourceQuality';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the quality came from a default source.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality', @level2type = N'COLUMN', @level2name = N'IsDefaultedQuality';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the quality is used to define the node content.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality', @level2type = N'COLUMN', @level2name = N'IsUsedForNodeContent';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionQuality';

