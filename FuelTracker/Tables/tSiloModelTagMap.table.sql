CREATE TABLE [FuelTracker].[tSiloModelTagMap] (
    [SiloModelTagMapID] INT            IDENTITY (1, 1) NOT NULL,
    [SiloModelID]       INT            NOT NULL,
    [FieldName]         NVARCHAR (255) NOT NULL,
    [Tag]               NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tSiloModelTagMap] PRIMARY KEY CLUSTERED ([SiloModelTagMapID] ASC),
    CONSTRAINT [FK_tSiloModelTagMap_SiloModelID_tSiloModel] FOREIGN KEY ([SiloModelID]) REFERENCES [FuelTracker].[tSiloModel] ([SiloModelID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tSiloModelTagMap';

