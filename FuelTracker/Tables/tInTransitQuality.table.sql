CREATE TABLE [FuelTracker].[tInTransitQuality] (
    [FTInTransitQualityID] INT IDENTITY (1, 1) NOT NULL,
    [FTInTransitID]        INT NOT NULL,
    [FuelQualityID]        INT NOT NULL,
    CONSTRAINT [PK_FTInTransitQualityID] PRIMARY KEY CLUSTERED ([FTInTransitQualityID] ASC),
    CONSTRAINT [FK_tFTInTransitQuality_FTInTransitID_tFTInTransit] FOREIGN KEY ([FTInTransitID]) REFERENCES [FuelTracker].[tInTransit] ([FTInTransitID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFTInTransitQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuel qualities associated with a given in transit source.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tInTransitQuality';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tInTransitQuality';

