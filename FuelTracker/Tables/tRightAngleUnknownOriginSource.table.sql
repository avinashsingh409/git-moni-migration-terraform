CREATE TABLE [FuelTracker].[tRightAngleUnknownOriginSource] (
    [FTRightAngleUnknownOriginSourceID] INT            IDENTITY (1, 1) NOT NULL,
    [OriginID]                          INT            NOT NULL,
    [OriginDesc]                        NVARCHAR (255) NOT NULL,
    [DestinationID]                     INT            NOT NULL,
    [DestinationDesc]                   NVARCHAR (255) NOT NULL,
    [MovementID]                        INT            NOT NULL,
    [OrderID]                           INT            NOT NULL,
    [ActualStartDate]                   DATETIME       NOT NULL,
    [Quantity_ton]                      REAL           NOT NULL,
    CONSTRAINT [PK_tFTRightAngleUnknownOriginSource] PRIMARY KEY CLUSTERED ([FTRightAngleUnknownOriginSourceID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Store origins that don''t provide enough information to determine the fuel source from the Right Angle system.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tRightAngleUnknownOriginSource';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tRightAngleUnknownOriginSource';

