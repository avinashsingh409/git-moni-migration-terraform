CREATE TABLE [FuelTracker].[tInTransitSource] (
    [FTInTransitSourceID] INT  IDENTITY (1, 1) NOT NULL,
    [FTInTransitID]       INT  NOT NULL,
    [FuelSourceID]        INT  NOT NULL,
    [Quantity_percent]    REAL NOT NULL,
    CONSTRAINT [PK_FTInTransitSourceID] PRIMARY KEY CLUSTERED ([FTInTransitSourceID] ASC),
    CONSTRAINT [FK_tFTInTransitSource_FTInTransitID_tFTInTransit] FOREIGN KEY ([FTInTransitID]) REFERENCES [FuelTracker].[tInTransit] ([FTInTransitID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFTInTransitSource_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source definition for a given in transit record by mass percent.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tInTransitSource';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tInTransitSource';

