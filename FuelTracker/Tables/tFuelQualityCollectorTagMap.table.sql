CREATE TABLE [FuelTracker].[tFuelQualityCollectorTagMap] (
    [FTFuelQualityCollectorTagMapID] INT            IDENTITY (1, 1) NOT NULL,
    [FTFuelQualityCollectorID]       INT            NOT NULL,
    [FieldName]                      NVARCHAR (255) NOT NULL,
    [DoNotCaptureBelow_tph]          REAL           NOT NULL,
    [QualityTag]                     NVARCHAR (255) NOT NULL,
    [IsLogicDefault]                 BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTFuelQualityCollectorTagMap] PRIMARY KEY CLUSTERED ([FTFuelQualityCollectorTagMapID] ASC),
    CONSTRAINT [FK_tFTFuelQualityCollectorTagMap_FTFuelQualityCollectorID_tFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tFuelQualityCollectorTagMap';

