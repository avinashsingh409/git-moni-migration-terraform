CREATE TABLE [FuelTracker].[tNode] (
    [FTNodeID]                    INT            IDENTITY (1, 1) NOT NULL,
    [FTNodeSortOrder]             REAL           NOT NULL,
    [UnitID]                      INT            NULL,
    [FTNodeAbbrev]                NVARCHAR (255) NOT NULL,
    [IsAccumulating]              BIT            CONSTRAINT [DF_tFTNode_IsAccumulating] DEFAULT ((0)) NOT NULL,
    [MaxAccumulationCapacity_ton] REAL           NOT NULL,
    [FTNodeDesc]                  NVARCHAR (255) NOT NULL,
    [FTNodeWithdrawalTypeID]      INT            NOT NULL,
    [FTNodeTypeID]                INT            NOT NULL,
    [IsUnloadingNode]             BIT            CONSTRAINT [DF_tFTNode_IsUnloadingNode] DEFAULT ((0)) NOT NULL,
    [StationGroupID]              INT            NULL,
    [StationID]                   INT            NULL,
    [AssetID]                     INT            NULL,
    CONSTRAINT [PK_tFTNode] PRIMARY KEY CLUSTERED ([FTNodeID] ASC),
    CONSTRAINT [FK_FuelTracker_tNode_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_FuelTracker_tNode_StationGroupID_Asset_tStationGroup] FOREIGN KEY ([StationGroupID]) REFERENCES [Asset].[tStationGroup] ([StationGroupID]),
    CONSTRAINT [FK_FuelTracker_tNode_StationID_Asset_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID]),
    CONSTRAINT [FK_FuelTracker_tNode_UnitID_Asset_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [FK_tFTNode_FTNodeTypeID_tFTNodeType] FOREIGN KEY ([FTNodeTypeID]) REFERENCES [FuelTracker].[tNodeType] ([FTNodeTypeID]),
    CONSTRAINT [FK_tFTNode_FTNodeWithdrawalTypeID_tFTNodeWithdrawalType] FOREIGN KEY ([FTNodeWithdrawalTypeID]) REFERENCES [FuelTracker].[tNodeWithdrawalType] ([FTNodeWithdrawalTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNode';

