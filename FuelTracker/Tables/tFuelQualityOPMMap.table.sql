CREATE TABLE [FuelTracker].[tFuelQualityOPMMap] (
    [FTFuelQualityOPMMapID] INT            IDENTITY (1, 1) NOT NULL,
    [UnitID]                INT            NOT NULL,
    [DataServer]            NVARCHAR (50)  NOT NULL,
    [DataSource]            NVARCHAR (255) NOT NULL,
    [FieldName]             NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tFTFuelQualityOPMMap] PRIMARY KEY CLUSTERED ([FTFuelQualityOPMMapID] ASC),
    CONSTRAINT [FK_tFTFuelQualityOPMMap_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tFuelQualityOPMMap';

