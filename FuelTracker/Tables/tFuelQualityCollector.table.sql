CREATE TABLE [FuelTracker].[tFuelQualityCollector] (
    [FTFuelQualityCollectorID]     INT            IDENTITY (1, 1) NOT NULL,
    [FTFuelQualityCollectorDesc]   NVARCHAR (255) NOT NULL,
    [FTFuelQualityCollectorAbbrev] NVARCHAR (255) NOT NULL,
    [IsPhysicalEquipment]          BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTFuelQualityCollector] PRIMARY KEY CLUSTERED ([FTFuelQualityCollectorID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tFuelQualityCollector';

