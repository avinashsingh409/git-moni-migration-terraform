CREATE TABLE [FuelTracker].[tNodeContentSourceDefault] (
    [FTNodeContentSourceDefaultID] INT      IDENTITY (1, 1) NOT NULL,
    [FTNodeID]                     INT      NOT NULL,
    [FuelSourceID]                 INT      NOT NULL,
    [CurrentTimestamp]             DATETIME NOT NULL,
    CONSTRAINT [PK_tFTNodeContentSourceDefault] PRIMARY KEY CLUSTERED ([FTNodeContentSourceDefaultID] ASC),
    CONSTRAINT [FK_tFTNodeContentSourceDefault_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID]),
    CONSTRAINT [FK_tFTNodeContentSourceDefault_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Default Fuel Sources over a given time associated with a node.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodeContentSourceDefault';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodeContentSourceDefault';

