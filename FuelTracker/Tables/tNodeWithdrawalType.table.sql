CREATE TABLE [FuelTracker].[tNodeWithdrawalType] (
    [FTNodeWithdrawalTypeID]     INT            IDENTITY (1, 1) NOT NULL,
    [FTNodeWithdrawalTypeDesc]   NVARCHAR (255) NULL,
    [FTNodeWithdrawalTypeAbbrev] NVARCHAR (255) NULL,
    CONSTRAINT [PK_tFTNodeWithdrawalType] PRIMARY KEY CLUSTERED ([FTNodeWithdrawalTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tNodeWithdrawalType';

