CREATE TABLE [FuelTracker].[tSiloModelNodePathMap] (
    [SiloModelNodePathMapID] INT IDENTITY (1, 1) NOT NULL,
    [SiloModelID]            INT NOT NULL,
    [NodePathID]             INT NOT NULL,
    CONSTRAINT [PK_tSiloModelNodePathMap] PRIMARY KEY CLUSTERED ([SiloModelNodePathMapID] ASC),
    CONSTRAINT [FK_tSiloModelNodePathMap_NodePathID_tNodePath] FOREIGN KEY ([NodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID]),
    CONSTRAINT [FK_tSiloModelNodePathMap_SiloModelID_tSiloModel] FOREIGN KEY ([SiloModelID]) REFERENCES [FuelTracker].[tSiloModel] ([SiloModelID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tSiloModelNodePathMap';

