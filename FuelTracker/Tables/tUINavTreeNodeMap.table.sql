CREATE TABLE [FuelTracker].[tUINavTreeNodeMap] (
    [UIFTNavTreeID] INT NOT NULL,
    [FTNodeID]      INT NOT NULL,
    CONSTRAINT [PK_tUIFTNavTreeNodeMap] PRIMARY KEY CLUSTERED ([UIFTNavTreeID] ASC, [FTNodeID] ASC),
    CONSTRAINT [FK_tUIFTNavTreeNodeMap_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID]),
    CONSTRAINT [FK_tUIFTNavTreeNodeMap_UIFTNavTreeID_tUIFTNavTree] FOREIGN KEY ([UIFTNavTreeID]) REFERENCES [FuelTracker].[tUINavTree] ([UIFTNavTreeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tUINavTreeNodeMap';

