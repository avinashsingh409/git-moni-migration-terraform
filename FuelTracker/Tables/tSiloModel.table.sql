CREATE TABLE [FuelTracker].[tSiloModel] (
    [SiloModelID]       INT            IDENTITY (1, 1) NOT NULL,
    [SiloModelDesc]     NVARCHAR (255) NOT NULL,
    [SiloModelAbbrev]   NVARCHAR (255) NOT NULL,
    [UnitID]            INT            NOT NULL,
    [Bunker]            NVARCHAR (10)  NOT NULL,
    [BunkerLevelFactor] REAL           NOT NULL,
    [InletFlowFactor]   REAL           NOT NULL,
    [OutletFlowFactor]  REAL           NOT NULL,
    [WidthFromCenter]   REAL           NOT NULL,
    [OffsetFromCenter]  REAL           NOT NULL,
    [MinimumLevel]      REAL           NOT NULL,
    [IsOn]              BIT            CONSTRAINT [DF_FuelTrackertSiloModel] DEFAULT ((1)) NOT NULL,
    [AssetID]           INT            NOT NULL,
    CONSTRAINT [PK_tSiloModel] PRIMARY KEY CLUSTERED ([SiloModelID] ASC),
    CONSTRAINT [FK_tSiloModel_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tSiloModel';

