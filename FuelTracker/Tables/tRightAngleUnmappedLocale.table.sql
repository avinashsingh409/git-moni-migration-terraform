CREATE TABLE [FuelTracker].[tRightAngleUnmappedLocale] (
    [FTRightAngleUnmappedLocaleID] INT            IDENTITY (1, 1) NOT NULL,
    [LocaleID]                     INT            NOT NULL,
    [LocaleDesc]                   NVARCHAR (255) NOT NULL,
    [PlantAssociationDesc]         NVARCHAR (255) NOT NULL,
    [FirstMovementDate]            DATETIME       NOT NULL,
    [IsMine]                       BIT            CONSTRAINT [DF_tFTRightAngleUnmappedLocale_IsMine] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTRightAngleUnmappedLocale] PRIMARY KEY CLUSTERED ([FTRightAngleUnmappedLocaleID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Store unmapped Locale id''s from the Right Angle system.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tRightAngleUnmappedLocale';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tRightAngleUnmappedLocale';

