CREATE TABLE [FuelTracker].[tMovement] (
    [FTMovementID]     INT      IDENTITY (1, 1) NOT NULL,
    [CurrentTimestamp] DATETIME NOT NULL,
    [FTNodePathID]     INT      NOT NULL,
    [Quantity_ton]     REAL     NOT NULL,
    [IsRightAngle]     BIT      CONSTRAINT [DF_tFTMovement_IsRightAngle] DEFAULT ((0)) NOT NULL,
    [IsManual]         BIT      CONSTRAINT [DF_tFTMovement_IsManual] DEFAULT ((0)) NOT NULL,
    [IsLogic]          BIT      CONSTRAINT [DF_tFTMovement_IsLogic] DEFAULT ((0)) NOT NULL,
    [HasProcessed]     BIT      CONSTRAINT [DF_tFTMovement_HasProcessed] DEFAULT ((0)) NOT NULL,
    [FTInTransitID]    INT      NULL,
    [IsImport]         BIT      CONSTRAINT [DF_tMovement_IsImport] DEFAULT ((0)) NOT NULL,
    [IsTrueUp]         BIT      CONSTRAINT [DF_FuelTracker_tMovement_IsTrueUp] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tFTMovement] PRIMARY KEY CLUSTERED ([FTMovementID] ASC),
    CONSTRAINT [FK_tFTMovement_FTInTransitID] FOREIGN KEY ([FTInTransitID]) REFERENCES [FuelTracker].[tInTransit] ([FTInTransitID]),
    CONSTRAINT [FK_tFTMovement_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tMovement';

