CREATE TABLE [FuelTracker].[tManualMovement] (
    [FTManualMovementID] INT           IDENTITY (1, 1) NOT NULL,
    [FromFTNodeID]       INT           NULL,
    [ToFTNodeID]         INT           NULL,
    [MovedBy]            NVARCHAR (50) NULL,
    [TimeMoved]          DATETIME      NULL,
    [Quantity_tons]      REAL          NULL,
    [PlantID]            INT           NULL,
    [StationID]          INT           NULL,
    CONSTRAINT [PK_tFTManualMovement] PRIMARY KEY CLUSTERED ([FTManualMovementID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'temporary - will be replaced by Station ID', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tManualMovement', @level2type = N'COLUMN', @level2name = N'PlantID';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tManualMovement';

