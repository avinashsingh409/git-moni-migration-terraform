CREATE TABLE [FuelTracker].[tUINavTree] (
    [UIFTNavTreeID]        INT            IDENTITY (1, 1) NOT NULL,
    [ParentUIFTNavTreeID]  INT            NOT NULL,
    [UIFTNavTreeSetID]     INT            NOT NULL,
    [UIFTNavTreeSortOrder] REAL           NOT NULL,
    [UIFTNavTreeDesc]      NVARCHAR (255) NOT NULL,
    [IsExpanded]           BIT            CONSTRAINT [DF_tFTNavTree_IsExpanded] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tUIFTNavTree] PRIMARY KEY CLUSTERED ([UIFTNavTreeID] ASC),
    CONSTRAINT [FK_tUIFTNavTree_UIFTNavTreeSetID_tUIFTNavTreeSet] FOREIGN KEY ([UIFTNavTreeSetID]) REFERENCES [FuelTracker].[tUINavTreeSet] ([UIFTNavTreeSetID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tUINavTree';

