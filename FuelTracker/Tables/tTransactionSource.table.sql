CREATE TABLE [FuelTracker].[tTransactionSource] (
    [FTTransactionSourceID]        INT  IDENTITY (1, 1) NOT NULL,
    [FTTransactionID]              INT  NOT NULL,
    [FuelSourceID]                 INT  NOT NULL,
    [Quantity_percent]             REAL NOT NULL,
    [IsNodeContentSource]          BIT  CONSTRAINT [DF_tFTTransactionSource_IsNodeContentSource] DEFAULT ((0)) NOT NULL,
    [IsFuelQualityCollectorSource] BIT  CONSTRAINT [DF_tFTTransactionSource_IsFuelQualityCollectorSource] DEFAULT ((0)) NOT NULL,
    [IsDefaultedSource]            BIT  CONSTRAINT [DF_tFTTransactionSource_IsDefaultedSource] DEFAULT ((0)) NOT NULL,
    [IsUsedForNodeContent]         BIT  CONSTRAINT [DF_tFTTransactionSource_IsUsedForNodeContent] DEFAULT ((0)) NOT NULL,
    [IsInTransitSource]            BIT  CONSTRAINT [DF_tFTTransactionSource_IsInTransit] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FTTransactionSource] PRIMARY KEY CLUSTERED ([FTTransactionSourceID] ASC),
    CONSTRAINT [FK_tFTTransactionSource_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFTTransactionSource_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source definition for a given transaction by mass percent.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionSource';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the source came from node content.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionSource', @level2type = N'COLUMN', @level2name = N'IsNodeContentSource';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the source came from one of the installed fuel quality collector inputs.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionSource', @level2type = N'COLUMN', @level2name = N'IsFuelQualityCollectorSource';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag specifying that the source is the definition used to define the node content.', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionSource', @level2type = N'COLUMN', @level2name = N'IsUsedForNodeContent';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tTransactionSource';

