CREATE TABLE [FuelTracker].[tFuelQualityCollectorThermoSourceMap] (
    [FuelQualityCollectorThermoSourceMapID] INT      IDENTITY (1, 1) NOT NULL,
    [FTFuelQualityCollectorID]              INT      NOT NULL,
    [FuelSourceID]                          INT      NOT NULL,
    [ThermoSource]                          INT      NOT NULL,
    [EffectiveDate]                         DATETIME NOT NULL,
    CONSTRAINT [PK_FuelTrackertFuelQualityCollectorThermoSourceMap] PRIMARY KEY CLUSTERED ([FuelQualityCollectorThermoSourceMapID] ASC),
    CONSTRAINT [FK_FuelTrackertFuelQualityCollectorThermoSourceMap_FTFuelQualityCollectorID_FuelTrackertFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID]),
    CONSTRAINT [FK_FuelTrackertFuelQualityCollectorThermoSourceMap_FuelSourceID_FueltFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'TABLE', @level1name = N'tFuelQualityCollectorThermoSourceMap';

