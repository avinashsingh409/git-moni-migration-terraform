﻿CREATE VIEW [FuelTracker].[vFuelsByStationCoalYard]
	AS
	SELECT a.CurrentTimestamp, e.StationID, e.DisplayOrder AS StationDisplayOrder, 
	e.StationAbbrev, e.StationDesc, d.FTNodeTypeID, c.FTNodeDesc, 
	d.FTNodeTypeDesc, c.FTNodeID, c.FTNodeAbbrev, c.FTNodeSortOrder, a.TransactionSortOrder, 
	b.FuelSourceID, b.FuelSourceDesc, c.StationGroupID
	FROM FuelTracker.tNodeContent a INNER JOIN Fuel.tFuelSource b ON a.FuelSourceID = b.FuelSourceID 
	INNER JOIN FuelTracker.tNode c ON a.FTNodeID = c.FTNodeID
	INNER JOIN FuelTracker.tNodeType d ON c.FTNodeTypeID = d.FTNodeTypeID 
	INNER JOIN Asset.tStation e ON c.StationID = e.StationID
	WHERE d.FTNodeTypeID IN (3, 4, 6, 7, 8, 9)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tFTLogNodeContent"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tFTLogNodeContentSource"
            Begin Extent = 
               Top = 6
               Left = 314
               Bottom = 121
               Right = 548
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tFuelSource"
            Begin Extent = 
               Top = 6
               Left = 586
               Bottom = 121
               Right = 771
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tFTNode"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tFTNodeType"
            Begin Extent = 
               Top = 126
               Left = 320
               Bottom = 241
               Right = 552
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tStation"
            Begin Extent = 
               Top = 126
               Left = 590
               Bottom = 241
               Right = 795
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
     ', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'VIEW', @level1name = N'vFuelsByStationCoalYard';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'    Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'VIEW', @level1name = N'vFuelsByStationCoalYard';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'FuelTracker', @level1type = N'VIEW', @level1name = N'vFuelsByStationCoalYard';

