﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [PPMD_Luminant_Test], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', FILEGROWTH = 102400 KB) TO FILEGROUP [PRIMARY];

