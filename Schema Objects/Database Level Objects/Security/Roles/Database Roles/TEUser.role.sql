﻿CREATE ROLE [TEUser]
    AUTHORIZATION [dbo];










GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Base group for all technology enabler users.', @level0type = N'USER', @level0name = N'TEUser';



GO
EXECUTE sp_addrolemember @rolename = N'TEUser', @membername = N'PowerPlantMD';

