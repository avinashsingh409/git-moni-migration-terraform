CREATE TABLE [AdaptivePlanning].[tUnitPlanAdjustmentScenarioMap] (
    [UnitPlanAdjustmentScenarioMapID] INT IDENTITY (1, 1) NOT NULL,
    [UnitPlanAdjustmentID]            INT NOT NULL,
    [ScenarioID]                      INT NOT NULL,
    CONSTRAINT [PK_tUnitPlanAdjustmentScenarioMap] PRIMARY KEY CLUSTERED ([UnitPlanAdjustmentScenarioMapID] ASC),
    CONSTRAINT [FK_tUnitPlanAdjustmentScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitPlanAdjustmentScenarioMap_tUnitPlanAdjustment] FOREIGN KEY ([UnitPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustment] ([UnitPlanAdjustmentID]) ON DELETE CASCADE
);



  



GO
CREATE NONCLUSTERED INDEX IDX_tUnitPlanAdjustmentScenarioMap_1
ON AdaptivePlanning.tUnitPlanAdjustmentScenarioMap (UnitPlanAdjustmentID,ScenarioID)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tUnitPlanAdjustmentScenarioMap';

