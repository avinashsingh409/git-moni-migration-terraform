CREATE TABLE [AdaptivePlanning].[tFuelPlanAdjustmentSet] (
    [FuelPlanAdjustmentSetID] INT            IDENTITY (1, 1) NOT NULL,
    [Description]             NVARCHAR (255) NOT NULL,
    [AnalysisSetID]           INT            NOT NULL,
    [BaseFuelPlanSetID]       INT            NOT NULL,
    [CreatedBy]               NVARCHAR (255) NOT NULL,
    [ChangedBy]               NVARCHAR (255) NOT NULL,
    [CreateDate]              DATETIME       NOT NULL,
    [ChangeDate]              DATETIME       NOT NULL,
    CONSTRAINT [PK_tFuelPlanAdjustmentSet] PRIMARY KEY CLUSTERED ([FuelPlanAdjustmentSetID] ASC),
    CONSTRAINT [FK_tFuelPlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanAdjustmentSet_tFuelPlanSet] FOREIGN KEY ([BaseFuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tFuelPlanAdjustmentSet';

