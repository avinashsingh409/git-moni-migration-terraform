CREATE TABLE [AdaptivePlanning].[tMarketPlanAdjustmentUnitMap] (
    [MarketPlanAdjustmentUnitMapID] INT IDENTITY (1, 1) NOT NULL,
    [MarketPlanAdjustmentID]        INT NOT NULL,
    [UnitID]                        INT NOT NULL,
    CONSTRAINT [PK_tMarketPlanAdjustmentUnitMap] PRIMARY KEY CLUSTERED ([MarketPlanAdjustmentUnitMapID] ASC),
    CONSTRAINT [FK_tMarketPlanAdjustmentSetUnitMap_tMarketPlanAdjustment] FOREIGN KEY ([MarketPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tMarketPlanAdjustment] ([MarketPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMarketPlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMarketPlanAdjustmentUnitMap';

