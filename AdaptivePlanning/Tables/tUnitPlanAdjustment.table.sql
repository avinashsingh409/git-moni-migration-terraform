CREATE TABLE [AdaptivePlanning].[tUnitPlanAdjustment] (
    [UnitPlanAdjustmentID]     INT            IDENTITY (1, 1) NOT NULL,
    [Description]              NVARCHAR (255) NOT NULL,
    [UnitPlanAdjustmentSetID]  INT            NOT NULL,
    [StartDate]                DATETIME       NOT NULL,
    [EndDate]                  DATETIME       NOT NULL,
    [CanAdustFGDRemEff]        BIT            CONSTRAINT [DF_tUnitPlanAdjustment_CanAdustFGDRemEff] DEFAULT ((0)) NOT NULL,
    [CanAdjustSCRorSCNRRemEff] BIT            CONSTRAINT [DF_tUnitPlanAdjustment_CanAdjustSCRorSCNRRemEff] DEFAULT ((0)) NOT NULL,
    [CanAjustSCRorSCNRRate]    BIT            CONSTRAINT [DF_tUnitPlanAdjustment_CanAjustSCRorSCNRRate] DEFAULT ((0)) NOT NULL,
    [CanAdjustHgRemEff]        BIT            CONSTRAINT [DF_tUnitPlanAdjustment_CanAdjustHgRemEff] DEFAULT ((0)) NOT NULL,
    [CanAdjustHeatRate]        BIT            CONSTRAINT [DF_tUnitPlanAdjustment_CanAdjustHeatRate] DEFAULT ((0)) NOT NULL,
    [CreatedBy]                NVARCHAR (255) NOT NULL,
    [ChangedBy]                NVARCHAR (255) NOT NULL,
    [CreateDate]               DATETIME       CONSTRAINT [DF_tUnitPlanAdjustment_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]               DATETIME       CONSTRAINT [DF_tUnitPlanAdjustment_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tUnitPlanAdjustment] PRIMARY KEY CLUSTERED ([UnitPlanAdjustmentID] ASC),
    CONSTRAINT [FK_tUnitPlanAdjustment_tUnitPlanAdjustmentSe] FOREIGN KEY ([UnitPlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustmentSet] ([UnitPlanAdjustmentSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tUnitPlanAdjustment';

