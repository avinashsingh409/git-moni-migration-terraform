CREATE TABLE [AdaptivePlanning].[tGenerationPlanAdjustment] (
    [GenerationPlanAdjustmentID]    INT            IDENTITY (1, 1) NOT NULL,
    [Description]                   NVARCHAR (255) NOT NULL,
    [GenerationPlanAdjustmentSetID] INT            NOT NULL,
    [StartDate]                     DATETIME       NULL,
    [EndDate]                       DATETIME       NULL,
    [CreatedBy]                     NVARCHAR (255) NOT NULL,
    [ChangedBy]                     NVARCHAR (255) NOT NULL,
    [CreateDate]                    DATETIME       CONSTRAINT [DF_tGenerationPlanAdjustment_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                    DATETIME       CONSTRAINT [DF_tGenerationPlanAdjustment_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tGenerationPlanAdjustment] PRIMARY KEY CLUSTERED ([GenerationPlanAdjustmentID] ASC),
    CONSTRAINT [FK_tGenerationPlanAdjustment_tGenerationPlanAdjustmentSe] FOREIGN KEY ([GenerationPlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tGenerationPlanAdjustmentSet] ([GenerationPlanAdjustmentSetID]) ON DELETE CASCADE
);







GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tGenerationPlanAdjustment';

