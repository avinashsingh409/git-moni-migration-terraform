CREATE TABLE [AdaptivePlanning].[tMarketPlanAdjustment] (
    [MarketPlanAdjustmentID]    INT            IDENTITY (1, 1) NOT NULL,
    [Description]               NVARCHAR (255) NOT NULL,
    [MarketPlanAdjustmentSetID] INT            NOT NULL,
    [StartDate]                 DATETIME       NULL,
    [EndDate]                   DATETIME       NULL,
    [CreatedBy]                 NVARCHAR (255) NOT NULL,
    [ChangedBy]                 NVARCHAR (255) NOT NULL,
    [CreateDate]                DATETIME       CONSTRAINT [DF_tMarketPlanAdjustment_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                DATETIME       CONSTRAINT [DF_tMarketPlanAdjustment_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tMarketPlanAdjustment] PRIMARY KEY CLUSTERED ([MarketPlanAdjustmentID] ASC),
    CONSTRAINT [FK_tMarketPlanAdjustment_tMarketPlanAdjustmentSe] FOREIGN KEY ([MarketPlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tMarketPlanAdjustmentSet] ([MarketPlanAdjustmentSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMarketPlanAdjustment';

