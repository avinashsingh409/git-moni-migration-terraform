CREATE TABLE [AdaptivePlanning].[tUnitPlanAdjustmentUnitMap] (
    [UnitPlanAdjustmentUnitMapID] INT IDENTITY (1, 1) NOT NULL,
    [UnitPlanAdjustmentID]        INT NOT NULL,
    [UnitID]                      INT NOT NULL,
    CONSTRAINT [PK_tUnitPlanAdjustmentUnitMap] PRIMARY KEY CLUSTERED ([UnitPlanAdjustmentUnitMapID] ASC),
    CONSTRAINT [FK_tUnitPlanAdjustmentSetUnitMap_tUnitPlanAdjustment] FOREIGN KEY ([UnitPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustment] ([UnitPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitPlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tUnitPlanAdjustmentUnitMap';

