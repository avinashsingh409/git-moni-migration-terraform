CREATE TABLE [AdaptivePlanning].[tGenerationPlanAdjustmentSet] (
    [GenerationPlanAdjustmentSetID] INT            IDENTITY (1, 1) NOT NULL,
    [Description]                   NVARCHAR (255) NOT NULL,
    [AnalysisSetID]                 INT            NOT NULL,
    [BaseGenerationSetID]           INT            NOT NULL,
    [CreatedBy]                     NVARCHAR (255) NOT NULL,
    [ChangedBy]                     NVARCHAR (255) NOT NULL,
    [CreateDate]                    DATETIME       NOT NULL,
    [ChangeDate]                    DATETIME       NOT NULL,
    CONSTRAINT [PK_tGenerationPlanAdjustmentSet] PRIMARY KEY CLUSTERED ([GenerationPlanAdjustmentSetID] ASC),
    CONSTRAINT [FK_tGenerationPlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationPlanAdjustmentSet_tGenerationSet] FOREIGN KEY ([BaseGenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tGenerationPlanAdjustmentSet';

