CREATE TABLE [AdaptivePlanning].[tGenerationPlanAdjustmentUnitMap] (
    [GenerationPlanAdjustmentUnitMapID] INT IDENTITY (1, 1) NOT NULL,
    [GenerationPlanAdjustmentID]        INT NOT NULL,
    [UnitID]                            INT NOT NULL,
    CONSTRAINT [PK_tGenerationPlanAdjustmentUnitMap] PRIMARY KEY CLUSTERED ([GenerationPlanAdjustmentUnitMapID] ASC),
    CONSTRAINT [FK_tGenerationPlanAdjustmentSetUnitMap_tGenerationPlanAdjustment] FOREIGN KEY ([GenerationPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tGenerationPlanAdjustment] ([GenerationPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationPlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tGenerationPlanAdjustmentUnitMap';

