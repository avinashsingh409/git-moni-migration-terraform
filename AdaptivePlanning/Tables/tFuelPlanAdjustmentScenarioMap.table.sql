CREATE TABLE [AdaptivePlanning].[tFuelPlanAdjustmentScenarioMap] (
    [FuelPlanAdjustmentScenarioMapID] INT IDENTITY (1, 1) NOT NULL,
    [FuelPlanAdjustmentID]            INT NOT NULL,
    [ScenarioID]                      INT NOT NULL,
    CONSTRAINT [PK_tFuelPlanAdjustmentScenarioMap] PRIMARY KEY CLUSTERED ([FuelPlanAdjustmentScenarioMapID] ASC),
    CONSTRAINT [FK_tFuelPlanAdjustmentScenarioMap_tFuelPlanAdjustment] FOREIGN KEY ([FuelPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanAdjustmentScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);



  



GO
CREATE NONCLUSTERED INDEX IDX_tFuelPlanAdjustmentScenarioMap_1
ON AdaptivePlanning.tFuelPlanAdjustmentScenarioMap (FuelPlanAdjustmentID,ScenarioID)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tFuelPlanAdjustmentScenarioMap';

