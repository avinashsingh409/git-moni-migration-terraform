CREATE TABLE [AdaptivePlanning].[tMarketPlanAdjustmentScenarioMap] (
    [MarketPlanAdjustmentScenarioMapID] INT IDENTITY (1, 1) NOT NULL,
    [MarketPlanAdjustmentID]            INT NOT NULL,
    [ScenarioID]                        INT NOT NULL,
    CONSTRAINT [PK_tMarketPlanAdjustmentScenarioMap] PRIMARY KEY CLUSTERED ([MarketPlanAdjustmentScenarioMapID] ASC),
    CONSTRAINT [FK_tMarketPlanAdjustmentScenarioMap_tMarketPlanAdjustment] FOREIGN KEY ([MarketPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tMarketPlanAdjustment] ([MarketPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMarketPlanAdjustmentScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);



  



GO
CREATE NONCLUSTERED INDEX IDX_tMarketPlanAdjustmentScenarioMap_1
ON AdaptivePlanning.tMarketPlanAdjustmentScenarioMap (MarketPlanAdjustmentID,ScenarioID)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMarketPlanAdjustmentScenarioMap';

