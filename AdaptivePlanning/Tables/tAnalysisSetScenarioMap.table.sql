CREATE TABLE [AdaptivePlanning].[tAnalysisSetScenarioMap] (
    [AnalysisSetScenarioMapID] INT IDENTITY (1, 1) NOT NULL,
    [AnalysisSetID]            INT NOT NULL,
    [ScenarioID]               INT NOT NULL,
    CONSTRAINT [PK_tAnalysisSetScenarioMap] PRIMARY KEY CLUSTERED ([AnalysisSetScenarioMapID] ASC),
    CONSTRAINT [FK_tAnalysisSetScenarioMap_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAnalysisSetScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);



  



GO
CREATE NONCLUSTERED INDEX IDX_tAnalysisSetScenarioMap_1
ON AdaptivePlanning.tAnalysisSetScenarioMap (ScenarioID)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tAnalysisSetScenarioMap';

