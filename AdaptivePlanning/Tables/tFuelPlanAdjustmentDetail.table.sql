CREATE TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] (
    [FuelPlanAdjustmentDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [FuelPlanAdjustmentID]       INT            NOT NULL,
    [UnitID]                     INT            NOT NULL,
    [FuelSourceID]               INT            NOT NULL,
    [WeekdaysOnPeak_pct]         FLOAT (53)     NULL,
    [WeekdaysOffPeak_pct]        FLOAT (53)     NULL,
    [WeekendsOnPeak_pct]         FLOAT (53)     NULL,
    [WeekendsOffPeak_pct]        FLOAT (53)     NULL,
    [IsAnnuallyRecurring]        BIT            NULL,
    [HHV_btu_per_lbm]            FLOAT (53)     NULL,
    [SO2_pct]                    FLOAT (53)     NULL,
    [Ash_pct]                    FLOAT (53)     NULL,
    [Moisture_pct]               FLOAT (53)     NULL,
    [Iron_pct]                   FLOAT (53)     NULL,
    [Hg_ppm]                     FLOAT (53)     NULL,
    [CreatedBy]                  NVARCHAR (255) NOT NULL,
    [ChangedBy]                  NVARCHAR (255) NOT NULL,
    [CreateDate]                 DATETIME       CONSTRAINT [DF_tFuelPlanAdjustmentDetail_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                 DATETIME       CONSTRAINT [DF_tFuelPlanAdjustmentDetail_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tFuelPlanAdjustmentDetail] PRIMARY KEY CLUSTERED ([FuelPlanAdjustmentDetailID] ASC),
    CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tFuelPlanAdjustment] FOREIGN KEY ([FuelPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tFuelPlanAdjustmentDetail';

