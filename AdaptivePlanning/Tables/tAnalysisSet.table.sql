CREATE TABLE [AdaptivePlanning].[tAnalysisSet] (
    [AnalysisSetID]     INT            IDENTITY (1, 1) NOT NULL,
    [Description]       NVARCHAR (255) NOT NULL,
    [Purpose]           VARCHAR (MAX)  NOT NULL,
    [BaseScenarioID]    INT            NOT NULL,
    [StartDate]         DATETIME       NULL,
    [EndDate]           DATETIME       NULL,
    [IsArchived]        BIT            CONSTRAINT [DF_tAnalysisSet_IsArchived] DEFAULT ((0)) NOT NULL,
    [IsPublic]          BIT            CONSTRAINT [DF_tAnalysisSet_IsPublic] DEFAULT ((0)) NOT NULL,
    [IsForMargin]       BIT            CONSTRAINT [DF_tAnalysisSet_IsForMargin] DEFAULT ((0)) NOT NULL,
    [IsForGeneration]   BIT            CONSTRAINT [DF_tAnalysisSet_IsForGeneration] DEFAULT ((0)) NOT NULL,
    [IsForEmissions]    BIT            CONSTRAINT [DF_tAnalysisSet_IsForEmissions] DEFAULT ((0)) NOT NULL,
    [IsForFuelBlends]   BIT            CONSTRAINT [DF_tAnalysisSet_IsForFuelBlends] DEFAULT ((0)) NOT NULL,
    [IsForReliability]  BIT            CONSTRAINT [DF_tAnalysisSet_IsForReliability] DEFAULT ((0)) NOT NULL,
    [IsForHeatRate]     BIT            CONSTRAINT [DF_tAnalysisSet_IsForHeatRate] DEFAULT ((0)) NOT NULL,
    [IsForFuelQuality]  BIT            CONSTRAINT [DF_tAnalysisSet_IsForFuelQuality] DEFAULT ((0)) NOT NULL,
    [IsForAllMetrics]   BIT            CONSTRAINT [DF_tAnalysisSet_IsForAllMetrics] DEFAULT ((0)) NOT NULL,
    [IsDynamicUpdate]   BIT            CONSTRAINT [DF_tAnalysisSet_IsDynamicUpdate] DEFAULT ((0)) NOT NULL,
    [RunDate]           DATETIME       NULL,
    [RunStatus]         TINYINT        CONSTRAINT [DF_tAnalysisSet_RunStatus] DEFAULT ((0)) NULL,
    [CreatedBy]         NVARCHAR (255) NOT NULL,
    [CreateDate]        DATETIME       CONSTRAINT [DF_tAnalysisSet_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangedBy]         NVARCHAR (255) NOT NULL,
    [ChangeDate]        DATETIME       CONSTRAINT [DF_tAnalysisSet_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [AnalysisSetTypeID] INT            DEFAULT ((0)) NOT NULL,
    [OwningAssetID]     INT            NULL,
    CONSTRAINT [PK_tAnalysisSet] PRIMARY KEY CLUSTERED ([AnalysisSetID] ASC),
    CONSTRAINT [FK_AdaptivePlanning_tAnalysisSet_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tAnalysisSet_tScenario] FOREIGN KEY ([BaseScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID])
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tAnalysisSet';

