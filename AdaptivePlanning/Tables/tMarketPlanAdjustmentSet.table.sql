CREATE TABLE [AdaptivePlanning].[tMarketPlanAdjustmentSet] (
    [MarketPlanAdjustmentSetID] INT            IDENTITY (1, 1) NOT NULL,
    [Description]               NVARCHAR (255) NOT NULL,
    [AnalysisSetID]             INT            NOT NULL,
    [BaseForecastSetID]         INT            NOT NULL,
    [CreatedBy]                 NVARCHAR (255) NOT NULL,
    [ChangedBy]                 NVARCHAR (255) NOT NULL,
    [CreateDate]                DATETIME       NOT NULL,
    [ChangeDate]                DATETIME       NOT NULL,
    CONSTRAINT [PK_tMarketPlanAdjustmentSet] PRIMARY KEY CLUSTERED ([MarketPlanAdjustmentSetID] ASC),
    CONSTRAINT [FK_tMarketPlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMarketPlanAdjustmentSet_tForecastSet] FOREIGN KEY ([BaseForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMarketPlanAdjustmentSet';

