CREATE TABLE [AdaptivePlanning].[tGenerationPlanAdjustmentScenarioMap] (
    [GenerationPlanAdjustmentScenarioMapID] INT IDENTITY (1, 1) NOT NULL,
    [GenerationPlanAdjustmentID]            INT NOT NULL,
    [ScenarioID]                            INT NOT NULL,
    CONSTRAINT [PK_tGenerationPlanAdjustmentScenarioMap] PRIMARY KEY CLUSTERED ([GenerationPlanAdjustmentScenarioMapID] ASC),
    CONSTRAINT [FK_tGenerationPlanAdjustmentScenarioMap_tGenerationPlanAdjustment] FOREIGN KEY ([GenerationPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tGenerationPlanAdjustment] ([GenerationPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tGenerationPlanAdjustmentScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);



  



GO
CREATE NONCLUSTERED INDEX IDX_tGenerationPlanAdjustmentScenarioMap_1
ON AdaptivePlanning.tGenerationPlanAdjustmentScenarioMap (GenerationPlanAdjustmentID,ScenarioID)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tGenerationPlanAdjustmentScenarioMap';

