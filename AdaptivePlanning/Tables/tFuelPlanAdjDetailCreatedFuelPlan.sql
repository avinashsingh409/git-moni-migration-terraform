CREATE TABLE [AdaptivePlanning].[tFuelPlanAdjDetailCreatedFuelPlan] (
    [FuelPlanAdjDetailCreatedFuelPlanID] INT IDENTITY (1, 1) NOT NULL,
    [FuelPlanAdjustmentDetailID]         INT NOT NULL,
    [FuelPlanSetID]                      INT NOT NULL,
    CONSTRAINT [PK_FuelPlanAdjDetailCreatedFuelPlan] PRIMARY KEY CLUSTERED ([FuelPlanAdjDetailCreatedFuelPlanID] ASC),
    CONSTRAINT [FK_tFuelPlanAdjDetailCreatedFuelPlan_tFuelPlanAdjustmentDetail] FOREIGN KEY ([FuelPlanAdjustmentDetailID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanAdjDetailCreatedFuelPlan_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID])
);



 
GO


GO


GO

 
	
GO

 
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tFuelPlanAdjDetailCreatedFuelPlan';

