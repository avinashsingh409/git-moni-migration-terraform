-- Create unit plan adjustment tables

CREATE TABLE [AdaptivePlanning].[tUnitPlanAdjustmentSet] (
    [UnitPlanAdjustmentSetID]     INT            IDENTITY (1, 1) NOT NULL,
    [Description]                 NVARCHAR (255) NOT NULL,
    [AnalysisSetID]               INT            NOT NULL,
    [BaseUnitConfigScheduleSetID] INT            NOT NULL,
    [CreatedBy]                   NVARCHAR (255) NOT NULL,
    [ChangedBy]                   NVARCHAR (255) NOT NULL,
    [CreateDate]                  DATETIME       NOT NULL,
    [ChangeDate]                  DATETIME       NOT NULL,
    CONSTRAINT [PK_tUnitPlanAdjustmentSet] PRIMARY KEY CLUSTERED ([UnitPlanAdjustmentSetID] ASC),
    CONSTRAINT [FK_tUnitPlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitPlanAdjustmentSet_tUnitConfigScheduleSet] FOREIGN KEY ([BaseUnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tUnitPlanAdjustmentSet';

