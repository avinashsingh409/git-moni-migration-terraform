CREATE TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentUnitMap] (
    [MaintenancePlanAdjustmentUnitMapID] INT IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanAdjustmentID]        INT NOT NULL,
    [UnitID]                             INT NOT NULL,
    CONSTRAINT [PK_tMaintenancePlanAdjustmentUnitMap] PRIMARY KEY CLUSTERED ([MaintenancePlanAdjustmentUnitMapID] ASC),
    CONSTRAINT [FK_tMaintenancePlanAdjustmentSetUnitMap_tMaintenancePlanAdjustment] FOREIGN KEY ([MaintenancePlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tMaintenancePlanAdjustment] ([MaintenancePlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMaintenancePlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMaintenancePlanAdjustmentUnitMap';

