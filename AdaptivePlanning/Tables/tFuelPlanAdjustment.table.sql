CREATE TABLE [AdaptivePlanning].[tFuelPlanAdjustment] (
    [FuelPlanAdjustmentID]    INT            IDENTITY (1, 1) NOT NULL,
    [Description]             NVARCHAR (255) NOT NULL,
    [FuelPlanAdjustmentSetID] INT            NOT NULL,
    [StartDate]               DATETIME       NULL,
    [EndDate]                 DATETIME       NULL,
    [IsWeekDayOnPeak]         BIT            CONSTRAINT [DF_tFuelPlanAdjustment_IsWeekDayOnPeak] DEFAULT ((0)) NOT NULL,
    [IsWeekDayOffPeak]        BIT            CONSTRAINT [DF_tFuelPlanAdjustment_IsWeekDayOffPeak] DEFAULT ((0)) NOT NULL,
    [IsWeekEndOnPeak]         BIT            CONSTRAINT [DF_tFuelPlanAdjustment_IsWeekEndOnPeak] DEFAULT ((0)) NOT NULL,
    [IsWeekEndOffPeak]        BIT            CONSTRAINT [DF_tFuelPlanAdjustment_IsWeekEndOffPeak] DEFAULT ((0)) NOT NULL,
    [CreatedBy]               NVARCHAR (255) NOT NULL,
    [ChangedBy]               NVARCHAR (255) NOT NULL,
    [CreateDate]              DATETIME       CONSTRAINT [DF_tFuelPlanAdjustment_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]              DATETIME       CONSTRAINT [DF_tFuelPlanAdjustment_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tFuelPlanAdjustment] PRIMARY KEY CLUSTERED ([FuelPlanAdjustmentID] ASC),
    CONSTRAINT [FK_tFuelPlanAdjustment_tFuelPlanAdjustmentSe] FOREIGN KEY ([FuelPlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustmentSet] ([FuelPlanAdjustmentSetID]) ON DELETE CASCADE
);







GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tFuelPlanAdjustment';

