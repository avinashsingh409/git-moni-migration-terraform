CREATE TABLE [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] (
    [FuelPlanAdjustmentUnitMapID] INT IDENTITY (1, 1) NOT NULL,
    [FuelPlanAdjustmentID]        INT NOT NULL,
    [UnitID]                      INT NOT NULL,
    CONSTRAINT [PK_tFuelPlanAdjustmentUnitMap] PRIMARY KEY CLUSTERED ([FuelPlanAdjustmentUnitMapID] ASC),
    CONSTRAINT [FK_tFuelPlanAdjustmentSetUnitMap_tFuelPlanAdjustment] FOREIGN KEY ([FuelPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tFuelPlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tFuelPlanAdjustmentUnitMap';

