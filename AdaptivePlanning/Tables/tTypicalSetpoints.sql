CREATE TABLE [AdaptivePlanning].[tTypicalSetpoints] (
    [Id]                       INT        IDENTITY (1, 1) NOT NULL,
    [AssetID]                  INT        NOT NULL,
    [UnitEquipmentStateTypeID] INT        NOT NULL,
    [Usage]                    INT        DEFAULT ((0)) NOT NULL,
    [Value]                    FLOAT (53) DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tTypicalSetpoints_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [FK_tTypicalSetpoints_tUnitEquipmentStateType] FOREIGN KEY ([UnitEquipmentStateTypeID]) REFERENCES [Projection].[tUnitEquipmentStateType] ([UnitEquipmentStateTypeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tTypicalSetpoints';

