CREATE TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] (
    [UnitPlanAdjustmentDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [UnitPlanAdjustmentID]       INT            NOT NULL,
    [UnitID]                     INT            NOT NULL,
    [UnitEquipmentStateTypeID]   INT            NOT NULL,
    [NewValue]                   FLOAT (53)     NULL,
    [IsAnnuallyRecurring]        BIT            NULL,
    [CreatedBy]                  NVARCHAR (255) NOT NULL,
    [ChangedBy]                  NVARCHAR (255) NOT NULL,
    [CreateDate]                 DATETIME       CONSTRAINT [DF_tUnitPlanAdjustmentDetail_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                 DATETIME       CONSTRAINT [DF_tUnitPlanAdjustmentDetail_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tUnitPlanAdjustmentDetail] PRIMARY KEY CLUSTERED ([UnitPlanAdjustmentDetailID] ASC),
    CONSTRAINT [FK_tUnitPlanAdjustmentDetail_ttUnitEquipmentStateType] FOREIGN KEY ([UnitEquipmentStateTypeID]) REFERENCES [Projection].[tUnitEquipmentStateType] ([UnitEquipmentStateTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitPlanAdjustmentDetail_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitPlanAdjustmentDetail_tUnitPlanAdjustment] FOREIGN KEY ([UnitPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustment] ([UnitPlanAdjustmentID]) ON DELETE CASCADE
);



  



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tUnitPlanAdjustmentDetail';

