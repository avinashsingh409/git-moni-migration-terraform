CREATE TABLE [AdaptivePlanning].[tMaintenancePlanAdjustment] (
    [MaintenancePlanAdjustmentID]    INT            IDENTITY (1, 1) NOT NULL,
    [Description]                    NVARCHAR (255) NOT NULL,
    [MaintenancePlanAdjustmentSetID] INT            NOT NULL,
    [StartDate]                      DATETIME       NULL,
    [EndDate]                        DATETIME       NULL,
    [CreatedBy]                      NVARCHAR (255) NOT NULL,
    [ChangedBy]                      NVARCHAR (255) NOT NULL,
    [CreateDate]                     DATETIME       CONSTRAINT [DF_tMaintenancePlanAdjustment_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                     DATETIME       CONSTRAINT [DF_tMaintenancePlanAdjustment_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tMaintenancePlanAdjustment] PRIMARY KEY CLUSTERED ([MaintenancePlanAdjustmentID] ASC),
    CONSTRAINT [FK_tMaintenancePlanAdjustment_tMaintenancePlanAdjustmentSe] FOREIGN KEY ([MaintenancePlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tMaintenancePlanAdjustmentSet] ([MaintenancePlanAdjustmentSetID]) ON DELETE CASCADE
);



 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMaintenancePlanAdjustment';

