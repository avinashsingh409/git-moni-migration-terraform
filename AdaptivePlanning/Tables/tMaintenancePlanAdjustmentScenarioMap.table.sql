CREATE TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentScenarioMap] (
    [MaintenancePlanAdjustmentScenarioMapID] INT IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanAdjustmentID]            INT NOT NULL,
    [ScenarioID]                             INT NOT NULL,
    CONSTRAINT [PK_tMaintenancePlanAdjustmentScenarioMap] PRIMARY KEY CLUSTERED ([MaintenancePlanAdjustmentScenarioMapID] ASC),
    CONSTRAINT [FK_tMaintenancePlanAdjustmentScenarioMap_tMaintenancePlanAdjustment] FOREIGN KEY ([MaintenancePlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tMaintenancePlanAdjustment] ([MaintenancePlanAdjustmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMaintenancePlanAdjustmentScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);



  



GO
CREATE NONCLUSTERED INDEX IDX_tMaintenancePlanAdjustmentScenarioMap_1
ON AdaptivePlanning.tMaintenancePlanAdjustmentScenarioMap (MaintenancePlanAdjustmentID,ScenarioID)
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AdaptivePlanning', @level1type = N'TABLE', @level1name = N'tMaintenancePlanAdjustmentScenarioMap';

