CREATE TABLE [CurrentPerformance].[tCLEventLogItem] (
    [CLEventLogItemID]          INT            IDENTITY (1, 1) NOT NULL,
    [TimeStamp]                 DATETIME       CONSTRAINT [DF_CLEventLogItem_TimeStamp] DEFAULT (getdate()) NOT NULL,
    [AssetVariableTypeTagMapID] INT            NOT NULL,
    [OperatorInitials]          VARCHAR (255)  NOT NULL,
    [CreatedBy]                 VARCHAR (255)  NOT NULL,
    [CLEventLogItemStatusID]    INT            NOT NULL,
    [Comment]                   NVARCHAR (MAX) NULL,
    [ShortTermIssue]            BIT            CONSTRAINT [DF_CLEventLogItem_ShortTermIssue] DEFAULT ((0)) NOT NULL,
    [ActualLossValue]           FLOAT (53)     NULL,
    [ExpectedLossValue]         FLOAT (53)     NULL,
    CONSTRAINT [PK_CLEventLogItem] PRIMARY KEY CLUSTERED ([CLEventLogItemID] ASC),
    CONSTRAINT [UK_CLEventLogItem_AssetVariableTypeTagMapID_AssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]),
    CONSTRAINT [UK_CLEventLogItem_CLEventLogItemStatusID_CLEventLogItemStatusID] FOREIGN KEY ([CLEventLogItemStatusID]) REFERENCES [CurrentPerformance].[tCLEventLogItemStatus] ([CLEventLogItemStatusID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CurrentPerformance', @level1type = N'TABLE', @level1name = N'tCLEventLogItem';

