CREATE TABLE [CurrentPerformance].[tCLEventLogItemStatus] (
    [CLEventLogItemStatusID] INT           IDENTITY (1, 1) NOT NULL,
    [StatusDesc]             VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_CLEventLogItemStatus] PRIMARY KEY CLUSTERED ([CLEventLogItemStatusID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CurrentPerformance', @level1type = N'TABLE', @level1name = N'tCLEventLogItemStatus';

