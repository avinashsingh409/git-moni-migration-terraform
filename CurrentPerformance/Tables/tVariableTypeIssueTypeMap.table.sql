CREATE TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] (
    [VariableTypeIssueTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [VariableTypeID]             INT NOT NULL,
    [ValueTypeID]                INT NULL,
    [IssueTypeID]                INT NOT NULL,
    CONSTRAINT [PK_tVariableTypeIssueTypeMap] PRIMARY KEY CLUSTERED ([VariableTypeIssueTypeMapID] ASC),
    CONSTRAINT [FK_tVariableTypeIssueTypeMap_IssueTypeID_tIssueType] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID]),
    CONSTRAINT [FK_tVariableTypeIssueTypeMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tVariableTypeIssueTypeMap_VariableTypeID_tVariableTypeID] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [UK_tVariableTypeIssueTypeMap_VariableTypeID_ValueTYpeID_IssueTypeID] UNIQUE NONCLUSTERED ([VariableTypeID] ASC, [ValueTypeID] ASC, [IssueTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'CurrentPerformance', @level1type = N'TABLE', @level1name = N'tVariableTypeIssueTypeMap';

