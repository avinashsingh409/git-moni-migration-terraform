CREATE TABLE [CurrentPerformance].[tCPNavTreeVariableMap] (
    [PDNavTreeVariableMapID] INT            IDENTITY (1, 1) NOT NULL,
    [PDNavTreeID]            INT            NULL,
    [PDVariableID]           INT            NOT NULL,
    [DisplayContext]         NVARCHAR (255) NULL,
    [ValueTypeID]            INT            NULL,
    [Direction]              INT            NULL,
    CONSTRAINT [PK_tPDNavTreeVariableMap] PRIMARY KEY CLUSTERED ([PDNavTreeVariableMapID] ASC),
    CONSTRAINT [FK_tCPNavTreeVariableMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tPDNavTreeVariableMap_PDNavTreeID_tPDNavTree] FOREIGN KEY ([PDNavTreeID]) REFERENCES [CurrentPerformance].[tCPNavTree] ([PDNavTreeID]),
    CONSTRAINT [FK_tPDNavTreeVariableMap_PDVariableID_tPDVariable] FOREIGN KEY ([PDVariableID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [UK_tPDNavTreeVariableMap_PDNavTreeVariableMapID_PDVariableID] UNIQUE NONCLUSTERED ([PDNavTreeVariableMapID] ASC, [PDVariableID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CurrentPerformance', @level1type = N'TABLE', @level1name = N'tCPNavTreeVariableMap';

