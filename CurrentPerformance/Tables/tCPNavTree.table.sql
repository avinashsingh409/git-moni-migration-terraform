CREATE TABLE [CurrentPerformance].[tCPNavTree] (
    [PDNavTreeID]       INT            IDENTITY (1, 1) NOT NULL,
    [ParentPDNavTreeID] INT            NOT NULL,
    [PDTrendID]         INT            NULL,
    [DisplayOrder]      INT            CONSTRAINT [DF_tPDNavTree_DisplayOrder] DEFAULT ((0)) NOT NULL,
    [DisplayText]       NVARCHAR (255) NOT NULL,
    [IsLossNode]        BIT            CONSTRAINT [DF_tPDNavTree_IsLossNode] DEFAULT ((0)) NOT NULL,
    [IsPerformanceNode] BIT            CONSTRAINT [DF_tPDNavTree_IsPerformanceNode] DEFAULT ((0)) NOT NULL,
    [IsComparisonNode]  BIT            CONSTRAINT [DF_tPDNavTree_IsComparisonNode] DEFAULT ((0)) NOT NULL,
    [IsCustomNode]      BIT            CONSTRAINT [DF_tPDNavTree_IsCustomNode] DEFAULT ((0)) NOT NULL,
    [IsSystemGroupNode] BIT            CONSTRAINT [DF_tPDNavTree_IsSystemGroupNode] DEFAULT ((0)) NOT NULL,
    [CreatedBy]         NVARCHAR (255) NOT NULL,
    [ChangedBy]         NVARCHAR (255) NOT NULL,
    [CreateDate]        DATETIME       CONSTRAINT [DF__tPDNavTree__CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]        DATETIME       CONSTRAINT [DF__tPDNavTree__ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tPDNavTree] PRIMARY KEY CLUSTERED ([PDNavTreeID] ASC),
    CONSTRAINT [FK_tPDNavTree_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CurrentPerformance', @level1type = N'TABLE', @level1name = N'tCPNavTree';

