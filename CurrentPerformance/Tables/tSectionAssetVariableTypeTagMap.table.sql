CREATE TABLE [CurrentPerformance].[tSectionAssetVariableTypeTagMap] (
    [SectionAssetVariableTypeTagMapID] INT IDENTITY (1, 1) NOT NULL,
    [SectionAssetID]                   INT NOT NULL,
    [AssetVariableTypeTagMapID]        INT NOT NULL,
    [DisplayOrder]                     INT CONSTRAINT [DF_tSectionAssetVariableTypeTagMap_DisplayOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tSectionAssetVariableTypeTagMap] PRIMARY KEY CLUSTERED ([SectionAssetVariableTypeTagMapID] ASC),
    CONSTRAINT [FK_tSectionAssetVariableTypeTagMap_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]),
    CONSTRAINT [FK_tSectionAssetVariableTypeTagMap_SectionAssetID_AssetID_tAsset] FOREIGN KEY ([SectionAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [UK_tSectionAssetVariableTypeTagMap_SectionAssetID_AssetVariableTypeTagMapID] UNIQUE NONCLUSTERED ([SectionAssetID] ASC, [AssetVariableTypeTagMapID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CurrentPerformance', @level1type = N'TABLE', @level1name = N'tSectionAssetVariableTypeTagMap';

