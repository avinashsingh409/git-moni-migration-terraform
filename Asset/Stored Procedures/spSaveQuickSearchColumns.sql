﻿CREATE PROCEDURE [Asset].[spSaveQuickSearchColumns]  
	@QuickSearchID as INT,
	@QuickSearchColumns [Asset].[tpQuickSearchColumn] READONLY
AS
BEGIN
	DELETE FROM [Asset].[tQuickSearchColumn] WHERE [QuickSearchID] = @QuickSearchID;
	
	INSERT INTO [Asset].[tQuickSearchColumn] (
	[QuickSearchID],
	[Title],
	[DisplayOrder],
	[AttributeTypeID], 
	[ScheduleID],
	[ScheduleType],
	[EventAssetClassTypeID],
	[EventAttributeTypeID],
	[Sort],
	[IsBuiltin],
	[FilterString],
	[DateFilterMin],
	[DateFilterMax],
	[ProcessDataVariableID],
	[ProcessDataValueTypeID], 
	[Width])

	SELECT 	[QuickSearchID],
	[Title],
	[DisplayOrder],
	[AttributeTypeID], 
	[ScheduleID],
	[ScheduleType],
	[EventAssetClassTypeID],
	[EventAttributeTypeID],
	[Sort],
	[IsBuiltin],
	[FilterString],
	[DateFilterMin],
	[DateFilterMax],
	[ProcessDataVariableID],
	[ProcessDataValueTypeID], 
	[Width]
	FROM @QuickSearchColumns
	RETURN;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveQuickSearchColumns]  TO [TEUser]
    AS [dbo];
GO
