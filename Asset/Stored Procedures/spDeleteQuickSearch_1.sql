﻿

CREATE PROCEDURE [Asset].[spDeleteQuickSearch]
	@QuickSearchId int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM Asset.tQuickSearchCategoryMap WHERE QuickSearchID = @QuickSearchId;
	DELETE FROM [Asset].[tQuickSearch] WHERE QuickSearchId = @QuickSearchId;
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spDeleteQuickSearch] TO [TEUser]
    AS [dbo];

