﻿----------------------------------------------------------
CREATE PROCEDURE [Asset].[spGetAvailableAssetAttributes] 
(	
	@securityUserID as int = NULL,	
    @assetID as int = NULL
)
AS
BEGIN
SELECT DISTINCT b.AttributeTypeID, b.AttributeTypeDesc,  b.AttributeTypeKey, b.EngrUnits, b.DisplayFormat,
		b.IsStandard, b.IsExempt, b.ClientID 		
		FROM [Asset].[ufnGetAvailableAssetAttributesIDs](@securityUserID, @assetID,1) a JOIN Asset.tAttributeType b 
		ON a.AttributeTypeID = b.AttributeTypeID
END

GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spGetAvailableAssetAttributes] TO [TEUser]
    AS [dbo];

