﻿CREATE PROCEDURE [Asset].[spSaveAsset] 
	-- Add the parameters for the stored procedure here
	@SecurityUserID INT,
	@AssetID INT = NULL,
	@ParentAssetID INT,
	@AssetAbbrev NVARCHAR(50),
	@AssetDesc NVARCHAR(MAX),
	@AssetClassTypeID INT,
	@AssetTreeNodeChildConfigurationTypeID INT = NULL,
	@DisplayOrder INT,
	@Date DATETIME = NULL,
	@IsHidden BIT,
	@NewAssetID INT OUTPUT,
	@NewGlobalID UNIQUEIDENTIFIER OUTPUT,
	@Track BIT
AS
BEGIN

	DECLARE @user NVARCHAR(MAX) = NULL;
	SELECT TOP 1 @user = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID; 
	IF @user IS NULL
	BEGIN
		RAISERROR('User does not exist',16,1)
	END

	IF @ParentAssetID IS NOT NULL AND @ParentAssetID>0 AND  Asset.ufnDoesUserHaveAccessToAsset(@ParentAssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User cannot add child to asset',16, 1)
	END

	IF @AssetID IS NOT NULL AND Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
	BEGIN
		RAISERROR( 'User cannot access asset',16, 1);
	END
	
	IF @Date IS NULL
		SET @Date = SYSDATETIME();

	SET NOCOUNT ON;
	IF @AssetID IS NULL 
	BEGIN
		SET @NewGlobalID = NEWID();
		
		INSERT INTO [Asset].[tAsset]
           ([ParentAssetID]
           ,[AssetAbbrev]
           ,[AssetDesc]
           ,[AssetClassTypeID]
           ,[AssetTreeNodeChildConfigurationTypeID]
           ,[DisplayOrder]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[GlobalID]
           ,[IsHidden]
		   ,[Track])
     VALUES
           (@ParentAssetID
           ,@AssetAbbrev
           ,@AssetDesc
           ,@AssetClassTypeID
           ,@AssetTreeNodeChildConfigurationTypeID
           ,@DisplayOrder
           ,@user
           ,@user
           ,@Date
           ,@Date
           ,@NewGlobalID
           ,@IsHidden
		   ,@Track)
		   SET @NewAssetID = (SELECT SCOPE_IDENTITY()); 
	END
	ELSE
	BEGIN
		UPDATE Asset.tAsset
		SET 
			ChangeDate = @Date,
			ChangedBy = @user,
			ParentAssetID = @ParentAssetID,
			AssetAbbrev = @AssetAbbrev,
			AssetDesc = @AssetDesc,
			AssetClassTypeID = @AssetClassTypeID,
			AssetTreeNodeChildConfigurationTypeID = @AssetTreeNodeChildConfigurationTypeID,
			DisplayOrder = @DisplayOrder,
			IsHidden = @IsHidden,
			Track = @Track
		WHERE AssetID = @AssetID;

		SELECT TOP 1  @NewGlobalID = GlobalID FROM Asset.tAsset WHERE AssetID = @AssetID;
		SET @NewAssetID = @AssetID;
	END
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveAsset] TO [TEUser]
    AS [dbo];