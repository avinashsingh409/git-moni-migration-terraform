﻿CREATE PROCEDURE [Asset].[spSetAssetParent]
(
	@assetID INT,
	@parentAssetID INT,
	@securityUserID INT
)
AS
BEGIN
	DECLARE @assetClassType AS INT = (SELECT AssetClassTypeID FROM Asset.tAsset where AssetID = @assetID)
	DECLARE @parentAssetClassType AS INT = (SELECT AssetClassTypeID FROM Asset.tAsset where AssetID = @parentAssetID)
	DECLARE @assetType AS NVARCHAR(255) = (SELECT c.AssetTypeDesc FROM Asset.tAsset a JOIN Asset.tAssetClassType b ON a.AssetClassTypeID = b.AssetClassTypeID
										JOIN Asset.tAssetType c ON b.AssetTypeID = c.AssetTypeID where AssetID = @assetID)

	IF (@assetClassType IS NULL)
	BEGIN
		RAISERROR('Asset does not exist', 13, -1, -1)
		RETURN
	END
	ELSE IF (@parentAssetClassType IS NULL)
	BEGIN
		RAISERROR('Parent asset does not exist', 13, -1, -1)
		RETURN
	END
	ELSE IF (Asset.ufnDoesUserHaveAccessToAsset(@assetID, @securityUserID,-1) = 0)
	BEGIN
		RAISERROR('User does not have access to asset', 13, -1, -1);
		RETURN
	END
	ELSE IF (Asset.ufnDoesUserHaveAccessToAsset(@parentAssetID, @securityUserID,-1) = 0)
	BEGIN
		RAISERROR('User does not have access to new parent asset', 13, -1, -1)
		RETURN
	END
	BEGIN TRY
		BEGIN TRANSACTION
		IF(@assetType = 'Event')
		BEGIN
			--More than 1 event group may exist under the parent asset.
			--If we're updating an event, make sure we get the right parent. Otherwise just take the first one.
			DECLARE @UpsertedEventGroupID AS INT
			SELECT TOP 1 @UpsertedEventGroupID = eventGroups.AssetID
			FROM Asset.tAsset eventGroups
			INNER JOIN Asset.tAsset eventAssets on eventAssets.ParentAssetID = eventGroups.AssetID
			WHERE eventGroups.ParentAssetID = @parentAssetID AND eventGroups.AssetClassTypeID = 7002
			AND (ISNULL(@assetID, -1) <= 0 OR eventAssets.AssetID = @assetID)

			--if we didn't find the EventGroup, make one.
			IF @UpsertedEventGroupID IS NULL
			BEGIN
				EXEC Asset.spSaveAsset @SecurityUserID, NULL, @parentAssetID, 'Events','Events',7002,NULL,100,NULL,0,@UpsertedEventGroupID OUTPUT, NULL, 0;
			END
			UPDATE Asset.tAsset SET ParentAssetID = @UpsertedEventGroupID WHERE AssetID = @assetID;	
		END
		ELSE IF(@assetType = 'Scenario')
		BEGIN
			--More than 1 scenario group may exist under the parent asset.
			--If we're updating an scenario, make sure we get the right parent. Otherwise just take the first one.
			DECLARE @UpsertedScenarioGroupID AS INT
			SELECT TOP 1 @UpsertedScenarioGroupID = scenarioGroups.AssetID
			FROM Asset.tAsset scenarioGroups
			INNER JOIN Asset.tAsset scenarioAssets on scenarioAssets.ParentAssetID = scenarioGroups.AssetID
			WHERE scenarioGroups.ParentAssetID = @parentAssetID AND scenarioGroups.AssetClassTypeID = 7001
			AND (ISNULL(@assetID, -1) <= 0 OR scenarioAssets.AssetID = @assetID)

			--if we didn't find the ScenarioGroup, make one.
			IF @UpsertedScenarioGroupID IS NULL
			BEGIN
				EXEC Asset.spSaveAsset @SecurityUserID, NULL, @parentAssetID, 'Scenarios','Scenarios',7001,NULL,100,NULL,0,@UpsertedScenarioGroupID OUTPUT, NULL, 0;
			END
			UPDATE Asset.tAsset SET ParentAssetID = @UpsertedScenarioGroupID WHERE AssetID = @assetID;	
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT * FROM Asset.tAssetClassTypeAssetClassTypeMap WHERE ChildAssetClassTypeId = @assetClassType AND ParentAssetClassTypeID = @parentAssetClassType)
			BEGIN
				RAISERROR(N'VIOLATION OF tAsset business rule: AssetClassTypeID: %d cannot be a parent of AssetClassTypeID: %d.', 16, -1, @parentAssetClassType, @assetClassType);					
				RETURN
			END 
			UPDATE Asset.tAsset SET ParentAssetID = @parentAssetID WHERE AssetID = @assetID;		
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spSetAssetParent] TO [TEUser]
    AS [dbo];

