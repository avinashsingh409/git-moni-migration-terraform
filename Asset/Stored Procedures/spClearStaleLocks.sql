﻿CREATE PROCEDURE [Asset].[spClearStaleLocks]
@MaxLockDuration_Minutes int = 120 -- clear a lock if the lock has been in place for more than 2 hours
AS 

BEGIN
    DECLARE @lockGroup uniqueidentifier

    DECLARE curUP CURSOR LOCAL	
			FOR SELECT LockGroup FROM Asset.tClient where datediff(minute, lockedsincetime, GETDATE()) > @MaxLockDuration_Minutes
	OPEN curUP;
	FETCH NEXT FROM curUP INTO @lockGroup

	DECLARE @OK AS INT = 1
    
	WHILE @OK = 1 
		BEGIN			
			IF @@FETCH_STATUS =  0
				BEGIN
				EXEC Asset.spUnlockGroup @LockGroup
				FETCH NEXT FROM curUP INTO @LockGroup
				END
			ELSE
				BEGIN
				SET @OK = 0
				END	  

		END

	CLOSE curUP;
	DEALLOCATE curUP;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spClearStaleLocks] TO [TEUser]
    AS [dbo];
