﻿
CREATE PROCEDURE [Asset].[spUpsertAssetAttribute]
--provide AssetID and either Name or AttributeTypeID
	@SecurityUserID INT = 0,
	@AssetID INT = 0,
	@AttributeTypeID INT = NULL,
	@Name NVARCHAR(255) = '',
	@Value NVARCHAR(MAX) = NULL,
	@Favorite BIT = 0,
	@DisplayOrder INT = 10,
	--For date attributes, will only use @Date if NULL or > '1900-01-01T00:00:00+00:00'. Otherwise, will attempt to parse @Value.
	@Date DATETIMEOFFSET = '1900-01-01T00:00:00+00:00',
	@IncludeCategories BIT = 1,
	@CategoryIDs Base.tpIntList READONLY,
	@NewDisplayFormat NVARCHAR(100) = 'string',
	@NewID INT OUTPUT,
	@NewTypeID INT OUTPUT
AS
BEGIN
DECLARE @ClientID int = NULL
DECLARE @AssetClassTypeID int = NULL
DECLARE @IsExempt bit = 0
DECLARE @IsStandard bit = 0
DECLARE @DisplayFormat nvarchar(100)
DECLARE @Attribute_int int = NULL
DECLARE @Attribute_string nvarchar(max) = NULL
DECLARE @Attribute_float real = NULL
DECLARE @AttributeOptionTypeID int = NULL
DECLARE @Attribute_date DATETIMEOFFSET = NULL
DECLARE @ValueIsNumeric bit = 0
--prevent parameter sniffing
DECLARE @tmpValue NVARCHAR(MAX) = @Value;

SET @NewID = -1
IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@SecurityUserID,-1) = 0
  BEGIN
    RAISERROR ('Error - user does not have rights to this asset.', 13, -1, -1)
    RETURN -1
  END

IF NOT EXISTS(SELECT IsValid FROM Asset.ufnAttributeIsValidForUser(@SecurityUserID,@AssetID,@AttributeTypeID))                   --checks categories/role assignments for all attributes
  BEGIN
    RAISERROR ('Error - user does not have rights to this attribute.', 13, -1, -1)
    RETURN -1
  END

IF @AttributeTypeID IS NOT NULL
  BEGIN
	SELECT @IsExempt = IsExempt, @IsStandard = IsStandard, @DisplayFormat = UPPER(DisplayFormat), @ClientID = ClientID, @Name = AttributeTypeDesc
	FROM Asset.tAttributeType WHERE AttributeTypeID = @AttributeTypeID
  END
ELSE
  BEGIN
	SET @ClientID = Asset.ufnGetClientForAsset(@AssetID);

	SELECT @AttributeTypeID = AttributeTypeID,@IsExempt = IsExempt, @IsStandard = IsStandard, @DisplayFormat = UPPER(DisplayFormat)
		FROM Asset.tAttributeType WHERE UPPER(AttributeTypeDesc) = UPPER(@Name) AND ((IsStandard = 1) OR (@ClientID = ClientID))
  END

IF @AttributeTypeID IS NULL OR @AttributeTypeID <= 0
  BEGIN
	SET @IsExempt = 0;
	SET @IsStandard = 0;
	SET @NewDisplayFormat = ISNULL(@NewDisplayFormat, 'string');
	SET @DisplayFormat = UPPER(@NewDisplayFormat);

	INSERT INTO Asset.tAttributeType (AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)
      VALUES (@Name, SUBSTRING(REPLACE(@Name,' ',''),0,99), NULL, @NewDisplayFormat, @IsStandard, @IsExempt, @ClientID)
	SELECT @AttributeTypeID = SCOPE_IDENTITY();
  END
ELSE IF ((@AttributeTypeID IS NOT NULL) AND (@IsExempt = 0) AND (@IsStandard = 1))
  BEGIN
  SELECT @AssetClassTypeID = AssetClassTypeID FROM Asset.tAsset WHERE AssetID = @AssetID
  IF NOT EXISTS(SELECT * FROM Asset.tAssetClassTypeAttributeTypeMap WHERE AssetClassTypeID = @AssetClassTypeID AND AttributeTypeID = @AttributeTypeID)
    BEGIN
	RAISERROR ('Error - that attribute/asset class type combination violates business rules.', 13, -1, -1)
    RETURN -1
	END
  END

SET @NewTypeID = @AttributeTypeID
SET @tmpValue = NULLIF(@tmpValue,'')
SET @ValueIsNumeric = ISNUMERIC(LEFT(@tmpValue,100))

IF @DisplayFormat='RADIO'
--If the type is radio then we need to try to find the value in the list of options for this attribute
--If it is not in the list of options, it might be the ID itself.  So try to cast it to an integer
  BEGIN
	SET @AttributeOptionTypeID = NULL;
	SELECT @AttributeOptionTypeID = AttributeOptionTypeID FROM Asset.tAttributeOptionType WHERE AttributeTypeID = @AttributeTypeID AND AttributeOptionTypeDesc = @tmpValue;
	IF @AttributeOptionTypeID IS NULL AND @tmpValue IS NOT NULL AND @tmpValue <> ''
	BEGIN
		SELECT @AttributeOptionTypeID = AttributeOptionTypeID FROM Asset.tAttributeOptionType WHERE AttributeTypeID = @AttributeTypeID AND AttributeOptionTypeID = @tmpValue;

	    IF @AttributeOptionTypeID IS NULL
		  BEGIN
			RAISERROR ('Error casting value for AttributeOptionTypeID.', 13, -1, -1)
			  RETURN -1
		  END
	END
  END
ELSE IF @DisplayFormat = 'INT'
  BEGIN
	IF @tmpValue IS NULL OR @tmpValue = ''
		SET @Attribute_Int = NULL
	ELSE IF @ValueIsNumeric = 0
      BEGIN
	    RAISERROR ('Error - value is not numeric or null for Attribute_int.', 13, -1, -1)
        RETURN -1
	  END
    ELSE
	  BEGIN
	    BEGIN TRY
		  SET @Attribute_int = CAST(@tmpValue AS int)
	    END TRY
	    BEGIN CATCH
		  RAISERROR ('Error casting value for Attribute_int.', 13, -1, -1)
		  RETURN -1
	    END CATCH
	  END
  END
ELSE IF SUBSTRING(@DisplayFormat,1,1) = 'F' OR SUBSTRING(@DisplayFormat,1,1) = 'P'
  BEGIN
	IF @tmpValue IS NULL OR @tmpValue = ''
		SET @Attribute_float = NULL
	ELSE IF @ValueIsNumeric = 0
      BEGIN
	    RAISERROR ('Error - value is not numeric or null for Attribute_float.', 13, -1, -1)
        RETURN -1
	  END
    ELSE
	  BEGIN
		BEGIN TRY
		  SET @Attribute_float = CAST(@tmpValue AS float)
	    END TRY
	    BEGIN CATCH
		  RAISERROR ('Error casting value for Attribute_float.', 13, -1, -1)
		  RETURN -1
	    END CATCH
	  END
  END
ELSE IF @DisplayFormat = 'BOOLEAN'
BEGIN
-- This is strictly not necessary because we should only be sending the value of true or false into this method, but
-- this is easy, doesnt take any extra time, and might prevent some potential errors in the future.
	IF @tmpValue = 'true' OR @tmpValue = '1' OR @tmpValue = 'yes' OR @tmpValue = 't' OR @tmpValue = 'y' OR @tmpValue = 'on'
		SET @Attribute_int = 1;
	ELSE IF @tmpValue IS NULL OR @tmpValue = ''
		SET @Attribute_int = NULL;
    ELSE
		SET @Attribute_int = 0;
END
ELSE IF @DisplayFormat = 'DATE'
BEGIN
-- In this case we need to convert the string passed in to a date.  We have to rely on the calling function to put the
-- date into the string in the right format.  We do need to make sure that no value is able to cause an exception
-- in the conversion.  If the date is not valid it should default to null.
	IF @Date IS NULL OR @Date > '1900-01-01T00:00:00+00:00'
		SET @Attribute_date = @Date
	ELSE IF @tmpValue IS NULL OR @tmpValue = ''
		SET @Attribute_date = NULL
	ELSE
		BEGIN TRY
			SET @Attribute_date = CONVERT(datetimeoffset, @tmpValue)
		END TRY
		BEGIN CATCH
			SET @Attribute_date = NULL
		END CATCH
END
ELSE
  BEGIN
    SET @Attribute_string = @tmpValue
  END

MERGE Asset.tAssetAttribute AS TARGET USING
  (SELECT @AssetID as AssetID, @AttributeTypeID as AttributeTypeID) AS SOURCE ON (TARGET.AssetID = SOURCE.AssetID
	AND TARGET.AttributeTypeID = SOURCE.AttributeTypeID)
WHEN MATCHED THEN UPDATE SET @NewID = Target.AssetAttributeID, Target.Attribute_Int = @Attribute_Int, Target.Attribute_string = @Attribute_string, Target.Attribute_float = @Attribute_float,
	Target.AttributeOptionTypeID = @AttributeOptionTypeID,Target.Attribute_date = @Attribute_date, Target.Favorite = @Favorite, Target.DisplayOrder = @DisplayOrder, Target.ChangeDate = GetDate(), Target.ChangedByUserID = @SecurityUserID
WHEN NOT MATCHED THEN INSERT (AssetID,AttributeTypeID,AttributeOptionTypeID,Attribute_int,Attribute_string, Attribute_float,Attribute_date,Favorite,DisplayOrder,CreatedByUserID,ChangedByUserID,CreateDate,ChangeDate)
	VALUES (@AssetID,@AttributeTypeID,@AttributeOptionTypeID,@Attribute_int,@Attribute_string,@Attribute_float,@Attribute_date, @Favorite,@DisplayOrder,@SecurityUserID,@SecurityUserID,Getdate(),Getdate());

IF @NewID = -1
BEGIN
	SET @NewID = SCOPE_IDENTITY()
END

IF (@IncludeCategories = 1)
  BEGIN
  EXEC [Asset].[spSetAssetAttributeCategories] @AttributeTypeID, @AssetID, @CategoryIDs
  END

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spUpsertAssetAttribute] TO [TEUser]
    AS [dbo];
GO

