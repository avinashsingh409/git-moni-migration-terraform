﻿CREATE PROCEDURE [Asset].[spCalculateAssetHops]
	 @deletedAssetIds Base.tpIntList READONLY,
	 @insertedAssetIds Base.tpIntList READONLY
AS
BEGIN
	 -- SET NOCOUNT ON added to prevent extra result sets from
	 -- interfering with SELECT statements.
	 SET NOCOUNT ON;

	 DECLARE @affectedAssetIds TABLE (
		  assetId int PRIMARY KEY NOT null
	 )

	 DECLARE @assetId int;
	 DECLARE @tmpAssetHop TABLE (
		  [HopAssetId] [int] NOT NULL,
		  [EndingAssetId] [int] NOT NULL,
		  [AncestryLevel] [int] NOT NULL
	 )

	 DECLARE @hopAssetIdsToDelete TABLE(
		  assetId int PRIMARY KEY NOT null
	 )

	 DECLARE @hopAssetIdsToInsert TABLE(
		  assetId int PRIMARY KEY NOT null
	 )

	 INSERT INTO @affectedAssetIds(assetId)
				SELECT distinct tah.EndingAssetId 
				FROM asset.tAssetHop tah 
				JOIN @deletedAssetIds ids ON tah.HopAssetId = ids.id
				LEFT JOIN @affectedAssetIds aai ON aai.assetId = tah.EndingAssetId				
				WHERE aai.assetId IS NULL

	 INSERT INTO @affectedAssetIds(assetId) select i.id from @deletedAssetIds i where 
	 NOT Exists(select * from Asset.tAssetHop h where i.id = HopAssetId)

	 INSERT INTO @hopAssetIdsToDelete(assetId)
		  SELECT aai.assetId
		  FROM @affectedAssetIds aai
		  LEFT JOIN @hopAssetIdsToDelete haitd ON aai.assetId = haitd.assetId
		  WHERE haitd.assetId IS NULL;

	 DECLARE @rowsAffected int;

	 DELETE FROM @affectedAssetIds

	 INSERT INTO @affectedAssetIds(assetId)
				SELECT distinct tah.EndingAssetId 
				FROM asset.tAssetHop tah 
				JOIN @insertedAssetIds ids ON tah.HopAssetId = ids.id
				LEFT JOIN @affectedAssetIds aai ON aai.assetId = tah.EndingAssetId				
				WHERE aai.assetId IS NULL

	 INSERT INTO @affectedAssetIds(assetId) select i.id from @insertedAssetIds i where 
	 NOT Exists(select * from Asset.tAssetHop h where i.id = HopAssetId)

	 INSERT INTO @hopAssetIdsToInsert(assetId)
		  SELECT aai.assetId
		  FROM @affectedAssetIds aai
		  LEFT JOIN @hopAssetIdsToInsert haitd ON aai.assetId = haitd.assetId
		  WHERE haitd.assetId IS NULL;
	 
	 WITH assets(assetid, parentassetid, levelsAway, endingAssetid)
		AS     (SELECT a.assetId AS n,b.ParentAssetID,0,a.assetId FROM @hopAssetIdsToInsert a join Asset.tAsset b on a.assetId = b.AssetID
			UNION ALL
				SELECT a.assetid, a.parentassetid, a2.levelsAway - 1,a2.endingAssetid
				FROM Asset.tasset a
				INNER JOIN assets a2 ON a2.parentassetid = a.assetid                
			)
	INSERT INTO @tmpAssetHop
		  (
				[HopAssetId] ,
				[EndingAssetId],
				[AncestryLevel]
		  )
	SELECT assetid, endingAssetid, levelsAway FROM   assets;

	 DELETE Asset.tAssetHop 
	 FROM Asset.tAssetHop tah
	 INNER JOIN @hopAssetIdsToDelete haitd ON tah.HopAssetId = haitd.assetId

	 DELETE Asset.tAssetHop 
	 FROM Asset.tAssetHop tah
	 INNER JOIN @hopAssetIdsToDelete haitd ON tah.endingassetid = haitd.assetId;
	 
	 INSERT INTO Asset.tAssetHop(HopAssetId, EndingAssetId, AncestryLevel)
		  SELECT [HopAssetId],[EndingAssetId],[AncestryLevel]
		  FROM @tmpAssetHop
		  
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spCalculateAssetHops] TO [TEUser]
    AS [dbo];

