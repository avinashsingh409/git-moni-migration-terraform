
CREATE PROCEDURE [Asset].[spUpdateAssets] 
	-- Add the parameters for the stored procedure here
	@SecurityUserID INT,
	@Date DATETIME = NULL,
	@assets [Asset].[tpAsset] READONLY
AS
BEGIN

	DECLARE @user NVARCHAR(MAX) = NULL;
	SELECT TOP 1 @user = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID; 
	IF @user IS NULL
	BEGIN
		RAISERROR('User does not exist',16,1)
	END

	IF @Date IS NULL
	BEGIN
		SET @Date = SYSDATETIME();
	END

	UPDATE
		t
	SET
		t.[ParentAssetID]  = s.ParentAssetID,
		t.[AssetAbbrev] = s.[AssetAbbrev],
		t.[AssetDesc] = s.[AssetDesc],
		t.[AssetClassTypeID] = s.[AssetClassTypeID],
		t.[AssetTreeNodeChildConfigurationTypeID] = s.[AssetTreeNodeChildConfigurationTypeID],
		t.[DisplayOrder] = s.[DisplayOrder],
		t.[IsHidden] = s.[IsHidden],
		t.[Track] = s.[Track],
		t.ChangedBy = @user,
		t.ChangeDate = @Date
	FROM 
		Asset.tAsset t
		INNER JOIN 	@assets AS s
		ON t.AssetID = s.AssetID

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spUpdateAssets] TO [TEUser]
    AS [dbo];

