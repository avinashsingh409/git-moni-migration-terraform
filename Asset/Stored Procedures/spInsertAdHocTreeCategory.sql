CREATE PROCEDURE [Asset].[spInsertAdHocTreeCategory]
  @SecurityUserID int,
  @ClientID int,
  @Name varchar(255),
  @CategoryTypeIDs Base.tpIntList READONLY,
  @ErrorIfExists bit = 1,
  @NewCategoryID int OUTPUT
AS
BEGIN

    SET @NewCategoryID = -1

	IF Not Exists(SELECT * FROM @CategoryTypeIDs)
	  BEGIN
	  RAISERROR ('Error - Category Types cannot be empty, cannot add.', 11,-1,-1)
	  RETURN
	  END

	IF EXISTS(SELECT * FROM @CategoryTypeIDs where id NOT IN (SELECT CategoryTypeID FROM Asset.tCategoryType))
	  BEGIN
	  RAISERROR ('Error - At least one category type is invalid, cannot add.', 11,-1,-1)
	  RETURN
	  END

	
	IF (@Name IS NULL OR REPLACE(@Name,' ', '') = '') BEGIN
		RAISERROR('CANNOT_SAVE:Empty strings are not allowed as categories. Save denied.', 16, 1)
		RETURN;
	END

	declare @CategoryID as int = null
	declare @ResultID table (CategoryID int)

	-- First look to see if this category is global
	select @CategoryID = CategoryID from Asset.tCategory WHERE ClientID is NULL AND Name = @Name

	IF @CategoryID IS NOT NULL
	  BEGIN
	  IF @ErrorIfExists = 1
	    BEGIN
	    RAISERROR ('Error - Category already exists, cannot add.', 11,-1,-1)
	    RETURN
		END
	  ELSE
	    BEGIN
		SET @NewCategoryID = @CategoryID
		RETURN
		END
	  END

	IF @ClientID IS NOT NULL
	  BEGIN
	  set @CategoryID = null
	  select @CategoryID = CategoryID from  Asset.tCategory Where Name = @Name and ClientID = @ClientID
	  IF @CategoryID is not null
		BEGIN
		-- Check if simply not in the category type mapping
	    DECLARE @missingIds As Base.tpIntList

	    INSERT INTO @missingIds SELECT ID from @CategoryTypeIDs WHERE ID NOT IN (SELECT CategoryTypeID FROM Asset.tCategoryTypeMap WHERE CategoryID = @CategoryID)

	    IF EXISTS(SELECT * FROM @missingIds)
	      BEGIN
		  SET @NewCategoryID = @CategoryID
		  INSERT INTO Asset.tCategoryTypeMap (CategoryID,CategoryTypeID) SELECT @CategoryID,ID FROM @missingIds
		  -- We're done here.  Category exists but just isn't associated with this type

   		  RETURN
		  END
	    ELSE
	      BEGIN
		  IF @ErrorIfExists = 1
	        BEGIN
		    RAISERROR ('Error - Category already exists, cannot add.', 11,-1,-1)
		    RETURN
			END
	     ELSE
		   BEGIN
		   SET @NewCategoryID = @CategoryID
		   RETURN
		   END
		  END
	    END

	  END
	ELSE
	  BEGIN
	  SET @ClientID = NULL -- global
	  END

	INSERT INTO Asset.tCategory (Name,CreatedByUserID,ChangedByUserID,ClientID) OUTPUT Inserted.CategoryID INTO @ResultID VALUES (@Name,@SecurityUserID,@SecurityUserID,@ClientID)
	INSERT INTO Asset.tCategoryTypeMap (CategoryID,CategoryTypeID) SELECT CategoryID,ID FROM @ResultID,@CategoryTypeIDs
	SELECT TOP 1 @NewCategoryID = CategoryID FROM @ResultID

END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spInsertAdHocTreeCategory] TO [TEUser]
    AS [dbo];
GO

