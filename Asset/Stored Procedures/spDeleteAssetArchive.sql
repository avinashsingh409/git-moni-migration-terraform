﻿CREATE PROCEDURE [Asset].[spDeleteAssetArchive]
  (@AssetArchiveID int)
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [Asset].[tAssetArchive] WHERE [AssetArchiveID]=@AssetArchiveID;
  SELECT @@ROWCOUNT as [Rows Affected];
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spDeleteAssetArchive] TO [TEUser]
    AS [dbo];
GO