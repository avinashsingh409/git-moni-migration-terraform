﻿CREATE PROCEDURE [Asset].[spSetAssetAttributeCategories]
  @AttributeTypeID as int,
  @AssetID int,
  @Categories Base.tpIntList READONLY
AS
BEGIN

-- First delete mappings to global categories
DELETE Asset.tAssetAttributeCategoryMap FROM Asset.tAssetAttributeCategoryMap a JOIN Asset.tCategory b on a.CategoryID = b.CategoryID 
WHERE a.AttributeTypeID = @AttributeTypeID AND b.ClientID IS NULL

-- DELETE Existing mappings where the attributetypeid is mapped to a category for a clientid that is in the ancestry of the asset
DELETE Asset.tAssetAttributeCategoryMap FROM Asset.tAssetAttributeCategoryMap a JOIN Asset.tCategory b on a.CategoryID = b.CategoryID JOIN Asset.ufnGetClientsForAsset(@AssetID) c ON b.ClientID = c.ClientID
WHERE a.AttributeTypeID = @AttributeTypeID

INSERT into Asset.tAssetAttributeCategoryMap (AttributeTypeID,CategoryID) SELECT @AttributeTypeID,ID FROM @Categories

END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSetAssetAttributeCategories]  TO [TEUser]
    AS [dbo];
GO
