﻿CREATE PROC [Asset].[spUpsertAssetAttributes]
(
	@UserID int,
	@Attributes AS Asset.tpUpsertAssetAttribute READONLY,
    @AddAssetDiscussionEntries BIT = 1,
	@deleteAllExistingAttributes BIT = 0,
	@errorMsg varchar(255) OUTPUT
)
AS

BEGIN

SET NOCOUNT ON

BEGIN TRY

begin transaction

	declare @assets as base.tpIntList
	insert into @assets select distinct assetid from @attributes

	IF EXISTS(SELECT * FROM @Attributes a WHERE a.AttributeTypeID NOT IN (SELECT AttributeTypeID FROM Asset.tAttributeType))
	  BEGIN
	  SET @errorMsg = 'At least one attribute was not found'
	  RAISERROR(@errorMsg, 13, 1)
	  RETURN -1
	  END

	IF EXISTS(SELECT * FROM @assets a WHERE a.ID NOT IN (SELECT AssetID FROM Asset.tAsset))
	  BEGIN
	  SET @errorMsg = 'At least one asset was not found'
	  RAISERROR(@errorMsg, 13, 1)
	  RETURN -1
	  END

-- upsert categories

	DECLARE @customAttributeCategories TABLE (
	    AttributeTypeID int NULL,
		AttributeName varchar(255) NULL,
		CategoryID    int          NULL,
		CategoryName  varchar(255) NULL
	);

	INSERT INTO @customAttributeCategories
		(AttributeTypeID,AttributeName, CategoryID, CategoryName)
	SELECT DISTINCT cac.AttributeTypeID, cac.Name, NULL, ufn.string
	FROM
		@Attributes cac
	CROSS APPLY [Base].[ufnSplitString](cac.Categories, ';') ufn;

	UPDATE a SET AttributeName = b.AttributeTypeDesc FROM @customAttributeCategories a
			JOIN Asset.tAttributeType b ON a.AttributeTypeID = b.AttributeTypeID
	DECLARE @firstImportedAssetID int
	SELECT TOP 1
		@firstImportedAssetID = AssetID
	FROM @Attributes where [Delete] =0

	DECLARE @categoryAttributeTypeID int
	SELECT TOP 1
		@categoryAttributeTypeID = CategoryTypeID
	FROM Asset.tCategoryType
	WHERE CategoryTypeName LIKE 'Attributes'

	DECLARE @categoryTypeIDs AS Base.tpIntList
	INSERT INTO @categoryTypeIDs
	VALUES
		(@categoryAttributeTypeID)

	DECLARE @restrictedValues TABLE (
		ID   int          NULL,
		Name varchar(255) NULL
		);

	DECLARE @categoryNameList AS Base.tpStringList
	INSERT INTO @categoryNameList
	SELECT DISTINCT CategoryName
	FROM @customAttributeCategories

	IF @firstImportedAssetID IS NOT NULL
	  BEGIN
	  INSERT INTO @restrictedValues
		([Name])
	  SELECT CategoryName
	    FROM Asset.ufnWhichCategoriesAreProhibited(@firstImportedAssetID, @userID, @categoryNameList, @categoryAttributeTypeID)


	  IF EXISTS(SELECT TOP 1
		  *
	  FROM @restrictedValues)
	  BEGIN
		  DECLARE @concatCategories varchar(MAX)
		  SELECT @concatCategories = COALESCE(@concatCategories + ', ', '') + [Name]
		  FROM (SELECT DISTINCT TOP 5
				  [Name]
			  FROM @restrictedValues) AS A
		  IF (SELECT COUNT(*)
		  FROM @restrictedValues) > 5
		  BEGIN
  			SET @concatCategories = @concatCategories +  ', ...'
		  END
		  SET @concatCategories = 'User does not have permission to import categories: ' + @concatCategories
		  RAISERROR(@concatCategories, 13, 1)
		  RETURN -1
	  END

	  DECLARE @attributeStringList AS Base.tpStringList
	  INSERT INTO @attributeStringList
	  SELECT DISTINCT [Name]
	    FROM @Attributes where [Delete] =0

	  INSERT INTO @restrictedValues
		([Name])
	  SELECT AttributeName
	    FROM Asset.ufnWhichAttributeNamesAreProhibited(@firstImportedAssetID, @userID, @attributeStringList)
	    OPTION (RECOMPILE)

	  IF EXISTS (SELECT TOP 1
		*
	  FROM @restrictedValues)
	    BEGIN
		DECLARE @concatAttributes varchar(MAX)
		SELECT @concatAttributes = COALESCE(@concatAttributes + ', ', '') + [Name]
		FROM (SELECT DISTINCT TOP 5
				[Name]
			FROM @restrictedValues) AS A
		IF (SELECT COUNT(*)
		FROM @restrictedValues) > 5
		BEGIN
			SET @concatAttributes = @concatAttributes + ', ...'
		END
		SET @concatAttributes = 'User does not have permission to import attributes: ' + @concatAttributes
		RAISERROR(@concatAttributes, 13, 1)
		RETURN -1
	    END

	  UPDATE ca
	  SET CategoryID = ufn.CategoryID
	  FROM @customAttributeCategories ca
		  INNER JOIN Asset.ufnGetAvailableAssetsCategories(@userID, @assets, @categoryAttributeTypeID) ufn
		  ON ufn.Name = ca.CategoryName

	  WHILE EXISTS(SELECT TOP 1
		*
	  FROM @customAttributeCategories
	  WHERE CategoryID IS NULL)
	    BEGIN
		DECLARE @categoryName varchar(255)
		DECLARE @categoryID int
		SELECT TOP 1
			@categoryName = CategoryName
		FROM @customAttributeCategories
		WHERE CategoryID IS NULL
		EXEC Asset.spInsertCategory @userID, @firstImportedAssetID, @categoryName, @categoryTypeIDs, 0, @categoryID OUTPUT
		UPDATE @customAttributeCategories SET CategoryID = @categoryID WHERE CategoryName = @categoryName
	    END
	END

	create table #validattributes
	(
		AssetID int,
		AttributeTypeID int,
		IsValid bit
	)

	CREATE INDEX tmp_idx_1 ON #validattributes (AssetID);

	CREATE INDEX tmp_idx_2 ON #validattributes (AttributeTypeID);

	insert into #validattributes select distinct A.AssetID, b.AttributeTypeID,0 from @Attributes a join Asset.tassetattribute b on a.AssetID = b.AssetID where [Delete] =0
	UPDATE a set IsValid = 1 FROM #validattributes a WHERE (SELECT IsValid from Asset.ufnAttributeIsValidForUser(@UserID, AssetID, AttributeTypeID)) = 1

	CREATE TABLE #newAttributes  (
		attributeName NVARCHAR(255) NULL,
		attributeTypeID INT NULL,
		attributeValue NVARCHAR(MAX) NOT NULL,
		displayOrder INT NULL,
		favorite BIT NULL,
		attributeID INT,
		assetID INT NOT NULL,
		IsValid bit NULL,
		displayFormat varchar(255),
		DiscreteOptions varchar(4000)
	)

	CREATE TABLE #deleteAttributes  (
		attributeName NVARCHAR(255) NULL,
		AssetID int NOT NULL,
		attributeTypeID INT NULL
	)

	CREATE INDEX IDX_1 ON #newAttributes(attributeName);
	CREATE INDEX IDX_2 ON #newAttributes(attributeTypeID);
	CREATE INDEX IDX_3 ON #newAttributes(assetID);
	CREATE INDEX IDX_4 ON #newAttributes(isValid);

	INSERT INTO #newAttributes(attributeName, attributeValue, displayOrder, favorite, assetID, attributeTypeID, displayFormat,DiscreteOptions)
	select a.Name,a.Value, a.DisplayOrder,a.Favorite,a.AssetID,a.AttributeTypeID,a.DisplayFormat,DiscreteOptions
	FROM @Attributes a where [Delete] = 0

	update a set attributeName = b.AttributeTypeDesc from #newAttributes a JOIN Asset.tAttributeType b on a.attributeTypeID = b.AttributeTypeID

	update a set attributeTypeID = b.AttributeTypeID from #newAttributes a JOIN Asset.tAttributeType b on a.attributeName = b.AttributeTypeDesc JOIN
	Asset.tAssetAttribute c on a.AssetID = c.AssetID AND b.AttributeTypeID = c.AttributeTypeID

	INSERT INTO #deleteAttributes(attributeName, AssetID, attributeTypeID)
	select a.Name,a.AssetID,a.AttributeTypeID
	FROM @Attributes a where a.[Delete] = 1

	update a set attributeName = b.AttributeTypeDesc from #deleteAttributes a JOIN Asset.tAttributeType b on a.attributeTypeID = b.AttributeTypeID

	update a set attributeTypeID = b.AttributeTypeID from #deleteAttributes a JOIN Asset.tAttributeType b on a.attributeName = b.AttributeTypeDesc JOIN
	Asset.tAssetAttribute c on a.AssetID = c.AssetID AND b.AttributeTypeID = c.AttributeTypeID

	DECLARE @OldValues as TABLE
	(
	  AssetID int,
	  AttributeTypeID int,
	  Value varchar(255)
	)

	INSERT INTO @OldValues
	SELECT a.AssetID,b.AttributeTypeID,b.Value
	FROM #newAttributes a JOIN Asset.vAssetAttribute b ON a.AssetID = b.assetID JOIN Asset.tAttributeType c on b.AttributeTypeID =
	c.AttributeTypeID where a.attributeName = c.AttributeTypeDesc

	if @deleteAllExistingAttributes = 1
	  begin
	  delete a from Asset.tassetattribute a JOIN #validattributes b on a.AssetID = b.AssetID and a.AttributeTypeID = b.AttributeTypeID where b.IsValid = 1
	  end
	else
	  begin
	  delete b from #newAttributes a JOIN Asset.tAssetAttribute b ON a.AssetID = b.assetID JOIN Asset.tAttributeType c on b.AttributeTypeID =
	                  c.AttributeTypeID where a.attributeName = c.AttributeTypeDesc
      delete b from #deleteAttributes a JOIN Asset.tAssetAttribute b ON a.AssetID = b.assetID JOIN Asset.tAttributeType c on b.AttributeTypeID =
	                  c.AttributeTypeID where a.attributeName = c.AttributeTypeDesc
	  end

	create table #AssetsWithClient
	(
	 ClientID int,
	 AssetID int
	)

	create index idx_1 ON #AssetsWithClient(ClientID);
	create index idx_2 ON #AssetsWithClient(AssetID);

	create table #AssetsWithClientTemp
	(
	 ClientID int,
	 AssetID int,
	 AncestryLevel int
	)

	create index idx_1 ON #AssetsWithClientTemp(ClientID);
	create index idx_2 ON #AssetsWithClientTemp(AssetID);
	create index idx_3 ON #AssetsWithClientTemp(AncestryLevel);

	insert into #AssetsWithClientTemp (AssetID,ClientID,AncestryLevel)
	select distinct a.AssetID,c.ClientID,b.AncestryLevel from #newAttributes a join Asset.tAssetHop b on a.AssetID = b.EndingAssetId join Asset.tClient c on b.HopAssetId = c.AssetID

	create table #assetAndAncestryLevel
	(
		assetId int,
		ancestrylevel int
	)

	create index idx_1 on #assetAndAncestryLevel(assetId)
	insert into #assetAndAncestryLevel
	select AssetID,MAX(AncestryLevel) as AncestryLevel from #AssetsWithClientTemp group by AssetID

	INSERT INTO #AssetsWithClient (AssetID,ClientID) SELECT X.AssetID,X.ClientID from #AssetsWithClientTemp x JOIN #assetAndAncestryLevel b ON x.AssetID = b.assetid and x.AncestryLevel = b.ancestrylevel

	create table #Attributes
	(
	  ClientID int null,
	  AttributeTypeID int null,
	  AttributeName varchar(255),
	  IsTextDataType bit,
	  IsFloatDataType bit,
	  IsStandard bit,
	  DisplayFormat varchar(255)
	)

	create index idx_1 ON #Attributes(ClientID);
	create index idx_2 ON #Attributes(AttributeName);
	create index idx_3 ON #Attributes(IsTextDataType);
	create index idx_4 ON #Attributes(IsFloatDataType);
    create index idx_5 ON #Attributes(IsStandard);

	declare @newAttributeTypes as Table
	(
	  AttributeTypeID int,
	  AttributeTypeDesc varchar(255),
	  ClientID int
	)

	INSERT INTO #Attributes (ClientID,AttributeName,IsTextDataType,IsFloatDataType,IsStandard,DisplayFormat)
	SELECT DISTINCT ClientID,AttributeName,0,0,0,DisplayFormat FROM #newAttributes a JOIN #AssetsWithClient b on A.AssetID = b.AssetID

	-- find all existing custom attributes and mark whether they are 'string'.  String attributes will be handled in a special case since they don't
	-- require range or value validation
	update a set a.AttributeTypeID = b.AttributeTypeID,
	IsTextDataType = CASE WHEN b.DisplayFormat = 'string' then 1 else 0 END,
	IsFloatDataType = CASE WHEN b.DisplayFormat like 'f%' or b.DisplayFormat like 'p%' then 1 else 0 END
	 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
	AND a.ClientID = b.ClientID

	-- find all standard attributes
	update a set a.AttributeTypeID = b.AttributeTypeID,
	IsTextDataType = CASE WHEN b.DisplayFormat = 'string' then 1 else 0 END,
	IsFloatDataType = CASE WHEN b.DisplayFormat like 'f%' or b.DisplayFormat like 'p%' then 1 else 0 END,
	IsStandard = 1
	 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
	WHERE b.ClientID IS NULL

	declare @newRadioTypeInfo as Table
	(
	  AttributeTypeID int NULL,
	  AttributeTypeDesc varchar(255),
	  ClientID int,
	  DiscreteOptions varchar(4000)
	)

	INSERT INTO @newRadioTypeInfo (AttributeTypeDesc,ClientID,DiscreteOptions)
	SELECT a.attributeName,b.ClientID,max(a.DiscreteOptions) as DiscreteOptions FROM #newAttributes a
		  JOIN #AssetsWithClient b on A.AssetID = b.AssetID where a.attributeTypeID IS NULL AND a.displayFormat = 'radio' and a.DiscreteOptions<>''
		  group by a.attributeName,b.ClientID

    -- Checks Attributes discrete list that need to be edited and upsert the radio type before saving the new radiotype
	IF (@deleteAllExistingAttributes <> 1 AND EXISTS(SELECT TOP 1 * FROM #newAttributes a WHERE a.attributeTypeID IS NOT NULL AND displayFormat = 'radio'))
	BEGIN
	DECLARE @editRadioTypeInfo as Table
	(
		AttributeTypeID int NULL,
		AttributeTypeDesc varchar(255),
		ClientID int,
		DiscreteOptions varchar(4000)
	)

	INSERT INTO @editRadioTypeInfo (AttributeTypeDesc,AttributeTypeID ,ClientID,DiscreteOptions)
	SELECT a.AttributeName, a.AttributeTypeID,b.ClientID,max(a.DiscreteOptions) FROM #newAttributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
	JOIN #AssetsWithClient c on a.assetID = c.AssetID
	INNER JOIN Asset.tAttributeType d on a.attributeTypeID = d.AttributeTypeID AND a.attributeName = d.AttributeTypeDesc
	AND c.ClientID = b.ClientID and a.DisplayFormat = 'radio' WHERE a.attributeTypeID IS NOT NULL
	GROUP BY a.attributeName,b.ClientID, a.AttributeTypeID

	DECLARE @AttributeTypeDesc VARCHAR(255)
	DECLARE @AttrTypeID INT
	DECLARE @DiscreteOptions VARCHAR(4000)
    DECLARE @forDeleteOptionType as Table (AttributeOptionTypeID int, AttributeOptionTypeDesc  varchar(255) NULL)
	DECLARE cur INSENSITIVE CURSOR FOR
		SELECT a.AttributeTypeDesc,a.AttributeTypeID,a.DiscreteOptions FROM @editRadioTypeInfo a
	OPEN cur;
	FETCH NEXT FROM cur INTO @AttributeTypeDesc, @AttrTypeID,@DiscreteOptions
    WHILE @@FETCH_STATUS = 0
	BEGIN
		IF OBJECT_ID('tempdb..#temp_attribute_newRadioValues') IS NOT NULL DROP TABLE #temp_attribute_newRadioValues
		CREATE TABLE #temp_attribute_newRadioValues (
			DisplayOrder int IDENTITY(1,1),
			AttributeTypeID int,
			AttributeOptionTypeDesc  varchar(255) NULL
		)
		DELETE FROM @forDeleteOptionType;
		INSERT INTO #temp_attribute_newRadioValues(AttributeTypeID,AttributeOptionTypeDesc) SELECT @AttrTypeID ,ufn.string FROM @editRadioTypeInfo
		CROSS APPLY [Base].[ufnSplitString](@DiscreteOptions, ';') ufn
		WHERE AttributeTypeID = @AttrTypeID

	    --Check if the option are subjected to be deleted
		INSERT INTO @forDeleteOptionType SELECT o.AttributeOptionTypeID,o.AttributeOptionTypeDesc FROM Asset.tAttributeOptionType o
		INNER JOIN Asset.tAttributeType at ON at.AttributeTypeID = o.AttributeTypeID
		WHERE o.AttributeTypeID = @AttrTypeID AND IsStandard = 0
		AND UPPER(AttributeOptionTypeDesc) NOT IN (SELECT UPPER(a.AttributeOptionTypeDesc) FROM #temp_attribute_newRadioValues a WHERE a.AttributeTypeID = @AttrTypeID)

		IF EXISTS(SELECT * FROM @forDeleteOptionType  WHERE AttributeOptionTypeID IS NOT NULL)
		BEGIN
			IF EXISTS (SELECT DISTINCT a.AssetID FROM Asset.tAssetAttribute a INNER JOIN Asset.tAttributeOptionType b ON b.AttributeTypeID = a.AttributeTypeID
			INNER JOIN Asset.tAttributeType at ON at.AttributeTypeID = a.AttributeTypeID
			JOIN @forDeleteOptionType c ON c.AttributeOptionTypeID = a.AttributeOptionTypeID
			WHERE a.AttributeTypeID = @AttrTypeID AND [Asset].[ufnDoesUserHaveAccessToAsset](a.AssetID,@UserID,-1 ) = 0)
			BEGIN
				 SET @errorMsg = 'User does not have access to all assets that would be affected by attribute change: ' + @AttributeTypeDesc;
				 RAISERROR (@errorMsg, 13, -1, -1)
				 RETURN -1
			END
			-- Delete attribute currently using this option value
			DELETE a FROM Asset.tAssetAttribute a
			INNER JOIN Asset.tAttributeOptionType b ON b.AttributeTypeID = a.AttributeTypeID
			INNER JOIN Asset.tAttributeType at ON at.AttributeTypeID = a.AttributeTypeID
			JOIN @forDeleteOptionType c on a.AttributeOptionTypeID = c.AttributeOptionTypeID
			WHERE a.AttributeTypeID = @AttrTypeID AND at.IsStandard = 0;

		-- Re-arrange the option display order
		IF OBJECT_ID('tempdb..#temp_attribute_reOrgNewRadioValues') IS NOT NULL DROP TABLE #temp_attribute_reOrgNewRadioValues
		CREATE TABLE #temp_attribute_reOrgNewRadioValues (
			DisplayOrder int IDENTITY(1,1),
			AttributeTypeID int,
			AttributeOptionTypeDesc  varchar(255) NULL
		)
		INSERT INTO #temp_attribute_reOrgNewRadioValues (AttributeTypeID,AttributeOptionTypeDesc)
		 SELECT @AttrTypeID ,AttributeOptionTypeDesc FROM #temp_attribute_newRadioValues WHERE AttributeTypeID = @AttrTypeID;

			INSERT INTO Asset.tAttributeOptionType (AttributeTypeID, AttributeOptionTypeDesc, DisplayOrder)
			  SELECT s.AttributeTypeID, s.AttributeOptionTypeDesc, s.DisplayOrder
			  FROM #temp_attribute_reOrgNewRadioValues s
			  WHERE NOT EXISTS (SELECT 1 FROM Asset.tAttributeOptionType t WHERE t.AttributeTypeID = s.AttributeTypeID AND t.AttributeOptionTypeDesc = s.AttributeOptionTypeDesc)

			UPDATE t
			  SET DisplayOrder = s.DisplayOrder
			  FROM Asset.tAttributeOptionType t
			  INNER JOIN #temp_attribute_reOrgNewRadioValues s ON t.AttributeOptionTypeDesc = s.AttributeOptionTypeDesc AND t.AttributeTypeID = s.AttributeTypeID

			DELETE FROM Asset.tAttributeOptionType
			  WHERE AttributeTypeID = @AttrTypeID AND AttributeOptionTypeDesc NOT IN
			  (SELECT AttributeOptionTypeDesc FROM #temp_attribute_reOrgNewRadioValues)
		END
		ELSE
		BEGIN
			INSERT INTO Asset.tAttributeOptionType (AttributeTypeID, AttributeOptionTypeDesc, DisplayOrder)
			  SELECT s.AttributeTypeID, s.AttributeOptionTypeDesc, s.DisplayOrder
			  FROM #temp_attribute_newRadioValues s
			  WHERE NOT EXISTS (SELECT 1 FROM Asset.tAttributeOptionType t WHERE t.AttributeTypeID = s.AttributeTypeID AND t.AttributeOptionTypeDesc = s.AttributeOptionTypeDesc)

			UPDATE t
			  SET DisplayOrder = s.DisplayOrder
			  FROM Asset.tAttributeOptionType t
			  INNER JOIN #temp_attribute_newRadioValues s ON t.AttributeOptionTypeDesc = s.AttributeOptionTypeDesc AND t.AttributeTypeID = s.AttributeTypeID
		END
		FETCH NEXT FROM cur INTO @AttributeTypeDesc,@AttrTypeID,@DiscreteOptions
	END
	CLOSE cur;
	DEALLOCATE cur;
    END

	declare @radioTypeInfo as Table
	(
	  AttributeTypeID int,
	  AttributeTypeDesc varchar(255),
	  ClientID int
	)

	INSERT INTO Asset.tAttributeType (AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)
	      OUTPUT inserted.AttributeTypeID,inserted.AttributeTypeDesc,inserted.ClientID INTO @radioTypeInfo
	      SELECT a.AttributeTypeDesc,REPLACE(a.AttributeTypeDesc,' ',''),'','radio',0,0,a.ClientID FROM @newRadioTypeInfo a

    insert into @newAttributeTypes select * from @radioTypeInfo

	update a set a.AttributeTypeID = b.AttributeTypeID
	 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
	   AND a.ClientID = b.ClientID AND a.AttributeTypeID IS NULL and a.DisplayFormat = 'radio'

	UPDATE a SET AttributeTypeID = b.AttributeTypeID FROM @newRadioTypeInfo a JOIN @radioTypeInfo b on a.AttributeTypeDesc = b.AttributeTypeDesc and a.ClientID = b.ClientID

	DECLARE @newRadioValues TABLE (
	    DisplayOrder int IDENTITY(1,1),
		AttributeTypeID int,
		OptionName  varchar(255) NULL
	);

	INSERT INTO @newRadioValues
		(AttributeTypeID, OptionName)
	SELECT a.AttributeTypeID, ufn.string
	FROM
		@newRadioTypeInfo a
	CROSS APPLY [Base].[ufnSplitString](a.discreteoptions, ';') ufn;

	INSERT INTO Asset.tAttributeOptionType (AttributeTypeID,AttributeOptionTypeDesc,DisplayOrder)
	SELECT AttributeTypeID,OptionName,DisplayOrder FROM @newRadioValues

	declare @tempTypeInfo as Table
	(
	  AttributeTypeID int,
	  AttributeTypeDesc varchar(255),
	  ClientID int
	)

	-- Insert new attribute types
	INSERT INTO Asset.tAttributeType (AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)
	OUTPUT inserted.AttributeTypeID,inserted.AttributeTypeDesc,inserted.ClientID INTO @tempTypeInfo
	SELECT AttributeName,SUBSTRING(REPLACE(AttributeName,' ',''),0,99),NULL,displayformat,0,0,ClientID FROM #Attributes WHERE
	IsStandard = 0 AND AttributeTypeID IS NULL AND ClientID IS NOT NULL

	INSERT INTO @newAttributeTypes select * from @tempTypeInfo

	-- Re-update table variable with new custom attribute types
	update a set a.AttributeTypeID = b.AttributeTypeID,
	IsTextDataType = CASE WHEN b.DisplayFormat = 'string' then 1 else 0 END
	 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
	AND a.ClientID = b.ClientID WHERE a.AttributeTypeID IS NULL

	-- update table with Attribute Type ID
	UPDATE a SET a.AttributeTypeID = C.AttributeTypeID FROM #newAttributes a JOIN #AssetsWithClient b on A.AssetID = b.AssetID JOIN #Attributes c on A.attributeName =
	c.AttributeName and b.ClientID = c.ClientID and a.attributeTypeID IS NULL and c.IsStandard = 0

	-- update table variable with Attribute Type ID for string or float attributes that already exist and are standard
	UPDATE a SET a.AttributeTypeID = C.AttributeTypeID FROM #newAttributes a
	JOIN #AssetsWithClient b on A.AssetID = b.AssetID JOIN #Attributes c on A.attributeName =
	c.AttributeName and (c.IsTextDataType=1 or c.IsFloatDataType=1) AND c.IsStandard = 1

	UPDATE a set IsValid = 1 FROM #newAttributes a WHERE (SELECT IsValid FROM Asset.ufnAttributeIsValidForUser(@UserID, AssetID, AttributeTypeID)) = 1

	INSERT Asset.tAssetAttribute  (AssetID,AttributeTypeID,AttributeOptionTypeID,Attribute_int,Attribute_string, Attribute_float,Favorite,DisplayOrder,
	CreatedByUserID,ChangedByUserID,CreateDate,ChangeDate)
	SELECT DISTINCT assetID,b.attributeTypeID,NULL,NULL,attributeValue,null,favorite,displayOrder,@userID,@userID,
	GETDATE(),GETDATE() FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID
	where b.attributeTypeID is not null AND c.IsTextDataType = 1 AND b.IsValid = 1

	DECLARE @badValueAttributeName as varchar(255)
  SET @badValueAttributeName = NULL

  SELECT TOP 1 @badValueAttributeName = b.attributeName FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID
  WHERE b.attributeTypeID IS NOT NULL AND c.IsFloatDataType = 1 AND (SELECT IsValid FROM Asset.ufnAttributeIsValidForUser(@userID, AssetID, b.AttributeTypeID)) = 1
  AND attributeValue != '' AND (LEN(attributeValue) > 309 OR ISNUMERIC(attributeValue) = 0)

  IF @badValueAttributeName IS NOT NULL
	  BEGIN
	  SET @errorMsg = 'Error in Excel inputs - expecting numeric value or blank for attribute: ' + @badValueAttributeName
	  RAISERROR (@errorMsg, 13, -1, -1)
	  RETURN -1
	  END

	INSERT Asset.tAssetAttribute (AssetID, AttributeTypeID, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float, Favorite, DisplayOrder, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
	SELECT DISTINCT assetID, b.attributeTypeID, NULL, NULL, NULL, CASE WHEN attributeValue = '' THEN NULL ELSE attributeValue END, favorite, displayOrder, @userID, @userID, GETDATE(), GETDATE()
	FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID
	WHERE b.attributeTypeID IS NOT NULL AND c.IsFloatDataType = 1 AND IsValid = 1

	-- insert blank date and radio attribute values
	INSERT Asset.tAssetAttribute  (AssetID,AttributeTypeID,Favorite,DisplayOrder,
	CreatedByUserID,ChangedByUserID,CreateDate,ChangeDate)
	SELECT DISTINCT assetID,b.attributeTypeID,favorite,displayOrder,@userID,@userID,
	GETDATE(),GETDATE() FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID
	JOIN Asset.tAttributeType d on c.AttributeTypeID = d.AttributeTypeID
	where b.attributeTypeID is not null AND UPPER(d.displayformat) in ('DATE','RADIO') AND IsValid = 1 AND b.attributeValue=''

	-- mark any processed attributes
	UPDATE b SET b.attributeID = d.AssetAttributeID FROM #newAttributes b  JOIN Asset.tAssetAttribute d on b.assetID = d.AssetID AND
	d.AttributeTypeID = b.attributeTypeID where b.attributeTypeID is not null and b.attributeID IS NULL

	-- loop through any remaining unprocessed attributes
    WHILE EXISTS(SELECT TOP 1 * FROM #newAttributes WHERE attributeID IS NULL)
	BEGIN

		DECLARE @newAttributeID INT;
		DECLARE @newTypeID INT;

		DECLARE @attAssetID INT;
		DECLARE @attributeName NVARCHAR(255);
		DECLARE @attributeValue NVARCHAR(MAX);
		DECLARE @attributeTypeID INT;
		DECLARE @favorite BIT;
		DECLARE @displayOrder INT;
		DECLARE @newID INT;
		SET @attributeName = NULL;
		SET @attributeTypeID = NULL;
		SELECT TOP 1 @attAssetID = assetID, @attributeName =attributeName, @attributeValue = attributeValue, @favorite = favorite, @displayOrder = displayOrder, @attributeTypeID = attributeTypeID  FROM #newAttributes
		WHERE attributeID IS NULL;

		BEGIN TRY
			DECLARE @emptylist as base.tpintlist
			EXEC Asset.spUpsertAssetAttribute 
			  @SecurityUserID = @userID
			, @AssetID = @attAssetID
			, @AttributeTypeID = @attributeTypeID
			, @Name = @attributeName
			, @Value = @attributeValue
			, @Favorite = @favorite
			, @DisplayOrder = @displayOrder
			-- I'm not passing an argument for @Date here so that spUpsertAssetAttribute will try parsing @Value for date attributes
			-- The @Date parameter is used by ETL.spImportCapPriorProjects to adjust dates entered in the user's local time zone
			, @IncludeCategories = 0
			, @CategoryIDs = @emptylist
			, @NewDisplayFormat = NULL
			, @NewID = @newAttributeID OUTPUT
			, @NewTypeID = @newTypeID OUTPUT
		END TRY
		BEGIN CATCH
			SET @errorMsg = 'Error - Invalid attribute value: ' + @attributeName + ':' + @attributeValue
			RAISERROR (@errorMsg, 13, -1, -1)
			RETURN -1
		END CATCH

		UPDATE #newAttributes
		SET attributeID = @newAttributeID
		WHERE attributeName = @attributeName AND assetID = @attAssetID

	END

	-- associate categories with attributes

	DECLARE @AssetCategories as TABLE
	(
	  CategoryID int,
	  AttributeTypeID int,
	  AssetID int,
	  ClientID int,
	  Processed bit NULL
	  )

	-- delete category maps for custom attributes
	DELETE m from Asset.tAssetAttributeCategoryMap m JOIN #newAttributes a ON m.AttributeTypeID = a.attributeTypeID
	JOIN #AssetsWithClient b on A.AssetID = b.AssetID JOIN #Attributes c on A.attributeName =
	c.AttributeName and b.ClientID = c.ClientID AND c.IsStandard = 0

	INSERT INTO @AssetCategories (CategoryID,AttributeTypeID,AssetID,Processed)
	SELECT DISTINCT b.CategoryID,a.attributeTypeID,a.assetID,0 FROM #newAttributes a JOIN
	@customAttributeCategories b ON a.attributeName = b.attributeName WHERE a.attributeTypeID IS NOT NULL

	UPDATE A SET ClientID = b.ClientID FROM @AssetCategories a JOIN #AssetsWithClient b on a.AssetID = b.AssetID OPTION (RECOMPILE)

	declare @clientcategories as table
	(
	  ClientID int,
	  ClientAssetID int,
	  AttributeTypeID int,
	  CategoryID int,
	  Processed bit
	)

	-- the upsert of categories is at a clientid level, so consolidate them down to a distinct list by client asset id
	INSERT INTO @clientcategories (ClientID,ClientAssetID,AttributeTypeID,CategoryID,Processed)
	select distinct A.ClientID,b.AssetID, a.AttributeTypeID,a.CategoryID,0 from @AssetCategories a JOIN
	Asset.tClient b on a.ClientID = b.ClientID

	-- loop through assets and attributes to set their categories
	WHILE EXISTS(SELECT TOP 1 * FROM @clientcategories WHERE Processed=0)
	  BEGIN
	  DECLARE @curAttributeTypeID as int
	  DECLARE @curAssetID as int
	  SELECT TOP 1 @curAttributeTypeID = AttributeTypeID, @curAssetID = ClientAssetID FROM @clientcategories WHERE Processed = 0 OPTION (RECOMPILE)

	  DECLARE @categoryIDs Base.tpIntList
	  DELETE FROM @categoryIDs;
	  INSERT INTO @categoryIDs(id)
		SELECT CategoryID FROM @clientcategories WHERE AttributeTypeID= @curAttributeTypeID and ClientAssetID = @curAssetID OPTION (RECOMPILE)
	  EXEC [Asset].[spSetAssetAttributeCategories] @curAttributeTypeID, @curAssetID, @CategoryIDs
	  UPDATE @clientcategories SET Processed = 1 WHERE ClientAssetID = @curAssetID and AttributeTypeID = @curAttributeTypeID OPTION (RECOMPILE)
	  END

    IF @addAssetDiscussionEntries = 1
	  BEGIN
	  DECLARE @now as DateTimeOffset
	  SET @now = GETDATE()

	  declare @blogentries as [Discussions].[tpAssetDiscussionEntry]

	  insert into @blogentries (AssetID,OwnerUserID,CreateDateZ,ChangeDateZ,CreatedByUserID,ChangedByUserID,Title,Content,Approved,AutoGenerated)
	  select AssetID,@UserID,@now,@now,@UserID,@UserID,'Asset Changed','Added New Attribute ' + CONVERT(varchar(20),a.attributeTypeID) +
	  ' (' +  c.AttributeTypeDesc + ')',1,1 FROM @newAttributeTypes a JOIN #newAttributes b ON
	                a.AttributeTypeID = b.attributeTypeID JOIN Asset.tAttributeType c on b.AttributeTypeID = c.AttributeTypeID

	  insert into @blogentries (AssetID,OwnerUserID,CreateDateZ,ChangeDateZ,CreatedByUserID,ChangedByUserID,Title,Content,Approved,AutoGenerated)
	  select AssetID,@UserID,@now,@now,@UserID,@UserID,'Asset Changed','Removed Attribute ' + CONVERT(varchar(20),b.attributeTypeID) +
	  ' (' +  c.AttributeTypeDesc + ')',1,1 FROM #deleteAttributes b JOIN Asset.tAttributeType c on b.AttributeTypeID = c.AttributeTypeID

	  insert into @blogentries (AssetID,OwnerUserID,CreateDateZ,ChangeDateZ,CreatedByUserID,ChangedByUserID,Title,Content,Approved,AutoGenerated)
	  select a.AssetID,@UserID,@now,@now,@UserID,@UserID,'Asset Changed','Attribute Value Changed From ' + a.Value +
	  ' to ' +  b.Value,1,1 FROM @OldValues a JOIN Asset.vAssetAttribute b on a.AssetID = b.AssetID and a.AttributeTypeID = b.AttributeTypeID
	   JOIN Asset.tAttributeType c on b.AttributeTypeID = c.AttributeTypeID WHERE a.Value<>b.Value

	  update a SET DiscussionID = b.DiscussionID FROM @blogentries a JOIN Asset.tDiscussionAssetMap b on a.AssetID = b.AssetID WHERE b.Type = 1

	  EXEC [Discussions].[spInsertAssetDiscussionEntries] @blogentries

	  END

	commit transaction

	return 0

END TRY
BEGIN CATCH

	if ISNULL(@errorMsg,'')=''
	  begin
	  set @errorMsg = error_message()
	  end

	rollback transaction

	return -1
END CATCH

END

GO

GRANT EXECUTE
    ON [Asset].[spUpsertAssetAttributes] TO [TEUser] as [DBO];
GO
