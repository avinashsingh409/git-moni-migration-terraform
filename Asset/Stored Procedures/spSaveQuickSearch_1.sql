﻿CREATE PROCEDURE [Asset].[spSaveQuickSearch]
	@QuickSearchId int,
	@Name nvarchar(255),
	@SearchExpression nvarchar(255),
	@IsPublic bit,
	@AssetId int,
	@CreatedBy int,
	@ChangedBy int,
	@StickySearch							bit,
	@SelectedMapGeoSpaID					int = null,
	@SearchingStatusesNotStatusChangeLog	bit = null,
	@StatusAsOf								nvarchar(32) = null,
	@SelectedStatuses						nvarchar(255) = null,
	@TimesliderSelection					datetime = null,
	@StatusChangeLogStartDate				datetime = null,
	@StatusChangeLogEndDate					datetime = null,
	@Columns								nvarchar(max) = null,
	@ScheduleSearch							bit = null,
	@AssetClassTypeID						int = null,
	@AssetClassTypeIDForAncestors			bit = 0,
	@Tab									nvarchar(255) = null,
	@IsAdvancedSearch						bit = NULL,
	@ScheduleMapID							INT = NULL,
	@ScheduleAssetClassTypeID				INT = NULL,
	@ViewType								NVARCHAR(32) = NULL,
	@ApplyToSelectedAssetDescendants		bit
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @QuickSearchId IS NULL BEGIN
		INSERT INTO [Asset].[tQuickSearch] (
		[Name],
		SearchExpression,
		IsPublic,
		AssetId,
		CreatedBy,
		ChangedBy,
		CreateDate,
		ChangeDate,
		StickySearch,
		SelectedMapGeoSpaID,
		SearchingStatusesNotStatusChangeLog,
		StatusAsOf,
		SelectedStatuses,
		TimesliderSelection,
		StatusChangeLogStartDate,
		StatusChangeLogEndDate,
		[Columns],
		AssetClassTypeID,
		AssetClassTypeIDForAncestors,
		ScheduleSearch,
		Tab,
		IsAdvancedSearch,
		ScheduleMapID,
		ScheduleAssetClassTypeID,
		[ViewType],
		ApplyToSelectedAssetDescendants
		)
		VALUES (
		@Name,
		@SearchExpression,
		@IsPublic,
		@AssetId,
		@CreatedBy,
		@ChangedBy,
		@now,
		@now,
		@StickySearch,
		@SelectedMapGeoSpaID,
		@SearchingStatusesNotStatusChangeLog,
		@StatusAsOf,
		@SelectedStatuses,
		@TimesliderSelection,
		@StatusChangeLogStartDate,
		@StatusChangeLogEndDate,
		@Columns,
		@AssetClassTypeID,
		@AssetClassTypeIDForAncestors,
		@ScheduleSearch,
		@Tab,
		@IsAdvancedSearch,
		@ScheduleMapID,
		@ScheduleAssetClassTypeID,
		@ViewType,
		@ApplyToSelectedAssetDescendants
		)
		SELECT SCOPE_IDENTITY();
		RETURN;
	END
	UPDATE [Asset].[tQuickSearch] 
	SET Name = @Name,
		SearchExpression = @SearchExpression,
		IsPublic = @IsPublic,
		AssetId = @AssetId,
		CreatedBy = @CreatedBy,
		ChangedBy = @ChangedBy,
		CreateDate = @now,
		ChangeDate = @now,
		StickySearch						 = @StickySearch,						
		SelectedMapGeoSpaID					 = @SelectedMapGeoSpaID,					
		SearchingStatusesNotStatusChangeLog	 = @SearchingStatusesNotStatusChangeLog,
		StatusAsOf							 = @StatusAsOf,							
		SelectedStatuses					 = @SelectedStatuses,					
		TimesliderSelection					 = @TimesliderSelection,
		StatusChangeLogStartDate			 = @StatusChangeLogStartDate,			
		StatusChangeLogEndDate				 = @StatusChangeLogEndDate,
		[Columns]							 = @Columns,
		ScheduleSearch						 = @ScheduleSearch,
		AssetClassTypeID					 = @AssetClassTypeID,
		AssetClassTypeIDForAncestors		 = @AssetClassTypeIDForAncestors,
		Tab									 = @Tab,
		IsAdvancedSearch					 = @IsAdvancedSearch,
		ScheduleMapID						 = @ScheduleMapID,
		ScheduleAssetClassTypeID			 = @ScheduleAssetClassTypeID,
		ViewType							 = @ViewType,
		ApplyToSelectedAssetDescendants		 = @ApplyToSelectedAssetDescendants
	WHERE QuickSearchId = @QuickSearchId
	SELECT @QuickSearchId;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveQuickSearch] TO [TEUser]
    AS [dbo];

