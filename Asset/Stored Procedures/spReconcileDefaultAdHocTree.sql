﻿CREATE PROCEDURE [Asset].[spReconcileDefaultAdHocTree]
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @defaultTreeId uniqueidentifier;
	DECLARE @defaultTreeName nvarchar(255) = 'Default Asset Tree';
	DECLARE @now datetime = GETDATE();

	SELECT TOP 1 @defaultTreeId = treeid FROM UIConfig.tAdHocTree WHERE IsDefaultTree=1 ORDER BY UIConfig.tAdHocTree.CreateDate;
   
   IF	@defaultTreeId IS NULL BEGIN
		  
		  SET @defaultTreeId = NEWID();
		  INSERT INTO UIConfig.tAdHocTree
		  (
		      TreeId,
		      TreeName,
		      Global,
		      CreatedBy,
		      ChangedBy,
		      CreateDate,
		      ChangeDate,
		      IsSearchable,
			  AllApp,
			  IsPrivate,
			  IsDefaultTree
		  )
		  VALUES
		  (
		      @defaultTreeId, -- TreeId - uniqueidentifier
		      @defaultTreeName, -- TreeName - nvarchar
		      1, -- Global - bit
		      1, -- CreatedBy - int
		      1, -- ChangedBy - int
		      @now, -- CreateDate - datetime
		      @now, -- ChangeDate - datetime
		      0, -- IsSearchable - bit
			  1, -- AllApp - bit
			  0,  -- IsPrivate - bit
			  1  -- IsDefault - bit
		  )
	END

   SELECT TOP 1 @defaultTreeId = treeid FROM UIConfig.tAdHocTree WHERE IsDefaultTree=1 ORDER BY UIConfig.tAdHocTree.CreateDate;

   DELETE a from UIConfig.tAdHocNode a
   join Asset.tAsset b on a.AssetId = b.AssetID
   where TreeId = @defaultTreeId and b.ParentAssetID is not null

   INSERT INTO UIConfig.tAdHocNode
   (
       TreeId,
       NodeAbbrev,
       NodeDesc,
       ParentNodeId,
       NodeTypeId,
       DisplayOrder,
       CreatedBy,
       ChangedBy,
       CreateDate,
       ChangeDate,
       AssetId,
       AssetNodeBehaviorId,
       CriteriaObjectId
   )

	SELECT @defaultTreeId ,
       ta.AssetAbbrev,
       ta.AssetDesc,
       NULL,
       2,
       ta.DisplayOrder,
       1,
       1,
       @now,
       @now,
       ta.AssetId,
       3,
       null
	FROM Asset.tAsset ta
	LEFT JOIN UIConfig.tAdHocNode tahn ON tahn.TreeId = @defaultTreeId AND ta.AssetID = tahn.AssetId
	WHERE ta.ParentAssetID IS NULL AND tahn.NodeId IS NULL
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spReconcileDefaultAdHocTree] TO [TEUser]
    AS [dbo];

