﻿CREATE PROCEDURE [Asset].[spGetAttributeTypeIDsForAssets] 
	@assetIDs Base.tpIntList READONLY  -- this assumes asset list has been filtered for authorization 
AS

BEGIN

CREATE TABLE #attrTypes
(
	AssetID INT,
	AttributeTypeID INT
)

CREATE TABLE #assets 
(
	ID INT
)

INSERT INTO #assets(ID)
SELECT DISTINCT id FROM @assetIDs

INSERT INTO #attrTypes (AssetID, AttributeTypeID)
SELECT aa.AssetID, at.AttributeTypeID  
  FROM Asset.tAttributeType at 
  INNER JOIN Asset.tAssetAttribute aa ON aa.AttributeTypeID = at.AttributeTypeID
  INNER JOIN #assets a ON a.ID = aa.AssetID 
UNION SELECT NULL, AttributeTypeID 
  FROM Asset.tAttributeType WHERE IsExempt = 1 
  
SELECT * FROM #attrTypes
RETURN

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spGetAttributeTypeIDsForAssets]   TO [TEUser]
    AS [dbo];
GO
