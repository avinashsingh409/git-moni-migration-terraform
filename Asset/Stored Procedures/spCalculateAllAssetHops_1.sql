﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Asset].[spCalculateAllAssetHops]
AS
BEGIN
	 -- SET NOCOUNT ON added to prevent extra result sets from
	 -- interfering with SELECT statements.
	 SET NOCOUNT ON;

	 DECLARE @assetId int;
	 DECLARE @tmpAssetHop TABLE (
		  [HopAssetId] [int] NOT NULL,
		  [EndingAssetId] [int] NOT NULL,
		  [AncestryLevel] [int] NOT NULL
	 )

	 DECLARE cursorAsset CURSOR FAST_FORWARD FOR
		  SELECT assetid
		  FROM asset.tAsset ta
		  WHERE ta.IsHidden = 0
		  ORDER BY ta.AssetID
	 
	 OPEN cursorAsset
	 FETCH NEXT FROM cursorAsset INTO @assetId
	 WHILE @@FETCH_STATUS=0 BEGIN
		  -- -------------------------------------------------------------------

		  WITH assets(assetid, parentassetid, levelsAway)
		  AS
		  (
				SELECT assetid, parentassetid, 0 AS levelsAway
				FROM Asset.tasset WHERE assetid=@assetId
				UNION ALL
				SELECT a.assetid, a.parentassetid, a2.levelsAway - 1
				FROM Asset.tasset a
				INNER JOIN assets a2 ON a2.parentassetid = a.assetid
		  )
		  INSERT INTO @tmpAssetHop
		  (
				[HopAssetId] ,
				[EndingAssetId],
				[AncestryLevel]
		  )
		  SELECT a.assetid, @assetId, a.levelsAway FROM assets a;
		  -- -------------------------------------------------------------------
		  FETCH NEXT FROM cursorAsset INTO @assetId
	 END

	 CLOSE cursorAsset
	 DEALLOCATE cursorAsset
	 
	 ALTER TABLE [Asset].[tAssetHop] DROP CONSTRAINT PK_tAssetHop

	 TRUNCATE TABLE Asset.tAssetHop

	 INSERT INTO Asset.tAssetHop(HopAssetId, EndingAssetId, AncestryLevel)
		  SELECT [HopAssetId],[EndingAssetId],[AncestryLevel]
		  FROM @tmpAssetHop


	ALTER TABLE [Asset].[tAssetHop] ADD	CONSTRAINT [PK_tAssetHop] PRIMARY KEY CLUSTERED 
	(
		[HopAssetId] ASC,
		[EndingAssetId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		  
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spCalculateAllAssetHops] TO [TEUser]
    AS [dbo];

