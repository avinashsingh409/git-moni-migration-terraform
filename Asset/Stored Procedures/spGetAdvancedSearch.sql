﻿CREATE PROCEDURE [Asset].[spGetAdvancedSearch] 
(	
	@page INT,
	@pageSize INT,
	@startingAssetId INT,
	@securityUserId INT,
	@includeAllAssets BIT,
	@includeDescendants BIT,
	@includeChildren BIT,
	@includeAncestors BIT,
	@includeParents BIT,
	@includeSiblings BIT,
	@includeUnitDescendants BIT,
	@includeSecondCousins BIT,
	@includeCousins BIT,
	@includeUnit BIT,
	@includeSelf BIT,
	@includeDescendantsOfSearchResults BIT,
	@searchClassIDs BIT,
	@classIDs Base.tpIntList READONLY,
	@attributeSortID INT,
	@sortOrder BIT,
	@ids Base.tpIntList READONLY,
	@terms Asset.tpTerms READONLY,
	@eventTypeIds Base.tpIntList READONLY,
	@eventAttributeTypeIds Base.tpIntList READONLY,
	@eventEndDate DATETIME,	
	@cashflowField NVARCHAR(255),
	@cashflowState INT, 
	@cashflowUseValue BIT,
	@cashflowActualFilter BIT,
	@cashflowPlanFilter BIT,
	@cashflowActualFilterTerms Base.tpIntStringPair READONLY,
	@cashflowPlanFilterTerms Base.tpIntStringPair READONLY,
	@totalAssets INT OUT,
	@totalEvents INT OUT
)
AS
BEGIN

--Row Num has to be present because it needs to sort.  
DECLARE @result TABLE
(
	AssetID int PRIMARY KEY not NULL,
	RowNum int NOT NULL
)

--Search Space is the full set of assets that can be considered 
IF OBJECT_ID('tempdb..#searchSpace_spGetAdvancedSearch') IS NOT NULL DROP TABLE #searchSpace_spGetAdvancedSearch
CREATE TABLE #searchSpace_spGetAdvancedSearch (
	AssetID INT PRIMARY KEY CLUSTERED,
	AssetDesc NVARCHAR(255),
	AssetAbbrev NVARCHAR(50),
	AssetClassTypeID INT,
	DisplayOrder INT)

--There are two methods for calculating the search space.  If you want to find assets based on relation to the root asset, 
--and then search descendants of those there is one method.  If you just want to find assets based on the relationship to 
--the root,and not search descendants the other is used.  
IF @includeDescendantsOfSearchResults = 1
BEGIN
    DECLARE @singleClassID as int
	DECLARE @classIDcnt as int
	SELECT @classIDcnt = COUNT(*) FROM @classIDs
	if @classIDcnt > 1
	  BEGIN
	  RAISERROR ('Error - expecting only one asset class type.', 13, -1, -1)
	  RETURN
	  END
	ELSE IF @classIDCnt=1
	  BEGIN
	  SELECT TOP 1 @singleClassID = ID FROM @classIDs
	  END
	
	INSERT INTO #searchSpace_spGetAdvancedSearch(AssetID, AssetDesc, AssetAbbrev,AssetClassTypeID, DisplayOrder) 
	SELECT ta.AssetId, ta.AssetDesc, ta.AssetAbbrev,ta.AssetClassTypeID, ta.DisplayOrder
	FROM Asset.ufnGetAssetRelationsAndDescendants(@startingAssetId,@securityUserId, @includeAllAssets, @includeDescendants,
	@includeChildren, @includeAncestors, @includeParents, @includeSiblings, 
	@includeUnitDescendants, @includeSecondCousins, @includeCousins, @includeUnit, @includeSelf, @singleClassID) ugatb
	INNER JOIN Asset.tAsset ta ON ta.AssetID = ugatb.AssetID 
	OPTION (RECOMPILE)
END
ELSE
BEGIN 
    -- short circuit search if it is simply getting descendants of starting asset
    IF (@includeDescendants = 1 AND @includeSelf=1 AND @searchClassIDs = 0 
		AND ISNULL(@includeAncestors,0) = 0 AND ISNULL(@includeParents,0) = 0
		AND ISNULL(@includeSiblings,0) = 0 AND ISNULL(@includeUnitDescendants,0) = 0
		AND ISNULL(@includeSecondCousins,0) = 0 AND ISNULL(@includeCousins,0) = 0
		AND ISNULL(@includeUnit,0) = 0 AND ISNULL(@includeDescendantsOfSearchResults,0) = 0)
	  BEGIN
	  INSERT INTO #searchSpace_spGetAdvancedSearch(AssetID, AssetDesc, AssetAbbrev,AssetClassTypeID, DisplayOrder) 
	    SELECT ta.AssetId, ta.AssetDesc, ta.AssetAbbrev,ta.AssetClassTypeID, ta.DisplayOrder
	    FROM
		Asset.ufnGetOnlyPhysicalAssetIdsForUserStartingAt(@securityUserId,@startingAssetId) ugatb
		INNER JOIN Asset.tAsset ta ON ta.AssetID = ugatb.AssetID OPTION (RECOMPILE)
	  END
	ELSE
	  BEGIN
	  INSERT INTO #searchSpace_spGetAdvancedSearch(AssetID, AssetDesc, AssetAbbrev,AssetClassTypeID, DisplayOrder) 
	    SELECT ta.AssetId, ta.AssetDesc, ta.AssetAbbrev,ta.AssetClassTypeID, ta.DisplayOrder
	    FROM Asset.ufnGetAssetRelations(@startingAssetId,@securityUserId, @includeAllAssets, @includeDescendants,
	    @includeChildren, @includeAncestors, @includeParents, @includeSiblings, 
	    @includeUnitDescendants, @includeSecondCousins, @includeCousins, @includeUnit, @includeSelf) ugatb
	    INNER JOIN Asset.tAsset ta ON ta.AssetID = ugatb.AssetID 
	    WHERE (@searchClassIDs = 0) OR (EXISTS (SELECT * FROM @classIDs WHERE id = ta.AssetClassTypeID))
	    OPTION (RECOMPILE)
	 END
END

IF EXISTS (SELECT TOP 1 id FROM @ids)
	DELETE FROM #searchSpace_spGetAdvancedSearch WHERE AssetID NOT IN (SELECT id FROM @ids)

--Create table with events associated with assets in the search space
IF OBJECT_ID('tempdb..#assetEvents_spGetAdvancedSearch') IS NOT NULL DROP TABLE #assetEvents_spGetAdvancedSearch
CREATE TABLE #assetEvents_spGetAdvancedSearch(assetEventId INT PRIMARY KEY CLUSTERED, assetId INT, eventTypeDesc NVARCHAR(MAX))

IF EXISTS (SELECT TOP 1 id From @eventTypeIds)
BEGIN
	--Event count only includes event occurences for event types and event attribute types specified
	INSERT INTO #assetEvents_spGetAdvancedSearch (assetEventId, assetId, eventTypeDesc)
	SELECT e.EventAssetID, e.EventOwnerAssetID, e.EventTypeDesc FROM Asset.vAssetEvent e
	JOIN @eventTypeIds c ON e.EventAssetClassTypeID = c.id
	JOIN Asset.tAssetAttribute a ON e.EventAssetID = a.AssetID
	JOIN Asset.tAttributeType d on a.AttributeTypeID = d.AttributeTypeID
	WHERE e.EventAssetClassTypeID IN (SELECT ID FROM @eventTypeIds)
	AND a.AttributeTypeID IN (SELECT id FROM @eventAttributeTypeIds)
	AND ((d.DisplayFormat='date' and  a.Attribute_date <= @eventEndDate) or (d.DisplayFormat<>'date'))
	GROUP BY e.EventOwnerAssetID, e.EventAssetID, e.EventTypeDesc

	--Remove assets from search space that don't have events of the specified type
	DELETE FROM #searchSpace_spGetAdvancedSearch
	WHERE AssetID NOT IN (SELECT assetId FROM #assetEvents_spGetAdvancedSearch)
END

--Now create a table to hold the assets that have been filtered.  We can't just reuse the search space
--because we may and/or terms.  
IF OBJECT_ID('tempdb..#filteredAssets_spGetAdvancedSearch') IS NOT NULL DROP TABLE #filteredAssets_spGetAdvancedSearch
CREATE TABLE #filteredAssets_spGetAdvancedSearch(id INT PRIMARY KEY CLUSTERED)


IF EXISTS (SELECT TOP 1 * FROM @terms)
BEGIN
	--Cursor to iterate through all of the terms. 
	DECLARE cursor_term CURSOR FORWARD_ONLY READ_ONLY LOCAL
	FOR SELECT AndOr, LeftHandSide, Operator, RightHandSide FROM @terms ORDER BY id

	DECLARE @cursor_AndOr INT
	DECLARE @cursor_LeftHandSide NVARCHAR(255)
	DECLARE @cursor_Operator INT
	DECLARE @cursor_RightHandSide NVARCHAR(255) 

	OPEN cursor_term
	FETCH NEXT FROM cursor_term
	INTO @cursor_AndOR,@cursor_LeftHandSide,@cursor_operator,@cursor_RightHandSide

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF OBJECT_ID('tempdb..#temp_assets_spGetAdvancedSearch') IS NOT NULL DROP TABLE #temp_assets_spGetAdvancedSearch
		IF OBJECT_ID('tempdb..#temp_attributes_spGetAdvancedSearch') IS NOT NULL DROP TABLE #temp_attributes_spGetAdvancedSearch
		CREATE TABLE #temp_assets_spGetAdvancedSearch(id INT PRIMARY KEY CLUSTERED)
		CREATE TABLE #temp_attributes_spGetAdvancedSearch(id INT PRIMARY KEY CLUSTERED)

		--Clear out the assets list for this term 
		DELETE FROM #temp_assets_spGetAdvancedSearch

		--search for assets with a specified description
		IF @cursor_LeftHandSide = 'asset'
		BEGIN
			INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			SELECT DISTINCT asset.AssetID FROM #searchSpace_spGetAdvancedSearch asset
			WHERE (@cursor_operator = 0 AND asset.AssetDesc like @cursor_RightHandSide OR asset.AssetAbbrev like @cursor_RightHandSide) OR 
			(@cursor_operator <> 0 AND asset.AssetDesc NOT like @cursor_RightHandSide AND asset.AssetAbbrev NOT like @cursor_RightHandSide)
		END 
		--search for assets with a specified tag
		ELSE IF @cursor_LeftHandSide = 'tag'
		BEGIN
		    IF @cursor_operator = 0 AND @cursor_RightHandSide = 'NULL' -- return records that have no tags
			  BEGIN
			  INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			  SELECT DISTINCT asset.AssetID FROM 
			    #searchSpace_spGetAdvancedSearch asset EXCEPT
				SELECT akm.AssetID FROM Asset.tAssetKeywordMap akm 
			    INNER JOIN Asset.tKeyword kw ON akm.KeywordID = kw.KeywordID
			  END
			ELSE
			  BEGIN
			  IF @cursor_operator = 0 AND @cursor_RightHandSide = '%' -- return records that have tags and ignore those that don't
			    BEGIN
			    INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			    SELECT DISTINCT asset.AssetID FROM 
			    #searchSpace_spGetAdvancedSearch asset INTERSECT
				SELECT akm.AssetID FROM Asset.tAssetKeywordMap akm 
			    INNER JOIN Asset.tKeyword kw ON akm.KeywordID = kw.KeywordID
			    END
			  ELSE
			    BEGIN
			    INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			    SELECT DISTINCT asset.AssetID FROM #searchSpace_spGetAdvancedSearch asset
			    INNER JOIN Asset.tAssetKeywordMap akm ON asset.AssetID = akm.AssetID
			    INNER JOIN Asset.tKeyword kw ON akm.KeywordID = kw.KeywordID
			    WHERE (@cursor_operator = 0 AND kw.Text like @cursor_RightHandSide) 
			      OR (@cursor_operator <> 0 AND kw.Text NOT like @cursor_RightHandSide)
			    END
			  END
		END
		ELSE
		BEGIN
			--In this situation we are not looking for an asset or tag.  So we need to look for the attributes we need to search
			--If no key is specified just get all of the attribute types for this set of assets.
			--If a key is specified then narrow the search based on the attribute 
			DELETE FROM #temp_attributes_spGetAdvancedSearch
			IF @cursor_LeftHandSide IS NULL OR @cursor_LeftHandSide = ''
			BEGIN
				INSERT INTO #temp_attributes_spGetAdvancedSearch(id) 
				SELECT DISTINCT atype.AttributeTypeID 
				FROM Asset.tAttributeType atype
				INNER JOIN Asset.tAssetAttribute att ON atype.AttributeTypeID = att.AttributeTypeID
				INNER JOIN #searchSpace_spGetAdvancedSearch asset ON att.AssetID = asset.AssetID
			END 
			ELSE
			BEGIN
				INSERT INTO #temp_attributes_spGetAdvancedSearch(id) 
				SELECT DISTINCT atype.AttributeTypeID 
				FROM Asset.tAttributeType atype
				INNER JOIN Asset.tAssetAttribute att ON atype.AttributeTypeID = att.AttributeTypeID
				INNER JOIN #searchSpace_spGetAdvancedSearch asset ON att.AssetID = asset.AssetID 
				WHERE atype.AttributeTypeDesc like @cursor_LeftHandSide OR CONVERT(NVARCHAR(50), atype.AttributeTypeID) like @cursor_LeftHandSide
			END 

			IF ISNULL(@cursor_RightHandSide,'')='NULL'  AND @cursor_Operator = 0 -- return all records that are blank/null or that have no value at all
			  BEGIN
			  INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			  SELECT DISTINCT asset.AssetID FROM 
			    #searchSpace_spGetAdvancedSearch asset EXCEPT
				SELECT a.AssetID FROM Asset.vAssetAttribute a INNER JOIN 
				#temp_attributes_spGetAdvancedSearch attributeType ON a.AttributeTypeID = attributeType.id

			  INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			  SELECT DISTINCT asset.AssetID FROM 
			  #searchSpace_spGetAdvancedSearch asset
			  INNER JOIN Asset.vAssetAttribute attribute ON asset.AssetID = attribute.AssetID 
			  INNER JOIN #temp_attributes_spGetAdvancedSearch attributeType ON attribute.AttributeTypeID = attributeType.id
			  WHERE ISNULL(attribute.Value,'') = ''
			  END
			ELSE
			  BEGIN

				DECLARE @FloatValue as FLOAT
				DECLARE @IntValue as INT
				SET @FloatValue = NULL
				SET @IntValue = NULL
				
				BEGIN TRY
				  SET @FloatValue = CAST(REPLACE(@cursor_RightHandSide,'%','') AS float)
				END TRY
				BEGIN CATCH
				  SET @FloatValue = NULL				  
				END CATCH

				BEGIN TRY
				  SET @IntValue = CAST(REPLACE(@cursor_RightHandSide,'%','') AS int)
				END TRY
				BEGIN CATCH
				  SET @IntValue = NULL
				END CATCH

			    --Now we have a list of attributes to search on.  We need to find the ones that match the value string 
			    INSERT INTO #temp_assets_spGetAdvancedSearch(id)
			    SELECT DISTINCT asset.AssetID FROM 
			    #searchSpace_spGetAdvancedSearch asset
			    INNER JOIN Asset.vAssetAttribute attribute ON asset.AssetID = attribute.AssetID 
			    INNER JOIN #temp_attributes_spGetAdvancedSearch attributeType ON attribute.AttributeTypeID = attributeType.id
			    WHERE (@cursor_Operator = 0 AND ISNULL(attribute.Value,'')<>'' AND  
				
				(
				          @cursor_RightHandSide = '%' -- find all non-null values
						  OR 
						  (attribute.Attribute_int IS NOT NULL AND attribute.Attribute_int = @IntValue) -- for numbers always look for exact matches
						  OR
						  (attribute.Attribute_float IS NOT NULL AND ABS(attribute.Attribute_float - @FloatValue) < 0.0000005)  
						  OR						
						  (attribute.Attribute_int is NULL AND attribute.Attribute_float is NULL AND ISNULL(attribute.Value,'') like @cursor_RightHandSide)
						  )
				)	

			    OR (@cursor_Operator <> 0 AND
				        (
				          (attribute.Attribute_int IS NOT NULL AND attribute.Attribute_int <> @IntValue)
						  OR
						  (attribute.Attribute_float IS NOT NULL AND ABS(attribute.Attribute_float - @FloatValue) > 0.0000005) 
						  OR						
						  (attribute.Attribute_int is NULL AND attribute.Attribute_float is NULL AND ISNULL(attribute.Value,'') NOT LIKE @cursor_RightHandSide)
						  )
						)
			 END

		END 

		--Now we have the search results of the term in question
		--We need to either and it to the set, or it to the set or exclude it from the set 
		IF @cursor_AndOr = 1 
		BEGIN
			--AND
			DELETE FROM #filteredAssets_spGetAdvancedSearch
			WHERE id NOT IN (SELECT id FROM #temp_assets_spGetAdvancedSearch)
		END
		ELSE IF @cursor_AndOr = 0
		BEGIN
			--OR
			INSERT INTO #filteredAssets_spGetAdvancedSearch(id)
			SELECT id FROM #temp_assets_spGetAdvancedSearch WHERE id NOT IN (SELECT id from #filteredAssets_spGetAdvancedSearch)
		END
		ELSE IF @cursor_AndOr = 2
		BEGIN
			--EXCEPT
			DELETE FROM #filteredAssets_spGetAdvancedSearch
			WHERE id IN (SELECT id FROM #temp_assets_spGetAdvancedSearch)
		END 

		--Increment the cursor
		FETCH NEXT FROM cursor_term
		INTO @cursor_AndOR,@cursor_LeftHandSide,@cursor_operator,@cursor_RightHandSide

	END
END
ELSE
	INSERT INTO #filteredAssets_spGetAdvancedSearch(id) SELECT DISTINCT asset.AssetID FROM #searchSpace_spGetAdvancedSearch asset

--Now we do the filter on schedule columns
IF @cashflowField IS NOT NULL AND @cashflowPlanFilter = 1
BEGIN
	DECLARE @allPlans TABLE(asset int, status int, scheduledate NVARCHAR(64))

	INSERT INTO @allPlans(asset,status, scheduledate)
	SELECT a.AssetID, a.Status, CONVERT(NVARCHAR(64), Min(a.Timestamp),101) 
	FROM CapPrior.tProjectCashFlow a
	INNER JOIN @cashflowPlanFilterTerms b ON a.Status = b.Id
	INNER JOIN #filteredAssets_spGetAdvancedSearch c ON a.AssetID = c.id
	WHERE a.Type = @cashflowField
	GROUP BY a.AssetID, a.Status 

	DECLARE cursor_schedulefilter_plan CURSOR FORWARD_ONLY READ_ONLY LOCAL
	FOR SELECT id, label FROM @cashflowPlanFilterTerms

	OPEN cursor_schedulefilter_plan

	DECLARE @cursor_id_plan INT
	DECLARE @cursor_label_plan NVARCHAR(255)

	FETCH NEXT FROM cursor_schedulefilter_plan 
	INTO @cursor_id_plan, @cursor_label_plan

	DECLARE @cursorlist_plan Base.tpIntList 

	WHILE @@FETCH_STATUS = 0
	BEGIN

		DELETE FROM @cursorlist_plan 

		IF @cursor_label_plan = '*'
		BEGIN
			INSERT INTO @cursorlist_plan(id)
			SELECT allplans.asset FROM @allPlans allplans WHERE allplans.status = @cursor_id_plan AND allplans.scheduledate IS NOT NULL AND allplans.scheduledate <> ''
		END
		ELSE IF @cursor_label_plan = 'null'
		BEGIN
			INSERT INTO @cursorlist_plan(id)
			SELECT id FROM #filteredAssets_spGetAdvancedSearch EXCEPT SELECT allplans.asset FROM @allPlans allplans WHERE allplans.status = @cursor_id_plan AND allplans.scheduledate IS NOT NULL AND allplans.scheduledate <> ''
		END 
		ELSE
		BEGIN
			INSERT INTO @cursorlist_plan(id)
			SELECT allplans.asset FROM @allPlans allplans WHERE allplans.status = @cursor_id_plan AND allplans.scheduledate LIKE '%' + @cursor_label_plan + '%'
		END 

		DELETE FROM #filteredAssets_spGetAdvancedSearch
		WHERE id NOT IN (select id from @cursorlist_plan)

		FETCH NEXT FROM cursor_schedulefilter_plan
		INTO @cursor_id_plan, @cursor_label_plan
	END 

END 

IF @cashflowField IS NOT NULL AND @cashflowActualFilter = 1
BEGIN
	DECLARE @allActuals TABLE(asset int, value int, scheduledate NVARCHAR(64))

	INSERT INTO @allActuals(asset,value, scheduledate)
	SELECT a.AssetID, a.Value, CONVERT(NVARCHAR(64), Min(a.Timestamp),101) 
	FROM CapPrior.tProjectCashFlow a
	INNER JOIN @cashflowActualFilterTerms b ON a.Value = b.Id
	INNER JOIN #filteredAssets_spGetAdvancedSearch c ON a.AssetID = c.id
	WHERE a.Type = @cashflowField
	GROUP BY a.AssetID, a.Value 

	DECLARE cursor_schedulefilter_actual CURSOR FORWARD_ONLY READ_ONLY LOCAL
	FOR SELECT id, label FROM @cashflowActualFilterTerms

	OPEN cursor_schedulefilter_actual

	DECLARE @cursor_id_actual INT
	DECLARE @cursor_label_actual NVARCHAR(255)

	FETCH NEXT FROM cursor_schedulefilter_actual
	INTO @cursor_id_actual, @cursor_label_actual

	DECLARE @cursorlist_actual Base.tpIntList 

	WHILE @@FETCH_STATUS = 0
	BEGIN

		DELETE FROM @cursorlist_actual 

		IF @cursor_label_actual = '*'
		BEGIN
			INSERT INTO @cursorlist_actual(id)
			SELECT allactuals.asset FROM @allActuals allactuals WHERE allactuals.value = @cursor_id_actual AND allactuals.scheduledate IS NOT NULL AND allactuals.scheduledate <> ''
		END
		ELSE IF @cursor_label_actual = 'null'
		BEGIN
			INSERT INTO @cursorlist_actual(id)
			SELECT id FROM #filteredAssets_spGetAdvancedSearch EXCEPT SELECT allactuals.asset FROM @allActuals allactuals WHERE allactuals.value = @cursor_id_actual AND allactuals.scheduledate IS NOT NULL AND allactuals.scheduledate <> ''
		END 
		ELSE
		BEGIN
			INSERT INTO @cursorlist_actual(id)
			SELECT allactuals.asset FROM @allActuals allactuals WHERE allactuals.value = @cursor_id_actual AND allactuals.scheduledate LIKE '%' + @cursor_label_actual + '%'
		END 

		DELETE FROM #filteredAssets_spGetAdvancedSearch
		WHERE id NOT IN (select id from @cursorlist_actual)

		FETCH NEXT FROM cursor_schedulefilter_actual
		INTO @cursor_id_actual, @cursor_label_actual
	END 
END

IF OBJECT_ID('tempdb..#sortTable_spGetAdvancedSearch') IS NOT NULL DROP TABLE #sortTable_spGetAdvancedSearch
CREATE TABLE #sortTable_spGetAdvancedSearch(id INT PRIMARY KEY CLUSTERED, val NVARCHAR(255), val2 INT, val3 INT)

--Here we are creating a table with all of the results.  The table will then be sorted.
--This way we don't have to create some elaborate mechanism to sort on different fields.  
IF @attributeSortID IS NOT NULL
BEGIN
	INSERT INTO #sortTable_spGetAdvancedSearch(id, val, val2, val3)
	SELECT asset.AssetID, COALESCE( attribute.Value, ''), act.AssetTypeID, asset.AssetID 
	FROM #searchSpace_spGetAdvancedSearch asset
	INNER JOIN #filteredAssets_spGetAdvancedSearch assetIds ON asset.AssetID = assetIds.id
	INNER JOIN Asset.tAssetClassType act ON asset.AssetClassTypeID = act.AssetClassTypeID
	LEFT JOIN ( 
				SELECT  
				CASE WHEN Asset.vAssetAttribute.Attribute_float IS NOT NULL THEN STR(Asset.vAssetAttribute.Attribute_float,100,30) 
		             WHEN Asset.vAssetAttribute.Attribute_int IS NOT NULL THEN STR(Asset.vAssetAttribute.Attribute_int,100,30) 
					 WHEN Asset.vAssetAttribute.Attribute_Date IS NOT NULL THEN CONVERT(varchar(255),Asset.vAssetAttribute.Attribute_Date,121)
		        ELSE Asset.vAssetAttribute.Value END as value, Asset.vAssetAttribute.AssetID FROM Asset.vAssetAttribute WHERE Asset.vAssetAttribute.AttributeTypeID = @attributeSortID 
    ) attribute ON asset.AssetID = attribute.AssetID 
END 
ELSE IF @cashflowField IS NOT NULL AND @cashflowState IS NOT NULL AND @cashflowUseValue IS NOT NULL
BEGIN
	INSERT INTO #sortTable_spGetAdvancedSearch(id, val, val2, val3)
	SELECT asset.AssetID, COALESCE(CONVERT(varchar(255),schedule.Value,102) , ''), act.AssetTypeID, asset.AssetID
	FROM #searchSpace_spGetAdvancedSearch asset
	INNER JOIN #filteredAssets_spGetAdvancedSearch assetIds ON asset.AssetID = assetIds.id
	INNER JOIN Asset.tAssetClassType act ON asset.AssetClassTypeID = act.AssetClassTypeID
	LEFT JOIN (
		SELECT flow.AssetID, Min(flow.Timestamp) as Value FROM CapPrior.tProjectCashFlow flow
		WHERE flow.Type = @cashFlowField AND ((@cashflowUseValue = 1 AND flow.Value = @cashFlowState) OR (@cashflowUseValue = 0 AND flow.Status = @cashFlowState))
		GROUP BY flow.AssetID 
	) schedule ON asset.AssetID = schedule.AssetID 

END
ELSE
BEGIN
	INSERT INTO #sortTable_spGetAdvancedSearch(id, val, val2, val3)
	SELECT asset.AssetID, asset.AssetAbbrev, act.AssetTypeID, asset.AssetID 
	FROM #searchSpace_spGetAdvancedSearch asset
	INNER JOIN #filteredAssets_spGetAdvancedSearch assetIds ON asset.AssetID = assetIds.id
	INNER JOIN Asset.tAssetClassType act ON asset.AssetClassTypeID = act.AssetClassTypeID
END;

SELECT @totalAssets = Count(*) FROM #sortTable_spGetAdvancedSearch;

--Remove events for assets no longer in the results
DELETE FROM #assetEvents_spGetAdvancedSearch WHERE assetId NOT IN (SELECT id FROM #sortTable_spGetAdvancedSearch);

--Get count for events in results; 15 per combination of asset and event type
WITH results AS
(
	SELECT
	ROW_NUMBER() OVER(PARTITION BY e.assetId, e.eventTypeDesc
	ORDER BY e.assetId) As RowNum
	FROM #assetEvents_spGetAdvancedSearch e
)
SELECT @totalEvents = Count(*)
FROM results
WHERE RowNum <= 15;

DECLARE @DisplayFormat nvarchar(100)
DECLARE @ORDERED TABLE(RowNum bigint,id INT PRIMARY KEY CLUSTERED, val NVARCHAR(255), val2 INT, val3 INT)
SELECT @DisplayFormat = UPPER(DisplayFormat) FROM Asset.tAttributeType WHERE AttributeTypeID = @attributeSortID

IF @DisplayFormat = 'INT'
BEGIN
INSERT INTO @ORDERED 
	SELECT ROW_NUMBER() OVER(ORDER BY CASE WHEN @sortOrder = 1 THEN CAST(TRY_CONVERT(DECIMAL,s.val) AS INT) END ASC, 
	CASE WHEN @sortOrder <> 1 THEN CAST(TRY_CONVERT(DECIMAL,s.val) AS INT) END DESC, s.val2, s.val3)AS RowNum, s.id, s.val, s.val2, s.val3 
	FROM #sortTable_spGetAdvancedSearch s
END
ELSE IF SUBSTRING(@DisplayFormat,1,1) = 'F' OR SUBSTRING(@DisplayFormat,1,1) = 'P'
BEGIN
INSERT INTO @ORDERED 
	SELECT ROW_NUMBER() OVER(ORDER BY CASE WHEN @sortOrder = 1 THEN TRY_CONVERT(DECIMAL(30,10),s.val)END ASC, 
	CASE WHEN @sortOrder <> 1 THEN TRY_CONVERT(DECIMAL(30,10),s.val) END DESC, s.val2, s.val3)AS RowNum, s.id, s.val, s.val2, s.val3 
	FROM #sortTable_spGetAdvancedSearch s
END
ELSE
BEGIN
INSERT INTO @ORDERED
SELECT ROW_NUMBER() OVER(ORDER BY CASE WHEN @sortOrder = 1 THEN
	s.val 
	END ASC,
	CASE WHEN @sortOrder <> 1 THEN
	s.val 
	END DESC, s.val2, s.val3)
	AS RowNum, s.id, s.val, s.val2, s.val3 FROM #sortTable_spGetAdvancedSearch s
END
INSERT INTO @result( AssetID, RowNum) 
SELECT id, RowNum
FROM @ORDERED 
WHERE (RowNum-1) >= @page * @pageSize AND (RowNum-1) < (@page + 1) * @pageSize


SELECT RowNum, AssetID FROM @result
ORDER BY RowNum;

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spGetAdvancedSearch] TO [TEUser]
    AS [dbo];
GO



