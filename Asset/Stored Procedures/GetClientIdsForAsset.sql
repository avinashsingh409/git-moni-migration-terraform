﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Asset].[GetClientIdsForAsset]
	-- Add the parameters for the stored procedure here
	@assetId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ClientID, AssetID
	FROM Asset.ufnGetClientsForAsset(@assetId)

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[GetClientIdsForAsset] TO [TEUser]
    AS [dbo];

