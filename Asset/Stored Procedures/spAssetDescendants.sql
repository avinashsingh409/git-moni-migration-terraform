﻿CREATE PROCEDURE [Asset].[spAssetDescendants] 
	-- Add the parameters for the stored procedure here
	@userID int,
	@parentID int = NULL,
	@appContextID int = NULL,
	@classTypes [Base].[tpIntList] READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @myClassTypes TABLE
	(
		[id] [int] NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (IGNORE_DUP_KEY = OFF)
	)

	IF EXISTS (SELECT 1 FROM @classTypes)
	BEGIN 
		INSERT INTO @myClassTypes SELECT id from @classTypes;
	END 
	ELSE
	BEGIN
		INSERT INTO @myClassTypes SELECT AssetClassTypeID FROM Asset.tAssetClassType
	END

	SELECT 
		a.AssetID,
		a.ParentAssetID,
		a.AssetAbbrev,
		a.AssetDesc,
		a.AssetClassTypeID,
		a.AssetTreeNodeChildConfigurationTypeID,
		a.DisplayOrder,
		a.CreatedBy,
		a.ChangedBy,
		a.CreateDate,
		a.ChangeDate,
		a.GlobalID,
		a.IsHidden,
		children.NumChildren,
		a.AssetClassTypeID,
		act.AssetTypeID,
		act.AssetClassTypeKey,
		act.AssetClassTypeAbbrev,
		act.AssetClassTypeDesc,
		act.DisplayOrder,
		ast.AssetTypeAbbrev,
		ast.AssetTypeDesc,
		attLong.longitude,
		attLat.latitude,
		attNetMW.netMW,
		issues.openIssues,
		tagMaps.sensors,
		allTags.tags
	 FROM Asset.tAsset a
	INNER JOIN Asset.ufnGetAssetTreeBranch(@parentID) branch on a.AssetID = branch.AssetID
	INNER JOIN [UIConfig].[ufnGetAssetIdsAllowedForUserIdAndAppContext](@userID,@appContextID) userassets on a.AssetID = userassets.AssetID
	INNER JOIN @myClassTypes ct on  a.AssetClassTypeID = ct.id
	LEFT JOIN (SELECT AssetID, Attribute_float longitude FROM  Asset.tAssetAttribute WHERE AttributeTypeID = 154)  attLong on a.AssetID = attLong.AssetID 
	LEFT JOIN (SELECT AssetID, Attribute_float latitude FROM  Asset.tAssetAttribute WHERE AttributeTypeID = 153)  attLat on a.AssetID = attLat.AssetID
	LEFT JOIN (SELECT AssetID, Attribute_float netMW FROM  Asset.tAssetAttribute WHERE AttributeTypeID = 106)  attNetMW on a.AssetID = attNetMW.AssetID
	LEFT JOIN (SELECT ParentAssetID, Count(*) as NumChildren FROM Asset.tAsset WHERE ParentAssetID is not null GROUP BY ParentAssetID) children on a.AssetID = children.ParentAssetID
	INNER JOIN Asset.tAssetClassType act on a.AssetClassTypeID = act.AssetClassTypeID
	INNER JOIN Asset.tAssetType ast on act.AssetTypeID = ast.AssetTypeID 
	LEFT JOIN (SELECT AssetID, COUNT(*) openIssues FROM Diagnostics.tAssetIssue WHERE ActivityStatusID = 1 GROUP BY AssetID) issues on a.AssetID = issues.AssetID
	LEFT JOIN (SELECT AssetID, Count(*) sensors FROM ProcessData.tAssetVariableTypeTagMap WHERE VariableTypeID IS NOT NULL AND TagID IS NOT NULL GROUP BY AssetID ) tagMaps on a.AssetID = tagMaps.AssetID
	LEFT JOIN (SELECT AssetID, Count(*) tags FROM ProcessData.tAssetVariableTypeTagMap WHERE TagID IS NOT NULL GROUP BY AssetID ) allTags on a.AssetID = allTags.AssetID
	WHERE a.IsHidden = 0
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spAssetDescendants] TO [TEUser]
    AS [dbo];

