﻿CREATE PROCEDURE [Asset].[spSaveQuickSearchSelectedEventTypes]
  @QuickSearchID as INT,
  @EventAssetClassTypeIDs Base.tpIntList READONLY
AS
BEGIN
	DELETE FROM [Asset].[tQuickSearchSelectedEventTypes] WHERE QuickSearchID = @QuickSearchID;
	INSERT INTO [Asset].[tQuickSearchSelectedEventTypes] (QuickSearchID,EventAssetClassTypeID) 
	SELECT @QuickSearchID,ID FROM @EventAssetClassTypeIDs;
	RETURN;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveQuickSearchSelectedEventTypes]  TO [TEUser]
    AS [dbo];
GO