﻿-- spUpsertOpStateEvent
create procedure [Asset].[spUpsertOpStateEvents]
	@OpStateEvents Asset.tpOpStateEvent readonly,
	@SecurityUserID int
as
begin
	set nocount on;

	-- inserts new events and updates existing events.
	-- events to insert are events with GlobalIDs that aren't found in the Asset.tOpStateEvent table;
	-- events to update are events with GlobalIDs that are    found in the Asset.tOpStateEvent table.
	-- returns the upserted tOpStateEvent records

	declare @now datetime = getdate();

	declare @upsertedOpStateEvents table
	(
		EventID int not null
	);
	insert into Asset.tOpStateEvent (AssetID, EventTypeID, StartTime, EndTime, Impact, [Description], ModifiedByEventID, IsDeleted, CreatedBy, ChangedBy, CreateDate, ChangeDate)
	output inserted.EventID into @upsertedOpStateEvents
	select e.AssetID, e.EventTypeID, e.StartTime, e.EndTime, e.Impact, e.[Description], e.ModifiedByEventID, e.IsDeleted, @securityUserID, @securityUserID, @now, @now
	from @OpStateEvents e left join Asset.tOpStateEvent o on e.GlobalID = o.GlobalID where o.GlobalID is null;

	update o
	set
		o.AssetID = e.AssetID
		, o.EventTypeID = e.EventTypeID
		, o.StartTime = e.StartTime
		, o.EndTime = e.EndTime
		, o.Impact = e.Impact
		, o.[Description] = e.[Description]
		, o.ModifiedByEventID = e.ModifiedByEventID
		, o.IsDeleted = e.IsDeleted
		, o.ChangedBy = @securityUserID
		, o.ChangeDate = @now
	output inserted.EventID into @upsertedOpStateEvents
	from Asset.tOpStateEvent o
	join @OpStateEvents e on o.GlobalID = e.GlobalID;

	select u.EventID, u.GlobalID, u.AssetID, u.EventTypeID, u.StartTime, u.EndTime, u.Impact, u.[Description], u.ModifiedByEventID, u.IsDeleted, createdby.Username as CreatedBy, changedby.Username as ChangedBy, u.CreateDate, u.ChangeDate
	from Asset.tOpStateEvent u join @upsertedOpStateEvents o on u.EventID = o.EventID
	join AccessControl.tUser createdby on u.CreatedBy = createdby.SecurityUserID
	join AccessControl.tUser changedby on u.ChangedBy = changedby.SecurityUserID;
end
go
grant execute on object::[Asset].[spUpsertOpStateEvents] to [TEUser] as [dbo];
go