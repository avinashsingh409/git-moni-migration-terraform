﻿CREATE PROCEDURE [Asset].[spGetCategoriesForMultipleAssetAttribute]
    @lookup  Base.tpIntIntPair READONLY,
	@securityUserID INT
AS
  BEGIN

    CREATE TABLE #assetIDs (      
      AssetID INT      
    )
		
    CREATE TABLE #query (      
      AssetID INT,      
      AttributeTypeID INT,
	  AttributeTypeDesc NVARCHAR(255)
    )

    CREATE TABLE #client (  
      AssetID INT,	
	  ClientID INT,    
	  AncestryGenerationsAway INT
    )

    CREATE TABLE #temp (  
	  AttributeTypeID INT,  
	  AttributeTypeDesc NVARCHAR(255),
	  CategoryID INT,
	  ClientID INT, 
      [Name] VARCHAR(255),    
	  CreatedByUserID INT,      
	  ChangedByUserID INT,
	  CreateDate DATETIME2(7),      
	  ChangeDate DATETIME2(7)
    )

	INSERT INTO #query (AssetId, AttributeTypeID, AttributeTypeDesc) 
		SELECT DISTINCT l.Id1, l.Id2, t.AttributeTypeDesc
		FROM @lookup l
		INNER JOIN Asset.tAttributeType t ON t.AttributeTypeID = l.Id2

	INSERT INTO #assetids SELECT DISTINCT AssetID FROM #query

	--Find clients of asset's ancestors
	INSERT INTO #client	(AssetID,ClientID,AncestryGenerationsAway)
		SELECT  a.AssetID, tc.ClientID, (ah.AncestryLevel * -1) 
		FROM #assetids a 	
		INNER JOIN Asset.tAsset ta ON ta.AssetID = a.AssetID AND ta.IsHidden = 0 
		INNER JOIN Asset.tAssetHop ah ON ta.AssetID = ah.EndingAssetId AND ah.AncestryLevel < 0
		INNER JOIN Asset.tClient tc ON ah.HopAssetId = tc.AssetID
	
	--Find client for asset
	INSERT INTO #client	(AssetID,ClientID,AncestryGenerationsAway)
		SELECT tc.AssetID, tc.ClientID, 0
		FROM #assetids a 
		INNER JOIN Asset.tClient tc ON a.AssetId = tc.AssetID

	-- Don't return any attributes-categories that belong to a category that the user does not have access to
	DELETE q
		FROM #query q
		INNER JOIN Asset.tAssetAttributeCategoryMap map ON q.AttributeTypeID = map.AttributeTypeID
		INNER JOIN Asset.tCategory cat ON map.CategoryID = cat.CategoryID
		INNER JOIN #client cl ON cat.ClientID = cl.ClientID
		INNER JOIN Asset.tCategoryTypeMap ctm ON cat.CategoryID = ctm.CategoryID AND ctm.CategoryTypeID = 1
			AND cat.CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
			AND cat.CategoryID NOT IN
			(
				SELECT CategoryID FROM AccessControl.tRoleCategoryMap r
				INNER JOIN AccessControl.tUserRoleMap urm ON r.SecurityRoleID = urm.SecurityRoleID
				WHERE urm.SecurityUserID = @securityUserID
			)
		WHERE q.AttributeTypeID NOT IN 
		(
			SELECT AttributeTypeID
			FROM Asset.tAttributeType
			WHERE IsExempt = 1
		)

    --First find global categories
    INSERT INTO #temp (AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
		SELECT q.AttributeTypeID, q.AttributeTypeDesc, cat.CategoryID, NULL, cat.[Name], cat.CreatedByUserID, cat.ChangedByUserID, cat.CreateDate, cat.ChangeDate 
		FROM Asset.tCategory cat 
		JOIN Asset.tAssetAttributeCategoryMap map ON cat.CategoryID = map.CategoryID 
		JOIN #query q on map.AttributeTypeID = q.AttributeTypeID
		JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID 
		WHERE d.CategoryTypeID = 1 AND cat.ClientID IS NULL

    --then add rest of associated categories
	INSERT INTO #temp (AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
		SELECT q.AttributeTypeID, q.AttributeTypeDesc, cat.CategoryID, cat.ClientID, cat.[Name], cat.CreatedByUserID, cat.ChangedByUserID, cat.CreateDate, cat.ChangeDate  
		FROM #query q 
		JOIN #client cl on q.AssetID = cl.AssetID
		JOIN Asset.tCategory cat ON cl.ClientID = cat.ClientID
		JOIN Asset.tAssetAttributeCategoryMap map ON cat.CategoryID = map.CategoryID and map.AttributeTypeID = q.AttributeTypeID		
		JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID 
		WHERE d.CategoryTypeID = 1 
	
	--finally add the attributes without associated categories (security exluded attributes already removed)
	INSERT INTO #temp (AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)  
		SELECT q.AttributeTypeID, q.AttributeTypeDesc, NULL, NULL, NULL, NULL, NULL, NULL, NULL
		FROM #query q
		WHERE q.AttributeTypeID NOT IN (SELECT AttributeTypeID FROM #temp)

	SELECT AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate
	FROM 
		( SELECT AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate,
				 ROW_NUMBER() OVER (PARTITION BY AttributeTypeID,CategoryID ORDER BY AttributeTypeID,CategoryID) AS rn                         
		  FROM #temp) tmp 
		WHERE rn = 1 ;
	
	RETURN
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spGetCategoriesForMultipleAssetAttribute] TO [TEUser]
    AS [dbo];


