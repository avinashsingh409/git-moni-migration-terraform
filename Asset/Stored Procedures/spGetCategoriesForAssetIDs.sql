﻿CREATE PROCEDURE [Asset].[spGetCategoriesForAssetIDs]
    @AssetIDs  Base.tpIntList READONLY,
	@securityUserID INT
AS
  BEGIN

  SET NOCOUNT ON

    CREATE TABLE #assetIDs (      
      AssetID INT      
    )
		
    CREATE TABLE #assetattributes (      
      AssetID INT,      
      AttributeTypeID INT
    )

	insert into #assetattributes
	select isnull(assetID,0),a.attributeTypeID 
	from [Asset].[ufnGetAttributeTypeIDsForAssets](@assetIds) a JOIN Asset.tAttributeType b on a.AttributeTypeID = b.AttributeTypeID 

	CREATE INDEX #idx_1 ON #assetattributes (AssetID,AttributeTypeID) 

	CREATE TABLE #attributes (
	  AttributeTypeID INT
	)

	insert into #attributes select distinct attributeTypeID from #assetattributes where AttributeTypeID >0

	CREATE INDEX #idx_1 ON #attributes (AttributeTypeID) 
	
    CREATE TABLE #client (  
      AssetID INT,	
	  ClientID INT,    
	  AncestryGenerationsAway INT
    )

	CREATE INDEX #idx_1 ON #client (AssetID,ClientID)

    CREATE TABLE #temp (  
	  AttributeTypeID INT,  
	  AttributeTypeDesc NVARCHAR(255),
	  CategoryID INT,
	  ClientID INT, 
      [Name] VARCHAR(255),    
	  CreatedByUserID INT,      
	  ChangedByUserID INT,
	  CreateDate DATETIME2(7),      
	  ChangeDate DATETIME2(7)
    )

	INSERT INTO #assetids SELECT DISTINCT AssetID FROM #assetattributes

	--Find clients of asset's ancestors
	INSERT INTO #client	(AssetID,ClientID,AncestryGenerationsAway)
		SELECT  a.AssetID, tc.ClientID, (ah.AncestryLevel * -1) 
		FROM #assetids a 	
		INNER JOIN Asset.tAssetHop ah ON a.AssetID = ah.EndingAssetId AND ah.AncestryLevel < 0
		INNER JOIN Asset.tClient tc ON ah.HopAssetId = tc.AssetID
	
	--Find client for asset
	INSERT INTO #client	(AssetID,ClientID,AncestryGenerationsAway)
		SELECT tc.AssetID, tc.ClientID, 0
		FROM #assetids a 
		INNER JOIN Asset.tClient tc ON a.AssetId = tc.AssetID

	CREATE TABLE #clients 
	(
	   ClientID int Primary Key
	)

	INSERT INTO #clients SELECT DISTINCT ClientID FROM #client

	-- Don't return any attributes-categories that belong to a category that the user does not have access to
	DELETE q
		FROM #attributes q
		INNER JOIN Asset.tAssetAttributeCategoryMap map ON q.AttributeTypeID = map.AttributeTypeID
		INNER JOIN Asset.tCategory cat ON map.CategoryID = cat.CategoryID
		INNER JOIN #clients cl ON cat.ClientID = cl.ClientID
		INNER JOIN Asset.tCategoryTypeMap ctm ON cat.CategoryID = ctm.CategoryID AND ctm.CategoryTypeID = 1
			AND cat.CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
			AND cat.CategoryID NOT IN
			(
				SELECT CategoryID FROM AccessControl.tRoleCategoryMap r
				INNER JOIN AccessControl.tUserRoleMap urm ON r.SecurityRoleID = urm.SecurityRoleID
				WHERE urm.SecurityUserID = @securityUserID
			)
		WHERE q.AttributeTypeID NOT IN 
		(
			SELECT AttributeTypeID
			FROM Asset.tAttributeType
			WHERE IsExempt = 1
		)

    --First find global categories
    INSERT INTO #temp (AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
		SELECT q.AttributeTypeID, aType.AttributeTypeDesc, cat.CategoryID, NULL, cat.[Name], cat.CreatedByUserID, cat.ChangedByUserID, cat.CreateDate, cat.ChangeDate 
		FROM Asset.tCategory cat 
		JOIN Asset.tAssetAttributeCategoryMap map ON cat.CategoryID = map.CategoryID 
		JOIN #attributes q on map.AttributeTypeID = q.AttributeTypeID
		JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID 
		JOIN Asset.tAttributeType aType ON q.AttributeTypeID = aType.AttributeTypeID
		WHERE d.CategoryTypeID = 1 AND cat.ClientID IS NULL	

    --then add rest of associated categories
     INSERT INTO #TEMP (AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
		SELECT att.AttributeTypeID, atype.AttributeTypeDesc, cat.CategoryID, cat.ClientID, cat.[Name], cat.CreatedByUserID, cat.ChangedByUserID, cat.CreateDate, cat.ChangeDate  
		FROM #attributes att
		JOIN Asset.tAssetAttributeCategoryMap map ON att.AttributeTypeID = map.AttributeTypeID	
		JOIN Asset.tCategory cat ON map.CategoryID = cat.CategoryID		
		JOIN Asset.tCategoryTypeMap d ON cat.CategoryID = d.CategoryID 
		JOIN Asset.tAttributeType atype on att.AttributeTypeID = atype.AttributeTypeID
		JOIN #clients client on cat.ClientID = client.ClientID
		WHERE d.CategoryTypeID = 1 

	--finally add the attributes without associated categories (security exluded attributes already removed)
	INSERT INTO #temp (AttributeTypeID, AttributeTypeDesc, CategoryID, ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)  
		SELECT DISTINCT att.AttributeTypeID, aType.AttributeTypeDesc, NULL, NULL, NULL, NULL, NULL, NULL, NULL
		FROM #attributes att JOIN Asset.tAttributeType aType on att.AttributeTypeID = aType.AttributeTypeID
		WHERE att.AttributeTypeID NOT IN (SELECT AttributeTypeID FROM #temp)

	SELECT tmp.AttributeTypeID, tmp.AttributeTypeDesc, tmp.CategoryID, tmp.ClientID, tmp.[Name], tmp.CreatedByUserID, tmp.ChangedByUserID, tmp.CreateDate, tmp.ChangeDate 
	FROM 
		( SELECT b.AttributeTypeID, b.AttributeTypeDesc, CategoryID, b.ClientID, [Name], CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate,
				 ROW_NUMBER() OVER (PARTITION BY b.AttributeTypeID,CategoryID ORDER BY b.AttributeTypeID,CategoryID) AS rn                         
		  FROM #temp a JOIN Asset.tAttributeType b on a.AttributeTypeID = b.AttributeTypeID) tmp 
		WHERE rn = 1 ;

	RETURN
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spGetCategoriesForAssetIDs] TO [TEUser]
    AS [dbo];

