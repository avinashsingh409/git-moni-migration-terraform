
CREATE PROCEDURE [Asset].[spGetKeywordsForAsset]
	@AssetID int,
	@UserID int
AS
BEGIN
	
	SELECT k.Text
    FROM Asset.tAsset a
    INNER JOIN Asset.tAssetKeywordMap m ON m.AssetID = a.AssetID
    INNER JOIN Asset.tKeyword k ON k.KeywordID = m.KeywordID
    WHERE a.AssetID = @AssetID AND Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @UserID, -1) != 0
    ORDER BY 1
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spGetKeywordsForAsset] TO [TEUser]
    AS [dbo];

