﻿CREATE PROCEDURE Asset.spUnlockGroup
@LockGroup as uniqueidentifier
AS  
BEGIN

UPDATE Asset.tClient SET Locked = 0, LockReasonID = NULL, LockedBy = NULL,LockedSinceTime = NULL, LockGroup = NULL where LockGroup = @LockGroup

END

GO

GRANT EXECUTE
    ON OBJECT::Asset.spUnlockGroup TO [TEUser]
    AS [dbo];

GO
