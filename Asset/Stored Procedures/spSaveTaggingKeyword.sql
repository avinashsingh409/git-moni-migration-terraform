﻿

CREATE PROCEDURE [Asset].[spSaveTaggingKeyword]
	@assetId	int,
	@text		nvarchar(255),
	@securityUserId int,
	@keywordId	bigint = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (@text IS NULL OR REPLACE(@text,' ', '') = '') BEGIN
		-- System reserved keyword
		RAISERROR('CANNOT_SAVE:Empty strings are not allowed as keywords. Save denied.', 16, 1)
		RETURN;
	END

	DECLARE @clients TABLE(
		  ClientId int,
		  AssetId int,
		  AncestryGenerationsAway int
	)

	INSERT INTO @clients(ClientId, AssetId, AncestryGenerationsAway)
	SELECT ugcfa.ClientID, ugcfa.AssetID, ugcfa.AncestryGenerationsAway
	FROM [Asset].ufnGetClientsForAsset(@assetId) ugcfa
	
	IF(NOT EXISTS(SELECT 1 FROM @clients c)) BEGIN
		RAISERROR('CANNOT_SAVE:Insufficient permissions.', 16, 1)
		RETURN;
	END

	DECLARE @clientId int;

	DECLARE @id bigint;
	SET @id = null;
	
	DECLARE @isSystemReservedKeyword bit;
	SET @isSystemReservedKeyword = 0;

	SELECT top 1 @id = tk.KeywordID, @isSystemReservedKeyword = tk.SystemReserved
	FROM @clients c
	INNER JOIN Asset.tKeyword tk ON c.ClientID=tk.ClientID
	WHERE tk.Text = @text
	ORDER BY tk.SystemReserved DESC
	
	IF (@id IS NOT NULL) BEGIN
		IF (@isSystemReservedKeyword=1) BEGIN
			-- System reserved keyword
			RAISERROR('CANNOT_SAVE:Attempted to use a system reserved keyword. Save denied.', 16, 1)
			RETURN;
		END ELSE BEGIN
			-- Already exists with SystemReserved = 0
			SET @keywordId = @id
			RETURN;
		END
	END
	
	SELECT TOP 1 @clientId = c.ClientID
	FROM @clients c
	ORDER BY c.AncestryGenerationsAway asc
	
	IF (@clientId IS NULL) BEGIN
		  RAISERROR('CANNOT_SAVE:Unable to determine client id for asset id. Save aborted.', 16, 1)
		  RETURN;
	END

	INSERT INTO Asset.tKeyword	(ClientID, Text, DateCreated, CreatedByUserID, ChangedByUserID)
	VALUES (@clientId, @text, GetDate(), case when @securityUserId = -1 then null else @securityUserId end, case when @securityUserId = -1 then null else @securityUserId end)
	SET @keywordId = SCOPE_IDENTITY();

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveTaggingKeyword] TO [TEUser]
    AS [dbo];
GO
