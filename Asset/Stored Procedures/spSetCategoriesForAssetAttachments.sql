﻿CREATE PROCEDURE [Asset].[spSetCategoriesForAssetAttachments]
	@attachmentCategoryMap Base.tpGuidIntList READONLY
AS
BEGIN
	-- make sure the guids are valid
	DECLARE @updatedList AS Base.tpGuidIntList
	INSERT INTO @updatedList (GuidID, IntID)
	SELECT DISTINCT a.AssetAttachmentID, b.IntID
	FROM Asset.tAssetAttachment a INNER JOIN @attachmentCategoryMap b on a.AssetAttachmentID = b.GuidID OR a.Head = b.GuidID

	DELETE FROM Asset.tAssetAttachmentCategoryMap
	WHERE AssetAttachmentID IN (SELECT DISTINCT GuidID FROM @updatedList)

	INSERT INTO Asset.tAssetAttachmentCategoryMap (AssetAttachmentID, CategoryID)
	SELECT GuidID, IntID FROM @updatedList
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSetCategoriesForAssetAttachments]  TO [TEUser]
    AS [dbo];
GO
