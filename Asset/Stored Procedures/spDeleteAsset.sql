﻿CREATE PROCEDURE [Asset].[spDeleteAsset] (@assetId INT, @onlyDeleteDescendants INT = 0)
AS

BEGIN

DECLARE @TranName VARCHAR(40);
SELECT @TranName = 'Delete asset descendants';

BEGIN TRANSACTION @TranName;

BEGIN TRY

declare @done as bit = 0
declare @loops as int = 0
declare @count as int

-- need to do this to pick up the hidden assets
select * into #branch from asset.ufnGetAssetTreeBranch(@assetId)

declare @lastcnt as int = 0
declare @cnt as int = -1
while @lastcnt!=@cnt
  begin
  insert into #branch select distinct b.AssetID from Asset.tAsset b where b.IsHidden = 1 and b.ParentAssetID in (select AssetID from #branch)  and b.AssetID not in (select AssetID from #branch)
  set @lastcnt = @cnt
  select @cnt = COUNT(*) from #branch
  end

while @done=0
  begin  
  select a.assetid into #leaves from #branch a join asset.tAsset b on a.AssetID = b.assetid 
  where a.AssetID not in (select ParentAssetID from asset.tAsset where ParentAssetID is not null)  
  
  if @onlyDeleteDescendants=1
    BEGIN
    delete from #leaves where AssetID = @assetId
    END
  
  
  print 'loop'

  select AssetIssueID into #issues from Diagnostics.tAssetIssue where AssetID in (select AssetID FROM #leaves)

    delete this from 
	Diagnostics.tAssetIssueRelatedAssetIssue this 
	 join #issues i on this.AssetIssueID = i.AssetIssueID 

    delete this from 
	Diagnostics.tAssetIssueRelatedAssetIssue this 
	 join #issues i on this.RelatedAssetIssueID = i.AssetIssueID 
	 
	delete this from 
	Diagnostics.tAssetIssueImpactScenario this join 
	Diagnostics.tAssetIssueAssetIssueImpactTypeMap map 
	on this.Efficiency_AssetIssueAssetIssueImpactTypeMapID = map.AssetIssueAssetIssueImpactTypeMapID
	 join #issues i on map.AssetIssueID = i.AssetIssueID 
	 
	 delete this from 
	Diagnostics.tAssetIssueImpactScenario this join 
	Diagnostics.tAssetIssueAssetIssueImpactTypeMap map 
	on this.Generation_AssetIssueAssetIssueImpactTypeMapID = map.AssetIssueAssetIssueImpactTypeMapID
	 join #issues i on map.AssetIssueID = i.AssetIssueID 
	 
	delete this from 
	Diagnostics.tAssetIssueImpactScenario this join 
	Diagnostics.tAssetIssueAssetIssueImpactTypeMap map 
	on this.OtherCosts_AssetIssueAssetIssueImpactTypeMapID = map.AssetIssueAssetIssueImpactTypeMapID
	 join #issues i on map.AssetIssueID = i.AssetIssueID  
	 
	delete this from 
	Diagnostics.tAssetIssueImpactScenario this join 
	Diagnostics.tAssetIssueAssetIssueImpactTypeMap map 
	on this.Maintenance_AssetIssueAssetIssueImpactTypeMapID = map.AssetIssueAssetIssueImpactTypeMapID
	 join #issues i on map.AssetIssueID = i.AssetIssueID 

  delete mi from Monitoring.tModelInput mi JOIN ProcessData.tAssetVariableTypeTagMap map on mi.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID 
  join #leaves l on map.AssetID = l.AssetID
  
  delete u from Monitoring.tUnit u join #leaves l on u.UnitAssetID = l.AssetID
 
  delete u from Monitoring.tUnit u join ProcessData.tServer s on u.PDServerID = s.PDServerID where 
    s.AssetID in (select AssetID from #leaves)

  delete u from Monitoring.tUnit u join ProcessData.tAssetVariableTypeTagMap map on u.LoadSensorID = map.AssetVariableTypeTagMapID where 
    map.AssetID in (select AssetID from #leaves)
  
  delete u from Asset.tUnit u where 
    u.AssetID in (select AssetID from #leaves)

  delete u from Asset.tUnit u join Asset.tStation s on u.StationID = s.StationID  where 
    s.AssetID in (select AssetID from #leaves)

  print 'deleting tag0'

  delete ann from ProcessData.ttag a join ProcessData.tServer b on a.PDServerID = b.PDServerID join #leaves c on b.AssetID = C.assetid join ProcessData.tAssetVariableTypeTagMap map on a.PDTagID = map.TagID join 
    ProcessData.tAnnotation ann on map.TagID = ann.TagID

  delete ann from ProcessData.ttag a join ProcessData.tServer b on a.PDServerID = b.PDServerID join #leaves c on b.AssetID = C.assetid join ProcessData.tTag t on b.PDServerID = t.PDServerID join 
    ProcessData.tAnnotation ann on t.PDTagID = ann.TagID

  delete m from Monitoring.tNDModel m join ProcessData.tAssetVariableTypeTagMap map on m.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID where 
    map.AssetID in (select AssetID from #leaves)
        
  delete s from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tSQLHistorian s on t.PDTagID = s.TagID 
          join #leaves l on map.AssetID = l.AssetID


  print 'deleting tag1'
  delete s from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID1
          join #leaves l on map.AssetID = l.AssetID

  delete s from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID2
          join #leaves l on map.AssetID = l.AssetID

  delete s from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID3
          join #leaves l on map.AssetID = l.AssetID

  delete s from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID4
          join #leaves l on map.AssetID = l.AssetID
 
  delete s from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID5
          join #leaves l on map.AssetID = l.AssetID
  
  print 'deleting band axis measurements'
  delete s from ProcessData.tTag t join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID1 JOIN ProcessData.tServer v on t.PDServerID = v.PDServerID
          join #leaves l on v.AssetID = l.AssetID

  delete s from ProcessData.tTag t join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID2 JOIN ProcessData.tServer v on t.PDServerID = v.PDServerID
          join #leaves l on v.AssetID = l.AssetID

  delete s from ProcessData.tTag t join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID3 JOIN ProcessData.tServer v on t.PDServerID = v.PDServerID
          join #leaves l on v.AssetID = l.AssetID

  delete s from ProcessData.tTag t join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID4 JOIN ProcessData.tServer v on t.PDServerID = v.PDServerID
          join #leaves l on v.AssetID = l.AssetID

  delete s from ProcessData.tTag t join ProcessData.tTrendBandAxisMeasurement s on t.PDTagID = s.TagID5 JOIN ProcessData.tServer v on t.PDServerID = v.PDServerID
          join #leaves l on v.AssetID = l.AssetID

  delete map from ProcessData.tAssetVariableTypeTagMap map join ProcessData.tTag t on map.TagID = t.PDTagID join ProcessData.tServer s on t.PDServerID = s.PDServerID 
          join #leaves l on map.AssetID = l.AssetID
  
  delete map from ProcessData.ttag a join ProcessData.tServer b on a.PDServerID = b.PDServerID join #leaves c on b.AssetID = C.assetid join ProcessData.tAssetVariableTypeTagMap map on a.PDTagID = map.TagID

  delete from ProcessData.tAssetVariableTypeTagMap where AssetID in (select AssetID from #leaves)
  
  delete a from ProcessData.tTrendBandAxisMeasurement a join  ProcessData.tTrendBandAxisBar b on a.BarID = b.TrendBandAxisBarID where assetid in (select AssetID from #leaves) 

  delete from ProcessData.tTrendBandAxisBar where assetid in (select AssetID from #leaves) 

  select convert(int,ScenarioId) as ScenarioID into #scenarios from projection.tScenario where OwningAssetID in (select AssetID from #leaves)
  insert into #scenarios (ScenarioID) select s.ScenarioID from Projection.tScenario s JOIN Projection.tGenerationSet a on s.BaseGenerationSetID = a.GenerationSetID where a.OwningAssetID in (select assetid from #leaves)
  insert into #scenarios (ScenarioID) select s.ScenarioID from Projection.tScenario s JOIN Projection.tUnitConfigScheduleSet a on s.BaseUnitConfigScheduleSetID = a.UnitConfigScheduleSetID where a.OwningAssetID in (select assetid from #leaves)
  insert into #scenarios (ScenarioID) select s.ScenarioID from Projection.tScenario s JOIN Projection.tFuelPlanSet a on s.BaseFuelPlanSetID = a.FuelPlanSetID where a.OwningAssetID in (select assetid from #leaves)
  
  delete x from AdaptivePlanning.tAnalysisSet x where x.BaseScenarioID in (select scenarioid from #scenarios)
  delete from AdaptivePlanning.tAnalysisSet where OwningAssetID in (select AssetID from #leaves)
  
  delete from Projection.tScenario where ScenarioID in (select ScenarioID from #scenarios)
      
  delete from Projection.tGenerationSet where OwningAssetID in (select AssetID from #leaves)    
  delete from Projection.tFuelPlanSet where OwningAssetID in (select AssetID from #leaves)    
  delete from PowerRAM.tMaintenancePlanSet where OwningAssetID in (select AssetID from #leaves)    
  delete from Projection.tUnitConfigScheduleSet where OwningAssetID in (select AssetID from #leaves)
      
  print 'deleting energy storage performance sets'
  delete x from EnergyStorage.tPerformanceSet x JOIN EnergyStorage.tGenerationDataSet g on x.GenDataSetID = g.SetID where g.AssetID in (select AssetID from #leaves)
  delete x from EnergyStorage.tPerformanceSet x JOIN EnergyStorage.tFrequencyDataSet g on x.FreqDataSetID = g.FrequencyDataSetID where g.AssetID in (select AssetID from #leaves)
  delete x from EnergyStorage.tPerformanceSet x JOIN EnergyStorage.tBattery g on x.BatteryID = g.BatteryID where g.AssetID in (select AssetID from #leaves)
  
  print 'deleting rest of energy storage'
  delete from EnergyStorage.tGenerationDataSet where AssetID in (select AssetID from #leaves)  
  delete from EnergyStorage.tFrequencyDataSet where AssetID in (select AssetID from #leaves)   
  delete from EnergyStorage.tPerformanceSet where AssetID in (select AssetID from #leaves)     
  
  print 'deleting batteries'
  delete x from EnergyStorage.tBattery x JOIN EnergyStorage.tPerformanceSet p on x.BatteryID = p.BatteryID where p.AssetID in (select AssetID from #leaves)
  delete x from EnergyStorage.tBattery x JOIN EnergyStorage.tPerformanceSet p on x.BatteryID = p.BatteryID  join EnergyStorage.tGenerationDataSet g on p.GenDataSetID = g.SetID where g.AssetID in (select AssetID from #leaves)
  delete x from EnergyStorage.tBattery x JOIN EnergyStorage.tPerformanceSet p on x.BatteryID = p.BatteryID  join EnergyStorage.tFrequencyDataSet g on p.FreqDataSetID = g.FrequencyDataSetID where g.AssetID in (select AssetID from #leaves)
     
  print 'deleting criteria objects'
  delete from Criteria.tCO where CoDSID in (select CoDSID from Criteria.tCoDS where AssetID in (select AssetID from #leaves))
  delete from Criteria.tCO where CoDFID in (select CoDFID from Criteria.tCoDF where AssetID in (select AssetID from #leaves))
  delete from Criteria.tCoDS where AssetID in (select AssetID from #leaves)  
  delete from Criteria.tCoDF where AssetID in (select AssetID from #leaves)  

  print 'deleting static raster widgets'
  delete from GeoSpa.tStaticRasterWidget where assetid in (select assetid from #leaves)

  delete from BI.tReportSectionMap where AssetID in (select AssetID from #leaves) 

  print 'deleting ad hoc trees'

  declare @trees as table
  (
    treeid uniqueidentifier
  )

  insert into @trees
  select distinct a.TreeId from UIConfig.tAdHocTree a join 
			 UIConfig.tAdHocNode b on a.TreeId = b.TreeId where b.AssetId in (select AssetId from #leaves)

  DELETE r FROM [BI].[tReport] r JOIN [UIConfig].[tAdHocNode] n ON r.[NodeId] = n.[NodeId] WHERE n.[TreeId] in (select TreeId from @trees)
  DELETE g FROM [UIConfig].[tNodeGeoSpaMap] g JOIN [UIConfig].[tAdHocNode] n ON g.[NodeId] = n.[NodeId] WHERE  n.[TreeId] in (select TreeId from @trees)
  DELETE FROM [UIConfig].[tAdHocNode] WHERE  [TreeId] in (select TreeId from @trees)
  DELETE FROM [UIConfig].[tAdHocTree] WHERE TreeId in (select TreeId from @trees)

  print 'deleting categories'

  DELETE c FROM  Asset.tCategory a JOIN Asset.tClient b ON a.ClientID = b.ClientID JOIN Asset.tAssetAttachmentCategoryMap c ON a.CategoryID = c.CategoryID
	  WHERE b.AssetID IN (SELECT AssetID from #leaves)
  DELETE c FROM  Asset.tCategory a JOIN Asset.tClient b ON a.ClientID = b.ClientID JOIN Asset.tAssetAttributeCategoryMap c ON a.CategoryID = c.CategoryID
	  WHERE b.AssetID IN (SELECT AssetID from #leaves)
  DELETE c FROM  Asset.tCategory a JOIN Asset.tClient b ON a.ClientID = b.ClientID JOIN UIConfig.tAdHocTreeCategoryMap c ON a.CategoryID = c.CategoryID
	  WHERE b.AssetID IN (SELECT AssetID from #leaves)
  DELETE c FROM  Asset.tCategory a JOIN Asset.tClient b ON a.ClientID = b.ClientID JOIN Asset.tQuickSearchCategoryMap c ON a.CategoryID = c.CategoryID
	  WHERE b.AssetID IN (SELECT AssetID from #leaves)

  DELETE a FROM Asset.tCategory a JOIN Asset.tClient b ON a.ClientID = b.ClientID WHERE b.AssetID IN (SELECT AssetID from #leaves)
  
  print 'deleting attributes'

  DELETE a FROM Asset.tAssetAttribute a where AssetID in (select AssetID from #leaves)

  DELETE x FROM Asset.tAttributeOptionType x JOIN Asset.tAttributeType a on x.AttributeTypeID = a.AttributeTypeID JOIN Asset.tClient b ON a.ClientID = b.ClientID WHERE b.AssetID IN (SELECT AssetID from #leaves)

  DELETE a FROM Asset.tAttributeType a JOIN Asset.tClient b ON a.ClientID = b.ClientID WHERE b.AssetID IN (SELECT AssetID from #leaves)
  
  select @count = COUNT(*) from #leaves
  if @count>0
    begin
    	
	print 'deleting assets'
    delete from asset.tAsset where AssetID in (select assetid from #leaves)
    
    set @loops = @loops + 1    
  
	if @loops > 50
	  begin
	  set @done = 1
	  end
    end
  else
    begin
    set @done = 1
    end    
  
  drop table #leaves
  drop table #issues
  drop table #scenarios
  end

  drop table #branch

  COMMIT TRANSACTION;
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION @TranName;
	PRINT N'Script Failed, rolling back';
	PRINT ERROR_MESSAGE();
END CATCH;
  
END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spDeleteAsset] TO [TEUser]
    AS [dbo];

