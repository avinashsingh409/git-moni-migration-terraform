﻿
CREATE PROCEDURE [Asset].[spDeleteAssetAttachment]
	@SecurityUserID INT,
	@AssetAttachmentID UNIQUEIDENTIFIER,
	@Date DATETIME = NULL
AS
	BEGIN

	IF EXISTS (
		SELECT * FROM Asset.tAssetAttachment WHERE AssetAttachmentID = @AssetAttachmentID AND 
		Asset.ufnDoesUserHaveAccessToAsset(AssetID, @SecurityUserID,-1) = 1
	)
	BEGIN	
		INSERT INTO Asset.tAssetAttachmentArchive(	
				[AssetAttachmentID],
				[AssetID],
				[CreateDate],
				[ChangeDate],
				[CreatedByUserID],
				[ChangedByUserID],
				[ContentID],
				[DisplayInstructions],
				[AttachmentType],
				[Title],
				[Caption],
				[DisplayOrder],
				[Favorite],
				[Deleted]
			)
			SELECT 
				[AssetAttachmentID],
				[AssetID],
				[CreateDate],
				COALESCE(@Date, SYSDATETIME()),
				[CreatedByUserID],
				@SecurityUserID,
				[ContentID],
				[DisplayInstructions],
				[AttachmentType],
				[Title],
				[Caption],
				[DisplayOrder],
				[Favorite],
				1
			FROM Asset.tAssetAttachment WHERE AssetAttachmentID = @AssetAttachmentID
	  
		DELETE FROM Asset.tAssetAttachment 
		WHERE AssetAttachmentID = @AssetAttachmentID OR Head = @AssetAttachmentID 
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spDeleteAssetAttachment] TO [TEUser]
    AS [dbo];