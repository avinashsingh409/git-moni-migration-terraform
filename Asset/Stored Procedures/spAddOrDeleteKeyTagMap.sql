﻿CREATE PROCEDURE [Asset].[spAddOrDeleteKeyTagMap]
@AssetID int,
@AssetIssueID int, 
@UserID int, 
@KeyTagOnIssue bit,           --apply key tag to asset issue if true, to asset if false
@KeyTagAction nvarchar(40), 
@KeyTag nvarchar(255),
@ChangeDate datetime
AS
BEGIN 
	DECLARE @KeyID int
	DECLARE @KeyMapID int
	DECLARE @IsDelete bit = 0
	
	BEGIN TRY	
		SET @KeyTag = LTRIM(RTRIM(ISNULL(@KeyTag,'')))
		SET @KeyTagAction = LTRIM(RTRIM(ISNULL(@KeyTagAction,'')))

		IF (@KeyTag = '') 
			BEGIN
			--RAISERROR('Empty strings are not allowed as keywords.', 16, 1)
			RETURN 0
			END
			
		IF (@KeyTagAction = 'delete') OR (@KeyTagAction = 'remove')
			BEGIN
			SET @IsDelete = 1
			END
		ELSE IF (@KeyTagAction = 'add') 
			BEGIN 
			SET @IsDelete = 0
			END
		ELSE
			BEGIN
			--RAISERROR('No keyword action (add or delete) defined.', 16, 1)
			RETURN 0
			END
						
		IF (@ChangeDate IS NULL)
			BEGIN
			SET @ChangeDate = SYSDATETIMEOFFSET();
			END
		
		DECLARE @Clients TABLE
		(
				ClientId int,
				AssetId int,
				AncestryGenerationsAway int
		)

		INSERT INTO @Clients(ClientId, AssetId, AncestryGenerationsAway)
		SELECT ugcfa.ClientID, ugcfa.AssetID, ugcfa.AncestryGenerationsAway
		FROM Asset.ufnGetClientsForAsset(@AssetID) ugcfa

		IF (@UserID <> -1) 
			BEGIN
			DELETE FROM @Clients WHERE Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @UserID, -1) = 0
			END
	
		IF (NOT EXISTS(SELECT 1 FROM @Clients)) 
			BEGIN
			--RAISERROR('User does not have access to asset', 16, 1)
			RETURN 0
			END

		DECLARE @ClientID int
		DECLARE @IsSysReserve bit = 0
	
		SELECT TOP 1 @ClientID = c.ClientID
		FROM @Clients c
		ORDER BY c.AncestryGenerationsAway
	
		IF (@ClientID IS NULL) 
			BEGIN
			--RAISERROR('Unable to determine client id for asset id.', 16, 1)
			RETURN 0 
			END

		IF (@KeyTagOnIssue = 1)  
			BEGIN
			SELECT TOP 1 @IsSysReserve = k.SystemReserved, @KeyID = k.KeywordID, @KeyMapID = m.AssetIssueKeywordMapID
			FROM Asset.tKeyword k 
			LEFT JOIN Diagnostics.tAssetIssueKeywordMap m ON k.KeywordID = m.KeywordID AND m.AssetIssueID = @AssetIssueID
			WHERE k.ClientID = @ClientID AND k.[Text] = @KeyTag
			ORDER BY k.SystemReserved DESC
			END
		ELSE      
			BEGIN
			SELECT TOP 1 @IsSysReserve = k.SystemReserved, @KeyID = k.KeywordID, @KeyMapID = m.AssetKeywordMapID
			FROM Asset.tKeyword k 
			LEFT JOIN Asset.tAssetKeywordMap m ON k.KeywordID = m.KeywordID AND m.AssetID = @AssetID
			WHERE k.ClientID = @ClientID AND k.[Text] = @KeyTag
			ORDER BY k.SystemReserved DESC
			END

		IF ((@KeyID IS NOT NULL) AND (@IsDelete = 1))
			BEGIN
			IF (@IsSysReserve = 1) 
				BEGIN
				--RAISERROR('Cannot delete a system reserved keyword.', 16, 1)
				RETURN 0
				END
			--ELSE  assume we don't delete the keyword because there is a cleanup routine for orphaned keywords, just the delete the map entry
			END
		ELSE IF ((@KeyID IS NULL) AND (@IsDelete = 0))
			BEGIN
			INSERT INTO Asset.tKeyword (ClientID, [Text], DateCreated, SystemReserved, CreatedByUserID, ChangedByUserID)
			VALUES (@ClientID, @KeyTag, @ChangeDate, 0, @UserID, @UserID)
					
			SET @KeyID = SCOPE_IDENTITY()
					
			IF @KeyID IS NULL
				BEGIN
				--RAISERROR('Unable to save new keyword', 16, 1) 
				RETURN 0
				END
			END

		IF ((@KeyMapID IS NOT NULL) AND (@IsDelete = 1))
			BEGIN
			IF (@KeyTagOnIssue = 1)  
				BEGIN
				DELETE FROM Diagnostics.tAssetIssueKeywordMap WHERE AssetIssueKeywordMapID = @KeyMapID
				END
			ELSE     
				BEGIN
				DELETE FROM Asset.tAssetKeywordMap WHERE AssetKeywordMapID = @KeyMapID
				END
			END
		ELSE IF ((@KeyMapID IS NULL) AND (@IsDelete = 0)) 
			BEGIN
			IF (@KeyTagOnIssue = 1)  
				BEGIN
				INSERT INTO Diagnostics.tAssetIssueKeywordMap (AssetIssueID, KeywordID, CreatedBy, TagDate, BackDate) VALUES (@AssetIssueID, @KeyID, @UserID, @ChangeDate, NULL)
				END
			ELSE     
				BEGIN
				INSERT INTO Asset.tAssetKeywordMap (AssetID, KeywordID, CreatedBy, TagDate, BackDate) VALUES (@AssetID, @KeyID, @UserID, @ChangeDate, NULL)
				END

			SET @KeyMapID = SCOPE_IDENTITY()
					
			IF @KeyMapID IS NULL
				BEGIN
				--RAISERROR('Unable to save keyword map entry', 16, 1) 
				RETURN 0
				END
			END		
	END TRY
		
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
		--RAISERROR(@ErrorMessage, 16, 1)
		RETURN 0
	END CATCH

	RETURN 1
END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spAddOrDeleteKeyTagMap] TO [TEUser]
    AS [dbo];
GO