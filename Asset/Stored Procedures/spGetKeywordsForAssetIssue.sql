
CREATE PROCEDURE [Asset].[spGetKeywordsForAssetIssue]
	@AssetID int,
	@AssetIssueID int,
	@UserID int
AS
BEGIN
	
	SELECT k.Text
    FROM Diagnostics.tAssetIssueKeywordMap m 
    INNER JOIN Asset.tKeyword k ON k.KeywordID = m.KeywordID
    WHERE m.AssetIssueID = @AssetIssueID AND Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @UserID, -1) != 0
    ORDER BY 1
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spGetKeywordsForAssetIssue] TO [TEUser]
    AS [dbo];

