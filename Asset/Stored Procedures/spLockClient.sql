﻿CREATE PROCEDURE Asset.spLockClient
@UserId as int,
@AssetId as int,
@LockReasonId as int,
@LockGroup as uniqueidentifier
AS  
BEGIN

DECLARE @ClientId as int
SELECT @ClientId = Asset.ufnGetClientForAsset(@AssetId)
IF @ClientId IS NULL
  BEGIN
  RAISERROR ('Error - client asset not found.', 13, -1, -1)
  RETURN -1
  END

UPDATE Asset.tClient SET Locked = 1, LockReasonID = @LockReasonId, LockedBy = @UserId,LockedSinceTime = GETDATE(), LockGroup = @LockGroup where ClientID = @ClientId

END

GO

GRANT EXECUTE
    ON OBJECT::Asset.spLockClient TO [TEUser]
    AS [dbo];

GO