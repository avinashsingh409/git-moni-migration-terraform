﻿CREATE PROCEDURE [Asset].[spSetAssetAttachmentCategories]
  @AssetAttachmentID as UNIQUEIDENTIFIER,
  @CategoryIDs Base.tpIntList READONLY
AS
BEGIN
	DECLARE @guidIntList AS Base.tpGuidIntList
	INSERT INTO @guidIntList (GuidID, IntID)
	SELECT @AssetAttachmentID, [id]
	FROM @CategoryIDs

	EXEC Asset.spSetCategoriesForAssetAttachments @guidIntList
END
GO


GRANT EXECUTE
    ON OBJECT::[Asset].[spSetAssetAttachmentCategories]  TO [TEUser]
    AS [dbo];
GO
