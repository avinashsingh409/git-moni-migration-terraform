﻿create   procedure [Asset].[spAttributeToSpatial]
	@AssetAttributes Asset.tpAssetAttribute readonly
as
begin

	declare @attributeIDs Base.tpIntlist; -- working copy
	insert into @attributeIDs (id) select AssetAttributeID from @AssetAttributes;

	declare @latAttrType int = 153;
	declare @lonAttrType int = 154;
	declare @pointGeomType int = 1;
	declare @lineAttrType int = 194;
	declare @lineGeomType int = 2;
	declare @polyAttrType int = 195;
	declare @polyGeomType int = 3;
	declare @lineStarter nvarchar(2);
	declare @lineEnder nvarchar(2);
	set @lineStarter = '[[';
	set @lineEnder = ']]';
	declare @polyLineStarter nvarchar(3);
	declare @polyLineEnder nvarchar(3);
	set @polyLineStarter = '[[[';
	set @polyLineEnder = ']]]';
	declare @multiSplitter nvarchar(5);
	set @multiSplitter = ']],[[';
	declare @group Base.tpIntList;

	declare @OutputSpatial table (
		OutputID int primary key identity(1, 1),
		AssetID int not null,
		Latitude real null,
		Longitude real null,
		LineAttribute nvarchar(max) null,
		PolyAttribute nvarchar(max) null,
		GeometryTypeID int not null,
		GeographyIsValid bit not null default 0,
		GeographyIsValidDetailed nvarchar(max) null,
		ValidGeography geography null,
		GeographyString nvarchar(max) null,
		ValidGeographyError nvarchar(max) null,
		LineStarterIndex bigint null,					-- these "Index" fields hold the position of certain
		LineEnderIndex bigint null,						-- characters (e.g. @lineEnder) that may or may not exist in the attribute values
		PolyStarterIndex bigint null,					-- we're trying to convert into spatial data. we need to know
		PolyEnderIndex bigint null,						-- their positions for data integrity checks
		MultiSplitterIndex bigint null,
		unique nonclustered
		(
			[AssetID],
			[GeometryTypeID]
		)
	);

	-- first, add records to @OutputSpatial
	-- get lat/lon pairs
	-- start with what is provided, then fill in the gaps from tAssetAttribute
	insert into @OutputSpatial (AssetID, Latitude, Longitude, GeometryTypeID)
	select lat.AssetID, lat.Attribute_float, lon.Attribute_float, @pointGeomType
	from @AssetAttributes lat join @AssetAttributes lon on lat.AssetID = lon.AssetID
	where
		(lat.AttributeTypeID = @latAttrType)
		and (lon.AttributeTypeID = @lonAttrType)
	;
	delete a from @attributeIDs a join @AssetAttributes t on a.id = t.AssetAttributeID join @OutputSpatial s on t.AssetID = s.AssetID
	where s.GeometryTypeID = @pointGeomType and t.AttributeTypeID in (@latAttrType, @lonAttrType)
	;

	insert into @OutputSpatial (AssetID, Latitude, Longitude, GeometryTypeID)
	select lat.AssetID, lat.Attribute_float, attr.Attribute_float, @pointGeomType
	from @attributeIDs a join @AssetAttributes lat on a.id = lat.AssetAttributeID
	left join (
		select l.AssetID, ar.Attribute_float
		from @attributeIDs b join @AssetAttributes l on b.id = l.AssetAttributeID left join Asset.tAssetAttribute ar on l.AssetID = ar.AssetID
		where
			(l.AttributeTypeID = @latAttrType)
			and (ar.AttributeTypeID = @lonAttrType)
	) attr on lat.AssetID = attr.AssetID
	where lat.AttributeTypeID = @latAttrType
	;
	delete a from @attributeIDs a join @AssetAttributes t on a.id = t.AssetAttributeID join @OutputSpatial s on t.AssetID = s.AssetID
	where s.GeometryTypeID = @pointGeomType and t.AttributeTypeID in (@latAttrType, @lonAttrType)
	;


	insert into @OutputSpatial (AssetID, Latitude, Longitude, GeometryTypeID)
	select lon.AssetID, attr.Attribute_float, lon.Attribute_float, @pointGeomType
	from @attributeIDs a join @AssetAttributes lon on a.id = lon.AssetAttributeID
	left join (
		select l.AssetID, ar.Attribute_float
		from @attributeIDs b join @AssetAttributes l on b.id = l.AssetAttributeID left join Asset.tAssetAttribute ar on l.AssetID = ar.AssetID
		where
			(l.AttributeTypeID = @lonAttrType)
			and (ar.AttributeTypeID = @latAttrType)
	) attr on lon.AssetID = attr.AssetID
	where lon.AttributeTypeID = @lonAttrType
	;
	delete a from @attributeIDs a join @AssetAttributes t on a.id = t.AssetAttributeID join @OutputSpatial s on t.AssetID = s.AssetID
	where s.GeometryTypeID = @pointGeomType and t.AttributeTypeID in (@latAttrType, @lonAttrType)
	;


	-- and lines, remove whitespace from attribute string
	insert into @OutputSpatial (AssetID, LineAttribute, GeometryTypeID)
	select a.AssetID, replace(a.Attribute_string, ' ', ''), @lineGeomType from @attributeIDs t join @AssetAttributes a on t.id = a.AssetAttributeID where a.AttributeTypeID = @lineAttrType;

	-- and polygons, remove whitespace from attribute string
	insert into @OutputSpatial (AssetID, PolyAttribute, GeometryTypeID)
	select a.AssetID, replace(a.Attribute_string, ' ', ''), @polyGeomType from @attributeIDs t join @AssetAttributes a on t.id = a.AssetAttributeID where a.AttributeTypeID = @polyAttrType;


	-- now check for obvious problems
	-- null Lat/Lon values for points
	update @OutputSpatial set ValidGeographyError = 'Points must have Latitude / Longitude values' where GeometryTypeID = @pointGeomType and (Latitude is null or Longitude is null);

	-- lines and polygons need non-empty values
	update @OutputSpatial set ValidGeographyError = 'Polylines and Polygons must have non-empty values'
	where
		(GeometryTypeID = @lineGeomType and (LineAttribute  is null or LineAttribute = ''))
		or
		(GeometryTypeID = @polyGeomType and (PolyAttribute is null or PolyAttribute = ''))
	;

	-- lines must be '[[ / ]]' or '[[[ / ]]]' style
	update @OutputSpatial
	set
		LineStarterIndex = charindex(@lineStarter, LineAttribute)
		, LineEnderIndex = charindex(@lineEnder, reverse(LineAttribute))
		, PolyStarterIndex = charindex(@polyLineStarter, LineAttribute)
		, PolyEnderIndex = charindex(@polyLineEnder, reverse(LineAttribute))
		, MultiSplitterIndex = charindex(@multiSplitter, LineAttribute)
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @lineGeomType
	;

	update @OutputSpatial set ValidGeographyError = 'Polylines must start with [[ or [[[ and end with ]] or ]]]'
	where
		ValidGeographyError is null
		and
		((GeometryTypeID = @lineGeomType and ((LineStarterIndex <> 1) or (LineEnderIndex <> 1)))
		or
		(GeometryTypeID = @lineGeomType) and ((PolyStarterIndex = 1 and PolyEnderIndex <> 1) or (PolyEnderIndex = 1 and PolyStarterIndex <> 1)))
	;

	-- multi line segments must be '[[[ / ]]]' style
	update @OutputSpatial set ValidGeographyError = 'Multi-segment polylines must start with [[[ and end with ]]]'
	where
		ValidGeographyError is null
		and
		(GeometryTypeID = @lineGeomType and (MultiSplitterIndex <> 0 and ((PolyStarterIndex <> 1) or (PolyEnderIndex <> 1))))


	-- polygons must start with '[[[' and end with ']]]'
	update @OutputSpatial
	set
		PolyStarterIndex = charindex(@polyLineStarter, PolyAttribute)
		, PolyEnderIndex = charindex(@polyLineEnder, reverse(PolyAttribute))
		, MultiSplitterIndex = charindex(@multiSplitter, PolyAttribute)
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @polyGeomType
	;

	update @OutputSpatial set ValidGeographyError = 'Polygons must start with [[[ and end with ]]]'
	where
		ValidGeographyError is null
		and
		(GeometryTypeID = @polyGeomType and ((PolyStarterIndex <> 1) or (PolyEnderIndex <> 1)))
	;




	-- polygons must be closed
	-- single-ring polygons
	insert into @group
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @polyGeomType
		and MultiSplitterIndex = 0
	;
	update s set s.GeographyString = reverse(PolyAttribute) from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = left(s.GeographyString, charindex('[,]', s.GeographyString)) from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(reverse(s.GeographyString), ']]', '') from @OutputSpatial s join @group g on s.OutputID = g.id;

	update s set ValidGeographyError = 'Polygons must be closed'
	from @OutputSpatial s join @group g on s.OutputID = g.id
	where
		replace(left(PolyAttribute, charindex('],[', PolyAttribute)), '[[', '') <> s.GeographyString
	;
	update s set s.GeographyString = null from @OutputSpatial s join @group g on s.OutputID = g.id;

	-- multi-ring polygons
	delete from @group;
	insert into @group
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @polyGeomType
		and MultiSplitterIndex <> 0
	;

	declare @myTestTable table (
		AssetID int not null,
		MyGeographyString nvarchar(max) not null,
		PrepWork nvarchar(max) null,
		PreOne nvarchar(max) null,
		PreFour nvarchar(max) null,
		PrevCharIndex bigint null,
		ThisCharIndex bigint null
	);
	declare @prepsToDropDueToOpenPolygons Base.tpIntList;
	declare @thisAsset int;
	declare @thisAssetGeographyString nvarchar(max);
	declare @thisRing nvarchar(max); 
	declare @preOne nvarchar(max);
	declare @preTwo nvarchar(max);
	declare @preThree nvarchar(max);
	declare @preFour nvarchar(max);
	declare @prevCharIndex bigint;
	declare @thisCharIndex bigint;

	declare curAst cursor local
	for select s.AssetID, replace(replace(s.PolyAttribute, '[[[', '[['), ']]]', ']]') from @OutputSpatial s join @group g on s.OutputID = g.id;
	open curAst;
	fetch next from curAst into @thisAsset, @thisAssetGeographyString
	while @@fetch_status = 0
	begin
		set @prevCharIndex = 1;
		set @thisCharIndex = charindex(@multiSplitter, @thisAssetGeographyString);
		set @thisRing = substring(@thisAssetGeographyString, @prevCharIndex, @thisCharIndex - @prevCharIndex + 2);
		set @preOne = replace(left(@thisRing, charindex('],[', @thisRing)), '[[', '[');
		set @preTwo = reverse(@thisRing);
		set @preThree = left(@preTwo, charindex('[,]', @preTwo));
		set @preFour = replace(reverse(@preThree), ']]', ']');
		insert into @myTestTable(AssetID, MyGeographyString, PrepWork, PrevCharIndex, ThisCharIndex, PreOne, PreFour)
		values (@thisAsset, @thisAssetGeographyString, @thisRing, @prevCharIndex, @thisCharIndex, @preOne, @preFour)
		set @prevCharIndex = @thisCharIndex + 1;
		set @thisCharIndex = charindex(@multiSplitter, @thisAssetGeographyString, @prevCharIndex);

		while @thisCharIndex > 1
		begin
			set @thisRing = substring(@thisAssetGeographyString, @prevCharIndex + 2, @thisCharIndex - @prevCharIndex);
			set @preOne = replace(left(@thisRing, charindex('],[', @thisRing)), '[[', '[');
			set @preTwo = reverse(@thisRing);
			set @preThree = left(@preTwo, charindex('[,]', @preTwo));
			set @preFour = replace(reverse(@preThree), ']]', ']');
			insert into @myTestTable(AssetID, MyGeographyString, PrepWork, PrevCharIndex, ThisCharIndex, PreOne, PreFour)
			values (@thisAsset, @thisAssetGeographyString, @thisRing, @prevCharIndex, @thisCharIndex, @preOne, @preFour)
			set @prevCharIndex = @thisCharIndex + 1;
			set @thisCharIndex = charindex(@multiSplitter, @thisAssetGeographyString, @prevCharIndex);
		end

		-- get the last ring
		set @thisRing = substring(@thisAssetGeographyString, @prevCharIndex + 2, len(@thisAssetGeographyString) - @prevCharIndex - 2);
		set @preOne = replace(replace(left(@thisRing, charindex('],[', @thisRing)), '],[', ''), '[[', '[');
		set @preTwo = reverse(@thisRing);
		set @preThree = left(@preTwo, charindex('[,]', @preTwo));
		set @preFour = replace(reverse(@preThree), ']]', ']');
		insert into @myTestTable(AssetID, MyGeographyString, PrepWork, PrevCharIndex, ThisCharIndex, PreOne, PreFour)
		values (@thisAsset, @thisAssetGeographyString, @thisRing, @prevCharIndex, @thisCharIndex, @preOne, @preFour)


		fetch next from curAst into @thisAsset, @thisAssetGeographyString
	end
	close curAst;
	deallocate curAst;

	insert into @prepsToDropDueToOpenPolygons (id)
	select distinct AssetID from @myTestTable where PreOne <> PreFour;
	update s set s.ValidGeographyError = 'Polygons must be closed' from @OutputSpatial s join @group g on s.OutputID = g.id join @prepsToDropDueToOpenPolygons d on s.AssetID = d.id;






	-- now generate GeographyString
	-- points
	update @OutputSpatial set GeographyString = 'POINT(' + cast(Longitude as nvarchar(max)) + ' ' + cast(Latitude as nvarchar(max)) + ')' where GeometryTypeID = @pointGeomType and ValidGeographyError is null;

	-- single-segment lines, [[ / ]] style
	delete from @group;
	insert into @group (id)
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @lineGeomType
		and
		MultiSplitterIndex = 0
		and (LineStarterIndex = 1 and PolyStarterIndex = 0)
		and (LineEnderIndex = 1 and PolyEnderIndex = 0)
	;

	update s set s.GeographyString = replace(s.LineAttribute, @lineStarter, 'LINESTRING(') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, @lineEnder, ')') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, ',', ' ') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, '] [', ', ') from @OutputSpatial s join @group g on s.OutputID = g.id;


	-- single-segment lines, [[[ / ]]] style
	delete from @group;
	insert into @group (id)
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @lineGeomType
		and
		MultiSplitterIndex = 0
		and (PolyStarterIndex = 1)
		and (PolyEnderIndex = 1)
	;

	update s set s.GeographyString = replace(s.LineAttribute, @polyLineStarter, 'LINESTRING(') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, @polyLineEnder, ')') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, ',', ' ') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, '] [', ', ') from @OutputSpatial s join @group g on s.OutputID = g.id;


	-- multi-segment lines
	delete from @group;
	insert into @group (id)
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @lineGeomType
		and
		MultiSplitterIndex <> 0
		and (PolyStarterIndex = 1)
		and (PolyEnderIndex = 1)
	;

	update s set s.GeographyString = replace(s.LineAttribute, @polyLineStarter, 'MULTILINESTRING((') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, @polyLineEnder, '))') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, ',', ' ') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, '] [', ', ') from @OutputSpatial s join @group g on s.OutputID = g.id
	update s set s.GeographyString = replace(s.GeographyString, '], [', '), (') from @OutputSpatial s join @group g on s.OutputID = g.id;


	-- single-ring polygons
	delete from @group;
	insert into @group (id)
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @polyGeomType
		and
		MultiSplitterIndex = 0
	;

	update s set s.GeographyString = replace(s.PolyAttribute, @polylineStarter, 'POLYGON((') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, @polylineEnder, '))') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, ',', ' ') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, '] [', ', ') from @OutputSpatial s join @group g on s.OutputID = g.id;


	-- multi-ring polygons
	delete from @group;
	insert into @group (id)
	select OutputID from @OutputSpatial
	where
		ValidGeographyError is null
		and
		GeometryTypeID = @polyGeomType
		and
		MultiSplitterIndex <> 0
	;

	update s set s.GeographyString = replace(s.PolyAttribute, @polylineStarter, 'MULTIPOLYGON(((') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, @polylineEnder, ')))') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, ',', ' ') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, '] [', ', ') from @OutputSpatial s join @group g on s.OutputID = g.id;
	update s set s.GeographyString = replace(s.GeographyString, '], [', ')), ((') from @OutputSpatial s join @group g on s.OutputID = g.id;


	-- populate ValidGeography
	update @OutputSpatial set ValidGeography = geography::Parse(GeographyString) where ValidGeographyError is null;
	update @OutputSpatial set GeographyIsValid = ValidGeography.STIsValid() where ValidGeography is not null; -- first check

	-- repair geography
	update @OutputSpatial set ValidGeography = ValidGeography.MakeValid() from @OutputSpatial where ValidGeography is not null and GeographyIsValid = 0;
	update @OutputSpatial set GeographyIsValid = ValidGeography.STIsValid() where ValidGeography is not null; -- check again after repair

	-- invert polygons with ReorientObject on EnvelopeAngle > 90
	update @OutputSpatial set ValidGeography = ValidGeography.ReorientObject() from @OutputSpatial where GeometryTypeID = @polyGeomType and GeographyIsValid = 1 and ValidGeography.EnvelopeAngle() > 90;

	update @OutputSpatial set GeographyString = ValidGeography.ToString() from @Outputspatial where GeographyIsValid = 1;

	update @OutputSpatial set GeographyIsValidDetailed = ValidGeography.IsValidDetailed() where ValidGeography is not null;

	update @OutputSpatial set ValidGeography = null, ValidGeographyError = GeographyIsValidDetailed where ValidGeography is not null and GeographyIsValid = 0;

	select * from @OutputSpatial;


end
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spAttributeToSpatial] TO [TEUser]
    AS [dbo];

