﻿CREATE PROCEDURE [Asset].[spSaveAssetAttachment] 
	-- Add the parameters for the stored procedure here
	@SecurityUserID INT,
	@AssetAttachmentID UNIQUEIDENTIFIER = NULL,
	@AssetID INT,
	@ContentID UNIQUEIDENTIFIER = NULL,
	@DisplayInstructions NVARCHAR(MAX) = NULL,
	@AttachmentType INT = NULL,
	@Title NVARCHAR(255) = NULL,
	@Caption NVARCHAR(MAX) = NULL,
	@Date DATETIME = NULL,
	@DisplayOrder INT = NULL,
	@Favorite BIT = NULL,
	@CategoryIDs Base.tpIntList READONLY 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @addArchive bit = 0; 
	DECLARE @isInsert bit = 0; 
	IF @AssetAttachmentID IS NULL OR NOT EXISTS (SELECT * FROM Asset.tAssetAttachment WHERE AssetAttachmentID = @AssetAttachmentID)
		SET @isInsert = 1; 

	IF @isInsert = 1
	BEGIN
		SET @addArchive = 1; 
		IF @AssetAttachmentID IS NULL
			SET @AssetAttachmentID = NEWID(); 

		IF @Date IS NULL
			SET @Date = SYSDATETIME();

		INSERT INTO Asset.tAssetAttachment(
		[AssetAttachmentID],
		[AssetID],
		[CreateDate],
		[ChangeDate],
		[CreatedByUserID],
		[ChangedByUserID],
		[ContentID],
		[DisplayInstructions],
		[AttachmentType],
		[Title],
		[Caption],
		[DisplayOrder],
		[Favorite])
		VALUES (
		@AssetAttachmentID,
		@AssetID,
		@Date,
		@Date,
		@SecurityUserID,
		@SecurityUserID,
		@ContentID,
		@DisplayInstructions,
		COALESCE(@AttachmentType,1),
		@Title,
		@Caption,
		COALESCE(@DisplayOrder,1),
		COALESCE(@Favorite,0))

	END
	ELSE
	BEGIN
		--If the content id has changed then create a new version.  Otherwise just update.

		DECLARE @oldTitle NVARCHAR(255) = NULL;
		DECLARE @oldCaption NVARCHAR(MAX) = NULL;
		DECLARE @oldDisplayOrder INT = NULL;
		DECLARE @oldFavorite BIT = NULL
		DECLARE @oldContentId uniqueidentifier = NULL;
		DECLARE @oldDisplayInstructions NVARCHAR(MAX) = NULL;
		DECLARE @oldAttachmentType INT = NULL;
		SELECT @oldTitle = Title, 
			@oldCaption = Caption, 
			@oldDisplayOrder = DisplayOrder, 
			@oldFavorite = Favorite, 
			@oldDisplayInstructions = DisplayInstructions,
			@oldAttachmentType = AttachmentType,
			@oldContentId = ContentID 
		FROM Asset.tAssetAttachment 
		WHERE AssetAttachmentID = @AssetAttachmentID

		IF @oldContentId != @ContentID
		BEGIN
			--In this case the content has changed and I need to create a new version.
			INSERT INTO Asset.tAssetAttachment
			(Head, AssetAttachmentID, AssetID, CreateDate, ChangeDate, CreatedByUserID, ChangedByUserID, ContentID, DisplayInstructions, AttachmentType, Title, Caption, DisplayOrder, Favorite)
			SELECT AssetAttachmentID, NEWID(), AssetID, CreateDate, ChangeDate, CreatedByUserID, ChangedByUserID, ContentID, DisplayInstructions, AttachmentType, Title, Caption, DisplayOrder, Favorite
			FROM Asset.tAssetAttachment WHERE AssetAttachmentID = @AssetAttachmentID 
		END

		IF @oldTitle != @Title OR 
		@oldCaption != @Caption OR 
		@oldDisplayOrder != @DisplayOrder OR 
		@oldFavorite != @Favorite OR 
		@oldContentId != @ContentID OR
		@oldDisplayInstructions != @DisplayInstructions OR
		@oldAttachmentType != @AttachmentType 
		BEGIN
			SET @addArchive = 1; 
			UPDATE Asset.tAssetAttachment 
			SET 
				ChangeDate = COALESCE(@Date, SYSDATETIME()),
				ChangedByUserID = @SecurityUserID,
				Title = @Title,
				Caption = @Caption,
				DisplayOrder = @DisplayOrder,
				Favorite = @Favorite,
				ContentID = @ContentID,
				DisplayInstructions = @DisplayInstructions,
				AttachmentType = @AttachmentType
			WHERE AssetAttachmentID = @AssetAttachmentID
		END
	END

	IF @addArchive = 1
	BEGIN
		INSERT INTO Asset.tAssetAttachmentArchive(	
			[AssetAttachmentID],
			[AssetID],
			[CreateDate],
			[ChangeDate],
			[CreatedByUserID],
			[ChangedByUserID],
			[ContentID],
			[DisplayInstructions],
			[AttachmentType],
			[Title],
			[Caption],
			[DisplayOrder],
			[Favorite],
			[Head]
		)
		SELECT 
			[AssetAttachmentID],
			[AssetID],
			[CreateDate],
			[ChangeDate],
			[CreatedByUserID],
			[ChangedByUserID],
			[ContentID],
			[DisplayInstructions],
			[AttachmentType],
			[Title],
			[Caption],
			[DisplayOrder],
			[Favorite],
			[Head]
		FROM Asset.tAssetAttachment WHERE AssetAttachmentID = @AssetAttachmentID
	END

	
	EXEC [Asset].[spSetAssetAttachmentCategories] @AssetAttachmentID, @CategoryIDs
	SELECT @AssetAttachmentID; 
END
GO




GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveAssetAttachment] TO [TEUser]
    AS [dbo];