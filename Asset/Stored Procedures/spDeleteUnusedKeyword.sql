﻿-- Stored Procedure
CREATE PROCEDURE Asset.spDeleteUnusedKeyword
	@assetID int,
	@keywordID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @clientID int
	DECLARE @instances int

	SELECT @instances = COUNT(*) 
	FROM asset.tAssetKeywordMap takm 
	WHERE takm.KeywordID = @keywordID

	IF (@instances > 0) BEGIN
		--'entries found in tAssetKeywordMap'
		RETURN
	END

	SELECT @instances = COUNT(*) 
	FROM asset.tAssetAttachmentKeywordMap taakm
	WHERE taakm.KeywordID = @keywordID

	IF (@instances > 0) BEGIN
		--'entries found in tAssetAttachmentKeywordMap'
		RETURN
	END

	SELECT @instances = COUNT(*) 
	FROM Discussions.tDiscussionEntryKeywordMap tdekm
	WHERE tdekm.KeywordID = @keywordID

	IF (@instances > 0) BEGIN
		--'entries found in tDiscussionEntryKeywordMap'
		RETURN
	END


	SELECT @instances = COUNT(*) 
	FROM Diagnostics.tAssetIssueKeywordMap taikm
	WHERE taikm.KeywordID = @keywordID

	IF (@instances > 0) BEGIN
		--'entries found in tAssetIssueKeywordMap'
		RETURN
	END

	SELECT TOP 1 @clientID = ClientID 
	FROM [Asset].[ufnGetClientsForAsset](@assetId) 
	ORDER BY AncestryGenerationsAway ASC


	IF @clientID IS NULL BEGIN
		--'asset not associated with any client'
		RETURN
	END

	DECLARE @keywordBelongsToNearestClient bit = 0

	SELECT @keywordBelongsToNearestClient = 1
	FROM Asset.tKeyword tk 
	WHERE tk.ClientID=@clientId 
	AND tk.KeywordID=@keywordID

	IF (@keywordBelongsToNearestClient = 0) BEGIN
		--'keyword does not belong to client'
		RETURN
	END

	--'delete keyword'
	DELETE FROM Asset.tKeyword WHERE KeywordID = @keywordID

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spDeleteUnusedKeyword] TO [TEUser]
    AS [dbo];

