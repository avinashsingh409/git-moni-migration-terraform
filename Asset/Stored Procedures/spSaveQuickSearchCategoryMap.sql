﻿CREATE PROCEDURE [Asset].[spSaveQuickSearchCategoryMap]
  @QuickSearchID as INT,
  @CategoryIDs Base.tpIntList READONLY
AS
BEGIN
	DELETE FROM [Asset].[tQuickSearchCategoryMap] WHERE [QuickSearchID] = @QuickSearchID;

	INSERT INTO [Asset].[tQuickSearchCategoryMap] (QuickSearchID,CategoryID) 
	SELECT @QuickSearchID,ID FROM @CategoryIDs;
	RETURN;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveQuickSearchCategoryMap]  TO [TEUser]
    AS [dbo];
GO
