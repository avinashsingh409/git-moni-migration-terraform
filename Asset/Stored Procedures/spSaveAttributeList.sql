﻿CREATE PROCEDURE Asset.spSaveAttributeList
@SecurityUserID int = 0,
@AttributeTypeID int = 0,
@Name nvarchar(255) = NULL,
@ClientID int = NULL,
@AssetID int = 0,
@SelectedDesc nvarchar(255) = '',
@Options Base.tpIntStringList READONLY,
@NewTypeID int OUTPUT,
@NewOptionTypeID int = 0 OUTPUT
AS
BEGIN
DECLARE @ct int 
DECLARE @DisplayFormat nvarchar(100) = ''
DECLARE @OptionStrings Base.tpStringList	

	IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@SecurityUserID,-1) = 0
	BEGIN
		RAISERROR ('Error - user does not have rights to this asset.', 13, -1, -1)
		RETURN -1
	END
	
	SELECT @DisplayFormat = DisplayFormat FROM Asset.tAttributeType 
	  WHERE AttributeTypeID = @AttributeTypeID
    
	SELECT @ct = COUNT(*) FROM @Options
	
	IF @DisplayFormat = '' AND @ct > 0
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			SET @ClientID = Asset.ufnGetClientForAsset(@AssetID);
		END

		IF @ClientID IS NULL OR @Name IS NULL
		BEGIN
			RETURN
		END
		SET @DisplayFormat = 'Radio'

		INSERT INTO Asset.tAttributeType (AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)
		  VALUES (@Name, SUBSTRING(REPLACE(@Name,' ',''),0,99), NULL, @DisplayFormat, 0, 0, @ClientID)
	
		SELECT @NewTypeID = SCOPE_IDENTITY()
		
		SET @AttributeTypeID = @NewTypeID
	END

	IF UPPER(@DisplayFormat) = 'RADIO' AND @ct > 0
	BEGIN
		INSERT INTO @OptionStrings SELECT Label FROM @Options

		DELETE FROM Asset.tAssetAttribute WHERE AttributeOptionTypeID IN 
		  (SELECT AttributeOptionTypeID FROM Asset.tAttributeOptionType o
		   INNER JOIN Asset.tAttributeType at ON at.AttributeTypeID = o.AttributeTypeID
		   WHERE o.AttributeTypeID = @AttributeTypeID AND IsStandard = 0 
		   AND UPPER(AttributeOptionTypeDesc) NOT IN (SELECT UPPER(value) FROM @OptionStrings))
				
		INSERT INTO Asset.tAttributeOptionType (AttributeTypeID, AttributeOptionTypeDesc, DisplayOrder)
			SELECT @AttributeTypeID, s.Label, s.DisplayOrder + 1 
			FROM @Options s
			WHERE NOT EXISTS (SELECT 1 FROM Asset.tAttributeOptionType t WHERE t.AttributeTypeID = @AttributeTypeID AND t.AttributeOptionTypeDesc = s.Label)
			ORDER BY DisplayOrder

		UPDATE t
			SET DisplayOrder = s.DisplayOrder + 1
			FROM Asset.tAttributeOptionType t 
			INNER JOIN @Options s ON t.AttributeOptionTypeDesc = s.Label AND t.AttributeTypeID = @AttributeTypeID
			
		DELETE FROM Asset.tAttributeOptionType 
			WHERE AttributeTypeID = @AttributeTypeID AND AttributeOptionTypeDesc NOT IN 
			(SELECT Label FROM @Options) 
	END

	SELECT @NewOptionTypeID = AttributeOptionTypeID FROM Asset.tAttributeOptionType WHERE AttributeTypeID = @AttributeTypeID AND AttributeOptionTypeDesc = @SelectedDesc
	IF (@NewOptionTypeID = 0)
	BEGIN
		SELECT TOP 1 @NewOptionTypeID = AttributeOptionTypeID FROM Asset.tAttributeOptionType WHERE AttributeTypeID = @AttributeTypeID ORDER BY DisplayOrder
	END
END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveAttributeList] TO [TEUser]
    AS [dbo];
GO
