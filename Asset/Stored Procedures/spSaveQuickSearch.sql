﻿----------------------------------------------------
CREATE PROCEDURE [Asset].[spSaveQuickSearch]
	@QuickSearchId int,
	@Name nvarchar(255),
	@SearchExpression nvarchar(255),
	@IsPublic bit,
	@AssetId int,
	@CreatedBy int,
	@ChangedBy int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @QuickSearchId IS NULL BEGIN
		INSERT INTO [Asset].[tQuickSearch] (Name,SearchExpression,IsPublic,AssetId,CreatedBy,ChangedBy,CreateDate,ChangeDate)
		VALUES (@Name,@SearchExpression,@IsPublic,@AssetId,@CreatedBy,@ChangedBy,@now,@now)
		SELECT SCOPE_IDENTITY();
		RETURN;
	END
	UPDATE [Asset].[tQuickSearch] 
	SET Name = @Name,
		SearchExpression = @SearchExpression,
		IsPublic = @IsPublic,
		AssetId = @AssetId,
		CreatedBy = @CreatedBy,
		ChangedBy = @ChangedBy,
		CreateDate = @now,
		ChangeDate = @now
	WHERE QuickSearchId = @QuickSearchId
	SELECT @QuickSearchId;
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveQuickSearch] TO [TEUser]
    AS [dbo];

