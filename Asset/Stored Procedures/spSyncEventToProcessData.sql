﻿

CREATE PROCEDURE [Asset].[spSyncEventToProcessData]
	@EventAttributes AS Asset.tpSyncEventToProcessData READONLY
AS
	DECLARE @attrId_PlnStrtDt INT = 205;
	DECLARE @attrId_PlnEndDt INT = 206;
	DECLARE @attrId_ActlStrtDt INT = 207;
	DECLARE @attrId_ActlEndDt INT = 208;

	DECLARE @tmpTags AS TABLE (
		[AssetID]        int           NULL,
		[AssetName]      nvarchar(255) NULL,
		[PDServerID]     int           NULL,
		[PDTagID]        int           NULL,
		[TagName]        nvarchar(400) NULL,
		[TagDesc]        nvarchar(400) NULL,
		[VariableTypeID] int           NULL,
		[VariableName]	 nvarchar(255) NULL,
		[ValueTypeID]    int           NULL,
		[EngUnits]       nvarchar(255) NULL,
		[PDTagMapID]	 int		   NULL
	);

	DECLARE @AssetIDs Base.tpIntList;
	INSERT INTO @AssetIDs SELECT DISTINCT OwningAssetID FROM @EventAttributes

	--Create PDServer for [EventServer]
	DECLARE @assetsAndTheirPDServerIDs TABLE (OwningAssetID INT, ClientAssetID INT, PDServerID INT)
	INSERT INTO @assetsAndTheirPDServerIDs
		SELECT a.OwningAssetID,
		Asset.ufnGetClientAssetIDForAsset(a.OwningAssetID),
		ProcessData.ufnGetPDServerForAssetID(a.OwningAssetID, 13, 'EventServer')
		FROM (SELECT DISTINCT OwningAssetID FROM @EventAttributes) AS a

	INSERT INTO ProcessData.tServer (PDServerTypeID, ServerDesc, Comment, AssetID, NDPumpAction, Active)
	SELECT DISTINCT 13,'[EventServer]',c.AssetAbbrev + ' EventServer',c.AssetID,0,1 FROM
	@assetsAndTheirPDServerIDs a
	JOIN Asset.tAsset c ON a.ClientAssetID = c.AssetID
	WHERE a.PDServerID IS NULL

	UPDATE @assetsAndTheirPDServerIDs SET PDServerID = ProcessData.ufnGetPDServerForAssetID(OwningAssetID, 13, 'EventServer')

	--Create tmpTags table
	DECLARE @EventValueTypeID int
	SELECT @EventValueTypeID = ValueTypeID FROM ProcessData.tValueType WHERE ValueTypeDesc = 'Event'

	INSERT INTO @tmpTags (AssetID,AssetName,PDServerID,PDTagID,TagName,TagDesc,VariableTypeID,VariableName,ValueTypeID,EngUnits, PDTagMapID)
	SELECT a.OwningAssetID, c.AssetAbbrev, b.PDServerID, NULL,
	a.AssetClassTypeAbbrev + ';' +
	CASE WHEN a.AttributeTypeID = @attrId_PlnStrtDt OR a.AttributeTypeID = @attrId_PlnEndDt THEN
		'Plan;' + CAST(a.OwningAssetID AS nvarchar(8)) + ';' + CAST(a.AssetClassTypeID AS nvarchar(8))
	ELSE
		'Actual;' + CAST(a.OwningAssetID AS nvarchar(8)) + ';' + CAST(a.AssetClassTypeID AS nvarchar(8))
	END,
	a.AssetClassTypeAbbrev + '-' +
	CASE WHEN a.AttributeTypeID = @attrId_PlnStrtDt OR a.AttributeTypeID = @attrId_PlnEndDt THEN
		'Plan'
	ELSE
		'Actual'
	END,
	NULL, NULL, @EventValueTypeID, NULL, NULL
	FROM @EventAttributes a JOIN @assetsAndTheirPDServerIDs b ON a.OwningAssetID = b.OwningAssetID
	JOIN Asset.tAsset c ON a.OwningAssetID = c.AssetID

	--Map to existing tag
	UPDATE a SET PDTagID = b.PDTagID, ValueTypeID = a.ValueTypeID, PDTagMapID = c.AssetVariableTypeTagMapID
	FROM @tmpTags a JOIN ProcessData.tTag b ON a.TagName = b.TagName AND a.PDServerID = b.PDServerID
	JOIN ProcessData.tAssetVariableTypeTagMap c ON b.PDTagID = c.TagID AND a.AssetID = c.AssetID

	--Create VariableType (if needed)
	UPDATE @tmpTags SET VariableName = REPLACE(TagDesc,' ','')

	INSERT INTO ProcessData.tVariableType (VariableDesc,VariableAbbrev,VariableName,DefaultValueTypeID,IsExempt)
	SELECT DISTINCT VariableName,VariableName,VariableName,@EventValueTypeID,1 FROM @tmpTags
	WHERE PDTagID IS NULL AND VariableName NOT IN (SELECT VariableName FROM ProcessData.tVariableType)

	UPDATE @tmpTags SET VariableTypeID = PDVariableID
	FROM @tmpTags a JOIN ProcessData.tVariableType b ON a.VariableName = b.VariableName

	--Loop through and create AdHocTag if needed.
	DECLARE @assetID int;
	DECLARE @tagID int;
	DECLARE @pDServerID int;
	DECLARE @tagName nvarchar(400);
	DECLARE @tagDesc nvarchar(400);
	DECLARE @variableTypeID int;
	DECLARE @valueTypeID int;
	DECLARE @engUnits nvarchar(255);
	DECLARE @createdBy nvarchar(255) = 'bvDataUser@bv.com';
	DECLARE @securityUserID int;

	SET @securityUserID = (SELECT SecurityUserID FROM AccessControl.tUser WHERE UserName = @createdBy)

	--Create tags and mapping to AssetVariableTypeTagMap
	DECLARE tag_cursor CURSOR FOR
		SELECT  PDServerID, TagName, AssetID, VariableTypeID, EngUnits, ValueTypeID, TagDesc, PDTagID
	FROM @tmpTags
	WHERE PDTagID IS NULL
	OPEN tag_cursor
	FETCH NEXT FROM tag_cursor INTO @pDServerID,@tagName, @assetID,@variableTypeID, @engUnits, @valueTypeID, @tagDesc, @tagID
	DECLARE @tag_Status int
	SET @tag_Status = @@FETCH_STATUS

	WHILE @tag_Status = 0
	BEGIN
		IF(@tagID IS NULL)
		BEGIN
			EXEC [ProcessData].[spCreateTag] @pDServerID, @tagName, @assetID, @variableTypeID,@engUnits, @securityUserID,@createdBy,@valueTypeID,@tagDesc,0,@tagID OUTPUT
			UPDATE @tmpTags SET PDTagID = @tagID WHERE AssetID = @assetID AND TagName = @tagName
		END

		FETCH NEXT FROM tag_cursor
			INTO @pDServerID,@tagName, @assetID,@variableTypeID, @engUnits, @valueTypeID, @tagDesc, @tagID
		SET @tag_Status = @@FETCH_STATUS
	END
	CLOSE tag_cursor
	DEALLOCATE tag_cursor
RETURN 0
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSyncEventToProcessData] TO [TEUser]
    AS [dbo];
GO