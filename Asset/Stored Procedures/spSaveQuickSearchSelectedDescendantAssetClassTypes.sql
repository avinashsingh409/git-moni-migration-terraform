﻿CREATE PROCEDURE [Asset].[spSaveQuickSearchSelectedDescendantAssetClassTypes]
  @QuickSearchID as INT,
  @DescendantAssetClassTypeIDs Base.tpIntList READONLY
AS
BEGIN
	DELETE FROM [Asset].[tQuickSearchSelectedDescendantAssetClassTypes] WHERE [QuickSearchID] = @QuickSearchID;

	INSERT INTO [Asset].[tQuickSearchSelectedDescendantAssetClassTypes] (QuickSearchID,DescendantAssetClassTypeID) 
	SELECT @QuickSearchID,ID FROM @DescendantAssetClassTypeIDs;
	RETURN;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spSaveQuickSearchSelectedDescendantAssetClassTypes]  TO [TEUser]
    AS [dbo];
GO