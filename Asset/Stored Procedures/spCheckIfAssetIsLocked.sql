﻿CREATE PROC [Asset].[spCheckIfAssetIsLocked]
(
	@AssetID int,
	@Locked bit OUTPUT,
	@LockedBy varchar(255) OUTPUT,
	@LockedSince DateTime OUTPUT,
	@LockReasonID int OUTPUT,
	@LockReason varchar(255) OUTPUT
)
AS
BEGIN

SELECT TOP 1 @Locked = Locked, @LockedBy = ISNULL(c.FirstName,'') + ' ' + ISNULL(c.LastName,''), 
		@LockedSince = b.LockedSinceTime, @LockReasonID = b.LockReasonID, @LockReason = d.LockReasonDesc
		FROM Asset.ufnGetClientsForAsset(@AssetID) a JOIN Asset.tClient b on a.ClientID = b.ClientID 
		JOIN AccessControl.tUser c on b.LockedBy = c.SecurityUserID 
		JOIN Asset.tClientLockReasonType d on b.LockReasonID = d.LockReasonID
		WHERE Locked = 1 ORDER BY a.AncestryGenerationsAway

SET @Locked = ISNULL(@Locked,0)

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spCheckIfAssetIsLocked]  TO [TEUser]
    AS [dbo];
GO
