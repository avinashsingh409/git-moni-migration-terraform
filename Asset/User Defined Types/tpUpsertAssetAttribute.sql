﻿CREATE TYPE [Asset].[tpUpsertAssetAttribute] AS TABLE(
	[Delete] [bit] NOT NULL,
	[AssetID] [int] NOT NULL,
	[AttributeTypeID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[Value] [nvarchar](MAX) NULL,
	[Favorite] [bit] NULL,
	[DisplayOrder] [int] NULL,
	[DisplayFormat] [nvarchar](100) NULL,
	[DiscreteOptions] [nvarchar](4000) NULL,
	[Categories] [nvarchar](4000) NULL
)
GO

GRANT EXECUTE
    ON TYPE::[Asset].[tpUpsertAssetAttribute] TO [TEUser];

