﻿--Used in the spGetAdvancedSearch stored procedure.  
CREATE TYPE [Asset].[tpTerms] AS TABLE(
	[id] [int] NULL,
	[AndOr] [int] NULL,
	[LeftHandSide] [nvarchar](255) NULL,
	[Operator] [int] NULL,
	[RightHandSide] [nvarchar](255) NULL
)

GO
GRANT EXECUTE
    ON TYPE::[Asset].[tpTerms] TO [TEUser];