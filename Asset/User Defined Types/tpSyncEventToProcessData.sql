﻿CREATE TYPE [Asset].[tpSyncEventToProcessData] AS TABLE
(
	[AssetAttributeID] INT NOT NULL,
	[AssetID] INT NOT NULL,
	[AssetAbbrev] NVARCHAR(50) NOT NULL,
	[AssetClassTypeID] INT NOT NULL,
	[AssetClassTypeAbbrev] NVARCHAR(255) NOT NULL,
	[OwningAssetID] INT NOT NULL,
	[AttributeTypeID] INT NOT NULL
)
GO

GRANT EXECUTE
    ON TYPE::[Asset].[tpSyncEventToProcessData] TO [TEUser];