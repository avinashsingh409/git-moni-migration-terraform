﻿CREATE TYPE [Asset].[tpQuickSearchColumn] AS TABLE
(
	[QuickSearchID] [int] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[AttributeTypeID] [int] NULL,
	[ScheduleID] [int] NULL,
	[ScheduleType] [nvarchar](255) NULL,
	[EventAssetClassTypeID] [int] NULL,
	[EventAttributeTypeID] [int] NULL,
	[Sort] [bit] NULL,
	[IsBuiltin] [bit] NULL,
	[FilterString] [nvarchar](255) NULL,
	[DateFilterMin] [datetime] NULL, 
	[DateFilterMax] [datetime] NULL,
	[ProcessDataVariableID] [INT] NULL,
	[ProcessDataValueTypeID] [INT] NULL,
	[Width] INT NULL
)
GO

GRANT EXECUTE
    ON TYPE::[Asset].[tpQuickSearchColumn] TO [TEUser];
