﻿create type [Asset].[tpOpStateEvent] as table
(
	EventID int null
	, AssetID int not null
	, EventTypeID int not null
	, StartTime datetimeoffset(7) not null
	, EndTime datetimeoffset(7) null
	, Impact real null
	, [Description] nvarchar(2000) not null
	, ModifiedByEventID int null
	, IsDeleted bit not null
	, GlobalID uniqueidentifier null
	, CreatedBy nvarchar(50) null
	, ChangedBy nvarchar(50) null
	, CreateDate datetime null
	, ChangeDate datetime null
)
go
grant exec on type::[Asset].[tpOpStateEvent] to [TEUser];
go