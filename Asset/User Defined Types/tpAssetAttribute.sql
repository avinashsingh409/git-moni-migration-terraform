﻿CREATE TYPE [Asset].[tpAssetAttribute] AS TABLE (
    [AssetAttributeID]      INT                NOT NULL,
    [AssetID]               INT                NOT NULL,
    [AttributeTypeID]       INT                NOT NULL,
    [AttributeOptionTypeID] INT                NULL,
    [Attribute_int]         INT                NULL,
    [Attribute_string]      NVARCHAR (MAX)     NULL,
    [Attribute_float]       REAL               NULL,
    [Favorite]              BIT                NOT NULL,
    [DisplayOrder]          INT                NOT NULL,
    [CreatedByUserID]       INT                NULL,
    [ChangedByUserID]       INT                NULL,
    [CreateDate]            DATETIME           NULL,
    [ChangeDate]            DATETIME           NULL,
    [Attribute_date]        DATETIMEOFFSET (7) NULL,
    UNIQUE CLUSTERED ([AssetID] ASC, [AttributeTypeID] ASC),
    PRIMARY KEY NONCLUSTERED ([AssetAttributeID] ASC));


GO
GRANT EXECUTE
    ON TYPE::[Asset].[tpAssetAttribute] TO [TEUser];

