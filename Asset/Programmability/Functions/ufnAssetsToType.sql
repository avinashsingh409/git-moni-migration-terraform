﻿
CREATE FUNCTION Asset.ufnAssetsToType 
(
	-- Add the parameters for the stored procedure here
	@rootID int=NULL,
	@assetTypeID int,
	@leafOnly bit = 1
)
RETURNS @assetIdTable TABLE
(
	AssetID int PRIMARY KEY not NULL
)
AS
BEGIN
	-- Insert statements for procedure here
	WITH AssetHierarchy (Level, AssetID, ParentAssetID,AssetTypeID) 
	AS
	(
	  SELECT 0 AS Level,
			 AssetHierarchy1.AssetID,
			 AssetHierarchy1.ParentAssetID,
			 AssetClassType1.AssetTypeID
	  FROM Asset.tAsset AssetHierarchy1
	  INNER JOIN Asset.tAssetClassType AssetClassType1 ON AssetHierarchy1.AssetClassTypeID = AssetClassType1.AssetClassTypeID
	  WHERE (@rootID IS NULL AND (AssetHierarchy1.ParentAssetID = 0 OR AssetHierarchy1.ParentAssetID IS NULL)) 
			OR (@rootID IS NOT NULL AND ( AssetHierarchy1.AssetID = @rootID))

	  UNION ALL

	  SELECT Level+1 AS Level,
			 AssetHierarchy1.AssetID,
			 AssetHierarchy1.ParentAssetID,
			 AssetClassType1.AssetTypeID
	  FROM Asset.tAsset AssetHierarchy1 INNER JOIN AssetHierarchy ON AssetHierarchy1.ParentAssetID = AssetHierarchy.AssetID
	  INNER JOIN Asset.tAssetClassType AssetClassType1 ON AssetHierarchy1.AssetClassTypeID = AssetClassType1.AssetClassTypeID
	  WHERE AssetHierarchy.AssetTypeID <> @assetTypeID
	)
	INSERT INTO @assetIdTable 
	SELECT a.AssetID AS AssetID FROM AssetHierarchy a
	WHERE (@leafOnly = 0) OR (AssetTypeID = @assetTypeID)

	RETURN
END
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnAssetsToType] TO [TEUser]
    AS [dbo];

