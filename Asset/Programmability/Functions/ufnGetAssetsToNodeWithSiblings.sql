﻿
CREATE FUNCTION [Asset].[ufnGetAssetsToNodeWithSiblings] (

@AssetID int)
RETURNS TABLE
AS RETURN (
	WITH parentage (assetid,[level],parentassetid,displayorder)
	AS 
	(			
		SELECT 
			a.assetid, 
			0 as [Level],
			a.parentassetid,
			a.displayorder
		 FROM 
			asset.tAsset a 
		where 
			a.parentassetid = (
				SELECT 
					ParentAssetID 
				FROM 
					asset.tAsset 
				WHERE 
					AssetID = @AssetID
			)		 
		UNION ALL
		SELECT 
			a.assetid,
			p.[Level] + 1, 
			a.parentassetid,
			a.displayorder        
		FROM 
			Asset.tAsset a JOIN parentage p ON p.parentassetid=a.assetid					
	) -- WITH  
	SELECT DISTINCT 
		assetid,
		[Level],
		parentassetid,
		displayorder
	FROM 
		parentage  
	UNION SELECT 
		assetid,
		(SELECT TOP 1 [Level] FROM parentage where parentassetid IS NULL ) AS [Level],
		parentassetid,
		displayorder
	FROM
		Asset.tAsset a1
	WHERE
		ParentAssetID IS NULL
	UNION SELECT
		a2.assetid,
		p2.[Level],
		a2.parentassetid,
		a2.displayorder
	FROM
		Asset.tAsset a2
		JOIN parentage p2 ON a2.ParentAssetID = p2.parentassetid )

GO