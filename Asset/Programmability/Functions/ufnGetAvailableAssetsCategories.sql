﻿CREATE FUNCTION [Asset].[ufnGetAvailableAssetsCategories] 
(	
	@securityUserID as int,
	@AssetIDs as base.tpIntList READONLY,
	@CategoryTypeID as int
)
RETURNS @returnedIds TABLE
(
	CategoryID int PRIMARY KEY not NULL,
	Name varchar(255) not NULL
)
AS
BEGIN
  declare @clients as base.tpIntList

  insert into @clients select distinct ClientID from [Asset].[ufnGetClientsForAssets]( @AssetIDs)

  DECLARE @ids as TABLE
  (
	CategoryID int PRIMARY KEY not NULL,
	Name varchar(255) not NULL
  )

  INSERT INTO @ids
    SELECT c.CategoryID,Name FROM Asset.tCategory c JOIN Asset.tCategoryTypeMap map ON c.CategoryID = map.CategoryID WHERE 
	c.ClientID is NULL AND CategoryTypeID = @CategoryTypeID

  INSERT INTO @ids
    SELECT c.CategoryID,Name FROM Asset.tCategory c JOIN Asset.tCategoryTypeMap map ON c.CategoryID = map.CategoryID 
	JOIN @clients a ON c.ClientID = a.ID
    WHERE map.CategoryTypeID = @CategoryTypeID

  -- remove those categories that are role controlled if the user does not belong to that role
  IF Exists(SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
    BEGIN
	DELETE FROM @ids WHERE 
	CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap) AND
	CategoryID NOT IN 
		(SELECT CategoryID FROM AccessControl.tRoleCategoryMap a JOIN AccessControl.tUserRoleMap b on a.SecurityRoleID = b.SecurityRoleID 
		  WHERE b.SecurityUserID = @securityUserID)
	END

  INSERT INTO @returnedIds select * FROM @ids order by Name

RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAvailableAssetsCategories]   TO [TEUser]
    AS [dbo];

GO
