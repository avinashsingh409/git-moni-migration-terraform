﻿
CREATE FUNCTION [Asset].[ufnGetParentUnitAssetIDOfAssetID]
(
	@AssetID int
)
RETURNS INT
AS
BEGIN
return Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@AssetID,'UN')
END


GO
GRANT EXECUTE
    ON OBJECT::[Asset].[ufnGetParentUnitAssetIDOfAssetID] TO [TEUser]
    AS [dbo];

