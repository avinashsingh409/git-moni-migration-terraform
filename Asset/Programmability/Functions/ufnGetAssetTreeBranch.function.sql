﻿CREATE FUNCTION [Asset].[ufnGetAssetTreeBranch]
( @assetID INT)
RETURNS TABLE AS
RETURN
(	 
	 SELECT ta.AssetID FROM asset.tAssetHop a 
	 INNER JOIN Asset.tAsset ta ON a.EndingAssetId = ta.AssetID AND ta.IsHidden = 0 
	 WHERE (a.HopAssetId=@assetID)
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetTreeBranch] TO [TEUser]
    AS [dbo];

GO