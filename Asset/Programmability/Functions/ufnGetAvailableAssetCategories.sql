﻿CREATE FUNCTION [Asset].[ufnGetAvailableAssetCategories] 
(	
	@securityUserID as int,
	@AssetID as int,
	@CategoryTypeID as int
)
RETURNS @returedIds TABLE
(
	CategoryID int PRIMARY KEY not NULL,
	Name varchar(255) not NULL
)
AS
BEGIN
  DECLARE @ids as TABLE
  (
	CategoryID int PRIMARY KEY not NULL,
	Name varchar(255) not NULL
  )

  INSERT INTO @ids
    SELECT c.CategoryID,Name FROM Asset.tCategory c JOIN Asset.tCategoryTypeMap map ON c.CategoryID = map.CategoryID WHERE 
	c.ClientID is NULL AND CategoryTypeID = @CategoryTypeID

  INSERT INTO @ids
    SELECT c.CategoryID,Name FROM Asset.tCategory c JOIN Asset.tCategoryTypeMap map ON c.CategoryID = map.CategoryID 
	JOIN Asset.ufnGetClientsForAsset(@AssetID) a ON c.ClientID = a.ClientID
    WHERE map.CategoryTypeID = @CategoryTypeID

  -- remove those categories that are role controlled if the user does not belong to that role
  IF Exists(SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
    BEGIN
	DELETE FROM @ids WHERE 
	CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap) AND
	CategoryID NOT IN 
		(SELECT CategoryID FROM AccessControl.tRoleCategoryMap a JOIN AccessControl.tUserRoleMap b on a.SecurityRoleID = b.SecurityRoleID 
		  WHERE b.SecurityUserID = @securityUserID)
	END

  INSERT INTO @returedIds select * FROM @ids order by Name

RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAvailableAssetCategories]  TO [TEUser]
    AS [dbo];
GO