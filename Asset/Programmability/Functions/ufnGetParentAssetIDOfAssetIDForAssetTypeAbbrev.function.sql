﻿CREATE FUNCTION [Asset].[ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev]
(
	@AssetID int,
	@AssetTypeAbbrev varchar(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @Result AS INT
	SET @Result = NULL
	select top 1 @Result = a.AssetID 
	from Asset.ufnAssetAncestorsWithLevelIncludeAsset(@AssetID) ancestry
	inner join asset.tAsset a on ancestry.AssetID = a.AssetID
	inner JOIN Asset.tAssetClassType c ON a.AssetClassTypeID=c.AssetClassTypeID 
	inner JOIN Asset.tAssetType d on c.AssetTypeID=d.AssetTypeID 
	WHERE d.AssetTypeAbbrev=@AssetTypeAbbrev
	order by AncestryGenerationsAway ASC 
	RETURN @Result
END
GO
GRANT EXECUTE
	ON OBJECT::[Asset].[ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev] TO [TEUser]
	AS [dbo];