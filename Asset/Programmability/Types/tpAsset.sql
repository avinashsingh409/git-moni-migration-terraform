CREATE TYPE [Asset].[tpAsset] AS TABLE (
    [AssetID]                               INT            NOT NULL,
    [ParentAssetID]                         INT            NULL,
    [AssetAbbrev]                           NVARCHAR (50)  NOT NULL,
    [AssetDesc]                             NVARCHAR (255) NOT NULL,
    [AssetClassTypeID]                      INT            NOT NULL,
    [AssetTreeNodeChildConfigurationTypeID] INT            NULL,
    [DisplayOrder]                          INT            NOT NULL,
    [IsHidden]                              BIT            NOT NULL,
    [Track]                                 BIT            NOT NULL);







GO
GRANT EXECUTE
    ON TYPE::[Asset].[tpAsset] TO [TEUser];

