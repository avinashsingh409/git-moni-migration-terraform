﻿CREATE PROCEDURE [Asset].[spUpsertEvent]
    @EventID INT = -1,
    @EventGuid UNIQUEIDENTIFIER = NULL,
    @EventClassTypeID INT,
    @OwningAssetID INT,
    @EventAbbrev NVARCHAR(50),
	@EventDesc NVARCHAR(255),
	@DisplayOrder INT,
	@AssetTreeNodeChildConfigurationTypeID INT = NULL,
	@Track BIT,
	@EventNotes NVARCHAR(MAX),
	@PlannedStartDate DATETIMEOFFSET,
	@PlannedEndDate DATETIMEOFFSET,
	@ActualStartDate DATETIMEOFFSET,
	@ActualEndDate DATETIMEOFFSET,
    @SecurityUserID INT,
    @UpsertedEventID INT OUTPUT,
	@UpsertedEventGUID UNIQUEIDENTIFIER OUTPUT,
	@UpsertedEventGroupID INT OUTPUT
AS
BEGIN
	DECLARE @username NVARCHAR(50) = NULL
	SELECT TOP 1 @username = Username FROM AccessControl.tUser WHERE @SecurityUserID = SecurityUserID
	IF @username IS NULL
	BEGIN
        RAISERROR('User not found', 13, -1, -1)
	END

	--If the event class type isn't valid, throw an error.
	--event class type might be very important for counting events per asset...
	IF NOT EXISTS (SELECT TOP 1 AssetClassTypeID FROM Asset.tAssetClassType WHERE AssetClassTypeID = @EventClassTypeID)
	BEGIN
        RAISERROR('Unrecognized event type', 13, -1, -1)
	END

    IF Asset.[ufnDoesUserHaveAccessToAsset](@OwningAssetID, @SecurityUserID, DEFAULT) = 0
    BEGIN
        RAISERROR('User does not have access to this asset', 13, -1, -1)
    END

	--if the event ID passed in is positive, make sure the user has rights to edit it.
	IF @EventID > 0
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 AssetID FROM Asset.tAsset tA WHERE tA.AssetID = @EventID)
		BEGIN
			RAISERROR('Event does not exist', 13, -1, -1)
		END

		IF Asset.[ufnDoesUserHaveAccessToAsset](@EventID, @SecurityUserID, -1) = 0
		BEGIN
			RAISERROR('User does not have access to this event', 13, -1, -1)
		END
	END

	IF @EventDesc is null AND @EventAbbrev is null
	BEGIN
        RAISERROR('Must provide event description and/or event abbreviation', 13, -1, -1)
	END
	ELSE IF @EventDesc is null
	BEGIN
		SET @EventDesc = @EventAbbrev
	END
	ELSE IF @EventAbbrev is null
	BEGIN
		SET @EventAbbrev = SUBSTRING(@EventDesc, 1, 50); --first 50 characters, or all if less than 50
	END

	--More than 1 event group may exist under the parent asset.
	--If we're updating an event, make sure we get the right parent.
	--Otherwise just take the first one.
	SELECT TOP 1 @UpsertedEventGroupID = eventGroups.AssetID
	FROM Asset.tAsset eventGroups
	INNER JOIN Asset.tAsset eventAssets on eventAssets.ParentAssetID = eventGroups.AssetID
	WHERE eventGroups.ParentAssetID = @OwningAssetID AND eventGroups.AssetClassTypeID = 7002
	AND (ISNULL(@EventID, -1) <= 0 OR eventAssets.AssetID = @EventID)


	BEGIN TRY
		BEGIN TRANSACTION

		--if we didn't find the EventGroup, make one.
		IF @UpsertedEventGroupID IS NULL
		BEGIN
			EXEC Asset.spSaveAsset @SecurityUserID, NULL, @OwningAssetID, 'Events','Events',7002,NULL,100,NULL,0,@UpsertedEventGroupID OUTPUT, NULL, 0;
		END

		--save the event. If the eventID is a nonpositive int rather that NULL, set it to NULL first.
		IF @EventID < 1
		BEGIN
			SET @EventID = null;
		END
		EXEC Asset.spSaveAsset @SecurityUserID, @EventID, @UpsertedEventGroupID, @EventAbbrev,@EventDesc,@EventClassTypeID,
			@AssetTreeNodeChildConfigurationTypeID,@DisplayOrder,NULL,0,@UpsertedEventID OUTPUT,@UpsertedEventGUID OUTPUT, @Track;

		-- add to Asset.tAssetAttribute
		DECLARE @Date DATETIME = SYSDATETIME();
		DECLARE @attrId_PlnStrtDt INT = 205;
		DECLARE @attrId_PlnEndDt INT = 206;
		DECLARE @attrId_ActlStrtDt INT = 207;
		DECLARE @attrId_ActlEndDt INT = 208;
		DECLARE @attrId_EvntNt INT = 209;

		IF ISNULL(@EventID, -1) <= 0
		BEGIN
			SET @EventID = @UpsertedEventID;
		END

		-- PlannedStartDate
		IF @PlannedStartDate IS NULL
		BEGIN
			DELETE [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_PlnStrtDt
		END
		ELSE
		BEGIN
			IF (SELECT COUNT(*) FROM [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_PlnStrtDt) > 0
			BEGIN
				UPDATE [Asset].[tAssetAttribute] SET
						 [ChangedByUserID] = @SecurityUserID
						,[ChangeDate] = @Date
						,[Attribute_date] = @PlannedStartDate
						WHERE [AssetID] = @EventID AND [AttributeTypeID] = @attrId_PlnStrtDt
			END
			ELSE
			BEGIN
				INSERT INTO [Asset].[tAssetAttribute] ([AssetID], [AttributeTypeID], [AttributeOptionTypeID], [Attribute_int], [Attribute_string], [Attribute_float], [Favorite], [DisplayOrder], [CreatedByUserID], [ChangedByUserID], [CreateDate], [ChangeDate], [Attribute_date])
					VALUES (@EventID, @attrId_PlnStrtDt, NULL, NULL, NULL, NULL, 0, 1, @SecurityUserID, @SecurityUserID, @Date, @Date, @PlannedStartDate)
			END
		END
		-- PlannedEndDate
		IF @PlannedEndDate IS NULL
		BEGIN
			DELETE [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_PlnEndDt
		END
		ELSE
		BEGIN
			IF (SELECT COUNT(*) FROM [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_PlnEndDt) > 0
			BEGIN
				UPDATE [Asset].[tAssetAttribute] SET
						 [ChangedByUserID] = @SecurityUserID
						,[ChangeDate] = @Date
						,[Attribute_date] = @PlannedEndDate
						WHERE [AssetID] = @EventID AND [AttributeTypeID] = @attrId_PlnEndDt
			END
			ELSE
			BEGIN
				INSERT INTO [Asset].[tAssetAttribute] ([AssetID], [AttributeTypeID], [AttributeOptionTypeID], [Attribute_int], [Attribute_string], [Attribute_float], [Favorite], [DisplayOrder], [CreatedByUserID], [ChangedByUserID], [CreateDate], [ChangeDate], [Attribute_date])
					VALUES (@EventID, @attrId_PlnEndDt, NULL, NULL, NULL, NULL, 0, 1, @SecurityUserID, @SecurityUserID, @Date, @Date, @PlannedEndDate)
			END
		END
		-- ActualStartDate
		IF @ActualStartDate IS NULL
		BEGIN
			DELETE [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_ActlStrtDt
		END
		ELSE
		BEGIN
			IF (SELECT COUNT(*) FROM [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_ActlStrtDt) > 0
			BEGIN
				UPDATE [Asset].[tAssetAttribute] SET
						 [ChangedByUserID] = @SecurityUserID
						,[ChangeDate] = @Date
						,[Attribute_date] = @ActualStartDate
						WHERE [AssetID] = @EventID AND [AttributeTypeID] = @attrId_ActlStrtDt
			END
			ELSE
			BEGIN
				INSERT INTO [Asset].[tAssetAttribute] ([AssetID], [AttributeTypeID], [AttributeOptionTypeID], [Attribute_int], [Attribute_string], [Attribute_float], [Favorite], [DisplayOrder], [CreatedByUserID], [ChangedByUserID], [CreateDate], [ChangeDate], [Attribute_date])
					VALUES (@EventID, @attrId_ActlStrtDt, NULL, NULL, NULL, NULL, 0, 1, @SecurityUserID, @SecurityUserID, @Date, @Date, @ActualStartDate)
			END
		END
		-- ActualEndDate
		IF @ActualEndDate IS NULL
		BEGIN
			DELETE [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_ActlEndDt
		END
		ELSE
		BEGIN
			IF (SELECT COUNT(*) FROM [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_ActlEndDt) > 0
			BEGIN
				UPDATE [Asset].[tAssetAttribute] SET
						 [ChangedByUserID] = @SecurityUserID
						,[ChangeDate] = @Date
						,[Attribute_date] = @ActualEndDate
						WHERE [AssetID] = @EventID AND [AttributeTypeID] = @attrId_ActlEndDt
			END
			ELSE
			BEGIN
				INSERT INTO [Asset].[tAssetAttribute] ([AssetID], [AttributeTypeID], [AttributeOptionTypeID], [Attribute_int], [Attribute_string], [Attribute_float], [Favorite], [DisplayOrder], [CreatedByUserID], [ChangedByUserID], [CreateDate], [ChangeDate], [Attribute_date])
					VALUES (@EventID, @attrId_ActlEndDt, NULL, NULL, NULL, NULL, 0, 1, @SecurityUserID, @SecurityUserID, @Date, @Date, @ActualEndDate)
			END
		END
		-- Note
		IF @EventNotes IS NULL OR @EventNotes = ''
		BEGIN
			DELETE [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_EvntNt
		END
		ELSE
		BEGIN
			IF (SELECT COUNT(*) FROM [Asset].[tAssetAttribute] WHERE [AssetID]=@EventID AND [AttributeTypeID]=@attrId_EvntNt) > 0
			BEGIN
				UPDATE [Asset].[tAssetAttribute] SET
						 [Attribute_string] = @EventNotes
						,[ChangedByUserID] = @SecurityUserID
						,[ChangeDate] = @Date
						WHERE [AssetID] = @EventID AND [AttributeTypeID] = @attrId_EvntNt
			END
			ELSE
			BEGIN
				INSERT INTO [Asset].[tAssetAttribute] ([AssetID], [AttributeTypeID], [AttributeOptionTypeID], [Attribute_int], [Attribute_string], [Attribute_float], [Favorite], [DisplayOrder], [CreatedByUserID], [ChangedByUserID], [CreateDate], [ChangeDate], [Attribute_date])
					VALUES (@EventID, @attrId_EvntNt, NULL, NULL, @EventNotes, NULL, 0, 1, @SecurityUserID, @SecurityUserID, @Date, @Date, NULL)
			END
		END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[spUpsertEvent] TO [TEUser]
    AS [dbo];

