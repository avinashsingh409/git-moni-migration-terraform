﻿CREATE PROCEDURE [Asset].[spAssetAutoCompleteSearch] 
	-- Add the parameters for the stored procedure here
	@search NVARCHAR(255),
	@count int = 12,
	@userID int,
	@parentID int = NULL,
	@startAtLevel int = NULL,
	@stopAtLevel int = NULL,
	@appContextID int = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  
  DECLARE @tempresults AS TABLE
  (
    AssetID int,
	AssetAbbrev varchar(50),
	AssetDesc varchar(255),
	GlobalID uniqueidentifier,
	ParentDesc varchar(255)
	)

  DECLARE @results AS TABLE
  (
    AssetID int,
	AssetAbbrev varchar(50),
	AssetDesc varchar(255),
	GlobalID uniqueidentifier
	)

  DECLARE @baseIDs as BASE.tpIntList
  
  INSERT INTO @baseIDs 
  SELECT a.[AssetID]
  FROM   
  [Asset].[tAsset] a
  INNER JOIN [Asset].[tAssetClassType] b ON a.AssetClassTypeID = b.AssetClassTypeID 
  LEFT JOIN Asset.tAsset p on a.ParentAssetID = p.AssetID
  WHERE a.IsHidden = 0 AND (a.AssetDesc like '%' + @search + '%' OR a.AssetAbbrev like '%' + @search + '%')
  AND (@startAtLevel IS NULL OR b.AssetTypeID >= @startAtLevel) 
  AND (@stopAtLevel IS NULL OR b.AssetTypeID <= @stopAtLevel)  
  ORDER BY b.AssetTypeID, a.AssetClassTypeID, a.AssetDesc 

  DECLARE @filteredIDs as TABLE ( AssetID int )
    
  INSERT INTO @filteredIDs SELECT AssetID FROM Asset.ufnUserHasAccessToAssets(@userID, @baseIDs)  
  
  INSERT INTO @tempresults
  SELECT a.[AssetID],a.[AssetAbbrev], a.[AssetDesc],a.GlobalID,
  p.AssetAbbrev as  ParentDesc
  FROM   
  @filteredIDs f JOIN
  [Asset].[tAsset] a ON f.AssetID = a.AssetID
  INNER JOIN [Asset].[tAssetClassType] b ON a.AssetClassTypeID = b.AssetClassTypeID 
  LEFT JOIN Asset.tAsset p on a.ParentAssetID = p.AssetID
  
  INSERT INTO @results select AssetID,AssetAbbrev,
  case when ParentDesc IS null then AssetDesc else ParentDesc + '\' + AssetDesc END
  AssetDesc,
  GlobalID  from @tempresults 

  IF @parentID IS NOT NULL
    BEGIN
	
	DECLARE @hasAncestry as TABLE
	( AssetID int)
	insert into @hasAncestry
	SELECT DISTINCT a.AssetID FROM @results a CROSS APPLY Asset.ufnAssetAncestors(a.AssetID) b WHERE b.AssetID=@parentID

	DELETE FROM @results WHERE AssetID NOT IN (SELECT AssetID FROM @hasAncestry)
	END
  
  SELECT TOP (@count) AssetID,AssetAbbrev,AssetDesc,GlobalID FROM @results 
END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[spAssetAutoCompleteSearch] TO [TEUser]
    AS [dbo];

