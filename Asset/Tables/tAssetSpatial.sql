﻿CREATE TABLE [Asset].[tAssetSpatial] (
    [AssetSpatialID] INT               IDENTITY (1, 1) NOT NULL,
    [AssetID]        INT               NOT NULL,
    [GeometryTypeID] INT               NOT NULL,
    [AssetGeography] [sys].[geography] NOT NULL,
    CONSTRAINT [PK_tAssetSpatial] PRIMARY KEY CLUSTERED ([AssetSpatialID] ASC),
    CONSTRAINT [FK_tAssetSpatial_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetSpatial_tGeometryType_GeometryTypeID] FOREIGN KEY ([GeometryTypeID]) REFERENCES [GeoSpa].[tGeometryType] ([GeometryTypeID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tAssetSpatial_AssetID_GeometryTypeID] UNIQUE NONCLUSTERED ([AssetID] ASC, [GeometryTypeID] ASC)
);


GO
CREATE SPATIAL INDEX [SPAIDX_tAssetSpatial_AssetGeography]
    ON [Asset].[tAssetSpatial] ([AssetGeography])
    USING GEOGRAPHY_GRID
    WITH  (
            GRIDS = (LEVEL_1 = MEDIUM, LEVEL_2 = MEDIUM, LEVEL_3 = MEDIUM, LEVEL_4 = MEDIUM)
          );

