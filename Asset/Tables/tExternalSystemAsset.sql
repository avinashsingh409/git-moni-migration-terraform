﻿CREATE TABLE [Asset].[tExternalSystemAsset](
	[ExternalSystemAssetID] [int] IDENTITY(1,1) NOT NULL,
	[ExternalSystemID] [int] NOT NULL,
	[AssetID] [int] NULL,
	[ExternalAssetTypeID] [int] NOT NULL,
	[ExternalAssetID] [nvarchar](255) NOT NULL,
	[ExternalAssetName] [nvarchar](255) NOT NULL,
	[ExternalParentAssetID] [nvarchar](255) NULL,
	[ExternalAssetMeta] [nvarchar](255) NULL,
 CONSTRAINT [PK_Asset_tExternalSystemAsset] PRIMARY KEY CLUSTERED (	[ExternalSystemAssetID] ASC),
 CONSTRAINT [UK_Asset_tExternalSystemAsset_ExternalSystemID_AssetID_ExternalAssetType] UNIQUE NONCLUSTERED (	[ExternalSystemID] ASC,	[AssetID] ASC,	[ExternalAssetTypeID] ASC),
);
GO

ALTER TABLE [Asset].[tExternalSystemAsset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_tExternalSystemAsset_ExternalSystemID_Asset_tExternalSystem] FOREIGN KEY([ExternalSystemID])
REFERENCES [Asset].[tExternalSystem] ([ExternalSystemID])
ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tExternalSystemAsset] CHECK CONSTRAINT [FK_Asset_tExternalSystemAsset_ExternalSystemID_Asset_tExternalSystem]
GO

ALTER TABLE [Asset].[tExternalSystemAsset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_tExternalSystemAsset_AssetID_Asset_tAsset] FOREIGN KEY([AssetID])
REFERENCES [Asset].[tAsset] ([AssetID])
ON DELETE SET NULL
GO

ALTER TABLE [Asset].[tExternalSystemAsset] CHECK CONSTRAINT [FK_Asset_tExternalSystemAsset_AssetID_Asset_tAsset]
GO