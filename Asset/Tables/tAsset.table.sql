﻿CREATE TABLE [Asset].[tAsset] (
    [AssetID]                               INT              IDENTITY (1, 1) NOT NULL,
    [ParentAssetID]                         INT              NULL,
    [AssetAbbrev]                           NVARCHAR (50)    NOT NULL,
    [AssetDesc]                             NVARCHAR (255)   NOT NULL,
    [AssetClassTypeID]                      INT              NOT NULL,
    [AssetTreeNodeChildConfigurationTypeID] INT              NULL,
    [DisplayOrder]                          INT              CONSTRAINT [DF_tAsset_DisplayOrder] DEFAULT ((10)) NOT NULL,
    [CreatedBy]                             NVARCHAR (MAX)   CONSTRAINT [DF_tAsset_CreatedBy] DEFAULT ('BV') NOT NULL,
    [ChangedBy]                             NVARCHAR (MAX)   CONSTRAINT [DF_tAsset_ChangedBy] DEFAULT ('BV') NOT NULL,
    [CreateDate]                            DATETIME         CONSTRAINT [DF_tAsset_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                            DATETIME         CONSTRAINT [DF_tAsset_ChangeDate] DEFAULT (getdate()) NULL,
    [GlobalID]                              UNIQUEIDENTIFIER CONSTRAINT [DF_tAsset_GlobalID] DEFAULT (newid()) NOT NULL,
    [IsHidden]                              BIT              CONSTRAINT [DF_tAsset_IsHidden] DEFAULT ((0)) NOT NULL,
    [Track]                                 BIT              CONSTRAINT [DF_tAsset_Track] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tAsset] PRIMARY KEY NONCLUSTERED ([AssetID] ASC),
    CONSTRAINT [FK_Asset_tAsset_ParentAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([ParentAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_Asset_tAsset_tAssetTreeNodeChildConfigurationTypeID_tAssetTreeNodeChildConfigurationType] FOREIGN KEY ([AssetTreeNodeChildConfigurationTypeID]) REFERENCES [Asset].[tAssetTreeNodeChildConfigurationType] ([AssetTreeNodeChildConfigurationTypeID]) ON DELETE SET NULL,
    CONSTRAINT [FK_tAsset_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tAsset_ParentAssetIDAssetDesc] UNIQUE NONCLUSTERED ([ParentAssetID] ASC, [AssetDesc] ASC),
	CONSTRAINT [UK_tAsset_ParentAssetIDAssetAbbrev] UNIQUE NONCLUSTERED ([ParentAssetID] ASC, [AssetAbbrev] ASC)
);






GO




CREATE NONCLUSTERED INDEX [IDX_GlobalID]
	ON [Asset].[tAsset] ([GlobalID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
	ON [PRIMARY];
GO

CREATE NONCLUSTERED INDEX [IDX_tAsset_AssetClassTypeID_IsHidden_For_AutoCompleteSearch]
	ON [Asset].[tAsset] ([AssetClassTypeID] ASC, [IsHidden] ASC)
	INCLUDE
	(
		[AssetID],
		[AssetAbbrev],
		[AssetDesc],
		[GlobalID]
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON);
GO

CREATE NONCLUSTERED INDEX [IDX_tAsset_IsHidden_For_AutoCompleteSearch]
	ON [Asset].[tAsset] ([IsHidden] ASC)
	INCLUDE 
	(
		[AssetID],
		[AssetAbbrev],
		[AssetDesc],
		[AssetClassTypeID],
		[GlobalID]
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON);
GO

CREATE CLUSTERED INDEX [IDX_tAsset_ParentAssetID_AssetClassTypeID_AssetID_IsHidden] ON [Asset].[tAsset]
(
	[ParentAssetID] ASC,
	[AssetClassTypeID] ASC,
	[AssetID] ASC,
	[IsHidden]
)

GO

CREATE INDEX [IX_AssetAbbrev] ON [Asset].[tAsset] ([AssetAbbrev])  WITH (FILLFACTOR=100);
GO




EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAsset';
GO