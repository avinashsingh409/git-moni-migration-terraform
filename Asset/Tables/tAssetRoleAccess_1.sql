﻿CREATE TABLE [Asset].[tAssetRoleAccess] (
    [AssetId] INT NOT NULL,
    [RoleId]  INT NOT NULL,
    [Allowed] BIT NOT NULL,
    CONSTRAINT [PK_tAssetRoleAccess] PRIMARY KEY CLUSTERED ([AssetId] ASC, [RoleId] ASC)
);

