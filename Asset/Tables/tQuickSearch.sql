﻿CREATE TABLE [Asset].[tQuickSearch] (
    [QuickSearchId]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (255) NOT NULL,
    [SearchExpression] NVARCHAR (255) NOT NULL,
    [IsPublic]         BIT            NOT NULL,
    [AssetId]          INT            NULL,
    [CreatedBy]        INT            NOT NULL,
    [ChangedBy]        INT            NOT NULL,
    [CreateDate]       DATETIME       NULL,
    [ChangeDate]       DATETIME       NULL,
    CONSTRAINT [PK_tQuickSearch] PRIMARY KEY CLUSTERED ([QuickSearchId] ASC),
    CONSTRAINT [FK_tQuickSearch_tAsset_AssetId] FOREIGN KEY ([AssetId]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tQuickSearch_tUser_ChangedBy] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tQuickSearch_tUser_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);

