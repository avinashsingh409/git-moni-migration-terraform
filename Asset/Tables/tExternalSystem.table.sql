CREATE TABLE [Asset].[tExternalSystem] (
    [ExternalSystemID]     INT           IDENTITY (1, 1) NOT NULL,
    [ExternalSystemTypeID] INT           NOT NULL,
    [ExternalSystemDesc]   NVARCHAR (50) NOT NULL,
    [ExternalSystemMeta] [nvarchar](1024) null,
    [TopLevelAssetID] [int] not null
    CONSTRAINT [PK_Base_tExternalSystem] PRIMARY KEY CLUSTERED ([ExternalSystemID] ASC),
    CONSTRAINT [FK_Asset_tExternalSystem_ExternalSystemTypeID_Asset_tExternalSystemType] FOREIGN KEY ([ExternalSystemTypeID]) REFERENCES [Asset].[tExternalSystemType] ([ExternalSystemTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tExternalSystem';

