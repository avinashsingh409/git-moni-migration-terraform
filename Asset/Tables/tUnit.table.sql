﻿CREATE TABLE [Asset].[tUnit] (
    [UnitID]                      INT            IDENTITY (1, 1) NOT NULL,
    [StationID]                   INT            NOT NULL,
    [UnitDesc]                    NVARCHAR (255) NOT NULL,
    [UnitAbbrev]                  NVARCHAR (50)  NOT NULL,
    [DisplayOrder]                INT            NOT NULL,
    [RatedNetCapacity_mw]         FLOAT (53)     NOT NULL,
    [CreatedBy]                   NVARCHAR (255) NOT NULL,
    [ChangedBy]                   NVARCHAR (255) NOT NULL,
    [CreateDate]                  DATETIME       CONSTRAINT [DF__tUnit__CreateDat__4FD1D5C8] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                  DATETIME       DEFAULT (getdate()) NOT NULL,
    [EnergyFcstSubCat]            NVARCHAR (255) NULL,
    [EnergyFcstVar]               NVARCHAR (255) NULL,
    [SCR_SNCRFcstSubCat]          NVARCHAR (255) NULL,
    [SCR_SNCRFcstVar]             NVARCHAR (255) NULL,
    [FGDFcstSubCat]               NVARCHAR (255) NULL,
    [FGDFcstVar]                  NVARCHAR (255) NULL,
    [MercuryFcstSubCat]           NVARCHAR (255) NULL,
    [MercuryFcstVar]              NVARCHAR (255) NULL,
    [ScrubberFcstSubCat]          NVARCHAR (255) NULL,
    [ScrubberFcstVar]             NVARCHAR (255) NULL,
    [FlyAshDisposalFcstSubCat]    NVARCHAR (255) NULL,
    [FlyAshDisposalFcstVar]       NVARCHAR (255) NULL,
    [FlyAshSalesFcstSubCat]       NVARCHAR (255) NULL,
    [FlyAshSalesFcstVar]          NVARCHAR (255) NULL,
    [BottomAshDisposalFcstSubCat] NVARCHAR (255) NULL,
    [BottomAshDisposalFcstVar]    NVARCHAR (255) NULL,
    [BottomAshSalesFcstSubCat]    NVARCHAR (255) NULL,
    [BottomAshSalesFcstVar]       NVARCHAR (255) NULL,
    [CoalAddFcstSubCat]           NVARCHAR (255) NULL,
    [CoalAddFcstVar]              NVARCHAR (255) NULL,
    [SO3AddFcstSubCat]            NVARCHAR (255) NULL,
    [SO3AddFcstVar]               NVARCHAR (255) NULL,
    [GypsumSalesFcstSubCat]       NVARCHAR (255) NULL,
    [GypsumSalesFcstVar]          NVARCHAR (255) NULL,
    [AssetID]                     INT            NOT NULL,
    [FGDWaterFcstVar]             NVARCHAR (255) NULL,
    [FGDFixativeFcstVar]          NVARCHAR (255) NULL,
    [FGDAshFcstVar]               NVARCHAR (255) NULL,
    [FGDReagentFcstVar]           NVARCHAR (255) NULL,
    [FGDDBAAddFcstVar]            NVARCHAR (255) NULL,
    [CFBBedReagentFcstVar]        NVARCHAR (255) NULL,
    [NH3AddFcstVar]               NVARCHAR (255) NULL,
    [OMFixedFcstVar]              NVARCHAR (255) NULL,
    [OMVariableFcstVar]           NVARCHAR (255) NULL,
    [EnergyDerateVar]             NVARCHAR (255) NULL,
    [EnergyWeekdayOnPeakFcstVar]  NVARCHAR (255) NULL,
    [EnergyWeekdayOffPeakFcstVar] NVARCHAR (255) NULL,
    [EnergyWeekendOnPeakFcstVar]  NVARCHAR (255) NULL,
    [EnergyWeekendOffPeakFcstVar] NVARCHAR (255) NULL,
    [FurnaceSorbentFcstVar]       NVARCHAR (255) NULL,
    CONSTRAINT [PK_tUnit] PRIMARY KEY CLUSTERED ([UnitID] ASC),
    CONSTRAINT [FK_tUnit_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID]),
    CONSTRAINT [FK_tUnit_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of generating units available in the database.', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tUnit';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tUnit';


GO


CREATE NONCLUSTERED INDEX IDX_tUnit_AssetID ON [Asset].[tUnit]
(
	[AssetID] ASC
)

GO