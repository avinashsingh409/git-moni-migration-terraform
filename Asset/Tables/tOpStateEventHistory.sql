﻿create table [Asset].[tOpStateEventHistory]
(
	EventHistoryID int identity(1, 1) not null
	, EventID int not null
	, AssetID int not null
	, EventTypeID int not null
	, StartTime datetimeoffset(7) not null
	, EndTime datetimeoffset(7) null
	, Impact real null
	, [Description] nvarchar(2000) not null
	, ModifiedByEventID int null
	, IsDeleted bit not null
	, CreatedBy int not null
	, ChangedBy int not null
	, CreateDate datetime not null
	, ChangeDate datetime not null
	constraint [PK_Asset.tOpStateEventHistory] primary key clustered ([EventHistoryID] asc) with (pad_index=off, statistics_norecompute=off, ignore_dup_key=off, allow_row_locks=on, allow_page_locks=on) on [PRIMARY]
)
go

create index [IX_EventID] on [Asset].[tOpStateEventHistory] ([EventID]) with (fillfactor=100) -- based on Schumacher's op mode params indices
go
