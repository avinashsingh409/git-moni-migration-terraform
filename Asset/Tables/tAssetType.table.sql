CREATE TABLE [Asset].[tAssetType] (
    [AssetTypeID]     INT            NOT NULL,
    [AssetTypeAbbrev] NVARCHAR (255) NOT NULL,
    [AssetTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAssetType] PRIMARY KEY CLUSTERED ([AssetTypeID] ASC),
    CONSTRAINT [UK_tAssetType_AssetTypeAbbrev] UNIQUE NONCLUSTERED ([AssetTypeAbbrev] ASC),
    CONSTRAINT [UK_tAssetType_AssetTypeDesc] UNIQUE NONCLUSTERED ([AssetTypeDesc] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetType';

