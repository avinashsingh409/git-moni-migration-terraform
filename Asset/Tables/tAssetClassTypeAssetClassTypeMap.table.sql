CREATE TABLE [Asset].[tAssetClassTypeAssetClassTypeMap] (
    [AssetClassTypeAssetClassTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [ParentAssetClassTypeID]            INT NULL,
    [ChildAssetClassTypeId]             INT NOT NULL,
    [DisplayOrder]                      INT CONSTRAINT [DF_tAssetClassTypeAssetClassTypeMap_DisplayOrder] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tAssetClassTypeAssetClassTypeMap] PRIMARY KEY CLUSTERED ([AssetClassTypeAssetClassTypeMapID] ASC),
    CONSTRAINT [FK_tAssetClassTypeAssetClassTypeMap_tAssetClassType] FOREIGN KEY ([ParentAssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetClassTypeAssetClassTypeMap_tAssetClassType1] FOREIGN KEY ([ChildAssetClassTypeId]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetClassTypeAssetClassTypeMap';

