CREATE TABLE [Asset].[tExternalSystemType] (
    [ExternalSystemTypeID]     INT            NOT NULL,
    [ExternalSystemTypeDesc]   NVARCHAR (255) NOT NULL,
    [ExternalSystemTypeAbbrev] NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Base_tExternalSystemType] PRIMARY KEY CLUSTERED ([ExternalSystemTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tExternalSystemType';

