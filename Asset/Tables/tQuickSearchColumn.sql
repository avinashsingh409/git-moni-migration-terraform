CREATE TABLE [Asset].[tQuickSearchColumn](
	[QuickSearchColumnID] [int] IDENTITY(1,1) NOT NULL,
	[QuickSearchID] [int] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[AttributeTypeID] [int] NULL,
	[ScheduleID] [int] NULL,
	[ScheduleType] [nvarchar](255) NULL,
	[EventAssetClassTypeID] [int] NULL,
	[EventAttributeTypeID] [int] NULL,
	[Sort] [bit] NULL,
	[IsBuiltin] [bit] NULL,
	[FilterString] [nvarchar](255) NULL,
	[DateFilterMin] [datetime] NULL, 
	[DateFilterMax] [datetime] NULL,
	[ProcessDataVariableID] [INT] NULL,
	[ProcessDataValueTypeID] [INT] NULL,
	[Width] INT NULL,
 CONSTRAINT [PK_tQuickSearchColumn] PRIMARY KEY CLUSTERED 
(
	[QuickSearchColumnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [Asset].[tQuickSearchColumn]  WITH CHECK ADD  CONSTRAINT [FK_tQuickSearchColumn_QuickSearchID_tQuickSearch] FOREIGN KEY([QuickSearchID])
REFERENCES [Asset].[tQuickSearch] ([QuickSearchId])
ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tQuickSearchColumn] CHECK CONSTRAINT [FK_tQuickSearchColumn_QuickSearchID_tQuickSearch]
GO

CREATE INDEX [IDX_QuickSearchID] ON [Asset].[tQuickSearchColumn] ([QuickSearchID])  WITH (FILLFACTOR=100);
GO