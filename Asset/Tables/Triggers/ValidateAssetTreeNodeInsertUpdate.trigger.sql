﻿CREATE TRIGGER [Asset].[ValidateAssetTreeNodeInsertUpdate]
   ON  [Asset].[tAssetTreeNode]
   FOR INSERT,UPDATE
AS
BEGIN
    
	DECLARE @AssetTreeID as int
	DECLARE @ParentAssetID as int
	DECLARE @ChildAssetID as int
    DECLARE @OK as int

	DECLARE curUP INSENSITIVE CURSOR
			FOR SELECT AssetTreeID,AssetID,ParentAssetID FROM inserted
	OPEN curUP;
	FETCH NEXT FROM curUP INTO @AssetTreeID,@ChildAssetID,@ParentAssetID

    SET @OK = 1
	WHILE @OK = 1
		BEGIN			
			IF @@FETCH_STATUS =  0
				BEGIN
				
                IF @ParentAssetID IS NOT NULL AND NOT EXISTS(SELECT * FROM Asset.tAssetTreeNode WHERE AssetTreeID=@AssetTreeID AND AssetID=@ParentAssetID)
                    BEGIN
                    RAISERROR(N'VIOLATION OF tAssetTreeNode business rule: Parent Asset ID: %d must already exist in tree',
							16, -- Severity.
							1, -- State.,
							@ParentAssetID
							);
						ROLLBACK TRANSACTION
                    END
				
				FETCH NEXT FROM curUP INTO @AssetTreeID,@ChildAssetID,@ParentAssetID
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	CLOSE curUP;
	DEALLOCATE curUP;

END
