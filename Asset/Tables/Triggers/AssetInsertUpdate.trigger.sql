﻿CREATE TRIGGER [Asset].[AssetInsertUpdate]
   ON  [Asset].[tAsset]
   FOR INSERT,UPDATE,DELETE
AS
BEGIN

BEGIN TRY

	--Insert values into the StationGroup, Station and Unit tables
	INSERT INTO Asset.tStationGroup (AssetID, StationGroupDesc,
	        StationGroupAbbrev, CreatedBy, CreateDate,DisplayOrder,
	        ChangedBy)
	SELECT AssetID,AssetDesc,AssetAbbrev,CreatedBy,CreateDate,i.DisplayOrder,ChangedBy 
	FROM inserted i JOIN asset.tAssetClassType b 
	ON i.AssetClassTypeID = b.AssetClassTypeID 
	WHERE b.AssetTypeID=2 AND NOT EXISTS (SELECT * FROM deleted d where d.AssetID = i.AssetID)
	        
    INSERT INTO Asset.tStation (AssetID, StationDesc,
	       StationAbbrev, CreatedBy, CreateDate, StationGroupID,
	       DisplayOrder,ChangedBy)
	SELECT i.AssetID,i.AssetDesc,AssetAbbrev,i.CreatedBy,i.CreateDate,sg.StationGroupID,i.DisplayOrder,i.ChangedBy 
	FROM inserted i join asset.tAssetClassType b 
	ON i.AssetClassTypeID = b.AssetClassTypeID 
	JOIN Asset.tStationGroup sg 
	ON i.ParentAssetID = sg.AssetID 
	WHERE b.AssetTypeID=3 AND NOT EXISTS (SELECT * FROM deleted d where d.AssetID = i.AssetID)
	       	        
    INSERT INTO Asset.tUnit (AssetID, UnitDesc,
	       UnitAbbrev, CreatedBy, CreateDate, StationID,
	       DisplayOrder,ChangedBy,RatedNetCapacity_mw)
	SELECT i.AssetID,i.AssetDesc,AssetAbbrev,i.CreatedBy,i.CreateDate, stn.StationID,i.DisplayOrder, i.ChangedBy,0 
	FROM inserted i join asset.tAssetClassType b 
	ON i.AssetClassTypeID = b.AssetClassTypeID 
	JOIN Asset.tStation stn 
	ON i.ParentAssetID = stn.AssetID
	WHERE b.AssetTypeID=4 and i.AssetID not in (select AssetID from Asset.tUnit)

    INSERT INTO Asset.tAssetArchive (AssetID, ParentAssetID,
	       AssetAbbrev, AssetDesc, AssetClassTypeID, AssetTreeNodeChildConfigurationTypeID,
	       DisplayOrder, CreatedBy, ChangedBy, CreateDate,
	       ChangeDate, GlobalID, IsHidden) 
	SELECT i.AssetID, i.ParentAssetID, i.AssetAbbrev, i.AssetDesc,
	       i.AssetClassTypeID, i.AssetTreeNodeChildConfigurationTypeID,
	       i.DisplayOrder, i.CreatedBy, i.ChangedBy, i.CreateDate,
	       i.ChangeDate, i.GlobalID, i.IsHidden 
	FROM inserted i
	WHERE i.Track = 1

	UPDATE c set IsHidden=1 FROM inserted a cross apply Asset.ufnGetAssetTreeBranch(a.AssetID) b JOIN Asset.tAsset c ON b.AssetID = c.AssetID
	where a.IsHidden=1

    /* Change the asset abbreviation and asset description so there is less of a possibility of a constraint violation for new assets */
    UPDATE a set AssetAbbrev=a.GlobalID, AssetDesc=a.GlobalID FROM inserted i JOIN Asset.tAsset a ON i.AssetID = a.AssetID
    WHERE i.IsHidden=1
	
	-- BEGIN recalculate Asset.tAssetHop

	 DECLARE @updated Base.tpIntList
	 DECLARE @inserted Base.tpIntList
	 DECLARE @deleted Base.tpIntList

	INSERT INTO @updated
		 SELECT i.AssetID
		 FROM INSERTED i
		 INNER JOIN DELETED d ON i.assetid = d.assetid
		 WHERE i.parentAssetId <> d.parentAssetId
	 EXEC [Asset].[spCalculateAssetHops] @updated, @updated


	INSERT INTO @inserted
		 SELECT i.AssetID
		 FROM INSERTED i
		 LEFT JOIN DELETED d ON i.assetid = d.assetid
		 WHERE d.AssetID IS null

	INSERT INTO @deleted
		 SELECT d.AssetID
		 FROM DELETED d 
		 LEFT JOIN INSERTED i ON i.assetid = d.assetid
		 WHERE i.AssetID IS null

	EXEC [Asset].[spCalculateAssetHops] @deleted, @inserted

	-- END recalculate Asset.tAssetHop

	-- Update asset.tClient with any stationgroup asset

	insert into Asset.tClient (AssetID,CreatedBy,changedby,CreateDate,ChangeDate) 
      select assetid,c.SecurityUserID,c.SecurityUserID,GETDATE(),GETDATE() from INSERTED a, Asset.tAssetClassType b, AccessControl.tUser c where a.AssetClassTypeID = b.AssetClassTypeID and AssetTypeID <= 2 and a.AssetID not in (select AssetID from Asset.tClient) and 
      c.Username = 'bvDatauser@bv.com';

	-- On assetclasstype change, convert non-conforming tagmaps into adhoc tagmaps
	WITH updatedACT AS (SELECT i.AssetID, i.AssetClassTypeID, i.ChangedBy, i.ChangeDate
	FROM INSERTED i JOIN DELETED d ON i.AssetID = d.AssetID WHERE i.AssetClassTypeID <> d.AssetClassTypeID)
		UPDATE avttm SET avttm.VariableTypeID = NULL, avttm.ValueTypeID = NULL, avttm.ChangedBy = u.ChangedBy, avttm.ChangeDate = u.ChangeDate
		FROM ProcessData.tAssetVariableTypeTagMap avttm JOIN updatedACT u ON avttm.AssetID = u.AssetID
		WHERE avttm.TagID IS NOT NULL AND avttm.VariableTypeID IS NOT NULL AND NOT EXISTS 
			(SELECT * FROM  ProcessData.tAssetClassTypeVariableTypeMap actvtm WHERE actvtm.AssetClassTypeID = u.AssetClassTypeID AND avttm.VariableTypeID = actvtm.VariableTypeID);		

END TRY

BEGIN CATCH
	PRINT N'Trigger Failed, rolling back';
	PRINT ERROR_MESSAGE();
END CATCH;

DECLARE @needToReconcileAdHoc as bit = 0

-- use case 1, deleting an asset that has no parent
IF EXISTS(SELECT 1 FROM DELETED WHERE DELETED.ParentAssetID IS NULL AND deleted.AssetID not in (select AssetID from inserted)) BEGIN
  set @needToReconcileAdHoc = 1
END

-- use case 2, inserting an asset that has no parent
IF EXISTS(SELECT 1 FROM inserted WHERE inserted.ParentAssetID IS NULL AND inserted.AssetID not in (select AssetID from deleted)) BEGIN
  set @needToReconcileAdHoc = 1
END

-- use case 3 and 4, demoting an asset that used to have no parent or promoting an asset that used to have a parent
IF EXISTS(SELECT 1 FROM DELETED JOIN inserted ON deleted.AssetID = inserted.AssetID WHERE 
(inserted.ParentAssetID IS NULL AND deleted.ParentAssetID is not null) OR 
(inserted.ParentAssetID IS NOT NULL AND deleted.ParentAssetID is null))
BEGIN
  set @needToReconcileAdHoc = 1
END

if @needToReconcileAdHoc = 1
  BEGIN
  Exec Asset.spReconcileDefaultAdHocTree
  END
END

GO