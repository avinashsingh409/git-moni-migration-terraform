﻿CREATE TABLE [Asset].[tQuickSearchSelectedEventTypes](
	[QuickSearchID] [int] NOT NULL,
	[EventAssetClassTypeID] [int] NOT NULL,
CONSTRAINT [PK_tQuickSearchSelectedEventTypes] PRIMARY KEY CLUSTERED
(
	[QuickSearchID] ASC,
	[EventAssetClassTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [Asset].[tQuickSearchSelectedEventTypes]  WITH CHECK ADD  CONSTRAINT [FK_tQuickSearchSelectedEventTypes_QuickSearchID_tQuickSearch] FOREIGN KEY([QuickSearchID])
REFERENCES [Asset].[tQuickSearch] ([QuickSearchId])
ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tQuickSearchSelectedEventTypes]  WITH CHECK ADD  CONSTRAINT [FK_tQuickSearchSelectedEventTypes_EventAssetClassTypeID_tAssetClassType] FOREIGN KEY([EventAssetClassTypeID])
REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID])
GO

ALTER TABLE [Asset].[tQuickSearchSelectedEventTypes] CHECK CONSTRAINT [FK_tQuickSearchSelectedEventTypes_QuickSearchID_tQuickSearch]
GO

ALTER TABLE [Asset].[tQuickSearchSelectedEventTypes] CHECK CONSTRAINT [FK_tQuickSearchSelectedEventTypes_EventAssetClassTypeID_tAssetClassType]
GO