CREATE TABLE [Asset].[tAssetTree] (
    [AssetTreeID]     INT            IDENTITY (1000, 1) NOT NULL,
    [AssetTreeAbbrev] NVARCHAR (255) NOT NULL,
    [AssetTreeDesc]   NVARCHAR (255) NOT NULL,
    [AssetTreeKey]    NVARCHAR (255) NOT NULL,
    [CreatedBy]       NVARCHAR (MAX) NOT NULL,
    [ChangedBy]       NVARCHAR (MAX) NOT NULL,
    [CreateDate]      DATETIME       CONSTRAINT [DF_tAssetTree_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME       CONSTRAINT [DF_tAssetTree_ChangeDate()] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tAssetTree] PRIMARY KEY CLUSTERED ([AssetTreeID] ASC),
    CONSTRAINT [UK_tAssetTree_AssetTreeAbbrev] UNIQUE NONCLUSTERED ([AssetTreeAbbrev] ASC),
    CONSTRAINT [UK_tAssetTree_AssetTreeDesc] UNIQUE NONCLUSTERED ([AssetTreeDesc] ASC),
    CONSTRAINT [UK_tAssetTree_AssetTreeKey] UNIQUE NONCLUSTERED ([AssetTreeKey] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetTree';

