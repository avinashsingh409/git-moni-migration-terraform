CREATE TABLE [Asset].[tAssetTreeNodeChildConfigurationType] (
    [AssetTreeNodeChildConfigurationTypeID]     INT            IDENTITY (1, 1) NOT NULL,
    [AssetTreeNodeChildConfigurationTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [AssetTreeNodeChildConfigurationTypeDesc]   NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tAssetTreeNodeChildConfigurationType] PRIMARY KEY CLUSTERED ([AssetTreeNodeChildConfigurationTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetTreeNodeChildConfigurationType';

