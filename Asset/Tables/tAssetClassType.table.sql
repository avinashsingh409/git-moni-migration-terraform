﻿CREATE TABLE [Asset].[tAssetClassType] (
    [AssetClassTypeID]     INT            IDENTITY (1, 1) NOT NULL,
    [AssetTypeID]          INT            NOT NULL,
    [AssetClassTypeKey]    NVARCHAR (255) NOT NULL,
    [AssetClassTypeAbbrev] NVARCHAR (255) NOT NULL,
    [AssetClassTypeDesc]   NVARCHAR (MAX) NOT NULL,
    [DisplayOrder]         INT            CONSTRAINT [DF_tAssetClassType_DisplayOrder] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tAssetSubType] PRIMARY KEY CLUSTERED ([AssetClassTypeID] ASC),
    CONSTRAINT [FK_tAssetClassType_tAssetType] FOREIGN KEY ([AssetTypeID]) REFERENCES [Asset].[tAssetType] ([AssetTypeID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tAssetClassType_AssetClassTypeAbbrev] UNIQUE NONCLUSTERED ([AssetClassTypeAbbrev] ASC),
    CONSTRAINT [UK_tAssetClassType_AssetClassTypeKey] UNIQUE NONCLUSTERED ([AssetClassTypeKey] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetClassType';

