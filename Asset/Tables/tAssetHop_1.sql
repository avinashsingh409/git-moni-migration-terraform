﻿CREATE TABLE [Asset].[tAssetHop] (
    [HopAssetId]    INT NOT NULL,
    [EndingAssetId] INT NOT NULL,
    [AncestryLevel] INT NOT NULL,
    CONSTRAINT [PK_tAssetHop] PRIMARY KEY CLUSTERED ([HopAssetId] ASC, [EndingAssetId] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [tAssetHop_EndingAssetId_AncestryLevel]
    ON [Asset].[tAssetHop]([EndingAssetId] ASC, [AncestryLevel] ASC);


GO
CREATE STATISTICS [s_helper]
    ON [Asset].[tAssetHop]([HopAssetId]);

