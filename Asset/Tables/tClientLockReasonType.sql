﻿CREATE TABLE [Asset].[tClientLockReasonType] (
    [LockReasonID]     INT           NOT NULL,
    [LockReasonAbbrev] VARCHAR (50)  NOT NULL,
    [LockReasonDesc]   VARCHAR (255) NOT NULL,
    [DisplayOrder]     INT           NOT NULL,
    CONSTRAINT [PK_tClientLockReasonTypee] PRIMARY KEY CLUSTERED ([LockReasonID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tClientLockReasonType';

