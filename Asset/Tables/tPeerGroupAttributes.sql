CREATE TABLE [Asset].[tPeerGroupAttributes] (
    [PeerGroupAttributeID]  INT            IDENTITY (1, 1) NOT NULL,
    [PeerGroupID]           INT            NOT NULL,
    [AttributeTypeID]       INT            NOT NULL,
    [AttributeOptionTypeID] INT            NULL,
    [Attribute_int]         INT            NULL,
    [Attribute_string]      NVARCHAR (MAX) NULL,
    [Attribute_float_lower] REAL           NULL,
    [Attribute_float_upper] REAL           NULL,
    CONSTRAINT [PK_tPeerGroupAttributes] PRIMARY KEY CLUSTERED ([PeerGroupAttributeID] ASC),
    CONSTRAINT [FK_tPeerGroupAttributes_AttributeOptionTypeID_tAttributeOptionType] FOREIGN KEY ([AttributeOptionTypeID]) REFERENCES [Asset].[tAttributeOptionType] ([AttributeOptionTypeID]),
    CONSTRAINT [FK_tPeerGroupAttributes_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tPeerGroupAttributes_PeerGroupID_tPeerGroup] FOREIGN KEY ([PeerGroupID]) REFERENCES [Asset].[tPeerGroup] ([PeerGroupID]),
    CONSTRAINT [UK_tPeerGroupAttributes_PeerGroupID_AttributeTypeID] UNIQUE NONCLUSTERED ([PeerGroupID] ASC, [AttributeTypeID] ASC)
);




GO

GO


GO

GO


GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tPeerGroupAttributes';

