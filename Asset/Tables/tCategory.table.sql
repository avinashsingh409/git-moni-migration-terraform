﻿CREATE TABLE [Asset].[tCategory](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NULL,
	[Name] VarChar(255) NOT NULL,	
	[CreatedByUserID] [int],
	[ChangedByUserID] [int],
	[CreateDate] [datetime],
	[ChangeDate] [datetime],
 CONSTRAINT [PK_tCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tCategoryID_ClientID_Name] UNIQUE NONCLUSTERED 
(
	[ClientID] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Asset].[tCategory] ADD  CONSTRAINT [DF_tCategory_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [Asset].[tCategory] ADD  CONSTRAINT [DF_tCategory_ChangeDate]  DEFAULT (getdate()) FOR [ChangeDate]
GO

ALTER TABLE [Asset].[tCategory] ADD  CONSTRAINT [FK_tCategory_AssetID_tClient] FOREIGN KEY([ClientID])
REFERENCES [Asset].[tClient] ([ClientID])
ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tCategory]  ADD  CONSTRAINT [FK_tCategory_ChangedByUserID_tUser] FOREIGN KEY([ChangedByUserID])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO

ALTER TABLE [Asset].[tCategory] ADD  CONSTRAINT [FK_tCategory_CreatedByUserID_tUser] FOREIGN KEY([CreatedByUserID])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Asset', @level1type=N'TABLE',@level1name=N'tCategory'
GO
