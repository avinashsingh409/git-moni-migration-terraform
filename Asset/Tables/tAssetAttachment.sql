﻿CREATE TABLE [Asset].[tAssetAttachment] (
    [AssetAttachmentID]   UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [AssetID]             INT              NOT NULL,
    [CreateDate]          DATETIME         NOT NULL,
    [ChangeDate]          DATETIME         NOT NULL,
    [CreatedByUserID]     INT              NOT NULL,
    [ChangedByUserID]     INT              NOT NULL,
    [ContentID]           UNIQUEIDENTIFIER NULL,
    [DisplayInstructions] NVARCHAR (MAX)   NULL,
    [AttachmentType]      INT              DEFAULT ((1)) NOT NULL,
    [Title]               NVARCHAR (255)   NULL,
    [Caption]             NVARCHAR (MAX)   NULL,
    [DisplayOrder]        INT              DEFAULT ((1)) NOT NULL,
    [Favorite]            BIT              CONSTRAINT [DF_AssetAttachment_Favorite] DEFAULT ((1)) NOT NULL,
    [Head]                UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    CONSTRAINT [PK_tAssetAttachment] PRIMARY KEY CLUSTERED ([AssetAttachmentID] ASC),
    CONSTRAINT [FK_Asset_AssetAttachment_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);







 
GO


GO


GO

CREATE NONCLUSTERED INDEX IDX_tAssetAAttachment_AssetID
ON asset.tassetattachment ([AssetID])
