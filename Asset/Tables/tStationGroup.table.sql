﻿CREATE TABLE [Asset].[tStationGroup] (
    [StationGroupID]             INT            IDENTITY (1, 1) NOT NULL,
    [StationGroupDesc]           NVARCHAR (255) NOT NULL,
    [StationGroupAbbrev]         NVARCHAR (50)  NOT NULL,
    [DisplayOrder]               INT            NOT NULL,
    [CreatedBy]                  NVARCHAR (255) NOT NULL,
    [ChangedBy]                  NVARCHAR (255) NOT NULL,
    [CreateDate]                 DATETIME       CONSTRAINT [DF__tSystem__CreateD__39E294A9] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                 DATETIME       DEFAULT (getdate()) NOT NULL,
    [SO2AllowanceFcstSubCat]     NVARCHAR (255) NULL,
    [SO2AllowanceFcstVar]        NVARCHAR (255) NULL,
    [NOxAllowanceFcstSubCat]     NVARCHAR (255) NULL,
    [NOxAllowanceFcstVar]        NVARCHAR (255) NULL,
    [CO2AllowanceFcstSubCat]     NVARCHAR (255) NULL,
    [CO2AllowanceFcstVar]        NVARCHAR (255) NULL,
    [MercuryAllowanceFcstSubCat] NVARCHAR (255) NULL,
    [MercuryAllowanceFcstVar]    NVARCHAR (255) NULL,
    [AssetID]                    INT            NOT NULL,
    CONSTRAINT [PK_tStationGroup] PRIMARY KEY CLUSTERED ([StationGroupID] ASC),
    CONSTRAINT [FK_tStationGroup_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of stations.', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tStationGroup';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tStationGroup';


GO

CREATE NONCLUSTERED INDEX IDX_tStationGroup_AssetID ON [Asset].[tStationGroup]
(
	[AssetID] ASC
)

GO
