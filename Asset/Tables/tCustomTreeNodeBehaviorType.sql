﻿CREATE TABLE [Asset].[tCustomTreeNodeBehaviorType]
(
	[BehaviorTypeId]     INT           NOT NULL,
    [BehaviorTypeAbbrev] NVARCHAR(50)  NOT NULL,
    [BehaviorTypeDesc]   NVARCHAR(255) NOT NULL,
    CONSTRAINT [PK_tCustomTreeNodeBehaviorType] PRIMARY KEY CLUSTERED ([BehaviorTypeId] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tCustomTreeNodeBehaviorType';