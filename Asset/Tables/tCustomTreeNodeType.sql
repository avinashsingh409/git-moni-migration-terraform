﻿CREATE TABLE [Asset].[tCustomTreeNodeType]
(
	[NodeTypeId]     INT           NOT NULL,
    [NodeTypeAbbrev] NVARCHAR(50)  NOT NULL,
    [NodeTypeDesc]   NVARCHAR(255) NOT NULL,
    CONSTRAINT [PK_tCustomTreeNodeType] PRIMARY KEY CLUSTERED ([NodeTypeId] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tCustomTreeNodeType';

