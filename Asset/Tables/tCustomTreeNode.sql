﻿CREATE TABLE [Asset].[tCustomTreeNode]
(
    [NodePk]                  INT              IDENTITY (1, 1) NOT NULL,
	[NodeId]                  UNIQUEIDENTIFIER CONSTRAINT [DF_tCustomTreeNode_NodeId] DEFAULT (newid()) NOT NULL,
    [TreeId]                  UNIQUEIDENTIFIER NOT NULL,
    [NodeAbbrev]              NVARCHAR(50)     NOT NULL,
    [NodeDesc]                NVARCHAR(255)    NOT NULL,
    [ParentNodeId]            UNIQUEIDENTIFIER NULL,
    [NodeTypeId]              INT              NOT NULL,
    [DisplayOrder]            INT              NOT NULL,
    [AssetId]                 INT              NULL,
    [AssetNodeBehaviorTypeId] INT              NULL,
    [CreatedBy]               INT              NOT NULL,
    [ChangedBy]               INT              NOT NULL,
    [CreateDate]              DATETIMEOFFSET   NOT NULL,
    [ChangeDate]              DATETIMEOFFSET   NOT NULL,
    CONSTRAINT [PK_tCustomTreeNode] PRIMARY KEY CLUSTERED ([NodePk] ASC),
    CONSTRAINT [FK_tCustomTreeNode_tCustomTreeNodeBehaviorType_AssetNodeBehaviorTypeId] FOREIGN KEY ([AssetNodeBehaviorTypeId]) REFERENCES [Asset].[tCustomTreeNodeBehaviorType] ([BehaviorTypeId]),
    CONSTRAINT [FK_tCustomTreeNode_tCustomTreeNode_ParentNodeId] FOREIGN KEY ([ParentNodeId]) REFERENCES [Asset].[tCustomTreeNode] ([NodeId]),
    CONSTRAINT [FK_tCustomTreeNode_tCustomTreeNodeType_NodeTypeId] FOREIGN KEY ([NodeTypeId]) REFERENCES [Asset].[tCustomTreeNodeType] ([NodeTypeId]),
    CONSTRAINT [FK_tCustomTreeNode_tCustomTree_TreeId] FOREIGN KEY ([TreeId]) REFERENCES [Asset].[tCustomTree] ([TreeId]),
    CONSTRAINT [FK_tCustomTreeNode_tAsset_AssetId] FOREIGN KEY ([AssetId]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tCustomTreeNode_CreatedBy_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCustomTreeNode_ChangedBy_tUser] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tCustomTreeNode_NodeId] UNIQUE NONCLUSTERED ([NodeId])
);

GO
CREATE NONCLUSTERED INDEX [IDX_tCustomTreeNode_ParentNodeId]
    ON [Asset].[tCustomTreeNode]([ParentNodeId] ASC);

GO
CREATE NONCLUSTERED INDEX [IDX_tCustomTreeNode_TreeId_ParentNodeId]
    ON [Asset].[tCustomTreeNode]([TreeId] ASC, [ParentNodeId] ASC);