CREATE TABLE [Asset].[tAssetTreeNode] (
    [AssetTreeNodeID]                       INT            IDENTITY (1, 1) NOT NULL,
    [AssetTreeID]                           INT            NOT NULL,
    [AssetID]                               INT            NOT NULL,
    [ParentAssetID]                         INT            NULL,
    [DisplayOrder]                          INT            CONSTRAINT [DF_tAssetTreeNode_DisplayOrder] DEFAULT ((1)) NOT NULL,
    [AssetTreeNodeChildConfigurationTypeID] INT            NULL,
    [CreatedBy]                             NVARCHAR (MAX) NOT NULL,
    [ChangedBy]                             NVARCHAR (MAX) NOT NULL,
    [CreateDate]                            DATETIME       CONSTRAINT [DF_tAssetTreeNode_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                            DATETIME       CONSTRAINT [DF_tAssetTreeNode_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tAssetTreeNode] PRIMARY KEY CLUSTERED ([AssetTreeNodeID] ASC),
    CONSTRAINT [FK_tAssetTreeNode_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetTreeNode_tAsset1] FOREIGN KEY ([ParentAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tAssetTreeNode_tAssetTree] FOREIGN KEY ([AssetTreeID]) REFERENCES [Asset].[tAssetTree] ([AssetTreeID]),
    CONSTRAINT [FK_tAssetTreeNode_tAssetTreeNodeChildConfigurationType] FOREIGN KEY ([AssetTreeNodeChildConfigurationTypeID]) REFERENCES [Asset].[tAssetTreeNodeChildConfigurationType] ([AssetTreeNodeChildConfigurationTypeID]),
    CONSTRAINT [UK_tAssetTreeNode_AssetTreeID_AssetID] UNIQUE NONCLUSTERED ([AssetTreeID] ASC, [AssetID] ASC),
    CONSTRAINT [UK_tAssetTreeNode_AssetTreeID_AssetID_ParentAssetID] UNIQUE NONCLUSTERED ([AssetTreeID] ASC, [AssetID] ASC, [ParentAssetID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetTreeNode';

