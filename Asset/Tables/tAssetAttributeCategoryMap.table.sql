﻿CREATE TABLE [Asset].[tAssetAttributeCategoryMap](
    [AssetAttributeCategoryMapID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeTypeID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL
 CONSTRAINT [PK_tAssetAttributeCategoryMap] PRIMARY KEY CLUSTERED 
(
	[AssetAttributeCategoryMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tAssetAttributeCategoryMap_UniqueKey] UNIQUE NONCLUSTERED 
(	
	[AttributeTypeID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Asset].[tAssetAttributeCategoryMap] ADD  
CONSTRAINT [FK_tAssetAttributeCategoryMap_AssetAttributeID_tAttributeType] FOREIGN KEY([AttributeTypeID])
REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]) ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tAssetAttributeCategoryMap] ADD  
CONSTRAINT [FK_tAssetAttributeCategoryMap_CategoryID_tCategory] FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID]) 
GO

CREATE INDEX [IX_CategoryID_Includes] ON [Asset].[tAssetAttributeCategoryMap] ([CategoryID])  INCLUDE ([AttributeTypeID]) WITH (FILLFACTOR=100);
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Asset', @level1type=N'TABLE',@level1name=N'tAssetAttributeCategoryMap'
GO
