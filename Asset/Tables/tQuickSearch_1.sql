﻿CREATE TABLE [Asset].[tQuickSearch] (
    [QuickSearchId]                       INT            IDENTITY (1, 1) NOT NULL,
    [Name]                                NVARCHAR (255) NOT NULL,
    [SearchExpression]                    NVARCHAR (255) NOT NULL,
    [IsPublic]                            BIT            NOT NULL,
    [AssetId]                             INT            NULL,
    [CreatedBy]                           INT            NOT NULL,
    [ChangedBy]                           INT            NOT NULL,
    [CreateDate]                          DATETIME       NULL,
    [ChangeDate]                          DATETIME       NULL,
    [StickySearch]                        BIT            CONSTRAINT [DF_tQuickSearch_StickySearch] DEFAULT ((0)) NOT NULL,
    [SelectedMapGeoSpaID]                 INT            NULL,
    [SearchingStatusesNotStatusChangeLog] BIT            NULL,
    [StatusAsOf]                          NVARCHAR (32)  NULL,
    [SelectedStatuses]                    NVARCHAR (255) NULL,
    [TimesliderSelection]                 DATETIME       NULL,
    [StatusChangeLogStartDate]            DATETIME       NULL,
    [StatusChangeLogEndDate]              DATETIME       NULL,
    [Columns]                             NVARCHAR (MAX) NULL,
    [AssetClassTypeID]                    INT            NULL,
    [AppContext]                          NVARCHAR (255) NULL,
    [Tab]                                 NVARCHAR (255) NULL,
    [AssetClassTypeIDForAncestors]        BIT            NOT NULL,
    [IsAdvancedSearch]                    BIT            NULL,
	[ScheduleSearch]                      BIT            NULL,
	[ScheduleMapID]						  INT			 NULL,
	[ScheduleAssetClassTypeID]			  INT			 NULL,
	[ViewType]							  NVARCHAR (32)	 NULL,
	[ApplyToSelectedAssetDescendants]	  BIT            DEFAULT ((0)) NOT NULL
    CONSTRAINT [PK_tQuickSearch] PRIMARY KEY CLUSTERED ([QuickSearchId] ASC),
    CONSTRAINT [FK_tQuickSearch_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tQuickSearch_tAsset_AssetId] FOREIGN KEY ([AssetId]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tQuickSearch_tUser_ChangedBy] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tQuickSearch_tUser_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);
GO

CREATE NONCLUSTERED INDEX [IDX_tQuickSearch_AssetID]
    ON [Asset].[tQuickSearch]([AssetId] ASC);
GO

CREATE NONCLUSTERED INDEX [IDX_tQuickSearch_AssetID_QuickSearchID]
    ON [Asset].[tQuickSearch]([AssetId] ASC, [QuickSearchId] ASC);
GO



