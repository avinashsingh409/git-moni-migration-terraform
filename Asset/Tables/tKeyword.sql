﻿CREATE TABLE [Asset].[tKeyword] (
    [KeywordID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [ClientID]        INT            NOT NULL,
    [Text]            NVARCHAR (255) NOT NULL,
    [DateCreated]     DATETIME       CONSTRAINT [DF_tKeyword_DateCreated] DEFAULT (getdate()) NOT NULL,
    [SystemReserved]  BIT            CONSTRAINT [DF_tKeyword_SystemReserved] DEFAULT ((0)) NOT NULL,
    [CreatedByUserID] INT            NULL,
    [ChangedByUserID] INT            NULL,
    CONSTRAINT [PK_tKeyword] PRIMARY KEY CLUSTERED ([KeywordID] ASC),
    CONSTRAINT [FK_tKeyWord_ChangedByUserID_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tKeyWord_CreatedByUserID_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tKeyword_tClient] FOREIGN KEY ([ClientID]) REFERENCES [Asset].[tClient] ([ClientID]) ON DELETE CASCADE
);



GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tKeyword]
    ON [Asset].[tKeyword]([ClientID] ASC, [Text] ASC);

