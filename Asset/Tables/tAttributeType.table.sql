﻿CREATE TABLE [Asset].[tAttributeType] (
    [AttributeTypeID]   INT            IDENTITY (10000, 1) NOT NULL,
    [AttributeTypeDesc] NVARCHAR (255) NOT NULL,
    [AttributeTypeKey]  NVARCHAR (100) NOT NULL,
    [EngrUnits]         NVARCHAR (100) NULL,
    [DisplayFormat]     NVARCHAR (100) NOT NULL,
    [IsStandard]        BIT            CONSTRAINT [DF_tAttributeType_IsStandard] DEFAULT ((0)) NOT NULL,
    [IsExempt]          BIT            CONSTRAINT [DF_tAttributeType_IsExempt] DEFAULT ((0)) NOT NULL,
    [ClientID]          INT            NULL,
    CONSTRAINT [PK_tAttributeType] PRIMARY KEY CLUSTERED ([AttributeTypeID] ASC),
    CONSTRAINT [FK_tAttributeType_AssetID_tClient] FOREIGN KEY ([ClientID]) REFERENCES [Asset].[tClient] ([ClientID]),
    CONSTRAINT [UK_tAttributeType_ClientID_AttributeTypeDesc] UNIQUE NONCLUSTERED ([ClientID] ASC, [AttributeTypeDesc] ASC)
);

GO
CREATE INDEX [IX_IsExempt] ON [Asset].[tAttributeType] ([IsExempt])  WITH (FILLFACTOR=100);

GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of AttributeTypes.', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAttributeType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAttributeType';





GO


