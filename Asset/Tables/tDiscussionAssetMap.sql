﻿CREATE TABLE [Asset].[tDiscussionAssetMap] (
    [DiscussionAssetMapID] INT              IDENTITY (1, 1) NOT NULL,
    [DiscussionID]         UNIQUEIDENTIFIER NOT NULL,
    [AssetID]              INT              NOT NULL,
    [Type]                 INT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tDiscussionAssetMap] PRIMARY KEY CLUSTERED ([DiscussionAssetMapID] ASC),
    CONSTRAINT [FK_Asset_DiscussionAssetMap_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Asset_DiscussionAssetMap_DiscussionID_Discussions_tDiscussion] FOREIGN KEY ([DiscussionID]) REFERENCES [Discussions].[tDiscussion] ([DiscussionID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Asset_tDiscussionAssetMap_Type_Asset_tDiscussionAssetMapType] FOREIGN KEY ([Type]) REFERENCES [Asset].[tDiscussionAssetMapType] ([DiscussionAssetMapTypeID]) ON DELETE CASCADE,
    CONSTRAINT [AK_DiscussionAssetMapType] UNIQUE NONCLUSTERED ([AssetID] ASC, [Type] ASC)
);

GO
CREATE INDEX [IX_DiscussionID_Includes] ON [Asset].[tDiscussionAssetMap] ([DiscussionID])  INCLUDE ([Type]) WITH (FILLFACTOR=100);

GO
