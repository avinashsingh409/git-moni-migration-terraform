﻿CREATE TABLE [Asset].[tCustomTree]
(
    [TreePk]        INT              IDENTITY (1, 1) NOT NULL,
	[TreeId]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [TreeName]      NVARCHAR(255)    NOT NULL,
    [CustomerID]    UNIQUEIDENTIFIER NOT NULL,
    [IsDefaultTree] BIT              DEFAULT ((0)) NOT NULL,
    [IsPrivate]     BIT              DEFAULT ((0)) NOT NULL,
    [CreatedBy]     INT              NOT NULL, 
    [ChangedBy]     INT              NOT NULL, 
    [CreateDate]    DATETIMEOFFSET   NOT NULL, 
    [ChangeDate]    DATETIMEOFFSET   NOT NULL,
    CONSTRAINT [PK_tCustomTree] PRIMARY KEY CLUSTERED ([TreePk] ASC),
    CONSTRAINT [FK_tCustomTree_tCustomer_CustomerID] FOREIGN KEY ([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID]),
    CONSTRAINT [FK_tCustomTree_tUser_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCustomTree_tUser_ChangedBy] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUSerID]),
    CONSTRAINT [UK_tCustomTree_TreeId] UNIQUE NONCLUSTERED ([TreeId])
);
GO