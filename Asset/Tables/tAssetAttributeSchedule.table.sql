﻿CREATE TABLE [Asset].[tAssetAttributeSchedule] (
    [AssetAttributeScheduleID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]                  INT            NOT NULL,
    [AttributeTypeID]          INT            NOT NULL,
    [EffectiveDate]            DATETIME       NOT NULL,
    [Attribute]                REAL           CONSTRAINT [DF_tAssetAttributeSchedule_Attribute] DEFAULT ((0)) NOT NULL,
    [Attribute_string]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tAssetAttributeSchedule] PRIMARY KEY CLUSTERED ([AssetAttributeScheduleID] ASC),
    CONSTRAINT [FK_tAssetAttributeSchedule_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetAttributeSchedule_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [UK_tAssetAttributeSchedule_AssetID_EffectiveData_AttributeTypeID] UNIQUE NONCLUSTERED ([AssetID] ASC, [EffectiveDate] ASC, [AttributeTypeID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetAttributeSchedule';

