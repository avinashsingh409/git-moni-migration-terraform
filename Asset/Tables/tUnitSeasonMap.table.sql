CREATE TABLE [Asset].[tUnitSeasonMap] (
    [UnitSeasonMapID] INT          IDENTITY (1, 1) NOT NULL,
    [UnitID]          INT          NOT NULL,
    [Month]           INT          NOT NULL,
    [Season]          VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_tUnitSeasonMap] PRIMARY KEY CLUSTERED ([UnitSeasonMapID] ASC),
    CONSTRAINT [FK_tUnitSeasonMap_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tUnitSeasonMap';

