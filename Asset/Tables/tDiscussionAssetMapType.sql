﻿CREATE TABLE [Asset].[tDiscussionAssetMapType] (
    [DiscussionAssetMapTypeID]   INT            NOT NULL,
    [DiscussionAssetMapTypeName] NVARCHAR (255) NOT NULL,
    [DiscussionAssetMapTypeDesc] NVARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([DiscussionAssetMapTypeID] ASC)
);


