﻿CREATE TABLE [Asset].[tAssetAttachmentCategoryMap](
    [AssetAttachmentCategoryMapID] [int] IDENTITY(1,1) NOT NULL,
	[AssetAttachmentID] [UniqueIdentifier] NOT NULL,
	[CategoryID] [int] NOT NULL
 CONSTRAINT [PK_tAssetAttachmentCategoryMap] PRIMARY KEY CLUSTERED 
(
	[AssetAttachmentCategoryMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tAssetAttachmentCategoryMap_UniqueKey] UNIQUE NONCLUSTERED 
(	
	[AssetAttachmentID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Asset].[tAssetAttachmentCategoryMap] ADD  
CONSTRAINT [FK_tAssetAttachmentCategoryMap_AssetAttachmentID_tAssetAttachment] FOREIGN KEY([AssetAttachmentID])
REFERENCES [Asset].[tAssetAttachment] ([AssetAttachmentID]) ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tAssetAttachmentCategoryMap] ADD  
CONSTRAINT [FK_tAssetAttachmentCategoryMap_CategoryID_tCategory] FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID]) 
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Asset', @level1type=N'TABLE',@level1name=N'tAssetAttachmentCategoryMap'
GO