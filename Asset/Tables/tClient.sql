﻿CREATE TABLE [Asset].[tClient] (
    [ClientID]        INT              IDENTITY (1, 1) NOT NULL,
    [AssetID]         INT              NOT NULL,
    [CreatedBy]       INT              NOT NULL,
    [CreateDate]      DATETIME         NOT NULL,
    [ChangedBy]       INT              NOT NULL,
    [ChangeDate]      DATETIME         NOT NULL,
    [NERCCIP]         BIT              DEFAULT ((0)) NOT NULL,
    [Locked]          BIT              CONSTRAINT [DF_tClient_Locked] DEFAULT ((0)) NOT NULL,
    [LockReasonID]    INT              NULL,
    [LockedSinceTime] DATETIME         NULL,
    [LockedBy]        INT              NULL,
    [LockGroup]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_tClientAssetMap] PRIMARY KEY CLUSTERED ([ClientID] ASC),
    CONSTRAINT [CHK_tClient_Locked] CHECK ([Locked]=(0) AND [LockedSinceTime] IS NULL AND [LockReasonID] IS NULL AND [LockedBy] IS NULL AND [LockGroup] IS NULL OR [Locked]=(1) AND [LockedSinceTime] IS NOT NULL AND [LockReasonID] IS NOT NULL AND [LockedBy] IS NOT NULL AND [LockGroup] IS NOT NULL),
    CONSTRAINT [FK_tClient_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tClient_tClientLockReasonType_LockReasonID] FOREIGN KEY ([LockReasonID]) REFERENCES [Asset].[tClientLockReasonType] ([LockReasonID]),
    CONSTRAINT [FK_tClient_tUser_ChangedBy] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tClient_tUser_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tClient_tUser_LockedBy] FOREIGN KEY ([LockedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);








GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tClient]
    ON [Asset].[tClient]([AssetID] ASC);

