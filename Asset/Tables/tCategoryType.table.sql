﻿CREATE TABLE [Asset].[tCategoryType](
	[CategoryTypeID] [int] NOT NULL,
	[CategoryTypeName] VarChar(255) NOT NULL,		
 CONSTRAINT [PK_tCategoryType] PRIMARY KEY CLUSTERED 
(
	[CategoryTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tCategoryType_CategoryTypeID] UNIQUE NONCLUSTERED 
(	
	[CategoryTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Asset', @level1type=N'TABLE',@level1name=N'tCategoryType'
GO
