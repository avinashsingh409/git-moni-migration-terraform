﻿create table [Asset].[tOpStateEvent]
(
	EventID int identity(1,1) not null
	, AssetID int not null
	, EventTypeID int not null
	, StartTime datetimeoffset(7) not null
	, EndTime datetimeoffset(7) null
	, Impact real null
	, [Description] nvarchar(2000) not null
	, ModifiedByEventID int null
	, IsDeleted bit not null
	, GlobalID uniqueidentifier not null
	, CreatedBy int not null
	, ChangedBy int not null
	, CreateDate datetime not null
	, ChangeDate datetime not null
	constraint [PK_Asset.tOpStateEvent] primary key clustered ([EventID] asc) with (pad_index=off, statistics_norecompute=off, ignore_dup_key=off, allow_row_locks=on, allow_page_locks=on) on [PRIMARY]
)
go

alter table [Asset].[tOpStateEvent] add constraint [DF_tOpStateEvent_IsDeleted] default ((0)) for [IsDeleted]
go
alter table [Asset].[tOpStateEvent] add constraint [DF_tOpStateEvent_GlobalID] default (newid()) for [GlobalID]
go

alter table [Asset].[tOpStateEvent] with check add constraint [FK_tOpStateEvent_AssetID_tAsset_AssetID] foreign key ([AssetID]) references [Asset].[tAsset] ([AssetID]) on delete cascade -- only fk with cascade delete
go
alter table [Asset].[tOpStateEvent] check constraint [FK_tOpStateEvent_AssetID_tAsset_AssetID]
go

alter table [Asset].[tOpStateEvent] with check add constraint [FK_tOpStateEvent_EventTypeID_tEventType_EventTypeID] foreign key ([EventTypeID]) references [Asset].[tEventType] ([EventTypeID])
go
alter table [Asset].[tOpStateEvent] check constraint [FK_tOpStateEvent_EventTypeID_tEventType_EventTypeID]
go

alter table [Asset].[tOpStateEvent] with check add constraint [FK_tOpStateEvent_CreatedBy_tUser_SecurityUserID] foreign key ([CreatedBy]) references [AccessControl].[tUser] ([SecurityUserID])
go
alter table [Asset].[tOpStateEvent] check constraint [FK_tOpStateEvent_CreatedBy_tUser_SecurityUserID]
go

alter table [Asset].[tOpStateEvent] with check add constraint [FK_tOpStateEvent_ChangedBy_tUser_SecurityUserID] foreign key ([ChangedBy]) references [AccessControl].[tUser] ([SecurityUserID])
go
alter table [Asset].[tOpStateEvent] check constraint [FK_tOpStateEvent_ChangedBy_tUser_SecurityUserID]
go

alter table [Asset].[tOpStateEvent] with check add constraint [FK_tOpStateEvent_ModifiedByEventID_tOpStateEvent_EventID] foreign key ([ModifiedByEventID]) references [Asset].[tOpStateEvent] ([EventID])
go
alter table [Asset].[tOpStateEvent] check constraint [FK_tOpStateEvent_ModifiedByEventID_tOpStateEvent_EventID]
go

create index [IX_AssetID_StartTime] on [Asset].[tOpStateEvent] ([AssetID], [StartTime]) include ([EndTime], [EventTypeID], [Impact])
go

-- tOpStateEvent trigger
create trigger [Asset].[tr_tOpStateEvent_Upsert] on [Asset].[tOpStateEvent] after insert, update
as
begin
	insert into Asset.tOpStateEventHistory (EventID, AssetID, EventTypeID, StartTime, EndTime, Impact, [Description], ModifiedByEventID, IsDeleted, CreatedBy, ChangedBy, CreateDate, ChangeDate)
	select EventID, AssetID, EventTypeID, StartTime, EndTime, Impact, [Description], ModifiedByEventID, IsDeleted, CreatedBy, ChangedBy, CreateDate, ChangeDate from inserted;
end
go
alter table [Asset].[tOpStateEvent] enable trigger [tr_tOpStateEvent_Upsert]
go