﻿CREATE TABLE [Asset].[tStation] (
    [StationID]      INT            IDENTITY (1, 1) NOT NULL,
    [StationGroupID] INT            NOT NULL,
    [StationDesc]    NVARCHAR (255) NOT NULL,
    [StationAbbrev]  NVARCHAR (50)  NOT NULL,
    [DeliveryMethod] NVARCHAR (40)  NULL,
    [DisplayOrder]   INT            NOT NULL,
    [CreatedBy]      NVARCHAR (255) NOT NULL,
    [ChangedBy]      NVARCHAR (255) NOT NULL,
    [CreateDate]     DATETIME       CONSTRAINT [DF__tStation__Create__4B0D20AB] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]     DATETIME       CONSTRAINT [DF__tStation__Change__202DAF9E] DEFAULT (getdate()) NOT NULL,
    [AssetID]        INT            NOT NULL,
    CONSTRAINT [PK_tStation] PRIMARY KEY CLUSTERED ([StationID] ASC),
    CONSTRAINT [FK_tStation_StationGroupID_tStationGroup] FOREIGN KEY ([StationGroupID]) REFERENCES [Asset].[tStationGroup] ([StationGroupID]),
    CONSTRAINT [FK_tStation_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of units.', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tStation';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tStation';


GO

CREATE NONCLUSTERED INDEX IDX_tStation_AssetID ON [Asset].[tStation]
(
	[AssetID] ASC
)

GO
