﻿CREATE TABLE [Asset].[tAssetAttribute] (
    [AssetAttributeID]      INT                IDENTITY (1, 1) NOT NULL,
    [AssetID]               INT                NOT NULL,
    [AttributeTypeID]       INT                NOT NULL,
    [AttributeOptionTypeID] INT                NULL,
    [Attribute_int]         INT                NULL,
    [Attribute_string]      NVARCHAR (MAX)     NULL,
    [Attribute_float]       REAL               NULL,
    [Favorite]              BIT                CONSTRAINT [DF_tAssetAttribute_Bit] DEFAULT ((1)) NOT NULL,
    [DisplayOrder]          INT                CONSTRAINT [DF_tAssetAttribute_DisplayOrder] DEFAULT ((10)) NOT NULL,
    [CreatedByUserID]       INT                NULL,
    [ChangedByUserID]       INT                NULL,
    [CreateDate]            DATETIME           NULL,
    [ChangeDate]            DATETIME           NULL,
    [Attribute_date]        DATETIMEOFFSET (7) NULL,
    CONSTRAINT [PK_tAssetAttribute] PRIMARY KEY CLUSTERED ([AssetAttributeID] ASC),
    CONSTRAINT [FK_tAssetAttribute_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetAttribute_AttributeOptionTypeID_tAttributeOptionType] FOREIGN KEY ([AttributeOptionTypeID]) REFERENCES [Asset].[tAttributeOptionType] ([AttributeOptionTypeID]),
    CONSTRAINT [FK_tAssetAttribute_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tAssetAttribute_ChangedByUserID_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tAssetAttribute_CreatedByUserID_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tAssetAttribute_AssetID_AttributeTypeID] UNIQUE NONCLUSTERED ([AssetID] ASC, [AttributeTypeID] ASC)
);

GO

CREATE NONCLUSTERED INDEX IDX_tAssetAttribute_AttributeTypeID
ON [Asset].[tAssetAttribute] ([AttributeTypeID])
INCLUDE ([AssetID],[AttributeOptionTypeID],[Attribute_int],[Attribute_string],[Attribute_float],[Favorite],[DisplayOrder],[CreatedByUserID],[ChangedByUserID],[CreateDate],[ChangeDate],[Attribute_date])

GO

















GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetAttribute';


GO






create trigger [Asset].[tAssetAttribute_TR_After_Insert_Update_Delete]
on [Asset].[tAssetAttribute]
after insert, update, delete
as
begin

    -- START OF EVENT ASSETS INGESTION INTO PROCESSDATA
	DECLARE @attrId_PlnStrtDt INT = 205;
	DECLARE @attrId_PlnEndDt INT = 206;
	DECLARE @attrId_ActlStrtDt INT = 207;
	DECLARE @attrId_ActlEndDt INT = 208;
	DECLARE @eventAttributes AS Asset.tpSyncEventToProcessData;
	
	--Create PDServer and Associated Tags for EventAttributes inserted
	INSERT INTO @eventAttributes (AssetAttributeID, AssetID, AssetAbbrev, AssetClassTypeID, AssetClassTypeAbbrev, OwningAssetID, AttributeTypeID)
	SELECT new.AssetAttributeID, new.AssetID, a.AssetAbbrev, a.AssetClassTypeID, act.AssetClassTypeAbbrev, v.EventOwnerAssetID, new.AttributeTypeID
	FROM inserted new LEFT JOIN deleted old ON old.AssetAttributeID = new.AssetAttributeID
	JOIN Asset.tAsset a ON new.AssetID = a.AssetID
	JOIN Asset.vAssetEvent v ON new.AssetID = v.EventAssetID
	JOIN Asset.tAssetClassType act ON a.AssetClassTypeID = act.AssetClassTypeID
	JOIN Asset.tAttributeType attr ON new.AttributeTypeID = attr.AttributeTypeID 
	WHERE old.Attribute_Date IS NULL AND
	attr.AttributeTypeID IN (@attrId_PlnStrtDt, @attrId_PlnEndDt, @attrId_ActlStrtDt, @attrId_ActlEndDt)

	IF(SELECT COUNT(*) FROM @eventAttributes) > 0
	BEGIN
		EXEC Asset.spSyncEventToProcessData @eventAttributes;
	END

    -- START OF GEOSPATIAL DATA CONVERSION
	declare @latAttrType int = 153;
	declare @lonAttrType int = 154;
	declare @lineAttrType int = 194;
	declare @polyAttrType int = 195;
	declare @pointGeomType int = 1;
	declare @lineGeomType int = 2;
	declare @polyGeomType int = 3;


	declare @spatialAttributes Asset.tpAssetAttribute;
	insert into @spatialAttributes (AssetAttributeID, AssetID, AttributeTypeID, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float, Favorite, DisplayOrder, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate, Attribute_date)
	select AssetAttributeID, AssetID, AttributeTypeID, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float, Favorite, DisplayOrder, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate, Attribute_date from inserted where AttributeTypeID in (@latAttrType, @lonAttrType, @lineAttrType, @polyAttrType);

	-- determine which deletes are true deletes and not just
	-- former-state udpates
	declare @deletedSpatialAttributes Asset.tpAssetAttribute;
	insert into @deletedSpatialAttributes (AssetAttributeID, AssetID, AttributeTypeID, Favorite, DisplayOrder)
	select d.AssetAttributeID, d.AssetID, d.AttributeTypeID, 0, 0
	from deleted d
	join (
		select deletes.AssetAttributeID as MaybeDeletedID, inthetable.AssetAttributeID as JustUpdatedID from deleted deletes
		left join Asset.tAssetAttribute inthetable on deletes.AssetAttributeID = inthetable.AssetAttributeID
		where deletes.AttributeTypeID in (@latAttrType, @lonAttrType, @lineAttrType, @polyAttrType)
	) maybedeletes on d.AssetAttributeID = maybedeletes.MaybeDeletedID
	where maybedeletes.JustUpdatedID is null;

	-- if there aren't any spatial attributes in inserted or true deletes, return
	if ((select count (*) from @spatialAttributes) + (select count (*) from @deletedSpatialAttributes)) = 0
	begin
		return;
	end

	-- this is instructing the db to commit those parts of a transaction that don't encounter an error.
	-- we know that until we progress from 2008R2 the parsing of certain candidate geography strings will
	-- cause the db to throw an exception. we want to preserve the changes to tAssetAttribute even if
	-- they result in these exceptions.
	SET XACT_ABORT OFF

	declare @OutputSpatial table (
		OutputID int primary key,
		AssetID int not null,
		Latitude real null,
		Longitude real null,
		LineAttribute nvarchar(max) null,
		PolyAttribute nvarchar(max) null,
		GeometryTypeID int not null,
		GeographyIsValid bit not null default 0,
		GeographyIsValidDetailed nvarchar(max) null,
		ValidGeography geography null,
		GeographyString nvarchar(max) null,
		ValidGeographyError nvarchar(max) null,
		LineStarterIndex bigint null,
		LineEnderIndex bigint null,
		PolyStarterIndex bigint null,
		PolyEnderIndex bigint null,
		MultiSplitterIndex bigint null,
		unique nonclustered
		(
			[AssetID],
			[GeometryTypeID]
		)
	);

	insert into @OutputSpatial (OutputID, AssetID, Latitude, Longitude, LineAttribute, PolyAttribute, GeometryTypeID, GeographyIsValid, GeographyIsValidDetailed, ValidGeography, GeographyString, ValidGeographyError, LineStarterIndex, LineEnderIndex, PolyStarterIndex, PolyEnderIndex, MultiSplitterIndex)
	exec Asset.spAttributeToSpatial @spatialAttributes;

	-- update valid existing
	update s
	set
		s.AssetGeography = o.ValidGeography
	from Asset.tAssetSpatial s
	join @OutputSpatial o on s.AssetID = o.AssetID and s.GeometryTypeID = o.GeometryTypeID
	where o.GeographyIsValid = 1
	;

	-- eradicate newly invalid existing and
	-- existing that no longer have a source attribute
	delete s from Asset.tAssetSpatial s
	join
	(
		select AssetID, GeometryTypeID from @OutputSpatial where GeographyIsValid = 0
		union
		select AssetID,
			case AttributeTypeID
				when @latAttrType then @pointGeomType
				when @lonAttrType then @pointGeomType
				when @lineAttrType then @lineGeomType
				when @polyAttrType then @polyGeomType end as GeometryTypeID
		from @deletedSpatialAttributes where AttributeTypeID in (@latAttrType, @lonAttrType, @lineAttrType, @polyAttrType)
	) o on s.AssetID = o.AssetID and s.GeometryTypeID = o.GeometryTypeID
	;

	-- insert new valid
	insert into Asset.tAssetSpatial (AssetID, GeometryTypeID, AssetGeography)
	select o.AssetID, o.GeometryTypeID, o.ValidGeography
	from @OutputSpatial o
	join
	(
		select os.OutputID, s.AssetGeography from @OutputSpatial os
		left join Asset.tAssetSpatial s on os.AssetID = s.AssetID and os.GeometryTypeID = s.GeometryTypeID
		where os.GeographyIsValid = 1
	) sos on o.OutputID = sos.OutputID
	where sos.AssetGeography is null
	;

end
GO
CREATE NONCLUSTERED INDEX [NC_tAssetAttribute_AttributeOptionTypeID]
    ON [Asset].[tAssetAttribute]([AttributeOptionTypeID] ASC);

