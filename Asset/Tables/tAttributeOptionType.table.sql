CREATE TABLE [Asset].[tAttributeOptionType] (
    [AttributeOptionTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [AttributeTypeID]         INT            NOT NULL,
    [AttributeOptionTypeDesc] NVARCHAR (255) NOT NULL,
    [DisplayOrder]            INT            NOT NULL,
    CONSTRAINT [PK_tAttributeOptionType] PRIMARY KEY CLUSTERED ([AttributeOptionTypeID] ASC),
    CONSTRAINT [FK_tAttributeOptionType_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [UK_tAttributeOptionType_AttributeTypeID_OptionTypeDesc] UNIQUE NONCLUSTERED ([AttributeTypeID] ASC, [AttributeOptionTypeDesc] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of AttributeOptionTypes.', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAttributeOptionType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAttributeOptionType';

