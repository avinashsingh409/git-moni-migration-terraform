﻿CREATE TABLE [Asset].[tQuickSearchCategoryMap](
	[QuickSearchCategoryMapID] [int] IDENTITY(1,1) NOT NULL,
	[QuickSearchID] INT NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_tQuickSearchCategoryMap] PRIMARY KEY CLUSTERED 
(
	[QuickSearchCategoryMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UK_QuickSearchCategoryMap_UniqueKey] UNIQUE NONCLUSTERED 
(
	[QuickSearchID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [Asset].[tQuickSearchCategoryMap] ADD  CONSTRAINT [FK_tQuickSearchCategoryMap_CategoryID_tCategory] FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID])
GO

ALTER TABLE [Asset].[tQuickSearchCategoryMap] ADD  CONSTRAINT [FK_tQuickSearchCategoryMap_QuickSearchID_tQuickSearch] FOREIGN KEY([QuickSearchID])
REFERENCES [Asset].[tQuickSearch] ([QuickSearchID])
ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Asset', @level1type=N'TABLE',@level1name=N'tQuickSearchCategoryMap'
GO