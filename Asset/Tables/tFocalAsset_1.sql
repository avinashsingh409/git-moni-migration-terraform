﻿CREATE TABLE [Asset].[tFocalAsset] (
    [FocalAssetId]   INT NOT NULL,
    [RelatedAssetId] INT NOT NULL,
    [LevelsAway]     INT NOT NULL,
    CONSTRAINT [PK_tFocalAsset] PRIMARY KEY NONCLUSTERED ([FocalAssetId] ASC, [RelatedAssetId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_FocalAsset_FocalAssetId_ReleatedAssetID_LevelsAway]
    ON [Asset].[tFocalAsset]([FocalAssetId] ASC, [RelatedAssetId] ASC, [LevelsAway] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170613-102728]
    ON [Asset].[tFocalAsset]([FocalAssetId] ASC, [LevelsAway] ASC, [RelatedAssetId] ASC);


GO
CREATE UNIQUE CLUSTERED INDEX [IDX_FocalAsset_Clustered]
    ON [Asset].[tFocalAsset]([RelatedAssetId] ASC, [FocalAssetId] ASC, [LevelsAway] ASC);

