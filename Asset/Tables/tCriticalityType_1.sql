﻿CREATE TABLE [Asset].[tCriticalityType] (
    [CriticalityTypeID] INT            NOT NULL,
    [CriticalityAbbrev] NVARCHAR (50)  NOT NULL,
    [CriticalityDesc]   NVARCHAR (255) NOT NULL,
    [DisplayOrder]      INT            NOT NULL,
    CONSTRAINT [PK_tCriticalityType] PRIMARY KEY CLUSTERED ([CriticalityTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tCriticalityType';

