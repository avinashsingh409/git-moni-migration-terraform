﻿CREATE TABLE [Asset].[tAssetAttachmentKeywordMap] (
    [AssetAttachmentKeywordMapID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [AssetAttachmentID]           UNIQUEIDENTIFIER NOT NULL,
    [KeywordID]                   BIGINT           NOT NULL,
    [CreatedBy]                   INT              NOT NULL,
    [TagDate]                     DATETIME         NOT NULL,
    [BackDate]                    DATE             NULL,
    CONSTRAINT [PK_tAssetAttachmentKeywordMap] PRIMARY KEY CLUSTERED ([AssetAttachmentKeywordMapID] ASC),
    CONSTRAINT [FK_tAssetAttachmentKeywordMap_tAssetAttachment] FOREIGN KEY ([AssetAttachmentID]) REFERENCES [Asset].[tAssetAttachment] ([AssetAttachmentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetAttachmentKeywordMap_tKeyword] FOREIGN KEY ([KeywordID]) REFERENCES [Asset].[tKeyword] ([KeywordID]),
    CONSTRAINT [FK_tAssetAttachmentKeywordMap_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);








GO
CREATE NONCLUSTERED INDEX [IX_tAssetAttachmentKeywordMap_KeywordID]
    ON [Asset].[tAssetAttachmentKeywordMap]([KeywordID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tAssetAttachmentKeywordMap_AssetAttachmentID]
    ON [Asset].[tAssetAttachmentKeywordMap]([AssetAttachmentID] ASC);

