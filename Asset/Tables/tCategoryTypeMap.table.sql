﻿CREATE TABLE [Asset].[tCategoryTypeMap](
	[CategoryTypeMapID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[CategoryTypeID] [int] NOT NULL,
 CONSTRAINT [PK_tCategoryTypeMap] PRIMARY KEY CLUSTERED 
(
	[CategoryTypeMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tCategoryTypeMap_CategoryTypeID] UNIQUE NONCLUSTERED 
(	
	[CategoryID] ASC,
	[CategoryTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Asset].[tCategoryTypeMap] ADD  
CONSTRAINT [FK_tCategoryTypeMap_CategoryID_tCategory] FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID]) ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tCategoryTypeMap] ADD  
CONSTRAINT [FK_tCategoryTypeMap_CategoryID_tCategoryType] FOREIGN KEY([CategoryTypeID])
REFERENCES [Asset].[tCategoryType] ([CategoryTypeID]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Asset', @level1type=N'TABLE',@level1name=N'tCategoryTypeMap'
GO