﻿CREATE TABLE [Asset].[tAssetArchive] (
    [AssetArchiveID]                        INT              IDENTITY (1, 1) NOT NULL,
    [AssetID]                               INT              NOT NULL,
    [ParentAssetID]                         INT              NULL,
    [AssetAbbrev]                           NVARCHAR (50)    NOT NULL,
    [AssetDesc]                             NVARCHAR (255)   NOT NULL,
    [AssetClassTypeID]                      INT              NOT NULL,
    [AssetTreeNodeChildConfigurationTypeID] INT              NULL,
    [DisplayOrder]                          INT              NOT NULL,
    [CreatedBy]                             NVARCHAR (MAX)   NOT NULL,
    [ChangedBy]                             NVARCHAR (MAX)   NOT NULL,
    [CreateDate]                            DATETIME         NOT NULL,
    [ChangeDate]                            DATETIME         NULL,
    [GlobalID]                              UNIQUEIDENTIFIER NOT NULL,
    [IsHidden]                              BIT              NOT NULL,
    CONSTRAINT [PK_tAssetArchive] PRIMARY KEY NONCLUSTERED ([AssetArchiveID] ASC)
);

 

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetArchive';

