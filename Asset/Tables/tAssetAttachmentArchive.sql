﻿CREATE TABLE [Asset].[tAssetAttachmentArchive] (
    [AssetAttachmentArchiveID] INT              IDENTITY (1, 1) NOT NULL,
    [AssetAttachmentID]        UNIQUEIDENTIFIER NOT NULL,
    [AssetID]                  INT              NOT NULL,
    [CreateDate]               DATETIME         NOT NULL,
    [ChangeDate]               DATETIME         NOT NULL,
    [CreatedByUserID]          INT              NOT NULL,
    [ChangedByUserID]          INT              NOT NULL,
    [ContentID]                UNIQUEIDENTIFIER NULL,
    [DisplayInstructions]      NVARCHAR (MAX)   NULL,
    [AttachmentType]           INT              NOT NULL,
    [Title]                    NVARCHAR (255)   NULL,
    [Caption]                  NVARCHAR (MAX)   NULL,
    [Deleted]                  BIT              DEFAULT ((0)) NULL,
    [DisplayOrder]             INT              DEFAULT ((1)) NOT NULL,
    [Favorite]                 BIT              DEFAULT ((0)) NOT NULL,
    [Head]                     UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    CONSTRAINT [PK_tAssetAttachmentArchive] PRIMARY KEY CLUSTERED ([AssetAttachmentArchiveID] ASC)
);




GO
