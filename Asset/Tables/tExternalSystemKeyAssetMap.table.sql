﻿CREATE TABLE [Asset].[tExternalSystemKeyAssetMap] (
    [ExternalSystemKeyAssetMapID] INT            IDENTITY (1, 1) NOT NULL,
    [ExternalSystemID]            INT            NOT NULL,
    [AssetID]                     INT            NOT NULL,
    [ExternalSystemKey]           NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Base_tExternalSystemKeyAssetMap] PRIMARY KEY CLUSTERED ([ExternalSystemKeyAssetMapID] ASC),
    CONSTRAINT [FK_Asset_tExternalSystemKeyAssetMap_ExternalSystemID_Asset_tExternalSystem] FOREIGN KEY ([ExternalSystemID]) REFERENCES [Asset].[tExternalSystem] ([ExternalSystemID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Asset_tExternalSystemKeyAssetMap_ExternalSystemTypeID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [UK_Asset_tExternalSystemKeyAssetMap_ExternalSystemID_AssetID_ExternalSystemKey] UNIQUE NONCLUSTERED ([ExternalSystemID] ASC, [AssetID] ASC, [ExternalSystemKey] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tExternalSystemKeyAssetMap';

