﻿CREATE TABLE [Asset].[tAssetKeywordMap] (
    [AssetKeywordMapID] BIGINT   IDENTITY (1, 1) NOT NULL,
    [AssetID]           INT      NOT NULL,
    [KeywordID]         BIGINT   NOT NULL,
    [CreatedBy]         INT      NOT NULL,
    [TagDate]           DATETIME NOT NULL,
    [BackDate]          DATE     NULL,
    CONSTRAINT [PK_tAssetKeywordMap] PRIMARY KEY CLUSTERED ([AssetKeywordMapID] ASC),
    CONSTRAINT [FK_tAssetKeywordMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetKeywordMap_tKeyword] FOREIGN KEY ([KeywordID]) REFERENCES [Asset].[tKeyword] ([KeywordID]),
    CONSTRAINT [FK_tAssetKeywordMap_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);










GO
CREATE NONCLUSTERED INDEX [IX_tAssetKeywordMap_KeywordID]
    ON [Asset].[tAssetKeywordMap]([KeywordID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tAssetKeywordMap_AssetID]
    ON [Asset].[tAssetKeywordMap]([AssetID] ASC);


GO

-- Trigger
CREATE TRIGGER [Asset].[AssetKeywordMapUpdate]
   ON  [Asset].[tAssetKeywordMap]
   FOR DELETE
AS
BEGIN

	BEGIN TRANSACTION 
	DECLARE @assetId int;
	DECLARE @keywordId int;

	BEGIN TRY
		DECLARE cDeleted CURSOR FOR SELECT assetid, keywordID FROM deleted d
		OPEN cDeleted
		FETCH NEXT FROM cDeleted INTO @assetId, @keywordId
		WHILE(@@FETCH_STATUS = 0) BEGIN
			EXEC Asset.spDeleteUnusedKeyword @assetId, @keywordId
			FETCH NEXT FROM cDeleted INTO @assetId, @keywordId
		END
		CLOSE cDeleted
		DEALLOCATE cDeleted

		
	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		PRINT N'Trigger Failed, rolling back';
		PRINT ERROR_MESSAGE();
	END CATCH;

END