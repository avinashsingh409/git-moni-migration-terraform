﻿CREATE TABLE [Asset].[tAssetMap] (
    [MapID]       INT               IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX)    NOT NULL,
    [Location]    [sys].[geography] NOT NULL,
    [Map_X1]      FLOAT (53)        NOT NULL,
    [Map_X2]      FLOAT (53)        NOT NULL,
    [Map_X3]      FLOAT (53)        NOT NULL,
    [Map_Y1]      FLOAT (53)        NOT NULL,
    [Map_Y2]      FLOAT (53)        NOT NULL,
    [Map_Y3]      FLOAT (53)        NOT NULL,
    [World_Long1] FLOAT (53)        NOT NULL,
    [World_Long2] FLOAT (53)        NOT NULL,
    [World_Long3] FLOAT (53)        NOT NULL,
    [World_Lat1]  FLOAT (53)        NOT NULL,
    [World_Lat2]  FLOAT (53)        NOT NULL,
    [World_Lat3]  FLOAT (53)        NOT NULL,
    [CreatedBy]   NVARCHAR (MAX)    DEFAULT ('BV') NOT NULL,
    [ChangedBy]   NVARCHAR (MAX)    DEFAULT ('BV') NOT NULL,
    [CreateDate]  DATETIME          DEFAULT (getdate()) NOT NULL,
    [ChangeDate]  DATETIME          DEFAULT (getdate()) NOT NULL,
    [AssetID]     INT               NULL,
    [GlobalID]    UNIQUEIDENTIFIER  DEFAULT (newid()) NOT NULL,
    PRIMARY KEY CLUSTERED ([MapID] ASC),
    CONSTRAINT [FK_Asset_tAssetMap_AssetID_Asset_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);







GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetMap';

