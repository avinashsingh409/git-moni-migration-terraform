CREATE TABLE [Asset].[tPeerGroup] (
    [PeerGroupID] INT            IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Asset.tPeerGroup] PRIMARY KEY CLUSTERED ([PeerGroupID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tPeerGroup';

