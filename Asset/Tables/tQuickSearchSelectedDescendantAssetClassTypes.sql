﻿CREATE TABLE [Asset].[tQuickSearchSelectedDescendantAssetClassTypes](
	[QuickSearchID] [int] NOT NULL,
	[DescendantAssetClassTypeID] [int] NOT NULL,
CONSTRAINT [PK_tQuickSearchSelectedDescendantAssetClassTypes] PRIMARY KEY CLUSTERED
(
	[QuickSearchID] ASC,
	[DescendantAssetClassTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [Asset].[tQuickSearchSelectedDescendantAssetClassTypes]  WITH CHECK ADD  CONSTRAINT [FK_tQuickSearchSelectedDescendantAssetClassTypes_QuickSearchID_tQuickSearch] FOREIGN KEY([QuickSearchID])
REFERENCES [Asset].[tQuickSearch] ([QuickSearchId])
ON DELETE CASCADE
GO

ALTER TABLE [Asset].[tQuickSearchSelectedDescendantAssetClassTypes]  WITH CHECK ADD  CONSTRAINT [FK_tQuickSearchSelectedDescendantAssetClassTypes_DescendantAssetClassTypeID_tAssetClassType] FOREIGN KEY([DescendantAssetClassTypeID])
REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID])
GO

ALTER TABLE [Asset].[tQuickSearchSelectedDescendantAssetClassTypes] CHECK CONSTRAINT [FK_tQuickSearchSelectedDescendantAssetClassTypes_QuickSearchID_tQuickSearch]
GO

ALTER TABLE [Asset].[tQuickSearchSelectedDescendantAssetClassTypes] CHECK CONSTRAINT [FK_tQuickSearchSelectedDescendantAssetClassTypes_DescendantAssetClassTypeID_tAssetClassType]
GO
