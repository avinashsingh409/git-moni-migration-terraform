CREATE TABLE [Asset].[tAssetClassTypeAttributeTypeMap] (
    [AssetClassTypeAttributeTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeID]                 INT NOT NULL,
    [AttributeTypeID]                  INT NOT NULL,
    CONSTRAINT [PK_tAssetClassTypeAttributeTypeMap] PRIMARY KEY CLUSTERED ([AssetClassTypeAttributeTypeMapID] ASC),
    CONSTRAINT [FK_tAssetClassTypeAttributeTypeMap_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetClassTypeAttributeTypeMap_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Asset', @level1type = N'TABLE', @level1name = N'tAssetClassTypeAttributeTypeMap';

