﻿CREATE VIEW Asset.vAssetEvent AS

-- Grouping Asset Type = 7
-- Event Asset Type = 9

-- handle situation where the events are direct children of the owning asset
select parent.AssetID as EventOwnerAssetID, children.GlobalID as EventGUID, children.AssetID as EventAssetID,children.AssetClassTypeID as EventAssetClassTypeID,
childType.AssetClassTypeDesc as EventTypeDesc
from Asset.tAsset parent
join Asset.tAssetClassType pType on parent.AssetClassTypeID = pType.AssetClassTypeID
join Asset.tAsset children on children.ParentAssetID = parent.AssetID
join Asset.tAssetClassType childType on children.AssetClassTypeID = childType.AssetClassTypeID
where pType.AssetTypeID<=6 and childType.AssetTypeID = 9
union
-- handle situation where there is a grouping asset node (asset type 7) between the owning asset and the event asset
select parent.AssetID as EventOwnerAssetID, grandChildren.GlobalID as EventGUID, grandChildren.AssetID as EventAssetID,grandChildren.AssetClassTypeID as EventAssetClassTypeID,
gcType.AssetClassTypeDesc as EventTypeDesc
from Asset.tAsset parent
join Asset.tAssetClassType pType on parent.AssetClassTypeID = pType.AssetClassTypeID
join Asset.tAsset children on children.ParentAssetID = parent.AssetID
join Asset.tAssetClassType childType on children.AssetClassTypeID = childType.AssetClassTypeID
join Asset.tAsset grandChildren on grandChildren.ParentAssetID = children.AssetID
join Asset.tAssetClassType gcType on grandChildren.AssetClassTypeID = gcType.AssetClassTypeID
where pType.AssetTypeID<=6 and childType.AssetTypeID = 7 and gcType.AssetTypeID = 9

GO
