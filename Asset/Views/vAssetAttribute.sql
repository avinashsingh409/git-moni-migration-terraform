﻿CREATE VIEW [Asset].[vAssetAttribute]
AS 

SELECT        Asset.tAssetAttribute.AssetAttributeID, Asset.tAssetAttribute.AssetID, Asset.tAssetAttribute.AttributeTypeID, Asset.tAssetAttribute.AttributeOptionTypeID, Asset.tAssetAttribute.Attribute_int, Asset.tAssetAttribute.Attribute_string, 
                         Asset.tAssetAttribute.Attribute_float, Asset.tAssetAttribute.Favorite, Asset.tAssetAttribute.DisplayOrder, Asset.tAttributeType.DisplayFormat, Asset.tAttributeType.EngrUnits, Asset.tAttributeOptionType.AttributeOptionTypeDesc, 
                         Asset.ufnGetAttributeValue(Asset.tAttributeType.DisplayFormat, Asset.tAssetAttribute.Attribute_int, Asset.tAssetAttribute.Attribute_string, Asset.tAssetAttribute.Attribute_float,Asset.tAssetAttribute.Attribute_date, Asset.tAttributeOptionType.AttributeOptionTypeDesc) 
                         AS Value,
						 Asset.tAssetAttribute.Attribute_date
FROM            Asset.tAssetAttribute INNER JOIN
                         Asset.tAttributeType ON Asset.tAssetAttribute.AttributeTypeID = Asset.tAttributeType.AttributeTypeID LEFT OUTER JOIN
                         Asset.tAttributeOptionType ON Asset.tAssetAttribute.AttributeOptionTypeID = Asset.tAttributeOptionType.AttributeOptionTypeID
GO
