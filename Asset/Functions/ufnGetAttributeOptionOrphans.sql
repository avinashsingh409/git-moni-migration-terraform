﻿CREATE FUNCTION Asset.ufnGetAttributeOptionOrphans
(
@AttributeTypeID int,
@Options Base.tpStringList READONLY
)
RETURNS @Orphans TABLE
(
Id int,
Label nvarchar(255)
)
AS
BEGIN
	INSERT INTO @Orphans
	SELECT o.AttributeOptionTypeID, AttributeOptionTypeDesc
		FROM Asset.tAttributeOptionType o
		INNER JOIN Asset.tAttributeType at ON at.AttributeTypeID = o.AttributeTypeID
		WHERE o.AttributeTypeID = @AttributeTypeID AND IsStandard = 0 
		AND UPPER(AttributeOptionTypeDesc) NOT IN (SELECT UPPER(value) FROM @Options)
RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAttributeOptionOrphans] TO [TEUser]
    AS [dbo];
GO


