﻿CREATE FUNCTION [Asset].[ufnWhichAttributeNamesAreProhibited]
(
	@checkAssetID as int,
	@securityUserID as int,
	@attributeNameCandidates as Base.tpStringList READONLY
)
RETURNS @result TABLE
(
   AttributeName varchar(255)
)

AS

BEGIN

INSERT INTO @result (AttributeName) select DISTINCT [Value] from @attributeNameCandidates a

declare @clientIDs as base.tpIntList
insert into @clientIDs select ClientID FROM Asset.ufnGetClientsForAsset(@checkAssetID)

declare @prohibited as base.tpStringList
insert into @prohibited
select attType.AttributeTypeDesc from Asset.tAssetAttributeCategoryMap map
JOIN Asset.tAttributeType attType ON map.AttributeTypeID = attType.AttributeTypeID
JOIN Asset.tCategory c on map.CategoryID = c.CategoryID JOIN @clientIDs d ON  
	                                        c.ClientID = d.ID JOIN Asset.tCategoryTypeMap ctm  ON c.CategoryID = ctm.CategoryID AND ctm.CategoryTypeID = 1 AND 
											(c.CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
											AND
											c.CategoryID
											NOT IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap r JOIN AccessControl.tUserRoleMap urm 
											  ON r.SecurityRoleID = urm.SecurityRoleID WHERE urm.SecurityUserID = @securityUserID))

insert into @prohibited
select attType.AttributeTypeDesc from Asset.tAssetAttributeCategoryMap map
JOIN Asset.tAttributeType attType ON map.AttributeTypeID = attType.AttributeTypeID
JOIN Asset.tCategory c on map.CategoryID = c.CategoryID AND c.ClientID IS NULL JOIN Asset.tCategoryTypeMap ctm  ON c.CategoryID = ctm.CategoryID AND ctm.CategoryTypeID = 1 AND 
											(c.CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
											AND
											c.CategoryID
											NOT IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap r JOIN AccessControl.tUserRoleMap urm 
											  ON r.SecurityRoleID = urm.SecurityRoleID WHERE urm.SecurityUserID = @securityUserID))

DELETE FROM @result WHERE AttributeName NOT in (SELECT [VALUE] from @prohibited)

RETURN

END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnWhichAttributeNamesAreProhibited] TO [TEUser]
    AS [dbo];
GO