﻿CREATE FUNCTION [Asset].[ufnGetAssetDescendantsSecurity]
(
	@asset int,
	@levels INT,
	@includeParent BIT,
	@securityUserID INT,
	@appContext INT
)
RETURNS @returntable TABLE
(
	assetID INT,
	lvl INT
)
AS
BEGIN
	DECLARE @rootAssets Base.tpIntList

	IF(@asset IS NULL)
	BEGIN
		INSERT INTO @rootAssets(id) 
		SELECT AssetID FROM Asset.ufnGetRootAssets(@securityUserID, @appContext)

		SET @levels = @levels - 1 
	END
	ELSE 
	BEGIN
		INSERT INTO @rootAssets(id) VALUES ( @asset)
	END 

	DECLARE @results as TABLE(assetID int, lvl int)

	SET @levels = -1 * @levels 

	INSERT INTO @results(assetID, lvl)
	SELECT assetHop.EndingAssetId, -1 * assetHop.AncestryLevel
	FROM Asset.tAssetHop assetHop
	INNER JOIN @rootAssets rootAssets ON assetHop.HopAssetId = rootAssets.id
	WHERE assetHop.AncestryLevel >= @levels

	IF @includeParent = 0 
	BEGIN
		DELETE FROM @results WHERE assetID = @asset
	END 

	INSERT INTO @returntable(assetID, lvl)
	SELECT assetID, lvl FROM @results a WHERE Asset.ufnDoesUserHaveAccessToAsset( a.assetID , @securityUserID, @appContext) = 1

	RETURN
END
GO

GRANT SELECT
	ON OBJECT::[Asset].[ufnGetAssetDescendantsSecurity] TO [TEUser]
	AS [dbo];