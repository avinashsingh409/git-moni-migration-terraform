﻿CREATE FUNCTION [Asset].[ufnAssetAncestorsForAllDistinct] 
(
	-- Add the parameters for the stored procedure here
	@securityuserid int,
	@assetIDs base.tpIntList READONLY
)
RETURNS TABLE
AS
RETURN
(
    SELECT distinct b.AssetID 
	FROM @assetIDs a 
	CROSS apply Asset.ufnAssetAncestors(A.id ) b 
	JOIN Asset.ufnGetAssetIdsForUser(@securityuserid) c on b.AssetID = c.AssetID

)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnAssetAncestorsForAllDistinct] TO [TEUser]
    AS [dbo];