﻿
CREATE FUNCTION [Asset].[ufnGetAssetRelationsAndDescendants]
(
	@assetID int,
	@securityUserID int,
	@includeAllAssets bit,
	@includeDescendants bit,
	@includeChildren bit,
	@includeAncestors bit,
	@includeParents bit,
	@includeSiblings bit,
	@includeUnitDescendants bit,
	@includeSecondCousins bit,
	@includeCousins bit,
	@includeUnit bit,
	@includeSelf bit,
	@assetClassType int
)
RETURNS TABLE
AS
RETURN
(
select DISTINCT c.AssetID from [Asset].[ufnGetAssetRelations]
(
	@assetID,
	@securityUserID,
	@includeAllAssets,
	@includeDescendants,
	@includeChildren,
	@includeAncestors,
	@includeParents,
	@includeSiblings,
	@includeUnitDescendants,
	@includeSecondCousins,
	@includeCousins,
	@includeUnit,
	@includeSelf) a 
	JOIN Asset.tAssetHop b on a.AssetID = b.HopAssetId JOIN Asset.tAsset c on b.EndingAssetId = c.AssetID 
	WHERE c.IsHidden = 0 AND (ISNULL(@assetClassType,-1) <= 0 OR c.AssetClassTypeID = @assetClassType)

)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetRelationsAndDescendants] TO [TEUSER]
    AS [dbo];

GO