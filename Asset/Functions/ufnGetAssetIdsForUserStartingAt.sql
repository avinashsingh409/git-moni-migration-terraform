﻿CREATE FUNCTION [Asset].[ufnGetAssetIdsForUserStartingAt]
(
  @securityuserid  INT,
  @startingAssetID INT
)
RETURNS TABLE AS
RETURN
(	 
select a.EndingAssetId AS AssetID from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID JOIN   
AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID   
JOIN Asset.tAsset d on a.EndingAssetId = d.AssetID  
JOIN Asset.tAssetHop e on d.AssetID = e.EndingAssetId   
WHERE (c.SecurityUserID = @securityUserID) AND d.IsHidden = 0 AND (e.HopAssetID = @startingAssetID)  
GROUP BY a.EndingAssetId   
  HAVING   
  MIN(CAST(B.Allowed as INT)) = 1  

UNION ALL

select a.EndingAssetId AS AssetID from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID JOIN   
AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID   
JOIN Asset.tAsset d on a.EndingAssetId = d.AssetID  
JOIN Asset.tAssetHop e on d.AssetID = e.EndingAssetId   
WHERE (c.SecurityUserID = @securityUserID) AND d.IsHidden = 0 AND (@startingAssetID IS NULL)  
GROUP BY a.EndingAssetId   
  HAVING   
  MIN(CAST(B.Allowed as INT)) = 1  
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetIdsForUserStartingAt] TO [TEUser]
    AS [dbo];

GO
