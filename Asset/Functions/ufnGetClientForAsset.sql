﻿CREATE FUNCTION Asset.ufnGetClientForAsset
(		
	@AssetID int
)
RETURNS int
AS
BEGIN
  DECLARE @clientID as int = NULL
  
  SELECT TOP 1 @clientID = ClientID FROM Asset.ufnGetClientsForAsset(@AssetID) ORDER BY AncestryGenerationsAway

  RETURN @clientID
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnGetClientForAsset]  TO [TEUser]
    AS [dbo];
GO