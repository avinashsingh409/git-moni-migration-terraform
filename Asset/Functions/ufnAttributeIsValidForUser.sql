﻿CREATE FUNCTION [Asset].[ufnAttributeIsValidForUser]
(	
	@UserID int,
	@AssetID int,
	@AttributeTypeID int	
)
RETURNS TABLE
AS
RETURN
(
	 SELECT 1 AS [IsValid]
     WHERE NOT (EXISTS(SELECT * FROM Asset.tAssetAttributeCategoryMap a JOIN AccessControl.tRoleCategoryMap b ON a.CategoryID = b.CategoryID WHERE AttributeTypeID=@AttributeTypeID)
        AND NOT EXISTS(SELECT * FROM Asset.ufnGetClientsForAsset(@AssetID) client JOIN Asset.tCategory c ON client.ClientID = c.ClientID JOIN  Asset.tAssetAttributeCategoryMap map ON c.CategoryID = map.CategoryID AND map.AttributeTypeID = @AttributeTypeID
                        JOIN Asset.tCategoryTypeMap ctm  ON c.CategoryID = ctm.CategoryID AND ctm.CategoryTypeID = 1 AND 
                        (EXISTS (SELECT CategoryID FROM AccessControl.tRoleCategoryMap WHERE CategoryID=c.CategoryID)
                         AND
                        NOT EXISTS (SELECT CategoryID FROM AccessControl.tRoleCategoryMap r JOIN AccessControl.tUserRoleMap urm 
                            ON r.SecurityRoleID = urm.SecurityRoleID WHERE urm.SecurityUserID = @UserID AND r.CategoryID= c.CategoryID))))
)

GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnAttributeIsValidForUser]  TO [TEUser]
    AS [dbo];
GO
