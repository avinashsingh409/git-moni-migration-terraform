﻿CREATE FUNCTION [Asset].[ufnGetAssetDescendantsFromNode]
(
   @nodeGuid uniqueidentifier,
   @userID int
)
RETURNS @returnTable TABLE
(
  NodeID uniqueidentifier,
  AssetID int
)
AS
BEGIN

DECLARE @table TABLE
(
       nodeID UniqueIdentifier,
       AssetID int NULL,
       LinkedNode bit,
       NodeType int,
       NodeBehavior int,
       lvl int
);     

       WITH getLevel(nodeID, assetid, linkedNode, nodeType, nodeBehavior, lvl)
       AS(
              SELECT a.NodeId AS NodeID, A.AssetId as AssetID, case when a.AssetId IS null then 0 else 1 end as linkedNode, A.NodeTypeId, A.AssetNodeBehaviorTypeId
                as NodeBehavior, 0 AS lvl FROM Asset.tCustomTreeNode a WHERE a.NodeId = @nodeGuid 
              UNION ALL 
              SELECT a.NodeId AS NodeID, A.AssetId, case when a.AssetId IS null then 0 else 1 end as linkedNode, A.NodeTypeId, A.AssetNodeBehaviorTypeId
                as NodeBehavior, b.lvl + 1 AS lvl FROM Asset.tCustomTreeNode a 
              INNER JOIN getLevel b ON a.ParentNodeId = b.nodeID
              WHERE b.lvl < 100
       )
       INSERT INTO @table(nodeID, AssetID, LinkedNode, NodeType, NodeBehavior, lvl)
       SELECT NodeID, assetid, linkedNode, nodeType, nodeBehavior, lvl FROM getLevel

       
-- asset linked nodes including descendants
insert into @table
select nodeID,b.AssetID,0 as LinkedNode,-1,-1,lvl from @table a CROSS APPLY 
Asset.ufnGetAssetIdsForUserStartingAt(@userID,a.AssetID) b
WHERE a.NodeType = 2 and a.NodeBehavior = 3

-- asset linked nodes including children
insert into @table
select nodeID,b.AssetID,0 as LinkedNode,-1,-1,lvl from @table a JOIN Asset.tAsset b on b.ParentAssetID = a.AssetID 
WHERE a.NodeType = 2 and a.NodeBehavior = 2 AND Asset.ufnDoesUserHaveAccessToAsset(b.AssetID,@userID,-1) = 1

declare @ids as base.tpintlist
insert into @ids select distinct assetid from @table where AssetID is not null

INSERT INTO @returnTable
select distinct a.nodeID,a.AssetID from @table a join @ids b on a.AssetID = b.id OPTION (recompile)

RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetDescendantsFromNode] TO [TEUser]
    AS [dbo];

