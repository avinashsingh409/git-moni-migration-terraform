﻿CREATE FUNCTION [Asset].[ufnSearchAssetsById]
(
	 @securityUserId int,
	 @searchAssetIds Base.tpIntList READONLY
)
RETURNS 
@AssetIds TABLE 
(
	AssetId int
)
AS
BEGIN
	IF @securityUserId IS NULL
	BEGIN
		INSERT INTO @AssetIds
			SELECT a.id FROM @searchAssetIds a
			JOIN Asset.tAsset b on a.id = b.AssetID
	END
	ELSE
	BEGIN
		INSERT INTO @AssetIds
			SELECT AssetID
			FROM Asset.ufnUserHasAccessToAssets(@securityUserID, @searchAssetIds)
	END
	RETURN 
END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSearchAssetsById] TO [TEUser]
    AS [dbo];

