﻿CREATE FUNCTION [Asset].[ufnGetEventNotesForEventView](
	 @eventIds Base.tpIntList READONLY
)
RETURNS TABLE AS 
RETURN
(
	SELECT a.EventOwnerAssetID, a.EventAssetClassTypeID, a.EventTypeDesc, a.EventAssetID, note.Attribute_string AS EventNotes
	FROM Asset.vAssetEvent a
	JOIN @eventIds e ON a.EventAssetID = e.id
	JOIN Asset.tAssetAttribute note ON a.EventAssetID = note.AssetID
	WHERE note.AttributeTypeID = 209
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetEventNotesForEventView] TO [TEUser]
    AS [dbo];

GO