﻿CREATE FUNCTION [Asset].[ufnAssetAncestors] 
(
	-- Add the parameters for the stored procedure here
	@assetID int
)
RETURNS TABLE
AS
RETURN
(
	select HopAssetId as AssetID
	from Asset.tAssetHop
	where EndingAssetId = @assetID
	AND HopAssetId <> @assetID
)
GO
GRANT SELECT
	ON OBJECT::[Asset].[ufnAssetAncestors] TO [TEUser]
	AS [dbo];