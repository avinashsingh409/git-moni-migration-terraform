﻿CREATE FUNCTION [Asset].[ufnGetOnlyPhysicalAssetIdsForUser]
(
  @securityuserid  INT
)
RETURNS TABLE AS
RETURN
(	 
select a.EndingAssetId AS AssetID from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID JOIN 
AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 
JOIN Asset.tAsset d on a.EndingAssetId = d.AssetID JOIN Asset.tAssetClassType e on d.AssetClassTypeID = e.AssetClassTypeID 
WHERE (c.SecurityUserID = @securityUserID) AND d.IsHidden = 0 AND e.AssetTypeID <= 6
GROUP BY a.EndingAssetId 
		HAVING 
		MIN(CAST(B.Allowed as INT)) = 1

)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetOnlyPhysicalAssetIdsForUser] TO [TEUSER]
    AS [dbo];

GO
