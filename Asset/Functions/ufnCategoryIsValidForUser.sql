﻿CREATE FUNCTION [Asset].[ufnCategoryIsValidForUser]
(	
	@UserID int,
	@AssetID int,
	@Name varchar(255),
	@CategoryTypeID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit
	SET @result = 0 --until otherwise determined.
	
	IF NOT EXISTS(SELECT * FROM Asset.tCategoryType WHERE CategoryTypeID = @CategoryTypeID)
	  BEGIN	  
	  RETURN 0
	  END
		
	IF EXISTS(SELECT * FROM Asset.ufnGetAvailableAssetCategories(@UserID,@AssetID,@CategoryTypeID) a 
	          WHERE a.[Name] = @Name)
	  BEGIN
	  -- category exists for this user, so return TRUE
	  set @result = 1
	  END	
	-- now check if there is a category with that name but against a different category type that the user has rights to
	ELSE 
	  BEGIN
	  IF EXISTS(SELECT * FROM Asset.tCategoryType a CROSS APPLY Asset.ufnGetAvailableAssetCategories(@UserID,@AssetID,a.categorytypeid) b
	                WHERE b.[Name] = @Name)
	    BEGIN
		SET @result = 1
        END
	  ELSE
	    BEGIN	  	  
	    IF NOT EXISTS(SELECT * FROM Asset.ufnGetClientsForAsset(@AssetID) a JOIN Asset.tCategory b on a.ClientID = b.ClientID WHERE b.[Name] = @Name)	  
	      BEGIN
		  -- category name doesn't exist for anywhere up the hierarchy from this asset
		  SET @result = 1
		  END
	    ELSE
	      BEGIN
		  SET @result = 0
		  END
		END	  
	  END
	-- Return the result of the function
	RETURN @result
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnCategoryIsValidForUser]  TO [TEUser]
    AS [dbo];
GO