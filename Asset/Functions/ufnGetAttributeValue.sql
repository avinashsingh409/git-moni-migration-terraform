﻿CREATE FUNCTION [Asset].[ufnGetAttributeValue] 
(
	@DisplayFormat NVARCHAR(100),
	@Attribute_int INT, 
	@Attribute_string NVARCHAR(MAX),
	@Attribute_float REAL,
	@Attribute_date DATETIMEOFFSET,
	@AttributeOptionValue NVARCHAR(255)
)
RETURNS NVARCHAR(255)
AS
BEGIN
	--This is varchar max because the string field in the attribute is varchar max
	DECLARE @result NVARCHAR(MAX)

	-- This should match the formatting of the get value function of the asset attribute object.
	--Eventually we can create a small attribute that just lets this method format and carries along the result. 
	IF @DisplayFormat = 'curve'
	BEGIN
		SET @result = @Attribute_string
	END
	ELSE IF @DisplayFormat = 'int'
	BEGIN
		SET @result = FORMAT( @Attribute_int, 'G', 'en-us')
	END
	ELSE IF @DisplayFormat = 'radio'
	BEGIN
		SET @result = @AttributeOptionValue
	END
	ELSE IF @DisplayFormat = 'float'
	BEGIN
		SET @result = FORMAT( @Attribute_float, 'G', 'en-us')
	END
	ELSE IF @DisplayFormat = 'boolean'
	BEGIN
		IF @Attribute_int = 1 
			SET @result = 'true'
		ELSE
			SET @result = 'false'
	END
	ELSE IF @DisplayFormat like 'f%'
	BEGIN
		SET @result = FORMAT( @Attribute_float, @DisplayFormat, 'en-us')
	END
	ELSE IF @DisplayFormat like 'p%'
	BEGIN
		SET @result = FORMAT( (@Attribute_float / 100), @DisplayFormat, 'en-us')
	END
	ELSE IF @DisplayFormat = 'date'
	BEGIN
		SET @result = COALESCE(FORMAT(@Attribute_date,N'M/d/yyyy, h\:mm\:ss ttK'),'')
	END
	ELSE 
	BEGIN
		SET @result = @Attribute_string
	END

	-- Return the result of the function
	RETURN @result
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnGetAttributeValue] TO [TEUser]
    AS [dbo];
GO