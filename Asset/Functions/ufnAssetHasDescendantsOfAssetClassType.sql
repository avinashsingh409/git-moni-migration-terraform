﻿


CREATE FUNCTION [Asset].[ufnAssetHasDescendantsOfAssetClassType] (
	-- Add the parameters for the stored procedure here
	@rootID int = NULL, 
	@assetClassTypeID int
	)
RETURNS bit
AS
BEGIN

	DECLARE @hasDescendants bit = 0;

	WITH AssetHierarchy (Level, AssetID, ParentAssetID, AssetClassTypeID, Success)
AS
(
	SELECT 0 AS Level,
		AssetHierarchy1.AssetID,
		AssetHierarchy1.ParentAssetID,
		AssetClassType1.AssetClassTypeID,
		'N'
	FROM Asset.tAsset AssetHierarchy1
	INNER JOIN Asset.tAssetClassType AssetClassType1 ON AssetHierarchy1.AssetClassTypeID = AssetClassType1.AssetClassTypeID
	WHERE (@rootID IS NULL AND (AssetHierarchy1.ParentAssetID = 0 OR AssetHierarchy1.ParentAssetID IS NULL)) 
		   OR (@rootID IS NOT NULL AND ( AssetHierarchy1.AssetID = @rootID))
		   
	UNION ALL
	
	SELECT Level+1 AS Level,
		AssetHierarchy1.AssetID,
		AssetHierarchy1.ParentAssetID,
		AssetClassType1.AssetClassTypeID,
		CASE WHEN AssetClassType1.AssetClassTypeID = @assetClassTypeID THEN 'Y' ELSE 'N' END
		
		
	FROM Asset.tAsset AssetHierarchy1 INNER JOIN AssetHierarchy ON AssetHierarchy1.ParentAssetID = AssetHierarchy.AssetID
	INNER JOIN Asset.tAssetClassType AssetClassType1 ON AssetHierarchy1.AssetClassTypeID = AssetClassType1.AssetClassTypeID
		
)
	
	SELECT @hasDescendants = (CASE WHEN (SELECT COUNT(AssetID) FROM AssetHierarchy WHERE Success = 'Y') > 0 THEN 1 ELSE 0 END);

	RETURN @hasDescendants;

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[ufnAssetHasDescendantsOfAssetClassType] TO [TEUser]
    AS [dbo];

