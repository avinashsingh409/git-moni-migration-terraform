﻿CREATE FUNCTION [Asset].[ufnDoesUserHaveAccessToAsset] (
	@assetID int, 
	@securityuserid int,
	@appContextId int = -1
	)
RETURNS bit
AS
BEGIN	
	DECLARE @allowed int = 0; 

    IF ISNULL(@securityuserid,-1) >= 1
	  BEGIN
	  if exists(select * from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID join AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 	        
			where a.EndingAssetId = @assetID and b.Allowed = 0 and c.SecurityUserID = @securityuserid)
		begin
		set @allowed = 0
		end
  	  else
		begin
		if exists(select * from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID join AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 
			where a.EndingAssetId = @assetID and b.Allowed = 1 and c.SecurityUserID = @securityuserid)
			begin
			set @allowed = 1

			if ISNULL(@appContextId,-1) >=0
			  begin
			  if exists(select * from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID join AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 
					JOIN AccessControl.tAccessRule d ON c.SecurityRoleID = d.SecurityRoleID JOIN UIConfig.tAppContext e on d.SecurityResourceID = e.SecurityResourceID
					where a.EndingAssetId = @assetID and b.Allowed = 0 and c.SecurityUserID = @securityuserid and e.AppContextID = @appContextId)
				begin
				set @allowed = 0
				end
			  end
			  
			end
		end
      IF @allowed = 1
	    BEGIN
		SELECT @allowed = 0 FROM Asset.tAsset where AssetID = @assetID and IsHidden = 1
		END
	  END
	ELSE
		BEGIN
		select @allowed = 1 from Asset.tAsset WHERE AssetID = @assetID and IsHidden = 0
		END
	return @allowed

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnDoesUserHaveAccessToAsset] TO [TEUser]
    AS [dbo];