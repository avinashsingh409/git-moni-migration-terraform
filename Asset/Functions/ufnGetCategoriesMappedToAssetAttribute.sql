﻿CREATE FUNCTION [Asset].[ufnGetCategoriesMappedToAssetAttribute]
(	
	@AssetID int,
	@AttributeTypeID int
)
RETURNS @categories TABLE
(
	CategoryID int PRIMARY KEY not NULL,
	[Name] varchar(255)
)
AS
BEGIN

    -- first get global categories
    INSERT INTO @categories (CategoryID,[Name])
	SELECT cat.CategoryID,cat.[Name] FROM Asset.tCategory cat
		JOIN Asset.tAssetAttributeCategoryMap map ON cat.CategoryID = map.CategoryID and map.AttributeTypeID = @AttributeTypeID
		JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID WHERE 
	    d.CategoryTypeID = 1 AND cat.ClientID IS NULL

    INSERT INTO @categories (CategoryID,[Name])
	SELECT cat.CategoryID,cat.[Name] FROM Asset.ufnGetClientsForAsset(@assetid) b 
		JOIN Asset.tCategory cat ON b.ClientID = cat.ClientID
		JOIN Asset.tAssetAttributeCategoryMap map ON cat.CategoryID = map.CategoryID and map.AttributeTypeID = @AttributeTypeID
		JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID WHERE 
	    d.CategoryTypeID = 1 
	RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetCategoriesMappedToAssetAttribute] TO [TEUser]
    AS [dbo];
GO
