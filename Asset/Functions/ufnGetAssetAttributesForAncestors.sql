﻿CREATE FUNCTION [Asset].[ufnGetAssetAttributesForAncestors]
(
	@assetIDs AS Base.tpIntList READONLY,
	@securityUserId int,
	@attributeTypeId int,
	@attributeTypeKey nvarchar(100),
	@includeSelf bit
)
RETURNS TABLE
AS
RETURN
	SELECT
		 i.DescendentAssetID AS [SearchAssetID]
		,i.AncestryGenerationsAway
		,a.AssetID AS [AncestorAssetID]
		,a.AssetAttributeID
		,a.AttributeTypeID
		,a.AttributeOptionTypeID
		,a.Attribute_int
		,a.Attribute_string
		,a.Attribute_float
		,a.Attribute_date
		,a.Favorite
		,a.DisplayOrder
	FROM Asset.ufnAssetsAncestorsWithLevelsIncludeAssets(@assetIDs) i
	LEFT JOIN Asset.tAssetAttribute a ON a.AssetID=i.AssetID
	LEFT JOIN Asset.tAttributeType t ON t.AttributeTypeID = a.AttributeTypeID
	WHERE a.AssetID IS NOT NULL AND a.AssetAttributeID IS NOT NULL AND a.AttributeTypeID IS NOT NULL
	AND Asset.ufnDoesUserHaveAccessToAsset(i.AssetID, @securityUserId, DEFAULT) = 1
	AND (@attributeTypeId IS NULL OR t.AttributeTypeID = @attributeTypeId)
	AND (@attributeTypeKey IS NULL OR t.AttributeTypeKey=@attributeTypeKey)
	AND (@includeSelf = 1 OR i.AncestryGenerationsAway > 0)
GO

GRANT SELECT
	ON OBJECT::[Asset].[ufnGetAssetAttributesForAncestors] TO [TEUser]
	AS [dbo];