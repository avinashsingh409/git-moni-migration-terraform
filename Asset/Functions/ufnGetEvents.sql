﻿CREATE FUNCTION [Asset].[ufnGetEvents]
(
	@securityUserID INT,
	@assetSubtreeRootID INT = null
)
RETURNS TABLE
AS
RETURN
(
select 
a.EventAssetID as EventID,e.ParentAssetID as EventGroupID,a.EventOwnerAssetID as AssetID,a.EventGUID,
e.AssetAbbrev as EventAbbrev, e.AssetDesc as EventDesc, a.EventAssetClassTypeID as AssetClassTypeID,
e.AssetTreeNodeChildConfigurationTypeID, e.DisplayOrder,
createdUser.SecurityUserID AS [CreatedBy],
CAST(e.[CreateDate] AS DATETIMEOFFSET) AS [CreatedDate],
changedUser.SecurityUserID AS [ChangedBy],
CAST(e.[ChangeDate] AS DATETIMEOFFSET) AS [ChangedDate],
e.IsHidden,
e.Track,
note.Attribute_string as Note,
psd.Attribute_date as PlannedStartDate,
ped.Attribute_date as PlannedEndDate,
asd.Attribute_date as ActualStartDate,
aed.Attribute_date as ActualEndDate

 from Asset.vAssetEvent a JOIN Asset.tAsset e on a.EventAssetID = e.AssetID
    JOIN AccessControl.tUser createdUser on createdUser.Username = e.CreatedBy
	JOIN AccessControl.tUser changedUser on changedUser.Username = e.ChangedBy
	LEFT JOIN Asset.tAssetAttribute psd ON a.EventAssetID = psd.AssetID AND psd.AttributeTypeID = 205
	LEFT JOIN Asset.tAssetAttribute ped ON a.EventAssetID = ped.AssetID AND ped.AttributeTypeID = 206
	LEFT JOIN Asset.tAssetAttribute asd ON a.EventAssetID = asd.AssetID AND asd.AttributeTypeID = 207
    LEFT JOIN Asset.tAssetAttribute aed ON a.EventAssetID = aed.AssetID AND aed.AttributeTypeID = 208
	LEFT JOIN Asset.tAssetAttribute note ON a.EventAssetID = note.AssetID AND note.AttributeTypeID = 209	
	INNER JOIN Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @assetSubtreeRootID) validAssets ON a.EventAssetID = validAssets.AssetID
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetEvents] TO [TEUser]
    AS [dbo];

GO
