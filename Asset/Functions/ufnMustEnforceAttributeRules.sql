﻿
---------------------------------------------------------------

CREATE FUNCTION [Asset].[ufnMustEnforceAttributeRules] 
(		
	@attributeTypeID INT
)
RETURNS bit
AS
BEGIN		
DECLARE @enforce BIT
	SELECT @enforce = CASE WHEN IsExempt <> 1 THEN 1 ELSE 0 END FROM Asset.tAttributeType WHERE AttributeTypeID = @attributeTypeID   
	RETURN @enforce
END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnMustEnforceAttributeRules]  TO [TEUser]
    AS [dbo];
GO