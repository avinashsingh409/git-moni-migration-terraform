﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION Asset.ufnGetAssetIdsForClientsAndUser
(
	@clientIds Base.tpIntList READONLY,
	@userId int
)
RETURNS 
@assetIds TABLE
(
	 assetId int
)
AS
BEGIN
	 DECLARE @assetId int;

	-- Fill the table variable with the rows for your result set
	 DECLARE cClients CURSOR FOR 
		  SELECT b.assetID 
		  FROM @clientIds a 
		  INNER JOIN Asset.tClient c ON c.ClientId = a.id
		  INNER JOIN Asset.tAsset b ON c.AssetId = b.AssetID
	 OPEN cClients 

	 FETCH NEXT FROM cClients INTO @assetId
	 WHILE @@FETCH_STATUS = 0 BEGIN
		  INSERT INTO @assetIds(assetId)
				SELECT AssetID FROM Asset.ufnGetAssetIdsForUserStartingAt(@userId, @assetId)
		  FETCH NEXT FROM cClients INTO @assetId	  
	 END
	 
	 CLOSE cClients
	 DEALLOCATE cClients 

	 RETURN 
END
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetIdsForClientsAndUser] TO [TEUser]
    AS [dbo];

