﻿CREATE FUNCTION [Asset].[ufnAssetsFromGridSearch]
(
	@assetsAlreadyCheckedForSecurity AS Base.tpIntList READONLY,
	@attributeSearchCriteriaByAttributeTypeID AS Base.tpIntStringPair READONLY,
	@theseAttributeTypesMustMatchExactly AS Base.tpIntList READONLY,
	@assetTagFilter NVARCHAR(255),
	@assetPathFilter NVARCHAR(255),
	@sortAscending BIT,
	@sortByAssetDesc BIT,
	@sortByAssetKeywords BIT,
	@sortByAssetPath BIT,
	@sortByAttributeTypeID INT,
	@pageNumber INT, -- 1-based
	@assetsPerPage INT
)
RETURNS @filteredAssetIDs TABLE
(
	AssetID INT PRIMARY KEY,
	DisplayOrder INT
)
AS
BEGIN
	DECLARE @workingAssets Base.tpIntList
	INSERT INTO @workingAssets SELECT * FROM @assetsAlreadyCheckedForSecurity
	
	-- perform tag filtering
	IF @assetTagFilter IS NOT NULL AND LEN(@assetTagFilter) > 0
	BEGIN
		DECLARE @searchWords Base.tpStringList
		INSERT INTO @searchWords VALUES (@assetTagFilter)
		DECLARE @tagFoundAssets Base.tpIntList
		INSERT INTO @tagFoundAssets
			SELECT AssetID
			FROM Asset.ufnSearchAssetsByKeyword(NULL, @workingAssets, @searchWords, 1)
		DELETE @workingAssets
			FROM @workingAssets a
			LEFT OUTER JOIN @tagFoundAssets b on a.id = b.id
			WHERE b.id IS NULL -- delete where @tagFoundAssets doesnt have the asset from @workingAssets
	END

	-- perform the attribute filtering
	IF EXISTS (SELECT TOP 1 * FROM @attributeSearchCriteriaByAttributeTypeID)
	BEGIN
		-- call the other fn
		DELETE @workingAssets
			FROM @workingAssets a
			LEFT OUTER JOIN Asset.ufnSearchAssetsByAttributes(NULL, @workingAssets, @attributeSearchCriteriaByAttributeTypeID, @theseAttributeTypesMustMatchExactly) b ON b.AssetID = a.id
			WHERE b.AssetID IS NULL -- delete where not in filtered list
	END

	-- lastly, perform the path search
	IF @assetPathFilter IS NOT NULL AND LEN(@assetPathFilter) > 0
	BEGIN		
		DECLARE @assetsMatchingPath Base.tpIntList
		INSERT INTO @assetsMatchingPath
			SELECT id FROM @workingAssets WHERE Asset.GetAssetPath(id, '') LIKE '%' + @assetPathFilter + '%'

		DELETE @workingAssets
			FROM @workingAssets a
			LEFT OUTER JOIN @assetsMatchingPath b on a.id = b.id
			WHERE b.id IS NULL -- delete where match not found in join
	END

	-- finally, sort and paginate results
	INSERT INTO @filteredAssetIDs
		SELECT AssetID, DisplayOrder FROM Asset.ufnSortAndPageAssets(@workingAssets, NULL, @sortAscending,
		@sortByAssetDesc, @sortByAssetKeywords, @sortByAssetPath,
		@sortByAttributeTypeID, @pageNumber, @assetsPerPage)
		ORDER BY DisplayOrder
	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnAssetsFromGridSearch] TO [TEUser]
    AS [dbo];
GO