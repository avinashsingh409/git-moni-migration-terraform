﻿CREATE FUNCTION [Asset].[ufnGetAssetIdByGuidAndRelativePath]
(
	@startingAssetGuid UNIQUEIDENTIFIER,
	@assetPathRelativeToStartingAsset Base.tpIntStringPair READONLY,
	@securityUserID INT
)
RETURNS INT
AS
BEGIN
	DECLARE @searchTable TABLE (
		PathOrder INT PRIMARY KEY,
		AssetDesc VARCHAR(255) NOT NULL
	);
	INSERT INTO @searchTable
		SELECT Id, Label from @assetPathRelativeToStartingAsset;

	DECLARE @currentAssetID INT = NULL;
	SELECT TOP 1 @currentAssetID = AssetID
		FROM Asset.tAsset
		WHERE GlobalID = @startingAssetGuid
		AND Asset.ufnDoesUserHaveAccessToAsset(AssetID, @securityUserID, DEFAULT) = 1;

	IF @currentAssetID IS NULL
	BEGIN
		RETURN CAST('Could not find asset' AS INT);
	END
	
	DECLARE @whichDescendant INT;
	DECLARE @whichAssetDesc VARCHAR(255);
	DECLARE @parentAssetID INT;
	WHILE EXISTS (SELECT TOP 1 * FROM @searchTable)
	BEGIN
		-- get the next search items
		SELECT @whichDescendant = MIN(PathOrder) FROM @searchTable;
		SELECT @whichAssetDesc = AssetDesc FROM @searchTable WHERE PathOrder = @whichDescendant
		
		-- find the next child
		SET @parentAssetID = @currentAssetID;
		SET @currentAssetID = NULL;
		SELECT TOP 1 @currentAssetID = AssetID
			FROM Asset.tAsset
			WHERE ParentAssetID = @parentAssetID
			AND AssetDesc like @whichAssetDesc
			AND Asset.ufnDoesUserHaveAccessToAsset(AssetID, @securityUserID, DEFAULT) = 1;

		IF @currentAssetID IS NULL
		BEGIN
			RETURN CAST('Could not find asset' AS INT);
		END
		DELETE FROM @searchTable WHERE PathOrder = @whichDescendant;
	END

	RETURN @currentAssetID;
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnGetAssetIdByGuidAndRelativePath] TO [TEUser]
    AS [dbo];
GO