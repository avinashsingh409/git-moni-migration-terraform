﻿CREATE FUNCTION Asset.GetAssetPath
(
	@AssetID int,
	@Path varchar(4058)
)
RETURNS varchar(4058)
AS
BEGIN

declare @parentAssetID as int
declare @abbrev as varchar(255)
select @parentAssetID = ParentAssetID,@abbrev = AssetDesc from asset.tAsset where AssetID=@AssetID
if (@parentAssetID is not null)
  begin
  set @path = @abbrev + '\' + @path
  select @path = Asset.GetAssetPath(@parentAssetID,@Path)
  end

return @path

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[GetAssetPath] TO [TEUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[Asset].[GetAssetPath] TO [SIIFunctionReader]
    AS [dbo];

