﻿CREATE FUNCTION [Asset].[ufnGetAssetsTreeBranches]
( @assetIDs Base.tpIntList READONLY)
RETURNS TABLE AS
RETURN
(	 
	 SELECT DISTINCT ta.AssetID FROM asset.tAssetHop a 
	 INNER JOIN Asset.tAsset ta ON a.EndingAssetId = ta.AssetID AND ta.IsHidden = 0 
	 INNER JOIN @assetIDs aids on a.HopAssetId = aids.id
)
GO
GRANT SELECT
	ON OBJECT::[Asset].[ufnGetAssetsTreeBranches] TO [TEUser]
	AS [dbo];
GO