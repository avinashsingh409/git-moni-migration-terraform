﻿
CREATE FUNCTION [Asset].[ufnGetAllClientsForUser](
	 @userId int
)
RETURNS @clientsTable TABLE(
	ClientID int PRIMARY KEY not NULL,
	AssetAbbrev nvarchar(255) NOT NULL,
	AssetID int not NULL,
	ParentAssetID int NULL,
	GenerationLevel int NOT NULL
)
AS BEGIN

	 DECLARE @unsortedClients TABLE
	 (
		  ClientID int PRIMARY KEY not NULL,
		  AssetAbbrev nvarchar(255) NOT NULL,
		  AssetID int not NULL,
		  ParentAssetID int NULL
	 )

	 INSERT INTO @unsortedClients	(ClientID, AssetAbbrev, AssetID, ParentAssetID)
		  SELECT tc.ClientID, ta.AssetAbbrev, ta.AssetID, ta.ParentAssetID
		  FROM Asset.tClient tc
		  INNER JOIN Asset.tAsset ta ON tc.AssetID = ta.AssetID
		  WHERE ta.IsHidden = 0 and Asset.ufnDoesUserHaveAccessToAsset(tc.assetid,@userId,-1)=1	  		 

	 IF EXISTS (SELECT 1 FROM @unsortedClients uc WHERE uc.ParentAssetID is null) BEGIN
	 
		  ;WITH ancestors_CTE(ClientID, AssetAbbrev, AssetID, ParentAssetID, GenerationLevel )
		  AS
		  (
					 SELECT uc.ClientID, uc.AssetAbbrev, uc.AssetID, uc.ParentAssetId, 0 AS GenerationLevel
					 FROM @unsortedClients uc	  
					 WHERE uc.ParentAssetID IS NULL
					 UNION ALL 
					 SELECT uc.ClientID, uc.AssetAbbrev, uc.AssetID, uc.ParentAssetID, cte.GenerationLevel + 1
					 FROM @unsortedClients uc
					 INNER JOIN ancestors_CTE cte ON uc.ParentAssetID = cte.AssetID
		  )

		  INSERT INTO @clientsTable
		  (
				ClientID,
				AssetAbbrev,
				AssetID,
				ParentAssetID,
				GenerationLevel
		  )
		  SELECT 	     ClientID,
				AssetAbbrev,
				AssetID,
				ParentAssetID,
				GenerationLevel FROM ancestors_CTE ac
	 END ELSE BEGIN
		  INSERT INTO @clientsTable
		  (
				ClientID,
				AssetAbbrev,
				AssetID,
				ParentAssetID,
				GenerationLevel
		  )
		  SELECT uc.ClientID, uc.AssetAbbrev, uc.AssetID, uc.ParentAssetId, 0 AS GenerationLevel
		  FROM @unsortedClients uc	  
	 END

	 RETURN
END
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAllClientsForUser] TO [TEUser]
    AS [dbo];

