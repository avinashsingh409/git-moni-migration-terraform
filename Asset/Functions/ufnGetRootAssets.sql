﻿

CREATE FUNCTION [Asset].[ufnGetRootAssets] (
@SecurityUserID int,
@AppContextID int)
RETURNS @rootAssets TABLE 
(
	-- Add the column definitions for the TABLE variable here
	AssetID int
)
AS 
BEGIN

	--Get all of the roles for the user
	DECLARE @roles AS TABLE(SecurityRoleID INT NOT NULL, ParentSecurityRoleID INT);
	WITH roleInh(SecurityRoleID, ParentSecurityRoleID)
	AS (
		SELECT  r.SecurityRoleID, r.ParentSecurityRoleID FROM AccessControl.tRole r
		INNER JOIN AccessControl.tUserRoleMap m ON r.SecurityRoleID = m.SecurityRoleID
		WHERE m.SecurityUserID = @SecurityUserID
		UNION ALL 
		SELECT r2.SecurityRoleID, r2.ParentSecurityRoleID
		FROM AccessControl.tRole r2 
		INNER JOIN 
		roleInh h ON r2.SecurityRoleID = h.ParentSecurityRoleID
	)
	INSERT INTO @roles 
	SELECT roleInh.SecurityRoleID,roleInh.ParentSecurityRoleID FROM roleInh;

	--Find the allowed assets based on the roles
	DECLARE @parentAssetIDs as TABLE
	(
	  AssetID int NOT NULL
	)

	--Find all of the assets where you are given access to an asset, 
	--you have access to that asset, 
	--and you don't have access to the parent
	INSERT INTO @rootAssets 
	SELECT a.AssetID FROM AccessControl.tAssetAccessRule a 
	INNER JOIN @roles b on a.SecurityRoleID = b.SecurityRoleID
	INNER JOIN Asset.tAsset assets ON a.AssetID = assets.AssetID 
	WHERE 
	a.Allowed = 1 AND 
	assets.ParentAssetID IS NOT NULL AND 
	Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @SecurityUserID, @AppContextID) = 1 AND 
	Asset.ufnDoesUserHaveAccessToAsset(assets.ParentAssetID, @SecurityUserID, @AppContextID) = 0

	INSERT INTO @rootAssets(AssetID)
	SELECT a.AssetID FROM Asset.tAsset a
	WHERE ParentAssetID IS NULL AND Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @SecurityUserID, @AppContextID) = 1

	RETURN 
END 
GO
