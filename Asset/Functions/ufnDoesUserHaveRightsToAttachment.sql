﻿CREATE FUNCTION [Asset].[ufnDoesUserHaveRightsToAttachment] 
(	
	@securityUserID as int,
    @attachmentID as uniqueidentifier
)
RETURNS bit
AS
BEGIN

declare @result as bit = 1

declare @assetID as int = NULL

select @assetID = assetid from Asset.tAssetAttachment where AssetAttachmentID = @attachmentID

IF @assetID is NOT NULL AND Asset.ufnDoesUserHaveAccessToAsset(@assetID, @securityUserID, -1) = 1
   BEGIN
   IF EXISTS(SELECT * FROM Asset.tAssetAttachmentCategoryMap WHERE AssetAttachmentID = @attachmentID 
      AND CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap))
      BEGIN
	  -- Category is role controlled
	  IF EXISTS(SELECT * FROM Asset.tAssetAttachmentCategoryMap Where AssetAttachmentID = @attachmentID AND 
	     CategoryID NOT IN (SELECT CategoryID 
	                 FROM AccessControl.tRoleCategoryMap a JOIN AccessControl.tUserRoleMap b on 
					 a.SecurityRoleID = b.SecurityRoleID where b.SecurityUserID = @securityUserID))
		 BEGIN
		 SET @result = 0
		 END
	  END
   END
ELSE
  BEGIN
  SET @result = 1
  END

RETURN @result
END

GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnDoesUserHaveRightsToAttachment]  TO [TEUser]
    AS [dbo];
GO
