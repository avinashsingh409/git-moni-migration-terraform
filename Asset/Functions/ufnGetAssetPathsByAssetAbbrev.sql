﻿CREATE FUNCTION [Asset].[ufnGetAssetPathsByAssetAbbrev]
(
	@assetIds Base.tpIntList READONLY,
	@securityUserId INT = -1
)
RETURNS TABLE AS RETURN
(
	--Return asset path using AssetAbbrev from the starting asset up to the top level asset accessible by the user.
	--Leaving SecurityUserID defaulted to -1 will bypass security access check and return all paths up to the top asset.
	WITH AssetTree AS
	(
	SELECT a.AssetID as StartingAssetID, a.AssetID, a.ParentAssetID, CONVERT(NVARCHAR(1000), a.AssetAbbrev) as AssetPath
	FROM Asset.tAsset a INNER JOIN @assetIds s on a.AssetID = s.id
	WHERE Asset.ufnDoesUserHaveAccessToAsset(a.AssetID,@securityUserId,DEFAULT) = 1

	UNION ALL

	SELECT t.StartingAssetID, a.AssetID, a.ParentAssetID, CONVERT(NVARCHAR(1000), a.AssetAbbrev + '/' + t.AssetPath) as AssetPath
	FROM Asset.tAsset a
	INNER JOIN AssetTree t ON a.AssetID = t.ParentAssetID
	WHERE Asset.ufnDoesUserHaveAccessToAsset(a.AssetID,@securityUserId,DEFAULT) = 1
	)
	SELECT grp.AssetID, grp.AssetPath FROM 
	( SELECT StartingAssetID AS AssetID, 
			 AssetPath, 
			 ROW_NUMBER() OVER (PARTITION BY StartingAssetID ORDER BY LEN(AssetPath) DESC) RowNum
	  FROM AssetTree
	) grp
	WHERE grp.RowNum = 1
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetPathsByAssetAbbrev] TO [TEUser]
    AS [dbo];
GO
