﻿CREATE FUNCTION [Asset].[ufnGetAssetsForIA]
(
	 @assetID int,
	 @assetTypeID int
)
RETURNS @assetIdTable TABLE
(
  ProjID int PRIMARY KEY not NULL,
  LabelID int not NULL,
  Label nvarchar(255) not NULL
)
AS
BEGIN

IF (@assetTypeID = 2)
  BEGIN
	--ianode level
	WITH recurse (AssetID) AS
	(
		SELECT ianode.AssetID
			FROM Asset.tAsset ianode
			INNER JOIN Asset.tAssetClassType t ON ianode.AssetClassTypeID = t.AssetClassTypeID AND t.AssetClassTypeKey = 'InvestAccelerator'
			WHERE ianode.AssetID = @assetID
		UNION ALL SELECT child.assetid
			FROM Asset.tAsset child
			INNER JOIN recurse parent ON parent.AssetID = child.ParentAssetID
	)
	INSERT INTO @assetIdTable
	SELECT a.AssetID,parent.AssetID,parent.AssetDesc FROM Asset.tAsset a
    INNER JOIN Asset.tAsset parent ON parent.AssetID = a.ParentAssetID
    INNER JOIN recurse r on r.AssetID = a.AssetID
    INNER JOIN Asset.tAssetClassType c ON a.AssetClassTypeID = c.AssetClassTypeID
    WHERE AssetTypeID = 5
  END
ELSE IF (@assetTypeID = 3) --strategy level
  BEGIN
	WITH recurse (AssetID) AS
	(
		SELECT strat.AssetID
			FROM Asset.tAsset strat
			INNER JOIN Asset.tAsset ianode ON ianode.AssetID = strat.ParentAssetID AND strat.AssetID = @assetID
			INNER JOIN Asset.tAssetClassType t ON ianode.AssetClassTypeID = t.AssetClassTypeID AND t.AssetClassTypeKey = 'InvestAccelerator'
		UNION ALL SELECT child.assetid
			FROM Asset.tAsset child
			INNER JOIN recurse parent ON parent.AssetID = child.ParentAssetID
	)
	INSERT INTO @assetIdTable
	SELECT a.AssetID,parent.AssetID,parent.AssetDesc FROM Asset.tAsset a
    INNER JOIN Asset.tAsset parent ON parent.AssetID = a.ParentAssetID
    INNER JOIN recurse r on r.AssetID = a.AssetID
    INNER JOIN Asset.tAssetClassType c ON a.AssetClassTypeID = c.AssetClassTypeID
    WHERE AssetTypeID = 5
  END
ELSE IF (@assetTypeID = 4)  --option level get all projects below the option, proj is label
  BEGIN
	INSERT INTO @assetIdTable
	  SELECT a.AssetID,a.AssetID,a.AssetDesc FROM Asset.tAsset a
	  INNER JOIN Asset.tAssetClassType c ON a.AssetClassTypeID = c.AssetClassTypeID
	  WHERE c.AssetTypeID = 5 AND a.ParentAssetID = @assetID
  END
ELSE IF (@assetTypeID >= 5)  --project level, get the single proj only
  BEGIN
    if exists(select * from Asset.tAsset a where ParentAssetID = @assetID)
	  begin
	  INSERT INTO @assetIdTable
	  SELECT a.AssetID,a.AssetID,a.AssetDesc FROM Asset.tAsset a where ParentAssetID = @assetID
	  end
	else
	  begin
	  INSERT INTO @assetIdTable SELECT AssetID,AssetID,AssetDesc FROM Asset.tAsset WHERE AssetID = @assetid
	  end
  END
RETURN
END

GO


