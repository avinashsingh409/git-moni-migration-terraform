﻿CREATE FUNCTION [Asset].[ufnWhichCategoriesAreProhibited]
(
	@checkAssetID as int,
	@securityUserID as int,
	@categoryCandidates as Base.tpStringList READONLY,
	@categoryTypeID as int
)
RETURNS @result TABLE
(
   CategoryName varchar(255)
)

AS

BEGIN

DECLARE @temp as TABLE
(
  CategoryName varchar(255),  
  Prohibited bit
)

INSERT INTO @temp (CategoryName,Prohibited) select DISTINCT [Value],0 from @categoryCandidates a

declare @clientIDs as base.tpIntList
insert into @clientIDs select ClientID FROM Asset.ufnGetClientsForAsset(@checkAssetID)

UPDATE @temp SET Prohibited = 1 WHERE CategoryName in (SELECT a.Name FROM Asset.tCategory a JOIN AccessControl.tRoleCategoryMap b on a.CategoryID = b.CategoryID 
      JOIN @clientIDs c ON a.ClientID = c.ID
      WHERE a.Name = CategoryName AND b.SecurityRoleID NOT IN
	   (SELECT SecurityRoleID FROM AccessControl.tUserRoleMap WHERE SecurityUserID = @securityUserID ))

UPDATE @temp SET Prohibited = 1 WHERE CategoryName in (SELECT a.Name FROM Asset.tCategory a JOIN AccessControl.tRoleCategoryMap b on a.CategoryID = b.CategoryID 
      AND a.ClientID IS NULL
      WHERE a.Name = CategoryName AND b.SecurityRoleID NOT IN
	   (SELECT SecurityRoleID FROM AccessControl.tUserRoleMap WHERE SecurityUserID = @securityUserID ))


INSERT INTO @result SELECT CategoryName from @temp where Prohibited = 1


RETURN

END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnWhichCategoriesAreProhibited] TO [TEUser]
    AS [dbo];
GO