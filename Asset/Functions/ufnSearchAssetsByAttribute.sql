﻿CREATE FUNCTION [Asset].[ufnSearchAssetsByAttribute]
(
       @securityUserId int,
       @fromAssetIDs Base.tpIntList READONLY,
       @attribName nvarchar(255),
       @attribValues Base.tpStringList READONLY,
       @wildcardSearch bit
)
RETURNS @assetIds TABLE 
(
       AssetId int
)
AS
BEGIN
       DECLARE @attributeTypeIDs as Base.tpIntList
       INSERT INTO @attributeTypeIDs
         SELECT DISTINCT t.AttributeTypeID
              FROM @fromAssetIDs a
              JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
              JOIN Asset.tAttributeType t on t.AttributeTypeID = aa.AttributeTypeID
              WHERE t.AttributeTypeDesc = @attribName or t.AttributeTypeKey = @attribName

       IF EXISTS(SELECT * FROM @attributeTypeIDs) -- there is at least 1 attribute matching for these assets
       BEGIN
              DECLARE @searchCriteriaByAttributeTypeID Base.tpIntStringPair
              INSERT INTO @searchCriteriaByAttributeTypeID
                      SELECT a.ID, v.[value]
                      FROM @attributeTypeIDs a, @attribValues v
              DECLARE @attributesWhichNeedToMatchExactly Base.tpIntList
              IF @wildcardSearch = 0 -- When 0, attribute value must be an exact match; no wildcards
              BEGIN
                      INSERT INTO @attributesWhichNeedToMatchExactly select * from @attributeTypeIDs
              END
              INSERT INTO @assetIds
                      SELECT * FROM Asset.ufnSearchAssetsByAttributes(@securityUserId, @fromAssetIDs, @searchCriteriaByAttributeTypeID, @attributesWhichNeedToMatchExactly)
       END
       -- no ELSE required. No data found. return empty set
       RETURN 
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSearchAssetsByAttribute] TO [TEUser]
    AS [dbo];
GO