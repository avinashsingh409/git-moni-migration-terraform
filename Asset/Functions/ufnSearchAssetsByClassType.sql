﻿CREATE FUNCTION [Asset].[ufnSearchAssetsByClassType]
(
	@securityUserID INT,
	@fromAssetIDs Base.tpIntList READONLY,
	@assetClassTypes Base.tpStringList READONLY,
	@wildcardSearch bit
)
RETURNS
@AssetIds TABLE 
(
	AssetId int
)
AS
BEGIN
	 DECLARE @accessibleAssetIds Base.tpIntList
	 IF @securityUserID IS NULL
	 BEGIN
		INSERT INTO @accessibleAssetIds
			SELECT * FROM @fromAssetIDs
	 END
	 ELSE
	 BEGIN
		INSERT INTO @accessibleAssetIds
			SELECT AssetID FROM Asset.ufnUserHasAccessToAssets(@securityUserID, @fromAssetIDs)
	END

	DECLARE @searchTerms Base.tpStringList
	INSERT INTO @searchTerms
		SELECT CASE WHEN @wildcardSearch = 1 THEN '%' + a.[value] + '%' ELSE a.[value] END
		FROM @assetClassTypes a
		WHERE RTRIM(ISNULL(a.[value], '')) <> ''

	DECLARE @matchedAssetsNotDistinct TABLE (AssetID INT)
	IF @wildcardSearch = 1
	BEGIN
		INSERT INTO @matchedAssetsNotDistinct
			SELECT a.id
			FROM @accessibleAssetIds a
			JOIN Asset.tAsset b on b.AssetID = a.id
			JOIN Asset.tAssetClassType c on c.AssetClassTypeID = b.AssetClassTypeID
			JOIN @searchTerms d on c.AssetClassTypeDesc like d.[value]-- like
	END
	ELSE
	BEGIN
		INSERT INTO @matchedAssetsNotDistinct
			SELECT a.id
			FROM @accessibleAssetIds a
			JOIN Asset.tAsset b on b.AssetID = a.id
			JOIN Asset.tAssetClassType c on c.AssetClassTypeID = b.AssetClassTypeID
			JOIN @searchTerms d on d.[value] = c.AssetClassTypeDesc -- strict equals
	END

	INSERT INTO @AssetIds
		SELECT DISTINCT AssetID FROM @matchedAssetsNotDistinct
	RETURN 
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSearchAssetsByClassType] TO [TEUser]
    AS [dbo];
GO