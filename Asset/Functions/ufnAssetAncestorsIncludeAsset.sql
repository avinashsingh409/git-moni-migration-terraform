﻿CREATE FUNCTION [Asset].[ufnAssetAncestorsIncludeAsset] 
(	
	@assetID int
)
RETURNS TABLE
AS
RETURN
(
	--ufnAssetAncestors never returns @assetID in its results.
	--So we can use UNION ALL to union in @assetID without 
	--triggering the (useless) ensure-uniqueness logic of plain UNION.
	SELECT AssetID FROM Asset.ufnAssetAncestors(@assetID ) 	
	UNION ALL Select @assetID as AssetID 
)
GO
GRANT SELECT
	ON OBJECT::[Asset].[ufnAssetAncestorsIncludeAsset] TO [TEUser]
	AS [dbo];