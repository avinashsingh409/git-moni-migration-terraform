﻿CREATE FUNCTION [Asset].[ufnGetEventOccurrencesForAssets]
(
	@assetIds Base.tpIntList READONLY,
	@eventTypes Base.tpIntList READONLY,
	@attributeTypeIds Base.tpIntList READONLY,
	@startDate DATETIME,  
	@endDate DATETIME,
	@maxOccurences INT = 15, 
	@sortOrder BIT 
)
RETURNS TABLE AS
RETURN
(
	WITH results AS
	(
		--Select @maxOccurences of events for the list of assets, event types, and attribute type supplied
		--@sortOrder will affect which events are returned if there are more events than @maxOccurences
		SELECT 
		ROW_NUMBER()
			OVER(PARTITION BY a.EventOwnerAssetID, a.EventAssetClassTypeID
			ORDER BY CASE WHEN @sortOrder = 1 THEN c.Attribute_Date END ASC, CASE WHEN @sortOrder <> 1 THEN c.Attribute_Date END DESC) As RowNum,
		a.*, c.AttributeTypeID, d.AttributeTypeDesc, c.Attribute_Date, c.Value as EventAttributeValue
		FROM Asset.vAssetEvent a
		JOIN @assetIds b ON a.EventOwnerAssetID = b.id 
		JOIN Asset.vAssetAttribute c ON a.EventAssetID = c.AssetID
		JOIN Asset.tAttributeType d ON c.AttributeTypeID = d.AttributeTypeID
		JOIN @eventTypes e ON a.EventAssetClassTypeID = e.id
		JOIN @attributeTypeIds f on c.AttributeTypeID = f.id
		AND 
		((d.DisplayFormat='date'
		AND c.Attribute_date BETWEEN ISNULL(@startDate, CAST('01/01/1753 00:00:00.000' AS DATETIME)) 
		AND ISNULL(@endDate, CAST('12/31/9999 23:59:59.997' AS DATETIME)))
		OR 
		(
		 d.DisplayFormat<>'date'
		))

	)
	SELECT
	RowNum, EventOwnerAssetID, EventGUID, EventAssetID, EventAssetClassTypeID, EventTypeDesc, AttributeTypeID, AttributeTypeDesc, Attribute_Date, EventAttributeValue
	FROM results
	WHERE RowNum <= @maxOccurences
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetEventOccurrencesForAssets] TO [TEUser]
    AS [dbo];

GO
