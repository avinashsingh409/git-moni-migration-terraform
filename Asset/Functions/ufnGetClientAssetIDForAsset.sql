﻿CREATE FUNCTION Asset.ufnGetClientAssetIDForAsset
(		
	@AssetID int
)
RETURNS int
AS
BEGIN
  DECLARE @clientAssetID as int = NULL
  
  SELECT TOP 1 @clientAssetID = AssetID FROM Asset.ufnGetClientsForAsset(@AssetID) ORDER BY AncestryGenerationsAway

  RETURN @clientAssetID
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnGetClientAssetIDForAsset]  TO [TEUser]
    AS [dbo];
GO