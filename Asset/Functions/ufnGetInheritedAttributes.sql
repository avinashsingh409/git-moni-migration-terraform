﻿CREATE FUNCTION [Asset].[ufnGetInheritedAttributes]
(
	@Assets Base.tpIntList readonly,
	@AttributeTypeID int,
	@UserId int,
	@StopAtLevel int = null,
	@AppContextID int = -1,
	@FallBackOptionTypeID int = null,
	@FallBackValueInt int = null,
	@FallBackValueString nvarchar(max) = null,
	@FallBackValueFloat real = null,
	@FallBackAllEmpties bit = 0,
	@RequireNonEmptyOptionTypeID bit = 0,
	@RequireNonEmptyInt bit = 0,
	@RequireNonEmptyString bit = 0,
	@RequireNonEmptyFloat bit = 0
)
returns @AttributeInstances table
(
	AssetID int not null,						-- core
	AttributeOptionTypeID int null,
	Attribute_int int null,
	Attribute_string nvarchar(max) null,
	Attribute_float real null,
	Attribute_date DATETIMEOFFSET NULL,

	AssetAttributeID int not null,				-- other tAssetAttribute
	AttributeTypeID int not null,
	Favorite bit not null,
	DisplayOrder int not null,
	CreatedByUserID int null,
	ChangedByUserID int null,
	CreateDate datetime null,
	ChangeDate datetime null,
	CreatedBy nvarchar(50) null,
	ChangedBy nvarchar(50) null,

	AttributeTypeDesc nvarchar(255) not null,	-- tAttributeType
	AttributeTypeKey nvarchar(100) not null,
	EngrUnits nvarchar(100) null,
	DisplayFormat nvarchar(100) not null,
	IsStandard bit not null,
	IsExempt bit not null,
	ClientID int null,
	primary key clustered
	(
		AssetID asc
	)
)
as
begin

	-- This function returns attribute values of the requested type for the provided set of assets to which the user has access.
	-- It starts looking for attribute instances associated with the provided assets.
	-- If an asset lacks an instance, its ancestry is reviewed for instances, with priority
	-- given to the nearest ancestors. If no instances are found in the ancestry, fallback values are returned,
	-- regardless of the @FallBackAllEmpties value.
	-- If the @FallBackAllEmpties flag is set to true, fallback values will be returned for empty instance values.
	-- Ancestry can be constrained by the @StopAtLevel parameter. Instance selection can be controlled by the @RequireNonEmpty... parameters.
	-- When a @RequireNonEmpty... flag is set to true instances with null values will be ignored. In the case of @RequireNonEmptyString,
	-- empty Attribute_string values '' are also ignored. In the case of @RequireNonEmptyOptionTypeID, instances with empty related AttributeTypeDesc
	-- values are also ignored.

	declare @myAssets Base.tpIntList;
	insert into @myAssets (id) select id from @Assets;

	declare @targetCount int = (select count (id) from @myAssets);

	-- this will be populated and used prior to returning
	declare @securityAssets Base.tpIntList;


	declare @attributeTypeDesc nvarchar(255);
	declare @attributeTypeKey nvarchar(100);
	declare @engrUnits nvarchar(100);
	declare @displayFormat nvarchar(100);
	declare @isStandard bit;
	declare @isExempt bit;
	declare @clientID int;

	-- get tAttributeType values
	select
		@attributeTypeDesc = AttributeTypeDesc,
		@attributeTypeKey = AttributeTypeKey,
		@engrUnits = EngrUnits,
		@displayFormat = DisplayFormat,
		@isStandard = IsStandard,
		@isExempt = IsExempt,
		@clientID = ClientID
	from Asset.tAttributeType where AttributeTypeID = @AttributeTypeID;

	-- start with assets that have this attribute, ensuring the user has access to the asset's attribute instance
	insert into @AttributeInstances (AssetID, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float,Attribute_date,					-- core

									AssetAttributeID, AttributeTypeID, Favorite, DisplayOrder, CreatedByUserID,						-- other tAssetAttribute
									ChangedByUserID, CreateDate, ChangeDate, CreatedBy, ChangedBy,

									AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)	--	tAttributeType
	select
		a.id, 
		case when @FallBackAllEmpties = 0 then t.AttributeOptionTypeID else case when t.AttributeOptionTypeID is not null and opt.AttributeOptionTypeDesc <> '' then t.AttributeOptionTypeID else @FallBackOptionTypeID end end,
		case when @FallBackAllEmpties = 0 then t.Attribute_int else coalesce (t.Attribute_int, @FallBackValueInt) end,
		case when @FallBackAllEmpties = 0 then t.Attribute_string else case when t.Attribute_string is not null and t.Attribute_string <> '' then t.Attribute_string else @FallBackValueString end end,
		case when @FallBackAllEmpties = 0 then t.Attribute_float else coalesce (t.Attribute_float, @FallBackValueFloat) end,			-- core
		CASE WHEN @FallBackAllEmpties = 0 THEN t.Attribute_date ELSE COALESCE(t.Attribute_date, NULL) END,

		t.AssetAttributeID, @AttributeTypeID, t.Favorite, t.DisplayOrder, t.CreatedByUserID,											-- other tAssetAttribute
		t.ChangedByUserID, t.CreateDate, t.ChangeDate, createdBy.Username, changedBy.Username,

		@attributeTypeDesc, @attributeTypeKey, @engrUnits, @displayFormat, @isStandard, @isExempt, @clientID							-- tAttributeType
	from @myAssets a join Asset.tAssetAttribute t on a.id = t.AssetID
	left join AccessControl.tUser createdBy on t.CreatedByUserID = createdBy.SecurityUserID
	left join AccessControl.tUser changedBy on t.ChangedByUserID = changedBy.SecurityUserID
	left join Asset.tAttributeOptionType opt on t.AttributeOptionTypeID = opt.AttributeOptionTypeID
	where
		t.AttributeTypeID = @AttributeTypeID
		and (select isValid from Asset.ufnAttributeIsValidForUser(@UserId, a.id, @AttributeTypeID)) = 1
		and case when @RequireNonEmptyOptionTypeID = 0 then 1 else case when t.AttributeOptionTypeID is not null and opt.AttributeOptionTypeDesc <> '' then 1 else 0 end end = 1
		and case when @RequireNonEmptyInt = 0 then 1 else case when t.Attribute_int is not null then 1 else 0 end end = 1
		and case when @RequireNonEmptyString = 0 then 1 else case when t.Attribute_string is not null and t.Attribute_string <> '' then 1 else 0 end end = 1
		and case when @RequireNonEmptyFloat = 0 then 1 else case when t.Attribute_float is not null then 1 else 0 end end = 1
	;

	-- return if we've found attribute instances for all assets
	if (select count (AssetID) from @AttributeInstances) = @targetCount
	begin
		-- asset access security
		insert into @securityAssets (id) select AssetID from @AttributeInstances except select id from @securityAssets;
		delete from @AttributeInstances where AssetID not in (select AssetID from Asset.ufnUserHasAccessToAssets(@UserId, @securityAssets));
		return;
	end

	-- remove assets from @myAssets that already have instances
	delete from @myAssets where id in (select AssetID from @AttributeInstances);  -- every AssetID in @AttributeInstances is going to be in @myAssets

	-- now get ancestry for the assets, ensuring the user has access to the ancestor's attribute instance
	declare @Ancestry table (
		AssetID int not null,						-- core
		AncestorID int not null,
		GenerationsAway int not null,
		AttributeOptionTypeID int null,
		Attribute_int int null,
		Attribute_string nvarchar(max) null,
		Attribute_float real null,
		Attribute_date DATETIMEOFFSET NULL,

		AssetAttributeID int not null,				-- other tAssetAttribute
		AttributeTypeID int not null,
		Favorite bit not null,
		DisplayOrder int not null,
		CreatedByUserID int null,
		ChangedByUserID int null,
		CreateDate datetime null,
		ChangeDate datetime null,
		CreatedBy nvarchar(50) null,
		ChangedBy nvarchar(50) null,

		AttributeTypeDesc nvarchar(255) not null,	-- tAttributeType
		AttributeTypeKey nvarchar(100) not null,
		EngrUnits nvarchar(100) null,
		DisplayFormat nvarchar(100) not null,
		IsStandard bit not null,
		IsExempt bit not null,
		ClientID int null
	);

	insert into @Ancestry (AssetID, AncestorID, GenerationsAway, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float, Attribute_date,	-- core

							AssetAttributeID, AttributeTypeID, Favorite, DisplayOrder, CreatedByUserID,										-- other tAssetAttribute
							ChangedByUserID, CreateDate, ChangeDate, CreatedBy, ChangedBy,
						
							AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)					-- tAttributeType
	select
		a.id,
		b.HopAssetId as AssetID,
		-b.AncestryLevel as AncestryGenerationsAway,
		case when @FallBackAllEmpties = 0 then t.AttributeOptionTypeID else case when t.AttributeOptionTypeID is not null and opt.AttributeOptionTypeDesc <> '' then t.AttributeOptionTypeID else @FallBackOptionTypeID end end,
		case when @FallBackAllEmpties = 0 then t.Attribute_int else coalesce (t.Attribute_int, @FallBackValueInt) end,
		case when @FallBackAllEmpties = 0 then t.Attribute_string else case when t.Attribute_string is not null and t.Attribute_string <> '' then t.Attribute_string else @FallBackVAlueString end end,
		case when @FallBackAllEmpties = 0 then t.Attribute_float else coalesce (t.Attribute_float, @FallBackValueFloat) end,				-- core
		CASE WHEN @FallBackAllEmpties = 0 THEN t.Attribute_date ELSE COALESCE(t.Attribute_date,NULL) END,
	
		t.AssetAttributeID, @AttributeTypeID, t.Favorite, t.DisplayOrder, t.CreatedByUserID,												-- other tAssetAttribute
		t.ChangedByUserID, t.CreateDate, t.ChangeDate, createdBy.Username, changedBy.Username,

		@attributeTypeDesc, @attributeTypeKey, @engrUnits, @displayFormat, @isStandard, @isExempt, @clientID								-- tAttributeType
	from @myAssets a
	join Asset.tAssetHop b on a.id = b.EndingAssetId
	join Asset.tAsset anc on b.HopAssetId = anc.AssetID
	join Asset.tAssetClassType ac on anc.AssetClassTypeID = ac.AssetClassTypeID
	join Asset.tAssetAttribute t on b.HopAssetId = t.AssetID
	left join Asset.tAttributeOptionType opt on t.AttributeOptionTypeID = opt.AttributeOptionTypeID
	left join AccessControl.tUser createdBy on t.CreatedByUserID = createdBy.SecurityUserID
	left join AccessControl.tUser changedBy on t.ChangedByUserID = changedBy.SecurityUserID
	where
		(@StopAtLevel is null or ac.AssetTypeID <= @StopAtLevel)
		and t.AttributeTypeID = @AttributeTypeID
		and (select isValid from Asset.ufnAttributeIsValidForUser(@UserId, anc.AssetID, @AttributeTypeID)) = 1
		and case when @RequireNonEmptyOptionTypeID = 0 then 1 else case when t.AttributeOptionTypeID is not null and opt.AttributeOptionTypeDesc <> '' then 1 else 0 end end = 1
		and case when @RequireNonEmptyInt = 0 then 1 else case when t.Attribute_int is not null then 1 else 0 end end = 1
		and case when @RequireNonEmptyString = 0 then 1 else case when t.Attribute_string is not null and t.Attribute_string <> '' then 1 else 0 end end = 1
		and case when @RequireNonEmptyFloat = 0 then 1 else case when t.Attribute_float is not null then 1 else 0 end end = 1
	;

	-- at this point we have our ancestry dataset.
	-- now it's a matter of grabbing the nearest instance.
	declare @mostGenerationsAway int = (select max (GenerationsAway) from @Ancestry);
	declare @thisGenerationAway int = 1;
	while @thisGenerationAway <= @mostGenerationsAway
	begin
		insert into @AttributeInstances (AssetID, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float,Attribute_date,					-- core

										AssetAttributeID, AttributeTypeID, Favorite, DisplayOrder, CreatedByUserID,							-- other tAssetAttribute
										ChangedByUserID, CreateDate, ChangeDate, CreatedBy, ChangedBy,
									
										AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)		-- tAttributreType
		select a.id, anc.AttributeOptionTypeID, anc.Attribute_int, anc.Attribute_string, anc.Attribute_float,anc.Attribute_date,								-- core

				anc.AssetAttributeID, @AttributeTypeID, anc.Favorite, anc.DisplayOrder, anc.CreatedByUserID,								-- other tAssetAttribute
				anc.ChangedByUserID, anc.CreateDate, anc.ChangeDate, anc.CreatedBy, anc.ChangedBy,

				@attributeTypeDesc, @attributeTypeKey, @engrUnits, @displayFormat, @isStandard, @isExempt, @clientID						-- tAttributeType
		from @myAssets a join @Ancestry anc on a.id = anc.AssetID
		where anc.GenerationsAway = @thisGenerationAway
		;

		-- return if we can
		if (select count (AssetID) from @AttributeInstances) = @targetCount
		begin
			-- asset access security
			insert into @securityAssets (id) select AssetID from @AttributeInstances except select id from @securityAssets;
			delete from @AttributeInstances where AssetID not in (select AssetID from Asset.ufnUserHasAccessToAssets(@UserId, @securityAssets));
			return;
		end

		delete from @myAssets where id in (select i.AssetID from @AttributeInstances i
											join @myAssets a on i.AssetID = a.id join @Ancestry anc on i.AssetID = anc.AssetID
											where anc.GenerationsAway = @thisGenerationAway) -- only attempt to delete those AssetIDs that were in this generation's @Ancestry
		;
		set @thisGenerationAway = @thisGenerationAway + 1;
	end

	-- for anything left over, use the fallbacks
	insert into @AttributeInstances (AssetID, AttributeOptionTypeID, Attribute_int, Attribute_string, Attribute_float,Attribute_date,					-- core
										AssetAttributeID, AttributeTypeID, Favorite, DisplayOrder, CreatedByUserID,						-- other tAssetAttribute
										ChangedByUserID, CreateDate, ChangeDate, CreatedBy, ChangedBy,
										AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)	-- tAttributeType
	select a.id, @FallBackOptionTypeID, @FallBackValueInt, @FallBackValueString, @FallBackValueFloat,NULL,									-- core
			-1, -1, 0, -1, null, null, null, null, null, null,																			-- other tAssetAttribute
			@attributeTypeDesc, @attributeTypeKey, @engrUnits, @displayFormat, @isStandard, @isExempt, @clientID						-- tAttributeType
	from @myAssets a
	;

	-- asset access security
	insert into @securityAssets (id) select AssetID from @AttributeInstances except select id from @securityAssets;
	delete from @AttributeInstances where AssetID not in (select AssetID from Asset.ufnUserHasAccessToAssets(@UserId, @securityAssets));

	return;
end
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetInheritedAttributes] TO [TEUser]
    AS [dbo];

