﻿CREATE FUNCTION [Asset].[ufnAssetAncestorsWithLevel] 
(
	-- Add the parameters for the stored procedure here
	@assetID int
)
RETURNS TABLE
AS
return
(
	SELECT ah.HopAssetId AssetID, (ah.AncestryLevel * -1) AncestryGenerationsAway
	FROM Asset.tAssetHop ah
	INNER JOIN Asset.tAsset ta ON ah.HopAssetId = ta.AssetID AND ta.IsHidden = 0
	WHERE AncestryLevel < 0 AND ah.EndingAssetId = @assetID
)
GO
GRANT SELECT
	ON OBJECT::[Asset].[ufnAssetAncestorsWithLevel] TO [TEUser]
	AS [dbo];