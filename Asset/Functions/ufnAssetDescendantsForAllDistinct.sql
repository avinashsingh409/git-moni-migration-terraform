﻿CREATE FUNCTION [Asset].[ufnAssetDescendantsForAllDistinct] 
(
	@securityUserId INT,
	@assetIds base.tpIntList READONLY
)
RETURNS TABLE
AS
RETURN
(
	SELECT DISTINCT hop.EndingAssetId AS AssetID
	FROM @assetIds a
	JOIN Asset.tAssetHop hop ON a.id = hop.HopAssetId
	JOIN Asset.ufnGetAssetIdsForUser(@securityUserId) s ON hop.EndingAssetId = s.AssetID
	WHERE hop.EndingAssetId <> a.id
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnAssetDescendantsForAllDistinct] TO [TEUser]
    AS [dbo];