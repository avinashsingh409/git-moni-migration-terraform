﻿----------------------------------------------------------------
CREATE FUNCTION [Asset].[ufnGetCategoriesMappedToAssetCustomAttribute]
(	
	@AssetID int,
	@Name varchar(255)
)
RETURNS @categories TABLE
(
	CategoryID int PRIMARY KEY not NULL,
	[Name] varchar(255)
)
AS
BEGIN
    -- First find global categories
	INSERT INTO @categories (CategoryID,[Name])
	  SELECT DISTINCT cat.CategoryID,cat.[Name] 
	  FROM Asset.tAssetAttributeCategoryMap map  
	  JOIN Asset.tAttributeType t ON t.AttributeTypeID = map.AttributeTypeID AND t.AttributeTypeDesc = @Name
	  JOIN Asset.tCategory cat ON map.CategoryID = cat.CategoryID and cat.ClientID IS NULL 
	  JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID AND d.CategoryTypeID = 1
	  
    INSERT INTO @categories (CategoryID,[Name])
	  SELECT DISTINCT cat.CategoryID,cat.[Name] 
	  FROM Asset.ufnGetClientsForAsset(@assetid) b 
	  JOIN Asset.tCategory c on b.ClientID = c.ClientID
	  JOIN Asset.tAssetAttributeCategoryMap map ON c.CategoryID = map.CategoryID 
	  JOIN Asset.tAttributeType t ON t.AttributeTypeID = map.AttributeTypeID AND t.AttributeTypeDesc = @Name
	  JOIN Asset.tCategory cat ON map.CategoryID = cat.CategoryID and b.ClientID = cat.ClientID 
	  JOIN Asset.tCategoryTypeMap d ON map.CategoryID = d.CategoryID AND d.CategoryTypeID = 1 
	RETURN
END


GO


GRANT SELECT
    ON OBJECT::[Asset].[ufnGetCategoriesMappedToAssetCustomAttribute] TO [TEUser]
    AS [dbo];
GO
