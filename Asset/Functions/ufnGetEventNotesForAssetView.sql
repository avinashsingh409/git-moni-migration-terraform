﻿CREATE FUNCTION [Asset].[ufnGetEventNotesForAssetView](
	 @assetIds Base.tpIntList READONLY,
	 @eventTypes Base.tpIntList READONLY,
	 @startDate DATETIME,
	 @endDate DATETIME
)
RETURNS TABLE AS 
RETURN
(
	-- Get most recent note (attribute type 209) that isn't null for list of assets and event types across attribute types 205, 206, 207, and 208 
	WITH results AS 
	(
		SELECT
		ROW_NUMBER()
			OVER(PARTITION BY r.EventOwnerAssetID, r.EventAssetClassTypeID 
			ORDER BY r.EventOwnerAssetID, r.EventTypeDesc, r.Attribute_date DESC) AS RowNum,
			r.EventOwnerAssetID, r.EventAssetClassTypeID, r.EventTypeDesc, r.Attribute_date, r.Attribute_string
		FROM
		(
			SELECT a.EventOwnerAssetID, a.EventAssetClassTypeID, a.EventTypeDesc, t.AttributeTypeID, t.Attribute_date, note.Attribute_string
			FROM Asset.vAssetEvent a
			JOIN @assetIds b ON a.EventOwnerAssetID = b.id
			JOIN @eventTypes c ON a.EventAssetClassTypeID = c.id
			LEFT JOIN Asset.tAssetAttribute t ON a.EventAssetID = t.AssetID AND t.AttributeTypeID = 205
			LEFT JOIN Asset.tAssetAttribute note ON a.EventAssetID = note.AssetID AND note.AttributeTypeID = 209
			WHERE t.Attribute_date BETWEEN ISNULL(@startDate, CAST('01/01/1753 00:00:00.000' AS DATETIME)) 
			AND ISNULL(@endDate, CAST('12/31/9999 23:59:59.997' AS DATETIME))
			AND COALESCE(note.Attribute_string, '') <> ''

			UNION ALL

			SELECT a.EventOwnerAssetID, a.EventAssetClassTypeID, a.EventTypeDesc, t.AttributeTypeID, t.Attribute_date, note.Attribute_string
			FROM Asset.vAssetEvent a
			JOIN @assetIds b ON a.EventOwnerAssetID = b.id
			JOIN @eventTypes c ON a.EventAssetClassTypeID = c.id
			LEFT JOIN Asset.tAssetAttribute t ON a.EventAssetID = t.AssetID AND t.AttributeTypeID = 206
			LEFT JOIN Asset.tAssetAttribute note ON a.EventAssetID = note.AssetID AND note.AttributeTypeID = 209
			WHERE t.Attribute_date BETWEEN ISNULL(@startDate, CAST('01/01/1753 00:00:00.000' AS DATETIME)) 
			AND ISNULL(@endDate, CAST('12/31/9999 23:59:59.997' AS DATETIME))
			AND COALESCE(note.Attribute_string, '') <> ''

			UNION ALL

			SELECT a.EventOwnerAssetID, a.EventAssetClassTypeID, a.EventTypeDesc, t.AttributeTypeID, t.Attribute_date, note.Attribute_string
			FROM Asset.vAssetEvent a
			JOIN @assetIds b ON a.EventOwnerAssetID = b.id
			JOIN @eventTypes c ON a.EventAssetClassTypeID = c.id
			LEFT JOIN Asset.tAssetAttribute t ON a.EventAssetID = t.AssetID AND t.AttributeTypeID = 207
			LEFT JOIN Asset.tAssetAttribute note ON a.EventAssetID = note.AssetID AND note.AttributeTypeID = 209
			WHERE t.Attribute_date BETWEEN ISNULL(@startDate, CAST('01/01/1753 00:00:00.000' AS DATETIME)) 
			AND ISNULL(@endDate, CAST('12/31/9999 23:59:59.997' AS DATETIME))
			AND COALESCE(note.Attribute_string, '') <> ''

			UNION ALL

			SELECT a.EventOwnerAssetID, a.EventAssetClassTypeID, a.EventTypeDesc, t.AttributeTypeID, t.Attribute_date, note.Attribute_string
			FROM Asset.vAssetEvent a
			JOIN @assetIds b ON a.EventOwnerAssetID = b.id
			JOIN @eventTypes c ON a.EventAssetClassTypeID = c.id
			LEFT JOIN Asset.tAssetAttribute t ON a.EventAssetID = t.AssetID AND t.AttributeTypeID = 208
			LEFT JOIN Asset.tAssetAttribute note ON a.EventAssetID = note.AssetID AND note.AttributeTypeID = 209
			WHERE t.Attribute_date BETWEEN ISNULL(@startDate, CAST('01/01/1753 00:00:00.000' AS DATETIME)) 
			AND ISNULL(@endDate, CAST('12/31/9999 23:59:59.997' AS DATETIME))
			AND COALESCE(note.Attribute_string, '') <> ''
		) r
	)
	SELECT EventOwnerAssetID, EventAssetClassTypeID, EventTypeDesc, Attribute_date, Attribute_string AS EventNotes FROM results
	WHERE RowNum=1
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetEventNotesForAssetView] TO [TEUser]
    AS [dbo];

GO