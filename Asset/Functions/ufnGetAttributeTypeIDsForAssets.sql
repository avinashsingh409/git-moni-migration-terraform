﻿CREATE FUNCTION [Asset].[ufnGetAttributeTypeIDsForAssets]
(
 @assetIDs Base.tpIntList READONLY
)
RETURNS TABLE AS
RETURN
(	
SELECT aa.AssetID, at.AttributeTypeID  
  FROM Asset.tAttributeType at 
  INNER JOIN Asset.tAssetAttribute aa ON aa.AttributeTypeID = at.AttributeTypeID
  INNER JOIN @assetIDs a ON a.ID = aa.AssetID 
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAttributeTypeIDsForAssets] TO [TEUSER]
    AS [dbo];
