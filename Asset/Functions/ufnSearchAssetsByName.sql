﻿CREATE FUNCTION [Asset].[ufnSearchAssetsByName]
(
	@securityUserId int,
	@fromAssetIDs Base.tpIntList READONLY,
	@assetNames Base.tpStringList READONLY,
	@wildcardSearch bit
)
RETURNS 
@AssetIds TABLE 
(
	AssetId int
)
AS
BEGIN
	 DECLARE @accessibleAssetIds Base.tpIntList
	 IF @securityUserID IS NULL
	 BEGIN
		INSERT INTO @accessibleAssetIds
			SELECT * FROM @fromAssetIDs
	 END
	 ELSE
	 BEGIN
		INSERT INTO @accessibleAssetIds
			SELECT AssetID FROM Asset.ufnUserHasAccessToAssets(@securityUserID, @fromAssetIDs)
	END

	DECLARE @searchTerms Base.tpStringList
		INSERT INTO @searchTerms
			SELECT CASE WHEN @wildcardSearch = 1 THEN '%' + a.[value] + '%' ELSE a.[value] END
			FROM @assetNames a
			WHERE RTRIM(ISNULL(a.[value], '')) <> ''

	DECLARE @matchedAssetsNotDistinct TABLE (AssetID INT)
	IF @wildcardSearch = 1
	BEGIN
		INSERT INTO @matchedAssetsNotDistinct
			SELECT a.id
			FROM @accessibleAssetIds a
			JOIN Asset.tAsset b on b.AssetID = a.id
			JOIN @searchTerms d on b.AssetAbbrev like d.[value] -- like
	END
	ELSE
	BEGIN
		INSERT INTO @matchedAssetsNotDistinct
			SELECT a.id
			FROM @accessibleAssetIds a
			JOIN Asset.tAsset b on b.AssetID = a.id
			JOIN @searchTerms d on d.[value] = b.AssetAbbrev -- strict equals
	END

	INSERT INTO @AssetIds
		SELECT DISTINCT AssetID FROM @matchedAssetsNotDistinct
	RETURN 
END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSearchAssetsByName] TO [TEUser]
    AS [dbo];

