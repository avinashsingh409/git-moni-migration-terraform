﻿CREATE FUNCTION [Asset].[ufnSearchAssetsByAttributes]
(
       @securityUserId int,
       @fromAssetIDs Base.tpIntList READONLY,
       @searchCriteriaByAttributeTypeID AS Base.tpIntStringPair READONLY, -- must satisfy at least 1 criteria per AttributeTypeID
       @theseAttributeTypesMustMatchExactly AS Base.tpIntList READONLY -- (AttributeTypeID)
)
RETURNS @assetIds TABLE 
(
       AssetId int
)
AS
BEGIN
       DECLARE @accessibleAssetIds Base.tpIntList
       IF @securityUserID IS NULL
       BEGIN
              INSERT INTO @accessibleAssetIds
                      SELECT * FROM @fromAssetIDs
       END
       ELSE
       BEGIN
           IF EXISTS(SELECT * FROM @fromAssetIDs)
                BEGIN
                INSERT INTO @accessibleAssetIds
                      SELECT AssetID FROM Asset.ufnUserHasAccessToAssets(@securityUserID, @fromAssetIDs)
                END
              ELSE
                BEGIN
                INSERT INTO @accessibleAssetIds
                      SELECT AssetID FROM Asset.ufnGetAssetIdsForUser(@securityUserID)
                END

       END

       DECLARE @distinctAssetAttributeTypeIDs Base.tpIntList
       INSERT INTO @distinctAssetAttributeTypeIDs SELECT DISTINCT Id FROM @searchCriteriaByAttributeTypeID

       DECLARE @exactMatchAttributeTypeIDs Base.tpIntList
       INSERT INTO @exactMatchAttributeTypeIDs
              SELECT Id FROM @theseAttributeTypesMustMatchExactly

       DECLARE @wildcardAttributeTypeIDs Base.tpIntList
       INSERT INTO @wildcardAttributeTypeIDs
              SELECT a.id
                      FROM @distinctAssetAttributeTypeIDs a
                      LEFT OUTER JOIN @exactMatchAttributeTypeIDs b on b.id = a.id
                      WHERE b.id IS NULL

       DECLARE @numAttributeTypes INT
       SELECT @numAttributeTypes = COUNT(*) FROM @distinctAssetAttributeTypeIDs
       
       -- filter assets down to only those assets which have all these attributes
       DECLARE @workingList table (id int)
       INSERT INTO @workingList
              select distinct a.id
                      from @accessibleAssetIds a
                      join @distinctAssetAttributeTypeIDs d on 1=1
                      left outer join asset.tassetattribute aa on aa.assetid = a.id and aa.AttributeTypeID = d.id
                      where aa.assetid is not null option (recompile)
       
       -- search for the exact matches within our smaller list
       IF EXISTS (SELECT TOP 1 * FROM @exactMatchAttributeTypeIDs)
       BEGIN
              DECLARE @assetsMatchingExactly Base.tpIntList
              INSERT INTO @assetsMatchingExactly
                      SELECT distinct b.id
                      FROM Asset.vAssetAttribute aa
                      JOIN @workingList b on b.id = aa.AssetID
                      JOIN @exactMatchAttributeTypeIDs e on e.id = aa.AttributeTypeID
                      JOIN @searchCriteriaByAttributeTypeID s on s.Id = aa.AttributeTypeID
                      LEFT OUTER JOIN Asset.tAttributeOptionType o on o.AttributeOptionTypeID = aa.AttributeOptionTypeID
                      WHERE
						(CASE WHEN aa.DisplayFormat = 'boolean' AND (aa.Value = s.Label OR (aa.Value = 'true' AND '1' = s.Label) OR (aa.Value = 'false' AND '0' = s.Label)) THEN 1
                            WHEN aa.DisplayFormat <> 'boolean' AND aa.Value = s.Label THEN 1
                            ELSE 0 
                        END = 1)
              DELETE a
                      FROM @workingList a
                      LEFT JOIN @assetsMatchingExactly b on b.id = a.id
                      WHERE b.id IS NULL
       END

       -- search for the wildcard matches within our even smaller list
       IF EXISTS (SELECT TOP 1 * FROM @wildcardAttributeTypeIDs)
       BEGIN
              DECLARE @assetsMatchingWildCard Base.tpIntList
              INSERT INTO @assetsMatchingWildCard
                      select distinct b.id
                      FROM Asset.vAssetAttribute aa
                      JOIN @workingList b on b.id = aa.AssetID
                      JOIN @wildcardAttributeTypeIDs e on e.id = aa.AttributeTypeID
                      JOIN @searchCriteriaByAttributeTypeID s on s.Id = aa.AttributeTypeID
                      LEFT OUTER JOIN Asset.tAttributeOptionType o on o.AttributeOptionTypeID = aa.AttributeOptionTypeID
                      WHERE
						(CASE WHEN aa.DisplayFormat = 'boolean' AND (aa.Value like '%' + s.Label + '%' OR (aa.Value = 'true' AND '1' like '%' + s.Label + '%') OR (aa.Value = 'false' AND '0' like '%' + s.Label + '%')) THEN 1
						    WHEN aa.DisplayFormat <> 'boolean' AND aa.Value like '%' + s.Label + '%' THEN 1
						    ELSE 0
						END = 1)
              DELETE a
                      FROM @workingList a
                      LEFT JOIN @assetsMatchingWildCard b on b.id = a.id
                      WHERE b.id IS NULL
       END

       -- result list
       INSERT INTO @assetIds
              SELECT id
              FROM @workingList
       RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSearchAssetsByAttributes] TO [TEUser]
    AS [dbo];
GO