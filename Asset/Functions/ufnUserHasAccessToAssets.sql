﻿CREATE FUNCTION [Asset].[ufnUserHasAccessToAssets] 
(
	-- Add the parameters for the stored procedure here
	@securityUserId int,
	@assetIDs base.tpIntList readonly
)
RETURNS @assetIdTable TABLE
(
	AssetID int PRIMARY KEY not NULL
)
AS
BEGIN

declare @allowed as base.tpintlist

insert into @allowed
select distinct a.id from @assetIDs a join  Asset.tAssetHop b on a.id = b.EndingAssetId
join AccessControl.tAssetAccessRule c on b.HopAssetId = c.AssetID
join AccessControl.tUserRoleMap d on c.SecurityRoleID = d.SecurityRoleID 
where d.SecurityUserID = @securityUserId AND c.Allowed = 1
OPTION (RECOMPILE)

declare @notallowed as base.tpintlist

insert into @notallowed
select distinct a.id from @assetIDs a join  Asset.tAssetHop b on a.id = b.EndingAssetId
join AccessControl.tAssetAccessRule c on b.HopAssetId = c.AssetID
join AccessControl.tUserRoleMap d on c.SecurityRoleID = d.SecurityRoleID 
where d.SecurityUserID = @securityUserId AND c.Allowed = 0
OPTION (RECOMPILE)

insert into @assetIdTable select id from @allowed a where NOT exists (select * from @notallowed b 
where a.id = b.id) OPTION (RECOMPILE)

return

END

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnUserHasAccessToAssets] TO [TEUser]
    AS [dbo];
GO