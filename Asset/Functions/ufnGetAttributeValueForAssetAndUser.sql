﻿CREATE FUNCTION [Asset].[ufnGetAttributeValueForAssetAndUser]
(
	@AttributeTypeId int,
	@AssetID int,
	@SecurityID int
)
RETURNS varchar(max)
AS
BEGIN

select @AttributeTypeId = isnull(@AttributeTypeId, 0)

declare @Value varchar(max)

	IF @AttributeTypeId > 0
	BEGIN
		select 
			@Value = Asset.ufnGetAttributeValue (a.DisplayFormat, aa.Attribute_int, aa.Attribute_string, aa.Attribute_float , aa.Attribute_date, '')
		FROM
		Asset.tAttributeType a
		LEFT JOIN Asset.tAssetAttribute aa
			on a.AttributeTypeID = aa.AttributeTypeID
		where
			 aa.AssetID  = @AssetID
			 and a.AttributeTypeID = @AttributeTypeId
			 AND EXISTS (select IsValid from Asset.ufnAttributeIsValidForUser (@SecurityID, @AssetID, @AttributeTypeId)  where IsValid = 1)
	END
	ELSE
	BEGIN
		SELECT @Value = NULL
	END
return @Value
END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnGetAttributeValueForAssetAndUser] TO [TEUSER]
    AS [dbo];
GO
