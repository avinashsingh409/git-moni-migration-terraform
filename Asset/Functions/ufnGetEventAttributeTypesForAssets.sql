﻿CREATE FUNCTION [Asset].[ufnGetEventAttributeTypesForAssets](
	 @assets base.tpIntList READONLY,
	 @includeOnlyDateTypes bit = 1
)
RETURNS TABLE AS
RETURN
(
select distinct a.EventAssetClassTypeID,a.EventTypeDesc, c.AttributeTypeID,c.AttributeTypeDesc from Asset.vAssetEvent a join Asset.tAssetAttribute b on a.EventAssetID = b.AssetID 
join Asset.tAttributeType c on b.AttributeTypeID = c.AttributeTypeID JOIN 
@assets z on a.EventAssetID = z.id where @includeOnlyDateTypes = 0 or c.DisplayFormat = 'date'
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetEventAttributeTypesForAssets]TO [TEUser]
    AS [dbo];

GO