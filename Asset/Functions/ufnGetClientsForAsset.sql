﻿CREATE FUNCTION [Asset].[ufnGetClientsForAsset]
(
	@assetId int
)
RETURNS 
@clientAssetEntries TABLE 
(
	-- Add the column definitions for the TABLE variable here
	ClientID int,
	AssetID int,
	AncestryGenerationsAway int
)
AS
BEGIN
	DECLARE @assets Base.tpIntList
	INSERT INTO @assets VALUES (@assetID)
	
	INSERT INTO @clientAssetEntries
		SELECT * from Asset.ufnGetClientsForAssets(@assets)
	RETURN
END
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnGetClientsForAsset] TO [TEUser]
    AS [dbo];

