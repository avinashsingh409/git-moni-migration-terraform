﻿
--Create [Asset].[GetAssetGUIDChain] to build a chain of GlobalIds to for each node in an asset path
CREATE FUNCTION [Asset].[GetAssetGUIDChain]
(
	@AssetId int,
	@Chain varchar(4058)
)
RETURNS varchar(4058)
AS
BEGIN

declare @parentAssetID as int
declare @guid as varchar(36)
select @parentAssetID = ParentAssetID, @guid = GlobalID from Asset.tAsset where AssetID=@AssetId
set @Chain = LOWER(@guid) + '\' + @Chain
if (@parentAssetID is not null)
  begin
  select @Chain = Asset.GetAssetGUIDChain(@parentAssetID,@Chain)
  end

return @Chain

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[GetAssetGUIDChain] TO [TEUser]
    AS [dbo];

