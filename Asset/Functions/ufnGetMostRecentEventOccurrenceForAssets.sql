﻿CREATE FUNCTION [Asset].[ufnGetMostRecentEventOccurrenceForAssets](
	 @AssetIDs base.tpIntList READONLY,
	 @EventAssetClassTypeID int,
	 @EventAttributeTypeID int,
	 @StartDate DateTime,
	 @EndDate DateTime
)
RETURNS TABLE AS 
RETURN
(
select d.EventOwnerAssetID,d.EventAssetID, d.EventAssetClassTypeID,d.EventTypeDesc, e.AttributeTypeID, e.AttributeTypeDesc, d.MostRecentOccurrence from 
@AssetIDs a JOIN
Asset.vAssetEvent b
on a.id = b.EventOwnerAssetID
join Asset.tAssetAttribute c ON b.EventAssetID = c.AssetID JOIN Asset.tAttributeType e on c.AttributeTypeID = e.AttributeTypeID
 CROSS APPLY [Asset].[ufnGetMostRecentEventOccurrenceForAsset](ID,@EventAssetClassTypeID,@EventAttributeTypeID,@StartDate,@EndDate) d
 WHERE b.EventAssetClassTypeID = @EventAssetClassTypeID and c.AttributeTypeID = @EventAttributeTypeID
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetMostRecentEventOccurrenceForAssets]TO [TEUser]
    AS [dbo];

GO
