﻿
CREATE FUNCTION [Asset].[ufnAssetsAncestorsWithLevelsIncludeAssets] 
(
	@assets as Base.tpIntList READONLY
)
RETURNS TABLE
AS
RETURN
(
	SELECT DISTINCT ah.HopAssetId AssetID, (ah.AncestryLevel * -1) AncestryGenerationsAway, ah.EndingAssetId DescendentAssetID
	FROM Asset.tAssetHop ah
	INNER JOIN Asset.tAsset ta ON ah.HopAssetId = ta.AssetID AND ta.IsHidden = 0
	INNER JOIN @assets a on a.id = ah.EndingAssetId
)
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnAssetsAncestorsWithLevelsIncludeAssets] TO [TEUser]
    AS [dbo];

