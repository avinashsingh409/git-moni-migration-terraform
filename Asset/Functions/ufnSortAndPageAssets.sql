﻿CREATE FUNCTION [Asset].[ufnSortAndPageAssets]
(
	@assetIDs Base.tpIntList READONLY,
	@securityUserID INT,
	@sortAscending BIT,
	@sortByAssetDesc BIT,
	@sortByAssetKeywords BIT,
	@sortByAssetPath BIT,
	@sortByAttributeTypeID INT,
	@pageNumber INT, -- 1-based
	@assetsPerPage INT
)
RETURNS @sortedAndPagedAssetIDs TABLE
(
	AssetID INT PRIMARY KEY,
	DisplayOrder INT
)
AS
BEGIN
	DECLARE @workingAssets Base.tpIntList
	INSERT INTO @workingAssets
		SELECT * FROM Asset.ufnSearchAssetsById(@securityUserID, @assetIDs)

	DECLARE @startingRecord BIGINT;
	DECLARE @endingRecord BIGINT;
	IF @pageNumber > 0 AND @assetsPerPage > 0
	BEGIN
		SET @startingRecord = (@pageNumber - 1) * @assetsPerPage + 1
		SET @endingRecord = (@pageNumber * @assetsPerPage)
	END
	ELSE
	BEGIN
		SET @startingRecord = 0
		SET @endingRecord = CAST(0x7FFFFFFFFFFFFFFF AS bigint);
	END

	-- sort and paginate
	IF @sortByAssetDesc = 1
	BEGIN
		IF @sortAscending = 1
		BEGIN
			INSERT INTO @sortedAndPagedAssetIDs
				SELECT a.AssetID, a.RN FROM (
					SELECT a.AssetID, ROW_NUMBER() OVER (ORDER BY a.AssetDesc, a.assetid) AS RN -- assetid is tie-breaker
					FROM Asset.tAsset a
					JOIN @workingAssets b on a.AssetID = b.id
				) a WHERE a.RN >= @startingRecord AND a.RN <= @endingRecord
		END
		ELSE
		BEGIN
			INSERT INTO @sortedAndPagedAssetIDs
				SELECT a.AssetID, a.RN FROM (
					SELECT a.AssetID, ROW_NUMBER() OVER (ORDER BY a.AssetDesc DESC, a.assetID DESC) AS RN -- assetid is tie-breaker
					FROM Asset.tAsset a
					JOIN @workingAssets b on a.AssetID = b.id
				) a WHERE a.RN >= @startingRecord AND a.RN <= @endingRecord
		END
	END
	ELSE IF @sortByAssetKeywords = 1
	BEGIN
		DECLARE @firstKeywordForAsset TABLE (AssetID INT PRIMARY KEY, Keyword NVARCHAR(255) NULL)
		INSERT INTO @firstKeywordForAsset
			SELECT x.id, x.[Text] FROM (
				SELECT a.id, k.[Text], ROW_NUMBER() OVER (PARTITION BY a.id ORDER BY k.[Text]) AS RN
				FROM @workingAssets a
				LEFT OUTER JOIN Asset.tAssetKeywordMap m on m.AssetID = a.id
				LEFT OUTER JOIN Asset.tKeyword k on k.KeywordID = m.KeywordID
			) x WHERE x.RN = 1

		IF @sortAscending = 1
		BEGIN
			INSERT INTO @sortedAndPagedAssetIDs
				SELECT x.AssetID, x.RN FROM (
					SELECT a.AssetID, ROW_NUMBER() OVER (ORDER BY a.Keyword, a.AssetID) AS RN -- assetid is the tie-breaker
					FROM @firstKeywordForAsset a
				) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
		END
		ELSE
		BEGIN
			INSERT INTO @sortedAndPagedAssetIDs
				SELECT x.AssetID, x.RN FROM (
					SELECT a.AssetID, ROW_NUMBER() OVER (ORDER BY a.Keyword DESC, a.AssetID DESC) AS RN -- assetid is the tie-breaker
					FROM @firstKeywordForAsset a
				) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
		END
	END
	ELSE IF @sortByAssetPath = 1
	BEGIN
		DECLARE @assetPaths TABLE (AssetID INT PRIMARY KEY, AssetPath VARCHAR(4058))
		INSERT INTO @assetPaths -- make sure we have data
			SELECT a.id, Asset.GetAssetPath(a.id, '')
			FROM @workingAssets a

		IF @sortAscending = 1
		BEGIN
			INSERT INTO @sortedAndPagedAssetIDs
				SELECT x.AssetID, x.RN FROM (
					SELECT a.AssetID, ROW_NUMBER() OVER (ORDER BY a.AssetPath, a.AssetID) AS RN -- assetid is the tie-breaker
					FROM @assetPaths a
					JOIN @workingAssets b on a.AssetID = b.id -- just make sure we use the right set of assets
				) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
		END
		ELSE
		BEGIN
			INSERT INTO @sortedAndPagedAssetIDs
				SELECT x.AssetID, x.RN FROM (
					SELECT a.AssetID, ROW_NUMBER() OVER (ORDER BY a.AssetPath DESC, a.AssetID DESC) AS RN -- assetid is the tie-breaker
					FROM @assetPaths a
					JOIN @workingAssets b on a.AssetID = b.id -- just make sure we use the right set of assets
				) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
		END
	END
	ELSE IF @sortByAttributeTypeID IS NOT NULL AND @sortByAttributeTypeID > 0
	BEGIN
		DECLARE @displayFormat VARCHAR(255);
		SELECT @displayFormat = DisplayFormat FROM Asset.tAttributeType WHERE AttributeTypeID = @sortByAttributeTypeID

		IF @displayFormat LIKE 'float' OR @displayFormat LIKE 'f%' OR @displayFormat LIKE 'p%'
		BEGIN
			IF @sortAscending = 1
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY aa.Attribute_float, a.id) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
			ELSE
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY aa.Attribute_float DESC, a.id DESC) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
		END
		ELSE IF @displayFormat LIKE 'int'
		BEGIN
			IF @sortAscending = 1
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY aa.Attribute_int, a.id) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
			ELSE
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY aa.Attribute_int DESC, a.id DESC) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
		END
		ELSE IF @displayFormat LIKE 'radio'
		BEGIN
			IF @sortAscending = 1
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY t.AttributeOptionTypeDesc, a.id) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						LEFT OUTER JOIN Asset.tAttributeOptionType t on t.AttributeOptionTypeID = aa.AttributeOptionTypeID
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
			ELSE
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY t.AttributeOptionTypeDesc, a.id) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						LEFT OUTER JOIN Asset.tAttributeOptionType t on t.AttributeOptionTypeID = aa.AttributeOptionTypeID
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
		END
		ELSE -- string
		BEGIN
			IF @sortAscending = 1
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY aa.Attribute_string, a.id) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
			ELSE
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY aa.Attribute_string DESC, a.id DESC) AS RN -- assetid is the tie-breaker
						FROM @workingAssets a
						JOIN Asset.tAssetAttribute aa on aa.AssetID = a.id
						WHERE aa.AttributeTypeID = @sortByAttributeTypeID
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
		END
	END
	ELSE -- just sort by asset ID
	BEGIN
			IF @sortAscending = 1
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY a.id) AS RN
						FROM @workingAssets a
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
			ELSE
			BEGIN
				INSERT INTO @sortedAndPagedAssetIDs
					SELECT x.id, x.RN FROM (
						SELECT a.id, ROW_NUMBER() OVER (ORDER BY a.id DESC) AS RN
						FROM @workingAssets a
					) x WHERE x.RN >= @startingRecord AND x.RN <= @endingRecord
			END
	END
	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSortAndPageAssets] TO [TEUser]
    AS [dbo];
GO