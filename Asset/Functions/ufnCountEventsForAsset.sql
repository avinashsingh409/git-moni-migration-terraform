﻿CREATE FUNCTION [Asset].[ufnCountEventsForAsset]
(
    @securityUserID int,
    @assetID int,
	@eventClassTypeID int = null,
	@startTime datetimeoffset = null,
	@endTime datetimeoffset = null
)
RETURNS TABLE AS RETURN
(
    SELECT COUNT(*) EventCount, AssetID, AssetClassTypeID as EventClassTypeID 
	FROM [Asset].ufnGetEvents(@securityUserID, @assetID)
	WHERE AssetID = @assetID
	AND (ISNULL(@eventClassTypeID, -1) < 0 OR AssetClassTypeID = @eventClassTypeID)
	AND (@startTime is null OR @startTime <= PlannedStartDate OR @startTime <= PlannedEndDate OR @startTime <= ActualStartDate OR @startTime <= ActualEndDate)
	AND (@endTime is null OR @endTime >= PlannedStartDate OR @endTime >= PlannedEndDate OR @endTime >= ActualStartDate OR @endTime >= ActualEndDate)
	GROUP BY AssetID, AssetClassTypeID
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnCountEventsForAsset] TO [TEUser]
    AS [dbo];

GO
