﻿CREATE FUNCTION [Asset].[ufnGetClientsForAssets]
(
	@assets as Base.tpIntList READONLY
)
RETURNS 
@clientAssetEntries TABLE 
(
	-- Add the column definitions for the TABLE variable here
	ClientID int,
	AssetID int,
	AncestryGenerationsAway int
)
AS
BEGIN
	INSERT INTO @clientAssetEntries	(ClientID,AssetID,AncestryGenerationsAway)
		SELECT DISTINCT tc.ClientID, tc.AssetID, ancestor.AncestryGenerationsAway
		FROM Asset.ufnAssetsAncestorsWithLevels(@assets) ancestor
		JOIN Asset.tClient tc ON ancestor.AssetID = tc.AssetID


	INSERT INTO @clientAssetEntries	(ClientID,AssetID,AncestryGenerationsAway)
		SELECT DISTINCT tc.ClientID, tc.AssetID, 0
		FROM Asset.tClient tc
		JOIN @assets a on a.id = tc.AssetID
	RETURN 
END
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnGetClientsForAssets] TO [TEUser]
    AS [dbo];
GO
