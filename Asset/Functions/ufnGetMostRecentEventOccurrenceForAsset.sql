﻿CREATE FUNCTION [Asset].[ufnGetMostRecentEventOccurrenceForAsset](
	 @AssetID int,
	 @EventAssetClassTypeID int,
	 @EventAttributeTypeID int,
	 @StartDate DateTime,
	 @EndDate DateTime
)
RETURNS TABLE AS 
RETURN
(
select A.EventOwnerAssetID,a.EventAssetID, A.EventAssetClassTypeID,A.EventTypeDesc,  MAX(b.Attribute_Date) as MostRecentOccurrence from Asset.vAssetEvent a join 
Asset.tAssetAttribute b ON a.EventAssetID = b.AssetID
where a.EventAssetClassTypeID = @EventAssetClassTypeID and b.AttributeTypeID = @EventAttributeTypeID AND a.EventOwnerAssetID = @AssetID AND 
b.Attribute_date BETWEEN ISNULL(@StartDate,CAST('01/01/1753 00:00:00.000' as datetime)) 
AND 
ISNULL(@EndDate,CAST('12/31/9999 23:59:59.997' as datetime))
GROUP BY A.EventOwnerAssetID,a.EventAssetID, A.EventAssetClassTypeID,A.EventTypeDesc
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetMostRecentEventOccurrenceForAsset]TO [TEUser]
    AS [dbo];

GO
