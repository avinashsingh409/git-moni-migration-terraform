﻿CREATE FUNCTION [Asset].[ufnGetClosestClientForAssets]
(
	@assetIds as Base.tpIntList READONLY
)
RETURNS TABLE AS
RETURN
(

select distinct b.ClientID,b.AssetID from (select a.EndingAssetId,MAX(a.ancestrylevel) as level  from Asset.tAssetHop a join Asset.tClient b on a.HopAssetId = b.AssetID join @assetids c on a.EndingAssetId = c.id
group by a.EndingAssetId) a join (select a.EndingAssetId,a.AncestryLevel,b.ClientID,b.AssetID from Asset.tAssetHop a join Asset.tClient b on a.HopAssetId = b.AssetID join @assetids c on a.EndingAssetId = c.id) b 
on a.EndingAssetId = b.EndingAssetId and a.level = b.AncestryLevel 

)

GO

GRANT SELECT
	ON OBJECT::[Asset].[ufnGetClosestClientForAssets] TO [TEUser]
	AS [dbo];
