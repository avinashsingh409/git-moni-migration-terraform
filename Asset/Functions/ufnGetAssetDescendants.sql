﻿CREATE FUNCTION [Asset].[ufnGetAssetDescendants]
(
	@asset int,
	@levels INT,
	@includeParent bit
)
RETURNS @returntable TABLE
(
	assetID int,
	lvl int
)
AS
BEGIN
	IF @includeParent = 1
	BEGIN

	WITH getLevel(AssetID, lvl)
	AS(
		SELECT a.AssetID AS AssetID, 0 AS lvl FROM Asset.tAsset a WHERE a.AssetID = @asset AND a.IsHidden = 0
		UNION ALL 
		SELECT a.AssetID AS AssetID, b.lvl + 1 AS lvl FROM Asset.tAsset a 
		INNER JOIN getLevel b ON a.ParentAssetID = b.AssetID
		WHERE b.lvl < @levels AND a.IsHidden = 0
	)
	INSERT INTO @returntable(assetID, lvl)
	SELECT AssetID, lvl FROM getLevel

	END
	ELSE
	BEGIN 

	WITH getLevel(AssetID, lvl)
	AS(
		SELECT a.AssetID AS AssetID, 1 AS lvl  FROM Asset.tAsset a WHERE a.ParentAssetID = @asset AND a.IsHidden = 0
		UNION ALL 
		SELECT a.AssetID AS AssetID, b.lvl + 1 AS lvl  FROM Asset.tAsset a 
		INNER JOIN getLevel b ON a.ParentAssetID = b.AssetID
		WHERE b.lvl < @levels AND a.IsHidden = 0
	)

	INSERT INTO @returntable(assetID, lvl)
	SELECT AssetID, lvl FROM getLevel

	END

	RETURN
END
GO