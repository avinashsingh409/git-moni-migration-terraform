﻿
CREATE FUNCTION [Asset].[GetYoungestCommonAncestor]
(
	@assetIds Base.tpIntList READONLY
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	 DECLARE @count int;
	 DECLARE @nearestCommonAssetId int;
	 DECLARE @instances int;
	 DECLARE @totalDistance int;

	 SELECT @count = count(*) FROM @assetIds;

	 WITH cteAssets(endingAssetId, assetid, parentassetid, distance) AS 
	 (
		  SELECT a.id, assetId, parentassetid, 0
		  FROM Asset.tAsset ta
		  INNER JOIN @assetIds a ON a.id = ta.AssetID 
		  UNION ALL
		  SELECT ca.endingAssetid, ta.assetid, ta.parentassetid, ca.distance - 1
		  FROM asset.tAsset ta
		  INNER JOIN cteAssets ca ON ta.AssetID = ca.ParentAssetId
	 )
	 SELECT top 1 @nearestCommonAssetId = assetId, @instances = count(*), @totalDistance = sum(ca.distance) 
	 FROM cteAssets ca
	 GROUP BY assetid
	 HAVING COUNT(*) = @count
	 ORDER BY 3 DESC

	 RETURN @nearestCommonAssetId

END
GO
GRANT EXECUTE
    ON OBJECT::[Asset].[GetYoungestCommonAncestor] TO [TEUser]
    AS [dbo];

