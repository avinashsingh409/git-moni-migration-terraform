﻿CREATE FUNCTION [Asset].[ufnGetAvailableAssetAttributesIDs] 
(	
	@securityUserID as int = NULL,	
    @assetID as int = NULL,
	@includeCustom AS BIT = NULL 
)
RETURNS @ids TABLE
(
	AttributeTypeID int PRIMARY KEY not NULL
)
AS
BEGIN
	DECLARE @assets Base.tpIntList
	INSERT INTO @assets VALUES (@assetID)
	
	INSERT INTO @ids
		SELECT * from Asset.ufnGetAvailableAssetsAttributesIDs(@securityUserID, @assets, @includeCustom)
	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAvailableAssetAttributesIDs]  TO [TEUser]
    AS [dbo];
GO
