﻿CREATE FUNCTION [Asset].[ufnGetAvailableAssetsAttributesIDs] 
(	
	@securityUserID as int,	
	@assets as base.tpintlist READONLY,
	@includeCustom AS BIT = NULL 
)
RETURNS @ids TABLE
(
	AttributeTypeID int PRIMARY KEY not NULL
)
AS
BEGIN

	-- there are places in our platform where we do not enforce the requirement that latitude, longitude, polyline and polygon attribute types exist in the asset class type 
	-- to attribute type mapping table.  So, this function goes ahead and returns those 4 attribute types as allowed

	INSERT INTO @ids
		SELECT AttributeTypeID
		FROM Asset.tAttributeType
		WHERE IsExempt = 1

	INSERT INTO @ids
		SELECT DISTINCT map.AttributeTypeID
		FROM Asset.tAsset a
		JOIN Asset.tAssetClassTypeAttributeTypeMap map ON a.AssetClassTypeID = map.AssetClassTypeID
		JOIN @assets assets on assets.id = a.AssetID
		WHERE map.AttributeTypeID NOT IN
		(
			SELECT AttributeTypeID
			FROM Asset.tAttributeType
			WHERE IsExempt = 1
		)

	DECLARE @clientIDs Base.tpIntList;
	INSERT INTO @clientIDs
		SELECT DISTINCT ClientID FROM Asset.ufnGetClientsForAssets(@assets)

	IF (@includeCustom = 1)
	BEGIN
		INSERT INTO @ids
		SELECT DISTINCT t.AttributeTypeID
		FROM Asset.tAttributeType t
			JOIN @clientIDs c ON t.ClientID = c.ID
	END

	-- Don't return any attributes that belong to a category that the user does not have access to
	DELETE i
		FROM @ids i
		JOIN Asset.tAssetAttributeCategoryMap map ON i.AttributeTypeID = map.AttributeTypeID
		JOIN Asset.tCategory c on map.CategoryID = c.CategoryID
		JOIN @clientIDs client on c.ClientID = client.id
		JOIN Asset.tCategoryTypeMap ctm
			ON c.CategoryID = ctm.CategoryID
			AND ctm.CategoryTypeID = 1
			AND
			c.CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
			AND
			c.CategoryID NOT IN
			(
				SELECT CategoryID FROM AccessControl.tRoleCategoryMap r
				JOIN AccessControl.tUserRoleMap urm ON r.SecurityRoleID = urm.SecurityRoleID
				WHERE urm.SecurityUserID = @securityUserID
			)
		WHERE i.AttributeTypeID NOT IN 
		(
			SELECT AttributeTypeID
			FROM Asset.tAttributeType
			WHERE IsExempt = 1
		)

	DELETE i
		FROM @ids i
		JOIN Asset.tAssetAttributeCategoryMap map ON i.AttributeTypeID = map.AttributeTypeID
		JOIN Asset.tCategory c on map.CategoryID = c.CategoryID and c.ClientID is null		
		JOIN Asset.tCategoryTypeMap ctm
			ON c.CategoryID = ctm.CategoryID
			AND ctm.CategoryTypeID = 1
			AND
			c.CategoryID IN (SELECT CategoryID FROM AccessControl.tRoleCategoryMap)
			AND
			c.CategoryID NOT IN
			(
				SELECT CategoryID FROM AccessControl.tRoleCategoryMap r
				JOIN AccessControl.tUserRoleMap urm ON r.SecurityRoleID = urm.SecurityRoleID
				WHERE urm.SecurityUserID = @securityUserID
			)
		WHERE i.AttributeTypeID NOT IN 
		(
			SELECT AttributeTypeID
			FROM Asset.tAttributeType
			WHERE IsExempt = 1
		)
		-- do not exclude lat, long, polyline, polygon based on category
	RETURN
END
GO


GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAvailableAssetsAttributesIDs]  TO [TEUser]
    AS [dbo];
GO
