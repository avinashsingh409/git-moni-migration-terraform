﻿CREATE FUNCTION [Asset].[ufnSearchAssetsByKeyword]
(
	@securityUserId int,
	@fromAssetIDs Base.tpIntList READONLY,
	@keywords Base.tpStringList READONLY,
	@wildcardSearch bit
)
RETURNS 
@AssetIds TABLE 
(
	AssetId int
)
AS
BEGIN
	 DECLARE @accessibleAssetIds Base.tpIntList
	 IF @securityUserID IS NULL
	 BEGIN
		INSERT INTO @accessibleAssetIds
			SELECT * FROM @fromAssetIDs
	 END
	 ELSE
	 BEGIN
		INSERT INTO @accessibleAssetIds
			SELECT AssetID FROM Asset.ufnUserHasAccessToAssets(@securityUserID, @fromAssetIDs)
	END

	DECLARE @searchTerms Base.tpStringList
		INSERT INTO @searchTerms
			SELECT CASE WHEN @wildcardSearch = 1 THEN '%' + a.[value] + '%' ELSE a.[value] END
			FROM @keywords a
			WHERE RTRIM(ISNULL(a.[value], '')) <> ''

	DECLARE @matchedAssetsNotDistinct TABLE (AssetID INT)
	IF @wildcardSearch = 1
	BEGIN
		INSERT INTO @matchedAssetsNotDistinct
			SELECT a.id
			FROM @accessibleAssetIds a
			JOIN Asset.tAssetKeywordMap m on m.AssetID = a.id
			JOIN Asset.tKeyword k on k.KeywordID = m.KeywordID
			JOIN @searchTerms d on k.[Text] like d.[value] -- like
	END
	ELSE
	BEGIN
		INSERT INTO @matchedAssetsNotDistinct
			SELECT a.id
			FROM @accessibleAssetIds a
			JOIN Asset.tAssetKeywordMap m on m.AssetID = a.id
			JOIN Asset.tKeyword k on k.KeywordID = m.KeywordID
			JOIN @searchTerms d on d.[value] = k.[Text] -- strict equals
	END

	INSERT INTO @AssetIds
		SELECT DISTINCT AssetID FROM @matchedAssetsNotDistinct
	RETURN 
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnSearchAssetsByKeyword] TO [TEUser]
    AS [dbo];

