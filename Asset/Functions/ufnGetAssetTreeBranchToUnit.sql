﻿
CREATE FUNCTION [Asset].[ufnGetAssetTreeBranchToUnit]
(
   @AssetID int
)

RETURNS @assetIdTable TABLE
(
  AssetID int PRIMARY KEY not NULL,
  ParentAssetID int NULL,
  IsLeafNode bit not NULL
)
AS
BEGIN

IF (@assetID is null)
  BEGIN
  INSERT INTO @assetIdTable select assetid,parentassetid,0 from asset.tasset a join asset.tAssetClassType b on a.AssetClassTypeID=b.AssetClassTypeID where b.assettypeid<=4
  END
ELSE
  BEGIN
    declare @parentassetid as int
    select @parentassetid = parentassetid from asset.tasset where AssetID=@AssetID;
    
	WITH parentage (assetid,parentassetid)
	AS 
	(
		SELECT a.assetid,a.parentassetid
			FROM asset.tAsset a join asset.tAssetClassType b on a.AssetClassTypeID=b.AssetClassTypeID where a.parentassetid = @assetID and b.assettypeid<=4
		UNION all
		SELECT a.assetid,a.parentassetid
			FROM Asset.tAsset a JOIN parentage c ON c.assetID=a.ParentAssetID join asset.tAssetClassType b on a.AssetClassTypeID=b.AssetClassTypeID where b.assettypeid<=4         
	)  

	INSERT INTO @assetIdTable SELECT a.assetid,parentassetid,0 from parentage a UNION SELECT @assetid,@parentassetid,0
	
  END 

  declare @temp as base.tpIntList
  insert into @temp select distinct parentassetid from @assetIdTable where ParentAssetID is not null

  update @assetIdTable set isleafnode = 1 where assetid not in (select id from @temp)
  
RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetTreeBranchToUnit] TO [TEUser]
    AS [dbo];
GO