﻿CREATE FUNCTION [Asset].[ufnGetAssetPathFromStartToEndAsset]
(
	-- Id1 = starting child asset
	-- Id2 = ending parent asset
	@startToEndAssetPairs Base.tpIntIntPair READONLY,
	@securityUserID INT = -1
)
RETURNS TABLE AS RETURN
(
	-- Return asset path using AssetAbbrev from the starting asset up to the ending asset accessible to the user.
	-- Excludes results where ending asset is not an ancestor of the starting asset.
	-- Leaving SecurityUserID defaulted to -1 will bypass security access check and return all paths up to the ending asset.
	WITH AssetTree AS
	(
		SELECT a.AssetID StartingAssetID, a.ParentAssetID, b.Id2 EndingAssetID, a.AssetID, CONVERT(nvarchar(1000), a.AssetAbbrev) AssetPath
		FROM Asset.tAsset a
		JOIN @startToEndAssetPairs b ON b.Id1 = a.AssetID
		WHERE Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @securityUserID, Default) = 1

		UNION ALL

		SELECT t.StartingAssetID, a.ParentAssetID, t.EndingAssetID, a.AssetID, CONVERT(nvarchar(1000), a.AssetAbbrev + '/' + t.AssetPath) AssetPath
		FROM Asset.tAsset a
		JOIN AssetTree t ON t.ParentAssetID = a.AssetID
		WHERE Asset.ufnDoesUserHaveAccessToAsset(a.AssetID, @securityUserID, Default) = 1
	)

	SELECT grp.StartingAssetID, grp.EndingAssetID, grp.AssetPath FROM (
		SELECT StartingAssetID,
			EndingAssetID,
			'/' + AssetPath AssetPath,
			-- Deprioritize assets above the ending asset even though they have a longer path
			ROW_NUMBER() OVER (PARTITION BY EndingAssetID, StartingAssetID ORDER BY IIF(AssetID = EndingAssetID, 1, 0) DESC, LEN(AssetPath) DESC) RowNum,
			-- An asset with a null parent that isn't the ending asset indicates the ending asset is not an ancestor of the starting asset
			IIF(ParentAssetID is null and AssetID <> EndingAssetID, 0, 1) HasAncestory
		FROM AssetTree a
		) grp
	WHERE grp.RowNum = 1 and grp.HasAncestory = 1
)
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetPathFromStartToEndAsset] TO [TEUser]
    AS [dbo];
GO