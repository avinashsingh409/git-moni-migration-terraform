﻿CREATE FUNCTION [Asset].[ufnGetOnlyPhysicalAssetIdsForUserStartingAt]
(
  @securityuserid  INT,
  @startingAssetID INT
)
RETURNS TABLE AS
RETURN
(	 
select a.EndingAssetId AS AssetID from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID JOIN 
AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 
JOIN Asset.tAsset d on a.EndingAssetId = d.AssetID
JOIN Asset.tAssetHop e on d.AssetID = e.EndingAssetId  JOIN Asset.tAssetClassType f on d.AssetClassTypeID = f.AssetClassTypeID 
WHERE (c.SecurityUserID = @securityUserID) AND d.IsHidden = 0 AND (e.HopAssetID = @startingAssetID) 
AND f.AssetTypeID <= 6
GROUP BY a.EndingAssetId 
		HAVING 
		MIN(CAST(B.Allowed as INT)) = 1

UNION ALL

select a.EndingAssetId AS AssetID from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID JOIN 
AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 
JOIN Asset.tAsset d on a.EndingAssetId = d.AssetID
JOIN Asset.tAssetHop e on d.AssetID = e.EndingAssetId  JOIN Asset.tAssetClassType f on d.AssetClassTypeID = f.AssetClassTypeID 
WHERE (c.SecurityUserID = @securityUserID) AND d.IsHidden = 0 AND (@startingAssetID IS NULL) 
AND f.AssetTypeID <= 6
GROUP BY a.EndingAssetId 
		HAVING 
		MIN(CAST(B.Allowed as INT)) = 1
)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetOnlyPhysicalAssetIdsForUserStartingAt] TO [TEUSER]
    AS [dbo];

GO