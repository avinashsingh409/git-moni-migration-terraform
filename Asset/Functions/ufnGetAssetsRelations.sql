﻿CREATE FUNCTION [Asset].[ufnGetAssetsRelations]
(
	@assetIDs base.tpIntList readonly,
	@securityUserID int,
	@includeAllAssets bit,
	@includeDescendants bit,
	@includeChildren bit,
	@includeAncestors bit,
	@includeParents bit,
	@includeSiblings bit,
	@includeUnitDescendants bit,
	@includeSecondCousins bit,
	@includeCousins bit,
	@includeUnit bit,
	@includeSelf bit	
)
RETURNS TABLE
AS
RETURN
(

SELECT DISTINCT b.AssetID FROM @assetIDs a CROSS APPLY 
 [Asset].[ufnGetAssetRelations]
(
	a.id,
	@securityUserID,
	@includeAllAssets,
	@includeDescendants,
	@includeChildren,
	@includeAncestors,
	@includeParents,
	@includeSiblings,
	@includeUnitDescendants,
	@includeSecondCousins,
	@includeCousins,
	@includeUnit,
	@includeSelf) b
)

GO

GRANT SELECT
	ON OBJECT::[Asset].[ufnGetAssetsRelations] TO [TEUser]
	AS [dbo];
GO
