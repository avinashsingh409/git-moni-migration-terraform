﻿CREATE FUNCTION [Asset].[ufnGetParentAssetIDsOfAssetIDsForAssetTypeAbbrev]
(
	@AssetIDs Base.tpIntList READONLY,
	@AssetTypeAbbrev varchar(50)
)
RETURNS TABLE
AS
RETURN
(
	select ancestry.AssetID
	from  
	(
		--Gets the owning child and ancestry level of the nearest ancestor of each child that has the proper asset type
		select innerAncestry.DescendentAssetID OwningChild, Min(innerAncestry.AncestryGenerationsAway) properAncestor 
		from Asset.[ufnAssetsAncestorsWithLevelsIncludeAssets](@AssetIDs) innerAncestry
		inner join asset.tAsset a on innerAncestry.AssetID = a.AssetID
		inner JOIN Asset.tAssetClassType c ON a.AssetClassTypeID=c.AssetClassTypeID 
		inner JOIN Asset.tAssetType d on c.AssetTypeID=d.AssetTypeID 
		WHERE d.AssetTypeAbbrev=@AssetTypeAbbrev
		GROUP BY innerAncestry.DescendentAssetID
	) ancestorFilter
	inner join Asset.[ufnAssetsAncestorsWithLevelsIncludeAssets](@AssetIDs) ancestry 
		on ancestorFilter.OwningChild = ancestry.DescendentAssetID AND ancestorFilter.properAncestor = ancestry.AncestryGenerationsAway
)
GO
GRANT SELECT
    ON OBJECT::[Asset].[ufnGetParentAssetIDsOfAssetIDsForAssetTypeAbbrev] TO [TEUser]
    AS [dbo];
GO
