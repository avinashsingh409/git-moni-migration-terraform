﻿CREATE FUNCTION [Asset].[ufnGetAssetRelations]
(
	@assetID int,
	@securityUserID int,
	@includeAllAssets bit,
	@includeDescendants bit,
	@includeChildren bit,
	@includeAncestors bit,
	@includeParents bit,
	@includeSiblings bit,
	@includeUnitDescendants bit,
	@includeSecondCousins bit,
	@includeCousins bit,
	@includeUnit bit,
	@includeSelf bit
)
RETURNS TABLE
AS
RETURN
(

	SELECT AssetID from Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) WHERE @includeAllAssets = 1

	UNION

	SELECT AssetID FROM Asset.ufnGetOnlyPhysicalAssetIDsForUserStartingAt(@securityUserID, @assetID) WHERE @includeDescendants = 1 AND AssetID <> @assetID 

	UNION
	
	SELECT a.AssetID FROM Asset.tAsset a JOIN Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) b on a.AssetID = b.AssetID
			WHERE @includeAllAssets = 0 AND @includeChildren = 1 AND a.ParentAssetID = @assetID 
	
	UNION

	SELECT a.AssetID FROM Asset.ufnAssetAncestors(@assetID) a JOIN Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) b ON a.AssetID = b.AssetID
		WHERE @includeAllAssets = 0 AND @includeAncestors = 1 

	UNION

	SELECT parent.AssetID 
		FROM Asset.tAsset parent
		INNER JOIN Asset.tAsset child
		ON child.ParentAssetID = parent.AssetID 
		JOIN Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) b ON parent.AssetID = b.AssetID
		WHERE @includeAllAssets = 0 AND @includeParents = 1 AND child.AssetID = @assetID 

	UNION

	SELECT sibs.AssetID
		FROM Asset.tAsset sibs 
		INNER JOIN Asset.tAsset a 
		ON a.ParentAssetID = sibs.ParentAssetID
		JOIN Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) b ON sibs.AssetID = b.AssetID
		WHERE @includeAllAssets = 0 AND @includeSiblings = 1 AND a.AssetID = @assetID AND sibs.AssetID <> @assetID					

	UNION
	
	SELECT descendants.AssetID
			FROM Asset.tUnit unitAssets 
			INNER JOIN Asset.ufnAssetAncestors(@assetID) anc on unitAssets.AssetID = anc.AssetID
			CROSS APPLY Asset.ufnGetOnlyPhysicalAssetIDsForUserStartingAt(@securityUserID, unitAssets.AssetID) descendants
			
			WHERE @includeAllAssets = 0 AND @includeUnitDescendants = 1

	UNION

	SELECT cousins.AssetID 
		FROM Asset.tAsset asset
		INNER JOIN Asset.tAsset parents ON asset.ParentAssetID = parents.AssetID
		INNER JOIN Asset.tAsset grandparents ON parents.ParentAssetID = grandparents.AssetID 
		INNER JOIN Asset.tAsset uncles ON grandparents.AssetID = uncles.ParentAssetID 
		INNER JOIN Asset.tAsset cousins ON uncles.AssetID = cousins.ParentAssetID 
		JOIN Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) b ON cousins.AssetID = b.AssetID
		WHERE @includeAllAssets = 0 AND @includeCousins = 1 AND asset.AssetID = @assetID AND cousins.ParentAssetID <> asset.ParentAssetID

	UNION

	SELECT secondCousins.AssetID 
		FROM Asset.tAsset asset
		INNER JOIN Asset.tAsset parents ON asset.ParentAssetID = parents.AssetID AND asset.AssetID = @assetID
		INNER JOIN Asset.tAsset grandparents ON parents.ParentAssetID = grandparents.AssetID 
		INNER JOIN Asset.tAsset greatGrandParents ON grandparents.ParentAssetID = greatGrandParents.AssetID 
		INNER JOIN Asset.tAsset greatUncles ON greatGrandParents.AssetID = greatUncles.ParentAssetID 
		INNER JOIN Asset.tAsset onceRemoved ON greatUncles.AssetID = onceRemoved.ParentAssetID 
		INNER JOIN Asset.tAsset secondCousins ON onceRemoved.AssetID = secondCousins.ParentAssetID 
		JOIN Asset.ufnGetOnlyPhysicalAssetIDsForUser(@securityUserID) b ON secondCousins.AssetID = b.AssetID
		WHERE @includeAllAssets = 0 AND @includeSecondCousins = 1 AND secondCousins.ParentAssetID <> asset.ParentAssetID

	UNION

	select Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@assetID, 'UN') as assetid
        where Asset.ufnDoesUserHaveAccessToAsset(Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@assetID, 'UN'), @securityUserID, -1) = 1	
		AND @includeAllAssets = 0 AND @includeUnit = 1


	UNION

	SELECT @assetID as AssetID FROM Asset.ufnGetOnlyPhysicalAssetIDsForUserStartingAt(@securityUserID,@assetID) WHERE @includeAllAssets = 0 AND @includeSelf = 1 

)

GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetRelations] TO [TEUSER]
    AS [dbo];

GO