﻿CREATE FUNCTION [Asset].[ufnDoesUserHaveAccessToQuickSearch] (
	@quickSearchID int, 
	@securityuserid int
	)
RETURNS bit
AS
BEGIN	
DECLARE @result BIT = 0 
IF EXISTS 
(
	SELECT TOP 1 a.CategoryID FROM 
	(
		SELECT b.CategoryID  FROM Asset.tQuickSearchColumn a
		INNER JOIN Asset.tAssetAttributeCategoryMap b ON a.AttributeTypeID = b.AttributeTypeID
		WHERE a.QuickSearchID = @quickSearchID 
		UNION 
		SELECT a.CategoryID FROM Asset.tQuickSearchCategoryMap a
	) a
	INNER JOIN AccessControl.tRoleCategoryMap b ON a.CategoryID = b.CategoryID
	WHERE b.SecurityRoleID NOT IN (
		SELECT a.SecurityRoleID FROM 
		AccessControl.tUserRoleMap a WHERE a.SecurityUserID = @securityUserID 
	)
)
	SET @result = 0
ELSE
	SET @result = 1

RETURN @result

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnDoesUserHaveAccessToQuickSearch] TO [TEUser]
    AS [dbo];