﻿CREATE FUNCTION [Asset].[ufnDoesAssetHaveAccessToQuickSearch]
(
	@assetID int, 
	@quickSearchID int 
)
RETURNS bit
AS
BEGIN	
DECLARE @result BIT = 0 
DECLARE @quickSearchOwningAssetID INT; 
DECLARE @applyToSelectedAssetDescendants BIT; 

SELECT  @quickSearchOwningAssetID = AssetId, 
		@applyToSelectedAssetDescendants = ApplyToSelectedAssetDescendants 
FROM [Asset].[tQuickSearch] WHERE QuickSearchId = @quickSearchID;

IF (@assetID = @quickSearchOwningAssetID)
	--QuickSearch is owned by @assetID
	BEGIN
		SET @result = 1
	END
ELSE IF (@applyToSelectedAssetDescendants = 1 AND 
		EXISTS(SELECT TOP 1 * FROM [Asset].[ufnAssetAncestors](@assetID) WHERE AssetID = @quickSearchOwningAssetID))
	--QuickSearch is cascaded down from ancestors	
	BEGIN 	
		DECLARE @descendantAssetClassTypeIDs Base.tpIntList;
		INSERT INTO @descendantAssetClassTypeIDs
		SELECT b.DescendantAssetClassTypeID FROM [Asset].[tQuickSearch] a 
			JOIN [Asset].tQuickSearchSelectedDescendantAssetClassTypes b ON a.QuickSearchId = b.QuickSearchID
		WHERE a.QuickSearchID = @quickSearchID

		IF EXISTS( SELECT TOP 1 * FROM @descendantAssetClassTypeIDs)
		--Cascaded quickSearch only applicable for selected asset type. 
			BEGIN
				IF EXISTS (SELECT * FROM Asset.tAsset a JOIN @descendantAssetClassTypeIDs b ON a.AssetClassTypeID = b.id
							WHERE a.AssetID = @assetID)
				BEGIN
					SET @result = 1
				END
			END
		ELSE
		--Cascaded quickSearch applicable for ALL asset types.
			BEGIN 
				SET @result = 1
			END
	END
ELSE
	BEGIN
		SET @result = 0
	END

RETURN @result

END
GO

GRANT EXECUTE
    ON OBJECT::[Asset].[ufnDoesAssetHaveAccessToQuickSearch] TO [TEUser]
    AS [dbo];
GO