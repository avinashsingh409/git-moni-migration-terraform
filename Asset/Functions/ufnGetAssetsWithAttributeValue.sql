﻿-------------------------------------------------------------------------------------------
CREATE   FUNCTION [Asset].[ufnGetAssetsWithAttributeValue]
(
@securityUserID int,
@attributeTypeDescription NVARCHAR(MAX),
@attributeTypeID INT,
@startingAssetId int,
@values Base.tpStringList READONLY
)
RETURNS @assetsTable TABLE
(
  assetID int, 
  value NVARCHAR(MAX)
)
AS
BEGIN


	IF @attributeTypeID IS NULL
	BEGIN
		DECLARE @clients Base.tpIntList
		INSERT INTO @clients(id)
		SELECT ClientID FROM Asset.ufnGetClientsForAsset(@startingAssetId)
	
		SELECT @attributeTypeID = a.AttributeTypeID
		FROM Asset.tAttributeType a
		INNER JOIN @clients c ON a.ClientID = c.id OR a.IsStandard = 1
		WHERE AttributeTypeDesc = @attributeTypeDescription
	END

    IF ISNULL(@startingAssetId,-1) <= 0
	  BEGIN
	  INSERT INTO @assetsTable 
	    SELECT att.AssetID, att.Attribute_string FROM Asset.tAssetAttribute att
		WHERE att.AttributeTypeID = @attributeTypeID 	
	     AND att.Attribute_string in (SELECT value from @values) 
	  END
	ELSE
	  BEGIN
	  INSERT INTO @assetsTable 
	    SELECT att.AssetID, att.Attribute_string FROM Asset.tAssetAttribute att 
	    INNER JOIN asset.ufnGetAssetTreeBranch(@startingAssetId) b on att.assetid = b.assetid
	    WHERE att.AttributeTypeID = @attributeTypeID
		 AND att.Attribute_string IN (SELECT value from @values) 
	  END

	DELETE FROM @assetsTable WHERE Asset.ufnDoesUserHaveAccessToAsset(assetid,@securityUserID,-1)!=1
	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Asset].[ufnGetAssetsWithAttributeValue] TO [TEUser]
    AS [dbo];
GO
