CREATE TABLE [PowerRAM].[tMaintenanceOutageDetail] (
    [MaintenanceOutageDetailID]  INT           IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanSetID]       INT           NOT NULL,
    [MaintenanceOutageID]        INT           NULL,
    [RAMModelID]                 INT           NOT NULL,
    [SystemNum]                  INT           NOT NULL,
    [ComponentID]                NVARCHAR (50) NOT NULL,
    [PostOutageIncrease_pct]     FLOAT (53)    CONSTRAINT [DF_tMaintenanceOutageDetail_PostOutageIncreasePct] DEFAULT ((0)) NOT NULL,
    [BaselineCycleLength_months] FLOAT (53)    CONSTRAINT [DF_tMaintenanceOutageDetail_BaselineCycleLength] DEFAULT ((0)) NOT NULL,
    [WorstMTBF_pct]              FLOAT (53)    CONSTRAINT [DF_tMaintenanceOutageDetail_WorstMTBFPct] DEFAULT ((0)) NOT NULL,
    [TimeToWorstMTBF_months]     FLOAT (53)    CONSTRAINT [DF_tMaintenanceOutageDetail_TimeToWorstMTBF_months] DEFAULT ((0)) NOT NULL,
    [OutageType]                 NVARCHAR (20) NOT NULL,
    [PostOutageAverageMTBF]      FLOAT (53)    NULL,
    [UseCalibrationMTBF]         BIT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tMaintenanceOutageDetail] PRIMARY KEY CLUSTERED ([MaintenanceOutageDetailID] ASC),
    CONSTRAINT [FK_tMaintenanceOutageDetail_tMaintenanceOutage] FOREIGN KEY ([MaintenanceOutageID]) REFERENCES [PowerRAM].[tMaintenanceOutage] ([MaintenanceOutageID]),
    CONSTRAINT [FK_tMaintenanceOutageDetail_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMaintenanceOutageDetail_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tMaintenanceOutageDetail';

