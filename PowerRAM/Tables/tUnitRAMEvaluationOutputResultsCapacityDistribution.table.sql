﻿CREATE TABLE [PowerRAM].[tUnitRAMEvaluationOutputResultsCapacityDistribution] (
    [UnitRAMEvaluationOutputResultsCapacityDistributionID] INT  IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationPeriodKeyID]                         INT  NOT NULL,
    [DeliveredCapacity_PercentOfPeak]                      REAL NULL,
    [DeliveredCapacity_PercentOfTimeEqualled]              REAL NULL,
    [DeliveredCapacity_PercentOfTimeEqualledOrExceeded]    REAL NULL,
    CONSTRAINT [PK_tUnitRAMEvaluationOutputResultsCapacityDistribution] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationOutputResultsCapacityDistributionID] ASC),
    CONSTRAINT [FK_tUnitRAMEvaluationOutputResultsCapacityDistribution_PowerRAM_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
);










GO
CREATE NONCLUSTERED INDEX [idx_tUnitRAMEvaluationOutputResultsCapacityDistribution_1]
    ON [PowerRAM].[tUnitRAMEvaluationOutputResultsCapacityDistribution]([DeliveredCapacity_PercentOfTimeEqualled] ASC)
    INCLUDE([UnitRAMEvaluationPeriodKeyID], [DeliveredCapacity_PercentOfPeak]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluationOutputResultsCapacityDistribution';


GO
CREATE NONCLUSTERED INDEX [IDX_RAM_CAPDIST]
    ON [PowerRAM].[tUnitRAMEvaluationOutputResultsCapacityDistribution]([UnitRAMEvaluationPeriodKeyID] ASC)
    INCLUDE([UnitRAMEvaluationOutputResultsCapacityDistributionID], [DeliveredCapacity_PercentOfPeak], [DeliveredCapacity_PercentOfTimeEqualled], [DeliveredCapacity_PercentOfTimeEqualledOrExceeded]);

