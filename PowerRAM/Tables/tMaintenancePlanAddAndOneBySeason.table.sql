CREATE TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] (
    [MaintenancePlanAddAndOneBySeasonID] INT           IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanSetID]               INT           NOT NULL,
    [UnitID]                             INT           NOT NULL,
    [Season]                             VARCHAR (10)  NOT NULL,
    [RAMModelID]                         INT           NOT NULL,
    [SystemNum]                          INT           NOT NULL,
    [ComponentID]                        NVARCHAR (10) NOT NULL,
    [UnavailableContributor]             INT           NOT NULL,
    [CapacityPercent]                    FLOAT (53)    NOT NULL,
    [DisplayOrder]                       INT           NOT NULL,
    CONSTRAINT [PK_tMaintenancePlanAddAndOneBySeason] PRIMARY KEY CLUSTERED ([MaintenancePlanAddAndOneBySeasonID] ASC),
    CONSTRAINT [FK_tMaintenancePlanAddAndOneBySeason_MaintenancePlanSetID_tMaintenancePlan] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]),
    CONSTRAINT [FK_tMaintenancePlanAddAndOneBySeason_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [FK_tRAMModelComponent_tMaintenancePlanAddAndOneBySeason_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tMaintenancePlanAddAndOneBySeason';

