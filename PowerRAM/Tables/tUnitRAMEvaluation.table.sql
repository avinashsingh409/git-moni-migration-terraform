CREATE TABLE [PowerRAM].[tUnitRAMEvaluation] (
    [UnitRAMEvaluationID] INT            IDENTITY (1, 1) NOT NULL,
    [RAMEvaluationID]     INT            NOT NULL,
    [UnitID]              INT            NOT NULL,
    [EvaluationDate]      DATETIME       NULL,
    [RAMEvaluationStatus] INT            NULL,
    [IsInitialized]       BIT            CONSTRAINT [DF_tUnitRAMEvaluation_IsInitialized] DEFAULT ((0)) NOT NULL,
    [ModelRunComplete]    BIT            CONSTRAINT [DF_tUnitRAMEvaluation_ModelRunComplete] DEFAULT ((0)) NOT NULL,
    [ResultsComplete]     BIT            CONSTRAINT [DF_tUnitRAMEvaluation_ResultsComplete] DEFAULT ((0)) NOT NULL,
    [StartTime]           DATETIME       NULL,
    [StopTime]            DATETIME       NULL,
    [LastErrorMsg]        VARCHAR (MAX)  NULL,
    [LastWarningMsg]      VARCHAR (MAX)  NULL,
    [CreatedBy]           NVARCHAR (255) NULL,
    [CreateDate]          DATETIME       CONSTRAINT [DF_tUnitRAMEvaluation_CreateDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_tUnitRAMEvaluation] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationID] ASC),
    CONSTRAINT [FK_tUnitRAMEvaluation_RAMEvaluationID_tRAMEvaluation] FOREIGN KEY ([RAMEvaluationID]) REFERENCES [PowerRAM].[tRAMEvaluation] ([RAMEvaluationID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tUnitRAMEvaluation_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluation';

