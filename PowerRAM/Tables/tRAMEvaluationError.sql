﻿CREATE TABLE [PowerRAM].[tRAMEvaluationError] (
    [RAMEvaluationErrorID]       INT            IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationPeriodKey] INT            NULL,
    [ExceptionDetails]           VARCHAR (MAX)  NOT NULL,
    [ServiceLog]                 VARCHAR (MAX)  NULL,
    [ChangedBy]                  NVARCHAR (255) NULL,
    [ChangeDate]                 DATETIME       NULL,
    [CreatedBy]                  NVARCHAR (255) NULL,
    [CreateDate]                 DATETIME       NULL,
    CONSTRAINT [PK_tRAMEvaluationError] PRIMARY KEY CLUSTERED ([RAMEvaluationErrorID] ASC),
    CONSTRAINT [FK_tRAMEvaluationError_UnitRAMEvaluationPeriodKey_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKey]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tRAMEvaluationError';

