CREATE TABLE [PowerRAM].[tMaintenanceOutage] (
    [MaintenanceOutageID]  INT            IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanSetID] INT            NOT NULL,
    [UnitID]               INT            NOT NULL,
    [OutageDesc]           NVARCHAR (255) NOT NULL,
    [OutageType]           NVARCHAR (20)  NOT NULL,
    [StartDate]            DATETIME       NOT NULL,
    [Duration_days]        INT            CONSTRAINT [DF_tMaintenanceOutage_Duration_days] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tMaintenanceOutage] PRIMARY KEY CLUSTERED ([MaintenanceOutageID] ASC),
    CONSTRAINT [FK_tMaintenanceOutage_MaintenancePlanSetID_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMaintenanceOutage_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tMaintenanceOutage';

