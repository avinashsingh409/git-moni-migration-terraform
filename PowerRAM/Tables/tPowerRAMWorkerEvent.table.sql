CREATE TABLE [PowerRAM].[tPowerRAMWorkerEvent] (
    [EventID]           INT            IDENTITY (1, 1) NOT NULL,
    [RAMEvaluationID]   INT            NOT NULL,
    [WorkerIPAddress]   NVARCHAR (50)  NULL,
    [WorkerMachineName] NVARCHAR (255) NULL,
    CONSTRAINT [PK_PowerRAM_tPowerRAMWorkerEvent] PRIMARY KEY CLUSTERED ([EventID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tPowerRAMWorkerEvent';

