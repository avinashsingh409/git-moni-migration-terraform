CREATE TABLE [PowerRAM].[tUnitRAMEvaluationOutputSystemResults] (
    [UnitRAMEvaluationOutputSystemResultsID] INT  IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationPeriodKeyID]           INT  NOT NULL,
    [SystemNum]                              INT  NOT NULL,
    [EAF]                                    REAL NULL,
    CONSTRAINT [PK_tUnitRAMEvaluationOutputSystemResults] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationOutputSystemResultsID] ASC),
    CONSTRAINT [FK_tUnitRAMEvaluationOutputSystemResults_PowerRAM_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX [IDX_tUnitRAMEvaluationOutputSystemResults_1]
    ON [PowerRAM].[tUnitRAMEvaluationOutputSystemResults]([UnitRAMEvaluationPeriodKeyID] ASC)
    INCLUDE([UnitRAMEvaluationOutputSystemResultsID], [SystemNum], [EAF]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluationOutputSystemResults';

