CREATE TABLE [PowerRAM].[tRAMSystemFile] (
    [RAMSystemFileID] INT           IDENTITY (1, 1) NOT NULL,
    [RAMSystemNum]    INT           NOT NULL,
    [RAMModelID]      INT           NOT NULL,
    [RawTextFile]     VARCHAR (MAX) NULL,
    [DisplayOrder]    INT           NULL,
    CONSTRAINT [PK_RAMSystemFile] PRIMARY KEY CLUSTERED ([RAMSystemFileID] ASC),
    CONSTRAINT [FK_tRAMSystemFile_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tRAMSystemFile';

