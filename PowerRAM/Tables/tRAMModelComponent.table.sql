CREATE TABLE [PowerRAM].[tRAMModelComponent] (
    [RAMModelComponentID] INT            IDENTITY (1, 1) NOT NULL,
    [RAMModelID]          INT            NOT NULL,
    [SystemNum]           INT            NOT NULL,
    [ComponentID]         NVARCHAR (10)  NOT NULL,
    [ShapeName]           NVARCHAR (20)  NULL,
    [ShapeNumber]         NVARCHAR (10)  NULL,
    [ShapeText]           NVARCHAR (255) NULL,
    [MTBF]                REAL           NULL,
    [MTTR]                REAL           NULL,
    [Comment]             NVARCHAR (255) NULL,
    [OriginalMTBF]        REAL           NULL,
    [DefaultMajorOutage]  BIT            NOT NULL,
    [DefaultMinorOutage]  BIT            NOT NULL,
    CONSTRAINT [PK_RAMModelComponent] PRIMARY KEY CLUSTERED ([RAMModelComponentID] ASC),
    CONSTRAINT [FK_tRAMModelComponent_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tRAMModelComponent';

