CREATE TABLE [PowerRAM].[tUnitRAMEvaluationPeriodKey] (
    [UnitRAMEvaluationPeriodKeyID] INT IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationID]          INT NOT NULL,
    [UnitID]                       INT NOT NULL,
    [UnitConfigID]                 INT NULL,
    [PeriodNum]                    INT NOT NULL,
    CONSTRAINT [PK_tUnitRAMEvaluationPeriodKey] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationPeriodKeyID] ASC),
    CONSTRAINT [FK_tUnitRAMEvaluationPeriodKey_UnitID_tAsset] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]),
    CONSTRAINT [FK_tUnitRAMEvaluationPeriodKey_UnitRAMEvaluationID_ttUnitRAMEvaluation] FOREIGN KEY ([UnitRAMEvaluationID]) REFERENCES [PowerRAM].[tUnitRAMEvaluation] ([UnitRAMEvaluationID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX [IDX_tUnitRAMEvaluationPeriodKey_1]
    ON [PowerRAM].[tUnitRAMEvaluationPeriodKey]([UnitRAMEvaluationID] ASC, [UnitID] ASC)
    INCLUDE([UnitRAMEvaluationPeriodKeyID]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluationPeriodKey';

