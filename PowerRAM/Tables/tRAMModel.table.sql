﻿CREATE TABLE [PowerRAM].[tRAMModel] (
    [RAMModelID]   INT            IDENTITY (1, 1) NOT NULL,
    [RAMModelDesc] NVARCHAR (255) NOT NULL,
    [RAMModelName] NVARCHAR (255) NOT NULL,
    [UnitID]       INT            NOT NULL,
    [Comment]      NVARCHAR (255) NULL,
    [IsPublic]     BIT            CONSTRAINT [DF_RAMModel_IsPublic] DEFAULT ((1)) NOT NULL,
    [IsArchived]   BIT            CONSTRAINT [DF_RAMModel_IsArchived] DEFAULT ((0)) NOT NULL,
    [CreateDate]   DATETIME       CONSTRAINT [DF_RAMModel_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    NVARCHAR (255) NOT NULL,
    [ChangeDate]   DATETIME       CONSTRAINT [DF_RAMModel_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [ChangedBy]    NVARCHAR (255) NOT NULL,
    [ImportDate]   DATETIME       NULL,
    CONSTRAINT [PK_RAMModel] PRIMARY KEY CLUSTERED ([RAMModelID] ASC),
    CONSTRAINT [FK_tRAMModel_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
CREATE TRIGGER [PowerRAM].[tRAMModelChange]
   ON  [PowerRAM].[tRAMModel]
   FOR UPDATE
AS
BEGIN    
	--Any RAMModel that gets updated needs to set the RecalculateMaintenancePlan
	--bit for all scenarios that use that plan.  Join on tMaintenancePlanSet and
	--tScenario to make it happen.
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 1
	where ScenarioID in
	(		
		Select scen.ScenarioID 
		from [Projection].[tScenario] scen 
		join [PowerRAM].[tMaintenancePlanSet] mps on scen.MaintenancePlanSetID = mps.MaintenancePlanSetID
		join [Projection].[tUnitConfigScheduleSet] ucss on scen.UnitConfigScheduleSetID = ucss.UnitConfigScheduleSetID
		join [Projection].[tUnitConfigScheduleDetail] ucsd on ucss.UnitConfigScheduleSetID = ucsd.UnitConfigScheduleSetID
		join [Projection].[tUnitConfig] uc on ucsd.UnitConfigID = uc.UnitConfigID
		join [PowerRAM].[tRAMModel] rm on uc.RAMModelID = rm.RAMModelID
		join inserted i on rm.RAMModelID = i.RAMModelID		
	)
END

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tRAMModel';

