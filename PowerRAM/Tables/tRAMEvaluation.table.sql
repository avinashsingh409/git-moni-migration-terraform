CREATE TABLE [PowerRAM].[tRAMEvaluation] (
    [RAMEvaluationID]     INT            IDENTITY (1, 1) NOT NULL,
    [ScenarioID]          INT            NOT NULL,
    [RAMEvaluationDesc]   NVARCHAR (255) NULL,
    [StartDate]           DATETIME       NOT NULL,
    [Duration]            INT            NOT NULL,
    [DurationType]        CHAR (1)       NOT NULL,
    [PeriodType]          CHAR (1)       NOT NULL,
    [EvaluationDate]      DATETIME       NOT NULL,
    [RAMEvaluationStatus] INT            NULL,
    [CreatedBy]           NVARCHAR (255) NULL,
    [CreateDate]          DATETIME       CONSTRAINT [DF_tRAMEvaluation_CreateDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_tRAMEvaluation] PRIMARY KEY CLUSTERED ([RAMEvaluationID] ASC),
    CONSTRAINT [FK_tRAMEvaluation_MaintenancePlanSetID_tMaintenancePlanSet] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [PowerRAM].[tMaintenanceCalcsSucceeded]
   ON  [PowerRAM].[tRAMEvaluation]
   FOR UPDATE
AS
BEGIN    
	--Any maintenance calcs that just succeeded should clear the RecalculateMaintenancePlan
	--bit for the scenario tied to the calc.  Join on tScenario to make it happen.
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 0
	where ScenarioID in
	(
		(
		Select scen.ScenarioID 
		from [Projection].[tScenario] scen
		join inserted as i on scen.ScenarioID = i.ScenarioID
		WHERE i.RAMEvaluationStatus = 4 -- 4 is Success.
		)
		--union
		--(
		--Select scen.ScenarioID 
		--from [Projection].[tScenario] scen
		--join [Projection].[tUnitConfigScheduleSet] ucss on scen.UnitConfigScheduleSetID = ucss.UnitConfigScheduleSetID
		--join inserted as i on ucss.UnitConfigScheduleSetID = i.UnitConfigScheduleSetID
		--)
	)
END
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tRAMEvaluation';

