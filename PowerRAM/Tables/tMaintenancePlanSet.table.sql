CREATE TABLE [PowerRAM].[tMaintenancePlanSet] (
    [MaintenancePlanSetID]     INT            IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanSetDesc]   NVARCHAR (255) NOT NULL,
    [Comment]                  NVARCHAR (255) NULL,
    [StartDate]                DATETIME       NOT NULL,
    [DataVersionID]            INT            NULL,
    [Duration]                 INT            NOT NULL,
    [DurationType]             CHAR (1)       CONSTRAINT [DF_tMaintenancePlanSet_DurationType] DEFAULT ('M') NOT NULL,
    [PeriodType]               CHAR (1)       CONSTRAINT [DF_tMaintenancePlanSet_PeriodType] DEFAULT ('M') NOT NULL,
    [IsConsensus]              BIT            CONSTRAINT [DF_tMaintenancePlanSet_IsConsensus] DEFAULT ((0)) NOT NULL,
    [IsSensitivity]            BIT            CONSTRAINT [DF_tMaintenancePlanSet_IsSensitivity] DEFAULT ((0)) NOT NULL,
    [IsPublic]                 BIT            CONSTRAINT [DF_tMaintenancePlanSet_IsPublic] DEFAULT ((1)) NOT NULL,
    [IsArchived]               BIT            CONSTRAINT [DF_tMaintenancePlanSet_IsArchived] DEFAULT ((0)) NOT NULL,
    [HasOverwrites]            BIT            CONSTRAINT [DF_tMaintenancePlanSet_HasOverwrites] DEFAULT ((0)) NOT NULL,
    [CreateDate]               DATETIME       CONSTRAINT [DF_tMaintenancePlanSet_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                NVARCHAR (255) NOT NULL,
    [ChangeDate]               DATETIME       CONSTRAINT [DF_tMaintenancePlanSet_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [ChangedBy]                NVARCHAR (255) NOT NULL,
    [OwningAssetID]            INT            NULL,
    [MaintenancePlanSetAbbrev] NVARCHAR (50)  CONSTRAINT [MaintenancePlanSetAbbrevDefault] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_tMaintenancePlanSet] PRIMARY KEY CLUSTERED ([MaintenancePlanSetID] ASC),
    CONSTRAINT [FK_PowerRAM_tMaintenancePlanSet_OwningAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([OwningAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tMaintenancePlanSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
);








GO

GO

CREATE TRIGGER [PowerRAM].[tMaintenancePlanSetChange]
   ON  [PowerRAM].[tMaintenancePlanSet]
   FOR UPDATE
AS
BEGIN    
	--Any maintenance plan that gets updated needs to set the RecalculateMaintenancePlan
	--bit for all scenarios that use that plan.  Join on tScenario to make it happen.
	Update [Projection].[tScenario]
	set RecalculateMaintenancePlan = 1
	where ScenarioID in
	(
		(
		Select scen.ScenarioID 
		from [Projection].[tScenario] scen
		join inserted as i on scen.MaintenancePlanSetID = i.MaintenancePlanSetID
		)		
	)
END
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tMaintenancePlanSet';

