﻿CREATE TABLE [PowerRAM].[tMaintenancePlanComponentMTBF] (
    [MaintenancePlanComponentMtbfID] INT  IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanSetID]           INT  NOT NULL,
    [RAMModelComponentID]            INT  NOT NULL,
    [MTBF]                           REAL CONSTRAINT [DF_MaintenancePlanComponentMTBF_MTBF] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MaintenancePlanComponentMTBF] PRIMARY KEY CLUSTERED ([MaintenancePlanComponentMtbfID] ASC),
    CONSTRAINT [FK_MaintenancePlanComponentMTBF_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_MaintenancePlanComponentMTBF_tRAMModelComponent] FOREIGN KEY ([RAMModelComponentID]) REFERENCES [PowerRAM].[tRAMModelComponent] ([RAMModelComponentID])
);








GO

GO

GO

GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_tMaintenancePlanComponentMTBF_MaintenancePlanSetID_RAMModelComponentID]
    ON [PowerRAM].[tMaintenancePlanComponentMTBF]([MaintenancePlanSetID] ASC, [RAMModelComponentID] ASC)
    INCLUDE([MTBF]);


GO

Create Trigger PowerRAM.tMaintenancePlanComponentMTBFChange
on PowerRAm.tMaintenancePlanComponentMTBF
FOR Insert, Update
AS
BEGIN

	UPDATE [PowerRAM].[tMaintenanceOutageDetail] 
	   SET [PostOutageAverageMTBF] = i.MTBF
	 FROM inserted i
	 join PowerRAM.tMaintenancePlanComponentMTBF mpcMTBF 
		on i.MaintenancePlanComponentMtbfID = mpcMTBF.MaintenancePlanComponentMtbfID 
	 join PowerRAM.tRAMModelComponent modComp 
		on mpcMTBF.RAMModelComponentID = modComp.RAMModelComponentID
	 WHERE [tMaintenanceOutageDetail].RAMModelID = modComp.RAMModelID
	 and [tMaintenanceOutageDetail].SystemNum = modComp.SystemNum
	 and [tMaintenanceOutageDetail].ComponentID = modComp.ComponentID
	 and [tMaintenanceOutageDetail].UseCalibrationMTBF = 1

END
;
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tMaintenancePlanComponentMTBF';

