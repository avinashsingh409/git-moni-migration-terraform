CREATE TABLE [PowerRAM].[tUnitRAMEvaluationOutputResultsEAFDistribution] (
    [UnitRAMEvaluationOutputResultsEAFDistributionID] INT  IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationPeriodKeyID]                    INT  NOT NULL,
    [EAF]                                             REAL NULL,
    [Probability]                                     REAL NULL,
    [CumulativeProbability]                           REAL NULL,
    CONSTRAINT [PK_tUnitRAMEvaluationOutputResultsEAFDistribution] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationOutputResultsEAFDistributionID] ASC),
    CONSTRAINT [FK_tUnitRAMEvaluationOutputResultsEAFDistribution_PowerRAM_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX [IDX_tUnitRAMEvaluationOutputResultsEAFDistribution_1]
    ON [PowerRAM].[tUnitRAMEvaluationOutputResultsEAFDistribution]([UnitRAMEvaluationPeriodKeyID] ASC)
    INCLUDE([UnitRAMEvaluationOutputResultsEAFDistributionID], [EAF], [Probability], [CumulativeProbability]);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluationOutputResultsEAFDistribution';

