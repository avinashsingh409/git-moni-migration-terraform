--Create mapping table RAM Component ID to Vista  
CREATE TABLE [PowerRAM].[tRAMVistaMapping] (
    [RAMVistaMappingID]  INT           IDENTITY (1, 1) NOT NULL,
    [RAMModelID]         INT           NOT NULL,
    [SystemNum]          INT           NOT NULL,
    [ComponentID]        NVARCHAR (10) NOT NULL,
    [VistaSysID]         INT           NOT NULL,
    [VistaSubSysID]      INT           NOT NULL,
    [VistaCompID]        INT           NOT NULL,
    [VistaFailureModeID] INT           NOT NULL,
    CONSTRAINT [PK_RAMVistaMapping] PRIMARY KEY CLUSTERED ([RAMVistaMappingID] ASC),
    CONSTRAINT [FK_tRAMVistaMapping_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tRAMVistaMapping';

