﻿CREATE TABLE [PowerRAM].[tUnitRAMEvaluationSystemFile] (
    [UnitRAMEvaluationSystemFileID] INT           IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationPeriodKeyID]  INT           NOT NULL,
    [RAMSystemNum]                  INT           NOT NULL,
    [RawTextFile]                   VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tUnitRAMEvaluationSystemFile] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationSystemFileID] ASC),
    CONSTRAINT [FK_tUnitRAMEvaluationSystemFile_UnitRAMEvaluationPeriodKeyID_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluationSystemFile';


GO
CREATE NONCLUSTERED INDEX [IDX_RAM_SYSFILE]
    ON [PowerRAM].[tUnitRAMEvaluationSystemFile]([UnitRAMEvaluationPeriodKeyID] ASC)
    INCLUDE([UnitRAMEvaluationSystemFileID], [RAMSystemNum]);

