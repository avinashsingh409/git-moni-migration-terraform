CREATE TABLE [PowerRAM].[tUnitRAMEvaluationOutputFiles] (
    [UnitRAMEvaluationOutputFilesID] INT           IDENTITY (1, 1) NOT NULL,
    [UnitRAMEvaluationPeriodKeyID]   INT           NOT NULL,
    [RAMOutputFile]                  VARCHAR (MAX) NOT NULL,
    [RAMErrorFile]                   VARCHAR (MAX) NOT NULL,
    [RAMWarningFile]                 VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tUnitRAMEvaluationOutputFiles] PRIMARY KEY CLUSTERED ([UnitRAMEvaluationOutputFilesID] ASC),
    CONSTRAINT [FK_ttUnitRAMEvaluationOutputFiles_UnitRAMEvaluationPeriodKeyID_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tUnitRAMEvaluationOutputFiles';

