CREATE TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] (
    [MaintenancePlanTimeSliceValuesID] INT            IDENTITY (1, 1) NOT NULL,
    [MaintenancePlanSetID]             INT            NOT NULL,
    [RAMModelID]                       INT            NOT NULL,
    [UnitID]                           INT            NOT NULL,
    [UnitConfigID]                     INT            NULL,
    [PeriodNum]                        INT            NOT NULL,
    [SystemNum]                        INT            NOT NULL,
    [ComponentID]                      NVARCHAR (10)  NOT NULL,
    [ShapeName]                        NVARCHAR (20)  NULL,
    [ShapeNumber]                      NVARCHAR (10)  NULL,
    [ShapeText]                        NVARCHAR (255) NULL,
    [MTBF]                             REAL           NULL,
    [MTTR]                             REAL           NULL,
    CONSTRAINT [PK_MaintenancePlanTimeSliceValues] PRIMARY KEY CLUSTERED ([MaintenancePlanTimeSliceValuesID] ASC),
    CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_MaintenancePlanSetID_tMaintenancePlan] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]),
    CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID]),
    CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'PowerRAM', @level1type = N'TABLE', @level1name = N'tMaintenancePlanTimeSliceValues';

