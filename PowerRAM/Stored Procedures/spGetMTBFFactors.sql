﻿CREATE PROCEDURE [PowerRAM].[spGetMTBFFactors]

@ScenarioID int,
@MaintenancePlanSetID int,
@UnitID int -- if @UnitID < 1 then get all units

AS

BEGIN

-- the CQI MA results are the same for each load point, so can just pull the full load results in (loadpoint=1)

SET NOCOUNT ON;

WITH SingleFuelQuality AS 
(
    SELECT FuelSourceID,fuelqualityid, rn=ROW_NUMBER() OVER (PARTITION BY FuelSourceID ORDER BY EffectiveDate)
        FROM Fuel.tFuelQuality AS m
) 
select uc.UnitID,SingleFuelQuality.FuelQualityID, uc.UnitConfigID ,e.RAMModelID,e.SystemNum, e.ComponentID,e.RAMModelComponentID,cqi.CQIResultID,
Convert(float,1.000) as Availability,CONVERT(float,1.000) as MTBF,0 as ManyToOne,0 as OneToOne,1 as IsBase
into #tmp_MTBFFactor 
 from SingleFuelQuality JOIN Projection.tUnitConfig uc on SingleFuelQuality.FuelSourceID = uc.BaseFuelSourceID 
 AND uc.IsBaseConfig = 1 
join powerram.tRAMModel d on uc.RAMModelID = d.RAMModelID join powerram.tRAMModelComponent e on d.RAMModelID = e.RAMModelID 
JOIN Projection.tCQIResult cqi ON uc.UnitConfigID = cqi.UnitConfigID and SingleFuelQuality.FuelQualityID = cqi.FuelQualityID
where rn=1 and cqi.CQIRunTypeID = 1 and e.ShapeName = 'Component' and cqi.LoadPoint=1 and (uc.UnitID=@UnitID OR @UnitID<1)


CREATE NONCLUSTERED INDEX #tmp_index1
ON #tmp_MTBFFactor ([FuelQualityID],[unitconfigid])
INCLUDE ([RAMModelID],[SystemNum],[ComponentID]);

SELECT  Year,Month,Day,Hour,ScenarioID,UNITID,FuelQualityID, UnitConfigID into #tmp_HourlyScenarioConfig FROM projection.vHourlyScenarioConfig where ScenarioID=@ScenarioID 
and (UnitID=@UnitID OR @UnitID<1)
SELECT DISTINCT UNITID,FuelQualityID, UnitConfigID into #tmp_UniqueScenarioConfig FROM #tmp_HourlyScenarioConfig

INSERT INTO #tmp_MTBFFactor (UnitID,FuelQualityID,UnitConfigID,RamModelID,SystemNum,ComponentID,RAMModelComponentID,CQIResultID,Availability,MTBF, ManyToOne,OneToOne,IsBase)
SELECT DISTINCT a.UnitID, a.FuelQualityID,a.unitconfigid
,e.RAMModelID, e.SystemNum,e.ComponentID,E.RAMModelComponentID,cqi.CQIResultID as CQIResultID, convert(float,1.0000) as Availability, 
convert(float,1.0000) as MTBF, 0 as ManyToOne, 0 as OneToOne,
0 as IsBase
from #tmp_HourlyScenarioConfig a 
join projection.tUnitConfig c on a.UnitConfigID = c.UnitConfigID 
join projection.tCQIResult cqi on a.UnitConfigID = cqi.UnitConfigID and a.FuelQualityID = cqi.FuelQualityID and cqi.CQIRunTypeID = 1
join powerram.tRAMModel d on c.RAMModelID = d.RAMModelID join powerram.tRAMModelComponent e on d.RAMModelID = e.RAMModelID 
where 
  e.ShapeName = 'Component' and cqi.LoadPoint=1 and
  a.ScenarioID=@ScenarioID and not exists (select * from #tmp_MTBFFactor tmp where tmp.FuelQualityID=a.FuelQualityID and tmp.UnitConfigID=a.UnitConfigID); 

select COUNT(*) as cnt,mapCnt.RAMModelID,mapCnt.SystemNum,MapCnt.ComponentID into #tmp_mapcnt from PowerRAM.tRAMVistaMapping mapCnt group by mapCnt.RAMModelID,mapCnt.SystemNum,MapCnt.ComponentID
 
UPDATE #tmp_MTBFFactor SET Availability = 1.000

-- handle the many to 1 case
SELECT tbl.CQIResultID,tbl.RAMModelID,tbl.SystemNum,tbl.ComponentID, 
case when ma.MeanTimeBetweenFailures > 0 then (ma.MeanTimeBetweenFailures*8760)/(ma.MeanTimeBetweenFailures*8760 + ma.CTTR_Hours) else 1 end AS Availability 
INTO #tmp_many_to_many
from #tmp_MTBFFactor tbl 
JOIN powerram.tRAMVistaMapping map ON tbl.RAMModelID = map.RAMModelID and tbl.SystemNum = map.SystemNum and tbl.ComponentID = map.ComponentID
JOIN Projection.tCQIResult cqi on tbl.FuelQualityID = cqi.FuelQualityID and tbl.UnitConfigID = cqi.UnitConfigID and cqi.CQIRunTypeID=1
JOIN Projection.tCQIResultMAFailMode ma on cqi.CQIResultID = ma.CQIResultID and map.VistaSysID = ma.SystemID and map.VistaSubSysID = ma.SubSysID 
and map.VistaCompID = ma.CompID
and map.VistaFailureModeID = ma.FailureModeID 
JOIN #tmp_mapcnt mapCnt ON map.RAMModelID=mapcnt.RAMModelID and map.SystemNum = mapCnt.SystemNum and map.ComponentID = mapcnt.ComponentID
WHERE cnt>1 and cqi.LoadPoint=1 and tbl.CQIResultID>0

-- t-sql doesn't have a PRODUCT aggregation, so use logs to accomplish the same thing

select CQIResultID, RAMModelID,SystemNum,ComponentID,
 Exp(Sum(
case when Abs(Availability)=0 then 0 else Log(Abs(Availability)) end
))*
case when Min(Abs(Availability))=0 then 0 else 1 end
*(1-2*(Sum(
case when Availability>=0 then 0 else 1 end
) % 2)) AS Product
into #tmp_sum_producct
 from 
#tmp_many_to_many group by CQIResultID, RAMModelID,SystemNum,ComponentID

UPDATE tbl set Availability = tmp.Product,ManyToOne=1 FROM #tmp_MTBFFactor tbl JOIN #tmp_sum_producct tmp ON tbl.CQIResultID=tmp.CQIResultID and tbl.RAMModelID=tmp.RAMModelID
and tbl.SystemNum=tmp.SystemNum and tbl.ComponentID=tmp.ComponentID

-- handle the 1 to 1 case
update tbl set tbl.MTBF = ma.MeanTimeBetweenFailures,tbl.OneToOne = 1, tbl.CQIResultID = cqi.CQIResultID 
from #tmp_MTBFFactor tbl 
JOIN powerram.tRAMVistaMapping map ON tbl.RAMModelID = map.RAMModelID and tbl.SystemNum = map.SystemNum and tbl.ComponentID = map.ComponentID
JOIN Projection.tCQIResult cqi on tbl.FuelQualityID = cqi.FuelQualityID and tbl.UnitConfigID = cqi.UnitConfigID and cqi.CQIRunTypeID=1
JOIN Projection.tCQIResultMAFailMode ma on cqi.CQIResultID = ma.CQIResultID and map.VistaSysID = ma.SystemID and map.VistaSubSysID = ma.SubSysID 
and map.VistaCompID = ma.CompID
and map.VistaFailureModeID = ma.FailureModeID 
JOIN #tmp_mapcnt mapCnt ON map.RAMModelID=mapcnt.RAMModelID and map.SystemNum = mapCnt.SystemNum and map.ComponentID = mapcnt.ComponentID
WHERE cnt=1 and cqi.LoadPoint=1

delete from #tmp_MTBFFactor where (not (ManyToOne=1 or OneToOne=1)) or (((MTBF=1 or MTBF<0) and OneToOne=1) or ((Availability=1 or Availability < 0) and OneToOne=0))

CREATE NONCLUSTERED INDEX #tmp_index3
ON [dbo].[#tmp_MTBFFactor] ([RAMModelComponentID])
INCLUDE ([FuelQualityID],[UnitConfigID],[Availability],[MTBF])

CREATE INDEX #tmp_HourlyScenarioConfig ON #tmp_HourlyScenarioConfig (ScenarioID,UnitID,Year,Month,Day,Hour)
CREATE INDEX #tmp_HourlyScenarioConfig2 ON #tmp_HourlyScenarioConfig (UnitConfigID,FuelQualityID)

-- create a temporary table to hold the hourly mtbf and availability values for each ram model component
select a.UnitID,c.UnitConfigID, a.Year,a.Month,a.Day,a.Hour,c.RAMModelComponentID,c.Availability,c.MTBF,b.Generation_mw as NetGeneration_mwh,OneToOne,b.EffectiveDate
into #tmp_results1
 from projection.tScenario scn
 JOIN Projection.tGenerationDetail_Adjusted b on isnull(scn.demandsetid,scn.GenerationSetID) = b.GenerationSetID
 JOIN #tmp_HourlyScenarioConfig a ON scn.ScenarioID = a.ScenarioID and b.UnitID = a.UnitID and b.Year=a.year and b.Month=a.month and b.Day=a.day and b.Hour=a.Hour
JOIN #tmp_MTBFFactor c on a.UnitConfigID = c.UnitConfigID and a.FuelQualityID = c.FuelQualityID
where Scn.ScenarioID=@ScenarioID 

select a.UnitID, a.Duration_days,a.StartDate,DATEADD(DAY,a.Duration_days-1,a.StartDate) as enddate into #tmp_outages from  powerram.tMaintenanceOutage a 
 where a.MaintenancePlanSetID = @MaintenancePlanSetID
 
-- take into account outages
update tbl set NetGeneration_mwh=0 
from #tmp_results1 tbl JOIN
#tmp_outages b on tbl.UnitID = b.UnitID where 
 tbl.EffectiveDate between b.StartDate and b.enddate

-- create a temporary table to hold the base fuel quality by unit
select distinct a.UnitConfigID,a.UnitID,c.FuelQualityID into #tmp_basefuelqualities from Projection.tUnitConfig a 
 join fuel.tFuelSource b on a.BaseFuelSourceID = b.FuelSourceID 
 join fuel.tFuelQuality c on b.FuelSourceID = c.FuelSourceID and c.FuelQualityID = (SELECT TOP 1 fq2.FuelQualityID FROM Fuel.tFuelQuality fq2 where 
fq2.FuelSourceID=b.FuelSourceID ORDER BY fq2.EffectiveDate DESC)

-- create a temporary table to hold the availability and mtbf values by unit and ram model component
select b.UnitID,c.RAMModelComponentID,c.Availability,c.MTBF
into #tmp_results2
 from #tmp_basefuelqualities b 
JOIN #tmp_MTBFFactor c on c.UnitConfigID = b.UnitConfigID and c.FuelQualityID = b.FuelQualityID

SELECT @ScenarioID as scenarioid, a.UnitID,a.UnitConfigID,a.Year,a.Month,a.RAMModelComponentID,
case when SUM(a.NetGeneration_mwh) <>0 then SUM(a.NetGeneration_mwh * a.CQFactor)/SUM(a.NetGeneration_mwh) else 1 end as MonthlyCQFactor
into #tmp_mtbfresults from (
select a.UnitID,a.UnitConfigID, a.Year,a.Month,a.Day,a.Hour,a.RAMModelComponentID,
case when OneToOne=1 then case when b.MTBF>0 and a.MTBF>0 then a.MTBF/b.MTBF else 1 end else -- for onetoone, ratio is comparison_coal/base
-- for manytoone, ratio is base/comparison_coal
case when b.Availability>0 and a.Availability>0 and (1-a.Availability<>0) then (1-b.Availability)/(1-a.Availability) else 1 end end as CQFactor,a.NetGeneration_mwh  from 
#tmp_results1 a
JOIN
#tmp_results2 b 
ON a.UnitID = b.UnitID and a.RAMModelComponentID = b.RAMModelComponentID
) a 
GROUP BY a.UnitID,a.UnitConfigID,a.Year,a.Month,a.RAMModelComponentID

select * from #tmp_mtbfresults where MonthlyCQFactor<>1

END