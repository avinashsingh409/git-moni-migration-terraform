﻿CREATE PROC [Economic].[UpdateUnitCostField]
    @scenarioid int,
    @unitPerfTable as varchar(50),
    @unitCostTable as varchar(50),
	@costfield varchar(50),
	@category varchar(50),
	@fcstfield varchar(50),	
	@qtyfield varchar(50),
	@isUnitLevel bit = 1,
	@forWeekdayPeak bit = 1,
	@forWeekdayOffPeak bit = 1,
	@forWeekendPeak bit = 1,
	@forWeekendOffPeak bit = 1,	
	@disposalField varchar(50) = null,
	@sellPercentField varchar(50) = null,
	@moisturePercentField varchar(50) = null,
	@isSaleValue bit = 1	
	AS
begin

SET NOCOUNT ON

DECLARE @tmptable as varchar(50)
select @tmptable = '#tmp_table1'

declare @sql as varchar(max)

create table #tmp_table1
(
ID int NOT NULL IDENTITY (1, 1), StationGroupID int, StationID int, UnitID int, BlendFuelSourceID int NULL, FuelSourceID int, PCT real, Year int, Month int, value real null
)

ALTER TABLE #tmp_table1 ADD CONSTRAINT
	PK_tmp_table1 PRIMARY KEY CLUSTERED 
	(
	ID
	)
	
if @costfield='Fuel'
  begin
  CREATE NONCLUSTERED INDEX #tmp_table1_idx1 ON #tmp_table1 ([FuelSourceID],[Year],[Month]) INCLUDE ([StationID],[value])
  end
else
  begin  
  CREATE NONCLUSTERED INDEX #tmp_table1_idx2 ON #tmp_table1 (unitid,year,month) INCLUDE ([value])
  end
  
if @costfield<>'Fuel'
	begin
			
	if @isUnitLevel=1
	  begin	  
	  set @sql = 'insert into #tmp_table1 (StationGroupID,StationID,UnitID,FuelSourceID,Pct,Year,Month,Value) '
	  set @sql = @sql + ' select distinct 0 as StationGroupID, 0 as StationID,UnitID,0 as FuelSourceID,0.000 as pct,YEAR,MONTH,CONVERT(float,null) as value from ' + @unitPerfTable +' where '
	  set @sql = @sql + ' scenarioid=' + convert(varchar(50),@scenarioid)
	  exec (@sql)
	
	  set @sql = 'update ' + @tmptable + ' set value = a.value from (select c.unitid,a.Year,a.Month,a.Category,a.Value from Economic.tForecastDetail a join projection.tScenario b on a.ForecastSetID=b.ForecastSetID '
	  set @sql = @sql + ' join asset.tUnit c on a.Variable = c.' + @fcstfield + ' where b.ScenarioID=' + convert(varchar(20),@scenarioid) + ' and a.Category = ''' + @category + ''') a'
	  set @sql = @sql + ' where ' + @tmptable + ' .UnitID=a.UnitID and ' + @tmptable + ' .Year=a.Year and ' + @tmptable + ' .Month=a.Month '
	  exec (@sql)
	  end
	else
	  begin
	  set @sql = 'insert into #tmp_table1 (StationGroupID,StationID,UnitID,FuelSourceID,Pct,Year,Month,Value) '
	  set @sql = @sql + ' select distinct c.StationGroupID, 0 as StationID,0 as UnitID,0 as FuelSourceID,0.000 as pct,YEAR,MONTH,CONVERT(float,null) as value from ' + @unitPerfTable + ' a ' 
	  set @sql = @sql + ' join asset.tUnit b on a.UnitID=b.UnitID join asset.tStation c on b.StationID=c.stationid where scenarioid=' + convert(varchar(50),@scenarioid)
	  exec (@sql)	  	    	    
	
	  set @sql = 'update ' + @tmptable + ' set value = a.value from (select c.stationgroupid,a.Year,a.Month,a.Category,a.Value from Economic.tForecastDetail a join projection.tScenario b on a.ForecastSetID=b.ForecastSetID '
	  set @sql = @sql + ' join asset.tStationGroup c on a.Variable = c.' + @fcstfield + ' where b.ScenarioID=' + convert(varchar(20),@scenarioid) + ' and a.Category = ''' + @category + ''') a'
	  set @sql = @sql + ' where ' + @tmptable + ' .StationGroupID=a.StationGroupID and ' + @tmptable + ' .Year=a.Year and ' + @tmptable + ' .Month=a.Month '
	  exec (@sql)
	  end
	end
else
	begin	
	
	SELECT DISTINCT FuelSourceID INTO #tmp_Fuels FROM [Projection].tScenarioUnitPerformance WHERE ScenarioID=@scenarioid
	
	create table #tmp_months
			(
			ID int NOT NULL IDENTITY (1, 1), StationID int, Year int, Month int
			)


	set @sql = 'insert into #tmp_months (StationID,Year,Month) select distinct b.StationID, a.YEAR,a.MONTH FROM ' + @unitPerfTable + ' a JOIN Asset.tUnit b on a.unitid=b.unitid '
	set @sql = @sql + ' WHERE ScenarioID=' + CONVERT(varchar(50),@scenarioid)
	exec (@sql)
	
	set @sql = 'insert into #tmp_table1 (StationGroupID, StationID,UnitID,FuelSourceID,Pct,Year,Month,Value)'
	set @sql = @sql + ' select distinct 0 as StationGroupID, a.StationID,0 as UnitID, c.FuelSourceID,1.000 as pct, a.YEAR,a.MONTH,CONVERT(float,null) as value from #tmp_months a '	
	set @sql = @sql + ' CROSS JOIN #tmp_fuels c'	
		
	exec (@sql)        

	set @sql = 'insert into #tmp_table1 (StationGroupID,StationID,UnitID,BlendFuelSourceID, FuelSourceID,pct,Year,Month) '
	set @sql = @sql + ' select distinct 0 as StationGroupID, a.StationID,0 as UnitID,c.BlendFuelSourceID, c.FuelSourceID,c.ParentMass_fraction as pct, a.YEAR,a.MONTH'
	set @sql = @sql + ' from #tmp_months a'	
	set @sql = @sql + ' CROSS JOIN ('
	set @sql = @sql + ' SELECT DISTINCT b.FuelSourceID as BlendFuelSourceID, b.ParentSourceID as FuelSourceID,b.ParentMass_fraction FROM ' + @unitPerfTable + ' a '
	set @sql = @sql + ' JOIN Fuel.tFuelBlendDefinition b on a.FuelSourceID = b.FuelSourceID'
	set @sql = @sql + ' WHERE ScenarioID=' + CONVERT(varchar(50),@scenarioid) + ') c'    
            
    exec (@sql)
            
    update #tmp_table1 set value = a.value from (
	select a.StationID,a.FuelSourceID,b.Year,b.Month,b.Category,b.Value from economic.tFuelSourceStationForecastMap a join Economic.tForecastDetail b on a.PriceFcstVar = b.Variable join 
	projection.tScenario c on b.ForecastSetID = c.ForecastSetID
	 where c.ScenarioID=@scenarioid and b.Category = @category) a
	 where #tmp_table1.StationID=a.StationID and #tmp_table1.FuelSourceID=a.FuelSourceID and #tmp_table1.Year=a.Year and #tmp_table1.Month=a.Month 	    
	end

if @costfield = 'Fuel'
  begin
  -- update fuel price for blends summing the proportionate prices of the constituent fuels
  --update #tmp_table1 set value = a.value from (
  -- select a.StationID,a.Year,a.Month,a.FuelSourceID,sum(c.value*b.ParentMass_fraction) as value from #tmp_table1 a join fuel.tFuelBlendDefinition b on a.FuelSourceID = b.FuelSourceID join 
  -- #tmp_table1 c on b.ParentSourceID = c.FuelSourceID and c.StationID = a.StationID and c.Year=a.Year and c.Month=a.month
  -- group by a.StationID,a.Year,a.Month,a.FuelSourceID) a where a.FuelSourceID = #tmp_table1.FuelSourceID and a.StationID=#tmp_table1.StationID and 
  -- a.Year=#tmp_table1.Year and a.Month=#tmp_table1.month  
  
  update #tmp_table1 set value = a.value from (
   select a.StationID,a.Year,a.Month,a.blendfuelsourceid,SUM(a.value*a.pct) as value from #tmp_table1 a where 
   a.blendfuelsourceid is not null group by a.StationID,a.Year,a.Month,a.blendfuelsourceid) a where 
   #tmp_table1.stationid=a.stationid and #tmp_table1.year=a.year and #tmp_table1.month=a.month and 
   #tmp_table1.fuelsourceid=a.blendfuelsourceid
   
  -- all fuel prices should be accounted for and represented by a record that has a pct of 100%, so delete the rest
  delete from #tmp_table1 where PCT<1
  end
  
declare @fuelsourceid as int
declare @stationGroupId as int
declare @stationid as int
declare @unitid as int
declare @year as int
declare @month as int
declare @value as float

declare @laststationgroupid as int
set @laststationgroupid = -1

declare @laststationid as int
set @laststationid = -1

declare @lastunitid as int
set @lastunitid = -1

declare @lastfuelsourceid as int
set @lastfuelsourceid = -1

declare @lastvalue as float
set @lastvalue = null
  
PRINT @costfield

--SELECT * INTO tmp_bkb FROM #tmp_table1

--SELECT * FROM #tmp_table1

declare @cnt as int

-- check to see if any work needs to be done to forward fill prices
if @costfield='Fuel'
  begin
  select @cnt = COUNT(*) from #tmp_table1 where value is null and exists (select * from #tmp_table1 a where a.stationid=#tmp_table1.stationid and a.year=#tmp_table1.year and a.month=#tmp_table1.month
   and a.fuelsourceid=#tmp_table1.fuelsourceid and value is not null)
  end
else
  begin
  select @cnt = COUNT(*) from #tmp_table1 where value is not null and value<>0
  end
   
if @cnt > 0
  begin
	DECLARE myCursor CURSOR LOCAL FOR select FuelSourceID,StationGroupID,StationID,UnitID,Year,Month,Value From #tmp_table1
	 order by StationGroupID,StationID,UnitID,FuelSourceID,YEAR,MONTH
	  FOR UPDATE OF VALUE
	OPEN myCursor;
	FETCH NEXT FROM myCursor INTO @FuelSourceID,@stationGroupId,@stationid,@unitid,@year,@month,@value
	WHILE @@FETCH_STATUS =  0
		BEGIN
		if (@laststationgroupid=@stationGroupId and @laststationid = @stationid and @lastfuelsourceid=@fuelsourceid and @costfield = 'Fuel') or 
		   (@lastunitid=@unitid and @isUnitLevel=1 and @costfield<>'Fuel') or 
		   (@laststationgroupid=@stationGroupId and @isUnitLevel=0 and @costfield<>'Fuel')
		  begin
		  if @value is null and @lastvalue is not null -- set to last non-null value for this unitid
			begin	    
			print @stationid
			print @fuelsourceid
			print @year
			print @month
			update #tmp_table1 set value = @lastvalue where CURRENT of myCursor	    	    
			end
		  end
		else
		  begin
		  set @lastvalue = null
		  end
		set @lastunitid = @unitid
		set @laststationid = @stationid
		set @laststationgroupid = @stationGroupId
		set @lastfuelsourceid = @fuelsourceid
		if @value is not null
		  begin
		  set @lastvalue = @value
		  end
		FETCH NEXT FROM myCursor INTO @FuelSourceID,@stationGroupId,@stationid,@unitid,@year,@month,@value
		END
	CLOSE myCursor;
	DEALLOCATE myCursor;
	end

set @sql = 'UPDATE ' + @unitCostTable + ' set ' + @costfield + ' = ' + @qtyfield + ' * b.value  from  ' + @unitPerfTable + '  a, '
set @sql = @sql + ' ' + @tmptable + ' b, Asset.tUnit u,Asset.tStation s where '
set @sql = @sql + ' a.UnitID=u.UnitID and u.StationID=s.StationID and '
set @sql = @sql + '  ' + @unitCostTable + ' .unitid = a.UnitID and  ' + @unitCostTable + ' .scenarioid=a.ScenarioID and '
set @sql = @sql + '  ' + @unitCostTable + ' .year=a.Year and  ' + @unitCostTable + ' .month=a.Month and  ' + @unitCostTable + ' .day = a.Day '
set @sql = @sql + ' and  ' + @unitCostTable + ' .hour=a.hour and '
if @costfield = 'Fuel'
  begin
  set @sql = @sql + ' u.StationID = b.StationID and a.FuelSourceID = b.FuelSourceID '
  end
else if @isUnitLevel=1
  begin
  set @sql = @sql + ' a.UnitID=b.UnitID '
  end
else
  begin
  set @sql = @sql + ' s.StationGroupID=b.StationGroupID '
  end
  
set @sql = @sql + ' and a.Year=b.Year and a.Month=b.Month and  ' + @unitCostTable + ' .scenarioid = ' + CONVERT(varchar(20),@scenarioid)

if @forWeekdayPeak<>1 or @forWeekendOffPeak<>1 or @forWeekendPeak<>1 or @forWeekendOffPeak<>1
  begin
	set @sql = @sql + ' AND ((1<>1) ' -- do this so that the next boolean clauses are easier to append to the sql
	if @forWeekdayPeak=1
	  begin
	  set @sql = @sql + ' OR (datepart(dw,base.dateserial( ' + @unitCostTable + ' .YEAR, ' + @unitCostTable + ' .MONTH, ' + @unitCostTable + ' .DAY, ' + @unitCostTable + ' .Hour,0,0)) between 2 and 6 and  ' + @unitCostTable + ' .Hour between 7 and 21) '
	  end

	if @forWeekdayOffPeak=1
	  begin
	  set @sql = @sql + ' OR (datepart(dw,base.dateserial( ' + @unitCostTable + ' .YEAR, ' + @unitCostTable + ' .MONTH, ' + @unitCostTable + ' .DAY, ' + @unitCostTable + ' .Hour,0,0)) between 2 and 6 and not  ' + @unitCostTable + ' .Hour between 7 and 21) '
	  end
	    

	if @forWeekendPeak=1
	  begin
	  set @sql = @sql + ' OR (not datepart(dw,base.dateserial( ' + @unitCostTable + ' .YEAR, ' + @unitCostTable + ' .MONTH, ' + @unitCostTable + ' .DAY, ' + @unitCostTable + ' .Hour,0,0)) between 2 and 6 and  ' + @unitCostTable + ' .Hour between 7 and 21) '
	  end
	  
	if @forWeekendOffPeak=1
	  begin
	  set @sql = @sql + ' OR (not datepart(dw,base.dateserial( ' + @unitCostTable + ' .YEAR, ' + @unitCostTable + ' .MONTH, ' + @unitCostTable + ' .DAY, ' + @unitCostTable + ' .Hour,0,0)) between 2 and 6 and not  ' + @unitCostTable + ' .Hour between 7 and 21) '
	  end
	  
	set @sql = @sql + ')'
	end

exec (@sql)

set @sql = 'drop table ' + @tmptable
exec (@sql)

end