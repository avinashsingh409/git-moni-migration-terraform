﻿CREATE PROC Economic.UpdateUnitCosts
    @scenarioid int,
    @unitPerfTable varchar(50) = 'Projection.tScenarioUnitPerformance',
    @unitCostTable varchar(50) = 'Projection.tScenarioUnitCost'
AS
BEGIN

declare @sql as varchar(4000)
	
set @sql = 'delete from ' + @unitCostTable + ' where scenarioid = ' + convert(varchar(50),@scenarioid)
exec (@sql)

set @sql = ' insert into ' + @unitCostTable + ' (ScenarioID,UnitID,Year,Month,Day,Hour)'
set @sql = @sql + ' SELECT scenarioid,unitid,YEAR,MONTH,DAY,hour from ' + @unitPerfTable + '  '
set @sql = @sql + ' where scenarioid = ' + convert(varchar(50),@scenarioid)
exec (@sql)

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Energy','Energy, Weekday Peak, $/MWh','EnergyWeekdayOnPeakFcstVar','NetGeneration_mwh',1,1,0,0,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Energy','Energy, Weekday Offpeak, $/MWh','EnergyWeekdayOffPeakFcstVar','NetGeneration_mwh',1,0,1,0,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Energy','Energy, Weekend Peak, $/MWh','EnergyWeekendOnPeakFcstVar','NetGeneration_mwh',1,0,0,1,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Energy','Energy, Weekend Offpeak, $/MWh','EnergyWeekendOffPeakFcstVar','NetGeneration_mwh',1,0,0,0,1

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Demand','Energy, Weekday Peak, $/MWh','EnergyWeekdayOnPeakFcstVar','Demand_mwh',1,1,0,0,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Demand','Energy, Weekday Offpeak, $/MWh','EnergyWeekdayOffPeakFcstVar','Demand_mwh',1,0,1,0,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Demand','Energy, Weekend Peak, $/MWh','EnergyWeekendOnPeakFcstVar','Demand_mwh',1,0,0,1,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Demand','Energy, Weekend Offpeak, $/MWh','EnergyWeekendOffPeakFcstVar','Demand_mwh',1,0,0,0,1

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'DeratedEnergy','Energy, Weekday Peak, $/MWh','EnergyWeekdayOnPeakFcstVar','DeratedEnergy_mwh',1,1,0,0,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'DeratedEnergy','Energy, Weekday Offpeak, $/MWh','EnergyWeekdayOffPeakFcstVar','DeratedEnergy_mwh',1,0,1,0,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'DeratedEnergy','Energy, Weekend Peak, $/MWh','EnergyWeekendOnPeakFcstVar','DeratedEnergy_mwh',1,0,0,1,0
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'DeratedEnergy','Energy, Weekend Offpeak, $/MWh','EnergyWeekendOffPeakFcstVar','DeratedEnergy_mwh',1,0,0,0,1

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'SaleableBottomAsh','Bottom Ash Re-Use, $/ton','BottomAshSalesFcstVar','CalcBottomAshSale_ton'
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'NonSaleableBottomAsh','Bottom Ash Disposal, $/ton','BottomAshDisposalFcstVar','CalcBottomAshDisposal_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'SaleableFlyAsh','Fly Ash Re-Use, $/ton','FlyAshSalesFcstVar','CalcFlyAshSale_ton'
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'NonSaleableFlyAsh','Fly Ash Disposal, $/ton','FlyAshDisposalFcstVar','CalcFlyAshDisposal_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Gypsum','Gypsum Re-Use, $/ton','GypsumSalesFcstVar','CalcGypsymSale_ton'
exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDWaste','FGD Sludge Disposal, $/ton','ScrubberFcstVar','CalcScrubberWaste_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDAsh','FGD Sludge Disposal, $/ton','FGDAshFcstVar','CalcFGDAsh_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDAdditive','FGD Reagent, $/ton','FGDReagentFcstVar','FGDAdditive_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDFixative','FGD Fixative, $/ton','FGDFixativeFcstVar','FGDFixative_tonhr'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDWater','FGD Water, $/kgal','FGDWaterFcstVar','FGDWater_gallon/1000.0'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDReagent','SCR/SNCR Reagent, $/ton','SCR_SNCRFcstVar','(SCRReagent_ton+SNCRReagent_ton)'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'MercuryAdditive','Mercury Additive (PAC), $/ton','MercuryFcstVar','HGAdditive_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'CoalAdditive','Coal Additive, $/ton','CoalAddFcstVar','CoalAdditive_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'CFBBedReagent','CFB Bed Reagent, $/ton','CFBBedReagentFcstVar','CFBBedReagent_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'NH3Feedstock','NH3 Additive, $/ton','NH3AddFcstVar','NH3Feedstock_ton'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'FGDDBAAdditive','FGD DBA Additive, $/gal','FGDDBAAddFcstVar','FGDDBAAdditive_gallon'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'Fuel','Delivered coal Price, $/ton','','FuelBurnRate_tonhr'

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'SO2Allowance','SO2 Allowance Price, $/ton','SO2AllowanceFcstVar','SO2_ton',0

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'CO2Allowance','CO2 Allowance Price, $/ton','CO2AllowanceFcstVar','CO2_ton',0

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'NOXAllowance','NOX Allowance Price, $/ton','NOXAllowanceFcstVar','NOX_ton',0

exec Economic.UpdateUnitCostField @scenarioid,@unitPerfTable,@unitCostTable,'HgAllowance','Mercury Allowance Price, $/lbm','MercuryAllowanceFcstVar','Hg_lbm',0

END