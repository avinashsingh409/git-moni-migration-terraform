CREATE TABLE [Economic].[tForecastDetail] (
    [ForecastDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [ForecastSetID]    INT            NOT NULL,
    [Category]         NVARCHAR (255) NOT NULL,
    [SubCategory]      NVARCHAR (255) NULL,
    [Variable]         NVARCHAR (255) NOT NULL,
    [Type]             NVARCHAR (255) NULL,
    [Year]             SMALLINT       NOT NULL,
    [Month]            TINYINT        NOT NULL,
    [Value]            FLOAT (53)     NOT NULL,
    CONSTRAINT [PK_tForecastDetail] PRIMARY KEY CLUSTERED ([ForecastDetailID] ASC),
    CONSTRAINT [FK_tForecastDetail_ForecastSetID_tForecastSet] FOREIGN KEY ([ForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID])
);






GO
CREATE NONCLUSTERED INDEX IDX_Economic_tForecastDetail_1
ON [Economic].[tForecastDetail] ([ForecastSetID],[Category],[Year])
INCLUDE ([Variable],[Month])
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Economic', @level1type = N'TABLE', @level1name = N'tForecastDetail';

