CREATE TABLE [Economic].[tForecastSet] (
    [ForecastSetID]     INT            IDENTITY (1, 1) NOT NULL,
    [ForecastSetDesc]   NVARCHAR (255) NOT NULL,
    [CreatedBy]         NVARCHAR (255) NOT NULL,
    [ChangedBy]         NVARCHAR (255) NOT NULL,
    [CreateDate]        DATETIME       DEFAULT (getdate()) NOT NULL,
    [ChangeDate]        DATETIME       DEFAULT (getdate()) NOT NULL,
    [ImportDate]        DATETIME       NULL,
    [IsConsensus]       BIT            DEFAULT ((0)) NOT NULL,
    [IsPublic]          BIT            CONSTRAINT [DF_tForecastSet_IsPublic] DEFAULT ((0)) NOT NULL,
    [EffectiveYear]     INT            NOT NULL,
    [IsArchived]        BIT            CONSTRAINT [DF_tForecastSet_IsArchived] DEFAULT ((0)) NOT NULL,
    [ForecastSetAbbrev] NVARCHAR (50)  CONSTRAINT [ForecastSetAbbrevDefault] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_ForecastSet] PRIMARY KEY CLUSTERED ([ForecastSetID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Economic', @level1type = N'TABLE', @level1name = N'tForecastSet';

