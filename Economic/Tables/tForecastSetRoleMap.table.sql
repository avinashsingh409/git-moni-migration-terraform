CREATE TABLE [Economic].[tForecastSetRoleMap] (
    [ForecastSetID]  INT NOT NULL,
    [SecurityRoleID] INT NOT NULL,
    CONSTRAINT [PK_tForecastSetRoleMap] PRIMARY KEY CLUSTERED ([ForecastSetID] ASC, [SecurityRoleID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Economic', @level1type = N'TABLE', @level1name = N'tForecastSetRoleMap';

