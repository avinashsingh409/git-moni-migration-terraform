CREATE TABLE [Economic].[tFuelSourceStationForecastMap] (
    [SrcStaFcstMapID] INT            IDENTITY (1, 1) NOT NULL,
    [FuelSourceID]    INT            NOT NULL,
    [StationID]       INT            NOT NULL,
    [PriceFcstSubCat] NVARCHAR (255) NULL,
    [PriceFcstVar]    NVARCHAR (255) NULL,
    [CreatedBy]       NVARCHAR (255) NOT NULL,
    [ChangedBy]       NVARCHAR (255) NOT NULL,
    [CreateDate]      DATETIME       CONSTRAINT [DF__tSrcStaFcstMap__Creat__5E1FF51F] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tSrcStaFcstMap] PRIMARY KEY NONCLUSTERED ([SrcStaFcstMapID] ASC),
    CONSTRAINT [FK_tSrcStaFcstMap_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tSrcStaFcstMap_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mapping of price forecasts to a fuel source-station combination.', @level0type = N'SCHEMA', @level0name = N'Economic', @level1type = N'TABLE', @level1name = N'tFuelSourceStationForecastMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Economic', @level1type = N'TABLE', @level1name = N'tFuelSourceStationForecastMap';

