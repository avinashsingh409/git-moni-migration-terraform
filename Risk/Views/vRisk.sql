﻿
CREATE VIEW [Risk].[vRisk]
AS 

SELECT r.RiskID
      ,r.OwningAssetID AssetID
	  ,ast.AssetDesc
	  ,ast.AssetAbbrev 
	  ,act.AssetClassTypeAbbrev
	  ,act.AssetClassTypeDesc
	  ,asstype.AssetTypeAbbrev
	  ,asstype.AssetTypeDesc 
      ,r.RiskAcceptabilityTypeID
	  ,ra.Description RiskAcceptabilityTypeDesc
	  ,ra.Value RiskAcceptabilityTypeValue
	  ,r.RiskConfidenceTypeID
	  ,rc.Description RiskConfidenceTypeDesc
	  ,rc.Value RiskConfidenceTypeValue
	  ,r.RiskNatureTypeID
	  ,rn.PrimaryNature RiskNaturePrimaryDesc
	  ,rn.SubNature RiskNatureSubDesc
	  ,rn.PrimaryNature + ': ' + rn.SubNature RiskNatureDesc
	  ,rn.OwningAssetID RiskNatureOwningAssetID
	  ,r.RiskStageTypeID
	  ,rs.Description RiskStageTypeDesc
	  ,r.RiskScoreCalcTypeID
	  ,rsc.Description RiskScoreCalcTypeDesc
      --,r.[Priority]
      --,r.[Action]
      ,r.[CreatedDate]
      ,r.[ChangedDate]
      ,r.[CreatedByID]
      ,r.[ChangedByID]
      ,r.Cause
      ,r.ControlAdequacy
      ,r.Description RiskDescription
      ,r.Title
	  ,r.ExistingControl
	  ,createdByUserData.Username as CreatedByName
	  ,createdByUserData.Email as CreatedByEmail
	  ,changedByUserData.Username as ChangedByName
	  ,changedByUserData.Email as ChangedByEmail
	  ,CASE WHEN r.OpenedDate IS NULL AND r.ClosedDate IS NULL THEN DATEDIFF(second,r.CreatedDate,GETDATE())
	        WHEN r.OpenedDate IS NOT NULL AND r.ClosedDate IS NULL THEN DATEDIFF(second,r.OpenedDate,GETDATE())
			WHEN r.OpenedDate IS NULL AND r.ClosedDate IS NOT NULL THEN DATEDIFF(second,r.CreatedDate,r.ClosedDate)
			ELSE DATEDIFF(second,r.OpenedDate,r.ClosedDate) END AS OpenDuration
	  ,r.OpenedDate
	  ,r.ClosedDate
	  ,vrc.ConsequenceScores
	  ,vrc.ConsequenceAssumptions
	  ,vrl.LikelihoodScore
	  ,vrl.LikelihoodAssumptions
	FROM Risk.tRisk r 
LEFT JOIN Risk.tRiskAcceptabilityType ra
	ON ra.RiskAcceptabilityTypeID = r.RiskAcceptabilityTypeID
LEFT JOIN Risk.tRiskConfidenceType rc
	ON rc.RiskConfidenceTypeID = r.RiskConfidenceTypeID
LEFT JOIN Risk.tRiskNatureType rn
	ON rn.RiskNatureTypeID = r.RiskNatureTypeID
LEFT JOIN Risk.tRiskStageType rs
	ON rs.RiskStageTypeID = r.RiskStageTypeID
LEFT JOIN Risk.tRiskScoreCalcType rsc
	ON rsc.RiskScoreCalcTypeID = r.RiskScoreCalcTypeID
LEFT JOIN Risk.vRiskConsequencesConcatenated vrc
	on vrc.RiskID = r.RiskID
LEFT JOIN Risk.vRiskLikelihood vrl
	on vrl.RiskID = r.RiskID
LEFT JOIN AccessControl.tUser createdByUserData
	ON r.CreatedByID = createdByUserData.SecurityUserID
LEFT JOIN AccessControl.tUser changedByUserData
	ON r.ChangedByID = changedByUserData.SecurityUserID
LEFT JOIN Asset.tAsset ast
	ON r.OwningAssetID = ast.AssetID 
LEFT JOIN Asset.tAssetClassType act 
	ON ast.AssetClassTypeID = act.AssetClassTypeID 
LEFT JOIN Asset.tAssetType asstype
	ON asstype.AssetTypeID = act.AssetTypeID 

