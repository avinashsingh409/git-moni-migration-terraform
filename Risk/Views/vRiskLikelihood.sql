﻿CREATE VIEW [Risk].[vRiskLikelihood]
AS 
SELECT RiskID, rdst.Value as LikelihoodScore, rd.Assumptions as LikelihoodAssumptions
FROM Risk.tRiskDetail rd
inner join risk.tRiskDetailCategory rdc on rd.RiskDetailCategoryID = rdc.RiskDetailCategoryID
INNER JOIN Risk.tRiskDetailType rdt 	on rdc.RiskDetailTypeID = rdt.RiskDetailTypeID
inner join risk.tRiskDetailScoreType rdst on rdst.RiskDetailScoreTypeID = rdc.RiskDetailScoreTypeID
WHERE rdt.Description = 'Likelihood'