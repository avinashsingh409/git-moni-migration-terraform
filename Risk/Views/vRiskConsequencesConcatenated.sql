﻿CREATE VIEW [Risk].[vRiskConsequencesConcatenated]
AS 
SELECT RiskID
  ,
	STUFF
	(
		(
			SELECT ', ' + Category + '[' + CAST(RiskDetailScoreTypeID AS VARCHAR(3)) + ']'
			FROM Risk.tRiskDetail rd2 join Risk.tRiskDetailCategory rdc2 on rd2.RiskDetailCategoryID = rdc2.RiskDetailCategoryID 
			WHERE (RiskID = rd.RiskID AND RiskDetailTypeID = 2) 
			ORDER BY rd2.RiskDetailCategoryID
			FOR XML PATH(''),TYPE
		).value('(./text())[1]','VARCHAR(MAX)')
  ,1,2,''
  ) AS ConsequenceScores
  ,
	STUFF((
	SELECT ', ' + Assumptions
	FROM Risk.tRiskDetail rd3 join Risk.tRiskDetailCategory rdc3 on rd3.RiskDetailCategoryID = rdc3.RiskDetailCategoryID
	WHERE (RiskID = rd.RiskID AND RiskDetailTypeID = 2) 
	ORDER BY rd3.RiskDetailCategoryID
	FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
  ,1,2,'') AS ConsequenceAssumptions

FROM Risk.tRiskDetail rd
INNER JOIN Risk.tRiskDetailCategory rdc on rdc.RiskDetailCategoryID = rd.RiskDetailCategoryID
INNER JOIN Risk.tRiskDetailType rdt on rdt.RiskDetailTypeID = rdc.RiskDetailTypeID
INNER JOIN Risk.tRiskDetailScoreType rdct on rdct.RiskDetailScoreTypeID = rdc.RiskDetailScoreTypeID
WHERE rdc.RiskDetailTypeID = 2
GROUP BY rd.RiskID