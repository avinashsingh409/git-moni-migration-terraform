﻿
CREATE FUNCTION [Risk].[ufnGetFilteredAssetRiskIDsFromAssetIDs]
(	
	--@startDate datetime = NULL,
	--@endDate datetime = NULL,
	@changedStartDate datetime = NULL,
	@changedEndDate datetime = NULL,
	@closedStartDate datetime = NULL,
	@closedEndDate datetime = NULL,
	@createdStartDate datetime = NULL,
	@createdEndDate datetime = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	--@impactCostHigh float = NULL,
	--@impactCostLow float = NULL,
	--@assignedTo NVARCHAR(255) = NULL,
	@riskAcceptabilities Base.tpIntList READONLY,
	@riskConfidences Base.tpIntList READONLY,
	@riskNatures Base.tpIntList  READONLY,
	@riskScoreCalcTypes Base.tpIntList READONLY,
	@riskStages Base.tpIntList READONLY,
	@assetIDs Base.tpIntList READONLY
	--@mitigated bit = NULL
	--@priority NVARCHAR(255) = NULL
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT tRisk.RiskID
	FROM Risk.tRisk JOIN @assetIDs b ON Risk.tRisk.OwningAssetID = b.id
	WHERE 
		--(@startDate IS NULL OR(Diagnostics.vAssetIssue.CreateDate >= @startDate)) AND
		--(@endDate IS NULL OR(Diagnostics.vAssetIssue.CreateDate <= @endDate)) AND
		(@changedStartDate  IS NULL OR(Risk.tRisk.ChangedDate >= @changedStartDate)) AND
		(@changedEndDate  IS NULL OR(Risk.tRisk.ChangedDate <= @changedEndDate)) AND
		(@closedStartDate   IS NULL OR(Risk.tRisk.ClosedDate >= @closedStartDate)) AND
		(@closedEndDate   IS NULL OR(Risk.tRisk.ClosedDate <= @closedEndDate)) AND
		(@createdStartDate   IS NULL OR(Risk.tRisk.CreatedDate >= @createdStartDate)) AND
		(@createdEndDate   IS NULL OR(Risk.tRisk.CreatedDate <= @createdEndDate)) AND

		--(@openDurationLow IS NULL OR (Diagnostics.vAssetIssue.OpenDuration IS NOT NULL AND Diagnostics.vAssetIssue.OpenDuration >= @openDurationLow)) AND
		--(@openDurationHigh IS NULL OR (Diagnostics.vAssetIssue.OpenDuration IS NOT NULL AND Diagnostics.vAssetIssue.OpenDuration <= @openDurationHigh)) AND
		--(@impactCostHigh IS NULL OR (Diagnostics.vAssetIssue.ImpactTotal <= @impactCostHigh)) AND
		--(@impactCostLow IS NULL OR (Diagnostics.vAssetIssue.ImpactTotal >= @impactCostLow)) AND
				
		(@id IS NULL OR ( CONVERT(varchar(10), Risk.tRisk.RiskID) LIKE '%' + @id + '%' ) ) AND
		(@title IS NULL OR (Risk.tRisk.Title LIKE '%' + @title + '%')) AND

		--(@assignedTo IS NULL OR (Diagnostics.vAssetIssue.AssignedToUsername LIKE '%' + @assignedTo + '%')) AND
		--(@priority IS NULL OR (Diagnostics.vAssetIssue.[Priority] LIKE '%' + @priority + '%')) AND

		--(@mitigated IS NULL OR (Risk.tRisk.Mitigated = @mitigated)) AND
		
		

		(NOT EXISTS (SELECT 1 FROM @riskAcceptabilities) OR (Risk.tRisk.RiskAcceptabilityTypeID IN (SELECT id FROM @riskAcceptabilities) )) AND 
		(NOT EXISTS (SELECT 1 FROM @riskConfidences) OR (Risk.tRisk.RiskConfidenceTypeID IN (SELECT id FROM @riskConfidences) )) AND 
		(NOT EXISTS (SELECT 1 FROM @riskNatures) OR (Risk.tRisk.RiskNatureTypeID IN (SELECT id FROM @riskNatures))) AND
		(NOT EXISTS (SELECT 1 FROM @riskScoreCalcTypes) OR (Risk.tRisk.RiskScoreCalcTypeID IN (SELECT id FROM @riskScoreCalcTypes))) AND
		(NOT EXISTS (SELECT 1 FROM @riskStages) OR (Risk.tRisk.RiskStageTypeID IN (SELECT id FROM @riskStages))) 

)
