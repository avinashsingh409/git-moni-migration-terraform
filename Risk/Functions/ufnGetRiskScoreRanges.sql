﻿CREATE FUNCTION [Risk].[ufnGetRiskScoreRanges]
(
	@StartYear smallint,
	@EndYear smallint,
	@RiskIDs base.tpIntList READONLY
)
RETURNS 
@return TABLE 
( 
	   [RiskID] int
      ,[ScoreRangeStartYear] smallint
      ,[ScoreRangeEndYear] smallint
      ,[StartLoFScore] float
      ,[EndLoFScore] float
      ,[StartRiskScore] float
      ,[EndRiskScore] float
	  ,[MaxLoFScore] float
	  ,[MinLoFScore] float
	  ,[MaxRiskScore] float
	  ,[MinRiskScore] float
	  ,[LoFScoreRange] nvarchar(39) null
	  ,[RiskScoreRange] nvarchar(39) null
)
AS
BEGIN
	INSERT INTO @return ( 
	  [RiskID] 
      ,[StartLoFScore] 
      ,[EndLoFScore] 
      ,[StartRiskScore] 
      ,[EndRiskScore] 
	  )
	SELECT 
		a.id RiskID, 
		Risk.ufnGetLoFScoreForRiskAndYear(a.id,@StartYear) [StartLoFScore], 
		Risk.ufnGetLoFScoreForRiskAndYear(a.id,@EndYear) [EndLoFScore], 
		Risk.ufnGetRiskScoreForRiskAndYear(a.id,@StartYear) [StartRiskScore], 
		Risk.ufnGetRiskScoreForRiskAndYear(a.id,@EndYear) [EndRiskScore]
	FROM @RiskIDs a
	;
	UPDATE @return
	SET [LoFScoreRange] =
		(case 
			WHEN [StartLoFScore] < [EndLoFScore] THEN CONVERT(nvarchar(18), [StartLoFScore]) + ' - ' + CONVERT(nvarchar(18), [EndLoFScore])
			WHEN [StartLoFScore] > [EndLoFScore] THEN CONVERT(nvarchar(18), [EndLoFScore]) + ' - ' + CONVERT(nvarchar(18), [StartLoFScore]) 
			ELSE CONVERT(nvarchar(18), [StartLoFScore])
		 end),
		[RiskScoreRange] = (case 
			WHEN [StartRiskScore] < [EndRiskScore] THEN CONVERT(nvarchar(18), [StartRiskScore]) + ' - ' + CONVERT(nvarchar(18), [EndRiskScore])
			WHEN [StartRiskScore] > [EndRiskScore] THEN CONVERT(nvarchar(18), [EndRiskScore]) + ' - ' + CONVERT(nvarchar(18), [StartRiskScore]) 
			ELSE CONVERT(nvarchar(18), [StartRiskScore])
		 end),
		 [MaxLoFScore] = case when [StartLoFScore] < [EndLoFScore] then [EndLoFScore] else StartLoFScore end,
		 [MinLoFScore] = case when [StartLoFScore] > [EndLoFScore] then [EndLoFScore] else StartLoFScore end,
		 [MaxRiskScore] = case when [StartRiskScore] < [EndRiskScore] then [EndRiskScore] else StartRiskScore end,
		 [MinRiskScore] = case when [StartRiskScore] > [EndRiskScore] then [EndRiskScore] else StartRiskScore end
	;
	RETURN;
END