﻿CREATE FUNCTION [Risk].[ufnGetRiskScoreForRiskAndYear](
	@RiskID int,
	@Year smallint
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result float;

	-- Add the T-SQL statements to compute the return value here
	if(SELECT COUNT(*) FROM Risk.tRiskScore WHERE RiskID = @RiskID AND [Year] = @Year) = 1
		select @result = RiskScore from Risk.tRiskScore WHERE RiskID = @RiskID AND [Year] = @Year
	else
	begin
		Declare @FirstYear smallint, @LastYear smallint;
		SELECT @FirstYear = Min([Year]), @LastYear = MAX([Year]) FROM Risk.tRiskScore WHERE RiskID = @RiskID;
		if @FirstYear > @Year
			select @result = RiskScore from Risk.tRiskScore WHERE RiskID = @RiskID AND [Year] = @FirstYear
		else
			select @result = RiskScore from Risk.tRiskScore WHERE RiskID = @RiskID AND [Year] = @LastYear
			
	end

	-- Return the result of the function
	RETURN @result
END