﻿
CREATE FUNCTION [Risk].[ufnGetFilteredAssetRiskIDs]
(	
	--@startDate datetime = NULL,
	--@endDate datetime = NULL,
	@changedStartDate datetime = NULL,
	@changedEndDate datetime = NULL,
	@closedStartDate datetime = NULL,
	@closedEndDate datetime = NULL,
	@createdStartDate datetime = NULL,
	@createdEndDate datetime = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	--@impactCostHigh float = NULL,
	--@impactCostLow float = NULL,
	--@assignedTo NVARCHAR(255) = NULL,
	@riskAcceptabilities Base.tpIntList READONLY,
	@riskConfidences Base.tpIntList READONLY,
	@riskNatures Base.tpIntList  READONLY,
	@riskScoreCalcTypes Base.tpIntList READONLY,
	@riskStages Base.tpIntList READONLY
	--@mitigated bit = NULL
	--@priority NVARCHAR(255) = NULL
)
RETURNS @riskTable TABLE
(
  RiskID int PRIMARY KEY not NULL
)
AS
BEGIN

DECLARE @assetIDs Base.tpIntList
INSERT INTO @assetIDs select distinct OwningAssetID from Risk.tRisk

INSERT INTO @riskTable
SELECT RiskID FROM Risk.ufnGetFilteredAssetRiskIDsFromAssetIDs(
--@startDate,@endDate,
@changedStartDate, @changedEndDate,@closedStartDate,@closedEndDate,@createdStartDate,@createdEndDate,
@id,@title,
--@impactCostHigh,@impactCostLow,@assignedTo,
@riskAcceptabilities,@riskConfidences,@riskNatures,@riskScoreCalcTypes,@riskStages,@assetIDs--,@mitigated,@priority
)

RETURN
END