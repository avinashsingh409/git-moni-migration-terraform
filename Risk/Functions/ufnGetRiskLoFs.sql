﻿

CREATE FUNCTION [Risk].[ufnGetRiskLoFs]
(	
	@riskIDs Base.tpIntList READONLY,
	@fetchAllIfNoIDsSupplied bit = 1
)
RETURNS @riskTable TABLE
(
  RiskID int PRIMARY KEY not NULL,  --we should only ever have one LoF value per risk.
  LoFValue real not null
)
AS
BEGIN
	Insert into @riskTable
	select a.RiskID,  d.Value as LoFValue --maybe?
	FROM Risk.tRiskDetail a
	inner join @riskIDs r on a.RiskID = r.id
	inner join Risk.tRiskDetailCategory b on a.RiskDetailCategoryID = b.RiskDetailCategoryID
	inner join Risk.tRiskDetailType c on c.RiskDetailTypeID = b.RiskDetailTypeID
	inner join risk.tRiskDetailScoreType d on d.RiskDetailScoreTypeID = b.RiskDetailScoreTypeID
	where c.Description like 'Likelihood'

	declare @suppliedIDs int;
	select @suppliedIDs = Count(*) From @riskIDs;
	if (0 = @suppliedIDs AND 1 = @fetchAllIfNoIDsSupplied)
	BEGIN
		Insert into @riskTable
		select a.RiskID,  d.Value as LoFValue --maybe?
		FROM Risk.tRiskDetail a
		inner join Risk.tRiskDetailCategory b on a.RiskDetailCategoryID = b.RiskDetailCategoryID
		inner join Risk.tRiskDetailType c on c.RiskDetailTypeID = b.RiskDetailTypeID
		inner join risk.tRiskDetailScoreType d on d.RiskDetailScoreTypeID = b.RiskDetailScoreTypeID
		where c.Description like 'Likelihood'
	END
	return;
END
