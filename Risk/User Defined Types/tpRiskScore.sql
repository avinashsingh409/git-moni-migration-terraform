﻿CREATE TYPE [Risk].[tpRiskScore] AS TABLE (
    [RiskID]    INT      NOT NULL,
    [Year]      SMALLINT NOT NULL,
    [LoFScore]  REAL     NOT NULL,
    [RiskScore] REAL     NOT NULL);


GO
GRANT EXECUTE
    ON TYPE::[Risk].[tpRiskScore] TO [TEUser];

