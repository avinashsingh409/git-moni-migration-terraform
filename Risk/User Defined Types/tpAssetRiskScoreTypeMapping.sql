﻿CREATE TYPE [Risk].[tpAssetRiskScoreTypeMapping] AS TABLE (
    [AssetClassType]     NVARCHAR (255)   NOT NULL UNIQUE,
    [XaxisType]          NVARCHAR (255)   NOT NULL,
    [YaxisType]          NVARCHAR (255)   NOT NULL,
    [LabelType]          NVARCHAR (255)
);
GO
GRANT EXECUTE
    ON TYPE::[Risk].[tpAssetRiskScoreTypeMapping] TO [TEUser];
GO
