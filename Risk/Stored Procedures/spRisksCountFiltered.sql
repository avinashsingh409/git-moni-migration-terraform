﻿
CREATE PROCEDURE [Risk].[spRisksCountFiltered] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID Base.tpIntList READONLY,
	--@startDate datetime = NULL,
	--@endDate datetime = NULL,
	@changedStartDate datetime = NULL,
	@changedEndDate datetime = NULL,
	@closedStartDate datetime = NULL,
	@closedEndDate datetime = NULL,
	@createdStartDate datetime = NULL,
	@createdEndDate datetime = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	--@impactCostHigh float = NULL,
	--@impactCostLow float = NULL,
	--@assignedTo NVARCHAR(255) = NULL,
	--@mitigated bit = NULL,
	--@priority NVARCHAR(255) = NULL,
	@riskAcceptabilities Base.tpIntList READONLY,
	@riskConfidences Base.tpIntList READONLY,
	@riskNatures Base.tpIntList  READONLY,
	@riskScoreCalcTypes Base.tpIntList READONLY,
	@riskStages Base.tpIntList READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET ARITHABORT ON
    
    DECLARE @securityassets TABLE
	(
		AssetID int PRIMARY KEY
	)

	IF( EXISTS(SELECT * FROM @assetID))
	BEGIN 
	
		declare @topAssetID as int

		select top 1 @topAssetID = assetid from asset.tAsset a join @assetID b on a.AssetID = b.id join asset.tAssetClassType c on a.AssetClassTypeID = c.AssetClassTypeID 
			order by c.AssetTypeID

		DECLARE @topassets TABLE
			(
				AssetID int PRIMARY KEY
			)

		insert into @topassets select assetid from Asset.ufnGetAssetTreeBranch(@topassetid)

		if exists(select * from @assetID where id not in (select assetid from @topassets))
			begin
			insert into @securityassets select distinct AssetId from @assetID cross apply Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,id)
			end
		else
			begin
			insert into @securityassets select distinct AssetId from Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,@topAssetID)
			end

	    DECLARE @branches TABLE
		(
		    TreeAssetID int,
			BranchAssetID int
		)
	
	    insert into @branches select distinct a.id,b.assetid from @assetID a CROSS APPLY Asset.ufnGetAssetTreeBranch(ID) b
	    
	    declare @assets base.tpIntList
	    insert into @assets select distinct BranchAssetID from @branches
	        
		SELECT id assetID, (
		SELECT COUNT(Risk.tRisk.RiskID)
					 FROM Risk.tRisk
					 --Join to filter on asset tree
					 INNER JOIN @branches branch on branch.TreeAssetID=ID and  Risk.tRisk.[OwningAssetID] = branch.BranchAssetID
					 --Join to provide security 
					 INNER JOIN @securityassets securityCheck 
					  		ON securityCheck.AssetID=Risk.tRisk.OwningAssetID
					 INNER JOIN [Risk].[ufnGetFilteredAssetRiskIDsFromAssetIDs]( 
							--@startDate, @endDate, 
							@changedStartDate,@changedEndDate,@closedStartDate,@closedEndDate,@createdStartDate,@createdEndDate, 
							@id, @title,
							--@impactCostHigh,@impactCostLow,@assignedTo,
							@riskAcceptabilities,@riskConfidences,@riskNatures,@riskScoreCalcTypes,@riskStages, @assets
							--, @mitigated--,@priority
							) riskIDCheck
							ON riskIDCheck.RiskID = Risk.tRisk.RiskID
		) riskCount
		into #results
		FROM @assetID
	
		SELECT r.*, a.AssetDesc, a.AssetClassTypeID, act.AssetClassTypeDesc from #results r
		INNER JOIN Asset.tAsset a on r.assetID = a.AssetID
		INNER JOIN Asset.tAssetClassType act on a.AssetClassTypeID = act.AssetClassTypeID
		WHERE r.riskCount > 0
				
	END 
	ELSE
	BEGIN 
	insert into @securityassets select AssetId from Asset.ufnGetAssetIdsForuser(@securityUserID)  
	
		SELECT 0 assetID, 
		(SELECT COUNT(risk.tRisk.RiskID)
					 FROM risk.tRisk
					 --Join to provide security 
					 INNER JOIN @securityassets securityCheck 
							ON securityCheck.AssetID=risk.tRisk.OwningAssetID
					 INNER JOIN Risk.[ufnGetFilteredAssetRiskIDs]( 
							--@startDate, @endDate, 
							@changedStartDate,@changedEndDate,@closedStartDate,@closedEndDate,@createdStartDate,@createdEndDate,
							@id,@title, 
							--@impactCostHigh,@impactCostLow,@assignedTo,
							@riskAcceptabilities,@riskConfidences,@riskNatures,@riskScoreCalcTypes,@riskStages
							--, @mitigated--,@priority
							) riskIDCheck
							ON riskIDCheck.RiskID = Risk.tRisk.RiskID
		) riskCount, 
		'' AssetDesc, 
		0 AssetClassTypeID, 
		'' AssetClassTypeDesc
	END
	
END

GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spRisksCountFiltered] TO [TEUser]
    AS [dbo];

