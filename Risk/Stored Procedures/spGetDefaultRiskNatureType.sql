﻿
CREATE PROCEDURE [Risk].[spGetDefaultRiskNatureType]
AS
BEGIN
    SELECT [RiskNatureTypeID]
		  ,[OwningAssetID]
          ,[PrimaryNature]
          ,[SubNature]
    FROM [Risk].[tRiskNatureType]
    WHERE [OwningAssetID] IS NULL
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetDefaultRiskNatureType] TO [TEUser]
    AS [dbo];

