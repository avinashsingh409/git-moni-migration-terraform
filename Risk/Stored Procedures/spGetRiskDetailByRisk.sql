﻿
CREATE PROCEDURE [Risk].[spGetRiskDetailByRisk]
	@riskID int
AS
BEGIN
	SELECT rd.[RiskDetailID]
		  ,rd.[RiskID]
		  ,rd.[RiskDetailCategoryID]
		  ,rd.[Assumptions]
		  ,rd.[StartDate]
		  ,rd.[ScoreWeight]
		  ,rdc.[RiskDetailTypeID]
		  ,rdc.[RiskDetailScoreTypeID]
		  ,rdc.[Definition]
		  ,rdc.[Category]
	FROM [Risk].[tRiskDetail] rd
	INNER JOIN [Risk].[tRiskDetailCategory] rdc 
	  ON rd.RiskDetailCategoryID = rdc.RiskDetailCategoryID
	WHERE [RiskID] = @riskID
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskDetailByRisk] TO [TEUser]
    AS [dbo];

