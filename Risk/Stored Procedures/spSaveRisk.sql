﻿
CREATE PROCEDURE [Risk].[spSaveRisk]
    @riskID int = null
   ,@owningAssetID int
   ,@riskNatureTypeID int = null
   ,@riskStagetypeID int = null
   ,@riskConfidenceTypeID int = null
   ,@riskAcceptabilityTypeID int = null
   ,@riskScoreCalcTypeID int
   ,@title nvarchar(128)
   ,@cause nvarchar(max) = null
   ,@existingControl nvarchar(max) = null
   ,@controlAdequacy nvarchar(max) = null
   ,@securityUserID int
   ,@openedDate date = null
   ,@closedDate date = null
   ,@description nvarchar(max) = null
AS 
BEGIN
SET NOCOUNT ON;
	IF @riskID IS NULL
	BEGIN
		INSERT INTO [Risk].[tRisk]
			( [OwningAssetID]
			 ,[RiskNatureTypeID]
			 ,[RiskStageTypeID]
			 ,[RiskConfidenceTypeID]
			 ,[RiskAcceptabilityTypeID]
			 ,[RiskScoreCalcTypeID]
			 ,[Title]
			 ,[Cause]
			 ,[ExistingControl]
			 ,[ControlAdequacy]
			 ,[CreatedByID]
			 ,[ChangedByID]
			 ,[CreatedDate]
			 ,[ChangedDate]
			 ,[OpenedDate]
			 ,[Description] )
		VALUES
			( @owningAssetID
			 ,@riskNatureTypeID
			 ,@riskStagetypeID
			 ,@riskConfidenceTypeID
			 ,@riskAcceptabilityTypeID
			 ,@riskScoreCalcTypeID
			 ,@title
			 ,@cause
			 ,@existingControl
			 ,@controlAdequacy
			 ,@securityUserID
			 ,@securityUserID
			 ,GETDATE()
			 ,GETDATE()
			 ,GETDATE()
			 ,@description )
	END
	ELSE
	BEGIN
		DECLARE @oldOpenDate date;
		SELECT TOP 1 @oldOpenDate = [OpenedDate] FROM [Risk].[tRisk] WHERE [RiskID] = @riskID;

		UPDATE [Risk].[tRisk]
		SET [RiskNatureTypeID] = @riskNatureTypeID
		   ,[RiskStageTypeID] = @riskStagetypeID
		   ,[RiskConfidenceTypeID] = @riskConfidenceTypeID
		   ,[RiskAcceptabilityTypeID] = @riskAcceptabilityTypeID
		   ,[RiskScoreCalcTypeID] = @riskScoreCalcTypeID
		   ,[Title] = @title
		   ,[Cause] = @cause
		   ,[ExistingControl] = @existingControl
		   ,[ControlAdequacy] = @controlAdequacy
		   ,[ChangedByID] = @securityUserID
		   ,[ChangedDate] = GETDATE()
		   ,[OpenedDate] = CASE WHEN @openedDate IS NULL THEN @oldOpenDate ELSE @openedDate END
		   ,[ClosedDate] = @closedDate
		   ,[Description] = @description
		WHERE [RiskID] = @riskID
	END
	
	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRisk] TO [TEUser]
    AS [dbo];

