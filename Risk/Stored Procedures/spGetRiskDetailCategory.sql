﻿
CREATE PROCEDURE [Risk].[spGetRiskDetailCategory]
    @riskDetailCategoryID int = null
   ,@owningAssetID int = null
AS
BEGIN
    SELECT [RiskDetailCategoryID]
		  ,[OwningAssetID]
          ,[RiskDetailTypeID]
          ,[RiskDetailScoreTypeID]
          ,[Definition]
          ,[Category]
    FROM [Risk].[tRiskDetailCategory]
    WHERE (@owningAssetID IS NOT NULL AND [OwningAssetID] = @owningAssetID) 
		  OR
		  (@riskDetailCategoryID IS NOT NULL AND [RiskDetailCategoryID] = @riskDetailCategoryID)
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskDetailCategory] TO [TEUser]
    AS [dbo];

