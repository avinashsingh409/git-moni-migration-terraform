﻿
CREATE PROCEDURE [Risk].[spDeleteRisk]
   @riskID int
AS 
BEGIN
   DELETE FROM [Risk].[tRisk]
   WHERE [RiskID] = @riskID
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spDeleteRisk] TO [TEUser]
    AS [dbo];

