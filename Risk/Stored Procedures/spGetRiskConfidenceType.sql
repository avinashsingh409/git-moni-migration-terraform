﻿
CREATE PROCEDURE [Risk].[spGetRiskConfidenceType]
    @riskConfidenceTypeID int = null
AS
BEGIN
    SELECT [RiskConfidenceTypeID]
          ,[Value]
          ,[Description]
    FROM [Risk].[tRiskConfidenceType]
    WHERE (@riskConfidenceTypeID IS NULL) OR
          (@riskConfidenceTypeID IS NOT NULL and [RiskConfidenceTypeID] = @riskConfidenceTypeID)
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskConfidenceType] TO [TEUser]
    AS [dbo];

