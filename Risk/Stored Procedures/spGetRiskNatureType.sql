﻿
CREATE PROCEDURE [Risk].[spGetRiskNatureType]
    @riskNatureTypeID int = null
   ,@owningAssetID int = null
AS
BEGIN
    SELECT [RiskNatureTypeID]
		  ,[OwningAssetID]
          ,[PrimaryNature]
          ,[SubNature]
    FROM [Risk].[tRiskNatureType]
    WHERE (@owningAssetID IS NOT NULL AND [OwningAssetID] = @owningAssetID) 
		  OR
		  (@riskNatureTypeID IS NOT NULL AND [RiskNatureTypeID] = @riskNatureTypeID)
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskNatureType] TO [TEUser]
    AS [dbo];

