﻿
CREATE PROCEDURE [Risk].[spGetRiskStageType]
    @riskStageTypeID int = null
AS
BEGIN
    SELECT [RiskStageTypeID]
          ,[Description]
    FROM [Risk].[tRiskStageType]
    WHERE (@riskStageTypeID IS NULL) OR
          (@riskStageTypeID IS NOT NULL and [RiskStageTypeID] = @riskStageTypeID)
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskStageType] TO [TEUser]
    AS [dbo];

