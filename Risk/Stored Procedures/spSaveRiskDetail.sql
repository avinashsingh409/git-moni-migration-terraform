﻿
CREATE PROCEDURE [Risk].[spSaveRiskDetail]
    @riskDetailID int = null
   ,@riskID int
   ,@riskDetailCategoryID int
   ,@assumptions nvarchar(max) = null
   ,@startDate date
   ,@scoreWeight real = null
AS 
BEGIN
SET NOCOUNT ON;
	IF @riskDetailID IS NULL
	BEGIN
		INSERT INTO [Risk].[tRiskDetail]
			( [RiskID]
			 ,[RiskDetailCategoryID]
			 ,[Assumptions]
			 ,[StartDate]
			 ,[ScoreWeight] )
		VALUES
			( @riskID
			 ,@riskDetailCategoryID
			 ,@assumptions
			 ,@startDate
			 ,CASE WHEN @scoreWeight IS NULL THEN 1 ELSE @scoreWeight END) 
	END
	ELSE
	BEGIN
		DECLARE @oldScoreWeight real= 1;
		SELECT TOP 1 @oldScoreWeight = [ScoreWeight] FROM [Risk].[tRiskDetail] WHERE [RiskDetailID] = @riskDetailID;
	
		UPDATE [Risk].[tRiskDetail]
		SET [RiskID] = @riskID
		   ,[RiskDetailCategoryID] = @riskDetailCategoryID
		   ,[Assumptions] = @assumptions
		   ,[StartDate] = @startDate
		   ,[ScoreWeight] = CASE WHEN @scoreWeight IS NULL THEN @oldScoreWeight ELSE @scoreWeight END
		WHERE [RiskDetailID] = @riskDetailID
	END
	
	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRiskDetail] TO [TEUser]
    AS [dbo];

