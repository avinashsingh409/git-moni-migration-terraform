﻿
CREATE PROCEDURE [Risk].[spGetDefaultRiskDetailScoreType]
AS
BEGIN
    SELECT [RiskDetailScoreTypeID]
		  ,[OwningAssetID]
          ,[Value]
          ,[Description]
    FROM [Risk].[tRiskDetailScoreType]
    WHERE [OwningAssetID] IS NULL
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetDefaultRiskDetailScoreType] TO [TEUser]
    AS [dbo];

