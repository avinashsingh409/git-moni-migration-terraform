﻿
CREATE PROCEDURE [Risk].[spRisksCountsByStage] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID int = NULL,
	--@startDate datetime = NULL,
	--@endDate datetime = NULL,
	@changedStartDate datetime = NULL,
	@changedEndDate datetime = NULL,
	@closedStartDate datetime = NULL,
	@closedEndDate datetime = NULL,
	@createdStartDate datetime = NULL,
	@createdEndDate datetime = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	--@impactCostHigh float = NULL,
	--@impactCostLow float = NULL,
	--@assignedTo NVARCHAR(255) = NULL,
	--@mitigated bit = NULL,
	--@priority NVARCHAR(255) = NULL,
	@riskAcceptabilities Base.tpIntList READONLY,
	@riskConfidences Base.tpIntList READONLY,
	@riskNatures Base.tpIntList  READONLY,
	@riskScoreCalcTypes Base.tpIntList READONLY,
	@riskStages Base.tpIntList READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET ARITHABORT ON
    
    DECLARE @assets base.tpIntList;
    
    insert into @assets select assetid from Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,@assetID) 
    --select * into #tmp_assets from Asset.ufnGetAssetTreeBranch(@assetID)
	-- Insert statements for procedure here
	select sub.RiskStageTypeID, rs.[Description] RiskStageTypeDesc, sub.RiskCount
	FROM
	(
		SELECT Risk.tRisk.RiskStageTypeID, COUNT(Risk.tRisk.RiskID) as RiskCount
		FROM Risk.tRisk
		INNER JOIN Risk.tRiskStageType rs on Risk.tRisk.RiskStageTypeID = rs.RiskStageTypeID
		--Join to filter on asset tree
		INNER JOIN @assets branch on Risk.tRisk.OwningAssetID = branch.ID
		INNER JOIN Risk.[ufnGetFilteredAssetRiskIDsFromAssetIDs]( 
								--@startDate, @endDate, 
								@changedStartDate,@changedEndDate,@closedStartDate,@closedEndDate,@createdStartDate,@createdEndDate, 
								@id, @title, 
								--@impactCostHigh,@impactCostLow,@assignedTo,
								@riskAcceptabilities,@riskConfidences,@riskNatures,@riskScoreCalcTypes,@riskStages, @assets
								--, @scorecard,@priority
								) f on f.RiskID = Risk.tRisk.RiskID
		GROUP BY Risk.tRisk.RiskStageTypeID
	)sub
	INNER JOIN Risk.tRiskStageType rs on sub.RiskStageTypeID = rs.RiskStageTypeID
END

GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spRisksCountsByStage] TO [TEUser]
    AS [dbo];

