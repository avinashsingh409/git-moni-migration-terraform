﻿

CREATE PROCEDURE [Risk].[spRisksFiltered] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID int = NULL,
	@selectedTimeRangeStartYear smallint = NULL,
	@selectedTimeRangeEndYear smallint = NULL,
	@changedStartDate datetime = NULL,
	@changedEndDate datetime = NULL,
	@closedStartDate datetime = NULL,
	@closedEndDate datetime = NULL,
	@createdStartDate datetime = NULL,
	@createdEndDate datetime = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	--@impactCostHigh float = NULL,
	--@impactCostLow float = NULL,
	--@assignedTo NVARCHAR(255) = NULL,
	--@scorecard bit = NULL,
	--@priority NVARCHAR(255) = NULL,
	@riskAcceptabilities Base.tpIntList READONLY,
	@riskConfidences Base.tpIntList READONLY,
	@riskNatures Base.tpIntList  READONLY,
	@riskScoreCalcTypes Base.tpIntList READONLY,
	@riskStages Base.tpIntList READONLY,
	@startIndex int = 1, 
	@endIndex int = 15,
	@sort NVARCHAR(100) = NULL,
	@sortOrder bit = 0
WITH RECOMPILE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    
    SET ARITHABORT ON
    
    DECLARE @assets base.tpIntList
    	
    insert into @assets select AssetId from Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID,@assetID) 
    ;
    
    DECLARE @riskIDs base.tpIntList;
	
	insert into @riskIDs select RiskID from
    [Risk].[ufnGetFilteredAssetRiskIDsFromAssetIDs]( 
		--@startDate, @endDate, 
		@changedStartDate,@changedEndDate,@closedStartDate,@closedEndDate,@createdStartDate,@createdEndDate, 
		@id,@title,
		--@impactCostHigh,@impactCostLow,@assignedTo,
		@riskAcceptabilities,@riskConfidences,@riskNatures,	@riskScoreCalcTypes,@riskStages,@assets
		--@scorecard,@priority
		) riskIDCheck;			 
	DECLARE @riskScores TABLE 
	( 
		[RiskID] int
		,[ScoreRangeStartYear] smallint
		,[ScoreRangeEndYear] smallint
		,[StartLoFScore] float
		,[EndLoFScore] float
		,[StartRiskScore] float
		,[EndRiskScore] float
		,[MaxLoFScore] float
		,[MinLoFScore] float
		,[MaxRiskScore] float
		,[MinRiskScore] float
		,[LoFScoreRange] nvarchar(39) null
		,[RiskScoreRange] nvarchar(39) null
	)
	Insert into @riskScores
	Select * FROM [Risk].[ufnGetRiskScoreRanges](@selectedTimeRangeStartYear, @selectedTimeRangeEndYear, @riskIDs) riskScoreRanges;

	-- Insert statements for procedure here
	WITH records as (
		SELECT ROW_NUMBER() OVER (
		
		ORDER BY 
		CASE WHEN @sort = 'RiskID' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskID ELSE NULL END,
		CASE WHEN @sort = 'CreatedDate' AND @sortOrder = 0 THEN [Risk].[vRisk].CreatedDate ELSE NULL END,
		CASE WHEN @sort = 'ChangedDate' AND @sortOrder = 0 THEN [Risk].[vRisk].ChangedDate ELSE NULL END,
		CASE WHEN @sort = 'ClosedDate' AND @sortOrder = 0 THEN [Risk].[vRisk].ClosedDate ELSE NULL END,
		CASE WHEN @sort = 'OpenedDate' AND @sortOrder = 0 THEN [Risk].[vRisk].OpenedDate ELSE NULL END,
		CASE WHEN @sort = 'OpenDuration' AND @sortOrder = 0 THEN [Risk].[vRisk].OpenDuration ELSE NULL END,
		CASE WHEN @sort = 'Title' AND @sortOrder = 0 THEN [Risk].[vRisk].Title ELSE NULL END,
		CASE WHEN @sort = 'AssetClassTypeAbbrev' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetClassTypeAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'AssetClassTypeDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetClassTypeDesc] ELSE NULL END,
		CASE WHEN @sort = 'AssetTypeAbbrev' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetTypeAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'AssetTypeDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetTypeDesc] ELSE NULL END,
		CASE WHEN @sort = 'AssetAbbrev' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'AssetDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetDesc] ELSE NULL END,
		CASE WHEN @sort = 'RiskAcceptabilityTypeDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskAcceptabilityTypeDesc ELSE NULL END,
		CASE WHEN @sort = 'RiskAcceptabilityTypeValue' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskAcceptabilityTypeValue ELSE NULL END,
		CASE WHEN @sort = 'RiskConfidenceTypeDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskConfidenceTypeDesc ELSE NULL END,
		CASE WHEN @sort = 'RiskConfidenceTypeValue' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskConfidenceTypeValue ELSE NULL END,
		CASE WHEN @sort = 'RiskNatureDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskNatureDesc ELSE NULL END,
		CASE WHEN @sort = 'RiskStageDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskStageTypeDesc ELSE NULL END,
		CASE WHEN @sort = 'RiskScoreCalcTypeDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskScoreCalcTypeDesc ELSE NULL END,
		CASE WHEN @sort = 'Cause' AND @sortOrder = 0 THEN [Risk].[vRisk].Cause ELSE NULL END,
		CASE WHEN @sort = 'ControlAdequacy' AND @sortOrder = 0 THEN [Risk].[vRisk].ControlAdequacy ELSE NULL END,
		CASE WHEN @sort = 'Description' AND @sortOrder = 0 THEN [Risk].[vRisk].RiskDescription ELSE NULL END,
		CASE WHEN @sort = 'ExistingControls' AND @sortOrder = 0 THEN [Risk].[vRisk].ExistingControl ELSE NULL END,
		CASE WHEN @sort = 'CreatedByName' AND @sortOrder = 0 THEN [Risk].[vRisk].CreatedByName ELSE NULL END,
		CASE WHEN @sort = 'CreatedByEmail' AND @sortOrder = 0 THEN [Risk].[vRisk].CreatedByEmail ELSE NULL END,
		CASE WHEN @sort = 'ChangedByName' AND @sortOrder = 0 THEN [Risk].[vRisk].ChangedByName ELSE NULL END,
		CASE WHEN @sort = 'ChangedByEmail' AND @sortOrder = 0 THEN [Risk].[vRisk].ChangedByEmail ELSE NULL END,
		CASE WHEN @sort = 'ConsequenceScores' AND @sortOrder = 0 THEN [Risk].[vRisk].ConsequenceScores ELSE NULL END,
		CASE WHEN @sort = 'ConsequenceAssumptions' AND @sortOrder = 0 THEN [Risk].[vRisk].ConsequenceAssumptions ELSE NULL END,
		CASE WHEN @sort = 'LikelihoodScore' AND @sortOrder = 0 THEN [Risk].[vRisk].LikelihoodScore ELSE NULL END,
		CASE WHEN @sort = 'LikelihoodAssumptions' AND @sortOrder = 0 THEN [Risk].[vRisk].LikelihoodAssumptions ELSE NULL END,
		CASE WHEN @sort = 'RiskScore' AND @sortOrder = 0 THEN riskScores.MaxRiskScore ELSE NULL END,
		
		CASE WHEN @sort = 'RiskID' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskID ELSE NULL END DESC,
		CASE WHEN @sort = 'CreatedDate' AND @sortOrder = 1 THEN [Risk].[vRisk].CreatedDate ELSE NULL END DESC,
		CASE WHEN @sort = 'ChangedDate' AND @sortOrder = 1 THEN [Risk].[vRisk].ChangedDate ELSE NULL END DESC,
		CASE WHEN @sort = 'ClosedDate' AND @sortOrder = 1 THEN [Risk].[vRisk].ClosedDate ELSE NULL END DESC,
		CASE WHEN @sort = 'OpenedDate' AND @sortOrder = 1 THEN [Risk].[vRisk].OpenedDate ELSE NULL END DESC,
		CASE WHEN @sort = 'OpenDuration' AND @sortOrder = 0 THEN [Risk].[vRisk].OpenDuration ELSE NULL END DESC,
		CASE WHEN @sort = 'Title' AND @sortOrder = 1 THEN [Risk].[vRisk].Title ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetClassTypeAbbrev' AND @sortOrder = 1 THEN [Risk].[vRisk].[AssetClassTypeAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetClassTypeDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].[AssetClassTypeDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetTypeAbbrev' AND @sortOrder = 1 THEN [Risk].[vRisk].[AssetTypeAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetTypeDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].[AssetTypeDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetAbbrev' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetDesc' AND @sortOrder = 0 THEN [Risk].[vRisk].[AssetDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskAcceptabilityTypeDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskAcceptabilityTypeDesc ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskAcceptabilityTypeValue' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskAcceptabilityTypeValue ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskConfidenceTypeDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskConfidenceTypeDesc ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskConfidenceTypeValue' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskConfidenceTypeValue ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskNatureDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskNatureDesc ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskStageDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskStageTypeDesc ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskScoreCalcTypeDesc' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskScoreCalcTypeDesc ELSE NULL END DESC,
		CASE WHEN @sort = 'Cause' AND @sortOrder = 1 THEN [Risk].[vRisk].Cause ELSE NULL END DESC,
		CASE WHEN @sort = 'ControlAdequacy' AND @sortOrder = 1 THEN [Risk].[vRisk].ControlAdequacy ELSE NULL END DESC,
		CASE WHEN @sort = 'Description' AND @sortOrder = 1 THEN [Risk].[vRisk].RiskDescription ELSE NULL END DESC,
		CASE WHEN @sort = 'ExistingControls' AND @sortOrder = 1 THEN [Risk].[vRisk].ExistingControl ELSE NULL END DESC,
		CASE WHEN @sort = 'CreatedByName' AND @sortOrder = 1 THEN [Risk].[vRisk].CreatedByName ELSE NULL END DESC,
		CASE WHEN @sort = 'CreatedByEmail' AND @sortOrder = 1 THEN [Risk].[vRisk].CreatedByEmail ELSE NULL END DESC,
		CASE WHEN @sort = 'ChangedByName' AND @sortOrder = 1 THEN [Risk].[vRisk].ChangedByName ELSE NULL END DESC,
		CASE WHEN @sort = 'ChangedByEmail' AND @sortOrder = 1 THEN [Risk].[vRisk].ChangedByEmail ELSE NULL END DESC,
		CASE WHEN @sort = 'ConsequenceScores' AND @sortOrder = 1 THEN [Risk].[vRisk].ConsequenceScores ELSE NULL END DESC,
		CASE WHEN @sort = 'ConsequenceAssumptions' AND @sortOrder = 1 THEN [Risk].[vRisk].ConsequenceAssumptions ELSE NULL END DESC,
		CASE WHEN @sort = 'LikelihoodScore' AND @sortOrder = 1 THEN [Risk].[vRisk].LikelihoodScore ELSE NULL END DESC,
		CASE WHEN @sort = 'LikelihoodAssumptions' AND @sortOrder = 1 THEN [Risk].[vRisk].LikelihoodAssumptions ELSE NULL END DESC,
		CASE WHEN @sort = 'RiskScore' AND @sortOrder = 1 THEN riskScores.MaxRiskScore ELSE NULL END DESC,
		[Risk].[vRisk].RiskID
		) AS ROW_NUMBER,
			   [Risk].[vRisk].RiskID
		FROM [Risk].[vRisk]
		--Join to filter on asset tree
		INNER JOIN @assets branch on [Risk].[vRisk].AssetID = branch.ID	
		INNER JOIN @riskIDs riskIDCheck
			ON riskIDCheck.id = [Risk].[vRisk].RiskID
		INNER JOIN @riskScores riskScores
			on riskScores.RiskID = [Risk].[vRisk].RiskID
	)

	SELECT 
	r.ROW_NUMBER,
		asset.AssetDesc as asset_AssetDesc,
		asset.AssetAbbrev as asset_AssetAbbrev,
		asset.AssetClassTypeID as asset_AssetClassTypeID,
		syst.AssetDesc as system_AssetDesc,
		syst.AssetAbbrev as system_AssetAbbrev,
		syst.AssetClassTypeID as system_AssetClassTypeID,
		unit.AssetDesc as unit_AssetDesc,
		unit.AssetAbbrev as unit_AssetAbbrev,
		unit.AssetClassTypeID as unit_AssetClassTypeID,
		station.AssetDesc as station_AssetDesc,
		station.AssetAbbrev as station_AssetAbbrev,
		station.AssetClassTypeID as station_AssetClassTypeID,
		stationgroup.AssetDesc as stationgroup_AssetDesc,
		stationgroup.AssetAbbrev as stationgroup_AssetAbbrev,
		stationgroup.AssetClassTypeID as stationgroup_AssetClassTypeID,
		client.AssetDesc as client_AssetDesc,
		client.AssetAbbrev as client_AssetAbbrev,
		client.AssetClassTypeID as client_AssetClassTypeID,

		a.RiskID as RiskID,
		a.AssetID as AssetID,
		a.AssetClassTypeAbbrev as AssetClassTypeAbbrev,
		a.AssetClassTypeDesc as AssetClassTypeDesc,
		a.AssetTypeAbbrev as AssetTypeAbbrev,
		a.AssetTypeDesc as AssetTypeDesc,
		a.[CreatedDate] as [CreatedDate],
		a.[CreatedByID] as [CreatedByID],
		a.CreatedByName as CreatedByName,
		a.CreatedByEmail as CreatedByEmail,
		a.[ChangedDate] as [ChangedDate],
		a.[ChangedByID] as [ChangedByID],
		a.ChangedByName as ChangedByName,
		a.ChangedByEmail as ChangedByEmail,
		a.ClosedDate as ClosedDate,
		a.OpenedDate as OpenedDate,
		a.OpenDuration as OpenDuration,
		a.Title as Title,
		a.ConsequenceScores as ConsequenceScores,
		a.ConsequenceAssumptions as ConsequenceAssumptions,
		a.LikelihoodScore as LikelihoodScore,
		a.LikelihoodAssumptions as LikelihoodAssumptions,
		a.RiskAcceptabilityTypeID as RiskAcceptabilityTypeID,
		a.RiskAcceptabilityTypeDesc as RiskAcceptabilityTypeDesc,
		a.RiskAcceptabilityTypeValue as RiskAcceptabilityTypeValue,
		a.RiskConfidenceTypeID as RiskConfidenceTypeID,
		a.RiskConfidenceTypeDesc as RiskConfidenceTypeDesc,
		a.RiskConfidenceTypeValue as RiskConfidenceTypeValue,
		a.RiskNatureTypeID as RiskNatureTypeID,
		a.RiskNaturePrimaryDesc as RiskNaturePrimaryDesc,
		a.RiskNatureSubDesc as RiskNatureSubDesc,
		a.RiskNatureDesc as RiskNatureDesc,
		a.RiskNatureOwningAssetID as RiskNatureOwningAssetID,
		a.RiskStageTypeID as RiskStageTypeID, 
		a.RiskStageTypeDesc as RiskStageTypeDesc,
		a.RiskScoreCalcTypeID as RiskScoreCalcTypeID,
		a.RiskScoreCalcTypeDesc as RiskScoreCalcTypeDesc,
		a.Cause as Cause,
		a.RiskDescription as RiskDescription,
		a.ExistingControl as ExistingControl,
		riskScoreRanges.RiskScoreRange,
		riskScoreRanges.MaxRiskScore,
		riskScoreRanges.MinRiskScore
		FROM Risk.vRisk a 
		LEFT JOIN Asset.tAsset client 
			ON client.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'CLT')
		LEFT JOIN Asset.tAsset stationgroup 
			ON stationgroup.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'SG')
		LEFT JOIN Asset.tAsset station
			ON station.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'ST')
		LEFT JOIN Asset.tAsset unit
			ON unit.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'UN')
		LEFT JOIN Asset.tAsset syst
			ON syst.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'SYS')
		LEFT JOIN Asset.tAsset asset 
			ON asset.AssetID = a.AssetID
		LEFT JOIN [Risk].[ufnGetRiskScoreRanges](@selectedTimeRangeStartYear, @selectedTimeRangeEndYear, @riskIDs) riskScoreRanges 
			on a.RiskID = riskScoreRanges.RiskID
		INNER JOIN records r ON a.RiskID = r.RiskID
	WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex
	ORDER BY r.ROW_NUMBER
END


GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spRisksFiltered] TO [TEUser]
    AS [dbo];

