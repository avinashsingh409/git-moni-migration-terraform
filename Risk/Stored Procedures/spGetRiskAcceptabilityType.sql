﻿
CREATE PROCEDURE [Risk].[spGetRiskAcceptabilityType]
    @riskAcceptabilityTypeID int = null
AS
BEGIN
    SELECT [RiskAcceptabilityTypeID]
          ,[Value]
          ,[Description]
    FROM [Risk].[tRiskAcceptabilityType]
    WHERE (@riskAcceptabilityTypeID IS NULL) OR
          (@riskAcceptabilityTypeID IS NOT NULL and [RiskAcceptabilityTypeID] = @riskAcceptabilityTypeID)
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskAcceptabilityType] TO [TEUser]
    AS [dbo];

