﻿CREATE PROCEDURE [Risk].[spSaveRiskScores]
    @RiskScores [Risk].[tpRiskScore] READONLY
AS 
BEGIN
SET NOCOUNT ON;
MERGE INTO [Risk].[tRiskScore] as Target
USING @RiskScores as Source
ON (Target.RiskID = Source.RiskID AND Target.[Year] = Source.[Year])
WHEN MATCHED THEN
	UPDATE Set LoFScore = Source.LoFScore, RiskScore = Source.RiskScore
WHEN NOT MATCHED THEN
	INSERT (RiskID, [Year],LoFScore,RiskScore)
	VALUES (Source.RiskID, Source.[Year],Source.LoFScore,Source.RiskScore)
;
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRiskScores] TO [TEUser]
    AS [dbo];

