﻿
CREATE PROCEDURE [Risk].[spGetDefaultRiskDetailCategory]
AS
BEGIN
    SELECT [RiskDetailCategoryID]
		  ,[OwningAssetID]
          ,[RiskDetailTypeID]
          ,[RiskDetailScoreTypeID]
          ,[Definition]
          ,[Category]
    FROM [Risk].[tRiskDetailCategory]
    WHERE [OwningAssetID] IS NULL
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetDefaultRiskDetailCategory] TO [TEUser]
    AS [dbo];

