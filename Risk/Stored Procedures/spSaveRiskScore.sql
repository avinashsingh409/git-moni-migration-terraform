﻿CREATE PROCEDURE [Risk].[spSaveRiskScore]
    @RiskID int
    ,@Year smallint
    ,@LoFScore real
    ,@RiskScore real
AS 
BEGIN
SET NOCOUNT ON;
MERGE INTO [Risk].[tRiskScore] as Target
USING (SELECT @RiskID, @Year, @LoFScore, @RiskScore) as Source (RiskID, [Year],LoFScore,RiskScore)
ON (Target.RiskID = Source.RiskID AND Target.[Year] = Source.[Year])
WHEN MATCHED THEN
	UPDATE Set LoFScore = Source.LoFScore, RiskScore = Source.RiskScore
WHEN NOT MATCHED THEN
	INSERT (RiskID, [Year],LoFScore,RiskScore)
	VALUES (Source.RiskID, Source.[Year],Source.LoFScore,Source.RiskScore)
;
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRiskScore] TO [TEUser]
    AS [dbo];

