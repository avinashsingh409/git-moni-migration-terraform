﻿
CREATE PROCEDURE [Risk].[spCountRisksByConsequenceCategory]
	@securityUserID int,
	@assetID int = NULL,
	--@startDate datetime = NULL,
	--@endDate datetime = NULL,
	@changedStartDate datetime = NULL,
	@changedEndDate datetime = NULL,
	@closedStartDate datetime = NULL,
	@closedEndDate datetime = NULL,
	@createdStartDate datetime = NULL,
	@createdEndDate datetime = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	--@impactCostHigh float = NULL,
	--@impactCostLow float = NULL,
	--@assignedTo NVARCHAR(255) = NULL,
	--@mitigated bit = NULL,
	--@priority NVARCHAR(255) = NULL,
	@riskAcceptabilities Base.tpIntList READONLY,
	@riskConfidences Base.tpIntList READONLY,
	@riskNatures Base.tpIntList  READONLY,
	@riskScoreCalcTypes Base.tpIntList READONLY,
	@riskStages Base.tpIntList READONLY
AS
BEGIN
	SET NOCOUNT ON;
	SET ARITHABORT ON
	
	DECLARE @assetIDs Base.tpIntList
	
    INSERT into @assetIDs SELECT a.AssetID from Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,@assetID) a
    
	DECLARE @tmp_riskIDs Base.tpIntList;
	Insert into @tmp_riskIDs select * from [Risk].[ufnGetFilteredAssetRiskIDsFromAssetIDs]( 
							--@startDate, @endDate, 
							@changedStartDate,@changedEndDate,@closedStartDate,@closedEndDate,@createdStartDate,@createdEndDate, 
							@id, @title, 
							--@impactCostHigh,@impactCostLow,@assignedTo,
							@riskAcceptabilities,@riskConfidences,@riskNatures,@riskScoreCalcTypes,@riskStages, @assetIDs
							--, @scorecard,@priority
							)

	
	SELECT tRDC.Category, sum(riskLikelihood.LoFValue * tRDST.Value) SummedCoFTimesLoF, count(*) NumberOfConsequences
	FROM Risk.tRisk tR
	--Join to filter on asset tree
	INNER JOIN @assetIDs branch on tR.OwningAssetID = branch.ID
	inner join [Risk].[ufnGetRiskLoFs](@tmp_riskIDs,0) riskLikelihood on tR.RiskID = riskLikelihood.RiskID
	INNER JOIN Risk.tRiskDetail tRD on tR.RiskID = tRD.RiskID
	inner join Risk.tRiskDetailCategory tRDC on tRD.RiskDetailCategoryID = tRDC.RiskDetailCategoryID
	inner join Risk.tRiskDetailType tRDT on tRDT.RiskDetailTypeID = tRDC.RiskDetailTypeID
	inner join risk.tRiskDetailScoreType tRDST on tRDST.RiskDetailScoreTypeID = tRDC.RiskDetailScoreTypeID
	where tRDT.Description like 'Consequence'
	group by tRDC.Category

	
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spCountRisksByConsequenceCategory] TO [TEUser]
    AS [dbo];

