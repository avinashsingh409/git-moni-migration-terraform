﻿
CREATE PROCEDURE [Risk].[spDeleteRiskDetail]
   @riskDetailID int
AS 
BEGIN
   DELETE FROM [Risk].[tRiskDetail]
   WHERE [RiskDetailID] = @riskDetailID
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spDeleteRiskDetail] TO [TEUser]
    AS [dbo];

