﻿
CREATE PROCEDURE [Risk].[spGetRiskDetailScoreType]
    @riskDetailScoreTypeID int = null
   ,@owningAssetID int = null
AS
BEGIN
    SELECT [RiskDetailScoreTypeID]
		  ,[OwningAssetID]
          ,[Value]
          ,[Description]
    FROM [Risk].[tRiskDetailScoreType]
    WHERE (@owningAssetID IS NOT NULL AND [OwningAssetID] = @owningAssetID) 
		  OR
		  (@riskDetailScoreTypeID IS NOT NULL AND [RiskDetailScoreTypeID] = @riskDetailScoreTypeID)
END
GO
GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskDetailScoreType] TO [TEUser]
    AS [dbo];

