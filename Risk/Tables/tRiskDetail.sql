﻿CREATE TABLE [Risk].[tRiskDetail]
	(
	RiskDetailID int IDENTITY(1,1) NOT NULL,
	RiskID int NOT NULL,
	RiskDetailCategoryID int NOT NULL,
	Assumptions nvarchar(MAX) NULL,
	StartDate date NOT NULL,
	ScoreWeight real NOT NULL DEFAULT(1)
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskDetail] ADD CONSTRAINT
	[FK_Risk.tRiskDetail_Risk.tRiskDetailCategory] FOREIGN KEY
	(
	RiskDetailCategoryID
	) REFERENCES [Risk].[tRiskDetailCategory]
	(
	RiskDetailCategoryID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRiskDetail] ADD CONSTRAINT
	[FK_Risk.tRiskDetail_Risk.tRisk] FOREIGN KEY
	(
	RiskID
	) REFERENCES [Risk].[tRisk]
	(
	RiskID
	) ON UPDATE  NO ACTION 
	 ON DELETE CASCADE
GO
ALTER TABLE [Risk].[tRiskDetail] ADD CONSTRAINT
	[PK_Risk.tRiskDetail] PRIMARY KEY CLUSTERED 
	(
	RiskDetailID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]