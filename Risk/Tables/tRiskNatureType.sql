﻿CREATE TABLE [Risk].[tRiskNatureType]
	(
	RiskNatureTypeID int NOT NULL,
	OwningAssetID int NULL,
	PrimaryNature nvarchar(50) NOT NULL,
	SubNature nvarchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskNatureType] ADD CONSTRAINT
	[FK_Risk.tRiskNatureType_tAsset] FOREIGN KEY
	(
	OwningAssetID
	) REFERENCES Asset.tAsset
	(
	AssetID
	) ON UPDATE  NO ACTION 
	 ON DELETE CASCADE
GO
ALTER TABLE [Risk].[tRiskNatureType] ADD CONSTRAINT
	[PK_Risk.tRiskNatureType] PRIMARY KEY CLUSTERED 
	(
	RiskNatureTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Risk', @level1type=N'TABLE',@level1name=N'tRiskNatureType'