﻿CREATE TABLE [Risk].[tRiskDetailType]
	(
	RiskDetailTypeID int NOT NULL,
	Description nvarchar(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskDetailType] ADD CONSTRAINT
	[PK_Risk.tRiskDetailType] PRIMARY KEY CLUSTERED 
	(
	RiskDetailTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Risk', @level1type=N'TABLE',@level1name=N'tRiskDetailType'