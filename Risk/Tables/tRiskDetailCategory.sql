﻿CREATE TABLE [Risk].[tRiskDetailCategory]
	(
	RiskDetailCategoryID int NOT NULL,
	OwningAssetID int NULL,
	RiskDetailTypeID int NOT NULL,
	RiskDetailScoreTypeID int NOT NULL,
	Definition nvarchar(MAX) NOT NULL,
	Category nvarchar(50) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskDetailCategory] ADD CONSTRAINT
	[FK_Risk.tRiskDetailCategory_Risk.tRiskDetailType] FOREIGN KEY
	(
	RiskDetailTypeID
	) REFERENCES [Risk].[tRiskDetailType]
	(
	RiskDetailTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRiskDetailCategory] ADD CONSTRAINT
	[FK_Risk.tRiskDetailCategory_Risk.tRiskDetailScoreType] FOREIGN KEY
	(
	RiskDetailScoreTypeID
	) REFERENCES [Risk].[tRiskDetailScoreType]
	(
	RiskDetailScoreTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRiskDetailCategory] ADD CONSTRAINT
	[FK_Risk.tRiskDetailCategory_tAsset] FOREIGN KEY
	(
	OwningAssetID
	) REFERENCES Asset.tAsset
	(
	AssetID
	) ON UPDATE  NO ACTION 
	 ON DELETE CASCADE
GO
ALTER TABLE [Risk].[tRiskDetailCategory] ADD CONSTRAINT
	[PK_Risk.tRiskDetailCategory] PRIMARY KEY CLUSTERED 
	(
	RiskDetailCategoryID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]