﻿CREATE TABLE [Risk].[tRiskScore] (
    [RiskID]    INT      NOT NULL,
    [Year]      SMALLINT NOT NULL,
    [LoFScore]  REAL     NOT NULL,
    [RiskScore] REAL     NOT NULL,
    CONSTRAINT [PK_Risk.tRiskScore] PRIMARY KEY CLUSTERED ([RiskID] ASC, [Year] ASC),
    CONSTRAINT [FK_Risk.tRiskScore_Risk.tRisk] FOREIGN KEY ([RiskID]) REFERENCES [Risk].[tRisk] ([RiskID]) ON DELETE CASCADE
);

