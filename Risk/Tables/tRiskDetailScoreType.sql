﻿CREATE TABLE [Risk].[tRiskDetailScoreType]
	(
	RiskDetailScoreTypeID int NOT NULL,
	OwningAssetID int NULL,
	Value real NOT NULL,
	Description nvarchar(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskDetailScoreType] ADD CONSTRAINT
	[FK_Risk.tRiskDetailScoreType_tAsset] FOREIGN KEY
	(
	OwningAssetID
	) REFERENCES Asset.tAsset
	(
	AssetID
	) ON UPDATE  NO ACTION 
	 ON DELETE CASCADE
GO
ALTER TABLE [Risk].[tRiskDetailScoreType] ADD CONSTRAINT
	[PK_Risk.tRiskDetailScoreType] PRIMARY KEY CLUSTERED 
	(
	RiskDetailScoreTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Risk', @level1type=N'TABLE',@level1name=N'tRiskDetailScoreType'