﻿CREATE TABLE [Risk].[tRiskAcceptabilityType]
	(
	RiskAcceptabilityTypeID int NOT NULL,
	Value real NOT NULL,
	Description nvarchar(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskAcceptabilityType] ADD CONSTRAINT
	[PK_Risk.tRiskAcceptabilityType] PRIMARY KEY CLUSTERED 
	(
	RiskAcceptabilityTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Risk', @level1type=N'TABLE',@level1name=N'tRiskAcceptabilityType'