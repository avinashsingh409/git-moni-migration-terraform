﻿CREATE TABLE [Risk].[tRisk]
	(
	RiskID int IDENTITY(1,1) NOT NULL,
	OwningAssetID int NOT NULL,
	RiskNatureTypeID int NULL,
	RiskStageTypeID int NULL,
	RiskConfidenceTypeID int NULL,
	RiskAcceptabilityTypeID int NULL,
	RiskScoreCalcTypeID int NOT NULL,
	Title nvarchar(128) NOT NULL,
	Cause nvarchar(MAX) NULL,
	ExistingControl nvarchar(MAX) NULL,
	ControlAdequacy nvarchar(MAX) NULL,
	CreatedByID int NOT NULL,
	ChangedByID int NOT NULL,
	CreatedDate date NOT NULL,
	OpenedDate date NOT NULL,
	ChangedDate date NOT NULL,
	ClosedDate date NULL, 
    [Description] NVARCHAR(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_tUser] FOREIGN KEY
	(
	CreatedByID
	) REFERENCES AccessControl.tUser
	(
	SecurityUserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_tUser1] FOREIGN KEY
	(
	ChangedByID
	) REFERENCES AccessControl.tUser
	(
	SecurityUserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_tAsset] FOREIGN KEY
	(
	OwningAssetID
	) REFERENCES Asset.tAsset
	(
	AssetID
	) ON UPDATE  NO ACTION 
	 ON DELETE CASCADE
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_Risk.tRiskNatureType] FOREIGN KEY
	(
	RiskNatureTypeID
	) REFERENCES [Risk].[tRiskNatureType]
	(
	RiskNatureTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_Risk.tRiskStageType] FOREIGN KEY
	(
	RiskStageTypeID
	) REFERENCES [Risk].[tRiskStageType]
	(
	RiskStageTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_Risk.tRiskConfidenceType] FOREIGN KEY
	(
	RiskConfidenceTypeID
	) REFERENCES [Risk].[tRiskConfidenceType]
	(
	RiskConfidenceTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_Risk.tRiskAcceptabilityType] FOREIGN KEY
	(
	RiskAcceptabilityTypeID
	) REFERENCES [Risk].[tRiskAcceptabilityType]
	(
	RiskAcceptabilityTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[FK_Risk.tRisk_Risk.tRiskScoreCalcType] FOREIGN KEY
	(
	RiskScoreCalcTypeID
	) REFERENCES [Risk].[tRiskScoreCalcType]
	(
	RiskScoreCalcTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
GO
ALTER TABLE [Risk].[tRisk] ADD CONSTRAINT
	[PK_Risk.tRisk] PRIMARY KEY CLUSTERED 
	(
	RiskID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]