﻿CREATE TABLE [Risk].[tRiskScoreCalcType]
	(
	RiskScoreCalcTypeID int NOT NULL,
	Description nvarchar(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE [Risk].[tRiskScoreCalcType] ADD CONSTRAINT
	[PK_Risk.tRiskScoreCalcType] PRIMARY KEY CLUSTERED 
	(
	RiskScoreCalcTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Risk', @level1type=N'TABLE',@level1name=N'tRiskScoreCalcType'