CREATE TABLE [CQIIPM].[tCQIResultIPMBoilerSlicePerformance] (
    [CQIResultIPMBoilerSlicePerformanceID]     INT           IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                              INT           NOT NULL,
    [SliceNumber]                              INT           NOT NULL,
    [SliceName]                                NVARCHAR (50) NOT NULL,
    [SliceOrder]                               INT           NOT NULL,
    [SliceHeight_ft]                           REAL          NOT NULL,
    [GasInletTemp_f]                           REAL          NOT NULL,
    [GasOutletTemp_f]                          REAL          NOT NULL,
    [GasMassFlowRate_lbhr]                     REAL          NOT NULL,
    [GasVelocity_ftsec]                        REAL          NOT NULL,
    [HeatTransferedToWall_mbtuhr]              REAL          NOT NULL,
    [HeatTransferedToTube_mbtuhr]              REAL          NOT NULL,
    [WaterInletEnthalpy_btulbm]                REAL          NOT NULL,
    [WaterInletPressure_psia]                  REAL          NOT NULL,
    [WaterOutletEnthalpy_btulbm]               REAL          NOT NULL,
    [WaterOutletPressure_psia]                 REAL          NOT NULL,
    [WaterSideHeatTransferCoefficient_btuft2f] REAL          NOT NULL,
    [OverallHeatTransferCoefficient_btuft2f]   REAL          NOT NULL,
    [TubeSurfaceTemp_f]                        REAL          NOT NULL,
    [TubeBankExist]                            INT           NOT NULL,
    [TubeLocation]                             INT           NOT NULL,
    [SteamFlowOrder]                           INT           NOT NULL,
    [TubeDuty]                                 INT           NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMBoilerSlicePerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMBoilerSlicePerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMBoilerSlicePerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMBoilerSlicePerformance';

