CREATE TABLE [CQIIPM].[tCQIResultIPMBoilerPerformance] (
    [CQIResultIPMBoilerPerformanceID]                   INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                                       INT  NOT NULL,
    [BoilerEfficiency_percent]                          REAL NOT NULL,
    [BECreditSensibleHeatInSorbent_percent]             REAL NOT NULL,
    [BECreditSulfation_percent]                         REAL NOT NULL,
    [BELossCalcination_percent]                         REAL NOT NULL,
    [BELossCO_percent]                                  REAL NOT NULL,
    [BELossHotAQC_percent]                              REAL NOT NULL,
    [BELossRadiationToAshHopper_percent]                REAL NOT NULL,
    [BELossRadiationToAmbient_percent]                  REAL NOT NULL,
    [BELossSensibleHeatInRefuse_percent]                REAL NOT NULL,
    [BELossMoistureInSorbent_percent]                   REAL NOT NULL,
    [BELossUserUnaccountedFor_percent]                  REAL NOT NULL,
    [SplitBackpassFrontFraction_ratio]                  REAL NOT NULL,
    [SplitBackpassRearFraction_ratio]                   REAL NOT NULL,
    [FurnaceExitGasTemperature_f]                       REAL NOT NULL,
    [TemperatureAtEconomizerExit_f]                     REAL NOT NULL,
    [FurnacePressure_inwg]                              REAL NOT NULL,
    [ExcessO2_percent]                                  REAL NOT NULL,
    [BurnerTilt_degree]                                 REAL NOT NULL,
    [OverfireAirFlow1_kpph]                             REAL NOT NULL,
    [OverfireAirFlow2_kpph]                             REAL NOT NULL,
    [CO_ppm]                                            REAL NOT NULL,
    [NumLowerFurnSlice_ratio]                           INT  NOT NULL,
    [NumUpperFurnSlice_ratio]                           INT  NOT NULL,
    [NumConvPassSlice_ratio]                            INT  NOT NULL,
    [CleanlinessFurnace_ratio]                          REAL NOT NULL,
    [CleanlinessEconomizer_ratio]                       REAL NOT NULL,
    [CleanlinessPrimarySuperheater_ratio]               REAL NOT NULL,
    [CleanlinessSecondarySuperheaterPlaten_ratio]       REAL NOT NULL,
    [CleanlinessSecondarySuperheaterPendant_ratio]      REAL NOT NULL,
    [CleanlinessRadientSuperheaterFirstStg_ratio]       REAL NOT NULL,
    [CleanlinessRadientSuperheaterSecStg_ratio]         REAL NOT NULL,
    [CleanlinessScaleFurnace_ratio]                     REAL NOT NULL,
    [CleanlinessScaleEconomizer_ratio]                  REAL NOT NULL,
    [CleanlinessScalePrimarySuperheater_ratio]          REAL NOT NULL,
    [CleanlinessScaleSecondarySuperheaterPlaten_ratio]  REAL NOT NULL,
    [CleanlinessScaleSecondarySuperheaterPendant_ratio] REAL NOT NULL,
    [CleanlinessScaleRadientSuperheaterFirstStg_ratio]  REAL NOT NULL,
    [CleanlinessScaleRadientSuperheaterSecStg_ratio]    REAL NOT NULL,
    [CleanlinessFurnaceUpper_ratio]                     REAL NOT NULL,
    [CleanlinessFurnaceRoof_ratio]                      REAL NOT NULL,
    [CleanlinessScaleFurnaceUpper_ratio]                REAL NOT NULL,
    [CleanlinessScaleFurnaceRoof_ratio]                 REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMBoilerPerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMBoilerPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMBoilerPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMBoilerPerformance';

