CREATE TABLE [CQIIPM].[tCQIResultIPMCyclonePerformance] (
    [CQIResultIPMCyclonePerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                      INT  NOT NULL,
    [HeatAbsorbFraction_ratio]         REAL NOT NULL,
    [HeatAbsorbTotal_mbtuhr]           REAL NOT NULL,
    [TotalHeatInput_mbtuhr]            REAL NOT NULL,
    [IsCyclone1OutOfService_ratio]     BIT  NOT NULL,
    [IsCyclone2OutOfService_ratio]     BIT  NOT NULL,
    [IsCyclone3OutOfService_ratio]     BIT  NOT NULL,
    [IsCyclone4OutOfService_ratio]     BIT  NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMCyclonePerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMCyclonePerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMCyclonePerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMCyclonePerformance';

