CREATE TABLE [CQIIPM].[tCQIResultIPMFanPerformance] (
    [CQIResultIPMFanPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                  INT  NOT NULL,
    [IsFDFan1OutOfService]         BIT  NOT NULL,
    [IsFDFan2OutOfService]         BIT  NOT NULL,
    [IsIDFan1OutOfService]         BIT  NOT NULL,
    [IsIDFan2OutOfService]         BIT  NOT NULL,
    [IsPAFan1OutOfService]         BIT  NOT NULL,
    [IsPAFan2OutOfService]         BIT  NOT NULL,
    [IsPAFan3OutOfService]         BIT  NOT NULL,
    [IsPAFan4OutOfService]         BIT  NOT NULL,
    [IsPAFan5OutOfService]         BIT  NOT NULL,
    [IsPAFan6OutOfService]         BIT  NOT NULL,
    [FDFan1TempRise_f]             REAL NOT NULL,
    [FDFan2TempRise_f]             REAL NOT NULL,
    [IDFan1TempRise_f]             REAL NOT NULL,
    [IDFan2TempRise_f]             REAL NOT NULL,
    [FDFan1Flow_acfm]              REAL NOT NULL,
    [FDFan2Flow_acfm]              REAL NOT NULL,
    [IDFan1Flow_acfm]              REAL NOT NULL,
    [IDFan2Flow_acfm]              REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMFanPerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMFanPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMFanPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMFanPerformance';

