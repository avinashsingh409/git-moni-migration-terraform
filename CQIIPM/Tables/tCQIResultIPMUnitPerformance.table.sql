CREATE TABLE [CQIIPM].[tCQIResultIPMUnitPerformance] (
    [CQIResultIPMUnitPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                   INT  NOT NULL,
    [GrossLoad_mw]                  REAL NOT NULL,
    [NetLoad_mw]                    REAL NOT NULL,
    [NetPlantHeatrate_btukwh]       REAL NOT NULL,
    [CoalBurnRate_kpph]             REAL NOT NULL,
    [GasBurnRate_kpph]              REAL NOT NULL,
    [OilBurnRate_kpph]              REAL NOT NULL,
    [LimestoneFeedRate_kpph]        REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMUnitPerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMUnitPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMUnitPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMUnitPerformance';

