CREATE TABLE [CQIIPM].[tCQIResultIPMAirHeaterPerformance] (
    [CQIResultIPMAirHeaterPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                        INT  NOT NULL,
    [LeakagePrimary_percent]             REAL NOT NULL,
    [LeakageSecondaryA_percent]          REAL NOT NULL,
    [LeakageSecondaryB_percent]          REAL NOT NULL,
    [LeakageFlowPrimary_kpph]            REAL NOT NULL,
    [LeakageFlowSecondaryA_kpph]         REAL NOT NULL,
    [LeakageFlowSecondaryB_kpph]         REAL NOT NULL,
    [AirInletFlowPrimary_kpph]           REAL NOT NULL,
    [AirInletFlowSecondaryA_kpph]        REAL NOT NULL,
    [AirInletFlowSecondaryB_kpph]        REAL NOT NULL,
    [AirOutletFlowPrimary_kpph]          REAL NOT NULL,
    [AirOutletFlowSecondaryA_kpph]       REAL NOT NULL,
    [AirOutletFlowSecondaryB_kpph]       REAL NOT NULL,
    [GasInletFlowPrimary_kpph]           REAL NOT NULL,
    [GasInletFlowSecondaryA_kpph]        REAL NOT NULL,
    [GasInletFlowSecondaryB_kpph]        REAL NOT NULL,
    [GasOutletFlowPrimary_kpph]          REAL NOT NULL,
    [GasOutletFlowSecondaryA_kpph]       REAL NOT NULL,
    [GasOutletFlowSecondaryB_kpph]       REAL NOT NULL,
    [AirInletTempPrimary_f]              REAL NOT NULL,
    [AirInletTempSecondaryA_f]           REAL NOT NULL,
    [AirInletTempSecondaryB_f]           REAL NOT NULL,
    [AirOutletTempPrimary_f]             REAL NOT NULL,
    [AirOutletTempSecondaryA_f]          REAL NOT NULL,
    [AirOutletTempSecondaryB_f]          REAL NOT NULL,
    [GasInletTempPrimary_f]              REAL NOT NULL,
    [GasInletTempSecondaryA_f]           REAL NOT NULL,
    [GasInletTempSecondaryB_f]           REAL NOT NULL,
    [GasOutletTempPrimary_f]             REAL NOT NULL,
    [GasOutletTempSecondaryA_f]          REAL NOT NULL,
    [GasOutletTempSecondaryB_f]          REAL NOT NULL,
    [GasFractionPrimary_ratio]           REAL NOT NULL,
    [GasFractionSecondary_ratio]         REAL NOT NULL,
    [CleanlinessPrimary_ratio]           REAL NOT NULL,
    [CleanlinessSecondaryA_ratio]        REAL NOT NULL,
    [CleanlinessSecondaryB_ratio]        REAL NOT NULL,
    [CleanlinessScalePrimary_ratio]      REAL NOT NULL,
    [CleanlinessScaleSecondaryA_ratio]   REAL NOT NULL,
    [CleanlinessScaleSecondaryB_ratio]   REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMAirHeaterPerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMAirHeaterPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMAirHeaterPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMAirHeaterPerformance';

