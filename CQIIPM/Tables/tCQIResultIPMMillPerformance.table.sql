CREATE TABLE [CQIIPM].[tCQIResultIPMMillPerformance] (
    [CQIResultIPMMillPerformanceID] INT  IDENTITY (1, 1) NOT NULL,
    [CQIResultID]                   INT  NOT NULL,
    [OutletTemp_f]                  REAL NOT NULL,
    [DryingFraction_ratio]          REAL NOT NULL,
    [PAtoFuelRatio_ratio]           REAL NOT NULL,
    [IsMill1OutOfService_ratio]     BIT  NOT NULL,
    [IsMill2OutOfService_ratio]     BIT  NOT NULL,
    [IsMill3OutOfService_ratio]     BIT  NOT NULL,
    [IsMill4OutOfService_ratio]     BIT  NOT NULL,
    [IsMill5OutOfService_ratio]     BIT  NOT NULL,
    [IsMill6OutOfService_ratio]     BIT  NOT NULL,
    [Mill1Bias_ratio]               REAL NOT NULL,
    [Mill2Bias_ratio]               REAL NOT NULL,
    [Mill3Bias_ratio]               REAL NOT NULL,
    [Mill4Bias_ratio]               REAL NOT NULL,
    [Mill5Bias_ratio]               REAL NOT NULL,
    [Mill6Bias_ratio]               REAL NOT NULL,
    CONSTRAINT [PK_tCQIResultIPMMillPerformance] PRIMARY KEY CLUSTERED ([CQIResultIPMMillPerformanceID] ASC),
    CONSTRAINT [FK_tCQIResultIPMMillPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIResultIPMMillPerformance';

