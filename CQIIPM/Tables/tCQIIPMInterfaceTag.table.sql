CREATE TABLE [CQIIPM].[tCQIIPMInterfaceTag] (
    [CQIIPMInterfaceTagID] INT            IDENTITY (1, 1) NOT NULL,
    [UnitID]               INT            NOT NULL,
    [CQIVariableName]      NVARCHAR (MAX) NULL,
    [OPMTagName]           NVARCHAR (MAX) NULL,
    [IsScenario]           BIT            NOT NULL,
    [IsPredictionInput]    BIT            NOT NULL,
    [IsPredictionOutput]   BIT            NOT NULL,
    CONSTRAINT [PK_tCQIIPMInterfaceTag] PRIMARY KEY CLUSTERED ([CQIIPMInterfaceTagID] ASC),
    CONSTRAINT [FK_tCQIIPMInterfaceTag_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIIPMInterfaceTag';

