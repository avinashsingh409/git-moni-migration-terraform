CREATE TABLE [CQIIPM].[tCQIIPMServerFile] (
    [CQIIPMServerFileID] INT            IDENTITY (1, 1) NOT NULL,
    [UnitID]             INT            NOT NULL,
    [PDServerID]         INT            NOT NULL,
    [IPMScenarioFile]    NVARCHAR (MAX) NOT NULL,
    [IPMInputFile]       NVARCHAR (MAX) NOT NULL,
    [IPMOutputFile]      NVARCHAR (MAX) NOT NULL,
    [IPMRunPath]         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tCQIIPMServerFile] PRIMARY KEY CLUSTERED ([CQIIPMServerFileID] ASC),
    CONSTRAINT [FK_tCQIIPMServerFile_PDServerID_tPDServer] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]),
    CONSTRAINT [FK_tCQIIPMServerFile_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'CQIIPM', @level1type = N'TABLE', @level1name = N'tCQIIPMServerFile';

