CREATE TABLE [FuelPro].[tEvaluationFuelSourceMap] (
    [EvalOfferMapID] INT            IDENTITY (1, 1) NOT NULL,
    [EvaluationID]   INT            NOT NULL,
    [FuelSourceID]   INT            NOT NULL,
    [StationID]      INT            NOT NULL,
    [Year]           SMALLINT       NOT NULL,
    [AnnualTons]     INT            NOT NULL,
    [DeliveryMethod] NVARCHAR (40)  NOT NULL,
    [CreatedBy]      NVARCHAR (255) NOT NULL,
    [ChangedBy]      NVARCHAR (255) NOT NULL,
    [CreateDate]     DATETIME       CONSTRAINT [DF__tEvalOfferMap__Creat__695C9DA1] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]     DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tEvalOffer] PRIMARY KEY CLUSTERED ([EvalOfferMapID] ASC),
    CONSTRAINT [FK_tEvalOfferMap_EvaluationID_tEvaluation] FOREIGN KEY ([EvaluationID]) REFERENCES [FuelPro].[tEvaluation] ([EvaluationID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tEvalOfferMap_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]),
    CONSTRAINT [FK_tEvalOfferMap_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User defined collection of offers(fuel sources) with annual available tons for a FuelPro Evaluation.', @level0type = N'SCHEMA', @level0name = N'FuelPro', @level1type = N'TABLE', @level1name = N'tEvaluationFuelSourceMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelPro', @level1type = N'TABLE', @level1name = N'tEvaluationFuelSourceMap';

