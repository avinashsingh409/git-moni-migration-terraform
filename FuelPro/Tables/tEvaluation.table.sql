CREATE TABLE [FuelPro].[tEvaluation] (
    [EvaluationID]            INT            IDENTITY (1, 1) NOT NULL,
    [EvaluationDesc]          NVARCHAR (255) NOT NULL,
    [GenerationSetID]         INT            NULL,
    [FuelPlanSetID]           INT            NULL,
    [UnitConfigScheduleSetID] INT            NULL,
    [RunDate]                 DATETIME       NULL,
    [RunStatus]               NVARCHAR (40)  NULL,
    [CreatedBy]               NVARCHAR (255) NOT NULL,
    [ChangedBy]               NVARCHAR (255) NOT NULL,
    [CreateDate]              DATETIME       CONSTRAINT [DF__tEvaluation__Creat__695C9DA1] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]              DATETIME       CONSTRAINT [DF__tEvaluati__Chang__4A78EF25] DEFAULT (getdate()) NOT NULL,
    [FuelProEvalID]           INT            NULL,
    [ForecastSetID]           INT            NOT NULL,
    [RunFailure]              NVARCHAR (255) NULL,
    CONSTRAINT [PK_tEvaluation] PRIMARY KEY CLUSTERED ([EvaluationID] ASC),
    CONSTRAINT [FK_tEvaluation_ForecastSetID_tForecastSet] FOREIGN KEY ([ForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID]),
    CONSTRAINT [FK_tEvaluation_FuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE SET NULL,
    CONSTRAINT [FK_tEvaluation_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE SET NULL,
    CONSTRAINT [FK_tEvaluation_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]) ON DELETE SET NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User defined collection of data sets for a FuelPro evaluation.', @level0type = N'SCHEMA', @level0name = N'FuelPro', @level1type = N'TABLE', @level1name = N'tEvaluation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID of evaluation in FuelPro database', @level0type = N'SCHEMA', @level0name = N'FuelPro', @level1type = N'TABLE', @level1name = N'tEvaluation', @level2type = N'COLUMN', @level2name = N'FuelProEvalID';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'FuelPro', @level1type = N'TABLE', @level1name = N'tEvaluation';

