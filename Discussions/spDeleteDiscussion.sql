﻿CREATE PROCEDURE Discussions.spDeleteDiscussion
	@SecurityUserID INT,
	@DiscussionID UNIQUEIDENTIFIER,
	@Date DateTimeOffset = NULL
AS
BEGIN
	
INSERT INTO Discussions.tDiscussionAttachmentArchive(	
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		[Deleted]
	)
	SELECT 
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		COALESCE(@Date, SYSDATETIMEOFFSET()),
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		1
	FROM Discussions.tDiscussionAttachment WHERE DiscussionID = @DiscussionID
	OR DiscussionEntryID in (SELECT DiscussionEntryID FROM Discussions.tDiscussionEntry WHERE DiscussionID = @DiscussionID)

INSERT INTO Discussions.tDiscussionEntryArchive(	
		[DiscussionEntryID],
		[DiscussionID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Content],
		[Approved],
		[Deleted])
	SELECT 
		[DiscussionEntryID],
		[DiscussionID],
		[OwnerUserID],
		[CreateDateZ],
		COALESCE(@Date, SYSDATETIMEOFFSET()),
		[CreatedByUserID],
		@SecurityUserID,
		[AXExternalID],
		[Title],
		[Subtitle],
		[Content],
		[Approved],
		1
	FROM Discussions.tDiscussionEntry WHERE DiscussionID = @DiscussionID

INSERT INTO Discussions.tDiscussionArchive(	
		[DiscussionID],
		[AssetID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Moderated],
		[Deleted])
	SELECT 
		[DiscussionID],
		[AssetID],
		[OwnerUserID],
		[CreateDateZ],
		COALESCE(@Date, SYSDATETIMEOFFSET()),
		[CreatedByUserID],
		@SecurityUserID,
		[AXExternalID],
		[Title],
		[Subtitle],
		[Moderated],
		1
	FROM Discussions.tDiscussion WHERE DiscussionID = @DiscussionID
	  
DELETE FROM Discussions.tDiscussionAttachment
WHERE Discussions.tDiscussionAttachment.DiscussionID = @DiscussionID
OR Discussions.tDiscussionAttachment.DiscussionEntryID in (SELECT Discussions.tDiscussionEntry.DiscussionEntryID FROM Discussions.tDiscussionEntry WHERE Discussions.tDiscussionEntry.DiscussionID = @DiscussionID)

DELETE FROM Discussions.tDiscussionEntry 
WHERE DiscussionID = @DiscussionID

DELETE FROM Discussions.tDiscussion
WHERE DiscussionID = @DiscussionID

END
GO
GRANT EXECUTE
    ON OBJECT::[Discussions].[spDeleteDiscussion] TO [TEUser]
    AS [dbo];