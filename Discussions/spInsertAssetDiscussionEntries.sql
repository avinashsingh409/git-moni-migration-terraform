﻿CREATE PROCEDURE [Discussions].[spInsertAssetDiscussionEntries] 	
	@entries [Discussions].[tpAssetDiscussionEntry] READONLY
AS
BEGIN

DECLARE @insertedDiscussion as TABLE 
(
  AssetID int,
  DiscussionID uniqueidentifier
)

INSERT INTO Discussions.tDiscussion([DiscussionID],[AssetID],[OwnerUserID],[CreatedByUserID],[ChangedByUserID],
					[AXExternalID],[Title],[Subtitle], [CreateDateZ],[ChangeDateZ],[Moderated])
OUTPUT inserted.AssetID, inserted.DiscussionID into @insertedDiscussion
SELECT NEWID(),A.* FROM (SELECT DISTINCT a.AssetID,[OwnerUserID],[CreatedByUserID],[ChangedByUserID],[AXExternalID],
              'Discussion for ' + b.AssetDesc as Title,null as SubTitle, [CreateDateZ],[ChangeDateZ],0 as Moderated FROM @entries a JOIN Asset.tAsset b on A.AssetID = b.AssetID
			  WHERE A.DiscussionID IS NULL) a 

DELETE a from Asset.tDiscussionAssetMap a JOIN @insertedDiscussion b on A.assetid = b.AssetID where A.type = 1

INSERT INTO Asset.tDiscussionAssetMap(DiscussionID, AssetID, [Type])
SELECT DiscussionID,AssetID,1 FROM @insertedDiscussion

DECLARE @inserted AS base.tpGuidList

INSERT INTO Discussions.tDiscussionEntry([DiscussionEntryID],[DiscussionID],[OwnerUserID],
		[CreatedByUserID],[ChangedByUserID],[AXExternalID],[Title],[Subtitle],[Content],
		[ReferencedEntry],[ReferencedContent], [CreateDateZ], [ChangeDateZ],[Approved], [AutoGenerated])
output inserted.DiscussionEntryID into @inserted
select NEWID(),ISNULL(e.[DiscussionID],b.DiscussionID),[OwnerUserID],[CreatedByUserID],[ChangedByUserID],[AXExternalID],
		[Title],[Subtitle],[Content],[ReferencedEntry],[ReferencedContent], [CreateDateZ], [ChangeDateZ],
		[Approved], [AutoGenerated] from @entries e LEFT JOIN @insertedDiscussion b on e.AssetID = b.AssetID


INSERT INTO Discussions.tDiscussionEntryArchive(	
		[DiscussionEntryID],
		[DiscussionID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Content],
		[Approved],
		[AutoGenerated])
	SELECT 
		[DiscussionEntryID],
		[DiscussionID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Content],
		[Approved],
		[AutoGenerated]
	FROM Discussions.tDiscussionEntry e JOIN @inserted i on E.DiscussionEntryID = i.id

SELECT * FROM @inserted  -- return the DisscusionEntryIDs

END
GO

GRANT EXECUTE
    ON OBJECT::[Discussions].[spInsertAssetDiscussionEntries]  TO [TEUser]
    AS [dbo];

GO
