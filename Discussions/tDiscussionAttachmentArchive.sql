﻿CREATE TABLE Discussions.tDiscussionAttachmentArchive(
	[DiscussionAttachmentArchiveID] [INT] NOT NULL IDENTITY(1,1),
	[DiscussionAttachmentID] [UNIQUEIDENTIFIER] NOT NULL,
	[DiscussionID] UNIQUEIDENTIFIER NULL,
	[DiscussionEntryID] UNIQUEIDENTIFIER NULL,
	[CreateDateZ] datetimeoffset NOT NULL,
	[ChangeDateZ] datetimeoffset NOT NULL,
	[CreatedByUserID] int NOT NULL,
	[ChangedByUserID] int NOT NULL,
	[AXExternalID] NVARCHAR(MAX) NULL,
	[ContentID] UNIQUEIDENTIFIER NULL,
	[DisplayInstructions] NVARCHAR(MAX) NULL,
	[DiscussionAttachmentType] INT NOT NULL,
	[Title] NVARCHAR(255) NULL,
	[Deleted] bit DEFAULT 0,
	DisplayOrder INT NOT NULL DEFAULT 1,
	CONSTRAINT [PK_tDiscussionAttachmentArchive] PRIMARY KEY CLUSTERED 
	(
		[DiscussionAttachmentArchiveID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
GO