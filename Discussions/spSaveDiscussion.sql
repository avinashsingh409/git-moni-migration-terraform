﻿CREATE PROCEDURE Discussions.spSaveDiscussion 
	-- Add the parameters for the stored procedure here
	@SecurityUserID INT,
	@DiscussionID UNIQUEIDENTIFIER = NULL OUTPUT,
	@AssetID INT = NULL,
	@OwnerUserID INT = NULL,
	@AXExternalID NVARCHAR(MAX) = NULL,
	@Title NVARCHAR(255) = NULL,
	@Subtitle NVARCHAR(255) = NULL,
	@Date DateTimeOffset = NULL,
	@Moderated BIT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @DiscussionID IS NULL 
	BEGIN
		IF @AssetID IS NULL
		BEGIN
			RAISERROR (N'Cannot create a Discussion with no associated asset',11,11, 'Discussions.spSaveDiscussion');
			RETURN
		END

		if @Moderated IS NULL
			Set @Moderated = 0; 

		IF @OwnerUserID IS NULL
			SET @OwnerUserID = @SecurityUserID;

		IF @Date IS NULL
			SET @Date = SYSDATETIMEOFFSET();

		SET @DiscussionID = NEWID(); 

		INSERT INTO Discussions.tDiscussion([DiscussionID],[AssetID],[OwnerUserID],[CreatedByUserID],[ChangedByUserID],[AXExternalID],[Title],[Subtitle], [CreateDateZ],[ChangeDateZ],[Moderated])
		VALUES (@DiscussionID, @AssetID, @OwnerUserID, @SecurityUserID, @SecurityUserID, @AXExternalID, @Title, @Subtitle,@Date,@Date, @Moderated)

	END
	ELSE
	BEGIN
		DECLARE @oldAssetID INT;
		DECLARE @oldOwnerUserID INT;
		DECLARE @oldExternalID NVARCHAR(MAX);
		DECLARE @oldTitle NVARCHAR(255);
		DECLARE @oldSubtitle NVARCHAR(255);
		DECLARE @oldModerated BIT;

		SELECT TOP 1 @oldAssetID = [AssetID], @oldOwnerUserID = [OwnerUserID], @oldExternalID = AXExternalID, @oldTitle = Title, @oldSubtitle = Subtitle, @oldModerated = Moderated 
		FROM Discussions.tDiscussion 
		WHERE DiscussionID = @DiscussionID

		UPDATE Discussions.tDiscussion 
		SET AssetID = Coalesce( @AssetID , @oldAssetID),
		    [OwnerUserID] = Coalesce( @OwnerUserID, @oldOwnerUserID),
			ChangeDateZ = Coalesce(@Date, SYSDATETIMEOFFSET()),
			ChangedByUserID = @SecurityUserID,
			AXExternalID = COALESCE(@AXExternalID, @oldExternalID),
			Title = COALESCE(@Title, @oldTitle),
			Subtitle = COALESCE(@Subtitle, @oldSubtitle),
			Moderated = COALESCE(@Moderated, @oldModerated)
		WHERE DiscussionID = @DiscussionID
		
	END

	INSERT INTO Discussions.tDiscussionArchive(	
		[DiscussionID],
		[AssetID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Moderated])
	SELECT 
		[DiscussionID],
		[AssetID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Moderated]
	FROM Discussions.tDiscussion WHERE DiscussionID = @DiscussionID

	SELECT @DiscussionID; 

END
GO
GRANT EXECUTE
    ON OBJECT::[Discussions].[spSaveDiscussion] TO [TEUser]
    AS [dbo];