﻿CREATE PROCEDURE Discussions.spSaveDiscussionAttachment 
	-- Add the parameters for the stored procedure here
	@SecurityUserID INT,
	@DiscussionAttachmentID UNIQUEIDENTIFIER = NULL,
	@DiscussionID UNIQUEIDENTIFIER = NULL,
	@DiscussionEntryID UNIQUEIDENTIFIER = NULL,
	@AXExternalID NVARCHAR(MAX) = NULL,
	@ContentID UNIQUEIDENTIFIER = NULL,
	@DisplayInstructions NVARCHAR(MAX) = NULL,
	@AttachmentType INT = NULL,
	@Title NVARCHAR(255) = NULL,
	@Date DATETIMEOFFSET = NULL,
	@DisplayOrder INT = 1
AS
BEGIN
	SET NOCOUNT ON;
	IF @DiscussionAttachmentID IS NULL 
	BEGIN
		IF @DiscussionID IS NULL AND @DiscussionEntryID IS NULL
			RAISERROR (N'Cannot create an Attachment with no associated Discussion or DiscussionEntry',11,11, 'Discussions.spSaveDiscussionAttachment');

		SET @DiscussionAttachmentID = NEWID(); 

		IF @Date IS NULL
			SET @Date = SYSDATETIMEOFFSET();

		INSERT INTO Discussions.tDiscussionAttachment([DiscussionAttachmentID],[DiscussionID],[DiscussionEntryID],[CreateDateZ],[ChangeDateZ],[CreatedByUserID],[ChangedByUserID],[AXExternalID],[ContentID],[DisplayInstructions],[DiscussionAttachmentType],[Title],[DisplayOrder])
		VALUES (@DiscussionAttachmentID,@DiscussionID,@DiscussionEntryID,@Date,@Date,@SecurityUserID,@SecurityUserID,@AXExternalID,@ContentID,@DisplayInstructions,@AttachmentType,@Title,@DisplayOrder)

	END
	ELSE
	BEGIN
		DECLARE @oldTitle NVARCHAR(255);

		SELECT TOP 1 @oldTitle = Title
		FROM Discussions.tDiscussionAttachment 
		WHERE DiscussionAttachmentID = @DiscussionAttachmentID

		UPDATE Discussions.tDiscussionAttachment 
		SET 
			ChangeDateZ = COALESCE(@Date, SYSDATETIMEOFFSET()),
			ChangedByUserID = @SecurityUserID,
			Title = COALESCE(@Title, @oldTitle),
			DisplayOrder = @DisplayOrder
		WHERE DiscussionAttachmentID = @DiscussionAttachmentID
		
	END

	INSERT INTO Discussions.tDiscussionAttachmentArchive(	
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		[DisplayOrder]
	)
	SELECT 
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		[DisplayOrder]
	FROM Discussions.tDiscussionAttachment WHERE DiscussionAttachmentID = @DiscussionAttachmentID

	SELECT @DiscussionAttachmentID; 
END
GO
GRANT EXECUTE
    ON OBJECT::[Discussions].[spSaveDiscussionAttachment] TO [TEUser]
    AS [dbo];