﻿CREATE TABLE [Discussions].[tDiscussionArchive](
	[DiscussionArchiveID] [int] NOT NULL IDENTITY(1,1),
	[DiscussionID] [UNIQUEIDENTIFIER] NOT NULL,
	[AssetID] int NOT NULL,
	[OwnerUserID] int NOT NULL,
	[CreateDateZ] datetimeoffset NOT NULL,
	[ChangeDateZ] datetimeoffset NOT NULL,
	[CreatedByUserID] int NOT NULL,
	[ChangedByUserID] int NOT NULL,
	[AXExternalID] NVARCHAR(MAX) NULL,
	[Title] NVARCHAR(255) NULL,
	[Subtitle] NVARCHAR(255) NULL,
	[Moderated] bit NOT NULL,
	[Deleted] bit DEFAULT 0,
	CONSTRAINT [PK_tDiscussionArchive] PRIMARY KEY CLUSTERED 
	(
		[DiscussionArchiveID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
GO