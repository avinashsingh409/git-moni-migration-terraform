﻿CREATE PROCEDURE Discussions.spDeleteDiscussionEntry
	@SecurityUserID INT,
	@DiscussionEntryID UNIQUEIDENTIFIER,
	@Date DATETIMEOFFSET = NULL
AS
BEGIN
	
INSERT INTO Discussions.tDiscussionAttachmentArchive(	
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		[Deleted]
	)
	SELECT 
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		COALESCE(@Date, SYSDATETIMEOFFSET()),
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		1
	FROM Discussions.tDiscussionAttachment
	WHERE DiscussionEntryID = @DiscussionEntryID

INSERT INTO Discussions.tDiscussionEntryArchive(	
		[DiscussionEntryID],
		[DiscussionID],
		[OwnerUserID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[Title],
		[Subtitle],
		[Content],
		[Approved],
		[Deleted])
	SELECT 
		[DiscussionEntryID],
		[DiscussionID],
		[OwnerUserID],
		[CreateDateZ],
		COALESCE(@Date, SYSDATETIMEOFFSET()),
		[CreatedByUserID],
		@SecurityUserID,
		[AXExternalID],
		[Title],
		[Subtitle],
		[Content],
		[Approved],
		1
	FROM Discussions.tDiscussionEntry WHERE DiscussionEntryID = @DiscussionEntryID

DELETE FROM Discussions.tDiscussionAttachment
WHERE DiscussionEntryID = @DiscussionEntryID	  

DELETE FROM Discussions.tDiscussionEntryKeywordMap
WHERE DiscussionEntryID = @DiscussionEntryID

DELETE FROM Discussions.tDiscussionEntry 
WHERE DiscussionEntryID = @DiscussionEntryID

END
GO
GRANT EXECUTE
    ON OBJECT::[Discussions].[spDeleteDiscussionEntry] TO [TEUser]
    AS [dbo];