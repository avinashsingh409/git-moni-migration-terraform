﻿CREATE TABLE Discussions.tDiscussionAttachment(
	[DiscussionAttachmentID] [UNIQUEIDENTIFIER] NOT NULL DEFAULT NEWID(),
	[DiscussionID] UNIQUEIDENTIFIER NULL,
	[DiscussionEntryID] UNIQUEIDENTIFIER NULL,
	[CreateDateZ] datetimeoffset NOT NULL,
	[ChangeDateZ] datetimeoffset NOT NULL,
	[CreatedByUserID] int NOT NULL,
	[ChangedByUserID] int NOT NULL,
	[AXExternalID] NVARCHAR(MAX) NULL,
	[ContentID] UNIQUEIDENTIFIER NULL,
	[DisplayInstructions] NVARCHAR(MAX) NULL,
	[DiscussionAttachmentType] INT NOT NULL DEFAULT 1,
	[Title] NVARCHAR(255) NULL,
	DisplayOrder INT NOT NULL DEFAULT 1,
	CONSTRAINT [PK_tDiscussionAttachment] PRIMARY KEY CLUSTERED 
	(
		[DiscussionAttachmentID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
GO

ALTER TABLE [Discussions].[tDiscussionAttachment] ADD CONSTRAINT [FK_Discussions_DiscussionAttachment_DiscussionAttachmentType_Discussions_tDiscussionAttachmentType] FOREIGN KEY([DiscussionAttachmentType])
REFERENCES [Discussions].[tDiscussionAttachmentType] ([DiscussionAttachmentTypeID])
GO

ALTER TABLE [Discussions].[tDiscussionAttachment] ADD CONSTRAINT [FK_Discussions_DiscussionAttachment_DiscussionEntryID_Discussions_tDiscussion] FOREIGN KEY([DiscussionID])
REFERENCES [Discussions].[tDiscussion] ([DiscussionID])
GO

ALTER TABLE [Discussions].[tDiscussionAttachment] ADD CONSTRAINT [FK_Discussions_DiscussionAttachment_DiscussionEntryID_Discussions_tDiscussionEntry] FOREIGN KEY([DiscussionEntryID])
REFERENCES [Discussions].[tDiscussionEntry] ([DiscussionEntryID])
GO

CREATE INDEX [IDX_DiscussionEntryID] ON [Discussions].[tDiscussionAttachment] ([DiscussionEntryID])  WITH (FILLFACTOR=100);
GO