﻿CREATE PROCEDURE [Discussions].[spCanEditDiscussionEntry]
	@DiscussionEntryID UNIQUEIDENTIFIER,
	@SecurityUserID INT
AS
BEGIN
	DECLARE @result INT = 0;

	IF EXISTS (
		SELECT SecurityUserID FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID AND IsBVEmployee = 1
		UNION
		SELECT [OwnerUserID] FROM Discussions.tDiscussionEntry WHERE DiscussionEntryID = @DiscussionEntryID AND [OwnerUserID] = @SecurityUserID 
		)
	    BEGIN
		SET @result =  1;
	    END
    ELSE
	  BEGIN
	  set @result = AccessControl.ufnCanEditAll(@securityUserID)	  
	  END
	SELECT @result; 
END 
GO
GRANT EXECUTE
    ON OBJECT::[Discussions].[spCanEditDiscussionEntry] TO [TEUser]
    AS [dbo];