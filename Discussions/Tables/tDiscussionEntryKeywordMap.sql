﻿CREATE TABLE [Discussions].[tDiscussionEntryKeywordMap] (
    [DiscussionEntryKeywordMapID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [DiscussionEntryID]           UNIQUEIDENTIFIER NOT NULL,
    [KeywordID]                   BIGINT           NOT NULL,
    [CreatedBy]                   INT              NOT NULL,
    [TagDate]                     DATETIME         NOT NULL,
    [BackDate]                    DATE             NULL,
    CONSTRAINT [PK_tDiscussionEntryKeywordMap] PRIMARY KEY CLUSTERED ([DiscussionEntryKeywordMapID] ASC),
    CONSTRAINT [FK_tDiscussionEntryKeywordMap_tDiscussionEntry] FOREIGN KEY ([DiscussionEntryID]) REFERENCES [Discussions].[tDiscussionEntry] ([DiscussionEntryID]),
    CONSTRAINT [FK_tDiscussionEntryKeywordMap_tKeyword] FOREIGN KEY ([KeywordID]) REFERENCES [Asset].[tKeyword] ([KeywordID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tDiscussionEntryKeywordMap_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);








GO
CREATE NONCLUSTERED INDEX [IX_tDiscussionEntryKeywordMap_KeywordID]
    ON [Discussions].[tDiscussionEntryKeywordMap]([KeywordID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tDiscussionEntryKeywordMap_DiscussionEntryID]
    ON [Discussions].[tDiscussionEntryKeywordMap]([DiscussionEntryID] ASC);

