﻿CREATE TABLE Discussions.tDiscussionAttachmentType(
	DiscussionAttachmentTypeID INT NOT NULL,
	DiscussionAttachmentTypeAbbrev NVARCHAR(255) NOT NULL,
	DiscussionAttachmentTypeDesc NVARCHAR(255) NOT NULL,
	CONSTRAINT [PK_tDiscussionAttachmentType] PRIMARY KEY CLUSTERED 
	(
		DiscussionAttachmentTypeID ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
GO