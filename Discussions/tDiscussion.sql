﻿CREATE TABLE [Discussions].[tDiscussion](
	[DiscussionID] [UNIQUEIDENTIFIER] NOT NULL DEFAULT NEWID(),
	[AssetID] int NOT NULL,
	[OwnerUserID] int NOT NULL,
	[CreateDateZ] DateTimeOffset NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	[ChangeDateZ] DateTimeOffset NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	[CreatedByUserID] int NOT NULL,
	[ChangedByUserID] int NOT NULL,
	[AXExternalID] NVARCHAR(MAX) NULL,
	[Title] NVARCHAR(255) NULL,
	[Subtitle] NVARCHAR(255) NULL,
	[Moderated] bit NOT NULL DEFAULT 0,
	CONSTRAINT [PK_tDiscussion] PRIMARY KEY CLUSTERED 
	(
		[DiscussionID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
GO

ALTER TABLE [Discussions].[tDiscussion] ADD  CONSTRAINT [FK_Discussions_tDiscussion_CreatedByUserID_AccessControl_tUser] FOREIGN KEY([CreatedByUserID])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO

ALTER TABLE [Discussions].[tDiscussion] ADD  CONSTRAINT [FK_Discussions_tDiscussion_ChangedByUserID_AccessControl_tUser] FOREIGN KEY([ChangedByUserID])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO

ALTER TABLE [Discussions].[tDiscussion] ADD  CONSTRAINT [FK_Discussions_tDiscussion_OwnerUserID_AccessControl_tUser] FOREIGN KEY([OwnerUserID])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO