﻿CREATE PROCEDURE Discussions.spDeleteDiscussionAttachment
	@SecurityUserID INT,
	@DiscussionAttachmentID UNIQUEIDENTIFIER,
	@Date DATETIMEOFFSET = NULL
AS
BEGIN
	
INSERT INTO Discussions.tDiscussionAttachmentArchive(	
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		[ChangeDateZ],
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		[Deleted]
	)
	SELECT 
		[DiscussionAttachmentID],
		[DiscussionID],
		[DiscussionEntryID],
		[CreateDateZ],
		COALESCE(@Date, SYSDATETIMEOFFSET()),
		[CreatedByUserID],
		[ChangedByUserID],
		[AXExternalID],
		[ContentID],
		[DisplayInstructions],
		[DiscussionAttachmentType],
		[Title],
		1
	FROM Discussions.tDiscussionAttachment WHERE DiscussionAttachmentID = @DiscussionAttachmentID
	  
DELETE FROM Discussions.tDiscussionAttachment 
WHERE DiscussionAttachmentID = @DiscussionAttachmentID

END
GO
GRANT EXECUTE
    ON OBJECT::[Discussions].[spDeleteDiscussionAttachment] TO [TEUser]
    AS [dbo];