﻿
CREATE View Forecast.vForecastedPerformance AS

select b.ScenarioID,b.Year,b.Month,b.Day,b.Hour,b.UnitID,

-- Fuel
100 as Lignite_pct, 0 as PRB_pct, 0 as OtherFuel_pct,

-- Operations
case when isnull(netgeneration_mwh,0)=0 then 0 else 1 end as OperatingHours,
b.NetGeneration_mwh as Generation_mwh,
FuelBurnRate_mbtuhr as FuelBurn_mbtu,
FuelBurnRate_tonhr as FuelBurn_tons,
d.HHV_btulbm as HHV_btulbm, -- used for HHV_btu calc, HHV_btu = 
d.HHV_btulbm * b.FuelBurnRate_tonhr * 2000 AS HHV_btu,

-- Emissions
b.Particulate_ton as Particulate_tons,
b.SO2_ton as SO2Emissions_tons,
b.NOx_ton as NOxEmissions_tons,
b.Hg_lbm as HgEmissions_lbm,
b.CO2_ton/1000 as CO2Emissions_ktons,
case when d.HHV_btulbm is null then null 
else 
case when d.hhv_btulbm = 0 then 0 else (d.Sulfur_percent * 20000 / d.hhv_btulbm) * b.FuelBurn_mbtu/2000 end 
END AS SO2InletEmissions_tons,

-- Economics
c.GrossMargin as GrossMargin_dollars,
c.TotalRevenue as TotalRevenue_dollars,
c.Fuel as FuelCost_dollars,
c.TotalCosts as TotalVariableOandM_dollars,

-- RAM
0.0 as EAF_pct,
0.0 as ReliabilityIndex_pct,
0.0 as PlannedMaintenance_days,
0.0 as BalanceOfPlant_pct, 
0.0 as BoilerTubeFailureRate_pct,

-- MATS
0.0 AS MATS_Margin_pct,
0.0 AS MATS_SO2_HCl_Margin_pct,
0.0 AS MATS_Hg_Margin_pct,
0.0 AS MATS_PM_Margin_pct,
0.0 AS MATS_SO2_Margin_pct,
0.0 AS MATS_HCl_Margin_pct

 from 
 Projection.tScenarioUnitPerformance b 
JOIN Projection.tScenarioUnitCost c ON b.ScenarioID = c.ScenarioID
JOIN Fuel.tSolidFuelQuality d on b.fuelqualityid = d.fuelqualityid