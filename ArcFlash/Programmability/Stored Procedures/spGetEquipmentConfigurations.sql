﻿
CREATE PROCEDURE [ArcFlash].[spGetEquipmentConfigurations]
	@AssetID int = 0,
	@SecurityUserID int,
	@GetAllEquipmentConfigs bit,
	@ConfigID int = null
AS

BEGIN

DECLARE @AnscestorAssetIDs TABLE (AssetID int);
INSERT INTO @AnscestorAssetIDs SELECT AssetID FROM Asset.ufnAssetAncestors(@AssetID);

DECLARE @AttributeOptionTypeID int, @AttributeOptionTypeDesc nvarchar(255);

SELECT TOP 1 @AttributeOptionTypeDesc = AttributeOptionTypeDesc, @AttributeOptionTypeID = taot.AttributeOptionTypeID
FROM Asset.tAsset ta
inner join Asset.tAssetClassType tact		on ta.AssetClassTypeID = tact.AssetClassTypeID
inner join @AnscestorAssetIDs aaIDs			on ta.AssetID = aaIDs.AssetID
INNER JOIN asset.tAssetAttribute taa		on taa.AssetID = ta.AssetID
inner join asset.tAttributeOptionType taot	on taa.AttributeOptionTypeID = taot.AttributeOptionTypeID
where 
tact.AssetTypeID <=2 --station group or higher; drop this condition if we ever go to asset-specific formatting.
AND
taa.AttributeTypeID = 193 --ArcFlash label format attribute type
ORDER BY tact.AssetTypeID DESC --look to station group nodes before client nodes; deeper node format should take precedence.

DECLARE @tempAssets TABLE (
	AssetID int,
	ParentAssetID int,
	AssetAbbrev NVARCHAR(50),
	AssetDesc NVARCHAR(255),
	AssetTreeNodeChildConfigurationTypeID int NULL,
	DisplayOrder int,
	CreatedBy NVARCHAR(MAX),
	ChangedBy NVARCHAR(MAX),
	CreateDate datetime,
	ChangeDate datetime null,
	GlobalID uniqueidentifier,
	IsHidden bit,
	NumChildren int,
	AssetClassTypeID int, 
	AssetClassTypeAbbrev NVARCHAR(255),
	AssetClassTypeDesc NVARCHAR(MAX),
	AssetClassTypeKey NVARCHAR(255),
	AssetTypeID int, 
	AssetTypeDesc NVARCHAR(255), 
	AssetTypeAbbrev NVARCHAR(255),
	Voltage float
);

INSERT INTO @tempAssets
SELECT
	a.AssetID
	,a.ParentAssetID
	,a.AssetAbbrev
	,a.AssetDesc
	,a.AssetTreeNodeChildConfigurationTypeID
	,a.DisplayOrder
	,a.CreatedBy
	,a.ChangedBy
	,a.CreateDate
	,a.ChangeDate
	,a.GlobalID
	,a.IsHidden
	,(SELECT Count(kids.AssetID) FROM Asset.tAsset kids WHERE kids.ParentAssetID = a.AssetID ) as NumChildren
	, act.AssetClassTypeID, act.AssetClassTypeAbbrev ,act.AssetClassTypeDesc ,act.AssetClassTypeKey
	, at.AssetTypeID , at.AssetTypeDesc , at.AssetTypeAbbrev 
	, (SELECT TOP 1 Attribute_float FROM Asset.tAssetAttribute asatt WHERE a.AssetID = asatt.AssetID AND asatt.AttributeTypeID = 102) as Voltage
 FROM Asset.tAsset a
 INNER JOIN Asset.tAssetClassType act ON a.AssetClassTypeID = act.AssetClassTypeID
 INNER JOIN Asset.tAssetType at on act.AssetTypeID = at.AssetTypeID 
 INNER JOIN Asset.ufnGetAssetIdsForUser(@SecurityUserID) u ON a.AssetID = u.AssetID 
WHERE a.AssetID = @AssetID OR a.ParentAssetID = @AssetID;

SELECT * FROM @tempAssets;

SELECT 
	 c.[AssetModeledConfigurationTypeID]                                                          
     ,c.[AssetID] ownerAssetID                                                                   
     ,c.[DateAssigned]                                                                             
	 ,c.[DateCompiled] DateCompiled   
      ,c.[OwningAssetConfigurationID]                                                                      
      ,c.[ProtectiveDeviceAssetID]                                                                  
      ,c.[ProtectiveBoundary_ft]                                                                    
      ,c.[HazardBoundary_ft]                                                                        
      ,c.[HazardCategoryTypeID]                                                                     
      ,c.[IncidentEnergy_cal_cm2]                                                                   
      ,c.[WorkingDistance_in]                                                                       
      ,c.[ActiveConfiguration]                                                                      
	  ,a.AssetAbbrev                                                                                
	  ,a.AssetClassTypeID                                                                           
	  ,act.AssetTypeID                                                                              
	  ,act.AssetClassTypeKey                                                                        
	  ,act.AssetClassTypeAbbrev                                                                     
	  ,act.AssetClassTypeDesc                                                                       
	  ,act.DisplayOrder                                                                             
	  ,at.AssetTypeDesc                                                                             
	  ,at.AssetTypeAbbrev                                                                           
	  ,a.AssetDesc                                                                                  
	  ,a.AssetID                                                                                    
	  ,a.AssetTreeNodeChildConfigurationTypeID                                                      
	  ,a.ChangeDate                                                                                 
	  ,a.ChangedBy                                                                                  
	  ,a.CreateDate                                                                                 
	  ,a.CreatedBy                                                                                  
	  ,a.DisplayOrder                                                                               
	  ,a.GlobalID                                                                                   
	  ,a.IsHidden                                                                                   
	  ,a.ParentAssetID
	  , (SELECT Count(kids.AssetID) FROM Asset.tAsset kids WHERE kids.ParentAssetID = a.AssetID ) as NumChildren                                                         
	  ,ht.HazardCategoryName                                                                        
	  ,ht.HazardCategoryDesc
	  ,Label_DeviceID
	  ,Label_Voltage
	  ,Label_MaintSwitchStatus
	  ,Label_IncidentEnergy
	  ,Label_LAB
	  ,Label_PAB
	  ,Label_RAB
	  ,Label_GloveClass
	  ,Label_Exception
	  ,Label_MaintSwitchID
	  ,Label_FPB
	  ,Label_ProtDevice
	  ,Label_CatDesc
	  ,Label_PPEDescription
	  ,Label_BusID
	  ,Label_DeviceType
	  ,Label_DisclaimerText
	  ,Label_BVText
	  ,Label_AFStudyDate
	  ,BFCurrent       
	  ,ArcCurrent      
	  ,PDCurrent       
	  ,FCT             
	  ,BusType         
	  ,GroundingType   
	  ,CondGap         
	  ,Xfactor         
	  ,SCStudyCase     
	  ,ConfigCase      
	  ,ModelRevision   
	  ,ModelName       
	  ,ConfigAbbrev    
	  ,ConfigDesc      
	  ,PDType          
	  ,IDSCXRratio     
	  ,IDSCMF          
	  ,IDSCflag        
	  ,IDSymkA         
	  ,IDAdjSymkA      
	  ,IDAsymDeg       
	  ,IDcapSymkA      
	  ,IDcapAdjSymkA   
	  ,IDcapkV         
	  ,MDSCXRratio     
	  ,MDSCMF          
	  ,MDSCflag        
	  ,MDSymkA         
	  ,MDAsymkA        
	  ,MDAsymPeakkA    
	  ,MDcapSymkA      
	  ,MDcapAsymkA     
	  ,MDcapAsymPeakkA 
	  ,To_BusID        
	  ,Fm_BusID        
	  ,Ot_BusID   
	  ,ModeledBy
	  ,MaintenanceConfigurationID
	  ,tOwnerAsset.ParentAssetID ParentAssetID
	  ,tOwnerAsset.AssetAbbrev OwnerAssetAbbrev
	  ,tParentAsset.AssetAbbrev ParentAssetAbbrev
	  ,@AttributeOptionTypeID FormatTypeID
	  ,@AttributeOptionTypeDesc FormatTypeDesc
  FROM ArcFlash.tAssetModeledConfigurations c                                                        
  INNER JOIN ArcFlash.tHazardCategoryType ht			on c.HazardCategoryTypeID = ht.HazardCategoryTypeID    
  LEFT JOIN Asset.tAsset a								ON c.ProtectiveDeviceAssetID = a.AssetID                                 
  LEFT JOIN Asset.tAssetClassType act					ON a.AssetClassTypeID = act.AssetClassTypeID                  
  LEFT JOIN Asset.tAssetType at							ON act.AssetTypeID = at.AssetTypeID
  INNER JOIN @tempAssets ta								ON ta.AssetID = c.AssetID
  inner join Asset.tAsset tOwnerAsset					on c.AssetID = tOwnerAsset.AssetID
  left join asset.tAsset tParentAsset					on tOwnerAsset.ParentAssetID = tParentAsset.AssetID
  WHERE @GetAllEquipmentConfigs = 1 OR c.ActiveConfiguration = 1 AND (@ConfigID IS NULL OR c.AssetModeledConfigurationTypeID = @ConfigID); -- OR oac.ActiveMitigatedConfiguration = 1
  
END
;
GO

GRANT EXECUTE
    ON OBJECT::[ArcFlash].[spGetEquipmentConfigurations] TO [TEUser]
    AS [dbo];

