﻿CREATE TABLE [ArcFlash].[tHazardCategoryType] (
    [HazardCategoryTypeID] INT            NOT NULL,
    [HazardCategoryName]   NVARCHAR (255) NOT NULL,
    [HazardCategoryDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tHazardCategoryType] PRIMARY KEY CLUSTERED ([HazardCategoryTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ArcFlash', @level1type = N'TABLE', @level1name = N'tHazardCategoryType';

