﻿CREATE TABLE [ArcFlash].[tAssetModeledConfigurationsArchive] (
    [AssetModeledConfigurationArchiveID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetModeledConfigurationTypeID]    INT            NOT NULL,
    [AssetID]                            INT            NOT NULL,
    [DateCompiled]                       DATETIME       NOT NULL,
    [DateAssigned]                       DATETIME       NULL,
    [ChangeDate]                         DATETIME       NULL,
    [ProtectiveDeviceAssetID]            INT            NULL,
    [ProtectiveBoundary_ft]              FLOAT (53)     NULL,
    [HazardBoundary_ft]                  FLOAT (53)     NULL,
    [HazardCategoryTypeID]               INT            NOT NULL,
    [IncidentEnergy_cal_cm2]             FLOAT (53)     NULL,
    [WorkingDistance_in]                 FLOAT (53)     NULL,
    [ActiveConfiguration]                BIT            CONSTRAINT [DF_tAssetModeledConfigurationsArchive_ActiveConfiguration] DEFAULT ((0)) NULL,
    [CreateDate]                         DATETIME       CONSTRAINT [DF_tAssetModeledConfigurationsArchive_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreatedByUserID]                    INT            NULL,
    [ChangedByUserID]                    INT            NULL,
    [OwningAssetConfigurationID]         INT            NULL,
    [Label_DeviceID]                     NVARCHAR (MAX) NULL,
    [Label_Voltage]                      NVARCHAR (MAX) NULL,
    [Label_MaintSwitchStatus]            NVARCHAR (MAX) NULL,
    [Label_IncidentEnergy]               NVARCHAR (MAX) NULL,
    [Label_LAB]                          NVARCHAR (MAX) NULL,
    [Label_PAB]                          NVARCHAR (MAX) NULL,
    [Label_RAB]                          NVARCHAR (MAX) NULL,
    [Label_GloveClass]                   NVARCHAR (MAX) NULL,
    [Label_Exception]                    NVARCHAR (MAX) NULL,
    [Label_MaintSwitchID]                NVARCHAR (MAX) NULL,
    [Label_FPB]                          NVARCHAR (MAX) NULL,
    [Label_ProtDevice]                   NVARCHAR (MAX) NULL,
    [Label_CatDesc]                      NVARCHAR (MAX) NULL,
    [Label_PPEDescription]               NVARCHAR (MAX) NULL,
    [Label_BusID]                        NVARCHAR (MAX) NULL,
    [Label_DeviceType]                   NVARCHAR (MAX) NULL,
    [Label_DisclaimerText]               NVARCHAR (MAX) NULL,
    [Label_BVText]                       NVARCHAR (MAX) NULL,
    [BFCurrent]                          FLOAT (53)     NULL,
    [ArcCurrent]                         FLOAT (53)     NULL,
    [PDCurrent]                          FLOAT (53)     NULL,
    [FCT]                                FLOAT (53)     NULL,
    [BusType]                            NVARCHAR (MAX) NULL,
    [GroundingType]                      NVARCHAR (MAX) NULL,
    [CondGap]                            INT            NULL,
    [Xfactor]                            FLOAT (53)     NULL,
    [SCStudyCase]                        NVARCHAR (MAX) NULL,
    [ConfigCase]                         NVARCHAR (MAX) NULL,
    [ModelRevision]                      NVARCHAR (MAX) NULL,
    [ModelName]                          NVARCHAR (MAX) NULL,
    [ConfigAbbrev]                       NVARCHAR (MAX) NULL,
    [ConfigDesc]                         NVARCHAR (MAX) NULL,
    [PDType]                             NVARCHAR (MAX) NULL,
    [IDSCXRratio]                        FLOAT (53)     NULL,
    [IDSCMF]                             FLOAT (53)     NULL,
    [IDSCflag]                           BIT            NULL,
    [IDSymkA]                            FLOAT (53)     NULL,
    [IDAdjSymkA]                         FLOAT (53)     NULL,
    [IDAsymDeg]                          FLOAT (53)     NULL,
    [IDcapSymkA]                         FLOAT (53)     NULL,
    [IDcapAdjSymkA]                      FLOAT (53)     NULL,
    [IDcapkV]                            FLOAT (53)     NULL,
    [MDSCXRratio]                        FLOAT (53)     NULL,
    [MDSCMF]                             FLOAT (53)     NULL,
    [MDSCflag]                           BIT            NULL,
    [MDSymkA]                            FLOAT (53)     NULL,
    [MDAsymkA]                           FLOAT (53)     NULL,
    [MDAsymPeakkA]                       FLOAT (53)     NULL,
    [MDcapSymkA]                         FLOAT (53)     NULL,
    [MDcapAsymkA]                        FLOAT (53)     NULL,
    [MDcapAsymPeakkA]                    FLOAT (53)     NULL,
    [To_BusID]                           NVARCHAR (MAX) NULL,
    [Fm_BusID]                           NVARCHAR (MAX) NULL,
    [Ot_BusID]                           NVARCHAR (MAX) NULL,
    [ModeledBy]                          NVARCHAR (MAX) NULL,
    [Label_AFStudyDate]                  NVARCHAR (MAX) NULL,
	[MaintenanceConfigurationID]		 INT			NULL,
    CONSTRAINT [PK_tAssetModeledConfigurationsArchive] PRIMARY KEY CLUSTERED ([AssetModeledConfigurationArchiveID] ASC)
);















GO


GO
CREATE NONCLUSTERED INDEX [Idx_tAssetModeledConfigurationArchive_AssetID]
    ON [ArcFlash].[tAssetModeledConfigurationsArchive]([AssetID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ArcFlash', @level1type = N'TABLE', @level1name = N'tAssetModeledConfigurationsArchive';

