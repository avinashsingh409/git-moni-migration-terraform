﻿CREATE TABLE [ArcFlash].[tAssetModeledConfigurations] (
    [AssetModeledConfigurationTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]                         INT            NOT NULL,
    [DateCompiled]                    DATETIME       NOT NULL,
    [DateAssigned]                    DATETIME       NULL,
    [ProtectiveDeviceAssetID]         INT            NULL,
    [ProtectiveBoundary_ft]           FLOAT (53)     NULL,
    [HazardBoundary_ft]               FLOAT (53)     NULL,
    [HazardCategoryTypeID]            INT            NOT NULL,
    [IncidentEnergy_cal_cm2]          FLOAT (53)     NULL,
    [WorkingDistance_in]              FLOAT (53)     NULL,
    [ActiveConfiguration]             BIT            CONSTRAINT [DF_tAssetModeledConfigurations_ActiveConfiguration] DEFAULT ((0)) NULL,
    [Label_DeviceID]                  NVARCHAR (MAX) NULL,
    [Label_Voltage]                   NVARCHAR (MAX) NULL,
    [Label_MaintSwitchStatus]         NVARCHAR (MAX) NULL,
    [Label_IncidentEnergy]            NVARCHAR (MAX) NULL,
    [Label_LAB]                       NVARCHAR (MAX) NULL,
    [Label_PAB]                       NVARCHAR (MAX) NULL,
    [Label_RAB]                       NVARCHAR (MAX) NULL,
    [Label_GloveClass]                NVARCHAR (MAX) NULL,
    [Label_Exception]                 NVARCHAR (MAX) NULL,
    [Label_MaintSwitchID]             NVARCHAR (MAX) NULL,
    [Label_FPB]                       NVARCHAR (MAX) NULL,
    [Label_ProtDevice]                NVARCHAR (MAX) NULL,
    [Label_CatDesc]                   NVARCHAR (MAX) NULL,
    [Label_PPEDescription]            NVARCHAR (MAX) NULL,
    [Label_BusID]                     NVARCHAR (MAX) NULL,
    [Label_DeviceType]                NVARCHAR (MAX) NULL,
    [Label_DisclaimerText]            NVARCHAR (MAX) NULL,
    [Label_BVText]                    NVARCHAR (MAX) NULL,
    [CreateDate]                      DATETIME       CONSTRAINT [DF_tAssetModeledConfigurations_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                      DATETIME       CONSTRAINT [DF_tAssetModeledConfigurations_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CreatedByUserID]                 INT            NOT NULL,
    [ChangedByUserID]                 INT            NOT NULL,
    [OwningAssetConfigurationID]      INT            NULL,
    [BFCurrent]                       FLOAT (53)     NULL,
    [ArcCurrent]                      FLOAT (53)     NULL,
    [PDCurrent]                       FLOAT (53)     NULL,
    [FCT]                             FLOAT (53)     NULL,
    [BusType]                         NVARCHAR (MAX) NULL,
    [GroundingType]                   NVARCHAR (MAX) NULL,
    [CondGap]                         INT            NULL,
    [Xfactor]                         FLOAT (53)     NULL,
    [SCStudyCase]                     NVARCHAR (MAX) NULL,
    [ConfigCase]                      NVARCHAR (MAX) NULL,
    [ModelRevision]                   NVARCHAR (MAX) NULL,
    [ModelName]                       NVARCHAR (MAX) NULL,
    [ConfigAbbrev]                    NVARCHAR (MAX) NULL,
    [ConfigDesc]                      NVARCHAR (MAX) NULL,
    [PDType]                          NVARCHAR (MAX) NULL,
    [IDSCXRratio]                     FLOAT (53)     NULL,
    [IDSCMF]                          FLOAT (53)     NULL,
    [IDSCflag]                        BIT            NULL,
    [IDSymkA]                         FLOAT (53)     NULL,
    [IDAdjSymkA]                      FLOAT (53)     NULL,
    [IDAsymDeg]                       FLOAT (53)     NULL,
    [IDcapSymkA]                      FLOAT (53)     NULL,
    [IDcapAdjSymkA]                   FLOAT (53)     NULL,
    [IDcapkV]                         FLOAT (53)     NULL,
    [MDSCXRratio]                     FLOAT (53)     NULL,
    [MDSCMF]                          FLOAT (53)     NULL,
    [MDSCflag]                        BIT            NULL,
    [MDSymkA]                         FLOAT (53)     NULL,
    [MDAsymkA]                        FLOAT (53)     NULL,
    [MDAsymPeakkA]                    FLOAT (53)     NULL,
    [MDcapSymkA]                      FLOAT (53)     NULL,
    [MDcapAsymkA]                     FLOAT (53)     NULL,
    [MDcapAsymPeakkA]                 FLOAT (53)     NULL,
    [To_BusID]                        NVARCHAR (MAX) NULL,
    [Fm_BusID]                        NVARCHAR (MAX) NULL,
    [Ot_BusID]                        NVARCHAR (MAX) NULL,
    [ModeledBy]                       NVARCHAR (MAX) NULL,
    [Label_AFStudyDate]               NVARCHAR (MAX) NULL,
    [MaintenanceConfigurationID]      INT            NULL,
    CONSTRAINT [PK_tAssetModeledConfigurations] PRIMARY KEY CLUSTERED ([AssetModeledConfigurationTypeID] ASC),
    CONSTRAINT [FK_tAssetModeledConfigurations_ChangedByUserID_tUser_SecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tAssetModeledConfigurations_CreatedByUserID_tUser_SecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tAssetModeledConfigurations_tAsset] FOREIGN KEY ([ProtectiveDeviceAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetModeledConfigurations_tAssetModeledConfigurations] FOREIGN KEY ([MaintenanceConfigurationID]) REFERENCES [ArcFlash].[tAssetModeledConfigurations] ([AssetModeledConfigurationTypeID])
);





















GO


GO


GO


GO
CREATE NONCLUSTERED INDEX [UK_tAssetModeledConfigurations]
    ON [ArcFlash].[tAssetModeledConfigurations]([AssetID] ASC)
    INCLUDE([AssetModeledConfigurationTypeID], [DateCompiled], [DateAssigned], [OwningAssetConfigurationID], [ProtectiveDeviceAssetID], [ProtectiveBoundary_ft], [HazardBoundary_ft], [HazardCategoryTypeID], [IncidentEnergy_cal_cm2], [WorkingDistance_in], [ActiveConfiguration]);




GO
CREATE TRIGGER [ArcFlash].[tAssetModeledConfigurations_Update] 
   ON  [ArcFlash].[tAssetModeledConfigurations]
   FOR UPDATE, DELETE
AS 
BEGIN

 
   -- UPDATE Statement was executed
   INSERT INTO [ARCFlash].[tAssetModeledConfigurationsArchive](
                AssetModeledConfigurationTypeID,
                AssetID,
                DateCompiled,
                DateAssigned, 
                ChangeDate, 
                OwningAssetConfigurationID,
                ProtectiveDeviceAssetID, 
                ProtectiveBoundary_ft, 
                HazardBoundary_ft, 
                HazardCategoryTypeID, 
                IncidentEnergy_cal_cm2,
                WorkingDistance_in,
                ActiveConfiguration,
				MaintenanceConfigurationID,
	 Label_DeviceID        	    ,Label_Voltage          	,Label_MaintSwitchStatus	,Label_IncidentEnergy   	,Label_LAB              
	,Label_PAB              	,Label_RAB              	,Label_GloveClass       	,Label_Exception        	,Label_MaintSwitchID    
	,Label_FPB              	,Label_ProtDevice       	,Label_CatDesc          	,Label_PPEDescription   	,Label_BusID            
	,Label_DeviceType       	,Label_DisclaimerText   	,Label_BVText				,Label_AFStudyDate
	,BFCurrent					,ArcCurrent					,PDCurrent					,FCT						,BusType 
	,GroundingType				,CondGap					,Xfactor					,SCStudyCase				,ConfigCase 
	,ModelRevision				,ModelName					,ConfigAbbrev				,ConfigDesc					,PDType 
	,IDSCXRratio				,IDSCMF						,IDSCflag					,IDSymkA					,IDAdjSymkA 
	,IDAsymDeg					,IDcapSymkA					,IDcapAdjSymkA				,IDcapkV					,MDSCXRratio 
	,MDSCMF						,MDSCflag					,MDSymkA					,MDAsymkA					,MDAsymPeakkA 
	,MDcapSymkA					,MDcapAsymkA				,MDcapAsymPeakkA			,To_BusID					,Fm_BusID 
	,Ot_BusID 					,ModeledBy
   )
   SELECT
        i.AssetModeledConfigurationTypeID, 
        i.AssetID, 
        i.DateCompiled, 
        i.DateAssigned, 
        GETDATE(), 
        i.OwningAssetConfigurationID,
        i.ProtectiveDeviceAssetID, 
        i.ProtectiveBoundary_ft, 
        i.HazardBoundary_ft, 
        i.HazardCategoryTypeID, 
        i.IncidentEnergy_cal_cm2, 
        i.WorkingDistance_in, 
        i.ActiveConfiguration,
		i.MaintenanceConfigurationID,
	 i.Label_DeviceID         	,i.Label_Voltage          	,i.Label_MaintSwitchStatus	,i.Label_IncidentEnergy   	,i.Label_LAB              
	,i.Label_PAB              	,i.Label_RAB              	,i.Label_GloveClass       	,i.Label_Exception        	,i.Label_MaintSwitchID    
	,i.Label_FPB              	,i.Label_ProtDevice       	,i.Label_CatDesc          	,i.Label_PPEDescription   	,i.Label_BusID            
	,i.Label_DeviceType       	,i.Label_DisclaimerText   	,i.Label_BVText				,i.Label_AFStudyDate
	,i.BFCurrent				,i.ArcCurrent				,i.PDCurrent				,i.FCT						,i.BusType 
	,i.GroundingType			,i.CondGap					,i.Xfactor					,i.SCStudyCase				,i.ConfigCase 
	,i.ModelRevision			,i.ModelName				,i.ConfigAbbrev				,i.ConfigDesc				,i.PDType 
	,i.IDSCXRratio				,i.IDSCMF					,i.IDSCflag					,i.IDSymkA					,i.IDAdjSymkA 
	,i.IDAsymDeg				,i.IDcapSymkA				,i.IDcapAdjSymkA			,i.IDcapkV					,i.MDSCXRratio 
	,i.MDSCMF					,i.MDSCflag					,i.MDSymkA					,i.MDAsymkA					,i.MDAsymPeakkA 
	,i.MDcapSymkA				,i.MDcapAsymkA				,i.MDcapAsymPeakkA			,i.To_BusID					,i.Fm_BusID 
	,i.Ot_BusID					,i.ModeledBy

   FROM Deleted i
   ;


   if (select count(*) from deleted) > 0
   BEGIN
	  DECLARE @freshlyInvalidConfigIDs TABLE (ID int not null);
	  Insert into @freshlyInvalidConfigIDs 
	  select AssetModeledConfigurationTypeID
	  from deleted
	  where AssetModeledConfigurationTypeID Not in (select AssetModeledConfigurationTypeID from inserted);

      UPDATE [ARCFlash].[tAssetModeledConfigurations]
	  SET MaintenanceConfigurationID = null
	  where MaintenanceConfigurationID in (select ID from @freshlyInvalidConfigIDs);
   END
   
END
GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ArcFlash', @level1type = N'TABLE', @level1name = N'tAssetModeledConfigurations';

