﻿
----------------------------------------------------------

CREATE PROCEDURE [ArcFlash].[spGetLabelFormat]
	-- Add the parameters for the stored procedure here
	@AssetID int,
	@SecurityUserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @assetAndAnscestors as table (AssetID int);
	Insert into @assetAndAnscestors 
	select assetID from [Asset].[ufnAssetAncestors](@AssetID);
	insert into @assetAndAnscestors 
	values (@AssetID); 

    -- Insert statements for procedure here
	SELECT top 1 aa.*, at.AttributeTypeDesc, aot.AttributeOptionTypeDesc LabelFormat
	from Asset.tAssetAttribute aa
	inner join asset.tAttributeType at on aa.AttributeTypeID = at.AttributeTypeID
	inner join asset.tAttributeOptionType aot on aa.AttributeOptionTypeID = aot.AttributeOptionTypeID
	INNER JOIN Asset.ufnGetAssetIdsForUser(@SecurityUserID) u ON aa.AssetID = u.AssetID 
	inner join @assetAndAnscestors a on aa.AssetID = a.AssetID
	WHERE at.AttributeTypeKey = 'ArcFlashLabelFormat'
	order by a.AssetID desc --puts the deepest asset at the top, which will give us the most-specific label format 
							--(if we ever allow sub-assets to have different formats than their parents).
END
GO
GRANT EXECUTE
    ON OBJECT::[ArcFlash].[spGetLabelFormat] TO [TEUser]
    AS [dbo];

