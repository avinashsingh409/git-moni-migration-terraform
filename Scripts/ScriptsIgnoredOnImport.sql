﻿
/****** Object:  StoredProcedure [ProcessData].[spTrend]    Script Date: 12/14/2015 2:18:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('ProcessData.tPin', 'U') IS NOT NULL
  DROP TABLE ProcessData.tPin;
GO

IF OBJECT_ID('ProcessData.tPinType', 'U') IS NOT NULL
  DROP TABLE ProcessData.tPinType;
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(1, 'Unmodified',1)
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(2, 'No Gaps',2)
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(3, 'Overlay',3)
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE name=N'PinType' AND Object_ID = object_id(N'ProcessData.tTrend'))
BEGIN 
	ALTER TABLE ProcessData.tTrend
	ADD PinTypeID int NOT NULL DEFAULT 1
	CONSTRAINT [FK_tTrend_PinType_tPinType] FOREIGN KEY([PinTypeID])
	REFERENCES [ProcessData].[tPinType] ([PinTypeID])
END
GO

DROP PROCEDURE [ProcessData].[spTrend]
GO

/****** Object:  ForeignKey [FK_tProcess_tProcessServer]    Script Date: 03/03/2015 10:23:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Status].[FK_tProcess_tProcessServer]') AND parent_object_id = OBJECT_ID(N'[Status].[tProcessLog]'))
ALTER TABLE [Status].[tProcessLog] DROP CONSTRAINT [FK_tProcess_tProcessServer]
GO

/****** Object:  ForeignKey [FK_tProcess_tProcessStatus]    Script Date: 03/03/2015 10:23:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Status].[FK_tProcess_tProcessStatusType]') AND parent_object_id = OBJECT_ID(N'[Status].[tProcessLog]'))
ALTER TABLE [Status].[tProcessLog] DROP CONSTRAINT [FK_tProcess_tProcessStatusType]
GO

/****** Object:  ForeignKey [FK_tProcess_tProcessStatus]    Script Date: 03/03/2015 10:23:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Status].[FK_tProcess_tProcessType]') AND parent_object_id = OBJECT_ID(N'[Status].[tProcessLog]'))
ALTER TABLE [Status].[tProcessLog] DROP CONSTRAINT [FK_tProcess_tProcessType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Status].[spInsertUpdateProcessLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Status].[spInsertUpdateProcessLog]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Status].[tProcessLog]') AND type in (N'U'))
DROP TABLE [Status].[tProcessLog]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Status].[tProcessServer]') AND type in (N'U'))
DROP TABLE [Status].[tProcessServer]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Status].[tProcessType]') AND type in (N'U'))
DROP TABLE [Status].[tProcessType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Status].[tProcessStatusType]') AND type in (N'U'))
DROP TABLE [Status].[tProcessStatusType]
GO

/****** Object:  Table [Status].[tProcessStatus]    Script Date: 03/03/2015 10:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Status].[tProcessServer]    Script Date: 03/03/2015 10:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Status].[tProcessServer]    Script Date: 03/03/2015 10:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Status].[tProcess]    Script Date: 03/03/2015 10:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Status].[spInsertUpdateProcessLog]    Script Date: 03/03/2015 10:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GRANT EXECUTE ON [Status].[spInsertUpdateProcessLog] TO TEUser;
GO

DELETE FROM [Status].tProcessStatusType
GO

INSERT INTO [Status].tProcessStatusType (ProcessStatusTypeID, ProcessStatusAbbrev, ProcessStatusDesc)
values 
(1, 'START', 'Process Started'),
(2, 'FAILED', 'Process Failed - See Log for Errors'),
(3, 'PARTIAL', 'Process Partially Succeeded - See Log For Errors'),
(4, 'SUCCESS', 'Process Succeeded')
GO

DELETE FROM [Status].tProcessServer
GO

INSERT INTO [Status].tProcessServer (ProcessServerID,
ProcessServerAbbrev, ProcessServerDesc, EndPoint)
values 
(1, 'UNDEFINED', 'Undefined Server', 'Undefined EndPoint'),
(2, 'Luminant_DEV', 'Luminant Development', 'http://luminant.powerplantmd.com/luminant/WebServices_SII_Luminant/'),
(3, 'Luminant_TRAIN', 'Luminant Training','http://50.17.201.46/luminant/webservices_training/'),
(4, 'Luminant_PROD', 'Luminant Production', 'http://luminant.powerplantmd.com/luminant/webservices_test/')
GO

INSERT INTO [Status].tProcessType (ProcessTypeID,
ProcessTypeAbbrev, ProcessTypeDesc)
values 
(1, 'AC', 'Actuals Collector'),
(2, 'DCW', 'Data Cache Worker')
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName)
VALUES ('5.3.0.0-02','Adding new Status schema and related tables & procedure for process tracking (for actuals collector and data cache worker, currently)',
		'FromVersion_5-3-0-0_Script02_StatusSchemaSupportForProcessTracking.sql')
GO

/****** Object:  StoredProcedure [ProcessData].[spTrend]    Script Date: 12/14/2015 2:18:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('ProcessData.tPin', 'U') IS NOT NULL
  DROP TABLE ProcessData.tPin;
GO

IF OBJECT_ID('ProcessData.tPinType', 'U') IS NOT NULL
  DROP TABLE ProcessData.tPinType;
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(1, 'Unmodified',1)
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(2, 'No Gaps',2)
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(3, 'Overlay',3)
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE name=N'PinType' AND Object_ID = object_id(N'ProcessData.tTrend'))
BEGIN 
	ALTER TABLE ProcessData.tTrend
	ADD PinTypeID int NOT NULL DEFAULT 1
	CONSTRAINT [FK_tTrend_PinType_tPinType] FOREIGN KEY([PinTypeID])
	REFERENCES [ProcessData].[tPinType] ([PinTypeID])
END
GO

DROP PROCEDURE [ProcessData].[spTrend]
GO

/****** Object:  StoredProcedure [ProcessData].[spTrend]    Script Date: 12/14/2015 2:18:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('ProcessData.tPin', 'U') IS NOT NULL
  DROP TABLE ProcessData.tPin;
GO

IF OBJECT_ID('ProcessData.tPinType', 'U') IS NOT NULL
  DROP TABLE ProcessData.tPinType;
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(1, 'Unmodified',1)
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(2, 'No Gaps',2)
GO

INSERT INTO ProcessData.tPinType (PinTypeID, PinTypeDesc, DisplayOrder) VALUES
(3, 'Overlay',3)
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE name=N'PinType' AND Object_ID = object_id(N'ProcessData.tTrend'))
BEGIN 
	ALTER TABLE ProcessData.tTrend
	ADD PinTypeID int NOT NULL DEFAULT 1
	CONSTRAINT [FK_tTrend_PinType_tPinType] FOREIGN KEY([PinTypeID])
	REFERENCES [ProcessData].[tPinType] ([PinTypeID])
END
GO

DROP PROCEDURE [ProcessData].[spTrend]
GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spTrend] TO [TEUser]
    AS [dbo];
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('5.3.0.0-01','Add pinning to PDTrends',
'FromVersion_5-3-0-0_Script01_Pinning.sql')
GO

BEGIN TRANSACTION
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_NULLS ON
GO

SET ANSI_WARNINGS ON
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

BEGIN TRANSACTION
GO

COMMIT
GO

INSERT INTO [Risk].[tRiskScoreCalcType]
([RiskScoreCalcTypeID], [Description])
VALUES (1, 'Linear'), (2, 'Exponential'), (3, 'Logarithmic');
GO

INSERT INTO [Risk].[tRiskAcceptabilityType]
([RiskAcceptabilityTypeID], [Value], [Description])
VALUES (1, 1, 'Risk is Acceptable'), (2, 2, 'Risk is NOT Acceptable'), (3, 3, 'More Data Needed');
GO

INSERT INTO [Risk].[tRiskConfidenceType]
([RiskConfidenceTypeID], [Value], [Description])
VALUES (1, 1, 'Poor'), (2, 2, 'Medium'), (3, 3, 'Good');
GO

INSERT INTO [Risk].[tRiskNatureType]
([RiskNatureTypeID], [PrimaryNature], [SubNature])
VALUES (1, 'Physical', 'Functional Failure')
      ,(2, 'Physical', 'Incidental Damage')
      ,(3, 'Physical', 'Malicious Damage')
      ,(4, 'Physical', 'Terrorist Action')
      ,(5, 'Operational', 'Control')
      ,(6, 'Operational', 'Human Performance')
      ,(7, 'Operational', 'Other Activities')
      ,(8, 'Natural', 'Storm, Flood')
      ,(9, 'Natural', 'Other')
      ,(10, 'Third Party', NULL)
      ,(11, 'Stakeholder', NULL);
GO

INSERT INTO [Risk].[tRiskStageType]
([RiskStageTypeID], [Description])
VALUES (1, 'Investigation')
      ,(2, 'Analysis')
      ,(3, 'Evaluation')
      ,(4, 'Mitigation Development')
      ,(5, 'Mitigation Approval')
      ,(6, 'Mitigation Implementation')
      ,(7, 'Monitoring')
      ,(8, 'Closed');
GO

INSERT INTO [Risk].[tRiskDetailType]
([RiskDetailTypeID], [Description])
VALUES (1, 'Likelihood'), (2, 'Consequence');
GO

INSERT INTO [Risk].[tRiskDetailScoreType]
([RiskDetailScoreTypeID], [Value], [Description])
VALUES (1, 1, 'Low')
      ,(2, 2, 'Medium-Low')
      ,(3, 3, 'Medium')
      ,(4, 4, 'Medium-High')      
      ,(5, 5, 'High');
GO

INSERT INTO [Risk].[tRiskDetailCategory]
([RiskDetailCategoryID], [RiskDetailTypeID], [RiskDetailScoreTypeID], [Category], [Definition])
VALUES (1, 1, 1, NULL, 'Several events must occur concurrently or in a series to trigger failure.  Most, if not all of the events are very unlikely; potential failure is non-credible. Possibility of Failure >30 yrs')
      ,(2, 1, 2, NULL, 'The possibility cannot be ruled out, but there is no compelling evidence to suggest it has occurred or that a condition or flaw exists that could lead to its development. Likely (>50%) to occur in the next 20 years')
      ,(3, 1, 3, NULL, 'The fundamental condition or defect is known to exist, indirect evidence suggests it is plausible, and key evidence is weighted more heavily toward unlikely than likely Likely (>50%) to occur in the next 15 years')
      ,(4, 1, 4, NULL, 'The fundamental condition or defect is known to exist, indirect evidence suggests it is plausible, and key evidence is weighted more heavily toward likely than unlikely. Likely (>50%) to occur in the next 10 years')
      ,(5, 1, 5, NULL, 'There is direct evidence or substantial indirect evidence to suggest it has initiated and/or is likely to occur. Likely (>50%) to occur in the next 5 years')
      ,(6,  2, 1, 'Financial Impact', 'Less than $10,000')
      ,(7,  2, 2, 'Financial Impact', '$10,001 to $100,000')
      ,(8,  2, 3, 'Financial Impact', '$100,001 to $1,000,000')
      ,(9,  2, 4, 'Financial Impact', '$1 million to $10 million')
      ,(10, 2, 5, 'Financial Impact', 'Over $10 million')
      ,(11, 2, 1, 'Health & Safety Impact', 'None')
      ,(12, 2, 2, 'Health & Safety Impact', 'Minor reportable injury')
      ,(13, 2, 3, 'Health & Safety Impact', 'Major injury or temporary disability')
      ,(14, 2, 4, 'Health & Safety Impact', 'Permanent disability')
      ,(15, 2, 5, 'Health & Safety Impact', 'One or more fatalities')
      ,(16, 2, 1, 'Customer Impact', 'Under 5,000 customers')
      ,(17, 2, 2, 'Customer Impact', '5,001 to 10,000 customers')
      ,(18, 2, 3, 'Customer Impact', '10,001 to 15,000 customers and/or limited number of critical industrial and commercial customers.')
      ,(19, 2, 4, 'Customer Impact', '15,001 to 20,000 customers and/or limited number of critical customers.')
      ,(20, 2, 5, 'Customer Impact', 'Over 20,001 customers and/or significant number of critical customers.')
      ,(21, 2, 1, 'Regulatory & Environmental', 'No violation or impact.')
      ,(22, 2, 2, 'Regulatory & Environmental', 'Violates N-2 and/ or has a minor impact on the environment.')
      ,(23, 2, 3, 'Regulatory & Environmental', 'Violates N-1 and/or has localized effect on environment with recovery of damage within one year.')
      ,(24, 2, 4, 'Regulatory & Environmental', 'Violates N-1-1 and/or has major effect on the environment.')
      ,(25, 2, 5, 'Regulatory & Environmental', 'Requires emergency operations for adequacy and/or has permanent damage to the environment.')
      ,(26, 2, 1, 'Reputation', 'Complaints to Customer Service.')
      ,(27, 2, 2, 'Reputation', 'Some negative local media coverage. Issue raised with public officials.')
      ,(28, 2, 3, 'Reputation', 'Negative media coverage at state level.')
      ,(29, 2, 4, 'Reputation', 'Sustained negative media coverage. Issues raised by State Government.')
      ,(30, 2, 5, 'Reputation', 'Negative media coverage at national level. Public inquiry.')
GO

IF OBJECT_ID('Risk.spGetRiskStageType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRiskStageType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskStageType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRiskConfidenceType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRiskConfidenceType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskConfidenceType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRiskAcceptabilityType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRiskAcceptabilityType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskAcceptabilityType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRiskNatureType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRiskNatureType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskNatureType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetDefaultRiskNatureType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetDefaultRiskNatureType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetDefaultRiskNatureType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRiskDetailScoreType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRiskDetailScoreType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskDetailScoreType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetDefaultRiskDetailScoreType', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetDefaultRiskDetailScoreType]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetDefaultRiskDetailScoreType] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRiskDetailCategory', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRiskDetailCategory]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskDetailCategory] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetDefaultRiskDetailCategory', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetDefaultRiskDetailCategory]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetDefaultRiskDetailCategory] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRiskDetailByRisk', 'P') IS NOT NULL
	DROP PROC [Risk].[spGetRiskDetailByRisk]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRiskDetailByRisk] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spSaveRiskDetail', 'P') IS NOT NULL
	DROP PROC [Risk].[spSaveRiskDetail]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRiskDetail] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spDeleteRiskDetail', 'P') IS NOT NULL
	DROP PROC [Risk].[spDeleteRiskDetail]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spDeleteRiskDetail] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spGetRisk', 'P') IS NOT NULL
    DROP PROC [Risk].[spGetRisk]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spGetRisk] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spSaveRisk', 'P') IS NOT NULL
	DROP PROC [Risk].[spSaveRisk]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRisk] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spDeleteRisk', 'P') IS NOT NULL
	DROP PROC [Risk].[spDeleteRisk]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spDeleteRisk] TO [TEUser]
    AS [dbo];
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('5.4.0.0-03','Create Risk Schema',
'FromVersion_5-4-0-0_Script03_Risk_Schema.sql');
GO

BEGIN TRANSACTION
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_NULLS ON
GO

SET ANSI_WARNINGS ON
GO

COMMIT
GO

IF OBJECT_ID('Risk.spSaveRiskDetail', 'P') IS NOT NULL
	DROP PROC [Risk].[spSaveRiskDetail]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRiskDetail] TO [TEUser]
    AS [dbo];
GO

IF OBJECT_ID('Risk.spSaveRisk', 'P') IS NOT NULL
	DROP PROC [Risk].[spSaveRisk]
GO

GRANT EXECUTE
    ON OBJECT::[Risk].[spSaveRisk] TO [TEUser]
    AS [dbo];
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('5.4.0.0-04','Update Risk Schema',
'FromVersion_5-4-0-0_Script04_Update_Risk_Schema.sql');
GO

DECLARE @TranName VARCHAR(40);
SELECT @TranName = 'Create_Alerts_Tables';
BEGIN TRANSACTION @TranName;
BEGIN TRY
		CREATE TABLE [Monitoring].[tNDWatchLimitMeetsTemporalType] (
		[WatchLimitMeetsTemporalTypeID] INTEGER  NOT NULL  
		, [WatchLimitMeetsTemporalTypeAbbrev] nvarchar(50)  NOT NULL  
		, [WatchLimitMeetsTemporalTypeDesc] nvarchar(255)  NOT NULL  
		)
		
		ALTER TABLE [Monitoring].[tNDWatchLimitMeetsTemporalType] ADD CONSTRAINT [tNDWatchLimitMeetsTemporalType_PK] PRIMARY KEY CLUSTERED (
		[WatchLimitMeetsTemporalTypeID]
		)
		
		EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDWatchLimitMeetsTemporalType'

		CREATE TABLE [Monitoring].[tNDWatchCriteriaType] (
		[WatchCriteriaTypeID] INTEGER  NOT NULL  
		, [WatchCriteriaTypeAbbrev] nvarchar(50)  NOT NULL  
		, [WatchCriteriaTypeDesc] nvarchar(255)  NOT NULL  
		)
		
		ALTER TABLE [Monitoring].[tNDWatchCriteriaType] ADD CONSTRAINT [tNDWatchCriteriaType_PK] PRIMARY KEY CLUSTERED (
		[WatchCriteriaTypeID]
		)

		EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDWatchCriteriaType'
		
		CREATE TABLE [Monitoring].[tNDModelActionItemPriorityType] (
		[ModelActionItemPriorityTypeID] INTEGER  NOT NULL  
		, [ModelActionItemPriorityValue] INTEGER  NOT NULL  
		, [ModelActionItemPriorityAbbrev] nvarchar(50)  NOT NULL  
		, [ModelActionItemPriorityDesc] nvarchar(255)  NOT NULL  
		)
		
		ALTER TABLE [Monitoring].[tNDModelActionItemPriorityType] ADD CONSTRAINT [tNDModelActionItemPriorityType_PK] PRIMARY KEY CLUSTERED (
		[ModelActionItemPriorityTypeID]
		)

		EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDModelActionItemPriorityType'

		CREATE TABLE [Monitoring].[tNDModelActionItem] (
		[ModelID] INTEGER  NOT NULL  
		, [ModelActionItemPriorityTypeID] INTEGER  NULL  
		, [IsDiagnose] BIT  NULL  
		, [IsWatch] BIT  NULL  
		, [IsModelMaintenance] BIT  NULL  
		, [NoteText] nvarchar(255)  NULL  
		, [CreatedByUserID] INTEGER  NOT NULL  
		, [CreateDate] DATETIME  NOT NULL  
		, [ChangedByUserID] INTEGER  NOT NULL  
		, [ChangeDate] DATETIME  NOT NULL  
		, [WatchDuration] real  NULL  
		, [WatchDurationTemporalTypeID] INTEGER  NULL  
		, [WatchCriteriaTypeID] INTEGER  NULL  
		, [WatchLimit] real  NULL  
		, [WatchLimitMeetsTemporalTypeID] INTEGER  NULL
		, [Favorite] BIT NOT NULL DEFAULT ((0))
		)
		
		ALTER TABLE [Monitoring].[tNDModelActionItem] ADD CONSTRAINT [tNDModelActionItem_PK] PRIMARY KEY CLUSTERED (
		[ModelID]
		)
		
		CREATE TABLE [Monitoring].[tNDModelActionItemArchive] (
		[ModelActionItemArchiveID] INTEGER NOT NULL IDENTITY(1,1)
		, [ModelID] INTEGER NOT NULL 
		, [ModelActionItemPriorityTypeID] INTEGER NULL 
		, [IsDiagnose] BIT NULL 
		, [IsWatch] BIT NULL 
		, [IsModelMaintenance] BIT NULL 
		, [NoteText] nvarchar(255) NULL 
		, [CreatedByUserID] INTEGER NOT NULL 
		, [CreateDate] DATETIME NOT NULL 
		, [ChangedByUserID] INTEGER NOT NULL 
		, [ChangeDate] DATETIME NOT NULL 
		, [WatchDuration] real NULL 
		, [WatchDurationTemporalTypeID] INTEGER NULL 
		, [WatchCriteriaTypeID] INTEGER NULL 
		, [WatchLimit] real NULL 
		, [WatchLimitMeetsTemporalTypeID] INTEGER NULL
		, [Favorite] BIT NOT NULL DEFAULT ((0))
		)
		
		ALTER TABLE [Monitoring].[tNDModelActionItemArchive] ADD CONSTRAINT [tNDModelActionItemArchive_PK] PRIMARY KEY CLUSTERED (
		[ModelActionItemArchiveID]
		)

		CREATE TABLE [Monitoring].[tNDModel_AlertScreening] (
		[ModelID] INTEGER  NOT NULL  
		, [IgnoreStatus] BIT  NULL  
		, [IgnoreDurationHours] INTEGER  NULL  
		, [TagGroup] INTEGER  NULL  
		)
		ALTER TABLE [Monitoring].[tNDModel_AlertScreening] ADD CONSTRAINT [tNDModel_AlertScreening_PK] PRIMARY KEY CLUSTERED (
		[ModelID]
		)
		CREATE TABLE [Monitoring].[tNDModel_AlertScreening_FromAsset360] (
		[ModelID] INTEGER  NOT NULL  
		, [Unit] nvarchar(50)  NOT NULL  
		, [HasOpenIssue] BIT  NOT NULL  
		)
		ALTER TABLE [Monitoring].[tNDModel_AlertScreening_FromAsset360] ADD CONSTRAINT [tNDModel_AlertScreening_FromAsset360_PK] PRIMARY KEY CLUSTERED (
		[ModelID]
		)
		
		CREATE TABLE [Monitoring].[tNDModel_AlertScreening_FromND] (
		[ModelID] INTEGER  NOT NULL  
		, [NDAlertStatus] BIT  NOT NULL  
		, [PercentOOB] real  NOT NULL  
		, [Actual] real  NOT NULL  
		, [LowerLimit] real  NOT NULL  
		, [Expected] real  NOT NULL  
		, [UpperLimit] real  NOT NULL  
		)
		ALTER TABLE [Monitoring].[tNDModel_AlertScreening_FromND] ADD CONSTRAINT [tNDModel_AlertScreening_FromND_PK] PRIMARY KEY CLUSTERED (
		[ModelID]
		)
		
		CREATE TABLE [Monitoring].[tNDAlertPriorityType] (
		[AlertPriorityTypeID] INTEGER  NOT NULL  
		, [AlertPriorityTypeAbbrev] nvarchar(50)  NOT NULL  
		, [AlertPriorityTypeDesc] nvarchar(255)  NOT NULL  
		, [AlertPriorityLevel] nvarchar(50)  NOT NULL  
		)
		ALTER TABLE [Monitoring].[tNDAlertPriorityType] ADD CONSTRAINT [tNDAlertPriorityType_PK] PRIMARY KEY CLUSTERED (
		[AlertPriorityTypeID]
		)

		EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Monitoring', @level1type=N'TABLE',@level1name=N'tNDAlertPriorityType'
		
		CREATE TABLE [Monitoring].[tNDAlertPriority] (
		[ModelID] INT  NOT NULL  
		, [AlertPriorityTypeID] INTEGER  NOT NULL  
		, [AlertPriorityAbbrev] nvarchar(50)  NOT NULL  
		, [AlertPriorityDesc] nvarchar(255)  NOT NULL  
		)
		ALTER TABLE [Monitoring].[tNDAlertPriority] ADD CONSTRAINT [tNDAlertPriority_PK] PRIMARY KEY CLUSTERED (
		[ModelID]
		)
		
		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tAssetIssue_tNDModelIssueMap_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tNDModelIssueMap]'))
		ALTER TABLE [Monitoring].[tNDModelIssueMap] DROP CONSTRAINT [tAssetIssue_tNDModelIssueMap_FK1]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModel_tNDModelIssueMap_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tNDModelIssueMap]'))
		ALTER TABLE [Monitoring].[tNDModelIssueMap] DROP CONSTRAINT [tNDModel_tNDModelIssueMap_FK1]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModelIssueMap]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tNDModelIssueMap]
		
		CREATE TABLE [Monitoring].[tNDModelIssueMap] (
		[ModelIssueMapID] INTEGER  NOT NULL  
		, [ModelID] INTEGER  NOT NULL  
		, [AssetIssueID] INTEGER  NOT NULL  
		)
		ALTER TABLE [Monitoring].[tNDModelIssueMap] ADD CONSTRAINT [tNDModelIssueMap_PK] PRIMARY KEY CLUSTERED (
		[ModelIssueMapID]
		)
		
		
		ALTER TABLE [Monitoring].[tNDModelActionItem] WITH CHECK ADD CONSTRAINT [tNDModelActionItemPriorityType_tNDModelActionItem_FK1] FOREIGN KEY (
		[ModelActionItemPriorityTypeID]
		)
		REFERENCES [Monitoring].[tNDModelActionItemPriorityType] (
		[ModelActionItemPriorityTypeID]
		)
		ALTER TABLE [Monitoring].[tNDModelActionItem] WITH CHECK ADD CONSTRAINT [tNDWatchLimitMeetsTemporalType_tNDModelActionItem_FK1] FOREIGN KEY (
		[WatchLimitMeetsTemporalTypeID]
		)
		REFERENCES [Monitoring].[tNDWatchLimitMeetsTemporalType] (
		[WatchLimitMeetsTemporalTypeID]
		)
		ALTER TABLE [Monitoring].[tNDModelActionItem] WITH CHECK ADD CONSTRAINT [tNDWatchCriteriaType_tNDModelActionItem_FK1] FOREIGN KEY (
		[WatchCriteriaTypeID]
		)
		REFERENCES [Monitoring].[tNDWatchCriteriaType] (
		[WatchCriteriaTypeID]
		)
		ALTER TABLE [Monitoring].[tNDModelActionItem] WITH CHECK ADD CONSTRAINT [tNDModel_tNDModelActionItem_FK1] FOREIGN KEY (
		[ModelID]
		)
		REFERENCES [Monitoring].[tNDModel] (
		[ModelID]
		)
		
		ALTER TABLE [Monitoring].[tNDModel_AlertScreening_FromAsset360] WITH CHECK ADD CONSTRAINT [tNDModel_AlertScreening_tNDModel_AlertScreening_FromAsset360_FK1] FOREIGN KEY (
		[ModelID]
		)
		REFERENCES [Monitoring].[tNDModel_AlertScreening] (
		[ModelID]
		)
		
		ALTER TABLE [Monitoring].[tNDModel_AlertScreening] WITH CHECK ADD CONSTRAINT [tNDModel_tNDModel_AlertScreening_FK1] FOREIGN KEY (
		[ModelID]
		)
		REFERENCES [Monitoring].[tNDModel] (
		[ModelID]
		)
		
		ALTER TABLE [Monitoring].[tNDAlertPriority] WITH CHECK ADD CONSTRAINT [tNDModel_AlertScreening_FromND_tNDAlertPriority_FK1] FOREIGN KEY (
		[ModelID]
		)
		REFERENCES [Monitoring].[tNDModel_AlertScreening_FromND] (
		[ModelID]
		)
		ALTER TABLE [Monitoring].[tNDAlertPriority] WITH CHECK ADD CONSTRAINT [tNDAlertPriorityType_tNDAlertPriority_FK1] FOREIGN KEY (
		[AlertPriorityTypeID]
		)
		REFERENCES [Monitoring].[tNDAlertPriorityType] (
		[AlertPriorityTypeID]
		)
		
		ALTER TABLE [Monitoring].[tNDModel_AlertScreening_FromND] WITH CHECK ADD CONSTRAINT [tNDModel_AlertScreening_tNDModel_AlertScreening_FromND_FK1] FOREIGN KEY (
		[ModelID]
		)
		REFERENCES [Monitoring].[tNDModel_AlertScreening] (
		[ModelID]
		)
		
		ALTER TABLE [Monitoring].[tNDModelIssueMap] WITH CHECK ADD CONSTRAINT [tNDModel_tNDModelIssueMap_FK1] FOREIGN KEY (
		[ModelID]
		)
		REFERENCES [Monitoring].[tNDModel] (
		[ModelID]
		)
		ALTER TABLE [Monitoring].[tNDModelIssueMap] WITH CHECK ADD CONSTRAINT [tAssetIssue_tNDModelIssueMap_FK1] FOREIGN KEY (
		[AssetIssueID]
		)
		REFERENCES [Diagnostics].[tAssetIssue] (
		[AssetIssueID]
		)
		
		-- triggers
		EXEC('
			CREATE TRIGGER [Monitoring].[tNDModelActionItem_Update]
			   ON  [Monitoring].[tNDModelActionItem]
			   AFTER UPDATE
			AS 
			BEGIN
				 INSERT INTO [Monitoring].[tNDModelActionItemArchive]
				 (	  [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID],
					 [Favorite]
				 )
				 SELECT [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID],
					 [Favorite]
				 FROM [INSERTED] i
			END
		');

		EXEC('
			CREATE TRIGGER [Monitoring].[tNDModelActionItem_Insert]
			   ON  [Monitoring].[tNDModelActionItem]
			   AFTER INSERT
			AS 
			BEGIN
				 INSERT INTO [Monitoring].[tNDModelActionItemArchive]
				 (	  [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID],
					 [Favorite]
				 )
				 SELECT [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID],
					 [Favorite]
				 FROM [INSERTED] i
			END
		');
		
		EXEC('
			CREATE TRIGGER [Monitoring].[tNDModelActionItem_Delete]
			   ON  [Monitoring].[tNDModelActionItem]
			   AFTER DELETE
			AS 
			BEGIN
				DELETE [Monitoring].[tNDModelActionItemArchive]	 
				FROM [Monitoring].[tNDModelActionItemArchive] a
				INNER JOIN [DELETED] d ON a.[ModelID] = d.ModelId
			END
		');
		
		-- stored procs
		EXEC('
			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[Monitoring].[spNDModelsFiltered]'') AND type in (N''P'', N''PC''))
			DROP PROCEDURE [Monitoring].[spNDModelsFiltered]
			')
			
		EXEC('
			CREATE PROCEDURE [Monitoring].[spNDModelsFiltered] 
				-- Add the parameters for the stored procedure here
				@securityUserID int,
				@startAssetID int = NULL,
				@assetId NVARCHAR(10) = NULL,
				@active bit = NULL,
				@tagName NVARCHAR(400) = NULL,
				@tagDesc NVARCHAR(400) = NULL,
				@modelName NVARCHAR(255) = NULL,
				@modelTypeID Base.tpIntList READONLY,
				@predScore NVARCHAR(10) = NULL,
				@engUnits NVARCHAR(255) = NULL,
				@modelOpModeID Base.tpIntList READONLY,
				@startIndex int = 1,
				@endIndex int = 15,
				@sort NVARCHAR(100) = NULL,
				@sortOrder bit = 0,
				@isDiagnose bit = NULL,
				@isWatch bit = NULL,
				@isMaintenance bit = NULL,
				@isOpenIssue bit = NULL,
				@priority int = NULL,
				@unit nvarchar(255) = NULL,
				@isAlert bit = NULL,
				@isIgnore bit = NULL
			WITH RECOMPILE
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;
				SET ARITHABORT ON

				DECLARE @assets base.tpIntList
				INSERT INTO @assets SELECT AssetId from Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID,@startAssetID);

				DECLARE @modelIDs TABLE(
					ModelID int
				)

				INSERT INTO @modelIDs (ModelID)
				SELECT
					modelIDCheck.ModelID
				FROM [Monitoring].[ufnGetFilteredModelIDsFromAssetIDs] (
					@assetId, @active, @tagName, @tagDesc, @modelName,
					@modelTypeID, @predScore, @engUnits, @modelOpModeID, @assets
				) modelIDCheck;

				-- Insert statements for procedure here
				WITH records as (
					SELECT ROW_NUMBER() OVER (
					
					ORDER BY
					CASE WHEN @sort = ''ModelID'' AND @sortOrder = 0 THEN model.ModelID ELSE NULL END,
					CASE WHEN @sort = ''Active'' AND @sortOrder = 0 THEN model.Active ELSE NULL END,
					CASE WHEN @sort = ''TagName'' AND @sortOrder = 0 THEN model.TagName ELSE NULL END,
					CASE WHEN @sort = ''TagDesc'' AND @sortOrder = 0 THEN model.TagDesc ELSE NULL END,
					CASE WHEN @sort = ''ModelName'' AND @sortOrder = 0 THEN model.ModelName ELSE NULL END,
					CASE WHEN @sort = ''ModelTypeAbbrev'' AND @sortOrder = 0 THEN model.ModelTypeAbbrev ELSE NULL END,
					CASE WHEN @sort = ''Score'' AND @sortOrder = 0 THEN model.Score ELSE NULL END,
					CASE WHEN @sort = ''EngUnits'' AND @sortOrder = 0 THEN model.EngUnits ELSE NULL END,
					CASE WHEN @sort = ''AssetAbbrev'' AND @sortOrder = 0 THEN model.AssetAbbrev ELSE NULL END,
					CASE WHEN @sort = ''OpModeTypeAbbrev'' AND @sortOrder = 0 THEN model.OpModeTypeAbbrev ELSE NULL END,
					CASE WHEN @sort = ''NotePriority'' AND @sortOrder = 0 THEN model.NotePriority ELSE NULL END,
					CASE WHEN @sort = ''TagGroup'' AND @sortOrder = 0 THEN model.TagGroup ELSE NULL END,
					CASE WHEN @sort = ''AlertPriority,TagGroup'' AND @sortOrder = 0 THEN Right(''00000000'' + CAST(model.[AlertPriority] AS nvarchar(8)), 8) + model.[TagGroup] ELSE NULL END,
					CASE WHEN @sort = ''ModelID'' AND @sortOrder = 1 THEN model.ModelID ELSE NULL END DESC,
					CASE WHEN @sort = ''Active'' AND @sortOrder = 1 THEN model.Active ELSE NULL END DESC,
					CASE WHEN @sort = ''TagName'' AND @sortOrder = 1 THEN model.TagName ELSE NULL END DESC,
					CASE WHEN @sort = ''TagDesc'' AND @sortOrder = 1 THEN model.TagDesc ELSE NULL END DESC,
					CASE WHEN @sort = ''ModelName'' AND @sortOrder = 1 THEN model.ModelName ELSE NULL END DESC,
					CASE WHEN @sort = ''ModelTypeAbbrev'' AND @sortOrder = 1 THEN model.ModelTypeAbbrev ELSE NULL END DESC,
					CASE WHEN @sort = ''Score'' AND @sortOrder = 1 THEN model.Score ELSE NULL END DESC,
					CASE WHEN @sort = ''EngUnits'' AND @sortOrder = 1 THEN model.EngUnits ELSE NULL END DESC,
					CASE WHEN @sort = ''AssetAbbrev'' AND @sortOrder = 1 THEN model.AssetAbbrev ELSE NULL END DESC,
					CASE WHEN @sort = ''OpModeTypeAbbrev'' AND @sortOrder = 1 THEN model.OpModeTypeAbbrev ELSE NULL END DESC,
					CASE WHEN @sort = ''NotePriority'' AND @sortOrder = 1 THEN model.NotePriority ELSE NULL END DESC,
					CASE WHEN @sort = ''TagGroup'' AND @sortOrder = 1 THEN model.TagGroup ELSE NULL END DESC,
					CASE WHEN @sort = ''AlertPriority,TagGroup'' AND @sortOrder = 1 THEN Right(''00000000'' + CAST(model.[AlertPriority] AS nvarchar(8)), 8) + model.[TagGroup] ELSE NULL END DESC,
					model.ModelID
					) AS ROW_NUMBER,
						model.ModelID
					FROM (
						SELECT mdl.ModelID, 
							mdl.Active, 
							tag.TagName, 
							tag.TagDesc, 
							mdl.ModelName, 
							mdl.ModelTypeID, 
							modeltype.ModelTypeAbbrev, 
							predmap.Score, 
							tag.EngUnits, 
							ast.AssetAbbrev, 
							mdl.OpModeTypeID, 
							opmode.OpModeTypeAbbrev, 
							ast.AssetID, 
							params.MAELower, 
							params.MAEUpper, 
							tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
							tnst.AlertStatusPriorityTypeID AS AlertPriority,
							tmai.IsDiagnose AS ActionDiagnose,
							tmai.IsWatch AS ActionWatch,
							tmai.IsModelMaintenance AS ActionModelMaintenance,
							tmasfa.HasOpenIssue AS HasOpenIssue,
							tmasfn.PercentOOB AS PercentOutOfBounds,
							tmasfn.UpperLimit AS UpperLimit,
							tmasfn.LowerLimit AS LowerLimit,
							tmasfn.Actual AS Actual,
							tmasfn.Expected AS Expected,
							mdl.ActiveSinceTime AS AddedToAlerts,
							tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
							tmai.NoteText AS LastNote,
							tmai.CreateDate AS LastNoteDate,
							tmas.TagGroup AS TagGroup,
							CASE WHEN tmasfn.PercentOOB > 0 THEN 1 ELSE 0 END AS OutOfBounds,
							tmas.IgnoreStatus AS Ignore,
							tnst.AlertStatusTypeAbbrev AS StatusInfo
						FROM Monitoring.tNDModel mdl
							JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
							JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
							JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
							LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
							JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
							JOIN Monitoring.tNDOpModeType opmode ON mdl.OpModeTypeID = opmode.OpModeTypeID
							JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
							LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
							LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
							LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
							LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
							LEFT JOIN Monitoring.tNDModel_AlertScreening_FromAsset360 tmasfa ON tmasfa.ModelID = mdl.ModelID
							LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
							LEFT JOIN [Asset].[tUnit] tu ON map.[AssetID] = tu.[AssetID]
						WHERE ISNULL([tmai].[IsDiagnose], '''') = COALESCE(@isDiagnose, [tmai].[IsDiagnose], '''')
							AND ISNULL([tmai].[IsWatch], '''') = COALESCE(@isWatch, [tmai].[IsWatch], '''')
							AND ISNULL([tmai].[IsModelMaintenance], '''') = COALESCE(@isMaintenance, [tmai].[IsModelMaintenance], '''')
							AND ISNULL([tmasfa].[HasOpenIssue], '''') = COALESCE(@isOpenIssue, [tmasfa].[HasOpenIssue], '''')
							AND ISNULL([tmai].[ModelActionItemPriorityTypeID], '''') = COALESCE(@priority, [tmai].[ModelActionItemPriorityTypeID], '''')
							AND ISNULL([tu].[UnitDesc], '''') LIKE COALESCE(@unit, tu.[UnitDesc], '''')
							AND ISNULL([mdl].[Active], '''') = COALESCE(@isAlert, [mdl].[Active], '''')
							AND ISNULL([tmas].[IgnoreStatus], '''') = COALESCE(@isIgnore, [tmas].[IgnoreStatus], '''')
					) model
					INNER JOIN @assets branch ON model.AssetID = branch.id
					INNER JOIN @modelIDs modelIDCheck ON modelIDCheck.ModelID = model.ModelID
				)

				SELECT
					r.ROW_NUMBER
					, modelrecs.*
				FROM (
					 SELECT mdl.ModelID, 
							mdl.Active, 
							tag.TagName, 
							tag.TagDesc, 
							mdl.ModelName, 
							mdl.ModelTypeID, 
							modeltype.ModelTypeAbbrev, 
							predmap.Score, 
							tag.EngUnits, 
							ast.AssetAbbrev, 
							mdl.OpModeTypeID, 
							opmode.OpModeTypeAbbrev, 
							ast.AssetID, 
							params.MAELower, 
							params.MAEUpper, 
							tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
							tnst.AlertStatusPriorityTypeID AS AlertPriority,
							tmai.IsDiagnose AS ActionDiagnose,
							tmai.IsWatch AS ActionWatch,
							tmai.IsModelMaintenance AS ActionModelMaintenance,
							tmasfa.HasOpenIssue AS HasOpenIssue,
							tmasfn.PercentOOB AS PercentOutOfBounds,
							tmasfn.UpperLimit AS UpperLimit,
							tmasfn.LowerLimit AS LowerLimit,
							tmasfn.Actual AS Actual,
							tmasfn.Expected AS Expected,
							mdl.ActiveSinceTime AS AddedToAlerts,
							tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
							tmai.NoteText AS LastNote,
							tmai.CreateDate AS LastNoteDate,
							tmas.TagGroup AS TagGroup,
							CASE WHEN tmasfn.PercentOOB > 0 THEN 1 ELSE 0 END AS OutOfBounds,
							tmas.IgnoreStatus AS Ignore,
							tnst.AlertStatusTypeAbbrev AS StatusInfo
					 FROM Monitoring.tNDModel mdl
						  JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
						  JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
						  JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
						  LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
						  JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
						  JOIN Monitoring.tNDOpModeType opmode ON mdl.OpModeTypeID = opmode.OpModeTypeID
						  JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
						  LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
						  LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
						  LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
						  LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
						  LEFT JOIN Monitoring.tNDModel_AlertScreening_FromAsset360 tmasfa ON tmasfa.ModelID = mdl.ModelID
						  LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
						  LEFT JOIN [Asset].[tUnit] tu ON map.[AssetID] = tu.[AssetID]
						WHERE ISNULL([tmai].[IsDiagnose], '''') = COALESCE(@isDiagnose, [tmai].[IsDiagnose], '''')
							AND ISNULL([tmai].[IsWatch], '''') = COALESCE(@isWatch, [tmai].[IsWatch], '''')
							AND ISNULL([tmai].[IsModelMaintenance], '''') = COALESCE(@isMaintenance, [tmai].[IsModelMaintenance], '''')
							AND ISNULL([tmasfa].[HasOpenIssue], '''') = COALESCE(@isOpenIssue, [tmasfa].[HasOpenIssue], '''')
							AND ISNULL([tmai].[ModelActionItemPriorityTypeID], '''') = COALESCE(@priority, [tmai].[ModelActionItemPriorityTypeID], '''')
							AND ISNULL([tu].[UnitDesc], '''') LIKE COALESCE(@unit, tu.[UnitDesc], '''')
							AND ISNULL([mdl].[Active], '''') = COALESCE(@isAlert, [mdl].[Active], '''')
							AND ISNULL([tmas].[IgnoreStatus], '''') = COALESCE(@isIgnore, [tmas].[IgnoreStatus], '''')
					) modelrecs
				INNER JOIN records r ON modelrecs.ModelID = r.ModelID
				WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex
				ORDER BY r.ROW_NUMBER
			END
		');

		EXEC('
			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[Monitoring].[spGetNDModelSummary]'') AND type in (N''P'', N''PC''))
			DROP PROCEDURE [Monitoring].[spGetNDModelSummary]
		')
		
		EXEC('		
			CREATE PROCEDURE [Monitoring].[spGetNDModelSummary]
				@modelId int,
				@userId int
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;
				SELECT mdl.ModelID, 
					mdl.Active, 
					tag.TagName, 
					tag.TagDesc, 
					mdl.ModelName, 
					mdl.ModelTypeID, 
					modeltype.ModelTypeAbbrev, 
					predmap.Score, 
					tag.EngUnits, 
					ast.AssetAbbrev, 
					mdl.OpModeTypeID, 
					opmode.OpModeTypeAbbrev, 
					ast.AssetID, 
					params.MAELower, 
					params.MAEUpper, 
					tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
					tnst.AlertStatusPriorityTypeID AS AlertPriority,
					tmai.IsDiagnose AS ActionDiagnose,
					tmai.IsWatch AS ActionWatch,
					tmai.IsModelMaintenance AS ActionModelMaintenance,
					tmasfa.HasOpenIssue AS HasOpenIssue,
					tmasfn.PercentOOB AS PercentOutOfBounds,
					tmasfn.UpperLimit AS UpperLimit,
					tmasfn.LowerLimit AS LowerLimit,
					tmasfn.Actual AS Actual,
					tmasfn.Expected AS Expected,
					mdl.ActiveSinceTime AS AddedToAlerts,
					tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
					tmai.NoteText AS LastNote,
					tmai.CreateDate AS LastNoteDate,
					tmas.TagGroup AS TagGroup,
					CASE WHEN tmasfn.PercentOOB > 0 THEN 1 ELSE 0 END AS OutOfBounds,
					tmas.IgnoreStatus AS Ignore,
					tnst.AlertStatusTypeAbbrev AS StatusInfo
				FROM Monitoring.tNDModel mdl
					JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
					JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
					JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
					JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
					INNER JOIN (
							 SELECT AssetId 
							 FROM [Asset].[ufnGetAssetIdsForUser] (@userId)
					  ) accessibleAssetIds ON map.AssetID = accessibleAssetIds.AssetId
					LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
					JOIN Monitoring.tNDOpModeType opmode ON mdl.OpModeTypeID = opmode.OpModeTypeID
					JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
					LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
					LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
					LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModel_AlertScreening_FromAsset360 tmasfa ON tmasfa.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
					LEFT JOIN Asset.tUnit tu ON map.AssetID = tu.AssetID
				WHERE mdl.ModelID = @modelId		  
			END
		');

		EXEC('
			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[Monitoring].[spSaveNDModelSummary]'') AND type in (N''P'', N''PC''))
			DROP PROCEDURE [Monitoring].[spSaveNDModelSummary];
		')
		
		EXEC('
			CREATE PROCEDURE [Monitoring].[spSaveNDModelSummary]
				@modelId int,
				@modelActionItemPriorityTypeId int,
				@isDiagnose bit,
				@isWatch bit,
				@IsModelMaintenance bit,
				@noteText nvarchar(255),
				@watchDuration int,
				@watchDurationTemporalTypeId int,
				@watchCriteriaTypeId int,
				@watchLimit int,
				@watchLimitMeetsTemporalTypeId int,
				@userId int
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				IF EXISTS (SELECT tmai.ModelID FROM Monitoring.tNDModelActionItem tmai WHERE tmai.ModelID=@modelId ) BEGIN
					UPDATE Monitoring.tNDModelActionItem
					SET	Monitoring.tNDModelActionItem.ModelActionItemPriorityTypeID = @modelActionItemPriorityTypeId, -- int
						Monitoring.tNDModelActionItem.IsDiagnose = @isDiagnose, -- bit
						Monitoring.tNDModelActionItem.IsWatch = @isWatch, -- bit
						Monitoring.tNDModelActionItem.IsModelMaintenance = @isModelMaintenance, -- bit
						Monitoring.tNDModelActionItem.NoteText = @noteText, -- nvarchar
						Monitoring.tNDModelActionItem.ChangedByUserID = @userId, -- int
						Monitoring.tNDModelActionItem.ChangeDate = GETDATE(),
						Monitoring.tNDModelActionItem.WatchDuration = @watchDuration, -- real
						Monitoring.tNDModelActionItem.WatchDurationTemporalTypeID = @watchDurationTemporalTypeId, -- int
						Monitoring.tNDModelActionItem.WatchCriteriaTypeID = @watchCriteriaTypeId, -- int
						Monitoring.tNDModelActionItem.WatchLimit = @watchLimit, -- real
						Monitoring.tNDModelActionItem.WatchLimitMeetsTemporalTypeID = @watchDurationTemporalTypeId -- int
					FROM Monitoring.tNDModelActionItem tmai
					INNER JOIN Monitoring.tNDModel tn ON tn.ModelID = tmai.ModelID
					INNER JOIN ProcessData.tAssetVariableTypeTagMap tavttm ON tavttm.AssetVariableTypeTagMapID = tn.AssetVariableTypeTagMapID
					INNER JOIN (
						 SELECT AssetId 
						 FROM [Asset].[ufnGetAssetIdsForUser] (@userId)
					) accessibleAssetIds ON tavttm.AssetID = accessibleAssetIds.AssetId
					WHERE tmai.ModelID = @modelId
				END ELSE BEGIN
					INSERT INTO Monitoring.tNDModelActionItem
					(
						ModelID,
						ModelActionItemPriorityTypeID,
						IsDiagnose,
						IsWatch,
						IsModelMaintenance,
						NoteText,
						CreatedByUserID,
						CreateDate,
						ChangedByUserID,
						ChangeDate,
						WatchDuration,
						WatchDurationTemporalTypeID,
						WatchCriteriaTypeID,
						WatchLimit,
						WatchLimitMeetsTemporalTypeID
					)
					VALUES
					(
						@modelId, -- ModelID - int
						@modelActionItemPriorityTypeId, -- ModelActionItemPriorityTypeID - int
						@isDiagnose, -- IsDiagnose - bit
						@isWatch, -- IsWatch - bit
						@IsModelMaintenance, -- IsModelMaintenance - bit
						@noteText, -- NoteText - nvarchar
						@userId, -- CreatedByUserID - int
						GetDate(), -- CreateDate - datetime
						@userId, -- ChangedByUserID - int
						GetDate(), -- ChangeDate - datetime
						@watchDuration, -- WatchDuration - real
						@watchDurationTemporalTypeId, -- WatchDurationTemporalTypeID - int
						@watchCriteriaTypeId, -- WatchCriteriaTypeID - int
						@watchLimit, -- WatchLimit - real
						@watchLimitMeetsTemporalTypeId -- WatchLimitMeetsTemporalTypeID - int
					)
				END
			END
		');


		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tAlertPriorityType_tAlertPriority_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tAlertPriority]'))
		ALTER TABLE [Monitoring].[tAlertPriority] DROP CONSTRAINT [tAlertPriorityType_tAlertPriority_FK1]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromND_tAlertPriority_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tAlertPriority]'))
		ALTER TABLE [Monitoring].[tAlertPriority] DROP CONSTRAINT [tModel_AlertScreening_FromND_tAlertPriority_FK1]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tAlertPriority]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tAlertPriority]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tAlertPriorityType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tAlertPriorityType]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_tModel_AlertScreening_FromND_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromND]'))
		ALTER TABLE [Monitoring].[tModel_AlertScreening_FromND] DROP CONSTRAINT [tModel_AlertScreening_tModel_AlertScreening_FromND_FK1]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromND]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModel_AlertScreening_FromND]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_tModel_AlertScreening_FromAsset360_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromAsset360]'))
		ALTER TABLE [Monitoring].[tModel_AlertScreening_FromAsset360] DROP CONSTRAINT [tModel_AlertScreening_tModel_AlertScreening_FromAsset360_FK1]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromAsset360]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModel_AlertScreening_FromAsset360]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModel_tModel_AlertScreening_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening]'))
		ALTER TABLE [Monitoring].[tModel_AlertScreening] DROP CONSTRAINT [tNDModel_tModel_AlertScreening_FK1]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModel_AlertScreening]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItemArchive]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModelActionItemArchive]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItemPriorityType_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tModelActionItemPriorityType_tModelActionItem_FK1]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModel_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tNDModel_tModelActionItem_FK1]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchCriteriaType_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tWatchCriteriaType_tModelActionItem_FK1]

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchLimitMeetsTemporalType_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tWatchLimitMeetsTemporalType_tModelActionItem_FK1]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModelActionItem]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItemPriorityType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModelActionItemPriorityType]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchCriteriaType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tWatchCriteriaType]

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchLimitMeetsTemporalType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tWatchLimitMeetsTemporalType]

		
		COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION @TranName;
	PRINT N'Script Failed, rolling back';
	PRINT  ERROR_NUMBER() | ERROR_SEVERITY() |  ERROR_STATE() | ERROR_PROCEDURE() | ERROR_MESSAGE();
END CATCH;

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('6.0.0.0-14','Release 6.6.0.0 Update Monitoring ND Alerts.','FromVersion_6-0-0-0_Script14_Update_Monitoring_ND_Alerts.sql')


GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tAssetIssue_tNDModelIssueMap_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tNDModelIssueMap]'))
		ALTER TABLE [Monitoring].[tNDModelIssueMap] DROP CONSTRAINT [tAssetIssue_tNDModelIssueMap_FK1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModel_tNDModelIssueMap_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tNDModelIssueMap]'))
		ALTER TABLE [Monitoring].[tNDModelIssueMap] DROP CONSTRAINT [tNDModel_tNDModelIssueMap_FK1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModelIssueMap]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tNDModelIssueMap]
GO

-- triggers
		EXEC('
			CREATE TRIGGER [Monitoring].[tNDModelActionItem_Update]
			   ON  [Monitoring].[tNDModelActionItem]
			   AFTER UPDATE
			AS 
			BEGIN
				 INSERT INTO [Monitoring].[tNDModelActionItemArchive]
				 (	  [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID]
				 )
				 SELECT [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID]
				 FROM [INSERTED] i
			END
		');
GO

EXEC('
			CREATE TRIGGER [Monitoring].[tNDModelActionItem_Insert]
			   ON  [Monitoring].[tNDModelActionItem]
			   AFTER INSERT
			AS 
			BEGIN
				 INSERT INTO [Monitoring].[tNDModelActionItemArchive]
				 (	  [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID]
				 )
				 SELECT [ModelID],
					 [ModelActionItemPriorityTypeID],
					 [IsDiagnose],
					 [IsWatch],
					 [IsModelMaintenance],
					 [NoteText],
					 [CreatedByUserID],
					 [CreateDate],
					 [ChangedByUserID],
					 [ChangeDate],
					 [WatchDuration],
					 [WatchDurationTemporalTypeID],
					 [WatchCriteriaTypeID],
					 [WatchLimit],
					 [WatchLimitMeetsTemporalTypeID]
				 FROM [INSERTED] i
			END
		');
GO

EXEC('
			CREATE TRIGGER [Monitoring].[tNDModelActionItem_Delete]
			   ON  [Monitoring].[tNDModelActionItem]
			   AFTER DELETE
			AS 
			BEGIN
				DELETE [Monitoring].[tNDModelActionItemArchive]	 
				FROM [Monitoring].[tNDModelActionItemArchive] a
				INNER JOIN [DELETED] d ON a.[ModelID] = d.ModelId
			END
		');
GO

-- stored procs
		EXEC('
			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[Monitoring].[spNDModelsFiltered]'') AND type in (N''P'', N''PC''))
			DROP PROCEDURE [Monitoring].[spNDModelsFiltered]
			')
GO

EXEC('
			CREATE PROCEDURE [Monitoring].[spNDModelsFiltered] 
				-- Add the parameters for the stored procedure here
				@securityUserID int,
				@startAssetID int = NULL,
				@assetId NVARCHAR(10) = NULL,
				@active bit = NULL,
				@tagName NVARCHAR(400) = NULL,
				@tagDesc NVARCHAR(400) = NULL,
				@modelName NVARCHAR(255) = NULL,
				@modelTypeID Base.tpIntList READONLY,
				@predScore NVARCHAR(10) = NULL,
				@engUnits NVARCHAR(255) = NULL,
				@modelOpModeID Base.tpIntList READONLY,
				@startIndex int = 1,
				@endIndex int = 15,
				@sort NVARCHAR(100) = NULL,
				@sortOrder bit = 0,
				@isDiagnose bit = NULL,
				@isWatch bit = NULL,
				@isMaintenance bit = NULL,
				@isOpenIssue bit = NULL,
				@priority int = NULL,
				@unit nvarchar(255) = NULL,
				@isAlert bit = NULL,
				@isIgnore bit = NULL
			WITH RECOMPILE
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;
				SET ARITHABORT ON

				DECLARE @assets base.tpIntList
				INSERT INTO @assets SELECT AssetId from Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID,@startAssetID);

				DECLARE @modelIDs TABLE(
					ModelID int
				)

				INSERT INTO @modelIDs (ModelID)
				SELECT
					modelIDCheck.ModelID
				FROM [Monitoring].[ufnGetFilteredModelIDsFromAssetIDs] (
					@assetId, @active, @tagName, @tagDesc, @modelName,
					@modelTypeID, @predScore, @engUnits, @modelOpModeID, @assets
				) modelIDCheck;

				-- Insert statements for procedure here
				WITH records as (
					SELECT ROW_NUMBER() OVER (
					
					ORDER BY
					CASE WHEN @sort = ''ModelID'' AND @sortOrder = 0 THEN model.ModelID ELSE NULL END,
					CASE WHEN @sort = ''Active'' AND @sortOrder = 0 THEN model.Active ELSE NULL END,
					CASE WHEN @sort = ''TagName'' AND @sortOrder = 0 THEN model.TagName ELSE NULL END,
					CASE WHEN @sort = ''TagDesc'' AND @sortOrder = 0 THEN model.TagDesc ELSE NULL END,
					CASE WHEN @sort = ''ModelName'' AND @sortOrder = 0 THEN model.ModelName ELSE NULL END,
					CASE WHEN @sort = ''ModelTypeAbbrev'' AND @sortOrder = 0 THEN model.ModelTypeAbbrev ELSE NULL END,
					CASE WHEN @sort = ''Score'' AND @sortOrder = 0 THEN model.Score ELSE NULL END,
					CASE WHEN @sort = ''EngUnits'' AND @sortOrder = 0 THEN model.EngUnits ELSE NULL END,
					CASE WHEN @sort = ''AssetAbbrev'' AND @sortOrder = 0 THEN model.AssetAbbrev ELSE NULL END,
					CASE WHEN @sort = ''OpModeTypeAbbrev'' AND @sortOrder = 0 THEN model.OpModeTypeAbbrev ELSE NULL END,
					CASE WHEN @sort = ''NotePriority'' AND @sortOrder = 0 THEN model.NotePriority ELSE NULL END,
					CASE WHEN @sort = ''TagGroup'' AND @sortOrder = 0 THEN model.TagGroup ELSE NULL END,
					CASE WHEN @sort = ''AlertPriority,TagGroup'' AND @sortOrder = 0 THEN Right(''00000000'' + CAST(model.[AlertPriority] AS nvarchar(8)), 8) + model.[TagGroup] ELSE NULL END,
					CASE WHEN @sort = ''ModelID'' AND @sortOrder = 1 THEN model.ModelID ELSE NULL END DESC,
					CASE WHEN @sort = ''Active'' AND @sortOrder = 1 THEN model.Active ELSE NULL END DESC,
					CASE WHEN @sort = ''TagName'' AND @sortOrder = 1 THEN model.TagName ELSE NULL END DESC,
					CASE WHEN @sort = ''TagDesc'' AND @sortOrder = 1 THEN model.TagDesc ELSE NULL END DESC,
					CASE WHEN @sort = ''ModelName'' AND @sortOrder = 1 THEN model.ModelName ELSE NULL END DESC,
					CASE WHEN @sort = ''ModelTypeAbbrev'' AND @sortOrder = 1 THEN model.ModelTypeAbbrev ELSE NULL END DESC,
					CASE WHEN @sort = ''Score'' AND @sortOrder = 1 THEN model.Score ELSE NULL END DESC,
					CASE WHEN @sort = ''EngUnits'' AND @sortOrder = 1 THEN model.EngUnits ELSE NULL END DESC,
					CASE WHEN @sort = ''AssetAbbrev'' AND @sortOrder = 1 THEN model.AssetAbbrev ELSE NULL END DESC,
					CASE WHEN @sort = ''OpModeTypeAbbrev'' AND @sortOrder = 1 THEN model.OpModeTypeAbbrev ELSE NULL END DESC,
					CASE WHEN @sort = ''NotePriority'' AND @sortOrder = 1 THEN model.NotePriority ELSE NULL END DESC,
					CASE WHEN @sort = ''TagGroup'' AND @sortOrder = 1 THEN model.TagGroup ELSE NULL END DESC,
					CASE WHEN @sort = ''AlertPriority,TagGroup'' AND @sortOrder = 1 THEN Right(''00000000'' + CAST(model.[AlertPriority] AS nvarchar(8)), 8) + model.[TagGroup] ELSE NULL END DESC,
					model.ModelID
					) AS ROW_NUMBER,
						model.ModelID
					FROM (
						SELECT mdl.ModelID, 
							mdl.Active, 
							tag.TagName, 
							tag.TagDesc, 
							mdl.ModelName, 
							mdl.ModelTypeID, 
							modeltype.ModelTypeAbbrev, 
							predmap.Score, 
							tag.EngUnits, 
							ast.AssetAbbrev, 
							mdl.OpModeTypeID, 
							opmode.OpModeTypeAbbrev, 
							ast.AssetID, 
							params.MAELower, 
							params.MAEUpper, 
							tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
							tnst.AlertStatusPriorityTypeID AS AlertPriority,
							tmai.IsDiagnose AS ActionDiagnose,
							tmai.IsWatch AS ActionWatch,
							tmai.IsModelMaintenance AS ActionModelMaintenance,
							tmasfa.HasOpenIssue AS HasOpenIssue,
							tmasfn.PercentOOB AS PercentOutOfBounds,
							tmasfn.UpperLimit AS UpperLimit,
							tmasfn.LowerLimit AS LowerLimit,
							tmasfn.Actual AS Actual,
							tmasfn.Expected AS Expected,
							mdl.ActiveSinceTime AS AddedToAlerts,
							tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
							tmai.NoteText AS LastNote,
							tmai.CreateDate AS LastNoteDate,
							tmas.TagGroup AS TagGroup,
							CASE WHEN tmasfn.PercentOOB > 0 THEN 1 ELSE 0 END AS OutOfBounds,
							tmas.IgnoreStatus AS Ignore,
							tnst.AlertStatusTypeAbbrev AS StatusInfo
						FROM Monitoring.tNDModel mdl
							JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
							JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
							JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
							LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
							JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
							JOIN Monitoring.tNDOpModeType opmode ON mdl.OpModeTypeID = opmode.OpModeTypeID
							JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
							LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
							LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
							LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
							LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
							LEFT JOIN Monitoring.tNDModel_AlertScreening_FromAsset360 tmasfa ON tmasfa.ModelID = mdl.ModelID
							LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
							LEFT JOIN [Asset].[tUnit] tu ON map.[AssetID] = tu.[AssetID]
						WHERE ISNULL([tmai].[IsDiagnose], '''') = COALESCE(@isDiagnose, [tmai].[IsDiagnose], '''')
							AND ISNULL([tmai].[IsWatch], '''') = COALESCE(@isWatch, [tmai].[IsWatch], '''')
							AND ISNULL([tmai].[IsModelMaintenance], '''') = COALESCE(@isMaintenance, [tmai].[IsModelMaintenance], '''')
							AND ISNULL([tmasfa].[HasOpenIssue], '''') = COALESCE(@isOpenIssue, [tmasfa].[HasOpenIssue], '''')
							AND ISNULL([tmai].[ModelActionItemPriorityTypeID], '''') = COALESCE(@priority, [tmai].[ModelActionItemPriorityTypeID], '''')
							AND ISNULL([tu].[UnitDesc], '''') LIKE COALESCE(@unit, tu.[UnitDesc], '''')
							AND ISNULL([mdl].[Active], '''') = COALESCE(@isAlert, [mdl].[Active], '''')
							AND ISNULL([tmas].[IgnoreStatus], '''') = COALESCE(@isIgnore, [tmas].[IgnoreStatus], '''')
					) model
					INNER JOIN @assets branch ON model.AssetID = branch.id
					INNER JOIN @modelIDs modelIDCheck ON modelIDCheck.ModelID = model.ModelID
				)

				SELECT
					r.ROW_NUMBER
					, modelrecs.*
				FROM (
					 SELECT mdl.ModelID, 
							mdl.Active, 
							tag.TagName, 
							tag.TagDesc, 
							mdl.ModelName, 
							mdl.ModelTypeID, 
							modeltype.ModelTypeAbbrev, 
							predmap.Score, 
							tag.EngUnits, 
							ast.AssetAbbrev, 
							mdl.OpModeTypeID, 
							opmode.OpModeTypeAbbrev, 
							ast.AssetID, 
							params.MAELower, 
							params.MAEUpper, 
							tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
							tnst.AlertStatusPriorityTypeID AS AlertPriority,
							tmai.IsDiagnose AS ActionDiagnose,
							tmai.IsWatch AS ActionWatch,
							tmai.IsModelMaintenance AS ActionModelMaintenance,
							tmasfa.HasOpenIssue AS HasOpenIssue,
							tmasfn.PercentOOB AS PercentOutOfBounds,
							tmasfn.UpperLimit AS UpperLimit,
							tmasfn.LowerLimit AS LowerLimit,
							tmasfn.Actual AS Actual,
							tmasfn.Expected AS Expected,
							mdl.ActiveSinceTime AS AddedToAlerts,
							tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
							tmai.NoteText AS LastNote,
							tmai.CreateDate AS LastNoteDate,
							tmas.TagGroup AS TagGroup,
							CASE WHEN tmasfn.PercentOOB > 0 THEN 1 ELSE 0 END AS OutOfBounds,
							tmas.IgnoreStatus AS Ignore,
							tnst.AlertStatusTypeAbbrev AS StatusInfo
					 FROM Monitoring.tNDModel mdl
						  JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
						  JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
						  JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
						  LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
						  JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
						  JOIN Monitoring.tNDOpModeType opmode ON mdl.OpModeTypeID = opmode.OpModeTypeID
						  JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
						  LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
						  LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
						  LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
						  LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
						  LEFT JOIN Monitoring.tNDModel_AlertScreening_FromAsset360 tmasfa ON tmasfa.ModelID = mdl.ModelID
						  LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
						  LEFT JOIN [Asset].[tUnit] tu ON map.[AssetID] = tu.[AssetID]
						WHERE ISNULL([tmai].[IsDiagnose], '''') = COALESCE(@isDiagnose, [tmai].[IsDiagnose], '''')
							AND ISNULL([tmai].[IsWatch], '''') = COALESCE(@isWatch, [tmai].[IsWatch], '''')
							AND ISNULL([tmai].[IsModelMaintenance], '''') = COALESCE(@isMaintenance, [tmai].[IsModelMaintenance], '''')
							AND ISNULL([tmasfa].[HasOpenIssue], '''') = COALESCE(@isOpenIssue, [tmasfa].[HasOpenIssue], '''')
							AND ISNULL([tmai].[ModelActionItemPriorityTypeID], '''') = COALESCE(@priority, [tmai].[ModelActionItemPriorityTypeID], '''')
							AND ISNULL([tu].[UnitDesc], '''') LIKE COALESCE(@unit, tu.[UnitDesc], '''')
							AND ISNULL([mdl].[Active], '''') = COALESCE(@isAlert, [mdl].[Active], '''')
							AND ISNULL([tmas].[IgnoreStatus], '''') = COALESCE(@isIgnore, [tmas].[IgnoreStatus], '''')
					) modelrecs
				INNER JOIN records r ON modelrecs.ModelID = r.ModelID
				WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex
				ORDER BY r.ROW_NUMBER
			END
		');
GO

EXEC('
			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[Monitoring].[spGetNDModelSummary]'') AND type in (N''P'', N''PC''))
			DROP PROCEDURE [Monitoring].[spGetNDModelSummary]
		')
GO

EXEC('		
			CREATE PROCEDURE [Monitoring].[spGetNDModelSummary]
				@modelId int,
				@userId int
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;
				SELECT mdl.ModelID, 
					mdl.Active, 
					tag.TagName, 
					tag.TagDesc, 
					mdl.ModelName, 
					mdl.ModelTypeID, 
					modeltype.ModelTypeAbbrev, 
					predmap.Score, 
					tag.EngUnits, 
					ast.AssetAbbrev, 
					mdl.OpModeTypeID, 
					opmode.OpModeTypeAbbrev, 
					ast.AssetID, 
					params.MAELower, 
					params.MAEUpper, 
					tnst.AlertStatusTypeAbbrev AS AlertStatusType, 
					tnst.AlertStatusPriorityTypeID AS AlertPriority,
					tmai.IsDiagnose AS ActionDiagnose,
					tmai.IsWatch AS ActionWatch,
					tmai.IsModelMaintenance AS ActionModelMaintenance,
					tmasfa.HasOpenIssue AS HasOpenIssue,
					tmasfn.PercentOOB AS PercentOutOfBounds,
					tmasfn.UpperLimit AS UpperLimit,
					tmasfn.LowerLimit AS LowerLimit,
					tmasfn.Actual AS Actual,
					tmasfn.Expected AS Expected,
					mdl.ActiveSinceTime AS AddedToAlerts,
					tmaipt.ModelActionItemPriorityAbbrev AS NotePriority,
					tmai.NoteText AS LastNote,
					tmai.CreateDate AS LastNoteDate,
					tmas.TagGroup AS TagGroup,
					CASE WHEN tmasfn.PercentOOB > 0 THEN 1 ELSE 0 END AS OutOfBounds,
					tmas.IgnoreStatus AS Ignore,
					tnst.AlertStatusTypeAbbrev AS StatusInfo
				FROM Monitoring.tNDModel mdl
					JOIN ProcessData.tAssetVariableTypeTagMap map ON mdl.AssetVariableTypeTagMapID = map.AssetVariableTypeTagMapID
					JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
					JOIN Monitoring.tNDModelType modeltype ON mdl.ModelTypeID = modeltype.ModelTypeID
					JOIN Asset.tAsset ast ON map.AssetID = ast.AssetID
					INNER JOIN (
							 SELECT AssetId 
							 FROM [Asset].[ufnGetAssetIdsForUser] (@userId)
					  ) accessibleAssetIds ON map.AssetID = accessibleAssetIds.AssetId
					LEFT JOIN Monitoring.tNDModelPredictiveMethodMap predmap ON(mdl.ModelID = predmap.ModelID) AND (predmap.Active = 1)
					JOIN Monitoring.tNDOpModeType opmode ON mdl.OpModeTypeID = opmode.OpModeTypeID
					JOIN Monitoring.tNDOpModeParams params ON mdl.OpModeParamsID = params.OpModeParamsID
					LEFT JOIN Monitoring.tNDAlertStatusType tnst ON tnst.AlertStatusTypeID = mdl.AlertStatusTypeID
					LEFT JOIN Monitoring.tNDModelActionItem tmai ON tmai.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModelActionItemPriorityType tmaipt ON tmaipt.ModelActionItemPriorityTypeID = tmai.ModelActionItemPriorityTypeID
					LEFT JOIN Monitoring.tNDModel_AlertScreening tmas ON tmas.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModel_AlertScreening_FromAsset360 tmasfa ON tmasfa.ModelID = mdl.ModelID
					LEFT JOIN Monitoring.tNDModel_AlertScreening_FromND tmasfn ON tmasfn.ModelID = mdl.ModelID
					LEFT JOIN Asset.tUnit tu ON map.AssetID = tu.AssetID
				WHERE mdl.ModelID = @modelId		  
			END
		');
GO

EXEC('
			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[Monitoring].[spSaveNDModelSummary]'') AND type in (N''P'', N''PC''))
			DROP PROCEDURE [Monitoring].[spSaveNDModelSummary];
		')
GO

EXEC('
			CREATE PROCEDURE [Monitoring].[spSaveNDModelSummary]
				@modelId int,
				@modelActionItemPriorityTypeId int,
				@isDiagnose bit,
				@isWatch bit,
				@IsModelMaintenance bit,
				@noteText nvarchar(255),
				@watchDuration int,
				@watchDurationTemporalTypeId int,
				@watchCriteriaTypeId int,
				@watchLimit int,
				@watchLimitMeetsTemporalTypeId int,
				@userId int
			AS
			BEGIN
				-- SET NOCOUNT ON added to prevent extra result sets from
				-- interfering with SELECT statements.
				SET NOCOUNT ON;

				IF EXISTS (SELECT tmai.ModelID FROM Monitoring.tNDModelActionItem tmai WHERE tmai.ModelID=@modelId ) BEGIN
					UPDATE Monitoring.tNDModelActionItem
					SET	Monitoring.tNDModelActionItem.ModelActionItemPriorityTypeID = @modelActionItemPriorityTypeId, -- int
						Monitoring.tNDModelActionItem.IsDiagnose = @isDiagnose, -- bit
						Monitoring.tNDModelActionItem.IsWatch = @isWatch, -- bit
						Monitoring.tNDModelActionItem.IsModelMaintenance = @isModelMaintenance, -- bit
						Monitoring.tNDModelActionItem.NoteText = @noteText, -- nvarchar
						Monitoring.tNDModelActionItem.ChangedByUserID = @userId, -- int
						Monitoring.tNDModelActionItem.ChangeDate = GETDATE(),
						Monitoring.tNDModelActionItem.WatchDuration = @watchDuration, -- real
						Monitoring.tNDModelActionItem.WatchDurationTemporalTypeID = @watchDurationTemporalTypeId, -- int
						Monitoring.tNDModelActionItem.WatchCriteriaTypeID = @watchCriteriaTypeId, -- int
						Monitoring.tNDModelActionItem.WatchLimit = @watchLimit, -- real
						Monitoring.tNDModelActionItem.WatchLimitMeetsTemporalTypeID = @watchDurationTemporalTypeId -- int
					FROM Monitoring.tNDModelActionItem tmai
					INNER JOIN Monitoring.tNDModel tn ON tn.ModelID = tmai.ModelID
					INNER JOIN ProcessData.tAssetVariableTypeTagMap tavttm ON tavttm.AssetVariableTypeTagMapID = tn.AssetVariableTypeTagMapID
					INNER JOIN (
						 SELECT AssetId 
						 FROM [Asset].[ufnGetAssetIdsForUser] (@userId)
					) accessibleAssetIds ON tavttm.AssetID = accessibleAssetIds.AssetId
					WHERE tmai.ModelID = @modelId
				END ELSE BEGIN
					INSERT INTO Monitoring.tNDModelActionItem
					(
						ModelID,
						ModelActionItemPriorityTypeID,
						IsDiagnose,
						IsWatch,
						IsModelMaintenance,
						NoteText,
						CreatedByUserID,
						CreateDate,
						ChangedByUserID,
						ChangeDate,
						WatchDuration,
						WatchDurationTemporalTypeID,
						WatchCriteriaTypeID,
						WatchLimit,
						WatchLimitMeetsTemporalTypeID
					)
					VALUES
					(
						@modelId, -- ModelID - int
						@modelActionItemPriorityTypeId, -- ModelActionItemPriorityTypeID - int
						@isDiagnose, -- IsDiagnose - bit
						@isWatch, -- IsWatch - bit
						@IsModelMaintenance, -- IsModelMaintenance - bit
						@noteText, -- NoteText - nvarchar
						@userId, -- CreatedByUserID - int
						GetDate(), -- CreateDate - datetime
						@userId, -- ChangedByUserID - int
						GetDate(), -- ChangeDate - datetime
						@watchDuration, -- WatchDuration - real
						@watchDurationTemporalTypeId, -- WatchDurationTemporalTypeID - int
						@watchCriteriaTypeId, -- WatchCriteriaTypeID - int
						@watchLimit, -- WatchLimit - real
						@watchLimitMeetsTemporalTypeId -- WatchLimitMeetsTemporalTypeID - int
					)
				END
			END
		');
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tAlertPriorityType_tAlertPriority_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tAlertPriority]'))
		ALTER TABLE [Monitoring].[tAlertPriority] DROP CONSTRAINT [tAlertPriorityType_tAlertPriority_FK1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromND_tAlertPriority_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tAlertPriority]'))
		ALTER TABLE [Monitoring].[tAlertPriority] DROP CONSTRAINT [tModel_AlertScreening_FromND_tAlertPriority_FK1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tAlertPriority]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tAlertPriority]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tAlertPriorityType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tAlertPriorityType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_tModel_AlertScreening_FromND_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromND]'))
		ALTER TABLE [Monitoring].[tModel_AlertScreening_FromND] DROP CONSTRAINT [tModel_AlertScreening_tModel_AlertScreening_FromND_FK1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromND]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModel_AlertScreening_FromND]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_tModel_AlertScreening_FromAsset360_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromAsset360]'))
		ALTER TABLE [Monitoring].[tModel_AlertScreening_FromAsset360] DROP CONSTRAINT [tModel_AlertScreening_tModel_AlertScreening_FromAsset360_FK1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening_FromAsset360]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModel_AlertScreening_FromAsset360]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModel_tModel_AlertScreening_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening]'))
		ALTER TABLE [Monitoring].[tModel_AlertScreening] DROP CONSTRAINT [tNDModel_tModel_AlertScreening_FK1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModel_AlertScreening]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModel_AlertScreening]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItemArchive]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModelActionItemArchive]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItemPriorityType_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tModelActionItemPriorityType_tModelActionItem_FK1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tNDModel_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tNDModel_tModelActionItem_FK1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchCriteriaType_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tWatchCriteriaType_tModelActionItem_FK1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchLimitMeetsTemporalType_tModelActionItem_FK1]') AND parent_object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]'))
		ALTER TABLE [Monitoring].[tModelActionItem] DROP CONSTRAINT [tWatchLimitMeetsTemporalType_tModelActionItem_FK1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItem]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModelActionItem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tModelActionItemPriorityType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tModelActionItemPriorityType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchCriteriaType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tWatchCriteriaType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Monitoring].[tWatchLimitMeetsTemporalType]') AND type in (N'U'))
		DROP TABLE [Monitoring].[tWatchLimitMeetsTemporalType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ArcFlash].[spGetLabelFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ArcFlash].[spGetLabelFormat]
GO

GRANT EXECUTE ON ArcFlash.spGetLabelFormat to [TEUser]
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('6.0.0.0-27','New ArcFlash stored proc to fetch ArcFlash label format for a given asset',
'FromVersion_6-0-0-0_Script27_ArcFlash_spGetLabelFormat.sql')
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ProcessData].[spInsertTags]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ProcessData].[spInsertTags]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ProcessData].[spUpdateTags]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ProcessData].[spUpdateTags]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ProcessData].[spDeleteTags]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ProcessData].[spDeleteTags]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'tpTag' AND ss.name = N'ProcessData')
DROP TYPE [ProcessData].[tpTag]
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('6.1.0.0-01','PD Tag table edit procedures.','FromVersion_6-1-0-0_Script01_TagTableEditProcedures.sql')
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('17.0.0.0-01','Release 17.0.0.0 Nd Model Config Sync Supporting Stored Procedures.','FromVersion_17-0-0-0_Script01_NdModelConfigSync.sql')
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('17.1.0.0-09','Get PDTag IDs','FromVersion_17-1-0-0_Script09_Get_PDTagIDs.sql')
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName) 
VALUES ('20.1.1.0-12','Simple Asset Events Schema','FromVersion_20-1-1-0_Script12_Simple_Asset_Events_Schema.sql')
GO

DROP INDEX IF EXISTS [IX_OpModeParamsID_Active_Includes]
ON 
	[Monitoring].[tNDModel]
GO

DROP INDEX IF EXISTS [IX_OpModeParamsID]
ON
	[Monitoring].[tNDOpModeParamsAssetClassTypeMap]
GO

DROP INDEX IF EXISTS [IX_OpModeParamsID]
ON
	[Monitoring].[tNDOpModeParamsAssetMap]
GO

INSERT INTO Base.tDBVersion (DBVersion,DBVersionDesc,ScriptFileName)
VALUES ('20.2.0.0-06','Add Indexes to OpModeParams.','FromVersion_20-2-0-0_Script06_OpModeParamsIndexes.sql')
GO

ALTER TABLE Monitoring.tNDModel ALTER COLUMN ModelDesc NVARCHAR(512);
GO

INSERT INTO Base.tDBVersion (DBVersion, DBVersionDesc, ScriptFileName)
VALUES ('23-3-0-0-01', 'Update tNDModel table to allow for longer model description and added necessary permission for patch function', 'FromVersion_23-3-0-0_Script01_Model_Patch_Alterations.sql');
GO
