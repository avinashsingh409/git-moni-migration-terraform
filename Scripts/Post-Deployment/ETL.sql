﻿MERGE INTO [ETL].[SmartDGMasterInputs_ModuleType] AS TARGET
USING (
VALUES
	(1, 'Standard')
) AS Source ([ModuleTypeID], [ModuleTypeDesc])
ON (Target.[ModuleTypeID] = Source.[ModuleTypeID])
WHEN MATCHED THEN 
	UPDATE Set  [ModuleTypeDesc] = Source.[ModuleTypeDesc]
WHEN NOT MATCHED BY TARGET THEN
	INSERT 	([ModuleTypeID], [ModuleTypeDesc])
	VALUES (Source.[ModuleTypeID],Source.[ModuleTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN 
	DELETE
;