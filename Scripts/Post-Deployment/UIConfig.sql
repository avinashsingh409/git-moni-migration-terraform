﻿

DECLARE @asset360AC int =1, @asset360DN NVARCHAR(255) = 'Asset 360';
DECLARE @solarPVAC int=2, @solarPVDN NVARCHAR(255) = 'Solar PV';
DECLARE @matsLiteAC int=3, @matsLiteDN  NVARCHAR(255) = 'MATS';
DECLARE @pgAnalystAC int=4, @pgAnalystDN  NVARCHAR(255) = 'Performance Analyst';
DECLARE @evrnDshbdAC int=5, @evrnDshbdDN  NVARCHAR(255) = 'Environmental Dashboard';
DECLARE @csaprAC int=6, @csaprDN  NVARCHAR(255) = 'CSAPR';
DECLARE @projPriorAC int=7, @projPriorDN  NVARCHAR(255) = 'Project Prioritization';
DECLARE @scenarioPlannerAC int=8, @scenarioPlannerDN  NVARCHAR(255) = 'Scenario Planner';
DECLARE @apPerfAnalystAC int=9, @apPerfAnalystDN  NVARCHAR(255) = 'Adaptive Planning Performance Analyst';
DECLARE @regAdminAC int=10, @regAdminDN  NVARCHAR(255) = 'Regulatory Admin';
DECLARE @scenarioMonitorAC int=11, @scenarioMonitorDN  NVARCHAR(255) = 'Scenario Monitor';
DECLARE @systemHealthAC int=12, @systemHealthDN  NVARCHAR(255) = 'System Health Monitor';
DECLARE @waterCHPAC int=13, @waterCHPDN  NVARCHAR(255) = 'Water CHP';
DECLARE @oldWaterMeterAC int=14, @oldWaterMeterDN  NVARCHAR(255) = 'Old Water Meter';
DECLARE @waterReclamationAC int=15, @waterReclamationDN  NVARCHAR(255) = 'Water Reclamation';
DECLARE @arcFlashAC int=16, @arcFlashDN  NVARCHAR(255) = 'Arc Flash';
DECLARE @issuesManagementAC int=17, @issuesManagementDN  NVARCHAR(255) = 'Issues Management';
DECLARE @waterTreatmentAC int=18, @waterTreatmentDN  NVARCHAR(255) = 'Water Treatment';
DECLARE @microgridAC int=19, @microgridDN  NVARCHAR(255) = 'Microgrid';
DECLARE @investmentAccelAC int=20, @investmentAccelDN  NVARCHAR(255) = 'Investment Accelerator';
DECLARE @userAdminAC int=21, @userAdminDN  NVARCHAR(255) = 'User Administration';
DECLARE @alertsAC int=22, @alertsDN  NVARCHAR(255) = 'Alerts';
DECLARE @solarAnalystAC int=23, @solarAnalystDN  NVARCHAR(255) = 'Performance Analyst';
DECLARE @waterAnalystAC int=24, @waterAnalystDN  NVARCHAR(255) = 'Performance Analyst';
DECLARE @issuesAnalystAC int=31, @issuesAnalystDN NVARCHAR(255) = 'Performance Analyst';
DECLARE @airPermitsAC int=32, @airPermitsDN NVARCHAR(255) = 'Air Permits'
DECLARE @sludgeAnalystAC int=33, @sludgeAnalystDN NVARCHAR(255) = 'Performance Analyst'
DECLARE @OptionsExplorerAC int=34, @OptionsExplorerDN NVARCHAR(255) = 'Options Explorer'
DECLARE @RiskMatrixAC int=35, @RiskMatrixDN NVARCHAR(255) = 'Risk Matrix'
DECLARE @ViewExplorerAC int=36, @ViewExplorerDN NVARCHAR(255) = 'View Explorer'
DECLARE @SmartESAC int=37, @SmartESDN NVARCHAR(255) = 'Smart ES'
DECLARE @waterMeterAC int=38, @waterMeterDN  NVARCHAR(255) = 'Water Meter';
DECLARE @smartDGAC int=39, @smartDGDN  NVARCHAR(255) = 'Smart DG';
DECLARE @CHPAnalystAC int=40, @CHPAnalystDN NVARCHAR(255) = 'Performance Analyst';
DECLARE @AssetExplorerAC int=41, @AssetExplorerDN  NVARCHAR(255) = 'Asset Explorer';
DECLARE @SekoiaAC int=42, @SekoiaDN  NVARCHAR(255) = 'Program Navigator';
DECLARE @ProcessDataUploadUtilityAC int=43, @ProcessDataUploadUtilityDN NVARCHAR(255) = 'Process Data Upload Utility';
DECLARE @DispatchAnalystAC int=44, @DispatchAnalystDN NVARCHAR(255) = 'Performance Analyst';
DECLARE @nDTestRigAC int=45, @nDTestRigDN NVARCHAR(255) = 'nD Test Rig';
DECLARE @WaterQualityPlatformAC int=46, @WaterQualityPlatformDN NVARCHAR(255) = 'Water Quality Platform';
DECLARE @VirtualTourAC INT=47, @VirtualTourDN NVARCHAR(255) = 'Virtual Tour';
DECLARE @MapAnalystAC int=48, @MapAnalystDN NVARCHAR(255) = 'Performance Analyst';
DECLARE @ReportAnalystAC int=49, @ReportAnalystDN NVARCHAR(255) = 'Performance Analyst';
DECLARE @CriteriaObjectUtilityAC int=50, @CriteriaObjectUtilityDN NVARCHAR(255) = 'Criteria Object Utility';
DECLARE @WorkManagementAC int=51, @WorkManagementDN NVARCHAR(255) = 'Work Management';
DECLARE @RiskAssessmentAC int=52, @RiskAssessmentDN NVARCHAR(255) = 'Risk Assessment';
DECLARE @WasteWaterAC int=53, @WasteWaterDN NVARCHAR(255) = 'WasteWater';
DECLARE @WasteWaterMonitorAC int=54, @WasteWaterMonitorDN NVARCHAR(255) = 'Monitor';
DECLARE @WasteWaterInformAC int=55, @WasteWaterInformDN NVARCHAR(255) = 'Resolve';
DECLARE @WasteWaterAssetDataManagementAC int=56, @WasteWaterAssetDataManagementDN NVARCHAR(255) = 'Organize';
DECLARE @WasteWaterAnalystAC int=57, @wasteWaterAnalystDN  NVARCHAR(255) = 'Analyze';
DECLARE @WasteWaterTrendsAC int=58, @WasteWaterTrendsDN NVARCHAR(255) = 'Explore';
DECLARE @WasteWaterScorecardsAC int=59, @WasteWaterScorecardsDN NVARCHAR(255) = 'Evaluate';
DECLARE @WasteWaterViewsAC int=60, @WasteWaterViewsDN NVARCHAR(255) = 'Visualize';
DECLARE @DataExplorerAC int = 61, @DataExplorerDN NVARCHAR(255) = 'Data Explorer';

--Security Resource variables
DECLARE @asset360SR int
DECLARE @solarPVSR int
DECLARE @matsLiteSR int
DECLARE @pgAnalystSR int
DECLARE @evrnDshbdSR int
DECLARE @csaprSR int
DECLARE @projPriorSR int
DECLARE @scenarioPlannerSR int
DECLARE @apPerfAnalystSR int
DECLARE @regAdminSR int
DECLARE @scenarioMonitorSR int
DECLARE @systemHealthSR int
DECLARE @waterCHPSR int
DECLARE @oldWaterMeterSR int
DECLARE @waterMeterSR int
DECLARE @waterReclamationSR int
DECLARE @arcFlashSR int
DECLARE @arcFlashEquipStateSR int
DECLARE @arcFlashImportSR int
DECLARE @dataSR int
DECLARE @issuesSR int
DECLARE @impactScenarioSR int
DECLARE @issuesManagementSR int
DECLARE @waterTreatmentSR int
DECLARE @microgridSR int
DECLARE @investmentAccelSR int
DECLARE @userAdminSR int
DECLARE @alertsSR int
DECLARE @solarAnalystSR int
DECLARE @waterAnalystSR int
DECLARE @issuesAnalystSR int
DECLARE @airPermitsSR int
DECLARE @sludgeAnalystSR int
DECLARE @OptionsExplorerSR int
DECLARE @RiskMatrixSR int
DECLARE @ViewExplorerSR int
DECLARE @SmartESSR int
DECLARE @SmartDGSR int
DECLARE @CHPAnalystSR int
DECLARE @AssetExplorerSR int
DECLARE @SekoiaSR int
DECLARE @ProcessDataUploadUtilitySR int
DECLARE @DispatchAnalystSR int
DECLARE @nDTestRigSR int
DECLARE @WaterQualityPlatformSR INT
DECLARE @VirtualTourSR INT
DECLARE @MapAnalystSR int
DECLARE @ReportAnalystSR int
DECLARE @CriteriaObjectUtilitySR int
DECLARE @workManagementSR int
DECLARE @RiskAssessmentSR int

DECLARE @RiskAssessmentCalcsSR INT
DECLARE @DataExplorerSR int
DECLARE @WasteWaterSR INT
DECLARE @WasteWaterMonitorSR INT
DECLARE @WasteWaterInformSR INT
DECLARE @WasteWaterAssetDataManagementSR INT
DECLARE @WasteWaterAnalystSR INT
DECLARE @WasteWaterTrendsSR INT
DECLARE @WasteWaterScorecardsSR INT
DECLARE @WasteWaterViewsSR INT
DECLARE @AssetExplorerInfoTabSR int
DECLARE @AssetExplorerAttachmentsTabSR int
DECLARE @AssetExplorerBlogTabSR int
DECLARE @AssetExplorerLocationTabSR int
DECLARE @AssetExplorerListTabSR int
DECLARE @AssetExplorerSearchTabSR int
DECLARE @IssuesManagementIssuesTabSR int
DECLARE @IssuesManagementScorecardTabSR int
DECLARE @WorkManagementIssuesTabSR int
DECLARE @WorkManagementScorecardTabSR int
DECLARE @InvestmentAcceleratorSummaryTabSR int
DECLARE @InvestmentAcceleratorTimelineTabSR int
DECLARE @InvestmentAcceleratorMapTabSR int
DECLARE @InvestmentAcceleratorTrendsTabSR int
DECLARE @InvestmentAcceleratorViewsTabSR int
DECLARE @InvestmentAcceleratorComparisonTabSR int 
--Waste Water
DECLARE @WasteWaterAssetDataManagementInfoTabSR int
DECLARE @WasteWaterAssetDataManagementAttachmentsTabSR int
DECLARE @WasteWaterAssetDataManagementBlogTabSR int
DECLARE @WasteWaterAssetDataManagementListTabSR int
DECLARE @WasteWaterScorecardsSummaryTabSR int
DECLARE @WasteWaterInformIssuesTabSR int

--Under the Data Node
DECLARE @AssetExplorerEditSR int
DECLARE @SekoiaScheduleStatusImportSR int
DECLARE @SupportLeadSR int

PRINT 'Finding Security Resources'
--Find SecurityResourceIDs for each AppContext
SELECT @asset360SR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Asset360');
SELECT @solarPVSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SolarPV');
SELECT @matsLiteSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'MATSLite');
SELECT @pgAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'PowerGenAnalyst');
SELECT @evrnDshbdSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'EvrnDashboard');
SELECT @csaprSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'CSAPR');
SELECT @projPriorSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ProjPrior');
SELECT @scenarioPlannerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ScenarioPlanner');
SELECT @apPerfAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'APPerfAnalyst');
SELECT @regAdminSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RegAdmin');
SELECT @scenarioMonitorSR= (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ScenarioMonitor');
SELECT @systemHealthSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SystemHealth');
SELECT @waterCHPSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterCHP');
SELECT @oldWaterMeterSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'OldWaterMeter');
SELECT @waterMeterSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterMeter');
SELECT @waterReclamationSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterReclamation');
SELECT @arcFlashSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ArcFlash');
SELECT @arcFlashEquipStateSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ArcFlashEquipStates');
SELECT @arcFlashImportSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ArcFlashImport');
SELECT @dataSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Data');
SELECT @issuesSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Issues');
SELECT @impactScenarioSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ImpactScenario');
SELECT @waterTreatmentSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterTreatment');
SELECT @issuesManagementSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesManagement');
SELECT @microgridSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Microgrid');
SELECT @investmentAccelSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAccelerator');
SELECT @userAdminSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'UserAdmin');
SELECT @alertsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Alerts');
SELECT @solarAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SolarAnalyst');
SELECT @waterAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterAnalyst');
SELECT @issuesAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesAnalyst');
SELECT @airPermitsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AirPermits');
SELECT @sludgeAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SludgeAnalyst');
SELECT @OptionsExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'OptionsExplorer');
SELECT @RiskMatrixSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RiskMatrix');
SELECT @ViewExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ViewExplorer');
SELECT @SmartESSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SmartES');
SELECT @SmartDGSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SmartDG');
SELECT @CHPAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'CHPAnalyst');
SELECT @AssetExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorer');
SELECT @SekoiaSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SEKOIA');
SELECT @ProcessDataUploadUtilitySR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ProcessDataUploadUtility');
SELECT @DispatchAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'DispatchAnalyst');
SELECT @nDTestRigSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'nDTestRig');
SELECT @WaterQualityPlatformSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterQualityPlatform');
SELECT @VirtualTourSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'VirtualTour');
SELECT @MapAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'MapAnalyst');
SELECT @ReportAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ReportAnalyst');
SELECT @CriteriaObjectUtilitySR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'CriteriaObjectUtility');
SELECT @WorkManagementSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WorkManagement');
SELECT @RiskAssessmentSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RiskAssessment');
SELECT @RiskAssessmentCalcsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RiskAssessmentCalcs');
SELECT @WasteWaterSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWater');
SELECT @WasteWaterMonitorSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterMonitor');
SELECT @WasteWaterInformSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterInform');
SELECT @WasteWaterAssetDataManagementSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagement');
SELECT @WasteWaterAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAnalyst');
SELECT @WasteWaterTrendsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterTrends');
SELECT @WasteWaterScorecardsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterScorecards');
SELECT @WasteWaterViewsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterViews');
SELECT @DataExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'DataExplorer');

--Under the Data Node
SELECT @AssetExplorerEditSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerEdit');
SELECT @SekoiaScheduleStatusImportSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SEKOIA-ScheduleStatusImport');
SELECT @SupportLeadSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Support Lead');

--Tabs Under the app context nodes
SELECT @AssetExplorerInfoTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerInfoTab');
SELECT @AssetExplorerAttachmentsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerAttachmentsTab');
SELECT @AssetExplorerBlogTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerBlogTab');
SELECT @AssetExplorerLocationTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerLocationTab');
SELECT @AssetExplorerListTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerListTab');
SELECT @AssetExplorerSearchTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerSearchTab');

SELECT @IssuesManagementIssuesTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesManagementIssuesTab');
SELECT @IssuesManagementScorecardTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesManagementScorecardTab');

SELECT @WorkManagementIssuesTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WorkManagementIssuesTab');
SELECT @WorkManagementScorecardTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WorkManagementScorecardTab');

SELECT @InvestmentAcceleratorSummaryTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorSummaryTab')
SELECT @InvestmentAcceleratorTimelineTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorTimelineTab')
SELECT @InvestmentAcceleratorMapTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorMapTab')
SELECT @InvestmentAcceleratorTrendsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorTrendsTab')
SELECT @InvestmentAcceleratorViewsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorViewsTab')
SELECT @InvestmentAcceleratorComparisonTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorComparisonTab')

--Tabs Under the Waste Water
SELECT @WasteWaterAssetDataManagementInfoTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementInfoTab');
SELECT @WasteWaterAssetDataManagementAttachmentsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementAttachmentsTab');
SELECT @WasteWaterAssetDataManagementBlogTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementBlogTab');
SELECT @WasteWaterAssetDataManagementListTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementListTab');

SELECT @WasteWaterScorecardsSummaryTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterScorecardsSummaryTab')

SELECT @WasteWaterInformIssuesTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterInformIssuesTab');

PRINT 'Initial SR selection complete; checking for nulls'

--If SecurityResourceID is missing then add the resource to tResource
DECLARE @appContextNode int
SELECT @appContextNode = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AppContext');
IF @appContextNode IS NULL
BEGIN
  PRINT 'The Resource node for AppContext does not exist, cannot complete script'
END

IF @asset360SR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'Asset360'
	SELECT @asset360SR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Asset360');
END

IF @solarPVSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SolarPV'
	SELECT @solarPVSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SolarPV');
END

IF @matsLiteSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'MATSLite'
	SELECT @matsLiteSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'MATSLite');
END

IF @pgAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'PowerGenAnalyst'
	SELECT @pgAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'PowerGenAnalyst');
END

IF @evrnDshbdSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'EvrnDashboard'
	SELECT @evrnDshbdSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'EvrnDashboard');
END

IF @csaprSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'CSAPR'
	SELECT @csaprSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'CSAPR');
END

IF @projPriorSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ProjPrior'
	SELECT @projPriorSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ProjPrior');
END

IF @scenarioPlannerSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ScenarioPlanner'
	SELECT @scenarioPlannerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ScenarioPlanner');
END

IF @apPerfAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'APPerfAnalyst'
	SELECT @apPerfAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'APPerfAnalyst');
END

IF @regAdminSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'RegAdmin'
	SELECT @regAdminSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RegAdmin');
END

IF @scenarioMonitorSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ScenarioMonitor'
	SELECT @scenarioMonitorSR= (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ScenarioMonitor');
END

IF @systemHealthSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SystemHealth'
	SELECT @systemHealthSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SystemHealth');
END

IF @waterCHPSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WaterCHP'
	SELECT @waterCHPSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterCHP');
END

IF @waterMeterSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WaterMeter'
	SELECT @waterMeterSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterMeter');
END

IF @oldWaterMeterSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'OldWaterMeter'
	SELECT @oldWaterMeterSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'OldWaterMeter');
END

IF @waterReclamationSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WaterReclamation'
	SELECT @waterReclamationSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterReclamation');
END

IF @arcFlashSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ArcFlash'
	SELECT @arcFlashSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ArcFlash');
END

IF @arcFlashEquipStateSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @arcFlashSR, 'ArcFlashEquipStates'
	SELECT @arcFlashEquipStateSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ArcFlashEquipStates');
END

IF @arcFlashImportSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @arcFlashSR, 'ArcFlashImport'
	SELECT @arcFlashImportSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ArcFlashImport');
END

IF @issuesManagementSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'IssuesManagement'
	SELECT @issuesManagementSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesManagement');
END

IF @workManagementSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WorkManagement'
	SELECT @workManagementSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WorkManagement');
END

IF @dataSR IS NOT NULL
BEGIN
    IF @issuesSR is null
    BEGIN
  	    EXEC AccessControl.spInsertResource @dataSR, 'Issues'
	    SELECT @issuesSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Issues');
	END	
	
	IF @impactScenarioSR is null	
	BEGIN
		EXEC AccessControl.spInsertResource @issuesSR, 'ImpactScenario'
	    SELECT @impactScenarioSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ImpactScenario');
	END	
END

IF @waterTreatmentSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WaterTreatment'
	SELECT @waterTreatmentSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterTreatment');
END

IF @microgridSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'Microgrid'
	SELECT @microgridSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Microgrid');
END

IF @investmentAccelSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'InvestmentAccelerator'
	SELECT @investmentAccelSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAccelerator');
END

IF @userAdminSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'UserAdmin'
	SELECT @userAdminSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'UserAdmin');
END

IF @alertsSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'Alerts'
	SELECT @alertsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Alerts');
END

IF @solarAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SolarAnalyst'
	SELECT @solarAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SolarAnalyst');
END

IF @waterAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WaterAnalyst'
	SELECT @waterAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterAnalyst');
END
IF @issuesAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'IssuesAnalyst'
	SELECT @issuesAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesAnalyst');
END
IF @airPermitsSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'AirPermits'
	SELECT @airPermitsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AirPermits');
END
IF @sludgeAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SludgeAnalyst'
	SELECT @sludgeAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SludgeAnalyst');
END
IF @OptionsExplorerSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'OptionsExplorer'
	SELECT @OptionsExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'OptionsExplorer');
END
IF @RiskMatrixSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'RiskMatrix'
	SELECT @RiskMatrixSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RiskMatrix');
END

IF @ViewExplorerSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ViewExplorer'
	SELECT @ViewExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ViewExplorer');
END

IF @SmartESSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SmartES'
	SELECT @SmartESSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SmartES');
END

IF @SmartDGSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SmartDG'
	SELECT @SmartDGSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SmartDG');
END

IF @CHPAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'CHPAnalyst'
	SELECT @CHPAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'CHPAnalyst');
END

IF @AssetExplorerSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'AssetExplorer'
	SELECT @AssetExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorer');
END

IF @SekoiaSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'SEKOIA'
	SELECT @SekoiaSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SEKOIA');
END

IF @ProcessDataUploadUtilitySR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ProcessDataUploadUtility'
	SELECT @ProcessDataUploadUtilitySR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ProcessDataUploadUtility');
END

IF @DispatchAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'DispatchAnalyst'
	SELECT @DispatchAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'DispatchAnalyst');
END

IF @MapAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'MapAnalyst'
	SELECT @MapAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'MapAnalyst');
END

IF @ReportAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'ReportAnalyst'
	SELECT @ReportAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'ReportAnalyst');
END

IF @nDTestRigSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'nDTestRig'
	SELECT @nDTestRigSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'nDTestRig');
END

IF @WaterQualityPlatformSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WaterQualityPlatform'
	SELECT @WaterQualityPlatformSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WaterQualityPlatform');
END

IF @VirtualTourSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'VirtualTour'
	SELECT @VirtualTourSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'VirtualTour');
END

IF @CriteriaObjectUtilitySR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'CriteriaObjectUtility'
	SELECT @CriteriaObjectUtilitySR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'CriteriaObjectUtility');
END

--If SecurityResourceID is missing then add the resource to tResource
DECLARE @dataNode int
SELECT @dataNode = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Data');
IF @dataNode IS NULL
BEGIN
  --THROW AN ERROR HERE AND STOP
  RAISERROR ('The Resource node for Data does not exist, cannot complete script',10,1);
END

IF @AssetExplorerEditSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @dataNode, 'AssetExplorerEdit'
	SELECT @AssetExplorerEditSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerEdit');
END

IF @SekoiaScheduleStatusImportSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @dataNode, 'Sekoia-ScheduleStatusImport'
	SELECT @SekoiaScheduleStatusImportSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'SEKOIA-ScheduleStatusImport');
END

IF @SupportLeadSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @dataNode, 'Support Lead'
	SELECT @SupportLeadSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'Support Lead');
END

IF @RiskAssessmentSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'RiskAssessment'
	SELECT @RiskAssessmentSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RiskAssessment');
END
IF @RiskAssessmentCalcsSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @RiskAssessmentSR, 'RiskAssessmentCalcs'
	SELECT @RiskAssessmentCalcsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'RiskAssessmentCalcs');
END

IF @DataExplorerSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'DataExplorer'
	SELECT @DataExplorerSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'DataExplorer');
END

IF @WasteWaterSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWater'
	SELECT @WasteWaterSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWater');
END

IF @WasteWaterMonitorSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterMonitor'
	SELECT @WasteWaterMonitorSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterMonitor');
END

IF @WasteWaterInformSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterInform'
	SELECT @WasteWaterInformSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterInform');
END

IF @WasteWaterAssetDataManagementSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterAssetDataManagement'
	SELECT @WasteWaterAssetDataManagementSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagement');
END

IF @WasteWaterAnalystSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterAnalyst'
	SELECT @WasteWaterAnalystSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAnalyst');
END

IF @WasteWaterTrendsSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterTrends'
	SELECT @WasteWaterTrendsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterTrends');
END

IF @WasteWaterScorecardsSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterScorecards'
	SELECT @WasteWaterScorecardsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterScorecards');
END

IF @WasteWaterViewsSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @appContextNode, 'WasteWaterViews'
	SELECT @WasteWaterViewsSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterViews');
END

--Tabs Under the app context nodes

IF @AssetExplorerInfoTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @assetExplorerSR, 'AssetExplorerInfoTab'
	SELECT @AssetExplorerInfoTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerInfoTab');
END

IF @AssetExplorerAttachmentsTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @assetExplorerSR, 'AssetExplorerAttachmentsTab'
	SELECT @AssetExplorerAttachmentsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerAttachmentsTab');
END

IF @AssetExplorerBlogTabSR IS NULL
BEGIN 
	EXEC AccessControl.spInsertResource @assetExplorerSR, 'AssetExplorerBlogTab'
	SELECT @AssetExplorerBlogTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerBlogTab');
END

IF @AssetExplorerLocationTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @assetExplorerSR, 'AssetExplorerLocationTab'
	SELECT @AssetExplorerLocationTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerLocationTab');
END

IF @AssetExplorerListTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @assetExplorerSR, 'AssetExplorerListTab'
	SELECT @AssetExplorerListTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerListTab');
END

IF @AssetExplorerSearchTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @assetExplorerSR, 'AssetExplorerSearchTab'
	SELECT @AssetExplorerSearchTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'AssetExplorerSearchTab');
END


IF @IssuesManagementIssuesTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @issuesManagementSR, 'IssuesManagementIssuesTab'
	SELECT @IssuesManagementIssuesTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesManagementIssuesTab');
END

IF @IssuesManagementScorecardTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @issuesManagementSR, 'IssuesManagementScorecardTab'
	SELECT @IssuesManagementScorecardTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'IssuesManagementScorecardTab');
END

IF @WorkManagementIssuesTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @workManagementSR, 'WorkManagementIssuesTab'
	SELECT @WorkManagementIssuesTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WorkManagementIssuesTab');
END

IF @WorkManagementScorecardTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @workManagementSR, 'WorkManagementScorecardTab'
	SELECT @WorkManagementScorecardTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WorkManagementScorecardTab');
END

IF @InvestmentAcceleratorSummaryTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @investmentAccelSR, 'InvestmentAcceleratorSummaryTab'
	SELECT @InvestmentAcceleratorSummaryTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorSummaryTab');
END 

IF @InvestmentAcceleratorTimelineTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @investmentAccelSR, 'InvestmentAcceleratorTimelineTab'
	SELECT @InvestmentAcceleratorTimelineTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorTimelineTab');
END 

IF @InvestmentAcceleratorMapTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @investmentAccelSR, 'InvestmentAcceleratorMapTab'
	SELECT @InvestmentAcceleratorMapTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorMapTab');
END 

IF @InvestmentAcceleratorTrendsTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @investmentAccelSR, 'InvestmentAcceleratorTrendsTab'
	SELECT @InvestmentAcceleratorTrendsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorTrendsTab');
END 

IF @InvestmentAcceleratorViewsTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @investmentAccelSR, 'InvestmentAcceleratorViewsTab'
	SELECT @InvestmentAcceleratorViewsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorViewsTab');
END 

IF @InvestmentAcceleratorComparisonTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @investmentAccelSR, 'InvestmentAcceleratorComparisonTab'
	SELECT @InvestmentAcceleratorComparisonTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'InvestmentAcceleratorComparisonTab');
END 

--Tabs Under the Waste Water

IF @WasteWaterAssetDataManagementInfoTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @WasteWaterAssetDataManagementSR, 'WasteWaterAssetDataManagementInfoTab'
	SELECT @WasteWaterAssetDataManagementInfoTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementInfoTab');
END

IF @WasteWaterAssetDataManagementAttachmentsTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @WasteWaterAssetDataManagementSR, 'WasteWaterAssetDataManagementAttachmentsTab'
	SELECT @WasteWaterAssetDataManagementAttachmentsTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementAttachmentsTab');
END

IF @WasteWaterAssetDataManagementBlogTabSR IS NULL
BEGIN 
	EXEC AccessControl.spInsertResource @WasteWaterAssetDataManagementSR, 'WasteWaterAssetDataManagementBlogTab'
	SELECT @WasteWaterAssetDataManagementBlogTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementBlogTab');
END

IF @WasteWaterAssetDataManagementListTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @WasteWaterAssetDataManagementSR, 'WasteWaterAssetDataManagementListTab'
	SELECT @WasteWaterAssetDataManagementListTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterAssetDataManagementListTab');
END

IF @WasteWaterScorecardsSummaryTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @WasteWaterScorecardsSR, 'WasteWaterScorecardsSummaryTab'
	SELECT @WasteWaterScorecardsSummaryTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterScorecardsSummaryTab');
END 

IF @WasteWaterInformIssuesTabSR IS NULL
BEGIN
	EXEC AccessControl.spInsertResource @WasteWaterInformSR, 'WasteWaterInformIssuesTab'
	SELECT @WasteWaterInformIssuesTabSR = (SELECT TOP 1 SecurityResourceID FROM AccessControl.tResource WHERE [Text] = 'WasteWaterInformIssuesTab');
END

PRINT 'Null SR check complete; disabling foreign keys for merging App Contexts'

--Disable foreign keys before merge
ALTER TABLE [Projection].[tScenarioObjectiveTypeAppContextMap] NOCHECK CONSTRAINT [FK_tScenarioObjectiveTypeAppContextMap_tAppContext]
ALTER TABLE [UIConfig].[tAssetAssetViewMap] NOCHECK CONSTRAINT [FK_tAssetAssetViewMap_tAssetView]
ALTER TABLE [Projection].[tScenarioObjectiveTypeAppContextMap] NOCHECK CONSTRAINT [FK_tScenarioObjectiveTypeAppContextMap_tAppContext]

PRINT 'Populating UIConfig.tAppContextDisplayName';
MERGE INTO UIConfig.tAppContextDisplayName AS TARGET USING (VALUES

('Adaptive Planning Performance Analyst',0,1,1),
('Air Permits',0,1,1),
('Alerts',1,1,1),
('Analyze',0,1,1),
('Arc Flash',0,1,1),
('Asset 360',0,1,1),
('Asset Explorer',0,1,1),
('Criteria Object Utility',0,1,1),
('CSAPR',0,1,1),
('Data Explorer',0,1,1),
('Environmental Dashboard',0,1,1),
('Evaluate',0,0,1),
('Explore',0,1,1),
('Investment Accelerator',0,0,1),
('Issues Management',0,1,1),
('MATS',0,1,1),
('Microgrid',0,1,1),
('Monitor',0,1,1),
('nD Test Rig',0,1,1),
('Old Water Meter',0,1,1),
('Options Explorer',0,1,1),
('Organize',0,1,1),
('Performance Analyst',0,1,1),
('Process Data Upload Utility',0,1,1),
('Project Prioritization',0,1,1),
('Regulatory Admin',0,1,1),
('Resolve',0,1,1),
('Risk Assessment',1,0,1),
('Risk Matrix',0,1,1),
('Scenario Monitor',0,1,1),
('Scenario Planner',0,1,1),
('Program Navigator',1,0,1),
('Smart DG',0,1,1),
('Smart ES',0,1,1),
('Solar PV',0,1,1),
('System Health Monitor',0,1,1),
('User Administration',0,1,1),
('View Explorer',1,1,1),
('Visualize',1,1,1),
('Virtual Tour',0,1,1),
('WasteWater',0,1,1),
('Water CHP',0,1,1),
('Water Meter',0,1,1),
('Water Quality Platform',1,0,0),
('Water Reclamation',0,1,1),
('Water Treatment',0,1,1),
('Work Management',0,1,1)
)

AS SOURCE (Name,AdHocNavready,AllowAllApps,IncludePhysicalTree)
ON Target.Name = Source.Name
WHEN MATCHED THEN UPDATE SET AdHocNavReady = Source.AdHocNavReady, AllowAllApps = Source.AllowAllApps, IncludePhysicalTree = Source.IncludePhysicalTree
WHEN NOT MATCHED BY TARGET THEN
INSERT (Name,AdHocNavReady,AllowAllApps,IncludePhysicalTree)
VALUES (Name,AdHocNavReady,AllowAllApps,IncludePhysicalTree)
WHEN NOT MATCHED BY SOURCE THEN DELETE;

PRINT 'Merging into UIConfig.tAppContext'
SET IDENTITY_INSERT [UIConfig].[tAppContext] ON
MERGE INTO UIConfig.tAppContext AS TARGET USING (VALUES 
 (@asset360AC,'Asset360','SIIIcon.png',@asset360DN,												 1,@asset360SR,'Asset360',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@solarPVAC,'SolarPV','SolarIcon.png',@solarPVDN,												 2,@solarPVSR,'Asset360',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@matsLiteAC,'MATSLite','MATSIcon.png',@matsLiteDN,											 3,@matsLiteSR,'Asset360',0,4,1,'CurrentYear','OneMonthBackTwoMonthsForward',NULL,NULL,NULL)
,(@pgAnalystAC,'PGAnalyst','PGAnalyst.png',@pgAnalystDN,										 4,@pgAnalystSR,'Asset360',0,9,1,'OneYear','TwoWeeksBack',NULL,NULL,300)
,(@evrnDshbdAC,'EvrnDshbd','EVRNIcon.png',@evrnDshbdDN,											 8,@evrnDshbdSR,'Asset360',0,4,0,NULL,NULL,NULL,NULL,NULL)
,(@csaprAC,'CSAPR','CSAPRIcon.png',@csaprDN,													 5,@csaprSR,'Asset360',0,4,1,'CurrentYear','CurrentYear',NULL,NULL,NULL)
,(@projPriorAC,'ProjPrior','ProjPriorIcon.png',@projPriorDN,									 7,@projPriorSR,'Asset360',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@scenarioPlannerAC,'ScenarioPlanner','ScenPlanIcon.png',@scenarioPlannerDN,					12,@scenarioPlannerSR,'ScenarioPlanner/Index.html',1,9,0,NULL,NULL,NULL,NULL,NULL)
,(@apPerfAnalystAC,'APPerfAnalyst','APPerfAnalyst.png',@apPerfAnalystDN,						 9,@apPerfAnalystSR,'Asset360',0,9,1,'CurrentYear','NextMonth',NULL,NULL,NULL)
,(@regAdminAC,'RegAdmin','RegAdminIcon.png',@regAdminDN,										11,@regAdminSR,'RegulatoryAdmin/Index.html',1,4,0,NULL,NULL,NULL,NULL,NULL)
,(@scenarioMonitorAC,'ScenarioMonitor','ScenarioMonitorIcon.png',@scenarioMonitorDN,			21,@scenarioMonitorSR,'Utilities/ScenarioMonitor.html',1,9,0,NULL,NULL,NULL,NULL,NULL)
,(@systemHealthAC,'SystemHealth','HealthMonitorIcon.png',@systemHealthDN,						23,@systemHealthSR,'Utilities/SystemHealthMonitor.html',1,9,0,NULL,NULL,NULL,NULL,NULL)
,(@waterCHPAC,'WaterCHP','WaterCHPIcon.png',@waterCHPDN,										13,@waterCHPSR,'Asset360/#!/chpSummary?asset=17267&start=1403413199000&end=1408683599000&ac=13',0,9,0,':2014-01-01~2014-07-22',':2014-05-22~2014-07-22','en-UK',NULL,NULL)
,(@oldWaterMeterAC,'OldWaterMeter','WaterMeterIcon.png',@oldWaterMeterDN,						14,@oldWaterMeterSR,'Asset360',0,9,0,':2008-01-01~2016-02-29',':2014-12-01~2016-02-29',NULL,NULL,NULL)
,(@waterMeterAC,'WaterMeter','WaterMeterIcon.png',@waterMeterDN,								14,@waterMeterSR,'Asset360',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@waterReclamationAC,'WaterReclamation','WasteWaterFlowAdvisorIcon.png',@waterReclamationDN,	15,@waterReclamationSR,'Asset360',0,9,0,':2013-06-01~2014-05-30',':2013-06-01~2014-05-30',NULL,NULL,NULL)
,(@arcFlashAC,'ArcFlash','ArcFlashIcon.png',@arcFlashDN,										17,@arcFlashSR,'ArcFlash/Index.html',1,9,0,NULL,NULL,NULL,NULL,NULL)
,(@issuesManagementAC,'IssuesManagement','IssuesManagementIcon.png',@issuesManagementDN,		18,@issuesManagementSR,'IssuesManagement/Index.html',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@waterTreatmentAC,'WaterTreatment','SupplyWaterFlowAdvisorIcon.png',@waterTreatmentDN,		16,@waterTreatmentSR,'Asset360',0,9,0,':2013-01-01~2013-11-31',':2013-01-01~2013-11-31',NULL,NULL,NULL)
,(@microgridAC,'Microgrid','MicrogridIcon.png',@microgridDN,									19,@microgridSR,'Asset360/#!/microgridBVIP?asset=17034&start=1446597931000&end=1447202731000&ac=19',0,9,0,'LastThreeMonths','LastThreeMonths',NULL,NULL,3600)
,(@investmentAccelAC,'InvestmentAccel','InvestmentAccelIcon.png',@investmentAccelDN,			20,@investmentAccelSR,'Sekoia/index.html#!?ac=Investment%20Accelerator',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@userAdminAC,'UserAdmin','UserAdminIcon.png',@userAdminDN,									24,@userAdminSR,'UserAdmin/Index.html',1,9,0,NULL,NULL,NULL,NULL,NULL)
,(@alertsAC,'Alerts','AlertsIcon.png',@alertsDN,												22,@alertsSR,'Alerts/#!/main',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@solarAnalystAC,'SolarAnalyst','PGAnalyst.png',@solarAnalystDN,								 4,@solarAnalystSR,'Asset360',0,9,1,'OneYear','TwoWeeksBack',NULL,NULL,300)
,(@waterAnalystAC,'WaterAnalyst','PGAnalyst.png',@waterAnalystDN,								 4,@waterAnalystSR,'Asset360',0,9,1,'OneYear','TwoWeeksBack',NULL,NULL,300)
,(@issuesAnalystAC,'IssuesAnalyst','PGAnalyst.png',@issuesAnalystDN,							 4,@issuesAnalystSR,'Asset360',0,9,1,'OneYear','TwoWeeksBack',NULL,NULL,300)
,(@airPermitsAC,'AirPermits','AirPermitIcon.png',@airPermitsDN,									 6,@airPermitsSR,'Asset360',0,4,1,'CurrentYear','OneMonthBackTwoMonthsForward',NULL,NULL,NULL)
,(@sludgeAnalystAC,'SludgeAnalyst','PGAnalyst.png',@sludgeAnalystDN,							 4,@sludgeAnalystSR,'Asset360',0,9,1,'OneYear','TwoWeeksBack',NULL,NULL,300)
,(@OptionsExplorerAC,'OptionsExplorer','OptionsExplorer.png',@OptionsExplorerDN,				10,@OptionsExplorerSR,'OptionsExplorer/Index.html',0,4,0,null,null,NULL,NULL,NULL)
,(@RiskMatrixAC,'RiskMatrixMRV','RiskMatrix.png',@RiskMatrixDN,									18,@RiskMatrixSR,'Asset360',0,9,1,null,null,NULL,NULL,NULL)
,(@ViewExplorerAC,			'ViewExplorer',				'BI.png'			,@ViewExplorerDN,								 6,@ViewExplorerSR,'ViewExplorer/Index.html',0,9,0,NULL,NULL,NULL,NULL,300)
,(@SmartESAC,		'SmartES',			'SmartESIcon2.png'			,@SmartESDN,				25,@SmartESSR,'EnergyStorage/Index.html',1,9,0,NULL,NULL,NULL,NULL,NULL)
,(@SmartDGAC,		'SmartDG',			'SmartDGIcon.png'			,@SmartDGDN,				26,@SmartDGSR,'SmartDG/Index.html',1,4,0,NULL,NULL,NULL,NULL,NULL)
,(@CHPAnalystAC,'CHPAnalyst','PGAnalyst.png',@CHPAnalystDN,								         4,@CHPAnalystSR,'Asset360',0,9,1,'NULL','NULL',NULL,NULL,300)
,(@AssetExplorerAC,	'AssetExplorer',		'AssetExplorerIcon.png'		,@AssetExplorerDN,		27,@AssetExplorerSR,'AssetExplorer/Index.html',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@SekoiaAC,	'Program Navigator',		'sekoia-icon.png'		,@SekoiaDN,								28,@SekoiaSR,'Sekoia/index.html#!?ac=Program%20Navigator',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@ProcessDataUploadUtilityAC,'ProcessDataUploadUtility','DataUploadUtility.png'	,@ProcessDataUploadUtilityDN,29,@ProcessDataUploadUtilitySR,'Utilities/DataUpload.html',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@DispatchAnalystAC,'DispatchAnalyst','PGAnalyst.png',@DispatchAnalystDN,								         4,@DispatchAnalystSR,'Asset360',0,9,1,NULL,NULL,NULL,NULL,300)
,(@ndTestRigAC,'nDTestRig','BI.png',@ndTestRigDN,								         4,@ndTestRigSR,'ChartTest/NDTester/NDVisualtester.html',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@WaterQualityPlatformAC, 'WaterQualityPlatform', 'WaterQualityPlatform.png', @WaterQualityPlatformDN,	 30,@WaterQualityPlatformSR,'Sekoia/index.html#!/map?ac=Water%20Quality%20Platform',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@VirtualTourAC, 'VirtualTour','VirtualTour.png',@VirtualTourDN,								31,@VirtualTourSR,'Asset360',0,9,1,'OneYear','TwoWeeksBack',NULL,NULL,NULL)
,(@MapAnalystAC,'MapAnalyst','PGAnalyst.png',@MapAnalystDN,								         4,@MapAnalystSR,'Asset360',0,9,1,NULL,NULL,NULL,NULL,300)
,(@ReportAnalystAC,'ReportAnalyst','PGAnalyst.png',@ReportAnalystDN,								         4,@ReportAnalystSR,'Asset360',0,9,1,NULL,NULL,NULL,NULL,300)
,(@CriteriaObjectUtilityAC,'CriteriaObjectUtility','BI.png',@CriteriaObjectUtilityDN,			 32,@CriteriaObjectUtilitySR,'Utilities/AssetQueryUtility.html',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@workManagementAC,'WorkManagement','WorkManagementIcon.png',@workManagementDN,		33,@WorkManagementSR,'IssuesManagement/Index.html#!?ac=Work%20Management',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@RiskAssessmentAC,'RiskAssessment','RiskAssessment.png',@RiskAssessmentDN,		34,@RiskAssessmentSR,'RiskAssessment/index.html',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterAC,'WasteWater','WasteWaterFlowAdvisorIcon.png',@WasteWaterDN,						35,@WasteWaterSR,'WasteWater/index.html',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterMonitorAC,'WasteWaterMonitor','AlertsIcon.png',@WasteWaterMonitorDN,				36,@WasteWaterMonitorSR,'Alerts/Index.html#!?ac=Monitor',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterInformAC,'WasteWaterInform','WorkManagementIcon.png',@WasteWaterInformDN,			37,@WasteWaterInformSR,'IssuesManagement/Index.html#!?ac=Resolve',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterAssetDataManagementAC,	'WasteWaterAssetDataManagement',		'AssetExplorerIcon.png'		,@WasteWaterAssetDataManagementDN,		38,@WasteWaterAssetDataManagementSR,'AssetExplorer/Index.html#!?ac=Organize',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterAnalystAC,'WasteWaterAnalyst','PGAnalyst.png',@WasteWaterAnalystDN,				39,@WasteWaterAnalystSR,'Asset360',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterTrendsAC,	'WasteWaterTrends',		'DataExplorer.png'		,@WasteWaterTrendsDN,	40,@WasteWaterTrendsSR,'ChartEditor/Index.html#!?ac=Explore',0,9,0,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterScorecardsAC,'WasteWaterScorecards','InvestmentAccelIcon.png',@WasteWaterScorecardsDN,			41,@WasteWaterScorecardsSR,'Sekoia/index.html#!?ac=Evaluate',0,9,1,NULL,NULL,NULL,NULL,NULL)
,(@WasteWaterViewsAC,			'WasteWaterViews',				'BI.png'			,@WasteWaterViewsDN,    42,@WasteWaterViewsSR,'ViewExplorer/Index.html#!?ac=Visualize',0,9,0,NULL,NULL,NULL,NULL,300)
,(@DataExplorerAC,'DataExplorer','DataExplorer.png',@DataExplorerDN,							43,@DataExplorerSR,'ChartEditor/index.html',0,9,1,NULL,NULL,NULL,NULL,NULL)
)
AS SOURCE([AppContextID],[Name],[Icon],[DisplayName],[DisplayOrder],[SecurityResourceID],[Path],[OpenInNew],[StopAtLevel],[ShowFuture],[TimeRange],[TimeSelection],[Locale],[StartAsset],[Refresh])
ON Target.[AppContextID]= Source.[AppContextID]
WHEN MATCHED THEN UPDATE 
	SET [Name]=Source.[Name],[Icon]=Source.[Icon],[DisplayName]=Source.[DisplayName],
		[DisplayOrder]=Source.[DisplayOrder],[SecurityResourceID]=Source.[SecurityResourceID],
		[Path]=Source.[Path],[OpenInNew]=Source.[OpenInNew],[StopAtLevel]=Source.[StopAtLevel],
		[ShowFuture]=Source.[ShowFuture],
		[TimeRange]=Source.[TimeRange],
		[TimeSelection]=Source.[TimeSelection],
		[Locale]=Source.[Locale],
		[StartAsset]=Source.[StartAsset],
		[Refresh]=Source.[Refresh]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AppContextID],[Name],[Icon],[DisplayName],[DisplayOrder],[SecurityResourceID],[Path],[OpenInNew],[StopAtLevel],[ShowFuture],[TimeRange],[TimeSelection],[Locale],[StartAsset],[Refresh])
VALUES ([AppContextID],[Name],[Icon],[DisplayName],[DisplayOrder],[SecurityResourceID],[Path],[OpenInNew],[StopAtLevel],[ShowFuture],[TimeRange],[TimeSelection],[Locale],[StartAsset],[Refresh])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [UIConfig].[tAppContext] OFF

PRINT 'Merging into UIConfig.[tAssetView]'
--Sync [UIConfig].[tAssetView]
SET IDENTITY_INSERT [UIConfig].[tAssetView] ON
MERGE INTO [UIConfig].[tAssetView] AS TARGET USING (VALUES 
(1,'Asset Status','generic',1,null,null,null,null)
,(2,'Unit','unit',3,null,null,null,null)
,(3,'Map','solarplant',3,null,null,null,null)
,(4,'Map','fleet',2,null,null,null,null)
,(5,'Chart Gallery','chartgallery',100,null,null,null,null)
,(6,'Issues','issues',99,null,null,null,null)
,(7,'Solar PV Current Production','solarCurrentProduction',4,null,null,null,null)
,(8,'Forecast','weather',5,null,1,'Plus5Minus14',null)
,(9,'Performance Summary','solarPerfSummary',6,null,null,null,null)
,(10,'Current Trends','matsTab1',4,null,null,null,null)
,(11,'Scenario View','matsTab2',5,null,null,null,null)
,(12,'Current Trends','matsTab1Fleet',4,null,null,null,null)
,(13,'Availability','solarAvailability',7,null,null,null,null)
,(14,'Map','waterConsumptionMap',2,null,null,null,null)
,(15,'Chart','waterConsumptionChart',3,null,null,null,null)
,(16,'Data Viewer','solarChart',10,null,null,null,null)
,(17,'Scenario View','matsTab2Fleet',5,null,null,null,null)
,(18,'Summary','performanceAnalyst',80,null,null,null,null)
,(19,'Alerts','performanceAnalystAlerts',81,null,null,null,null)
,(20,'Issues','performanceAnalystIssues',82,null,null,null,null)
,(21,'Environmental Dashboard','evrnDshbrd',6,null,null,null,null)
,(22,'Current Trends','csaprTab1',4,null,null,null,null)
,(23,'Scenario View','csaprTab2',5,null,null,null,null)
,(24,'Revenue Performance','chpRevenue',3,null,null,null,null)
,(25,'Process Performance','chpProcess',4,null,null,null,null)
,(26,'Schedule Impact','projPrior',2,null,null,null,null)
,(27,'AP Analyst','apAnalyst',5,null,null,null,null)
,(28,'Map','waterConsumptionMap',3,null,null,null,null)
,(29,'Chart','waterConsumptionChart',4,null,null,null,null)
,(30,'Anomalies','waterConsumptionAnomaly',5,null,null,null,null)
,(31,'WWTP Flow Advisor','wwfaTab1',2,null,null,null,null)
,(32,'WTP Flow Advisor','wwfaTab2',2,null,null,null,null)
,(33,'Summary','microgridBVIP',2,null,null,null,null)
,(34,'Summary','chpperformanceAnalystSummary',2,null,null,null,null)
,(35,'Summary','iaSummary',1,null,null,null,null)
,(36,'Timeline','iaTimeline',2,null,null,null,null)
,(37,'Map','iaMap',3,null,null,null,null)
,(38,'Summary','waterConsumptionSummary', 2,null,null,null,null)
,(39,'Summary','performanceAnalystSummary', 2,null,null,null,null)
,(40,'Current Trends','airPermitsTab1',4,null,null,null,null)
,(41,'Scenario View','airPermitsTab2',5,null,null,null,null)
,(42,'Current Trends','airPermitsTab1Fleet',4,null,null,null,null)
,(43,'Summary','sludgeAnalystSummary',90,null,null,null,null)
,(44,'Summary','performanceAnalystLosses',3,null,null,null,null)
,(45,'Risk Matrix','riskMatrixMRV',99,null,null,null,null)
,(46,'Comparison','iaComparison',4,null,null,null,null)
,(47,'Summary','waterConsumptionOpDash',2,null,null,null,null)
,(48,'Dispatch','microgridDispatch',2,null,1,'PlusMinutes24MinusMinutes12',null)
,(49,'Summary','waterPaSummaryCombo',2,null,null,null,null)
,(50,'Report','performanceAnalystReport',10,null, null, null, null)
,(51,'Map','waterAppConsumptionMap',3,null,null,null,null)
,(52,'Chart','waterAppConsumptionChart',4,null,null,null,null)
,(53,'Anomalies','waterAppConsumptionAnomaly',5,null,null,null,null)
,(54,'Summary','waterAppConsumptionSummary',2,null,null,null,null)
,(55,'Dashboard','waterAppConsumptionOpDash',1,null,null,null,null)
,(56,'Financial','performanceAnalystFinancial',2,null, null, null, null) 
,(57,'OM','performanceAnalystOM',3,null, null, null, null) 
,(58,'Alerts','alerts',90,null, null, null,null)
,(59,'User Charts','performanceAnalystUserTrend',3,null, null, null, null) -- keshav
,(60,'Dispatch','microgridDispatch',3,null, 1, null, null)
,(61,'Summary','waterPaSummaryCombo',1,null, null, null, null)
,(62,'Map','waterPaMap',1,null, null, null, null) --waterPaSummaryTab
,(63,'Summary','performanceAnalystCoffeePot',10,null,0,null,null) --Coffee Pot Summary Tab
,(64,'Summary','solarSite',81,null,0,null,null)
,(65,'Summary','solarRollup',81,null,0,null,null)
,(66,'Summary','fleet',1,null,null,null,null)
,(67,'Details','performanceAnalystReport',2,null,null,null,null)
)
AS SOURCE([AssetViewID],[Name],[Path],[DisplayOrder],[StopAtLevel],[ShowFuture],[TimeRange],[Refresh])
ON Target.[AssetViewID]= Source.[AssetViewID]
WHEN MATCHED THEN UPDATE 
	SET [Name]=Source.[Name],
	[Path]=Source.[Path],
	[DisplayOrder]=Source.[DisplayOrder],
	[StopAtLevel]=Source.[StopAtLevel],
	[ShowFuture]=Source.[ShowFuture],
	[TimeRange]=Source.[TimeRange],
	[Refresh]=Source.[Refresh]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AssetViewID],[Name],[Path],[DisplayOrder],[StopAtLevel],[ShowFuture],[TimeRange],[Refresh])
VALUES ([AssetViewID],[Name],[Path],[DisplayOrder],[StopAtLevel],[ShowFuture],[TimeRange],[Refresh])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [UIConfig].[tAssetView] OFF

PRINT 'Merging into UIConfig.[tAssetAssetViewMap]'
--Sync UIConfig.tAssetAssetViewMap
SET IDENTITY_INSERT [UIConfig].[tAssetAssetViewMap] ON
MERGE INTO UIConfig.tAssetAssetViewMap AS TARGET USING (VALUES 
 (1,2,NULL,4001,NULL,NULL,@asset360DN,NULL,NULL)
,(2,3,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(3,5,NULL,NULL,NULL,NULL,@asset360DN,NULL,NULL)
,(4,6,NULL,NULL,NULL,NULL,@asset360DN,NULL,NULL)
,(5,4,NULL,NULL,1,NULL,@asset360DN,NULL,NULL)
,(6,4,NULL,NULL,2,NULL,@asset360DN,NULL,NULL)
,(7,4,NULL,NULL,3,NULL,@asset360DN,NULL,NULL)
,(8,7,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(9,8,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(10,9,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(11,10,NULL,4001,NULL,NULL,@matsLiteDN,NULL,NULL)
,(12,11,NULL,4001,NULL,NULL,@matsLiteDN,NULL,NULL)
,(13,4,NULL,NULL,1,NULL,@solarPVDN,NULL,NULL)
,(14,4,NULL,NULL,2,NULL,@solarPVDN,NULL,NULL)
,(15,4,NULL,NULL,3,NULL,@solarPVDN,NULL,NULL)
,(16,1,NULL,NULL,5,NULL,@solarPVDN,NULL,NULL)
,(17,12,NULL,NULL,3,NULL,@matsLiteDN,NULL,NULL)
,(18,12,NULL,NULL,2,NULL,@matsLiteDN,NULL,NULL)
,(19,13,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(20,14,NULL,6284,NULL,NULL,@asset360DN,NULL,NULL)
,(21,14,NULL,6285,NULL,NULL,@asset360DN,NULL,NULL)
,(22,14,NULL,6286,NULL,NULL,@asset360DN,NULL,NULL)
,(23,15,NULL,6284,NULL,NULL,@asset360DN,NULL,NULL)
,(24,15,NULL,6285,NULL,NULL,@asset360DN,NULL,NULL)
,(25,15,NULL,6286,NULL,NULL,@asset360DN,NULL,NULL)
,(26,16,NULL,NULL,5,NULL,@solarPVDN,NULL,NULL)
,(27,16,NULL,NULL,6,NULL,@solarPVDN,NULL,NULL)
,(28,12,NULL,NULL,1,NULL,@matsLiteDN,NULL,NULL)
,(29,21,NULL,NULL,NULL,NULL,@evrnDshbdDN,NULL,NULL)
,(30,22,NULL,NULL,2,NULL,@csaprDN,NULL,NULL)
,(31,23,NULL,NULL,2,NULL,@csaprDN,NULL,NULL)
,(32,22,NULL,NULL,3,NULL,@csaprDN,NULL,NULL)
,(33,23,NULL,NULL,3,NULL,@csaprDN,NULL,NULL)
,(34,22,NULL,NULL,4,NULL,@csaprDN,NULL,NULL)
,(35,23,NULL,NULL,4,NULL,@csaprDN,NULL,NULL)
,(36,22,NULL,NULL,1,NULL,@csaprDN,NULL,NULL)
,(37,23,NULL,NULL,1,NULL,@csaprDN,NULL,NULL)
,(38,2,NULL,4001,NULL,NULL,@asset360DN,NULL,NULL)
,(39,3,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(40,5,NULL,NULL,NULL,NULL,@asset360DN,NULL,NULL)
,(41,6,NULL,NULL,NULL,NULL,@asset360DN,NULL,NULL)
,(42,4,NULL,NULL,1,NULL,@asset360DN,NULL,NULL)
,(43,4,NULL,NULL,2,NULL,@asset360DN,NULL,NULL)
,(44,4,NULL,NULL,3,NULL,@asset360DN,NULL,NULL)
,(45,7,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(46,8,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(47,9,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(48,10,NULL,4001,NULL,NULL,@matsLiteDN,NULL,NULL)
,(49,11,NULL,4001,NULL,NULL,@matsLiteDN,NULL,NULL)
,(53,1,NULL,NULL,5,NULL,@solarPVDN,NULL,NULL)
,(54,12,NULL,NULL,3,NULL,@matsLiteDN,NULL,NULL)
,(55,12,NULL,NULL,2,NULL,@matsLiteDN,NULL,NULL)
,(56,13,NULL,4012,NULL,NULL,@solarPVDN,NULL,NULL)
,(57,14,NULL,6284,NULL,NULL,@asset360DN,NULL,NULL)
,(58,14,NULL,6285,NULL,NULL,@asset360DN,NULL,NULL)
,(59,14,NULL,6286,NULL,NULL,@asset360DN,NULL,NULL)
,(60,15,NULL,6284,NULL,NULL,@asset360DN,NULL,NULL)
,(61,15,NULL,6285,NULL,NULL,@asset360DN,NULL,NULL)
,(62,15,NULL,6286,NULL,NULL,@asset360DN,NULL,NULL)
,(63,16,NULL,NULL,5,NULL,@solarPVDN,NULL,NULL)
,(64,16,NULL,NULL,6,NULL,@solarPVDN,NULL,NULL)
,(65,12,NULL,NULL,1,NULL,@matsLiteDN,NULL,NULL)
,(66,18,NULL,NULL,NULL,NULL,@pgAnalystDN,NULL,NULL)
,(67,19,NULL,NULL,NULL,NULL,@pgAnalystDN,NULL,NULL)
,(68,20,NULL,NULL,NULL,NULL,@pgAnalystDN,NULL,NULL)
,(69,24,NULL,6302,NULL,NULL,@waterCHPDN,NULL,NULL)
,(70,25,NULL,6302,NULL,NULL,@waterCHPDN,NULL,NULL)
,(71,24,NULL,6303,NULL,NULL,@waterCHPDN,NULL,NULL)
,(72,25,NULL,6303,NULL,NULL,@waterCHPDN,NULL,NULL)
,(73,26,NULL,NULL,2,NULL,@projPriorDN,NULL,NULL)
,(74,26,NULL,NULL,3,NULL,@projPriorDN,NULL,NULL)
,(75,26,NULL,NULL,4,NULL,@projPriorDN,NULL,NULL)
,(76,27,NULL,NULL,NULL,NULL,@apPerfAnalystDN,NULL,NULL)

--Old Water Meter
,(77,28,NULL,6284,NULL,NULL,@oldWaterMeterDN,NULL,NULL) --waterConsumptionMap
,(78,28,NULL,6285,NULL,NULL,@oldWaterMeterDN,NULL,NULL)
,(79,28,NULL,6286,NULL,NULL,@oldWaterMeterDN,NULL,NULL)
,(80,29,NULL,6284,NULL,NULL,@oldWaterMeterDN,NULL,NULL) --waterConsumptionChart
,(81,29,NULL,6285,NULL,NULL,@oldWaterMeterDN,NULL,NULL)
,(82,29,NULL,6286,NULL,NULL,@oldWaterMeterDN,NULL,NULL)
,(83,30,NULL,6284,NULL,NULL,@oldWaterMeterDN,NULL,NULL) --waterConsumptionAnomaly
,(84,30,NULL,6285,NULL,NULL,@oldWaterMeterDN,NULL,NULL)
,(85,30,NULL,6286,NULL,NULL,@oldWaterMeterDN,NULL,NULL)
,(105,38,NULL,6375,NULL,NULL,@oldWaterMeterDN,NULL,NULL) --waterConsumptionSummary
,(132,47,null,6284,null,null,@oldWaterMeterDN,null,null) --waterConsumptionOpDash
,(133,47,null,6285,null,null,@oldWaterMeterDN,null,null)
,(134,47,null,6286,null,null,@oldWaterMeterDN,null,null)


,(86,31,NULL,4017,NULL,NULL,@waterReclamationDN,NULL,NULL)
,(87,32,NULL,4015,NULL,NULL,@waterTreatmentDN,NULL,NULL)

,(88,13,NULL,6240,NULL,NULL,@solarPVDN,NULL,NULL)
,(89,13,NULL,6229,NULL,NULL,@solarPVDN,NULL,NULL)

,(98,34,NULL,6302,NULL,NULL,@waterCHPDN,NULL,NULL)
,(99,34,NULL,6303,NULL,NULL,@waterCHPDN,NULL,NULL)
,(100,34,NULL,2000,NULL,NULL,@waterCHPDN,NULL,NULL)

,(101,35,NULL,NULL,NULL,NULL,@investmentAccelDN,NULL,NULL)
,(102,36,NULL,NULL,NULL,NULL,@investmentAccelDN,NULL,NULL)
,(103,37,NULL,NULL,NULL,NULL,@investmentAccelDN,NULL,NULL)
,(104,46,NULL,NULL,NULL,NULL,@investmentAccelDN,NULL,NULL)

--Performance Analyst
,(106,39,NULL,NULL,NULL,NULL,@pgAnalystDN,6377,NULL)
,(180,39,NULL,NULL,NULL,NULL,@pgAnalystDN,6378,NULL)
,(107,4,NULL,NULL,NULL,NULL,@pgAnalystDN,NULL,NULL)
,(144,50,null,null,null,null,@pgAnalystDN,null,null)

--Performance Analyst Solar
,(110,18,NULL,NULL,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(111,19,NULL,NULL,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(112,20,NULL,NULL,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(113,8,NULL,4012,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(114,13,NULL,4012,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(115,13,NULL,6240,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(116,13,NULL,6229,NULL,NULL,@solarAnalystDN,NULL,NULL)
,(117,3,NULL,4012,NULL,NULL,@solarAnalystDN,NULL,NULL)
-- AirPermits
,(118,40,NULL,NULL,4,NULL,@airPermitsDN,NULL,NULL)
,(119,41,NULL,NULL,4,NULL,@airPermitsDN,NULL,NULL)
,(120,42,NULL,NULL,3,NULL,@airPermitsDN,NULL,NULL)
,(121,42,NULL,NULL,2,NULL,@airPermitsDN,NULL,NULL)
,(122,42,NULL,NULL,1,NULL,@airPermitsDN,NULL,NULL)
,(123,40,NULL,NULL,4,NULL,@airPermitsDN,NULL,NULL)
,(124,41,NULL,NULL,4,NULL,@airPermitsDN,NULL,NULL)
,(125,42,NULL,NULL,3,NULL,@airPermitsDN,NULL,NULL)
,(126,42,NULL,NULL,2,NULL,@airPermitsDN,NULL,NULL)
,(127,42,NULL,NULL,1,NULL,@airPermitsDN,NULL,NULL)
--Sludge Analyst
,(128,43,NULL,6381,NULL,NULL,@sludgeAnalystDN,NULL,'SludgeAnalyst') --Add sludge analyst to the basin type
,(129,43,NULL,6382,NULL,NULL,@sludgeAnalystDN,NULL,'SludgeAnalyst') --Add sludge analyst to the sedimentation system type
,(130,44,NULL,NULL,NULL,NULL,@waterAnalystDN,6284,NULL)
--Risk Matrix
,(131,45,null,null,null,null,@riskMatrixDN,null,null)

----Microgrid
,(90,33,NULL,NULL,NULL,NULL,@microgridDN,6435,NULL)
,(135,48,null,null,null,null,@microgridDN,6435,null)
--Water Combo Tab Related @waterAnalystDN
,(136,49,null,6284,null,null,@waterAnalystDN,null,null)
,(137,49,null,6285,null,null,@waterAnalystDN,null,null)
--,(138,49,18512,null,null,null,@waterAnalystDN,null,null), this is added in below with a check on the asset
--,(139,49,18515,null,null,null,@waterAnalystDN,null,null), this is added in below with a check on the asset
,(140,39,null,6224,null,null,@waterAnalystDN,null,null)
,(141,39,null,6250,null,null,@waterAnalystDN,null,null)
,(142,49,null,6384,null,null,@waterAnalystDN,null,null)
,(143,44,null,6286,null,null,@waterAnalystDN,null,null)

--New Water Meter
,(145,51,NULL,6284,NULL,NULL,@waterMeterDN,NULL,NULL)--waterConsumptionMap
,(146,51,NULL,6285,NULL,NULL,@waterMeterDN,NULL,NULL)
,(147,51,NULL,6286,NULL,NULL,@waterMeterDN,NULL,NULL)
,(148,52,NULL,6284,NULL,NULL,@waterMeterDN,NULL,NULL) --waterConsumptionChart
,(149,52,NULL,6285,NULL,NULL,@waterMeterDN,NULL,NULL)
,(150,52,NULL,6286,NULL,NULL,@waterMeterDN,NULL,NULL)
,(151,53,NULL,6284,NULL,NULL,@waterMeterDN,NULL,NULL) --waterConsumptionAnomaly
,(152,53,NULL,6285,NULL,NULL,@waterMeterDN,NULL,NULL)
,(153,53,NULL,6286,NULL,NULL,@waterMeterDN,NULL,NULL)
,(154,54,NULL,6375,NULL,NULL,@waterMeterDN,NULL,NULL) --waterConsumptionSummary
,(155,55,null,6284,null,null,@waterMeterDN,null,null) --waterConsumptionOpDash
,(156,55,null,6285,null,null,@waterMeterDN,null,null)
,(157,55,null,6286,null,null,@waterMeterDN,null,null)

-- CHP Performance Analyst
,(158,56,null,null,null,null,@pgAnalystDN,6377,'CHPAnalyst')  
,(159,57,null,null,null,null,@pgAnalystDN,6377,'CHPAnalyst')  
,(160,58,null,null,null,null,@pgAnalystDN,6377,'CHPAnalyst')  
,(161,59,null,null,null,null,@pgAnalystDN,6377,'CHPAnalyst')  
,(162,34,null,null,null,null,@pgAnalystDN,6377,'CHPAnalyst')
,(163,60,null,null,null,null,@pgAnalystDN,6377,'CHPAnalyst')

--Highest is 162 in Performance Analyst for CHP

,(164,35,NULL,NULL,NULL,NULL,@sekoiaDN,NULL,NULL)
,(165,36,NULL,NULL,NULL,NULL,@sekoiaDN,NULL,NULL)
,(166,37,NULL,NULL,NULL,NULL,@sekoiaDN,NULL,NULL)
,(167,46,NULL,NULL,NULL,NULL,@sekoiaDN,NULL,NULL)

-- CHP Performance Analyst - Dispatch
,(168,60,NULL,6302,NULL,NULL,@pgAnalystDN,NULL,'CHPAnalyst')
,(169,60,NULL,NULL,NULL,NULL,@pgAnalystDN,6302,'CHPAnalyst')
-- Water analyst - Well fields
,(170,61,null,4015,null,null,@waterAnalystDN,null,null)
,(171,62,null,4015,null,null,@waterAnalystDN,null,null)
,(172,61,null,4017,null,null,@waterAnalystDN,null,null)
,(173,62,null,4017,null,null,@waterAnalystDN,null,null)
,(174,61,null,6548,null,null,@waterAnalystDN,null,null)
,(175,62,null,6548,null,null,@waterAnalystDN,null,null)
,(176,61,null,6549,null,null,@waterAnalystDN,null,null)
,(177,62,null,6549,null,null,@waterAnalystDN,null,null)
,(178,48,null,6548,null,null,@waterAnalystDN,null,null)
,(179,62,null,6206,null,null,@waterAnalystDN,null,null)

-- Dispatch Analyst
,(183,60,null,6548,null,null,@dispatchAnalystDN,null,null)

-- Water Quality Platform off SEKOIA baseline
,(184,35,NULL,NULL,NULL,NULL,@waterQualityPlatformDN,NULL,NULL)
,(185,36,NULL,NULL,NULL,NULL,@waterQualityPlatformDN,NULL,NULL)
,(186,37,NULL,NULL,NULL,NULL,@waterQualityPlatformDN,NULL,NULL)
,(187,46,NULL,NULL,NULL,NULL,@waterQualityPlatformDN,NULL,NULL)

-- Virtual Tour
,(188,4, NULL,NULL,NULL,NULL,@VirtualTourDN, NULL,NULL)
,(189,50, NULL,NULL,NULL,NULL,@VirtualTourDN, NULL,NULL)

--Water Analyst
,(190,61,null,null,null,null,@waterAnalystDN,4015,null)

--Solar PA
,(192,64,null,6436,null,null,@pgAnalystDN,null,null)
,(193,65,null,6724,null,null,@pgAnalystDN,null,null)
,(194,65,null,6725,null,null,@pgAnalystDN,null,null)

--Waste Water Analyst
,(195,66,NULL,NULL,NULL,NULL,@wasteWaterAnalystDN,NULL,NULL)
,(196,67,NULL,NULL,NULL,NULL,@wasteWaterAnalystDN,NULL,NULL)
)
AS SOURCE([AssetAssetViewMapID],[AssetViewID],[AssetID],[AssetClassTypeID],[AssetTypeID],[UserRoleID],[AppContext],[AncestorAssetClassTypeID],[AppContextName])
ON Target.[AssetAssetViewMapID]= Source.[AssetAssetViewMapID]
WHEN MATCHED THEN UPDATE 
	SET [AssetViewID]=Source.[AssetViewID],[AssetID]=Source.[AssetID],[AssetClassTypeID]=Source.[AssetClassTypeID],
		[AssetTypeID]=Source.[AssetTypeID],[UserRoleID]=Source.[UserRoleID],
		[AppContext]=Source.[AppContext],[AncestorAssetClassTypeID]=Source.[AncestorAssetClassTypeID],
		[AppContextName]=Source.[AppContextName]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AssetAssetViewMapID],[AssetViewID],[AssetID],[AssetClassTypeID],[AssetTypeID],[UserRoleID],[AppContext],[AncestorAssetClassTypeID],[AppContextName])
VALUES ([AssetAssetViewMapID],[AssetViewID],[AssetID],[AssetClassTypeID],[AssetTypeID],[UserRoleID],[AppContext],[AncestorAssetClassTypeID],[AppContextName])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

IF EXISTS(SELECT * FROM Asset.tAsset WHERE AssetID = 18512 and AssetAbbrev = 'Kansas City BPU')
BEGIN 
	DELETE FROM UIConfig.tAssetAssetViewMap WHERE AssetAssetViewMapID = 138;
	INSERT INTO UIConfig.tAssetAssetViewMap([AssetAssetViewMapID],[AssetViewID],[AssetID],[AssetClassTypeID],[AssetTypeID],[UserRoleID],[AppContext],[AncestorAssetClassTypeID],[AppContextName])
	VALUES (138,49,18512,null,null,null,@waterAnalystDN,null,null)
END 

IF EXISTS(SELECT * FROM Asset.tAsset WHERE AssetID = 18515 and AssetAbbrev = 'Water Distribution System')
BEGIN 
	DELETE FROM UIConfig.tAssetAssetViewMap WHERE AssetAssetViewMapID = 139;
	INSERT INTO UIConfig.tAssetAssetViewMap([AssetAssetViewMapID],[AssetViewID],[AssetID],[AssetClassTypeID],[AssetTypeID],[UserRoleID],[AppContext],[AncestorAssetClassTypeID],[AppContextName])
	VALUES (139,49,18515,null,null,null,@waterAnalystDN,null,null)
END 

SET IDENTITY_INSERT [UIConfig].[tAssetAssetViewMap] OFF

PRINT 'Merging into [Projection].[tScenarioObjectiveTypeAppContextMap]'
--Sync [Projection].[tScenarioObjectiveTypeAppContextMap]
SET IDENTITY_INSERT [Projection].[tScenarioObjectiveTypeAppContextMap] ON
MERGE INTO [Projection].[tScenarioObjectiveTypeAppContextMap] AS TARGET USING (VALUES 
(1,1,@scenarioPlannerAC)
,(2,2,@regAdminAC)
)
AS SOURCE([ScenarioObjectiveTypeAppContextMapID],[ScenarioObjectiveTypeID],[AppContextID])
ON Target.[ScenarioObjectiveTypeAppContextMapID]= Source.[ScenarioObjectiveTypeAppContextMapID]
WHEN MATCHED THEN UPDATE 
	SET [ScenarioObjectiveTypeID]=Source.[ScenarioObjectiveTypeID],[AppContextID]=Source.[AppContextID]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([ScenarioObjectiveTypeAppContextMapID],[ScenarioObjectiveTypeID],[AppContextID])
VALUES ([ScenarioObjectiveTypeAppContextMapID],[ScenarioObjectiveTypeID],[AppContextID])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [Projection].[tScenarioObjectiveTypeAppContextMap] OFF

--Restore FKs to [UIConfig].[tAppContext]
ALTER TABLE [Projection].[tScenarioObjectiveTypeAppContextMap] CHECK CONSTRAINT [FK_tScenarioObjectiveTypeAppContextMap_tAppContext]
ALTER TABLE [UIConfig].[tAssetAssetViewMap] CHECK CONSTRAINT [FK_tAssetAssetViewMap_tAssetView]
ALTER TABLE [Projection].[tScenarioObjectiveTypeAppContextMap] CHECK CONSTRAINT [FK_tScenarioObjectiveTypeAppContextMap_tAppContext]

PRINT 'Merging into [UIConfig].[tAppContextTab]'
--Create the necessary app context tabs.
MERGE INTO [UIConfig].[tAppContextTab] AS TARGET USING (VALUES 
(1,@pgAnalystAC,'Summary',1,NULL)
,(2,@pgAnalystAC,'Alerts',2,NULL)
,(4,@pgAnalystAC,'Map',4,NULL)
,(5,@pgAnalystAC,'Report',7,NULL)

,(6,@solarAnalystAC,'Summary',1,NULL)
,(7,@solarAnalystAC,'Alerts',2,NULL)
,(8,@solarAnalystAC,'Map',4,NULL)
,(9,@solarAnalystAC,'Forecast',5,NULL)
,(10,@solarAnalystAC,'Availability',6,NULL)
,(11,@solarAnalystAC,'Report',7,NULL)

,(12,@waterAnalystAC,'Summary',1,NULL)
,(13,@waterAnalystAC,'Alerts',2,NULL)
,(26,@waterAnalystAC,'Map',4,NULL)
,(14,@waterAnalystAC,'Report',7,NULL)

,(27,@dispatchAnalystAC,'Dispatch',10,NULL)

,(15,@issuesAnalystAC,'Issues',3,NULL)
,(16,@issuesAnalystAC,'Report',7,NULL)

,(17,@microgridAC ,'Summary',1,NULL)
,(18,@microgridAC,'Dispatch',2,NULL)

,(20,@CHPAnalystAC,'Financial',2,NULL)
,(21,@CHPAnalystAC,'OM',3,NULL)
,(22,@CHPAnalystAC,'User Charts',4,NULL) 
,(23,@CHPAnalystAC,'Forecast',8,NULL)
,(24,@CHPAnalystAC,'Availability',9,NULL)
,(25,@CHPAnalystAC,'Dispatch',10,NULL)

,(28,@mapAnalystAC,'Map',4,NULL)
,(29,@reportAnalystAC,'Report',7,NULL)

,(30, @assetExplorerAC,'Info',1,@AssetExplorerInfoTabSR)
,(31, @assetExplorerAC,'Attachments',2,@AssetExplorerAttachmentsTabSR)
,(32, @assetExplorerAC,'Blog',3,@AssetExplorerBlogTabSR)
,(33, @assetExplorerAC,'Location',4,@AssetExplorerLocationTabSR)
,(34, @assetExplorerAC,'List',5,@AssetExplorerListTabSR)
,(35, @assetExplorerAC,'Search',6,@AssetExplorerSearchTabSR)

,(36, @issuesManagementAC, 'Issues',1,@IssuesManagementIssuesTabSR)
,(37, @issuesManagementAC, 'Scorecard',2,@IssuesManagementScorecardTabSR)

,(38, @workManagementAC, 'Issues',1,@WorkManagementIssuesTabSR)
,(39, @workManagementAC, 'Scorecard',2,@WorkManagementScorecardTabSR)

,(40, @investmentAccelAC, 'Summary',1,@InvestmentAcceleratorSummaryTabSR)
,(41, @investmentAccelAC, 'Timeline',2,@InvestmentAcceleratorTimelineTabSR)
,(42, @investmentAccelAC, 'Map',3,@InvestmentAcceleratorMapTabSR)
,(43, @investmentAccelAC, 'Trends',4,@InvestmentAcceleratorTrendsTabSR)
,(44, @investmentAccelAC, 'Views',5,@InvestmentAcceleratorViewsTabSR)
,(45, @investmentAccelAC, 'Comparison',6,@InvestmentAcceleratorComparisonTabSR)
,(46, @wasteWaterAnalystAC, 'Summary', 1,NULL)

,(48, @WasteWaterAssetDataManagementAC,'Info',1,@WasteWaterAssetDataManagementInfoTabSR)
,(49, @WasteWaterAssetDataManagementAC,'Attachments',2,@WasteWaterAssetDataManagementAttachmentsTabSR)
,(50, @WasteWaterAssetDataManagementAC,'Blog',3,@WasteWaterAssetDataManagementBlogTabSR)
,(51, @WasteWaterAssetDataManagementAC,'List',4,@WasteWaterAssetDataManagementListTabSR)

,(52, @WasteWaterScorecardsAC, 'Summary', 1, @WasteWaterScorecardsSummaryTabSR)

,(53, @WasteWaterInformAC, 'Issues',1,@WasteWaterInformIssuesTabSR)
,(54, @wasteWaterAnalystAC, 'Details',2,NULL)
)
AS SOURCE([AppContextTabID],[AppContextID],[DisplayName],[DisplayOrder],[SecurityResourceID])
ON Target.[AppContextTabID]= Source.[AppContextTabID]
WHEN MATCHED THEN UPDATE 
	SET [AppContextID]=Source.[AppContextID],[DisplayName]=Source.[DisplayName],[DisplayOrder]=Source.[DisplayOrder], [SecurityResourceID]=Source.[SecurityResourceID]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AppContextTabID],[AppContextID],[DisplayName],[DisplayOrder],[SecurityResourceID])
VALUES ([AppContextTabID],[AppContextID],[DisplayName],[DisplayOrder],[SecurityResourceID])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

DECLARE @auditUser int = (select SecurityUserID from AccessControl.tUser where EmailUnique = 'TEST@test.com');
DECLARE @auditCustomer uniqueidentifier = (select CustomerID from AccessControl.tCustomer where [Name] = 'Atonix');

-- sort the app contexts by DisplayName

PRINT 'Setting tAppContext display order by DisplayName'
UPDATE a 
SET DisplayOrder = b.RN
FROM UIConfig.tAppContext a JOIN (select AppContextID,ROW_NUMBER() OVER(ORDER BY DisplayName ASC) as RN from UIConfig.tAppContext) b on 
a.AppContextID = b.AppContextID
;

INSERT INTO UIConfig.tAdHocNodeType(NodeTypeId, NodeTypeAbbrev, NodeTypeDesc)
VALUES
(1,N'ContainerNode',N'Container Node / Generic Node'),
(2,N'AssetLinkNode',N'Asset Link Node'),
(3,N'CriteriaObjectLinkNode',N'Asset Link Node')
GO

INSERT INTO UIConfig.tAdHocAssetNodeBehavior(AssetNodeBehaviorId, AssetNodeBehaviorAbbrev, AssetNodeBehaviorDesc)
VALUES
(1, 'AssetOnly','Asset only'),
(2, 'AssetAndChildren','Asset and Children'),
(3, 'AssetAndDescendants','And and all descendants')
GO

INSERT INTO UIConfig.tAdHocCoNodeBehavior (CoNodeBehaviorId, CoNodeBehaviorAbbrev, CoNodeBehaviorDesc)
VALUES
(1, 'AssestOnly', 'Assets Only'),
(2, 'AssetAndAncestry', 'Assets and Ancestry Nodes'),
(3, 'AssetAndDescendants', 'Assets and Descendant Nodes')

INSERT INTO UIConfig.tCategoryType ([ID],[Name]) VALUES (1,'Time Range Selector')