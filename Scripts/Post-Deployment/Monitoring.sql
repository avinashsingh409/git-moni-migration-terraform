﻿MERGE INTO Monitoring.tFilterType AS TARGET USING (VALUES 
(1,'Filter Frozen Data')
,(2,'Filter Transient Data')
,(3,'Filter Absolute Limits')
)
AS SOURCE([FilterTypeID],[FilterTypeDesc])
ON Target.[FilterTypeID]= Source.[FilterTypeID]
WHEN MATCHED THEN UPDATE SET [FilterTypeDesc]=Source.[FilterTypeDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([FilterTypeID],[FilterTypeDesc])
VALUES ([FilterTypeID],[FilterTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Monitoring.tModelType AS TARGET USING (VALUES 
(1,'Multivariant Linear Regression')
,(2,'Frozen Data Check')
,(3,'Fixed Limits')
)
AS SOURCE([ModelTypeID],[ModelTypeDesc])
ON Target.[ModelTypeID]= Source.[ModelTypeID]
WHEN MATCHED THEN UPDATE SET [ModelTypeDesc]=Source.[ModelTypeDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([ModelTypeID],[ModelTypeDesc])
VALUES ([ModelTypeID],[ModelTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;



-- Event Annotation Importer Mapping

MERGE INTO [Monitoring].[tEventType] AS Target
USING (VALUES
  (1,'MembraneTrainFF','Membrane Train Forward Filtration',0)
 ,(2,'MembraneTrainIT','Membrane Train IT',0)
 ,(3,'MembraneTrainEFM','Membrane Train EFM',0)
 ,(4,'MembraneTrainCIP','Membrane Train CIP',0)
 ,(5,'MembraneTrainBW','Membrane Train Backwash',0)
 ,(6,'MembraneTrainEFMCl2','Membrane Train EFM Cl2',0)
 ,(7,'MembraneTrainEFMCitric','Membrane Train EFM Citric',0)
 ,(8,'MembraneTrainEFMCaustic','Membrane Train EFM Caustic',0)
 ,(9,'MembraneTrainEFMSulfuric','Membrane Train EFM Sulfuric',0)
 ,(10,'MembraneTrainEFMSodiumBisulfate','Membrane Train EFM Sodium Bisulfate',0)
 ,(11,'MembraneTrainCIPCl2','Membrane Train CIP Cl2',0)
 ,(12,'MembraneTrainCIPCitric','Membrane Train CIP Citric',0)
 ,(13,'MembraneTrainCIPCaustic','Membrane Train CIP Caustic',0)
 ,(14,'MembraneTrainCIPSulfuric','Membrane Train CIP Sulfuric',0)
 ,(15,'MembraneTrainCIPSodiumBisulfate','Membrane Train CIP Sodium Bisulfate',0)
 ,(16,'SPVWeather(Snow)','SolarPV: Weather - Snow',0)
 ,(17,'SPVWeather(Wind)','SolarPV: Weather - Wind Event (tracker stow)',0)
 ,(18,'SPVForceMajeure','SolarPV: Force Majeure (weather, war, terrorism)',0)
 ,(19,'SPVInvMalfunction','SolarPV: Inverter Malfunction',0)
 ,(20,'SPVGridEvents','SolarPV: Grid Events',0)
 ,(21,'SPVTrackMalfunction','SolarPV: Tracker Malfunction',0)
 ,(22,'SPVDataLoss','SolarPV: Loss of Data',0)
 ,(23,'SPVPlannedMaintenance','SolarPV: Planned Maintenance',0)
 ,(24,'SPVUnplannedMaintenance','SolarPV: Unplanned Maintenance',0)
 ,(25,'SPVTheftVandalism','SolarPV: Theft/Vandalism',0)
) AS Source ([EventTypeID],[EventTypeAbbrev],[EventTypeDesc],[DelayBeforeNextEvent])
ON (Target.[EventTypeID] = Source.[EventTypeID])
WHEN MATCHED THEN
 UPDATE SET
  [EventTypeAbbrev] = Source.[EventTypeAbbrev], 
  [EventTypeDesc] = Source.[EventTypeDesc], 
  [DelayBeforeNextEvent] = Source.[DelayBeforeNextEvent]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([EventTypeID],[EventTypeAbbrev],[EventTypeDesc],[DelayBeforeNextEvent])
 VALUES(Source.[EventTypeID],Source.[EventTypeAbbrev],Source.[EventTypeDesc],Source.[DelayBeforeNextEvent])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;

SET IDENTITY_INSERT [Monitoring].[tEventTypeAnnotationImporterMap] ON

MERGE INTO [Monitoring].[tEventTypeAnnotationImporterMap] AS Target
USING (VALUES
	(1,1,1)
	,(2,2,1)
	,(3,3,1)
	,(4,4,1)
	,(5,5,1)
	,(6,6,1)
	,(7,7,1)
	,(8,8,1)
	,(9,9,1)
	,(10,10,1)
	,(11,11,1)
	,(12,12,1)
	,(13,13,1)
	,(14,14,1)
	,(15,15,1)
) AS Source ([EventTypeImporterMapID],[EventTypeID],[AnnotationImporterID])
ON (Target.[EventTypeImporterMapID] = Source.[EventTypeImporterMapID])
WHEN MATCHED THEN
	UPDATE SET
	[EventTypeID] = Source.[EventTypeID], 
	[AnnotationImporterID] = Source.[AnnotationImporterID]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([EventTypeImporterMapID],[EventTypeID],[AnnotationImporterID])
	VALUES(Source.[EventTypeImporterMapID],Source.[EventTypeID],Source.[AnnotationImporterID])
WHEN NOT MATCHED BY SOURCE THEN 
	DELETE
;

SET IDENTITY_INSERT [Monitoring].[tEventTypeAnnotationImporterMap] OFF
-- End Annotation Importer Mapping

MERGE INTO [Monitoring].[tNDModelActionItemPriorityType] AS Target
USING (VALUES
	(0,0,'','')
	,(1,1,'High','High')
	,(2,2,'Medium','Medium')
	,(3,3,'Low','Low')
) AS Source ([ModelActionItemPriorityTypeID],[ModelActionItemPriorityValue],[ModelActionItemPriorityAbbrev],[ModelActionItemPriorityDesc])
ON (Target.[ModelActionItemPriorityTypeID] = Source.[ModelActionItemPriorityTypeID])
WHEN MATCHED AND (
	NULLIF(Source.[ModelActionItemPriorityValue], Target.[ModelActionItemPriorityValue]) IS NOT NULL OR NULLIF(Target.[ModelActionItemPriorityValue], Source.[ModelActionItemPriorityValue]) IS NOT NULL OR 
	NULLIF(Source.[ModelActionItemPriorityAbbrev], Target.[ModelActionItemPriorityAbbrev]) IS NOT NULL OR NULLIF(Target.[ModelActionItemPriorityAbbrev], Source.[ModelActionItemPriorityAbbrev]) IS NOT NULL OR 
	NULLIF(Source.[ModelActionItemPriorityDesc], Target.[ModelActionItemPriorityDesc]) IS NOT NULL OR NULLIF(Target.[ModelActionItemPriorityDesc], Source.[ModelActionItemPriorityDesc]) IS NOT NULL) THEN
	UPDATE SET
	[ModelActionItemPriorityValue] = Source.[ModelActionItemPriorityValue], 
	[ModelActionItemPriorityAbbrev] = Source.[ModelActionItemPriorityAbbrev], 
	[ModelActionItemPriorityDesc] = Source.[ModelActionItemPriorityDesc]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([ModelActionItemPriorityTypeID],[ModelActionItemPriorityValue],[ModelActionItemPriorityAbbrev],[ModelActionItemPriorityDesc])
	VALUES(Source.[ModelActionItemPriorityTypeID],Source.[ModelActionItemPriorityValue],Source.[ModelActionItemPriorityAbbrev],Source.[ModelActionItemPriorityDesc])
WHEN NOT MATCHED BY SOURCE THEN 
	DELETE
;


MERGE INTO [Monitoring].[tNDModelActionItemType] AS Target
USING (VALUES
  (1,'Diagnose Set','Diagnose Set')
 ,(2,'Diagnose Cleared','Diagnose Cleared')
 ,(3,'Watch Set','Watch Set')
 ,(4,'Watch Cleared','Watch Cleared')
 ,(5,'Watch Expiration','Watch Expiration')
 ,(6,'Model Maintenance Set','Model Maintenance Set')
 ,(7,'Model Maintenance Cleared','Model Maintenance Cleared')
 ,(8,'Issue Created','Issue Created')
 ,(9,'Issue Closed','Issue Closed')
 ,(10,'Note Added','Note Added')
 ,(11,'Ignore Set','Ignore Set')
 ,(12,'Clear Alert Status','Clear Alert Status')
 ,(13,'Ignore Expiration','Ignore Expiration')
 ,(14, N'Watch Override', N'Watch Override')
 ,(15, N'Stop Ignoring', N'Stop Ignoring')
 ,(16,'Quick Watch Set','Quick Watch Set')
) AS Source ([ModelActionItemTypeID],[ModelActionItemTypeAbbrev],[ModelActionItemTypeDesc])
ON (Target.[ModelActionItemTypeID] = Source.[ModelActionItemTypeID])
WHEN MATCHED AND (
	NULLIF(Source.[ModelActionItemTypeAbbrev], Target.[ModelActionItemTypeAbbrev]) IS NOT NULL OR NULLIF(Target.[ModelActionItemTypeAbbrev], Source.[ModelActionItemTypeAbbrev]) IS NOT NULL OR 
	NULLIF(Source.[ModelActionItemTypeDesc], Target.[ModelActionItemTypeDesc]) IS NOT NULL OR NULLIF(Target.[ModelActionItemTypeDesc], Source.[ModelActionItemTypeDesc]) IS NOT NULL) THEN
 UPDATE SET
  [ModelActionItemTypeAbbrev] = Source.[ModelActionItemTypeAbbrev], 
  [ModelActionItemTypeDesc] = Source.[ModelActionItemTypeDesc]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ModelActionItemTypeID],[ModelActionItemTypeAbbrev],[ModelActionItemTypeDesc])
 VALUES(Source.[ModelActionItemTypeID],Source.[ModelActionItemTypeAbbrev],Source.[ModelActionItemTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;


MERGE INTO [Monitoring].[tNDPredictiveMethodType] AS Target
USING (VALUES
  (1,'Rolling Average','Rolling Average')
 ,(2,'Fixed Limit','Fixed Limit')
 ,(3,'LogReg','LogReg')
 ,(4,'GFF','GFF')
 ,(5,'Frozen Data Check','Frozon Data Check')
 ,(6,'LinReg','LinReg')
 ,(7,'ExpAvg','ExpAvg')
 ,(8,'Linear','Linear')
) AS Source ([PredictiveMethodTypeID],[PredictiveMethodAbbrev],[PredictiveMethodDesc])
ON (Target.[PredictiveMethodTypeID] = Source.[PredictiveMethodTypeID])
WHEN MATCHED THEN
 UPDATE SET
  [PredictiveMethodAbbrev] = Source.[PredictiveMethodAbbrev], 
  [PredictiveMethodDesc] = Source.[PredictiveMethodDesc]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([PredictiveMethodTypeID],[PredictiveMethodAbbrev],[PredictiveMethodDesc])
 VALUES(Source.[PredictiveMethodTypeID],Source.[PredictiveMethodAbbrev],Source.[PredictiveMethodDesc])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;

MERGE INTO [Monitoring].[tNDModelTypeEdicts] AS Target
USING (VALUES
  (1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,NULL)
 ,(2,0,1,1,1,1,1,1,1,1,1,1,1,0,0,1)
 ,(3,0,0,1,1,1,1,1,0,0,1,0,1,1,0,NULL)
 ,(4,0,0,0,0,0,0,1,0,0,0,0,0,0,1,NULL)
 ,(5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL)
 ,(6,0,1,1,1,1,1,0,1,1,1,1,0,0,0,7)
 ,(7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8)
 ,(8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8)
) AS Source ([ModelTypeEdictID],[InputsAvailable],[AltAreaAvailable],[AltFrequencyAvailable],[AltOscillationAvailable],[AltHighHighAvailable],[AltLowLowAvailable],[AltFrozenDataAvailable],[AnoMAEAvailable],[AnoBiasAvailable],[AnoFixedLimitAvailable],[DataAvailable],[MustHaveAnoMAEOrAnoBias],[MustHaveAnoFixedLimit],[MustHaveAltFrozenData],[PredictiveMethodTypeID])
ON (Target.[ModelTypeEdictID] = Source.[ModelTypeEdictID])
WHEN MATCHED THEN
 UPDATE SET
  [InputsAvailable] = Source.[InputsAvailable], 
  [AltAreaAvailable] = Source.[AltAreaAvailable], 
  [AltFrequencyAvailable] = Source.[AltFrequencyAvailable], 
  [AltOscillationAvailable] = Source.[AltOscillationAvailable], 
  [AltHighHighAvailable] = Source.[AltHighHighAvailable], 
  [AltLowLowAvailable] = Source.[AltLowLowAvailable], 
  [AltFrozenDataAvailable] = Source.[AltFrozenDataAvailable], 
  [AnoMAEAvailable] = Source.[AnoMAEAvailable], 
  [AnoBiasAvailable] = Source.[AnoBiasAvailable], 
  [AnoFixedLimitAvailable] = Source.[AnoFixedLimitAvailable], 
  [DataAvailable] = Source.[DataAvailable], 
  [MustHaveAnoMAEOrAnoBias] = Source.[MustHaveAnoMAEOrAnoBias], 
  [MustHaveAnoFixedLimit] = Source.[MustHaveAnoFixedLimit], 
  [MustHaveAltFrozenData] = Source.[MustHaveAltFrozenData], 
  [PredictiveMethodTypeID] = Source.[PredictiveMethodTypeID]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ModelTypeEdictID],[InputsAvailable],[AltAreaAvailable],[AltFrequencyAvailable],[AltOscillationAvailable],[AltHighHighAvailable],[AltLowLowAvailable],[AltFrozenDataAvailable],[AnoMAEAvailable],[AnoBiasAvailable],[AnoFixedLimitAvailable],[DataAvailable],[MustHaveAnoMAEOrAnoBias],[MustHaveAnoFixedLimit],[MustHaveAltFrozenData],[PredictiveMethodTypeID])
 VALUES(Source.[ModelTypeEdictID],Source.[InputsAvailable],Source.[AltAreaAvailable],Source.[AltFrequencyAvailable],Source.[AltOscillationAvailable],Source.[AltHighHighAvailable],Source.[AltLowLowAvailable],Source.[AltFrozenDataAvailable],Source.[AnoMAEAvailable],Source.[AnoBiasAvailable],Source.[AnoFixedLimitAvailable],Source.[DataAvailable],Source.[MustHaveAnoMAEOrAnoBias],Source.[MustHaveAnoFixedLimit],Source.[MustHaveAltFrozenData],Source.[PredictiveMethodTypeID])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;

MERGE INTO [Monitoring].[tNDModelType] AS Target
USING (VALUES
  (1,'APR','Advanced Pattern Recognition',1,1)
 ,(2,'Rolling Average','Rolling Average',2,2)
 ,(3,'Fixed Limit','Fixed Limit',3,3)
 ,(4,'Frozen Data','Frozen Data Check',4,4)
 ,(5,'External','External Model',5,5)
 ,(6,'Moving Average','Moving Average',6,6)
 ,(7,'Forecast','Forecast',7,7)
 ,(8,'Rate of Change','Rate of Change',8,8)
) AS Source ([ModelTypeID],[ModelTypeAbbrev],[ModelTypeDesc],[ModelTypeEdictID],[DisplayOrder])
ON (Target.[ModelTypeID] = Source.[ModelTypeID])
WHEN MATCHED AND (
	NULLIF(Source.[ModelTypeAbbrev], Target.[ModelTypeAbbrev]) IS NOT NULL OR NULLIF(Target.[ModelTypeAbbrev], Source.[ModelTypeAbbrev]) IS NOT NULL OR 
	NULLIF(Source.[ModelTypeDesc], Target.[ModelTypeDesc]) IS NOT NULL OR NULLIF(Target.[ModelTypeDesc], Source.[ModelTypeDesc]) IS NOT NULL OR 
	NULLIF(Source.[ModelTypeEdictID], Target.[ModelTypeEdictID]) IS NOT NULL OR NULLIF(Target.[ModelTypeEdictID], Source.[ModelTypeEdictID]) IS NOT NULL OR 
	NULLIF(Source.[DisplayOrder], Target.[DisplayOrder]) IS NOT NULL OR NULLIF(Target.[DisplayOrder], Source.[DisplayOrder]) IS NOT NULL) THEN
 UPDATE SET
  [ModelTypeAbbrev] = Source.[ModelTypeAbbrev], 
  [ModelTypeDesc] = Source.[ModelTypeDesc], 
  [ModelTypeEdictID] = Source.[ModelTypeEdictID], 
  [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ModelTypeID],[ModelTypeAbbrev],[ModelTypeDesc],[ModelTypeEdictID],[DisplayOrder])
 VALUES(Source.[ModelTypeID],Source.[ModelTypeAbbrev],Source.[ModelTypeDesc],Source.[ModelTypeEdictID],Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;

MERGE INTO [Monitoring].[tNDModelBuildStatusType] AS Target
USING (VALUES
	(1, 'Initiated', 'Initiated')
	, (2, 'Success', 'Success')
	, (3, 'Failure', 'Failure')
	, (4, 'Status', 'Status')
	, (5, 'PeriodicRetrainRequested', 'PeriodicRetrainRequested')
	, (6, 'ManualRetrainRequested', 'ManualRetrainRequested')
	, (7, 'QD Good Candidate', 'QD Good Candidate')
	, (8, 'QD Bad Candidate', 'QD Bad Candidate')
	, (9, 'QD Override Candidate', 'QD Override Candidate')
	, (10, 'QD Selection', 'QD Selection')
	, (11, 'QD Override', 'QD Override')
	, (12, 'QD Reconcile Delete Failure', 'QD Reconcile Delete Failure')
	, (13, 'QD Reconcile Pulled Replacement', 'QD Reconcile Pulled Replacement')
	, (14, 'QD Reconcile Validation Failure', 'QD Reconcile Validation Failure')
	, (15, 'Validation Failure', 'Validation Failure')
	, (16, 'Save Failure', 'Save Failure')
	, (17, 'Transport Failure', 'Transport Failure')
	, (18, 'Save Evaluation Requires DB Update', 'Save Evaluation Requires DB Update')
	, (19, 'Save Evaluation Requires ND Project Update', 'Save Evaluation Requires ND Project Update')
	, (20, 'Save Evaluation Requires Build', 'Save Evaluation Requires Build')
	, (21, 'Save Evaluation Unhandled Exception', 'Save Evaluation Unhandled Exception')
	, (22, 'Save', 'Save')
	, (23, 'Success', 'ND Project Update')
	, (24, 'Failure', 'ND Project Update Failure')
	, (25, 'Locked', 'Locked')
	, (26, 'Unlocked', 'Unlocked')
	, (27, 'Batch Edit', 'Batch Edit')
	, (28, 'Custodian Leaves Locked', 'Custodian Leaves Locked')
	, (29, 'Custodian Unlocked', 'Custodian Unlocked')
	, (30, 'Locked For Migration', 'Locked For Migration')
	, (31, 'Unlocked After Migration', 'Unlocked After Migration')
)AS Source ([ModelBuildStatusTypeID], [ModelBuildStatusTypeAbbrev], [ModelBuildStatusTypeDesc])
ON (Target.[ModelBuildStatusTypeID] = Source.[ModelBuildStatusTypeID])
WHEN MATCHED AND (
	NULLIF(Source.[ModelBuildStatusTypeAbbrev], Target.[ModelBuildStatusTypeAbbrev]) IS NOT NULL OR NULLIF(Target.[ModelBuildStatusTypeAbbrev], Source.[ModelBuildStatusTypeAbbrev]) IS NOT NULL OR
	NULLIF(Source.[ModelBuildStatusTypeDesc], Target.[ModelBuildStatusTypeDesc]) IS NOT NULL OR NULLIF(Target.[ModelBuildStatusTypeDesc], Source.[ModelBuildStatusTypeDesc]) IS NOT NULL) THEN
  UPDATE SET
	[ModelBuildStatusTypeAbbrev] = Source.[ModelBuildStatusTypeAbbrev],
	[ModelBuildStatusTypeDesc] = Source.[ModelBuildStatusTypeDesc]
WHEN NOT MATCHED BY TARGET THEN
  INSERT([ModelBuildStatusTypeID], [ModelBuildStatusTypeAbbrev], [ModelBuildStatusTypeDesc])
  VALUES(Source.[ModelBuildStatusTypeID], Source.[ModelBuildStatusTypeAbbrev], Source.[ModelBuildStatusTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN
  DELETE
;


MERGE INTO [Monitoring].[tNDLogicOperatorCategoryType] AS TARGET
   USING (VALUES
   (1, 'Comparison', 'Comparison', NULL, 1)
   , (2, 'IncreasingDecreasing', 'IncreasingDecreasing', 'Passed', 2)
   , (3, 'Transiency', 'Transiency', NULL, 3)  
   ) AS Source([LogicOperatorCategoryTypeID], [LogicOperatorCategoryTypeAbbrev], [LogicOperatorCategoryTypeDesc],
				[LogicOperatorCategoryTypeLabel], [DisplayOrder])
ON (TARGET.[LogicOperatorCategoryTypeID] = Source.[LogicOperatorCategoryTypeID])
WHEN MATCHED THEN
	UPDATE SET
	[LogicOperatorCategoryTypeID] = Source.[LogicOperatorCategoryTypeID]
	, [LogicOperatorCategoryTypeAbbrev] = Source.[LogicOperatorCategoryTypeAbbrev]
    , [LogicOperatorCategoryTypeDesc] = Source.[LogicOperatorCategoryTypeDesc]
	, [LogicOperatorCategoryTypeLabel] = Source.[LogicOperatorCategoryTypeLabel]
	, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LogicOperatorCategoryTypeID], [LogicOperatorCategoryTypeAbbrev], [LogicOperatorCategoryTypeDesc],
				[LogicOperatorCategoryTypeLabel], [DisplayOrder])
	VALUES(Source.[LogicOperatorCategoryTypeID], Source.[LogicOperatorCategoryTypeAbbrev], Source.[LogicOperatorCategoryTypeDesc],
				Source.[LogicOperatorCategoryTypeLabel], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;


MERGE INTO [Monitoring].[tNDLogicOperatorType] AS TARGET
   USING (VALUES
   (1, '=', 'Equal To', 1, 1)
   , (2, '>', 'Greater Than', 1, 2)
   , (3, '<', 'Less Than', 1, 3)
   , (4, '>=', 'Greater Than or Equal To', 1, 4)
   , (5, '<=', 'Less Than or Equal To', 1, 5)
   , (6, '<>', 'Not Equal To', 1, 6)
   , (7, 'Increasing', 'Increasing', 2, 7)
   , (8, 'Decreasing', 'Decreasing', 2, 8)
   , (9, 'Transient', 'Transient', 3, 9)
   , (10, 'Not Transient', 'Not Transient', 3, 10)
   ) AS Source([LogicOperatorTypeID], [LogicOperatorTypeAbbrev], [LogicOperatorTypeDesc],
				[LogicOperatorCategoryTypeID], [DisplayOrder])
ON (TARGET.[LogicOperatorTypeID] = Source.[LogicOperatorTypeID])
WHEN MATCHED THEN
	UPDATE SET
	[LogicOperatorTypeID] = Source.[LogicOperatorTypeID]
	, [LogicOperatorTypeAbbrev] = Source.[LogicOperatorTypeAbbrev]
    , [LogicOperatorTypeDesc] = Source.[LogicOperatorTypeDesc]
	, [LogicOperatorCategoryTypeID] = Source.[LogicOperatorCategoryTypeID]
	, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LogicOperatorTypeID], [LogicOperatorTypeAbbrev], [LogicOperatorTypeDesc],
				[LogicOperatorCategoryTypeID], [DisplayOrder])
	VALUES(Source.[LogicOperatorTypeID], Source.[LogicOperatorTypeAbbrev], Source.[LogicOperatorTypeDesc],
				Source.[LogicOperatorCategoryTypeID], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;


MERGE INTO [Monitoring].[tNDAlertStatusPriorityType] AS TARGET
USING (VALUES
	(1, 'High', 'High', 1)
	, (2, 'Low', 'Low', 2)
) AS SOURCE([AlertStatusPriorityTypeID], [AlertStatusPriorityTypeAbbrev], [AlertStatusPriorityTypeDesc], [DisplayOrder])
ON (TARGET.[AlertStatusPriorityTypeID] = Source.[AlertStatusPriorityTypeID])
WHEN MATCHED THEN
	UPDATE SET
	[AlertStatusPriorityTypeID] = Source.[AlertStatusPriorityTypeID]
	, [AlertStatusPriorityTypeAbbrev] = Source.[AlertStatusPriorityTypeAbbrev]
	, [AlertStatusPriorityTypeDesc] = Source.[AlertStatusPriorityTypeDesc]
	, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AlertStatusPriorityTypeID], [AlertStatusPriorityTypeAbbrev], [AlertStatusPriorityTypeDesc], [DisplayOrder])
	VALUES (Source.[AlertStatusPriorityTypeID], Source.[AlertStatusPriorityTypeAbbrev], Source.[AlertStatusPriorityTypeDesc], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;


MERGE INTO [Monitoring].[tNDAlertStatusType] AS TARGET
USING (VALUES
	(1, 'HHA', 'Hi-Hi Alert', 1)
	, (2, 'LLA', 'Low-Low Alert', 1)
	, (3, 'AAF', 'Area Alert - Fast Response', 1)
	, (4, 'WOR', 'Watch Override', 1)
	, (5, 'IOR', 'Ignore Override', 1)
	, (6, 'AAS', 'Area Alert - Slow Response', 2)
	, (7, 'FRZ', 'Frozen Data', 2)
	, (8, 'FRQ', 'Anomaly Frequency', 2)
	, (9, 'OSC', 'Oscillations', 2)
) AS SOURCE([AlertStatusTypeID], [AlertStatusTypeAbbrev], [AlertStatusTypeDesc], [AlertStatusPriorityTypeID])
ON (TARGET.[AlertStatusTypeID] = Source.[AlertStatusTypeID])
WHEN MATCHED THEN
	UPDATE SET
	[AlertStatusTypeID] = Source.[AlertStatusTypeID]
	, [AlertStatusTypeAbbrev] = Source.[AlertStatusTypeAbbrev]
	, [AlertStatusTypeDesc] = Source.[AlertStatusTypeDesc]
	, [AlertStatusPriorityTypeID] = Source.[AlertStatusPriorityTypeID]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AlertStatusTypeID], [AlertStatusTypeAbbrev], [AlertStatusTypeDesc], [AlertStatusPriorityTypeID])
	VALUES (Source.[AlertStatusTypeID], Source.[AlertStatusTypeAbbrev], Source.[AlertStatusTypeDesc], Source.[AlertStatusPriorityTypeID])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;

MERGE INTO [Monitoring].[tNDModelLockReasonType] AS TARGET
USING (VALUES
(1, 'Pending Changes', 'Pending Changes', 1)
, (2, 'Building', 'Building', 2)
, (3, 'QueueForBuild', 'Queue For Build', 3)
, (4, 'AutoRetrain', 'Scheduled Retrain', 4)
, (5, 'ND Migration', 'ND Migration',5)
) AS SOURCE([LockReasonID], [LockReasonAbbrev], [LockReasonDesc], [DisplayOrder])
ON (TARGET.[LockReasonID] = Source.[LockReasonID])
WHEN MATCHED THEN
	UPDATE SET
	[LockReasonID] = Source.[LockReasonID]
	, [LockReasonAbbrev] = Source.[LockReasonAbbrev]
	, [LockReasonDesc] = Source.[LockReasonDesc]
	, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LockReasonID], [LockReasonAbbrev], [LockReasonDesc], [DisplayOrder])
	VALUES (Source.[LockReasonID], Source.[LockReasonAbbrev], Source.[LockReasonDesc], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;

MERGE INTO [Monitoring].[tNDBatchType] AS TARGET
USING (VALUES
(1, 'Edit', 'Edit')
) AS SOURCE ([BatchTypeId], [BatchAbbrev], [BatchDesc])
ON (TARGET.[BatchTypeId] = Source.[BatchTypeId])
WHEN MATCHED THEN
	UPDATE SET
	[BatchTypeId] = Source.[BatchTypeId]
	, [BatchAbbrev] = Source.[BatchAbbrev]
	, [BatchDesc] = Source.[BatchDesc]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([BatchTypeId], [BatchAbbrev], [BatchDesc])
	VALUES (Source.[BatchTypeId], Source.[BatchAbbrev], Source.[BatchDesc])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;

MERGE INTO [Monitoring].[tModelRecInputCommentType] AS TARGET
USING (VALUES
	(1, 'Added for Fossil Fuel Units', 1, 1)
	,(2, 'Added for Coal Plant', 1, 1)
	,(3, 'Added for Gas Turbines and Combined Cycles', 1, 1)
	,(4, 'Added for WasteWater Assets', 1, 1)
	,(5, 'Added for Wind Turbines', 1, 1)
	,(6, 'Added for Ammonia plant (MH, PRH)', 1, 1)
	,(7, 'Added with Ammonia plant with steam turbine driven compressor (MH, PRH)', 1, 1)
	,(8, 'Added with Ammonia plant as compliment to existing inputs (MH, PRH)', 1, 1)
	,(9, 'Added for Ammonia plant with steam turbine driven compressor (MH, PRH)', 1, 1)
	,(10, 'Added for Ammonia plant as new model (MH, PRH)', 1, 1)
	,(11, 'Added with Ammonia plant as new model (MH, PRH)', 1, 1)
) AS Source ([CommentTypeID],[CommentDesc],[CreatedByUserID],[ChangedByUserID])
ON (Target.[CommentTypeID] = Source.[CommentTypeID])
WHEN MATCHED THEN
	UPDATE SET
	[CommentTypeID] = Source.[CommentTypeID]
	,[CommentDesc] = Source.[CommentDesc]
	,[CreatedByUserID] = Source.[CreatedByUserID]
	,[ChangedByUserID] = Source.[ChangedByUserID]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([CommentTypeID],[CommentDesc],[CreatedByUserID],[ChangedByUserID])
	VALUES (Source.[CommentTypeID],Source.[CommentDesc],Source.[CreatedByUserID],Source.[ChangedByUserID])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;