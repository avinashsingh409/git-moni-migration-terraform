﻿PRINT 'Populating ArcFlash.tHazardCategoryType';
MERGE INTO ArcFlash.tHazardCategoryType AS TARGET USING(VALUES
(-99,	'Dangerous',	'DANGEROUS!'),
(0,	'Category0',	'Category 0'),
(1,	'Category1',	'Category 1'),
(2,	'Category2',	'Category 2'),
(3,	'Category3',	'Category 3'),
(4,	'Category4',	'Category 4'),
(5,	'Category5',	'Category 5'),
(99,'Unknown','Unknown'))
AS SOURCE(HazardCategoryTypeID,HazardCategoryName,HazardCategoryDesc)
ON Target.HazardCategoryTypeID= Source.HazardCategoryTypeID
WHEN MATCHED THEN UPDATE SET HazardCategoryName=Source.HazardCategoryName, HazardCategoryDesc=Source.HazardCategoryDesc
WHEN NOT MATCHED BY TARGET THEN 
INSERT (HazardCategoryTypeID,HazardCategoryName,HazardCategoryDesc)
VALUES (HazardCategoryTypeID,HazardCategoryName,HazardCategoryDesc);
GO

--MERGE INTO ArcFlash.tConfigurationType AS TARGET USING(VALUES
--(1,	1,	'ALL',	'All Configurations Worst AF Bus'),
--(2,	1,	'NOPARALLEL',	'No Parallel Sources Tie Config Worst AF Bus'),
--(3,	1,	'NOTIE',	'No Tie Breaker Closed Config Worst AF Bus'),
--(4,	0,	'MITALL',	'All Configurations Worst AF Bus'),
--(5,	0,	'MITNOPARALLEL',	'No Parallel Sources Tie Config Worst AF Bus'),
--(6,	0,	'MITNOTIE',	'No Tie Breaker Closed Config Worst AF Bus')
--)
--AS SOURCE(ConfigurationTypeID,Base,ConfigurationName,ConfigurationDesc)
--ON Target.ConfigurationTypeID= Source.ConfigurationTypeID
--WHEN MATCHED THEN UPDATE SET Base=Source.Base, ConfigurationName=Source.ConfigurationName,ConfigurationDesc=Source.ConfigurationDesc
--WHEN NOT MATCHED BY TARGET THEN 
--INSERT (ConfigurationTypeID,Base,ConfigurationName,ConfigurationDesc)
--VALUES (ConfigurationTypeID,Base,ConfigurationName,ConfigurationDesc);
--GO