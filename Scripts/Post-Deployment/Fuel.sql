﻿MERGE INTO Fuel.tAdditiveChemicalType AS TARGET USING (VALUES 
(1,'Inert','Inert')
,(2,'Al2O3','Al2O3')
,(3,'BaO','BaO')
,(4,'CaBr2','CaBr2')
,(5,'CaO','CaO')
,(6,'Ca(OH)2','Ca(OH)2')
,(7,'CaCO3','CaCO3')
,(8,'CaMg(CO3)2','CaMg(CO3)2')
,(9,'CaSO4','CaSO4')
,(10,'FeO','FeO')
,(11,'Fe2O3','Fe2O3')
,(12,'Fe3O4','Fe3O4')
,(13,'H2O','H2O')
,(14,'K2O','K2O')
,(15,'KI','KI')
,(16,'MgO','MgO')
,(17,'Mg(OH)2','Mg(OH)2')
,(18,'MgCO3','MgCO3')
,(19,'Mn3O4','Mn3O4')
,(20,'NaBr','NaBr')
,(21,'NaOH','NaOH')
,(22,'NaHCO3','NaHCO3')
,(23,'Na2O','Na2O')
,(24,'Na2CO3','Na2CO3')
,(25,'Na2SO4','Na2SO4')
,(26,'P2O5','P2O5')
,(27,'SO3','SO3')
,(28,'SiO2','SiO2')
,(29,'SrO','SrO')
,(30,'TiO2','TiO2')
)
AS SOURCE([AdditiveChemicalTypeID],[AdditiveChemicalTypeDesc],[AdditiveChemicalTypeAbbrev])
ON Target.[AdditiveChemicalTypeID]= Source.[AdditiveChemicalTypeID]
WHEN MATCHED THEN UPDATE SET [AdditiveChemicalTypeDesc]=Source.[AdditiveChemicalTypeDesc],[AdditiveChemicalTypeAbbrev]=Source.[AdditiveChemicalTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AdditiveChemicalTypeID],[AdditiveChemicalTypeDesc],[AdditiveChemicalTypeAbbrev])
VALUES ([AdditiveChemicalTypeID],[AdditiveChemicalTypeDesc],[AdditiveChemicalTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Fuel.tAdditiveType AS TARGET USING (VALUES 
(1,'Customized Additive','Customized Additive',1)
,(2,'Calcium Hydroxide','Calcium Hydroxide',1)
,(3,'Dolomite','Dolomite',1)
,(4,'Limestone','Limestone',1)
,(5,'Lime','Lime',1)
,(6,'Magnesium Carbonate','Magnesium Carbonate',1)
,(7,'Magnesium Oxide','Magnesium Oxide',1)
,(8,'Magnesium Hydroxide','Magnesium Hydroxide',1)
,(9,'Sodium Hydroxide','Sodium Hydroxide',1)
,(10,'Sodium Sulfate','Sodium Sulfate',1)
,(11,'Trona','Trona',1)
,(12,'ChemMod(MercSorb)','ChemMod(MercSorb)',2)
,(13,'ChemMod(S-Sorb)','ChemMod(S-Sorb)',1)
,(14,'CyClean(A)','CyClean(A)',1)
,(15,'CyClean(B)','CyClean(B)',2)
)
AS SOURCE([AdditiveTypeID],[AdditiveTypeDesc],[AdditiveTypeAbbrev],[DefaultForm])
ON Target.[AdditiveTypeID]= Source.[AdditiveTypeID]
WHEN MATCHED THEN UPDATE SET [AdditiveTypeDesc]=Source.[AdditiveTypeDesc],[AdditiveTypeAbbrev]=Source.[AdditiveTypeAbbrev],[DefaultForm]=Source.[DefaultForm]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AdditiveTypeID],[AdditiveTypeDesc],[AdditiveTypeAbbrev],[DefaultForm])
VALUES ([AdditiveTypeID],[AdditiveTypeDesc],[AdditiveTypeAbbrev],[DefaultForm])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Fuel.tFuelQualityType AS TARGET USING (VALUES 
(1,'Dashboard Imported','Dashboard',0)
,(2,'Thermo Online Analysis','Thermo',1)
,(3,'Right Angle Analysis','Right',1)
,(4,'Blended Analysis','Blend',0)
,(5,'Fuel Tracker Imported','FTImport',1)
,(6,'Gas','Gas',0)
)
AS SOURCE([FuelQualityTypeID],[FuelQualityTypeDesc],[FuelQualityTypeAbbrev],[ShowInFuelTracker])
ON Target.[FuelQualityTypeID]= Source.[FuelQualityTypeID]
WHEN MATCHED THEN UPDATE SET [FuelQualityTypeDesc]=Source.[FuelQualityTypeDesc],[FuelQualityTypeAbbrev]=Source.[FuelQualityTypeAbbrev],[ShowInFuelTracker]=Source.[ShowInFuelTracker]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([FuelQualityTypeID],[FuelQualityTypeDesc],[FuelQualityTypeAbbrev],[ShowInFuelTracker])
VALUES ([FuelQualityTypeID],[FuelQualityTypeDesc],[FuelQualityTypeAbbrev],[ShowInFuelTracker])
WHEN NOT MATCHED BY SOURCE THEN DELETE;