﻿MERGE INTO [Criteria].[tCoTemporalType] AS Target
USING (VALUES
	(1, 'Minute', 'minute(s)', 1)
	, (2, 'Hour', 'hour(s)', 2)
	, (3, 'Day', 'day(s)', 3)
	, (4, 'Month', 'month(s)', 4)
	, (5, 'Quarter', 'quarter(s)', 5)
	, (6, 'Year', 'year(s)', 6)
	,(7, 'Week','week(s)',7)
	,(8, 'Season','season(s)',8)
) AS Source ([CoTemporalTypeID], [Title], [Abbrev], [DisplayOrder])
ON (Target.[CoTemporalTypeID] = Source.[CoTemporalTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[Title] = Source.[Title]
		, [Abbrev] = Source.[Abbrev]
		, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([CoTemporalTypeID], [Title], [Abbrev], [DisplayOrder])
	VALUES (Source.[CoTemporalTypeID], Source.[Title], Source.[Abbrev], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;


MERGE INTO [Criteria].[tCoDFRelativeReferenceType] AS Target
USING (VALUES
	(1, 'Now', 'Relative to Now', 1)
	, (2, 'Selected End Time', 'Relative to Time Selector End Time', 2)
	, (3, 'Selected Start Time', 'Relative to Time Selector Start Time', 3)
) AS Source ([CoDFRelativeReferenceTypeID], [Title], [Abbrev], [DisplayOrder])
ON (Target.[CoDFRelativeReferenceTypeID] = Source.[CoDFRelativeReferenceTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[Title] = Source.[Title]
		, [Abbrev] = Source.[Abbrev]
		, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([CoDFRelativeReferenceTypeID], [Title], [Abbrev], [DisplayOrder])
	VALUES (Source.[CoDFRelativeReferenceTypeID], Source.[Title], Source.[Abbrev], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;


MERGE INTO [Criteria].[tCoDFRelativeShiftType] AS Target
USING (VALUES
	(1, 'Reference', 'Reference', 1)
	, (2, 'Start of Hour', 'Start of Hour', 2)
	, (3, 'Start of Day', 'Start of Day', 3)
	, (4, 'Start of Month', 'Start of Month', 4)
	, (5, 'Start of Quarter', 'Start of Quarter', 5)
	, (6, 'Start of Year', 'Start of Year', 6)
) AS Source ([CoDFRelativeShiftTypeID], [Title], [Abbrev], [DisplayOrder])
ON (Target.[CoDFRelativeShiftTypeID] = Source.[CoDFRelativeShiftTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[Title] = Source.[Title]
		, [Abbrev] = Source.[Abbrev]
		, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([CoDFRelativeShiftTypeID], [Title], [Abbrev], [DisplayOrder])
	VALUES (Source.[CoDFRelativeShiftTypeID], Source.[Title], Source.[Abbrev], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;


MERGE INTO [Criteria].[tCoDFTimeFilterType] AS Target
USING (VALUES
	(1, 'Fixed', 'Fixed', 1, 1)
	, (2, 'Relative Span Offset', 'Relative Span Offset', 1, 2)
	, (3, 'Exclude Days', 'Exclude Days', 0, 3)
	, (4, 'Exclude Hours', 'Exclude Hours', 0, 4)
) AS Source ([CoDFTimeFilterTypeID], [Title], [Abbrev], [IsPreDataRetrieval], [DisplayOrder])
ON (Target.[CoDFTimeFilterTypeID] = Source.[CoDFTimeFilterTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[Title] = Source.[Title]
		, [Abbrev] = Source.[Abbrev]
		, [IsPreDataRetrieval] = Source.[IsPreDataRetrieval]
		, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([CoDFTimeFilterTypeID], [Title], [Abbrev], [IsPreDataRetrieval], [DisplayOrder])
	VALUES (Source.[CoDFTimeFilterTypeID], Source.[Title], Source.[Abbrev], Source.[IsPreDataRetrieval], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;

MERGE INTO [Criteria].[tCoDSSourceType] AS TARGET
USING (VALUES
	(1, 'Asset', 'Assets', 1)
	, (2, 'Tag', 'Tags', 2)
) AS Source([CoDSSourceTypeID], [Title], [Abbrev], [DisplayOrder])
ON (Target.[CoDSSourceTypeID] = Source.[CoDSSourceTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[Title] = Source.[Title]
		, [Abbrev] = Source.[Abbrev]
		, [DisplayOrder] = Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([CoDSSourceTypeID], [Title], [Abbrev], [DisplayOrder])
	VALUES (Source.[CoDSSourceTypeID], Source.[Title], Source.[Abbrev], Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;