﻿SET IDENTITY_INSERT [AccessControl].[tAccessType] ON
GO

MERGE INTO AccessControl.tAccessType AS TARGET USING (VALUES
(1,1,'View')
,(2,1,'Edit')
,(4,1,'Add')
,(5,1,'Delete')
,(6,2,'SetConsensus')
,(7,2,'SetSensitivity')
,(8,2,'SetPublic')
,(9,1,'BVEdit')
)
AS SOURCE([SecurityAccessID],[SecurityAccessGroupID],[Description])
ON Target.[SecurityAccessID]= Source.[SecurityAccessID]
WHEN MATCHED THEN UPDATE SET [SecurityAccessGroupID]=Source.[SecurityAccessGroupID],[Description]=Source.[Description]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([SecurityAccessID],[SecurityAccessGroupID],[Description])
VALUES ([SecurityAccessID],[SecurityAccessGroupID],[Description])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [AccessControl].[tAccessType] OFF
GO


SET IDENTITY_INSERT [AccessControl].[tAccessGroup] ON
GO

MERGE INTO AccessControl.tAccessGroup AS TARGET USING (VALUES
(1,'Basic Permissions')
,(2,'Consensus and Sensitivity')
)
AS SOURCE([SecurityAccessGroupID],[Description])
ON Target.[SecurityAccessGroupID]= Source.[SecurityAccessGroupID]
WHEN MATCHED THEN UPDATE SET [Description]=Source.[Description]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([SecurityAccessGroupID],[Description])
VALUES ([SecurityAccessGroupID],[Description])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [AccessControl].[tAccessGroup] OFF
GO

SET IDENTITY_INSERT [AccessControl].[tCustomer] ON
GO
PRINT 'Populating AccessControl.tCustomer';
--temporarily remove foreign key constraints
ALTER TABLE [AccessControl].[tCustomer] NOCHECK CONSTRAINT FK_tUser_SecurityUserID_tCustomer_ChangedBy
ALTER TABLE [AccessControl].[tCustomer] NOCHECK CONSTRAINT FK_tUser_SecurityUserID_tCustomer_CreatedBy
	GO

INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(1,NULL,'98DD729C-C8E5-4D6C-AD9B-141913734AD9', 'Atonix', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(2,1,'C0E51553-77D6-4C16-8FC9-5B87F0FDC049', 'None Specified', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(3,1,'268149f6-8d8c-4d1a-8b7e-f21dd213d2b3', 'Customer', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(4,3,'2c2177c1-5344-493b-a47c-e4dd48c885ce', 'ChildCustomerA', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(5,3,'c5b9a448-6bd8-4686-9b35-5588a2a1d612', 'ChildCustomerB', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(6,4,'76db9feb-edd6-4172-b145-ca446a6aeef1', 'GrandChildCustomerAA', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(7,4,'dc17f07e-f7fe-493d-af21-382996c81b39', 'GrandChildCustomerAB', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(8,5,'539bd40d-0212-4148-aba7-1af206d74a4b', 'GrandChildCustomerBA', 1, 1);
INSERT INTO [AccessControl].[tCustomer] ([CustID], [ParentCustID], [CustomerID], [Name], [CreatedBy], [ChangedBy])
VALUES
(9,5,'a97e9695-bcee-4f11-bdc7-996fa10332b7', 'GrandChildCustomerBB', 1, 1);
GO
SET IDENTITY_INSERT [AccessControl].[tCustomer] OFF
GO

SET IDENTITY_INSERT [AccessControl].[tUser] ON
GO

PRINT 'Populating AccessControl.tUser';
MERGE INTO [AccessControl].[tUser] AS Target
USING (VALUES
(1, 'TEST','TEST@test.com',1,1,'98DD729C-C8E5-4D6C-AD9B-141913734AD9')
)
AS Source([SecurityUserID], [Username], [Email],[CreatedByUserId],[ChangedByUserId],[CustomerID])
ON Target.[SecurityUserID] = Source.[SecurityUserID]
WHEN MATCHED THEN
UPDATE SET [Username]=Source.[Username],
           [Email]=Source.[Email]
WHEN NOT MATCHED BY TARGET THEN
INSERT([SecurityUserID],[Username],[Email],[CreatedByUserId],[ChangedByUserId],[CustomerID])
VALUES([SecurityUserID],[Username],[Email],[CreatedByUserId],[ChangedByUserId],[CustomerID])
WHEN NOT MATCHED BY SOURCE THEN
DELETE;

GO

SET IDENTITY_INSERT [AccessControl].[tUser] OFF
GO
--reinstate foreign key constraints
ALTER TABLE [AccessControl].[tCustomer] WITH CHECK CHECK CONSTRAINT FK_tUser_SecurityUserID_tCustomer_ChangedBy
ALTER TABLE [AccessControl].[tCustomer] WITH CHECK CHECK CONSTRAINT FK_tUser_SecurityUserID_tCustomer_CreatedBy
GO

SET IDENTITY_INSERT [AccessControl].[tCustomerEmailDomain] ON
GO

PRINT 'Populating AccessControl.tCustomerEmailDomain';
MERGE INTO [AccessControl].[tCustomerEmailDomain] AS Target
USING (VALUES
(1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9', 'test.com'),
(2, 'C0E51553-77D6-4C16-8FC9-5B87F0FDC049', 'hotmail.com'),
(3, 'C0E51553-77D6-4C16-8FC9-5B87F0FDC0499', 'gmail.com'),
(4, '268149f6-8d8c-4d1a-8b7e-f21dd213d2b3', 'customer.com'),
(5, '2c2177c1-5344-493b-a47c-e4dd48c885ce', 'childcustomera.com'),
(6, 'c5b9a448-6bd8-4686-9b35-5588a2a1d6123', 'childcustomerb.com'),
(7, '76db9feb-edd6-4172-b145-ca446a6aeef1', 'grandchildcustomeraa.com'),
(8, 'dc17f07e-f7fe-493d-af21-382996c81b39', 'grandchildcustomerab.com'),
(9, '539bd40d-0212-4148-aba7-1af206d74a4b', 'grandchildcustomerba.com'),
(10, 'a97e9695-bcee-4f11-bdc7-996fa10332b7', 'grandchildcustomerbb.com')
)
AS Source([ID], [CustomerID], [EmailDomain])
ON Target.[ID] = Source.[ID]
WHEN MATCHED THEN
UPDATE SET [CustomerID]=Source.[CustomerID],
		   [EmailDomain]=Source.[EmailDomain]
WHEN NOT MATCHED BY TARGET THEN
INSERT([ID], [CustomerID], [EmailDomain])
VALUES([ID], [CustomerID], [EmailDomain])
WHEN NOT MATCHED BY SOURCE THEN
DELETE;

GO

SET IDENTITY_INSERT [AccessControl].[tCustomerEmailDomain] OFF
GO

SET IDENTITY_INSERT [AccessControl].[tIdentityProvider] ON
GO

PRINT 'Populating AccessControl.tIdentityProvider';
MERGE INTO [AccessControl].[tIdentityProvider] AS Target
USING (VALUES
(1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9', 'TestIdentityProvider', 1)
)
AS Source([ID], [CustomerID], [IdentityProvider], [Enabled])
ON Target.[ID] = Source.[ID]
WHEN MATCHED THEN
UPDATE SET [CustomerID]=Source.[CustomerID],
		   [IdentityProvider]=Source.[IdentityProvider],
		   [Enabled]=Source.[Enabled]
WHEN NOT MATCHED BY TARGET THEN
INSERT([ID], [CustomerID], [IdentityProvider], [Enabled])
VALUES([ID], [CustomerID], [IdentityProvider], [Enabled])
WHEN NOT MATCHED BY SOURCE THEN
DELETE;

GO

SET IDENTITY_INSERT [AccessControl].[tIdentityProvider] OFF
GO


PRINT 'Populating AccessControl.tResource';
--Adding the Resources
DECLARE @guiPk INT;
DECLARE @dataPk INT;
DECLARE @plansPk INT;
DECLARE @fuelTrackerPk INT;
DECLARE @appContextNodePk INT;
DECLARE @arcFlashPk INT;	
DECLARE @issuesPk INT;
DECLARE @uiPk INT;
DECLARE @taskCenterPk INT;
DECLARE @issuesMgmtPk INT;
DECLARE @viewExplorerPk INT;
DECLARE @dashboardPk INT;
DECLARE @alertsPk INT;
DECLARE @dataExplorerPk INT;
DECLARE @utilityPk INT;
DECLARE @opModeConfigPk INT;
DECLARE @assetConfigPk INT;
DECLARE @tagConfigPk INT;
DECLARE @pinConfigPk INT;
DECLARE @modelConfigPk INT;
DECLARE @canEditAllPk INT;
DECLARE @eventsPk INT;
DECLARE @featurePk INT;
DECLARE @alphaPk INT;
DECLARE @betaPk INT;
DECLARE @alpha2Pk INT;
DECLARE @beta2Pk INT;
DECLARE @discussionPk INT;
DECLARE @userAdminPk INT;
DECLARE @customerManagementPk INT;

EXEC @guiPk = [AccessControl].[spInsertResource] @parentID = 0, @resourceName = N'GUI';
EXEC @dataPk = [AccessControl].[spInsertResource] @parentID = 0, @resourceName = N'Data';
EXEC @fuelTrackerPk = [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'FuelTracker' ;
EXEC @appContextNodePk = [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'AppContext' ;
EXEC @plansPk = [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'Plans' ;

EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'Emissions' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'FuelForward' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'HistoricalFuels' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'Executive' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'CreateEdit' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'VOMUCD' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'Administration' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'CurrentPerformance' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'MDAlert' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'TrueUp' ;
EXEC @canEditAllPk = [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'CanEditAll';
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'WorkflowAdminConfiguration' ;
EXEC [AccessControl].[spInsertResource] @parentID = @guiPk, @resourceName = N'EventsDashboardApp' ;

EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'Scenario' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'Forecasts' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'LimitsAndTargets' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'OfficialHeatRates' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'FuelPro' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'FuelSource' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'CQIExperience' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'Assets' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'AssetRelatedInfo' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'CriteriaObject' ;
EXEC [AccessControl].[spInsertResource] @parentID = @dataPk, @resourceName = N'AdHocNavigator' ;

EXEC [AccessControl].[spInsertResource] @parentID = @plansPk, @resourceName = N'Gen' ;
EXEC [AccessControl].[spInsertResource] @parentID = @plansPk, @resourceName = N'Fuel' ;
EXEC [AccessControl].[spInsertResource] @parentID = @plansPk, @resourceName = N'Unit' ;

EXEC [AccessControl].[spInsertResource] @parentID = @fuelTrackerPk, @resourceName = N'DeliveryDates' ;

PRINT 'Populating Security Resources';
--If SecurityResourceID is missing then add the resource to tResource

EXEC AccessControl.spInsertResource @appContextNodePk, 'Asset360'
EXEC AccessControl.spInsertResource @appContextNodePk, 'SolarPV'
EXEC AccessControl.spInsertResource @appContextNodePk, 'MATSLite'
EXEC AccessControl.spInsertResource @appContextNodePk, 'PowerGenAnalyst'
EXEC AccessControl.spInsertResource @appContextNodePk, 'EvrnDashboard'
EXEC AccessControl.spInsertResource @appContextNodePk, 'CSAPR'
EXEC AccessControl.spInsertResource @appContextNodePk, 'ProjPrior'
EXEC AccessControl.spInsertResource @appContextNodePk, 'ScenarioPlanner'
EXEC AccessControl.spInsertResource @appContextNodePk, 'APPerfAnalyst'
EXEC AccessControl.spInsertResource @appContextNodePk, 'RegAdmin'
EXEC AccessControl.spInsertResource @appContextNodePk, 'ScenarioMonitor'
EXEC AccessControl.spInsertResource @appContextNodePk, 'SystemHealth'
EXEC AccessControl.spInsertResource @appContextNodePk, 'WaterCHP'
EXEC AccessControl.spInsertResource @appContextNodePk, 'WaterMeter'
EXEC AccessControl.spInsertResource @appContextNodePk, 'WaterReclamation'
EXEC @arcFlashPk = AccessControl.spInsertResource @appContextNodePk, 'ArcFlash'
EXEC AccessControl.spInsertResource @arcFlashPk, 'ArcFlashEquipStates'
EXEC AccessControl.spInsertResource @arcFlashPk, 'ArcFlashImport'
EXEC AccessControl.spInsertResource @appContextNodePk, 'IssuesManagement'
EXEC @issuesPk = AccessControl.spInsertResource @dataPk, 'Issues'
EXEC AccessControl.spInsertResource @issuesPk, 'ImpactScenario'
EXEC AccessControl.spInsertResource @appContextNodePk, 'WaterTreatment'
EXEC AccessControl.spInsertResource @appContextNodePk, 'Microgrid'
EXEC AccessControl.spInsertResource @appContextNodePk, 'InvestmentAccelerator'
EXEC AccessControl.spInsertResource @appContextNodePk, 'UserAdmin'
EXEC AccessControl.spInsertResource @appContextNodePk, 'Alerts'
EXEC AccessControl.spInsertResource @appContextNodePk, 'SolarAnalyst'
EXEC AccessControl.spInsertResource @appContextNodePk, 'WaterAnalyst'
EXEC AccessControl.spInsertResource @appContextNodePk, 'IssuesAnalyst'
EXEC AccessControl.spInsertResource @appContextNodePk, 'AirPermits'
EXEC AccessControl.spInsertResource @appContextNodePk, 'SludgeAnalyst'
EXEC AccessControl.spInsertResource @appContextNodePk, 'OptionsExplorer'
EXEC AccessControl.spInsertResource @appContextNodePk, 'RiskMatrix'
EXEC @viewExplorerPk = AccessControl.spInsertResource @appContextNodePk, 'ViewExplorer'
EXEC AccessControl.spInsertResource @appContextNodePk, 'SmartES'
EXEC AccessControl.spInsertResource @appContextNodePk, 'NewAssetExplorer'
EXEC AccessControl.spInsertResource @guiPk, 'Alerts Configuration'
EXEC AccessControl.spInsertResource @dataPk, 'Process Data'
EXEC AccessControl.spInsertResource @dataPk, 'PDTags' ;

DECLARE @securityResource INT
DECLARE @singleLoginResource INT

PRINT 'Creating Security Resource'
EXEC @securityResource = AccessControl.spInsertResource 0, 'Security'

PRINT 'Creating Single Login Resource'
EXEC @singleLoginResource = AccessControl.spInsertResource @securityResource, 'SingleLogin'

PRINT 'Creating User Interaction Resource'
EXEC AccessControl.spInsertResource @securityResource, 'InactivityTimeout'

-- GetChildResourceRights Testing
EXEC @uiPk = [AccessControl].[spInsertResource] @parentID = 0, @resourceName = N'UI';
EXEC @taskCenterPk = [AccessControl].[spInsertResource] @parentID = @uiPk, @resourceName = N'TaskCenter';
EXEC [AccessControl].[spInsertResource] @parentID = @taskCenterPk, @resourceName = N'ChildResourceTesting';
EXEC @issuesMgmtPk = [AccessControl].[spInsertResource] @parentID = @taskCenterPk, @resourceName = N'IssuesManagement';
EXEC @discussionPk = [AccessControl].[spInsertResource] @parentID = @issuesMgmtPk, @resourceName = N'Discussion';
EXEC @dashboardPk = [AccessControl].[spInsertResource] @taskCenterPk, 'Dashboard';
EXEC @alertsPk = [AccessControl].[spInsertResource] @taskCenterPk, 'Alerts';
EXEC @dataExplorerPk = [AccessControl].[spInsertResource] @taskCenterPk, 'DataExplorer';
EXEC @utilityPk = [AccessControl].[spInsertResource] @uiPk, 'Utility';
EXEC @featurePk = [AccessControl].[spInsertResource] @uiPk, 'Feature';
EXEC @opModeConfigPk = [AccessControl].[spInsertResource] @utilityPk, 'OpModeConfig';
EXEC @assetConfigPk = [AccessControl].[spInsertResource] @utilityPk, 'AssetConfig';
EXEC @tagConfigPk = [AccessControl].[spInsertResource] @utilityPk, 'TagConfig';
EXEC @pinConfigPk = [AccessControl].[spInsertResource] @utilityPk, 'PinConfig';
EXEC @modelConfigPk = [AccessControl].[spInsertResource] @utilityPk, 'ModelConfig';
EXEC @eventsPk = [AccessControl].[spInsertResource] @utilityPk, 'Events';
EXEC @userAdminPk = [AccessControl].[spInsertResource] @utilityPk, 'UserAdmin';
EXEC @alphaPk = [AccessControl].[spInsertResource] @featurePk, 'Alpha';
EXEC @betaPk = [AccessControl].[spInsertResource] @featurePk, 'Beta';
EXEC @alpha2Pk = [AccessControl].[spInsertResource] @featurePk, 'Alpha2';
EXEC @beta2Pk = [AccessControl].[spInsertResource] @featurePk, 'Beta2';
EXEC @customerManagementPk = [AccessControl].[spInsertResource] @userAdminPk, 'CustomerManagement';
--GO

-- Roles start
DECLARE @rolePks Base.tpIntList;
DECLARE @rootRolePk INT;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] 0, 'Root', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @rootRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;

-- Productization Roles start
DECLARE
	@atxOIAnalystRolePk INT,
	@atxOILiteAnalystRolePk INT,
	@atxOIStaffRolePk INT,
	@atxOIAdminRolePk INT,
	@atxOIExplorerAnalystRolePk INT,
	@atxOIInsightAnalystRolePk INT,
	@atxOIResolveCoordinatorRolePk INT,
	@atxOIResolveStaffRolePk INT,
	@atxOIEventsAnalystRolePk INT,
	@atxOIAlphaRolePk INT,
	@atxOIBetaRolePk INT,
	@atxOIDiscussionManagerRolePk INT,
	@atxOIUserAdminRolePk INT,
	@atxOICustomerManagerRolePk INT,
	@atxOIAlpha2RolePk INT,
	@atxOIBeta2RolePk INT,
	@atxOITagConfigRolePk INT
;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Analyst', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOiAnalystRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Lite Analyst', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOILiteAnalystRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Staff', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIStaffRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Admin', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIAdminRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Explorer Analyst', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIExplorerAnalystRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Insight Analyst', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIInsightAnalystRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Resolve Coordinator', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIResolveCoordinatorRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Resolve Staff', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIResolveStaffRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Events Analyst', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIEventsAnalystRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Alpha', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIAlphaRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Beta', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIBetaRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Discussion Manager', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIDiscussionManagerRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI User Admin', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIUserAdminRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Customer Manager', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOICustomerManagerRolePk = (SELECT TOP 1 id FROM  @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Alpha2', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIAlpha2RolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Beta2', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOIBeta2RolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;
INSERT INTO @rolePks EXEC [AccessControl].[spInsertRole] @rootRolePk, 'AtonixOI Tag Config', 1, '98DD729C-C8E5-4D6C-AD9B-141913734AD9';
SET @atxOITagConfigRolePk = (SELECT TOP 1 id FROM @rolePks);
DELETE FROM @rolePks;

DELETE FROM [AccessControl].[tRole] WHERE SecurityRoleID = @rootRolePk;
-- Productization Roles end

-- Productization Roles Access Rules start
INSERT INTO AccessControl.tAccessRule (SecurityResourceID, SecurityRoleID, SecurityAccessID, Allowed, CreatedBy, ChangedBy)
VALUES
(@issuesPk, @atxOIAnalystRolePk, 5, 1, 1, 1),
(@issuesPk, @atxOIAnalystRolePK, 9, 1, 1, 1),
(@viewExplorerPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@alertsPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIAnalystRolePk,	2,	1,	1, 1),
(@issuesMgmtPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIAnalystRolePk,	2,	1,	1, 1),
(@dataExplorerPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@opModeConfigPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@assetConfigPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@tagConfigPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@pinConfigPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@modelConfigPk,	@atxOIAnalystRolePk,	1,	1,	1, 1),
(@issuesPk,	@atxOILiteAnalystRolePk,	5,	1,	1, 1),
(@issuesPk,	@atxOILiteAnalystRolePk,	9,	1,	1, 1),
(@viewExplorerPk,	@atxOILiteAnalystRolePk,	1,	1,	1, 1),
(@alertsPk,	@atxOILiteAnalystRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOILiteAnalystRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOILiteAnalystRolePk,	2,	1,	1, 1),
(@dashboardPk,	@atxOILiteAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOILiteAnalystRolePk,	2,	1,	1, 1),
(@dataExplorerPk,	@atxOILiteAnalystRolePk,	1,	1,	1, 1),
(@pinConfigPk,	@atxOILiteAnalystRolePk,	1,	1,	1, 1),
(@issuesPk,	@atxOIStaffRolePk,	5,	1,	1, 1),
(@issuesPk,	@atxOIStaffRolePk,	9,	1,	1, 1),
(@issuesMgmtPk,	@atxOIStaffRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIStaffRolePk,	2,	1,	1, 1),
(@dashboardPk,	@atxOIStaffRolePk,	1,	1,	1, 1),
(@dataExplorerPk,	@atxOIStaffRolePk,	1,	1,	1, 1),
(@pinConfigPk,	@atxOIStaffRolePk,	1,	1,	1, 1),
(@issuesPk,	@atxOIAdminRolePk,	5,	1,	1, 1),
(@issuesPk,	@atxOIAdminRolePk,	9,	1,	1, 1),
(@viewExplorerPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@canEditAllPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@alertsPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIAdminRolePk,	2,	1,	1, 1),
(@issuesMgmtPk,	@atxOIAdminRolePk,	9,	1,	1, 1),
(@dashboardPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIAdminRolePk,	2,	1,	1, 1),
(@dataExplorerPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@opModeConfigPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@assetConfigPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@tagConfigPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@pinConfigPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@modelConfigPk,	@atxOIAdminRolePk,	1,	1,	1, 1),
(@viewExplorerPk,	@atxOIExplorerAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIExplorerAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIExplorerAnalystRolePk,	2,	1,	1, 1),
(@dataExplorerPk,	@atxOIExplorerAnalystRolePk,	1,	1,	1, 1),
(@assetConfigPk,	@atxOIExplorerAnalystRolePk,	1,	1,	1, 1),
(@tagConfigPk,	@atxOIExplorerAnalystRolePk,	1,	1,	1, 1),
(@pinConfigPk,	@atxOIExplorerAnalystRolePk,	1,	1,	1, 1),
(@viewExplorerPk,	@atxOIInsightAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIInsightAnalystRolePk,	1,	1,	1, 1),
(@dashboardPk,	@atxOIInsightAnalystRolePk,	2,	1,	1, 1),
(@dataExplorerPk,	@atxOIInsightAnalystRolePk,	1,	1,	1, 1),
(@assetConfigPk,	@atxOIInsightAnalystRolePk,	1,	1,	1, 1),
(@tagConfigPk,	@atxOIInsightAnalystRolePk,	1,	1,	1, 1),
(@pinConfigPk,	@atxOIInsightAnalystRolePk,	1,	1,	1, 1),
(@issuesPk,	@atxOIResolveCoordinatorRolePk,	5,	1,	1, 1),
(@issuesPk,	@atxOIResolveCoordinatorRolePk,	9,	1,	1, 1),
(@canEditAllPk,	@atxOIResolveCoordinatorRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIResolveCoordinatorRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIResolveCoordinatorRolePk,	2,	1,	1, 1),
(@issuesMgmtPk,	@atxOIResolveCoordinatorRolePk,	9,	1,	1, 1),
(@assetConfigPk,	@atxOIResolveCoordinatorRolePk,	1,	1,	1, 1),
(@issuesPk,	@atxOIResolveStaffRolePk,	5,	1,	1, 1),
(@issuesPk,	@atxOIResolveStaffRolePk,	9,	1,	1, 1),
(@issuesMgmtPk,	@atxOIResolveStaffRolePk,	1,	1,	1, 1),
(@issuesMgmtPk,	@atxOIResolveStaffRolePk,	2,	1,	1, 1),
(@eventsPk,	@atxOIEventsAnalystRolePk,	1,	1,	1, 1),
(@alphaPk,	@atxOIAlphaRolePk,	1,	1,	1, 1),
(@betaPk,	@atxOIBetaRolePk,	1,	1,	1, 1),
(@discussionPk,	@atxOIDiscussionManagerRolePk,	9,	1,	1, 1),
(@userAdminPk,	@atxOIUserAdminRolePk,	1,	1,	1, 1),
(@userAdminPk,	@atxOICustomerManagerRolePk,	1,	1,	1, 1),
(@customerManagementPk,	@atxOICustomerManagerRolePk,	9,	1,	1, 1),
(@alpha2Pk,	@atxOIAlpha2RolePk,	1,	1,	1, 1),
(@beta2Pk,	@atxOIBeta2RolePk,	1,	1,	1, 1),
(@tagConfigPk,	@atxOITagConfigRolePk,	1,	1,	1, 1)
;
-- Productization Roles Access Rules start

-- Productization Roles User Role Map start
INSERT INTO AccessControl.tUserRoleMap (SecurityUserID, SecurityRoleID, CreatedBy, ChangedBy)
VALUES
(1, @atxOIUserAdminRolePk, 1, 1),
(1, @atxOICustomerManagerRolePk, 1, 1)
;
-- Productization Roles User Role Map start

-- Roles end