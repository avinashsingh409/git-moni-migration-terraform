﻿MERGE INTO [Configuration].[tAppType] AS Target
USING (VALUES
  (1, 'MDTransfer')
  , (2, 'nDAppendService')
  , (3, 'OPM2NDTransfer')
  , (4, 'nDTagSynchronization')
  , (5, 'nDModelEvaluation')
  , (6, 'Alerts Custodian')
  , (7, 'ModelRetrain')
  , (8, 'nDProcessDataJSONPump')
  , (9, 'ManualDataPumptoND')
) AS Source ([AppTypeId],[ApplicationName])
ON (Target.[AppTypeId] = Source.[AppTypeId])
WHEN NOT MATCHED BY TARGET THEN
 INSERT([AppTypeId],[ApplicationName])
 VALUES(Source.[AppTypeId],Source.[ApplicationName])
WHEN NOT MATCHED BY SOURCE THEN
 DELETE
;

MERGE INTO [Configuration].[tAppStateType] AS Target
USING (VALUES
	(1,'Pause', 'Finish current operation then pause')
	,(2,'Restart', 'Resume operation following pause')
	,(3,'Shutdown', 'Finish current operation then execute a managed shutdown')
) AS Source ([AppStateTypeID],[AppStateTypeAbbrev],[AppStateTypeDesc])
ON (Target.[AppStateTypeID] = Source.[AppStateTypeID])
WHEN NOT MATCHED BY TARGET THEN
 INSERT([AppStateTypeID],[AppStateTypeAbbrev],[AppStateTypeDesc])
 VALUES(Source.[AppStateTypeID],Source.[AppStateTypeAbbrev],Source.[AppStateTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN
 DELETE
;

declare @ApplicationName						as nvarchar(50) = 'ApplicationName';
declare @ArchiveDirectory						as nvarchar(50) = 'ArchiveDirectory';
declare @NDEndpoint								as nvarchar(50) = 'NDEndpoint';
declare @PDServerID								as nvarchar(50) = 'PDServerID';
declare @AllowCalcPoints						as nvarchar(50) = 'AllowCalcPoints';
declare @ExitNow								as nvarchar(50) = 'ExitNow';
declare @DestroyPump 							as nvarchar(50) = 'DestroyPump';
declare @MinuteInterval							as nvarchar(50) = 'MinuteInterval';
declare @MaxMinutesLimit						as nvarchar(50) = 'MaxMinutesLimit';
declare @CycleDelay								as nvarchar(50) = 'CycleDelay';
declare @AllowInactivePoints					as nvarchar(50) = 'AllowInactivePoints';
declare @UpdateWarningMinutes					as nvarchar(50) = 'UpdateWarningMinutes';
declare @PointFrequency							as nvarchar(50) = 'PointFrequency';
declare @BackfillMinutesLimit					as nvarchar(50) = 'BackfillMinutesLimit';
declare @CreatingPump							as nvarchar(50) = 'CreatingPump';
declare @LogDir 								as nvarchar(50) = 'LogDir';
declare @LogFile								as nvarchar(50) = 'LogFile';
declare @LogFileCount							as nvarchar(50) = 'LogFileCount';
declare @LogLevel								as nvarchar(50) = 'LogLevel';
declare @RunSetting								as nvarchar(50) = 'RunSetting';
declare @RunEverySeconds						as nvarchar(50) = 'RunEverySeconds';
declare @RunCP 									as nvarchar(50) = 'RunCP';
declare @RunCPStore								as nvarchar(50) = 'RunCPStore';
declare @RunSnapshot							as nvarchar(50) = 'RunSnapshot';
declare @RunSchemaChangeSnapshot 				as nvarchar(50) = 'RunSchemaChangeSnapshot';
declare @RunDPStore								as nvarchar(50) = 'RunDPStore';
declare @PumpDestroyCreate 						as nvarchar(50) = 'PumpDestroyCreate';
declare @DeleteExistingCPReleaseVariables		as nvarchar(50) = 'DeleteExistingCPReleaseVariables';
declare @ProcessExisting						as nvarchar(50) = 'ProcessExisting';
declare @SimStartExpr							as nvarchar(50) = 'SimStartExpr';
declare @SimEndExpr								as nvarchar(50) = 'SimEndExpr';
declare @OAuthUserName							as nvarchar(50) = 'OAuthUserName';
declare @OAuthUserPassword						as nvarchar(50) = 'OAuthUserPassword';
declare @CreateInactivePoints					as nvarchar(50) = 'CreateInactivePoints';
declare @ScheduleTimes							as nvarchar(50) = 'ScheduleTimes';
declare @PDServerIDs							as nvarchar(50) = 'PDServerIDs';
declare @SecurityUserEmailAddress				as nvarchar(50) = 'SecurityUserEmailAddress';
declare @TakeCount								as nvarchar(50) = 'TakeCount';
declare @LatestValuesRetryCount					as nvarchar(50) = 'LatestValuesRetryCount';
declare @UseModelNotFoundMessage				as nvarchar(50) = 'UseModelNotFoundMessage';
declare @ModelNotFoundMessage					as nvarchar(50) = 'ModelNotFoundMessage';
declare @StaleDurationHours						as nvarchar(50) = 'StaleDurationHours';
declare @BuildLagMinutes						as nvarchar(50) = 'BuildLagMinutes';
declare @RunAllChecks							as nvarchar(50) = 'RunAllChecks';
declare @RunOnce								as nvarchar(50) = 'RunOnce';
declare @nDCallbackUrl							as nvarchar(50) = 'NDCallbackUrl';
declare @BuildIntervalOverrideInDays			as nvarchar(50) = 'BuildIntervalOverrideInDays';
declare @retrain								as nvarchar(50) = 'Retrain';
declare @build									as nvarchar(50) = 'Build';
declare @includeAllQueuedForBuild				as nvarchar(50) = 'IncludeAllQueuedForBuild';
declare @maxModelsToInclude						as nvarchar(50) = 'MaxModelsToInclude';
declare @cloudWatchLoggerLogGroup				as nvarchar(50) = 'CloudWatchLoggerLogGroup';
declare @cloudWatchLoggerLogStream				as nvarchar(50) = 'CloudWatchLoggerLogStream';
declare @dataFileSizeHourLimit					as nvarchar(50) = 'DataFileSizeHourLimit';
declare @pDServerIDList							as nvarchar(50) = 'PDServerIDList';
declare @destinationPath						as nvarchar(50) = 'DestinationPath';
declare @pollingSleepTime						as nvarchar(50) = 'PollingSleepTime';
declare @s3BucketLocation						as nvarchar(50) = 'S3BucketLocation';
declare @isPaused								as nvarchar(50) = 'IsPaused';
declare @userID									as nvarchar(50) = 'UserID';
declare @lastPollTime							as nvarchar(50) = 'LastPollTime';


DECLARE @appTypeOPM2NDTransfer INT = 3;
DECLARE @appTypeNDTagSynchronization INT = 4;
DECLARE @appTypeNDModelEvaluation INT = 5;
DECLARE @appTypeAlertsCustodian INT = 6;
DECLARE @appTypeModelRetrain INT = 7;
DECLARE @appTypeNDProcessDataJSONPump INT = 8;
DECLARE @appTypeManualDataPumptoND INT = 9;



MERGE INTO [Configuration].[tAppSetting] AS Target
USING (VALUES
-- pump
(@ApplicationName, null, null)
, (@ArchiveDirectory, null, 'Archive source for data to be pumped')
, (@NDEndpoint, null, 'Endpoint target at nD')
, (@PDServerID, null, 'PDServer source id')
, (@AllowCalcPoints, null, 'Set this to 1 to allow it to load calc points from the DB and have the pump employ them')
, (@ExitNow, null, 'Set this to 1 to terminate at the completion of a cycle')
, (@DestroyPump, null, 'Set this to true to destroy the pump before exiting')
, (@MinuteInterval, null, 'Number of minutes that must be new before data is transferred.')
, (@MaxMinutesLimit, null, 'Maximum number of minutes to transfer each time.')
, (@CycleDelay, null, 'Seconds to delay the next step.  Used in multiple places like pump creation and termination.')
, (@AllowInactivePoints, null, 'Set this to false to remove PDTags that are not active.')
, (@UpdateWarningMinutes, null, null)
, (@PointFrequency, null, 'nD data point Sampling Period frequency')
, (@BackfillMinutesLimit, null, 'Number of minutes to transfer during backfill periods.')
, (@CreatingPump, null, null)
, (@LogDir, null, 'Dir to write log file.')
, (@LogFile, null, 'The file to which log messages should be written.')
, (@LogFileCount, null, 'The number of files that can be used to presever log entries before rolling over.')
, (@LogLevel, null, 'The level of logging for which the logger should display.')

-- tag sync, if not already covered by the pump
, (@RunSetting, null, '0   = run once *default; 1   = run continuously')
, (@RunEverySeconds, null, '300 *default')
, (@RunCP, null, 'Run calc points; 0  = do not run *default; 1  = run')
, (@RunCPStore, null, 'Run calc point store; 0  = do not run *default; 1  = run')
, (@RunSnapshot, null, 'Run non schema-changing snapshots; 0  = do not run *default; 1  = run')
, (@RunSchemaChangeSnapshot, null, 'Run schema-changing snapshots; 0  = do not run *default; 1  = run')
, (@RunDPStore, null, 'Run data point store; 0  = do not run *default; 1  = run')
, (@PumpDestroyCreate, null, 'Pump uses Destroy / Create to disassociate and associate calc points with schema changes; 0  = Remove points / Add points; 1  = Destroy / Create *default')
, (@DeleteExistingCPReleaseVariables, null, 'dictates whether tag sync deletes existing calc point expression and release variable if they exist should only be set to 1 if there are known cp schema changes causing the recreation of cp without a full model rebuild; 0 = no *default; 1 = yes')
, (@ProcessExisting, null, 'Set this flag to 1 if you want to release, snapshot, and reassociate all calc points that are associated with models for this PD Server')
, (@SimStartExpr, null, 'used to extract the portion of the sim function that indicates a schema change; if not set simStartExpr will default to "sim["')
, (@SimEndExpr, null, 'used to extract the portion of the sim function that indicates a schema change; if not set simEndExpr will default to "]"')
, (@OAuthUserName, null, null)
, (@OAuthUserPassword, null, null)
, (@CreateInactivePoints, null, 'Create nD data points for PDTags ExistsOnServer == 0')
, (@ScheduleTimes, null, 'semi-colon delimited list of times to be used for deciding when to run the main tag sync process')

-- model eval, if not already covered by the pump or tag sync
, (@PDServerIDs, null, null)
, (@SecurityUserEmailAddress, null, null)
, (@TakeCount, null, null)
, (@LatestValuesRetryCount, null, 'The number of retries Model Eval will attempt for each evaluation batch; Defaults to one')
, (@UseModelNotFoundMessage, null, 'When models are not returned in getLatestValues, dictates whether EvaluationErrorMessage is set with message defined in "ModelNotFoundMessage" key; "1" = use the message; anything else = persist exstng. EvaluationErrorMessage; defaults to anything else')
, (@ModelNotFoundMessage, null, 'the message with which EvaluationErrorMessage will be valued if not set or config value is empty, defaults to "Model not found in Alerts table"')

-- alerts custodian, if not already covered by others
, (@StaleDurationHours, null, 'Duration in hours prior to app execution time that dictates whether a lock is considered stale')
, (@BuildLagMinutes, null, 'Duration in minutes prior to app execution time that dictates whether build activity is considered to be occurring')
, (@RunAllChecks, null, 'Dictates whether app executes all unlock checks or stops when a check returns unlock')
, (@RunOnce, null, 'Dictates whether app executes once or continuously')

-- model retrain, if not already covered by others
, (@nDCallbackUrl, null, 'Base URL used for in-bound communication from nD')
, (@BuildIntervalOverrideInDays, null, 'Number of days since model last built timestamp that when exceeded will include the model in retrain')
, (@retrain, null, 'Directs Model Retrain to execute retrain action')
, (@build, null, 'Directs Model Retrain to execute build action')
, (@includeAllQueuedForBuild, null, 'Directs Model Retrain to include all models marked as QueueForBuild, regardless of other filters')
, (@maxModelsToInclude, null, 'Maximum number of models in include in a Model Retrain retrain or build action')
, (@cloudWatchLoggerLogGroup, null, 'Name of target CloudWatch log group')
, (@cloudWatchLoggerLogStream, null, 'Name of target CloudWatch log stream')

-- nDProcessDataJSONPump
, (@dataFileSizeHourLimit, null, 'Puts a limit on the number of data files that are read to load data for transfer by comparing the file time to the last time stamp')

-- ManualDataPumptoND
, (@pDServerIDList, null, 'Comma seperated integer list of PDServers that will have data manually uploaded to them to be pumped to ND')
, (@destinationPath, null, 'Physical or Virtual disk location on App server where JSON data to be pumped is located')
, (@pollingSleepTime, null, 'Time in minutes between executions of the search for manually uploaded data')
, (@s3BucketLocation, null, 'Name of Amazon web services bucket in which jobs have stored JSON data from the manual data upload.')
, (@isPaused, null, 'Boolean that indicates the application is in a Paused state')
, (@userID, null, 'UserID for Respository Security')
, (@lastPollTime, null, 'Last date and time S3 was polled for import jobs')

) AS Source ([Key], [Value], [Comment])
ON (Target.[Key] = Source.[Key])
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Key], [Value], [Comment])
VALUES (Source.[Key], Source.[Value], Source.[Comment])
WHEN NOT MATCHED BY SOURCE THEN
DELETE
;


SET IDENTITY_INSERT [Configuration].[tAppTypeAppSettingMap] ON
MERGE INTO [Configuration].[tAppTypeAppSettingMap] AS Target
USING (VALUES
-- pump
(1, @appTypeOPM2NDTransfer, @ApplicationName, 1, 0)
, (2, @appTypeOPM2NDTransfer, @ArchiveDirectory, 1, 0)
, (3, @appTypeOPM2NDTransfer, @NDEndpoint, 1, 0)
, (4, @appTypeOPM2NDTransfer, @PDServerID, 1, 0)
, (5, @appTypeOPM2NDTransfer, @AllowCalcPoints, 1, 0)
, (6, @appTypeOPM2NDTransfer, @ExitNow, 1, 0)
, (7, @appTypeOPM2NDTransfer, @DestroyPump, 1, 0)
, (8, @appTypeOPM2NDTransfer, @MinuteInterval, 1, 0)
, (9, @appTypeOPM2NDTransfer, @MaxMinutesLimit, 1, 0)
, (10, @appTypeOPM2NDTransfer, @CycleDelay, 1, 0)
, (11, @appTypeOPM2NDTransfer, @AllowInactivePoints, 1, 0)
, (12, @appTypeOPM2NDTransfer, @UpdateWarningMinutes, 1, 0)
, (13, @appTypeOPM2NDTransfer, @PointFrequency, 1, 0)
, (14, @appTypeOPM2NDTransfer, @BackfillMinutesLimit, 1, 0)
, (15, @appTypeOPM2NDTransfer, @CreatingPump, 1, 0)
, (16, @appTypeOPM2NDTransfer, @LogDir, 1, 0)
, (17, @appTypeOPM2NDTransfer, @LogFile, 1, 0)
, (18, @appTypeOPM2NDTransfer, @LogFileCount, 1, 0)
, (19, @appTypeOPM2NDTransfer, @LogLevel, 1, 0)

-- tag sync
, (20, @appTypeNDTagSynchronization, @ApplicationName, 1, 0)
, (21, @appTypeNDTagSynchronization, @NDEndpoint, 1, 0)
, (22, @appTypeNDTagSynchronization, @PDServerID, 1, 0)
, (23, @appTypeNDTagSynchronization, @RunSetting, 1, 0)
, (24, @appTypeNDTagSynchronization, @RunEverySeconds, 1, 0)
, (25, @appTypeNDTagSynchronization, @RunCP, 1, 0)
, (26, @appTypeNDTagSynchronization, @RunCPStore, 1, 0)
, (27, @appTypeNDTagSynchronization, @RunSnapshot, 1, 0)
, (28, @appTypeNDTagSynchronization, @RunSchemaChangeSnapshot, 1, 0)
, (29, @appTypeNDTagSynchronization, @RunDPStore, 1, 0)
, (30, @appTypeNDTagSynchronization, @PumpDestroyCreate, 1, 0)
, (31, @appTypeNDTagSynchronization, @DeleteExistingCPReleaseVariables, 1, 0)
, (32, @appTypeNDTagSynchronization, @ProcessExisting, 1, 0)
, (33, @appTypeNDTagSynchronization, @SimStartExpr, 1, 0)
, (34, @appTypeNDTagSynchronization, @SimEndExpr, 1, 0)
, (35, @appTypeNDTagSynchronization, @OAuthUserName, 1, 0)
, (36, @appTypeNDTagSynchronization, @OAuthUserPassword, 1, 0)
, (37, @appTypeNDTagSynchronization, @PointFrequency, 1, 0)
, (38, @appTypeNDTagSynchronization, @CreateInactivePoints, 1, 0)
, (39, @appTypeNDTagSynchronization, @LogDir, 1, 0)
, (40, @appTypeNDTagSynchronization, @LogFile, 1, 0)
, (41, @appTypeNDTagSynchronization, @LogFileCount, 1, 0)
, (42, @appTypeNDTagSynchronization, @ScheduleTimes, 1, 0)

-- model eval
, (43, @appTypeNDModelEvaluation, @ApplicationName, 1, 0)
, (44, @appTypeNDModelEvaluation, @NDEndpoint, 1, 0)
, (45, @appTypeNDModelEvaluation, @PDServerIDs, 1, 0)
, (46, @appTypeNDModelEvaluation, @RunSetting, 1, 0)
, (47, @appTypeNDModelEvaluation, @RunEverySeconds, 1, 0)
, (48, @appTypeNDModelEvaluation, @SecurityUserEmailAddress, 1, 0)
, (49, @appTypeNDModelEvaluation, @TakeCount, 1, 0)
, (50, @appTypeNDModelEvaluation, @LatestValuesRetryCount, 1, 0)
, (51, @appTypeNDModelEvaluation, @UseModelNotFoundMessage, 1, 0)
, (52, @appTypeNDModelEvaluation, @ModelNotFoundMessage, 1, 0)
, (53, @appTypeNDModelEvaluation, @LogDir, 1, 0)
, (54, @appTypeNDModelEvaluation, @LogFile, 1, 0)

-- alerts custodian
, (55, @appTypeAlertsCustodian, @PDServerIDs, 1, 1)
, (56, @appTypeAlertsCustodian, @RunOnce, 1, 0)
, (57, @appTypeAlertsCustodian, @ApplicationName, 1, 0)
, (58, @appTypeAlertsCustodian, @LogDir, 1, 0)
, (59, @appTypeAlertsCustodian, @LogFile, 1, 0)
, (60, @appTypeAlertsCustodian, @LogFileCount, 1, 0)
, (61, @appTypeAlertsCustodian, @StaleDurationHours, 1, 0)
, (62, @appTypeAlertsCustodian, @BuildLagMinutes, 1, 0)
, (63, @appTypeAlertsCustodian, @RunAllChecks, 1, 1)
, (64, @appTypeAlertsCustodian, @SecurityUserEmailAddress, 1, 0)
, (65, @appTypeAlertsCustodian, @CycleDelay, 1, 0)

-- model retrain
, (66, @appTypeModelRetrain, @securityUserEmailAddress, 1, 0)
, (67, @appTypeModelRetrain, @nDEndpoint, 1, 0)
, (68, @appTypeModelRetrain, @pDServerIDs, 0, 0)
, (69, @appTypeModelRetrain, @scheduleTimes, 0, 0)
, (70, @appTypeModelRetrain, @logLevel, 0, 0)
, (71, @appTypeModelRetrain, @logDir, 0, 0)
, (72, @appTypeModelRetrain, @logFile, 0, 0)
, (73, @appTypeModelRetrain, @applicationName, 0, 0)
, (74, @appTypeModelRetrain, @nDCallbackUrl, 1, 0)
, (75, @appTypeModelRetrain, @BuildIntervalOverrideInDays, 0, 0)
, (76, @appTypeModelRetrain, @retrain, 1, 0)
, (77, @appTypeModelRetrain, @build, 1, 0)
, (78, @appTypeModelRetrain, @includeAllQueuedForBuild, 0, 0)
, (79, @appTypeModelRetrain, @maxModelsToInclude, 0, 0)
, (80, @appTypeModelRetrain, @cloudWatchLoggerLogGroup, 1, 0)
, (81, @appTypeModelRetrain, @cloudWatchLoggerLogStream, 1, 0)

-- nDProcessDataJSONPump
, (82, @appTypeNDProcessDataJSONPump, @ApplicationName, 1, 0)
, (83, @appTypeNDProcessDataJSONPump, @ArchiveDirectory, 1, 0)
, (84, @appTypeNDProcessDataJSONPump, @NDEndpoint, 1, 0)
, (85, @appTypeNDProcessDataJSONPump, @PDServerID, 1, 0)
, (86, @appTypeNDProcessDataJSONPump, @AllowCalcPoints, 1, 0)
, (87, @appTypeNDProcessDataJSONPump, @ExitNow, 1, 0)
, (88, @appTypeNDProcessDataJSONPump, @DestroyPump, 1, 0)
, (89, @appTypeNDProcessDataJSONPump, @MinuteInterval, 1, 0)
, (90, @appTypeNDProcessDataJSONPump, @MaxMinutesLimit, 1, 0)
, (91, @appTypeNDProcessDataJSONPump, @CycleDelay, 1, 0)
, (92, @appTypeNDProcessDataJSONPump, @AllowInactivePoints, 1, 0)
, (93, @appTypeNDProcessDataJSONPump, @UpdateWarningMinutes, 1, 0)
, (94, @appTypeNDProcessDataJSONPump, @PointFrequency, 1, 0)
, (95, @appTypeNDProcessDataJSONPump, @BackfillMinutesLimit, 1, 0)
, (96, @appTypeNDProcessDataJSONPump, @LogDir, 1, 0)
, (97, @appTypeNDProcessDataJSONPump, @LogFile, 1, 0)
, (98, @appTypeNDProcessDataJSONPump, @LogFileCount, 1, 0)
, (99, @appTypeNDProcessDataJSONPump, @LogLevel, 1, 0)
, (100, @appTypeNDProcessDataJSONPump, @cloudWatchLoggerLogGroup, 1, 0)
, (101, @appTypeNDProcessDataJSONPump, @cloudWatchLoggerLogStream, 1, 0)
, (102, @appTypeNDProcessDataJSONPump, @dataFileSizeHourLimit, 1, 0)

--ManualDataPumptoND
, (103, @appTypeManualDataPumptoND,  @pDServerIDList, 1, 0)
, (104, @appTypeManualDataPumptoND,  @destinationPath, 1, 0)
, (105, @appTypeManualDataPumptoND,  @pollingSleepTime, 1, 0)
, (106, @appTypeManualDataPumptoND,  @s3BucketLocation, 1, 0)
, (107, @appTypeManualDataPumptoND,  @isPaused, 1, 0)
, (108, @appTypeManualDataPumptoND,  @userID, 1, 0)
, (109, @appTypeManualDataPumptoND,  @lastPollTime, 1, 0)
, (110, @appTypeManualDataPumptoND,  @exitNow, 1, 0)
, (111, @appTypeManualDataPumptoND,  @logDir, 1, 0)
, (112, @appTypeManualDataPumptoND,  @logFile, 1, 0)
, (113, @appTypeManualDataPumptoND,  @logFileCount, 1, 0)

) AS Source ([AppTypeAppSettingMapID], [AppTypeID], [Key], [IsRequired], [LocalOverRide])
ON (Target.[AppTypeAppSettingMapID] = Source.[AppTypeAppSettingMapID])
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AppTypeAppSettingMapID], [AppTypeID], [Key], [IsRequired], [LocalOverRide])
VALUES (Source.[AppTypeAppSettingMapID], Source.[AppTypeID], Source.[Key], Source.[IsRequired], Source.[LocalOverRide])
WHEN NOT MATCHED BY SOURCE THEN
DELETE
;
SET IDENTITY_INSERT [Configuration].[tAppTypeAppSettingMap] ON
