﻿MERGE INTO Diagnostics.tAssessmentType AS TARGET USING (VALUES
(1,'Vibration','Vibration')
,(2,'Thermography','Thermography')
,(3,'Acoustics','Acoustics')
,(4,'Oil Analysis','OilAnalysis')
,(5,'Eddy Current Testing','EddyCurrentTesting')
,(6,'Motor Test','MotorTest')
,(7,'Performance Test','PerformanceTest')
,(8,'Physical Inspection','PhysicalInspection')
)
AS SOURCE([AssessmentTypeID],[AssessmentTypeDesc],[AssessmentTypeAbbrev])
ON Target.[AssessmentTypeID]= Source.[AssessmentTypeID]
WHEN MATCHED THEN UPDATE SET [AssessmentTypeDesc]=Source.[AssessmentTypeDesc],[AssessmentTypeAbbrev]=Source.[AssessmentTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssessmentTypeID],[AssessmentTypeDesc],[AssessmentTypeAbbrev])
VALUES ([AssessmentTypeID],[AssessmentTypeDesc],[AssessmentTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
GO

SET IDENTITY_INSERT [Diagnostics].tAssetIssueImpactCalculationFactor ON;
MERGE INTO Diagnostics.tAssetIssueImpactCalculationFactor AS TARGET USING (VALUES
(1, N'Main Steam Temperature : Subcritical', N'Main Steam Temperature : Subcritical', 0.15, 10, N'°F', 1)
,(2, N'Main Steam Temperature : Supercritical', N'Main Steam Temperature : Supercritical', 0.2, 10, N'°F', 1)
,(3, N'Main Steam Pressure', N'Main Steam Pressure', 0.04, 10, N'psi', 1)
,(4, N'HP Turbine Efficiency : Reheat Unit', N'HP Turbine Efficiency : Reheat Unit', 0.19, 1, N'%', 1)
,(5, N'HP Turbine Efficiency : NonReheat Unit', N'HP Turbine Efficiency : NonReheat Unit', 0.6, 1, N'%', 1)
,(6, N'HRH Steam Temperature', N'HRH Steam Temperature', 0.14, 10, N'°F', 1)
,(7, N'IP Turbine Efficiency', N'IP Turbine Efficiency', 0.17, 1, N'%', 1)
,(8, N'LP Turbine Efficiency', N'LP Turbine Efficiency', 0.45, 1, N'%', 1)
,(9, N'Main Steam Attemperation', N'Main Steam Attemperation', 0.025, 10, N'kpph', 1)
,(10, N'Main Steam Attemperation From BFP Discharge', N'Main Steam Attemperation From BFP Discharge', 0.025, 1, N'MSF', 1)
,(11, N'Main Steam Attemperation From Final FW', N'Main Steam Attemperation From Final FW', 0.008, 1, N'MSF', 1)
,(12, N'Reheat Steam Attemperation by Flow', N'Reheat Steam Attemperation by Flow', 0.22, 10, N'kpph', 1)
,(13, N'Reheat Steam Attemperation by %MSF', N'Reheat Steam Attemperation by %MSF', 0.2, 1, N'MSF', 1)
,(14, N'Condenser Backpressure', N'Condenser Backpressure', 2.04, 1, N'inHg', 1)
,(15, N'Final Feedwater Temp', N'Final Feedwater Temp', 0.1, 5, N'°F', 1)
,(16, N'Top Stage FWH TTD', N'Top Stage FWH TTD', 0.02, 1, N'°F TTD', 1)
,(17, N'Top Stage FWH OOS', N'Top Stage FWH OOS', 1, 1, NULL, 1)
,(18, N'Top Stage FWH - 100% Shell Drain to Condenser', N'Top Stage FWH - 100% Shell Drain to Condenser', 1, 1, NULL, 1)
,(19, N'Next to Top Stage FWH TTD', N'Next to Top Stage FWH TTD', 0.005, 1, N'°F TTD', 1)
,(20, N'Next to Top Stage FWH OOS - Drain to Condenser', N'Next to Top Stage FWH OOS - Drain to Condenser', 1.8, 1, NULL, 1)
,(22, N'Other FWH TTD', N'Other FWH TTD', 0.006, 1, N'°F TTD', 1)
,(23, N'LP FWH OOS', N'LP FWH OOS', 0.006, 1, NULL, 1)
,(24, N'BFP Efficiency', N'BFP Efficiency', 0.02, 1, N'%', 1)
,(25, N'BFPT Efficiency', N'BFPT Efficiency', 0.02, 1, N'%', 1)
,(26, N'Recirculation Flow Valve - 1% Leak', N'Recirculation Flow Valve - 1% Leak', 0.01, 1, NULL, 1)
,(27, N'Excess O2', N'Excess O2', 0.29, 1, N'% O2', 1)
,(30, N'Air Heater Gas Out Temperature (AHGOT)', N'Air Heater Gas Out Temperature (AHGOT)', 0.25, 10, N'°F', 1)
,(31, N'Air Heater Leakage', N'Air Heater Leakage', 0.05, 1, N'% Leak', 1)
,(32, N'Air Heater Effectiveness', N'Air Heater Effectiveness', 0.15, 1, N'%', 1)
,(33, N'FD Fan Inlet Air Temperature', N'FD Fan Inlet Air Temperature', 0.05, 10, N'°F', 1)
,(34, N'Boiler Drum Blowdown Flow', N'Boiler Drum Blowdown Flow', 0.05, 5, N'kpph', 1)
,(35, N'Make Up Flow', N'Make Up Flow', 0.24, 1, N'%', 1)
,(36, N'Soot Blowing', N'Soot Blowing', 0.1, 5, N'kpph', 1)
,(37, N'Steam Coils From Drum', N'Steam Coils From Drum', 0.37, 1, N'% MSF', 1)
,(38, N'Steam Coils From Cold Reheat', N'Steam Coils From Cold Reheat', 0.25, 1, N'% MSF', 1)
,(39, N'Steam Coils From Cold Reheat', N'Steam Coils From Cold Reheat', 0.25, 1, N'% MSF', 1)
,(40, N'Moisture in Coal For 12000 Btu/lb Coal', N'Moisture in Coal For 12000 Btu/lb Coal', 0.1, 1, N'%', 1)
,(41, N'Moisture in Coal For 8000 Btu/lb Coal', N'Moisture in Coal For 8000 Btu/lb Coal', 0.17, 1, N'%', 1)
,(42, N'Mill Outlet Air Temperature', N'Mill Outlet Air Temperature', 0.04, 10, N'°F', 1)
,(43, N'Unburned Carbon', N'Unburned Carbon', 1, 1, N'%', 1)
,(44, N'Aux Steam Extract (from CRH) Impact by flow', N'Aux Steam Extract (from CRH) Impact by flow', 0.075, 5, N'kpph', 1)
,(45, N'Aux Power (Parasitic Load)', N'Aux Power (Parasitic Load)', 1, 1, N'% Load', 1)
,(46, N'Main Steam Temperature : Subcritical', N'Main Steam Temperature : Subcritical', 0.1, 10, N'°F', 3)
,(48, N'HP Turbine Efficiency : Reheat Unit', N'HP Turbine Efficiency : Reheat Unit', 0.28, 1, N'%', 3)
,(49, N'HRH Steam Temperature', N'HRH Steam Temperature', 0.5, 10, N'°F', 3)
,(50, N'IP Turbine Efficiency', N'IP Turbine Efficiency', 0.17, 1, N'%', 3)
,(51, N'LP Turbine Efficiency', N'LP Turbine Efficiency', 0.45, 1, N'%', 3)
,(52, N'Main Steam Attemperation From BFP Discharge', N'Main Steam Attemperation From BFP Discharge', 0.08, 1, N'MSF', 3)
,(53, N'Reheat Steam Attemperation by %MSF', N'Reheat Steam Attemperation by %MSF', 0.6, 1, N'MSF', 3)
,(54, N'Final Feedwater Temp', N'Final Feedwater Temp', 0.5, 5, N'°F', 3)
,(55, N'Top Stage FWH TTD', N'Top Stage FWH TTD', 0.05, 1, N'°F TTD', 3)
,(56, N'Air Heater Gas Out Temperature (AHGOT)', N'Air Heater Gas Out Temperature (AHGOT)', 0.26, 10, N'°F', 3)
,(57, N'Boiler Drum Blowdown Flow', N'Boiler Drum Blowdown Flow', 0.063, 5, N'kpph', 3)
,(58, N'Soot Blowing', N'Soot Blowing', 0.125, 5, N'kpph', 3)
,(59, N'Aux Steam Extract (from CRH) Impact by flow', N'Aux Steam Extract (from CRH) Impact by flow', 0.019, 5, N'kpph', 3)
,(60, N'Aux Steam Extract (from CRH) Impact by % CRH flow', N'Aux Steam Extract (from CRH) Impact by % CRH flow', 0.7, 1, N'% CRH Steam Flow', 3)
,(61, N'Economizer Exit Gas Temperature For 12000 Btu/lb Coal', N'Economizer Exit Gas Temperature For 12000 Btu/lb Coal', 0.25, 10, N'°F', 1)
,(62, N'Economizer Exit Gas Temperature For 8000 Btu/lb Coal', N'Economizer Exit Gas Temperature For 8000 Btu/lb Coal', 0.35, 10, N'°F', 1)
,(63, N'Main Steam Pressure : Constant Control Valve Positions', N'Main Steam Pressure : Constant Control Valve Positions', 1, 1, N'% Throttle Pressure', 3)
,(64,'Manual Input','Manual Input',1,1,' ',1)
,(65,'Manual Input','Manual Input',1,1,' ',3)
)
AS SOURCE([AssetIssueImpactCalculationFactorID],[CalcuationFactorAbbrev],[CalculationFactorDesc],[CalculationFactor],[CalculationFactorRatio],[CalculationFactorUnits],[AssetIssueImpactCategoryTypeID])
ON Target.[AssetIssueImpactCalculationFactorID]= Source.[AssetIssueImpactCalculationFactorID]
WHEN MATCHED THEN UPDATE SET [CalcuationFactorAbbrev]=Source.[CalcuationFactorAbbrev],
							 [CalculationFactor]=Source.[CalculationFactor],
							 [CalculationFactorRatio]=Source.[CalculationFactorRatio],
							 [CalculationFactorUnits]=Source.[CalculationFactorUnits],
							 [AssetIssueImpactCategoryTypeID]=Source.[AssetIssueImpactCategoryTypeID]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssetIssueImpactCalculationFactorID],[CalcuationFactorAbbrev],[CalculationFactorDesc],[CalculationFactor],[CalculationFactorRatio],[CalculationFactorUnits],[AssetIssueImpactCategoryTypeID])
VALUES ([AssetIssueImpactCalculationFactorID],[CalcuationFactorAbbrev],[CalculationFactorDesc],[CalculationFactor],[CalculationFactorRatio],[CalculationFactorUnits],[AssetIssueImpactCategoryTypeID])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [Diagnostics].tAssetIssueImpactCalculationFactor OFF;

 MERGE INTO Diagnostics.[tIssueClassType] AS TARGET USING (VALUES
(1,	'M&D',	'Monitoring and Diagnostics'),
(2,	'Fuels',	'Fuels'),
(3,	'Compliance', 'Compliance'),
(4,	'Membrane Cleaning',	'Membrane Cleaning' ),
(5,	'Arc Flash',	'Arc Flash' ),
(6, 'Project Management', 'Proj. Mgmt.'),

-- PBI 50814, technically only Asset Health needed but bringing it up to date
(7, 'Pole Attachment Program', 'Pole Attachment Program'),
(8, 'O&M', 'Operations and Maintenance'),
(9, 'Asset Health', 'Asset Health')
)
 AS SOURCE([IssueClassTypeID],[IssueClassTypeDesc],[IssueClassTypeAbbrev])
 ON Target.[IssueClassTypeID]=Source.[IssueClassTypeID]
 WHEN MATCHED THEN UPDATE SET
 [IssueClassTypeDesc]=Source.[IssueClassTypeDesc]
 ,[IssueClassTypeAbbrev]=Source.[IssueClassTypeAbbrev]
 WHEN NOT MATCHED BY TARGET THEN
 INSERT ([IssueClassTypeID],[IssueClassTypeDesc],[IssueClassTypeAbbrev])
 VALUES([IssueClassTypeID],[IssueClassTypeDesc],[IssueClassTypeAbbrev])
 WHEN NOT MATCHED BY SOURCE THEN DELETE;

  DISABLE TRIGGER [tIssueType].ValidateIssueTypeInsertUpdate ON [Diagnostics].[tIssueType]
GO

MERGE INTO Diagnostics.tIssueType AS TARGET USING (VALUES
 (1,'Generic M&D','Generic M&D',NULL,1,1),
(2,'Generic Fuels','Generic Fuels',NULL,1,1),
(3,'Generic Compliance','Generic Compliance',NULL,1,1),
(4,'Generic Membrane Cleaning','Generic Membrane Cleaning',NULL,1,1),
(6,'Condenser Cleanliness Degradation','Condenser Cleanliness Degradation',-1,1,1),
(9,'Condenser Subcooling Increasing','Subcooling Increasing',1,1,1),
(10,'Condenser Waterbox Pressure Drop Increasing','Waterbox Pressure Drop Increasing',1,1,1),
(11,'Condenser Drains Temperature Increasing','Drains Temperature Increasing',1,1,1),
(21,'Boiler Low Throttle Steam Temperature','Boiler Low Throttle Steam Temperature',-1,1,1),
(22,'Boiler Low Hot Reheat 1 Steam Temperature','Boiler Low Hot Reheat 1 Steam Temperature',-1,1,1),
(23,'Boiler Low Hot Reheat 2 Steam Temperature','Boiler Low Hot Reheat 2 Steam Temperature',-1,1,1),
(31,'Feedwater Heater TTD Degradation','Feedwater Heater TTD Degradation',-1,1,1),
(39,'Air Heater Cold-End-Average Temperature HR Loss Changing','Air Heater Cold-End-Average Temperature HR Loss Changing',0,1,1),
(40,'Air Heater Gas Outlet Temp (AH Heat Transfer) HR Loss Changing','Air Heater Gas Outlet Temp (AH Heat Transfer) HR Loss Changing',0,1,1),
(41,'Air Heater Leakage HR Loss Changing','Air Heater Leakage HR Loss Changing',0,1,1),
(42,'Auxilliary Power HR Loss Changing','Auxilliary Power HR Loss Changing',0,1,1),
(43,'Condenser Backpressure HR Loss Changing','Condenser Backpressure HR Loss Changing',0,1,1),
(44,'Condenser Backpressure (HP) HR Loss Changing','Condenser Backpressure (HP) HR Loss Changing',0,1,1),
(45,'Condenser Backpressure (LP) HR Loss Changing','Condenser Backpressure (LP) HR Loss Changing',0,1,1),
(46,'Condenser Subcooling HR Loss Changing','Condenser Subcooling HR Loss Changing',0,1,1),
(47,'Cycle Mass Loss HR Loss Changing','Cycle Mass Loss HR Loss Changing',0,1,1),
(48,'Econ Gas Outlet Temp (Sootblowing) HR Loss Changing','Econ Gas Outlet Temp (Sootblowing) HR Loss Changing',0,1,1),
(49,'Excess O2 at Economizer Exit HR Loss Changing','Excess O2 at Economizer Exit HR Loss Changing',0,1,1),
(50,'Generator Hydrogen Pressure HR Loss Changing','Generator Hydrogen Pressure HR Loss Changing',0,1,1),
(51,'Hot Reheat Steam Temp at Boiler HR Loss Changing','Hot Reheat Steam Temp at Boiler HR Loss Changing',0,1,1),
(52,'Hot Reheat Steam Temp at Boiler (RH 2) HR Loss Changing','Hot Reheat Steam Temp at Boiler (RH 2) HR Loss Changing',0,1,1),
(53,'HPT Efficiency HR Loss Changing','HPT Efficiency HR Loss Changing',0,1,1),
(54,'IPT Efficiency HR Loss Changing','IPT Efficiency HR Loss Changing',0,1,1),
(55,'Main Steam Temp at Boiler HR Loss Changing','Main Steam Temp at Boiler HR Loss Changing',0,1,1),
(56,'RH Pressure Drop from HPT to IPT HR Loss Changing','RH Pressure Drop from HPT to IPT HR Loss Changing',0,1,1),
(57,'RH Pressure Drop from HPT to IPT (RH 2) HR Loss Changing','RH Pressure Drop from HPT to IPT (RH 2) HR Loss Changing',0,1,1),
(58,'RH Spray Flowrate HR Loss Changing','RH Spray Flowrate HR Loss Changing',0,1,1),
(59,'RH Spray Flowrate (RH 2) HR Loss Changing','RH Spray Flowrate (RH 2) HR Loss Changing',0,1,1),
(60,'SH Spray Flowrate HR Loss Changing','SH Spray Flowrate HR Loss Changing',0,1,1),
(61,'Throttle Steam Pressure HR Loss Changing','Throttle Steam Pressure HR Loss Changing',0,1,1),
(62,'Top Feedwater Heater External TTD HR Loss Changing','Top Feedwater Heater External TTD HR Loss Changing',0,1,1),
(63,'VHPT Efficiency HR Loss Changing','VHPT Efficiency HR Loss Changing',0,1,1),
(64,'Evaporative Cooler Effectiveness Degradation','Evaporative Cooler Effectiveness Degradation',-1,1,1),
(65,'CT Air Intake Filter DP Increasing','CT Air Intake Filter DP Increasing',1,1,1),
(66,'Fuel Gas Temperature Drop','Fuel Gas Temperature Drop',-1,1,1),
(67,'Compressor Inlet Pressure Decreasing','Compressor Inlet Pressure Decreasing',-1,1,1),
(68,'Compressor Discharge Pressure Degradation','Compressor Discharge Pressure Degradation',-1,1,1),
(69,'Compressor Pressure Ratio Decreasing','Compressor Pressure Ratio Decreasing',-1,1,1),
(70,'Compressor Temperature Ratio Increase','Compressor Temperature Ratio Increase',1,1,1),
(71,'Compressor Isentropic Efficiency Degradation','Compressor Isentropic Efficiency Degradation',-1,1,1),
(72,'CT Corrected Gross Heat Rate (HHV) Increasing','CT Corrected Gross Heat Rate (HHV) Increasing',1,1,1),
(73,'CT Gross Heat Rate (HHV) Increasing','CT Gross Heat Rate (HHV) Increasing',1,1,1),
(74,'CT Corrected Gross Power Output Decreasing','CT Corrected Gross Power Output Decreasing',-1,1,1),
(75,'CT Gross Power Output Decreasing','CT Gross Power Output Decreasing',-1,1,1),
(76,'CT Exhaust Pressure Increasing','CT Exhaust Back Pressure Increasing',1,1,1),
(77,'HRSG Thermal Efficiency Degradation','HRSG Thermal Efficiency Degradation',-1,1,1),
(78,'HRSG LP Pinch Point Increasing','HRSG LP Pinch Point Increasing',1,1,1),
(79,'HRSG LP Effectiveness Decreasing','HRSG LP Effectiveness Decreasing',-1,1,1),
(80,'HRSG IP Pinch Point Increasing','HRSG IP Pinch Point Increasing',1,1,1),
(81,'HRSG IP Effectiveness Decreasing','HRSG IP Effectiveness Decreasing',-1,1,1),
(82,'HRSG HP Pinch Point Increasing','HRSG HP Pinch Point Increasing',1,1,1),
(83,'HRSG HP Effectivenss Decreasing','HRSG HP Effectivenss Decreasing',-1,1,1),
(84,'HRSG Steam Production Decreasing (Full Load CT Exhaust Conditions)','HRSG Steam Production Decreasing (Full Load CT Exhaust Conditions)',-1,1,1),
(85,'HRSG Steam Production Decreasing ( Part Load Conditions)','HRSG Steam Production Decreasing ( Part Load Conditions)',-1,1,1),
(86,'HRSG FW Flow Demand Increasing','HRSG FW Flow Demand Increasing',1,1,1),
(87,'HRSG HP Steam Temperature Low','HRSG HP Steam Temperature Low',-1,1,1),
(88,'HRSG HRH Steam Temperature Low','HRSG HRH Steam Temperature Low',-1,1,1),
(89,'HRSG LP Steam Enthalpy Low','HRSG LP Steam Enthalpy Low',-1,1,1),
(90,'RAC Blowdown Increasing','RAC Blowdown Increasing',1,1,1),
(91,'Rotor Air Temperature from RAC High','Rotor Air Temperature from RAC High',1,1,1),
(92,'SCR Outlet Nox High' ,'SCR Outlet Nox High',1,1,1),
(93,'HRSG Stack Temperature Low (Possible Subcooling in Economizer)','HRSG Stack Temperature Low (Possible Subcooling in Economizer)',-1,1,1),
(94,'HRSG Stack Temperature High','HRSG Stack Temperature High',1,1,1),
(95,'HPT Efficiency Decreasing','HPT Efficiency Decreasing',-1,1,1),
(96,'FD/PA Fan Amp Imbalance Increasing','FD/PA Fan Amp Imbalance Increasing',1,1,1),
(97,'Pump Bearing Vibration Increasing','Pump Bearing Vibration Increasing',1,1,1),
(98,'Boiler Throttle Steam Pressure Decreasing','Boiler Throttle Steam Pressure Decreasing',-1,1,1),
(99,'Pulverizer Power Input Increasing','Pulverizer Power Input Increasing',1,1,1),
(100,'CT Compressor Efficiency Decreasing','CT Compressor Efficiency Decreasing',-1,1,1),
(101,'CT Combustor Fuel Gas Temperature Decreasing','CT Combustor Fuel Gas Temperature Decreasing',-1,1,1),
(102,'CT Turbine Efficiency Decreasing','CT Turbine Efficiency Decreasing',-1,1,1),
(103,'CT Generator Stator Temperature Increasing','CT Generator Stator Temperature Increasing',1,1,1),
(104,'CT NOx Emissions Increasing','CT NOx Emissions Increasing',1,1,1),
(105,'Pulverizer High dP','Pulverizer High dP',1,1,1),
(106,'Pulverizer Outlet Temperature Low','Pulverizer Outlet Temperature Low',-1,1,1),
(107,'Pulverizer Outlet Temperature High','Pulverizer Outlet Temperature High',1,1,1),
(108,'Too Many Pulverizers in Service','Too Many Pulverizers in Service',1,1,1),
(109,'Pump Head Decreasing','Pump Head Decreasing',-1,1,1),
(110,'Pump Power Increasing','Pump Power Increasing',1,1,1),
(111,'Pump Efficiency Decreasing','Pump Efficiency Decreasing',-1,1,1),
(112,'Too Many Pumps in Service','Too Many Pumps in Service',1,1,1),
(113,'Pump Unnecessary Steam Source Flow','Pump Unnecessary Steam Source Flow',0,1,1),
(114,'ID Fan Amp Imbalance Increasing','ID Fan Amp Imbalance Increasing',1,1,1),
(115,'FD/PA Fan Set Total Current Increasing','FD/PA Fan Set Total Current Increasing',1,1,1),
(116,'ID Fan Set Total Current Increasing','ID Fan Set Total Current Increasing',1,1,1),
(117,'Fan Bearing Temperature Increasing','Fan Bearing Temperature Increasing',1,1,1),
(118,'Fan Motor Winding Temperature Increasing','Fan Motor Winding Temperature Increasing',1,1,1),
(119,'Fan Pressure Rise Increasing','Fan Pressure Rise Increasing',1,1,1),
(120,'Fan Efficiency Decreasing','Fan Efficiency Decreasing',-1,1,1),
(121,'Too Much Fan','Too Much Fan',1,1,1),
(122,'Fan Unnecessary Steam Source Flow','Fan Unnecessary Steam Source Flow',NULL,1,1),
(123,'Air Heater Gas Side Pressure Drop Increasing','Air Heater Gas Side Pressure Drop Increasing',1,1,1),
(124,'Air Heater Cold End Average Temperature Increasing','Air Heater Cold End Average Temperature Increasing',1,1,1),
(125,'Air Heater Cold End Average Temperature Decreasing','Air Heater Cold End Average Temperature Decreasing',-1,1,1),
(126,'Air Heater X Ratio Increasing','Air Heater X Ratio Increasing',1,1,1),
(127,'Air Heater Air Side dP Increasing','Air Heater Air Side dP Increasing',1,1,1),
(128,'Air Heater APH Coil dP Increasing','Air Heater APH Coil dP Increasing',1,1,1),
(129,'Air Heater Bearing Temperature Increasing','Air Heater Bearing Temperature Increasing',1,1,1),
(130,'Feedwater Heater DCA Degradation','Feedwater Heater DCA Degradation',-1,1,1),
(131,'Feedwater Heater Drain Flow Discrepancy','Feedwater Heater Drain Flow Discrepancy',0,1,1),
(132,'Feedwater Heater Extraction Pressure Drop Increasing','Feedwater Heater Extraction Pressure Drop Increasing',1,1,1),
(133,'Feedwater Heater Temperature Change after Bypass Tie-In Decreasing','Feedwater Heater Temperature Change after Bypass Tie-In Decreasing',-1,1,1),
(134,'Main Condenser Backpressure Degradation','Main Condenser Backpressure Degradation',-1,1,1),
(135,'Condenser Circulating Water Flow Decreasing','Condenser Circulating Water Flow Decreasing',-1,1,1),
(136,'Condenser Air In-leakage Increasing','Condenser Air In-leakage Increasing',1,1,1),
(137,'IPT Efficiency Decreasing','IPT Efficiency Decreasing',-1,1,1),
(138,'HPT Flow Factor Changing','HPT Flow Factor Changing',0,1,1),
(139,'HPT Throttle to 1st Stage Corrected Pressure Drop Increasing','HPT Throttle to 1st Stage Corrected Pressure Drop Increasing',1,1,1),
(140,'HPT 1st Stage to CRH Corrected Pressure Drop Increasing','HPT 1st Stage to CRH Corrected Pressure Drop Increasing',1,1,1),
(141,'IPT Hot RH to Heater F Ext Corrected Pressure Drop Increasing','IPT Hot RH to Heater F Ext Corrected Pressure Drop Increasing',1,1,1),
(142,'IPT Heater Ext to DA Ext Corrected Pressure Drop Increasing','IPT Heater Ext to DA Ext Corrected Pressure Drop Increasing',1,1,1),
(143,'IPT Efficiency Degradation Rebound','IPT Efficiency Degradation Rebound',1,1,1),
(144,'LPT Efficiency Decreasing','LPT Efficiency Decreasing',-1,1,1),
(145,'IP/LP Turbine Seal Leakage Increasing','IP/LP Turbine Seal Leakage Increasing',1,1,1),
(146,'Turbine Cold to Hot Reheat Pressure Drop Increasing','Turbine Cold to Hot Reheat Pressure Drop Increasing',1,1,1),
(147,'Turbine Unnecessary Gland Steam Flow','Turbine Unnecessary Gland Steam Flow',0,1,1),
(148,'Turbine Bearing Vibration Increasing','Turbine Bearing Vibration Increasing',1,1,1),
(149,'Turbine Bearing Oil Temperature Increasing','Turbine Bearing Oil Temperature Increasing',1,1,1),
(150,'Condensate Flow Error Changing','Condensate Flow Error Changing',0,1,1),
(151,'Make-up Flow Increasing','Make-up Flow Increasing',1,1,1),
(152,'Auxillary Power Increasing','Auxillary Power Increasing',1,1,1),
(153,'Feedwater Flow Error Changing','Feedwater Flow Error Changing',0,1,1),
(154,'Boiler Blowdown Flow Increasing','Boiler Blowdown Flow Increasing',1,1,1),
(155,'Net Turbine HR Decreasing','Net Turbine HR Decreasing',-1,1,1),
(156,'Boiler Hot Reheat Steam Temperature Decreasing','Boiler Hot Reheat Steam Temperature Decreasing',-1,1,1),
(157,'Boiler Exit Gas Temperature Increasing','Boiler Exit Gas Temperautre Increasing',1,1,1),
(158,'Boiler dP (Econ) Increasing','Boiler dP (Econ) Increasing',1,1,1),
(159,'Boiler dP (Furnace to Econ) Increasing','Boiler dP (Furnace to Econ) Increasing',1,1,1),
(160,'Boiler dP Increasing','Boiler dP Increasing',1,1,1),
(161,'Boiler Economizer Exit Temperature Stratification Increasing','Boiler Economizer Exit Temperature Stratification Increasing',1,1,1),
(162,'Boiler Economizer Exit O2 Stratification Increasing','Boiler Economizer Exit O2 Stratification Increasing',1,1,1),
(163,'Boiler Average O2 Increasing','Boiler Average O2 Increasing',1,1,1),
(164,'Boiler Cleanliness Decreasing','Boiler Cleanliness Decreasing',-1,1,1),
(165,'Boiler CO Production Increasing','Boiler CO Production Increasing',1,1,1),
(166,'CT Heat Rate Decreasing','CT Heat Rate Decreasing',-1,1,1),
(167,'CT Compressor Inlet Temperature Changing','CT Compressor Inlet Temperature Changing',0,1,1),
(168,'CT Compressor Inlet Differential Pressure Increasing','CT Compressor Inlet Differential Pressure Increasing',1,1,1),
(169,'CT Turbine Exhaust Spread Increasing','CT Turbine Exhaust Spread Increasing',1,1,1),
(170,'CT Turbine 1st Stage Fwd Wheelspace Temperature Increasing','CT Turbine 1st Stage Fwd Wheelspace Temperature Increasing',1,1,1),
(171,'CT Turbine 1st Stage Aft Wheelspace Temperature Increasing','CT Turbine 1st Stage Aft Wheelspace Temperature Increasing',1,1,1),
(172,'CT Turbine 2nd Stage Fwd Wheelspace Temperature Increasing','CT Turbine 2nd Stage Fwd Wheelspace Temperature Increasing',1,1,1),
(173,'CT Turbine 2nd Stage Aft Wheelspace Temperature Increasing','CT Turbine 2nd Stage Aft Wheelspace Temperature Increasing',1,1,1),
(174,'CT Turbine 3rd Stage Fwd Wheelspace Temperature Increasing','CT Turbine 3rd Stage Fwd Wheelspace Temperature Increasing',1,1,1),
(175,'CT Turbine 3rd Stage Aft Wheelspace Temperature Increasing','CT Turbine 3rd Stage Aft Wheelspace Temperature Increasing',1,1,1),
(176,'Scrubber Differential Pressure Increasing','Scrubber Differential Pressure Increasing',1,1,1),
(177,'Increasing rate of permeability loss','Increasing rate of permeability loss',-1,1,1),
(178,'Maintenance wash was not effective','Maintenance wash was not effective',NULL,1,1),
(179,'CIP was not effective','CIP was not effective',NULL,1,1),
(180,'System recovery low','System recovery low',-1,1,1),
(181,'Performance Ratio Decreasing','Performance Ratio Decreasing',-1,1,1),
(182,'Inverter Output Decreasing','Inverter Output Decreasing',-1,1,1),
(183,'String Amperage Decreasing','String Amperage Decreasing',-1,1,1),
(184,'SH Spray Flow Increasing','SH Spray Flow Increasing',1,1,1),
(185,'RH Spray Flow Increasing','RH Spray Flow Increasing',1,1,1),
(186,'Motor Bearing Temperature Increasing','Motor Bearing Temperature Increasing',1,1,1),
(187,'Motor Bearing Vibration Increasing','Motor Bearing Vibration Increasing',1,1,1),
(188,'Coupling Bearing Temperature Increasing','Coupling Bearing Temperature Increasing',1,1,1),
(189,'Coupling Bearing Vibration Increasing','Coupling Bearing Vibration Increasing',1,1,1),
(190,'Mill Bearing Temperature Increasing','Mill Bearing Temperature Increasing',1,1,1),
(191,'Mill Bearing Vibration Increasing','Mill Bearing Vibration Increasing',1,1,1),
(192,'Mill Motor Temperature Increasing','Mill Motor Temperature Increasing',1,1,1),
(193,'Pump Motor Temperature Increasing','Pump Motor Temperature Increasing',1,1,1),
(194,'Auxiliary Condenser Backpressure Increaing','Auxiliary Condenser Backpressure Increaing',1,1,1),
(195,'Auxiliary Condenser TTD Degradation','Auxiliary Condenser TTD Degradation',1,1,1),
(196,'Main Condenser TTD Degradation','Main Condenser TTD Degradation',1,1,1),
(197,'Scrubber Atomizer Vibration Increasing','Scrubber Atomizer Vibration Increasing',1,1,1),
(198,'Scrubber Too Many Recycle Pumps in Service','Scrubber Too Many Recycle Pumps in Service',NULL,1,1),
(199,'Scrubber Too Many Modules In Service','Scrubber Too Many Modules In Service',NULL,1,1),
(200,'Poor Data Quality','Poor Data Quality',NULL,1,1),
(201,'Imported vBulletin Issue of Unknown IssueType','Imported vBulletin Issue of Unknown IssueType',NULL,1,1),
(202,'Fan Bearing Vibration Increasing','Fan Bearing Vibration Increasing',1,1,1),
(203,'Turbine Bearing Temperature Increasing','Turbine Bearing Temperature Increasing',1,1,1),
(204,'Coupling Fluid Temperature Increasing','Coupling Fluid Temperature Increasing',1,1,1),
(205,'Feedwater Heater Level Change','Feedwater Heater Level Change',NULL,1,1),
(206,'Pump Recirculation Flow Increasing','Pump Recirculation Flow Increasing',1,1,1),
(207,'Condensate Water Quality Changes','Condensate Water Quality Changes',0,1,1),
(209,'Turbine Drains Temperature Increasing','Turbine Drains Temperature Increasing',1,1,1),
(210,'Feedwater Heater E-Drains Temperature Increasing','Feedwater Heater E-Drains Temperature Increasing',1,1,1),
(211,'Generator Stator Temperature Increasing','Generator Stator Temperature Increasing',1,1,1),
(212,'Generator Winding Temperature Increasing','Generator Winding Temperature Increasing',1,1,1),
(213,'Generator Hydrogen Temperature Increasing','Generator Hydrogen Temperature Increasing',1,1,1),
(214,'Generator Hydrogen Purity Decreasing','Generator Hydrogen Purity Decreasing',-1,1,1),
(215,'Compressor Motor Amps Increasing','Compressor Motor Amps Increasing',1,1,1),
(216,'Compressor Motor Temperature Increasing','Compressor Motor Temperature Increasing',1,1,1),
(217,'Compressor Motor Bearing Temperature Increasing','Compressor Motor Bearing Temperature Increasing',1,1,1),
(218,'Compressor Bearing Temperature Increasing','Compressor Bearing Temperature Increasing',1,1,1),
(219,'Feed Pump Turbine Steam Flow Increasing','Feed Pump Turbine Steam Flow Increasing',1,1,1),
(220,'Feed Pump Turbine Bearing Vibration Increasing','Feed Pump Turbine Bearing Vibration Increasing',1,1,1),
(221,'Feed Pump Turbine Bearing Temperature Increasing','Feed Pump Turbine Bearing Temperature Increasing',1,1,1),
(222,'Feed Pump Turbine Bearing Oil Temperature Increasing','Feed Pump Turbine Bearing Oil Temperature Increasing',1,1,1),
(223,'SCR Outlet NOx High or Removal Efficiency Low','SCR Outlet NOx High or Removal Efficiency Low',-1,1,1),
(224,'Boiler Efficiency Decreasing','Boiler Efficiency Decreasing',-1,1,1),
(225,'Unit Heat Rate Increasing','Unit Heat Rate Increasing',1,1,1),
(226,'Turbine Cycle Heat Rate Increasing','Turbine Cycle Heat Rate Increasing',1,1,1),
(227,'Pump Bearing Temperature Increasing','Pump Bearing Temperature Increasing',1,1,1),
(228,'HP Exhaust or Cold Reheat Temperature Low','HP Exhaust or Cold Reheat Temperature Low',-1,1,1),
(229,'MS Outlet or HPT Inlet Valves Position or dP Changes','MS Outlet or HPT Inlet Valves Position or dP Changes',0,1,1),
(230,'Feedwater Heater Drain Valve Changes','Feedwater Heater Drain Valve Changes',0,1,1),
(231,'Circulating Water Inlet Temperature Increasing','Circulating Water Inlet Temperature Increasing',1,1,1),
(232,'Condenser Hotwell Level or Makeup Flow Changes','Condenser Hotwell Level or Makeup Flow Changes',0,1,1),
(233,'Feedwater Water Quality Changes','Feedwater Water Quality Changes',0,1,1),
(234,'Drum Water Quality Changes','Drum Water Quality Changes',0,1,1),
(235,'Main Steam Water Quality Changes','Main Steam Water Quality Changes',0,1,1),
(236,'Primary Air Temperature Low','Primary Air Temperature Low',-1,1,1),
(237,'Secondary Air Temperature Low','Secondary Air Temperature Low',-1,1,1),
(238,'SCR Inlet Temperature High','SCR Inlet Temperature High',1,1,1),
(239,'HRH Outlet or IPT Inlet Valves Position or dP Changes','HRH Outlet or IPT Inlet Valves Position or dP Changes',0,1,1),
(240,'Condenser Cooling Water Quality Changes','Condenser Cooling Water Quality Changes',NULL,1,1),
(241,'Generator Cooling Water Quality Changes','Generator Cooling Water Quality Changes',NULL,1,1),
(242,'Air Heater Leakage Increasing','Air Heater Leakage Increasing',1,1,1),
(243,'SCR Differential Pressure Increasing','SCR Differential Pressure Increasing',1,1,1),
(260,'Arc Flash Recommended Actions','Arc Flash Recommended Actions',NULL,1,1),
(261,'Arc Flash Label','Arc Flash Label',NULL,1,1),
(262,'Information Inquiry Issue','Information Inquiry Issue',NULL,1,1),
(263,'Generic Project Management Issue','Generic Project Management Issue',NULL,1,1),
(264,'Generic Unassigned Issue','Generic Unassigned Issue',NULL,1,1),
(265,'Generic PMO Issue','Generic PMO Issue',NULL,1,1),
(266,'Generic Deliverable Issue','Generic Deliverable Issue',NULL,1,1),
(267,'Generic OT Issue','Generic OT Issue',NULL,1,1),
(268,'Generic IT Issue','Generic IT Issue',NULL,1,1),
(269,'Generic Telecom Issue','Generic Telecom Issue',NULL,1,1),
(270,'Generic Safety Issue','Generic Safety Issue',NULL,1,1),
(271,'Generic Engineering Issue','Generic Engineering Issue',NULL,1,1),
(272,'Generic Procurement Issue','Generic Procurement Issue',NULL,1,1),
(273,'Generic Permitting Issue','Generic Permitting Issue',NULL,1,1),
(274,'Generic Construction Issue','Generic Construction Issue',NULL,1,1),
(275,'Generic Closeout Issue','Generic Closeout Issue',NULL,1,1),
(276,'Generic Scope Issue','Generic Scope Issue',NULL,1,1),
(277,'Generic Planning Issue','Generic Planning Issue',NULL,1,1),
(278,'Generic Design Review Issue','Generic Design Review Issue',NULL,1,1),
(279,'Generic Strategic Sourcing Issue','Generic Strategic Sourcing Issue',NULL,1,1),
(280,'Generic Equipment Procurement Issue','Generic Equipment Procurement Issue',NULL,1,1),
(281,'Generic Testing Issue','Generic Testing Issue',NULL,1,1),
(282,'Generic Other Issue','Generic Other Issue',NULL,1,1),

-- PBI 50814, technically Generic Pole Management isn't needed but bringing it up to date
(283,'Generic Pole Management','Generic Pole Management',NULL,1,1),
(284,'Imbalance','Imbalance',NULL,1,1),
(285,'Looseness','Looseness',NULL,1,1),
(286,'Misalignment','Misalignment',NULL,1,1),
(287,'Bent Shaft','Bent Shaft',NULL,1,1),
(288,'Rotating Looseness','Rotating Looseness',NULL,1,1),
(289,'Vane Pass','Vane Pass',NULL,1,1),
(290,'Blade Pass','Blade Pass',NULL,1,1),
(291,'Cavitation','Cavitation',NULL,1,1),
(292,'Bearing Fault','Bearing Fault',NULL,1,1),
(293,'Belt Issues','Belt Issues',NULL,1,1),
(294,'Gearmesh','Gearmesh',NULL,1,1),
(295,'Other','Other',NULL,1,1)
)
AS SOURCE([IssueTypeID],[IssueTypeDesc],[IssueTypeAbbrev],[TrendDirection],[IsBVApproved],[CreatedByUserID])
ON Target.[IssueTypeID]= Source.[IssueTypeID]
WHEN MATCHED THEN UPDATE SET
	[IssueTypeDesc]=Source.[IssueTypeDesc],
	[IssueTypeAbbrev]=Source.[IssueTypeAbbrev],
	[TrendDirection]=Source.[TrendDirection],
	[IsBVApproved]=Source.[IsBVApproved],
	[CreatedByUserID]=Source.[CreatedByUserID]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([IssueTypeID],[IssueTypeDesc],[IssueTypeAbbrev],[TrendDirection],[IsBVApproved],[CreatedByUserID])
VALUES ([IssueTypeID],[IssueTypeDesc],[IssueTypeAbbrev],[TrendDirection],[IsBVApproved],[CreatedByUserID]);

ENABLE TRIGGER [tIssueType].ValidateIssueTypeInsertUpdate ON [Diagnostics].[tIssueType]
GO

DISABLE TRIGGER [tIssueCauseType].ValidateIssueCauseTypeInsertUpdate ON [Diagnostics].[tIssueCauseType]
GO

MERGE INTO Diagnostics.tIssueCauseType AS TARGET USING (VALUES
(14,6,'Condenser Backpressure Degradation due to Backpressure Measurement Error','Backpressure Measurement Error',1,1)
,(15,6,'Condenser Backpressure Degradation due to Cycle Isolation Dumping Extra Heat to Condenser','Cycle Isolation Dumping Extra Heat to Condenser',1,1)
,(16,6,'Condenser Backpressure Degradation due to Inadequate Air Removal Equipment Performance','Inadequate Air Removal Equipment Performance',1,1)
,(17,6,'Condenser Backpressure Degradation due to Excess Air In-Leakage','Excess Air In-Leakage',1,1)
,(18,6,'Condenser Backpressure Degradation due to Air Binding in the Condenser Waterbox','Air Binding in the Condenser Waterbox',1,1)
,(19,6,'Condenser Backpressure Degradation due to Condenser Tube Sheet Blockage','Condenser Tube Sheet Blockage',1,1)
,(20,6,'Condenser Backpressure Degradation due to Condenser Tube Fouling','Condenser Tube Fouling',1,1)
,(21,6,'Condenser Backpressure Degradation due to Cooling Water Flow Rate Measurement Error','Cooling Water Flow Rate Measurement Error',1,1)
,(32,9,'Condenser Subcooling Increasing due to Backpressure Measurement Error','Backpressure Measurement Error',1,1)
,(33,9,'Condenser Subcooling Increasing due to High Waterbox Level','High Waterbox Level',1,1)
,(34,9,'Condenser Subcooling Increasing due to Too Many CWP In Service','Too Many CWP In Service',1,1)
,(35,10,'Condenser Waterbox Pressure Drop Increasing due to Air Binding in the Condenser Waterbox','Air Binding in the Condenser Waterbox',1,1)
,(36,10,'Condenser Waterbox Pressure Drop Increasing due to Condenser Tube Sheet Blockage','Condenser Tube Sheet Blockage',1,1)
,(37,10,'Condenser Waterbox Pressure Drop Increasing due to Condenser Tube Fouling','Condenser Tube Fouling',1,1)
,(38,11,'Condenser Drains Temperature Increasing due to Hot Drain Leaking to Condenser','Hot Drain Leaking to Condenser',1,1)
,(70,21,'Steam Generator Low Throttle Steam Temperature due to Excessive Spray Flow (Spray valve issue)','Excessive Spray Flow (Spray valve issue)',1,1)
,(71,21,'Steam Generator Low Throttle Steam Temperature due to Low Boiler Section Cleanliness','Low Boiler Section Cleanliness',1,1)
,(72,21,'Steam Generator Low Throttle Steam Temperature due to Excessive Spray Flow (Control issue)','Excessive Spray Flow (Control issue)',1,1)
,(73,21,'Steam Generator Low Throttle Steam Temperature due to Instrumentation','Instrumentation',1,1)
,(74,22,'Steam Generator Low Hot Reheat 1 Steam Temperature due to Excessive Spray Flow (Spray valve issue)','Excessive Spray Flow (Spray valve issue)',1,1)
,(75,22,'Steam Generator Low Hot Reheat 1 Steam Temperature due to Low RH or High WW/SH Section Cleanliness','Low RH or High WW/SH Section Cleanliness',1,1)
,(76,22,'Steam Generator Low Hot Reheat 1 Steam Temperature due to Excessive Spray Flow (Control issue)','Excessive Spray Flow (Control issue)',1,1)
,(77,22,'Steam Generator Low Hot Reheat 1 Steam Temperature due to Instrumentation','Instrumentation',1,1)
,(78,23,'Steam Generator Low Hot Reheat 2 Steam Temperature due to Excessive Spray Flow (Spray valve issue)','Excessive Spray Flow (Spray valve issue)',1,1)
,(79,23,'Steam Generator Low Hot Reheat 2 Steam Temperature due to Low RH or High WW/SH Section Cleanliness','Low RH or High WW/SH Section Cleanliness',1,1)
,(80,23,'Steam Generator Low Hot Reheat 2 Steam Temperature due to Excessive Spray Flow (Control issue)','Excessive Spray Flow (Control issue)',1,1)
,(81,23,'Steam Generator Low Hot Reheat 2 Steam Temperature due to Instrumentation','Instrumentation',1,1)
,(100,31,'HP Feedwater Heaters System TTD Degradation due to Leaking or Partially Open Bypass Valve','Leaking or Partially Open Bypass Valve',1,1)
,(101,31,'HP Feedwater Heaters System TTD Degradation due to Tube Leak','Tube Leak',1,1)
,(102,31,'HP Feedwater Heaters System TTD Degradation due to Cycle Isolation Errror (Affects Calculated FW Flow Rate)','Cycle Isolation Errror (Affects Calculated FW Flow Rate)',1,1)
,(103,31,'HP Feedwater Heaters System TTD Degradation due to Excessive Extraction Line Pressure Drop','Excessive Extraction Line Pressure Drop',1,1)
,(104,31,'HP Feedwater Heaters System TTD Degradation due to High Water Level','High Water Level',1,1)
,(105,31,'HP Feedwater Heaters System TTD Degradation due to Excessive Vent Orifice Flow','Excessive Vent Orifice Flow',1,1)
,(106,31,'HP Feedwater Heaters System TTD Degradation due to Shell Side Air Blanketing','Shell Side Air Blanketing',1,1)
,(107,31,'HP Feedwater Heaters System TTD Degradation due to Leaking Division Plate','Leaking Division Plate',1,1)
,(108,31,'HP Feedwater Heaters System TTD Degradation due to Dirty Heater','Dirty Heater',1,1)
,(109,31,'HP Feedwater Heaters System TTD Degradation due to Internal Shell Side Leak','Internal Shell Side Leak',1,1)
,(149,39,'Air Heater Cold-End-Average Temperature HR Loss Cause','Default Cause',1,1)
,(150,40,'Air Heater Gas Outlet Temp (AH Heat Transfer) HR Loss Cause','Default Cause',1,1)
,(151,41,'Air Heater Leakage HR Loss Cause','Default Cause',1,1)
,(152,42,'Auxilliary Power HR Loss Cause','Default Cause',1,1)
,(153,43,'Condenser Backpressure HR Loss Cause','Default Cause',1,1)
,(154,44,'Condenser Backpressure (HP) HR Loss Cause','Default Cause',1,1)
,(155,45,'Condenser Backpressure (LP) HR Loss Cause','Default Cause',1,1)
,(156,46,'Condenser Subcooling HR Loss Cause','Default Cause',1,1)
,(157,47,'Cycle Mass Loss HR Loss Cause','Default Cause',1,1)
,(158,48,'Econ Gas Outlet Temp (Sootblowing) HR Loss Cause','Default Cause',1,1)
,(159,49,'Excess O2 at Economizer Exit HR Loss Cause','Default Cause',1,1)
,(160,50,'Generator Hydrogen Pressure HR Loss Cause','Default Cause',1,1)
,(161,51,'Hot Reheat Steam Temp at Boiler HR Loss Cause','Default Cause',1,1)
,(162,52,'Hot Reheat Steam Temp at Boiler (RH 2) HR Loss Cause','Default Cause',1,1)
,(163,53,'HPT Efficiency HR Loss Cause','Default Cause',1,1)
,(164,54,'IPT Efficiency HR Loss Cause','Default Cause',1,1)
,(165,55,'Main Steam Temp at Boiler HR Loss Cause','Default Cause',1,1)
,(166,56,'RH Pressure Drop from HPT to IPT HR Loss Cause','Default Cause',1,1)
,(167,57,'RH Pressure Drop from HPT to IPT (RH 2) HR Loss Cause','Default Cause',1,1)
,(168,58,'RH Spray Flowrate HR Loss Cause','Default Cause',1,1)
,(169,59,'RH Spray Flowrate (RH 2) HR Loss Cause','Default Cause',1,1)
,(170,60,'SH Spray Flowrate HR Loss Cause','Default Cause',1,1)
,(171,61,'Throttle Steam Pressure HR Loss Cause','Default Cause',1,1)
,(172,62,'Top Feedwater Heater External TTD HR Loss Cause','Default Cause',1,1)
,(173,63,'VHPT Efficiency HR Loss Cause','Default Cause',1,1)
,(174,64,'Evap Cooler Eff Down due to Weather Station Ambient Temperature or Relative Humidity Instrumentation Error','Weather Station Ambient Temperature or Relative Humidity Instrumentation Error',1,1)
,(175,64,'Evap Cooler Eff Down due to Water Supply Header Blockage / Leak','Water Supply Header Blockage / Leak',1,1)
,(176,64,'Evap Cooler Eff Down due to Evap Cooler Pump or Pump Motor Performance Degradation','Water Pump or Pump Motor Performance Degradation',1,1)
,(177,64,'Evap Cooler Eff Down due to Water Distribution Manifold Issues','Water Distribution Manifold Issues',1,1)
,(178,64,'Evap Cooler Eff Down due to High Ambient Relative Humidity','High Ambient Relative Humidity',1,1)
,(179,65,'CT Air Intake Filter DP Increasing due to Air Intake Filter Debris Buildup',' Air Intake Filter Debris Buildup',1,1)
,(180,65,'CT Air Intake Filter DP Increasing due to Pressure Transmitter supply line pluggage','DP Pressure Transmitter Supply Line Pluggage',1,1)
,(181,66,'Fuel Gas Temperature Drop due to FG Heater IP FW CV Malfunction','FG Heater IP FW CV Malfunction',1,1)
,(182,66,'Fuel Gas Temperature Drop due to FG Heater IP FW Temperature Decrease','FG Heater IP FW Temperature Decrease',1,1)
,(183,66,'Fuel Gas Temperature Drop due to FG Heater Tube Fouling & Plugging','FG Heater Tube Fouling & Plugging',1,1)
,(184,66,'Fuel Gas Temperature Drop due to FG Yard Dew Point Heater Malfunction','FG Yard Dew Point Heater Malfunction',1,1)
,(185,66,'Fuel Gas Temperature Drop due to FG Supply Temperature Decrease','FG Supply Temperature Decrease',1,1)
,(186,67,'Compressor Inlet Pressure Decreasing due to Air Intake Filters Debris Buildup','Air Intake Filters Debris Buildup',1,1)
,(187,67,'Compressor Inlet Pressure Decreasing due to Barometric Pressure Drop','Barometric Pressure Drop',1,1)
,(188,68,'Compressor Discharge Pressure Degradation due to Deposition /  Fouling of Compressor Blades','Deposition /  Fouling of Compressor Blades',1,1)
,(189,68,'Compressor Discharge Pressure Degradation due to Blade Failure or Seal Damage or Rub','Blade Failure or Seal damage or Rub',1,1)
,(190,68,'Compressor Discharge Pressure Degradation due to Blade Erosion or Seal Strip Wear ( May be Normal Degradation)','Blade Erosion or Seal Strip Wear ( May be Normal Degradation)',1,1)
,(191,68,'Compressor Discharge Pressure Degradation due to Excessive Cooling Air Losses ( Compressor Extraction Flows High / Failed Extraction CV)','Excessive Cooling Air Losses ( Compressor Extraction Flows High / Failed Extraction CV)',1,1)
,(192,68,'Compressor Discharge Pressure Degradation due to Low Compressor Inlet Pressure','Low Compressor Inlet Pressure',1,1)
,(193,68,'Compressor Discharge Pressure Degradation due to IGV Position Change','IGV Position Change',1,1)
,(194,69,'Compressor Pressure Ratio Decreasing due to Deposition /  Fouling of Compressor Blades','Deposition /  Fouling of Compressor Blades',1,1)
,(195,69,'Compressor Pressure Ratio Decreasing due to Blade Failure or Seal damage or Rub','Blade Failure or Seal damage or Rub',1,1)
,(196,69,'Compressor Pressure Ratio Decreasing due to Blade Erosion or Seal Strip Wear ( May be Normal Degradation)','Blade Erosion or Seal Strip Wear ( May be Normal Degradation)',1,1)
,(197,69,'Compressor Pressure Ratio Decreasing due to Excessive Cooling Air Losses ( Compressor Extraction Flows High / Failed Extraction CV)','Excessive Cooling Air Losses ( Compressor Extraction Flows High / Failed Extraction CV)',1,1)
,(198,69,'Compressor Pressure Ratio Decreasing due to Low Compressor Inlet Pressure',' Low Compressor Inlet Pressure',1,1)
,(199,69,'Compressor Pressure Ratio Decreasing due to IGV Position Change','IGV Position Change',1,1)
,(200,70,'Compressor Temperature Ratio Increase due to Decreasing Compressor Efficiency ( IF Constant Pressure Ratio)','Decreasing Compressor Efficiency ( IF Constant Pressure Ratio)',1,1)
,(201,71,'Compressor Isentropic Efficiency Degradation due to Deposition /  Fouling of Compressor Blades','Deposition /  Fouling of Compressor Blades',1,1)
,(202,71,'Compressor Isentropic Efficiency Degradation due to Blade Failure, Seal Damage, or Rub','Blade Failure, Seal Damage, or Rub',1,1)
,(203,71,'Compressor Isentropic Efficiency Degradation due to Blade Erosion or Seal Strip Wear ( May be Normal Degradation)','Blade Erosion or Seal Strip Wear ( May be Normal Degradation)',1,1)
,(204,71,'Compressor Isentropic Efficiency Degradation due to Excessive Cooling Air Losses ( Compressor Extraction Flows High / Failed Extraction CV)','Excessive Cooling Air Losses ( Compressor Extraction Flows High / Failed Extraction CV)',1,1)
,(205,71,'Compressor Isentropic Efficiency Degradation due to Low Compressor Inlet Pressure','Low Compressor Inlet Pressure',1,1)
,(206,71,'Compressor Isentropic Efficiency Degradation due to IGV Position Change','IGV Position Change',1,1)
,(207,71,'Compressor Isentropic Efficiency Degradation due to Incorrect Calculated Flow ( False Alarm, Instrument Error input to Calc)','Incorrect Calculated Work ( False Alarm, Instrument Error input to Calc)',1,1)
,(208,71,'Compressor Isentropic Efficiency Degradation due to Incorrect Compressor Discharge Pressure ( Instrumention Issue)','Incorrect Compressor Discharge Pressure ( Instrumention Issue)',1,1)
,(209,72,'CT Corrected Gross Heat Rate (HHV) Increasing due to Compressor Efficiency Degradation','Compressor Efficiency Degradation',1,1)
,(210,72,'CT Corrected Gross Heat Rate (HHV) Increasing due to Low Compressor Discharge Pressure','Low Compressor Discharge Pressure',1,1)
,(211,72,'CT Corrected Gross Heat Rate (HHV) Increasing due to High CT Exhaust Pressure','High CT Exhaust Pressure',1,1)
,(212,72,'CT Corrected Gross Heat Rate (HHV) Increasing due to Low Compressor Discharge Temperature ( T Fire Down)','Low Compressor Discharge Temperature ( Tfire Down)',1,1)
,(213,72,'CT Corrected Gross Heat Rate (HHV) Increasing due to High Turbine Exhaust Temperature','High Turbine Exhaust Temperature',1,1)
,(214,73,'CT Gross Heat Rate (HHV) Increasing due to Compressor Efficiency Degradation','Compressor Efficiency Degradation',1,1)
,(215,73,'CT Gross Heat Rate (HHV) Increasing due to Low Compressor Discharge Pressure','Low Compressor Discharge Pressure',1,1)
,(216,73,'CT Gross Heat Rate (HHV) Increasing due to High CT Exhaust Pressure','High CT Exhaust Pressure',1,1)
,(217,73,'CT Gross Heat Rate (HHV) Increasing due to Low Compressor Discharge Temperature ( T Fire Down)','Low Compressor Discharge Temperature ( Tfire Down)',1,1)
,(218,73,'CT Gross Heat Rate (HHV) Increasing due to High Turbine Exhaust Temperature','High Turbine Exhaust Temperature',1,1)
,(219,73,'CT Gross Heat Rate (HHV) Increasing due to Evap Cooler Eff Down or OOS','Evap Cooler Eff Down',1,1)
,(220,73,'CT Gross Heat Rate (HHV) Increasing due to High Ambient Temperature','High Ambient Temperature',1,1)
,(221,73,'CT Gross Heat Rate (HHV) Increasing due to High Ambient Relative Humidity','High Ambient Relative Humidity',1,1)
,(222,74,'CT Corrected Gross Power Output Decreasing due to Compressor Efficiency Degradation','Compressor Efficiency Degradation',1,1)
,(223,74,'CT Corrected Gross Power Output Decreasing due to Low Compressor Discharge Pressure','Low Compressor Discharge Pressure',1,1)
,(224,74,'CT Corrected Gross Power Output Decreasing due to High CT Exhaust Pressure','High CT Exhaust Pressure',1,1)
,(225,74,'CT Corrected Gross Power Output Decreasing due to Low Compressor Discharge Temperature ( T Fire Down)','Low Compressor Discharge Temperature ( Tfire Down)',1,1)
,(226,74,'CT Corrected Gross Power Output Decreasing due to High Turbine Exhaust Temperature','High Turbine Exhaust Temperature',1,1)
,(227,75,'CT Gross Power Output Decreasing due to Compressor Efficiency Degradation','Compressor Efficiency Degradation',1,1)
,(228,75,'CT Gross Power Output Decreasing due to Low Compressor Discharge Pressure','Low Compressor Discharge Pressure',1,1)
,(229,75,'CT Gross Power Output Decreasing due to High CT Exhaust Pressure','High CT Exhaust Pressure',1,1)
,(230,75,'CT Gross Power Output Decreasing due to Low Compressor Discharge Temperature ( T Fire Down)','Low Compressor Discharge Temperature ( Tfire Down)',1,1)
,(231,75,'CT Gross Power Output Decreasing due to High Turbine Exhaust Temperature','High Turbine Exhaust Temperature',1,1)
,(232,75,'CT Gross Power Output Decreasing due to Evap Cooler Eff Down or OOS','Evap Cooler Eff Down',1,1)
,(233,75,'CT Gross Power Output Decreasing due to High Ambient Temperature','High Ambient Temperature',1,1)
,(234,75,'CT Gross Power Output Decreasing due to High Ambient Relative Humidity','High Ambient Relative Humidity',1,1)
,(235,76,'CT Exhaust Backpressure Increasing due to HRSG Flow Straightening Grid Obstruction','HRSG Flow Straightening Grid Obstruction',1,1)
,(236,76,'CT Exhaust Backpressure Increasing due to HRSG Tube Fouling','HRSG Tube Fouling',1,1)
,(237,76,'CT Exhaust Backpressure Increasing due to SCR Catalyst pluggage.','SCR Catalyst pluggage.',1,1)
,(238,77,'HRSG Thermal Efficiency Degradation due to Increase Stack Gas Loss ( Stack Enthalpy Increase)','Increase Stack Gas Loss ( Stack Enthalpy Increase)',1,1)
,(239,77,'HRSG Thermal Efficiency Degradation due to Tube Fouling','Tube Fouling',1,1)
,(240,77,'HRSG Thermal Efficiency Degradation due to Tube Scaling','Tube Scaling',1,1)
,(241,77,'HRSG Thermal Efficiency Degradation due to Excessive Blowdown','Excessive Blowdown',1,1)
,(242,77,'HRSG Thermal Efficiency Degradation due to Increase in HRSG Leakage','Increase in HRSG Leakage',1,1)
,(243,77,'HRSG Thermal Efficiency Degradation due to Steam Power Aug In Service','Steam Power Aug In Service',1,1)
,(244,78,'HRSG LP Pinch Point Increasing due to Tube Fouling ( T gas Up)','Tube Fouling ( T gas Up)',1,1)
,(245,78,'HRSG LP Pinch Point Increasing due to Tube Scaling ( T gas Up)','Tube Scaling ( T gas Up)',1,1)
,(246,78,'HRSG LP Pinch Point Increasing due to Change in Economizer Bypass Valve postion. ( TWF Down)','Change in Economizer Bypass Valve postion. ( TWF Down)',1,1)
,(247,79,'HRSG LP Effectiveness Decreasing due to Tube Fouling','Tube Fouling',1,1)
,(248,79,'HRSG LP Effectiveness Decreasing due to Tube Scaling','Tube Scaling',1,1)
,(249,79,'HRSG LP Effectiveness Decreasing due to Low Economizer Gas Temperature','Low Economizer Gas Temperature',1,1)
,(250,80,'HRSG IP Pinch Point Increasing due to Tube Fouling ( T gas Up)','Tube Fouling ( T gas Up)',1,1)
,(251,80,'HRSG IP Pinch Point Increasing due to Tube Scaling ( T gas Up)','Tube Scaling ( T gas Up)',1,1)
,(252,80,'HRSG IP Pinch Point Increasing due to Low IP FW Temp','Low IP FW Temp',1,1)
,(253,81,'HRSG IP Effectiveness Decreasing due to Tube Fouling','Tube Fouling',1,1)
,(254,81,'HRSG IP Effectiveness Decreasing due to Tube Scaling','Tube Scaling',1,1)
,(255,82,'HRSG HP Pinch Point Increasing due to Tube Fouling ( T gas Up)','Tube Fouling ( T gas Up)',1,1)
,(256,82,'HRSG HP Pinch Point Increasing due to Tube Scaling ( T gas Up)','Tube Scaling ( T gas Up)',1,1)
,(257,82,'HRSG HP Pinch Point Increasing due to Low HP FW Temp','Low HP FW Temp',1,1)
,(258,83,'HRSG HP Effectiveness Decreasing due to Tube Fouling','Tube Fouling',1,1)
,(259,83,'HRSG HP Effectiveness Decreasing due to Tube Scaling','Tube Scaling',1,1)
,(260,84,'HRSG Steam Production Decreasing ( Same CT Exhaust Conditions) due to Tube Fouling','Tube Fouling',1,1)
,(261,84,'HRSG Steam Production Decreasing ( Same CT Exhaust Conditions) due to Exhuast Gas Leakage','Exhuast Gas Leakage',1,1)
,(262,85,'HRSG Steam Production Decreasing ( Part Load Conditions) due to IGVs Not in Exhaust Gas Temp Control Mode','IGVs Not in Exhaust Gas Temp Control Mode',1,1)
,(263,86,'HRSG FW Flow Demand Increasing due to Excessive Blowdown','Excessive Blowdown',1,1)
,(264,86,'HRSG FW Flow Demand Increasing due to Tube Leak','Tube Leak',1,1)
,(265,87,'HRSG HP Steam Temperature Low due to Excessive Desuperheating Spray','Excessive Desuperheating Spray',1,1)
,(266,87,'HRSG HP Steam Temperature Low due to Low CT Exhaust Temp','Low CT Exhaust Temp',1,1)
,(267,88,'HRSG HRH Steam Temperature Low due to Excessive Desuperheating Spray','Excessive Desuperheating Spray',1,1)
,(268,88,'HRSG HRH Steam Temperature Low due to Low CT Exhaust Temp','Low CT Exhaust Temp',1,1)
,(269,89,'HRSG LP Steam Enthalpy Drop due to  Degrading RAC Performance','Degrading RAC Performance',1,1)
,(270,89,'HRSG LP Steam Enthalpy Drop due to Excessive RAC Blowdown','Excessive RAC Blowdown',1,1)
,(271,90,'RAC Blowdown Increase due to Drain Valve Malfunction','Drain Valve Malfunction',1,1)
,(272,91,'Rotor Air Temperature from RAC High due to RAC Tube Pluggage','RAC Tube Pluggage',1,1)
,(273,91,'Rotor Air Temperature from RAC High due Excessive RAC Blowdown','Excessive RAC Blowdown',1,1)
,(274,92,'SCR Outlet Nox High due to Catalyst Degradation','Catalyst Degradation',1,1)
,(275,92,'SCR Outlet Nox High due to Catalyst Pluggage','Catalyst Pluggage',1,1)
,(276,92,'SCR Outlet Nox High due to due to Low Reagent Flow or Flow CV Failure','Low Reagent Flow or Flow CV Failure',1,1)
,(277,92,'SCR Outlet Nox High due to increase in CT Exhaust Gas Nox (DLN System Degredation)','CT Exhaust Gas Nox (DLN System Degredation)',1,1)
,(278,93,'Stack Temperature Low ( Possible Subcooling in Economizer) due to Exhaust Gas Leakage','Exhaust Gas Leakage',1,1)
,(279,93,'Stack Temperature Low ( Possible Subcooling in Economizer) due to Low CT Exhaust Temp','Low CT Exhaust Temp',1,1)
,(280,94,'Stack Temperature High due to DBs In Service','DBs In Service',1,1)
,(281,95,'Throttle, Governing, or Main Stop Valve Problem','Throttle, Governing, or Main Stop Valve Problem',1,1)
,(282,95,'Mesh Screen Plugging','Mesh Screen Plugging',1,1)
,(283,95,'Nozzle or Control Stage Erosion','Nozzle or Control Stage Erosion',1,1)
,(284,95,'Nozzle or Control Stage Failure','Nozzle or Control Stage Failure',1,1)
,(285,95,'Nozzle or Control Stage Deposits','Nozzle or Control Stage Deposits',1,1)
,(286,95,'Deposition','Deposition',1,1)
,(287,95,'Steam Path Blockage','Steam Path Blockage',1,1)
,(288,95,'Seal Wear or Blade/Diaphragm Erosion','Seal Wear or Blade/Diaphragm Erosion',1,1)
,(289,95,'Seal Damage or Rub','Seal Damage or Rub',1,1)
,(290,95,'Mechanical Damage','Mechanical Damage',1,1)
,(291,96,'Fan Damper Improperly Opening','Fan Damper Improperly Opening',1,1)
,(292,96,'Flow Obstruction','Flow Obstruction',1,1)
,(293,96,'Fan Blade Damage','Fan Blade Damage',1,1)
,(294,97,'Mechanical Looseness','Mechanical Looseness',1,1)
,(295,97,'Shaft Misalignment','Shaft Misalignment',1,1)
,(296,97,'Bearing Failure','Bearing Failure',1,1)
,(297,97,'Equipment Mechanical Damage','Equipment Mechanical Damage',1,1)
,(298,98,'Excessive Spray Flow (Spray valve issue)','Excessive Spray Flow (Spray valve issue)',1,1)
,(299,98,'Low Boiler Section Cleanliness','Low Boiler Section Cleanliness',1,1)
,(300,98,'Excessive Spray Flow (Control issue)','Excessive Spray Flow (Control issue)',1,1)
,(301,98,'Instrumentation','Instrumentation',1,1)
,(302,99,'Coal Quality','Coal Quality',1,1)
,(303,99,'Mill Wear','Mill Wear',1,1)
,(304,100,'Fouled Compressor','Fouled Compressor',1,1)
,(305,100,'Instrumentation Error','Instrumentation Error',1,1)
,(306,100,'Compressor Blade Damage','Compressor Blade Damage',1,1)
,(307,100,'Compressor Bleed Valve Leaking','Compressor Bleed Valve Leaking',1,1)
,(308,101,'Valve Position Error','Valve Position Error',1,1)
,(309,101,'Heater Malfunction','Heater Malfunction',1,1)
,(310,102,'Turbine Seal Damage','Turbine Seal Damage',1,1)
,(311,102,'Fouled Compressor','Fouled Compressor',1,1)
,(312,102,'Instrumentation Error','Instrumentation Error',1,1)
,(313,102,'Turbine Bucket or Nozzle Damage','Turbine Bucket or Nozzle Damage',1,1)
,(314,102,'Combustion Dynamics','Combustion Dynamics',1,1)
,(315,103,'Blocked Cooling Passages','Blocked Cooling Passages',1,1)
,(316,103,'Damaged Fan Blades','Damaged Fan Blades',1,1)
,(317,103,'Generator Thermal Sensitivity','Generator Thermal Sensitivity',1,1)
,(318,104,'Liner or Transition Piece Leak','Liner or Transition Piece Leak',1,1)
,(319,104,'Installation Error','Installation Error',1,1)
,(320,104,'Clogged Fuel Nozzle','Clogged Fuel Nozzle',1,1)
,(321,104,'Water/Steam to Fuel Ratio Error','Water/Steam to Fuel Ratio Error',1,1)
,(322,105,'Mill Wear','Mill Wear',1,1)
,(323,105,'Coal Quality','Coal Quality',1,1)
,(324,106,'TA Dampers Leaking','TA Dampers Leaking',1,1)
,(325,106,'Inadequate PA Flow','Inadequate PA Flow',1,1)
,(326,106,'High Coal Moisture','High Coal Moisture',1,1)
,(327,106,'Control Problem','Control Problem',1,1)
,(328,107,'Mill Fire','Mill Fire',1,1)
,(329,107,'Control Problem','Control Problem',1,1)
,(330,108,'Too Many Pulverizers in Service','Too Many Pulverizers in Service',1,1)
,(331,109,'Clogged Suction Filter','Clogged Suction Filter',1,1)
,(332,109,'Impeller Wear','Impeller Wear',1,1)
,(333,109,'Seal Wear (Internal Recirculation)','Seal Wear (Internal Recirculation)',1,1)
,(334,110,'Flow Obstruction','Flow Obstruction',1,1)
,(335,110,'Pump Impeller Damage','Pump Impeller Damage',1,1)
,(336,111,'Seal Wear','Seal Wear',1,1)
,(337,111,'Impeller Wear','Impeller Wear',1,1)
,(338,112,'Too Many Pumps in Service','Too Many Pumps in Service',1,1)
,(339,113,'HP Steam Control Valve Leaking','HP Steam Control Valve Leaking',1,1)
,(340,113,'Pump Impeller Wear','Pump Impeller Wear',1,1)
,(341,113,'Pump Seal Wear','Pump Seal Wear',1,1)
,(342,113,'Turbine Wear or Damage','Turbine Wear or Damage',1,1)
,(343,114,'Fan Damper Improperly Opening','Fan Damper Improperly Opening',1,1)
,(344,114,'Flow Obstruction','Flow Obstruction',1,1)
,(345,114,'Air Heater Leakage','Air Heater Leakage',1,1)
,(346,114,'Fan Blade Damage','Fan Blade Damage',1,1)
,(347,115,'Fan Damper on Opposite Fan Improperly Opening','Fan Damper on Opposite Fan Improperly Opening',1,1)
,(348,115,'Flow Obstruction on Opposite Fan','Flow Obstruction on Opposite Fan',1,1)
,(349,115,'Air Heater Leakage','Air Heater Leakage',1,1)
,(350,115,'Fan Blade Damage','Fan Blade Damage',1,1)
,(351,116,'Fan Damper on Opposite Fan Improperly Opening','Fan Damper on Opposite Fan Improperly Opening',1,1)
,(352,116,'Upstream Draft Pressure Loss','Upstream Draft Pressure Loss',1,1)
,(353,116,'Upstream Air Leakage or Infiltration','Upstream Air Leakage or Infiltration',1,1)
,(354,116,'Fan Blade Damage','Fan Blade Damage',1,1)
,(355,116,'Ash or Scrubber Carryover Buildup','Ash or Scrubber Carryover Buildup',1,1)
,(356,117,'Inadequate Lubrication or Cooling','Inadequate Lubrication or Cooling',1,1)
,(357,117,'Shaft Misalignment','Shaft Misalignment',1,1)
,(358,117,'Bearing Failure','Bearing Failure',1,1)
,(359,118,'Dirty Filter','Dirty Filter',1,1)
,(360,118,'Motor Failing','Motor Failing',1,1)
,(361,119,'Air Heater Leakage','Air Heater Leakage',1,1)
,(362,119,'System Pressure Loss','System Pressure Loss',1,1)
,(363,119,'Blade Wear','Blade Wear',1,1)
,(364,119,'Blade Fouling','Blade Fouling',1,1)
,(365,120,'Air Heater Leakage','Air Heater Leakage',1,1)
,(366,120,'System Pressure Loss','System Pressure Loss',1,1)
,(367,120,'Blade Wear','Blade Wear',1,1)
,(368,120,'Blade Fouling','Blade Fouling',1,1)
,(369,121,'Too Much Fan','Too Much Fan',1,1)
,(370,122,'Secondary Source Control Valve Leaking','Secondary Source Control Valve Leaking',1,1)
,(371,122,'Dump/Bypass Valve Leaking','Dump/Bypass Valve Leaking',1,1)
,(373,123,'Increased Gas Flow due to air in leakage upstream of the air heater','Increased Gas Flow due to air in leakage upstream of the air heater',1,1)
,(374,123,'Ash or Salt Plugging','Ash or Salt Plugging',1,1)
,(375,123,'Air Heater Basket Damage','Air Heater Basket Damage',1,1)
,(376,124,'Control Problem','Control Problem',1,1)
,(377,124,'Steam Supply Valve Leaking','Steam Supply Valve Leaking',1,1)
,(378,125,'Control Problem','Control Problem',1,1)
,(379,126,'Inaccurate X-Ratio Calculation Due to Change in Stratification or Instrument Error','Inaccurate X-Ratio Calculation Due to Change in Stratification or Instrument Error',1,1)
,(380,126,'Air Inleakage Between Up and Downstream O2 Probes','Air Inleakage Between Up and Downstream O2 Probes',1,1)
,(381,126,'Air Heater Seal Damage','Air Heater Seal Damage',1,1)
,(382,127,'Ash or Salt Plugging','Ash or Salt Plugging',1,1)
,(383,127,'Air Heater Basket Damage','Air Heater Basket Damage',1,1)
,(384,128,'Debris Plugging','Debris Plugging',1,1)
,(385,129,'Inadequate Oil Cooling','Inadequate Oil Cooling',1,1)
,(386,129,'Shaft Misalignment','Shaft Misalignment',1,1)
,(387,129,'Bearing Failure','Bearing Failure',1,1)
,(388,130,'Alternate Drain Valve Leaking (condensing zone drain)','Alternate Drain Valve Leaking (condensing zone drain)',1,1)
,(389,130,'Alternate Drain Valve Leaking (drain cooling zone drain)','Alternate Drain Valve Leaking (drain cooling zone drain)',1,1)
,(390,130,'Faulty Normal Drain Valve','Faulty Normal Drain Valve',1,1)
,(391,130,'Low Water Level','Low Water Level',1,1)
,(392,130,'Shell Side Drain Cooler Leak','Shell Side Drain Cooler Leak',1,1)
,(393,131,'Alternate Drain Valve Leakthru','Alternate Drain Valve Leakthru',1,1)
,(394,131,'Alt Drn Valve Positioner Problem','Alt Drn Valve Positioner Problem',1,1)
,(395,131,'Tube Leak','Tube Leak',1,1)
,(396,131,'Level Controller Problem','Level Controller Problem',1,1)
,(397,132,'Extraction valve partly closed','Extraction valve partly closed',1,1)
,(398,133,'Leaking or Partially Open Bypass Valve','Leaking or Partially Open Bypass Valve',1,1)
,(399,134,'Backpressure Measurement Error','Backpressure Measurement Error',1,1)
,(400,134,'Cycle Isolation Dumping Extra Heat to Condenser','Cycle Isolation Dumping Extra Heat to Condenser',1,1)
,(401,134,'Inadequate Air Removal Equipment Performance','Inadequate Air Removal Equipment Performance',1,1)
,(402,134,'Excess Air In-Leakage','Excess Air In-Leakage',1,1)
,(403,134,'Air Binding in the Condenser Waterbox','Air Binding in the Condenser Waterbox',1,1)
,(404,134,'Condenser Tube Sheet Blockage','Condenser Tube Sheet Blockage',1,1)
,(405,134,'Condenser Tube Fouling','Condenser Tube Fouling',1,1)
,(406,134,'Cooling Water Flow Rate Measurement Error','Cooling Water Flow Rate Measurement Error',1,1)
,(407,135,'Air Binding in the Condenser Waterbox','Air Binding in the Condenser Waterbox',1,1)
,(409,135,'False Indication of Low Flow','False Indication of Low Flow',1,1)
,(410,135,'Reversing or Throttling Valve Restriction','Reversing or Throttling Valve Restriction',1,1)
,(411,135,'Circulating Water Pump Degradation','Circulating Water Pump Degradation',1,1)
,(412,135,'Condenser Tube Fouling','Condenser Tube Fouling',1,1)
,(413,135,'Condenser Tube Sheet Blockage','Condenser Tube Sheet Blockage',1,1)
,(414,135,'Cooling Water Flow Rate Measurement Error','Cooling Water Flow Rate Measurement Error',1,1)
,(415,136,'Excess Air Leakage','Excess Air Leakage',1,1)
,(416,137,'Intercept or Stop Valve Problem','Intercept or Stop Valve Problem',1,1)
,(417,137,'Mesh Screen Plugging','Mesh Screen Plugging',1,1)
,(418,137,'Deposition','Deposition',1,1)
,(419,137,'Steam Path Blockage','Steam Path Blockage',1,1)
,(420,137,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(421,137,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(422,138,'Deposition','Deposition',1,1)
,(423,138,'Steam Path Blockage','Steam Path Blockage',1,1)
,(424,138,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(425,138,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(426,139,'Deposition','Deposition',1,1)
,(427,139,'Steam Path Blockage','Steam Path Blockage',1,1)
,(428,139,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(429,139,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(430,140,'Deposition','Deposition',1,1)
,(431,140,'Steam Path Blockage','Steam Path Blockage',1,1)
,(432,140,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(433,140,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(434,141,'Deposition','Deposition',1,1)
,(435,141,'Steam Path Blockage','Steam Path Blockage',1,1)
,(436,141,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(437,141,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(438,142,'Deposition','Deposition',1,1)
,(439,142,'Steam Path Blockage','Steam Path Blockage',1,1)
,(440,142,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(441,142,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(442,143,'Increasingd N2 or Dummy Packing Leak','Increasingd N2 or Dummy Packing Leak',1,1)
,(443,144,'Deposition','Deposition',1,1)
,(444,144,'Steam Path Blockage','Steam Path Blockage',1,1)
,(445,144,'Incorrect Calculated Flow (False Alarm, Instr, or other Trigger gives root cause)','Incorrect Calculated Flow (False Alarm, Instr, or other Trigger gives root cause)',1,1)
,(446,144,'Failed Extraction Line','Failed Extraction Line',1,1)
,(447,144,'Blade Erosion or Seal Strip Wear (May be Normal Degradation)','Blade Erosion or Seal Strip Wear (May be Normal Degradation)',1,1)
,(448,144,'Blade/Diaphragm Failure or Seal Strip Damage or Rub','Blade/Diaphragm Failure or Seal Strip Damage or Rub',1,1)
,(449,144,'Incorrect Backpressure','Incorrect Backpressure',1,1)
,(450,145,'Seal Strip Damage or Rub','Seal Strip Damage or Rub',1,1)
,(451,146,'Clogged/Damaged Intercept or Hot Reheat Stop Valve Strainers','Clogged/Damaged Intercept or Hot Reheat Stop Valve Strainers',1,1)
,(452,146,'Intercept or Stop Valve Problem','Intercept or Stop Valve Problem',1,1)
,(453,146,'Internal Scaling','Internal Scaling',1,1)
,(454,147,'Source Control Valve Leaking','Source Control Valve Leaking',1,1)
,(455,147,'Dump/Bypass Valve Leaking','Dump/Bypass Valve Leaking',1,1)
,(456,148,'Turbine Rotor Damage','Turbine Rotor Damage',1,1)
,(457,148,'Shaft Alignment or Balance','Shaft Alignment or Balance',1,1)
,(458,148,'Bowed Rotor','Bowed Rotor',1,1)
,(459,148,'Bearing Failure','Bearing Failure',1,1)
,(460,149,'Inadequate Oil Cooling','Inadequate Oil Cooling',1,1)
,(461,149,'Bearing Failure','Bearing Failure',1,1)
,(462,150,'HP FWH Emergency Drain Valve Open (Possible Tube Leak)','HP FWH Emergency Drain Valve Open (Possible Tube Leak)',1,1)
,(463,150,'HP FWH Emergency Drain Valve Leaking','HP FWH Emergency Drain Valve Leaking',1,1)
,(464,150,'Boiler Water Loss (Tube Leak or Increased Blowdown or Sootblow)','Boiler Water Loss (Tube Leak or Increased Blowdown or Sootblow)',1,1)
,(465,150,'Flow Measurement Error','Flow Measurement Error',1,1)
,(466,151,'Boiler Tube Leak','Boiler Tube Leak',1,1)
,(467,151,'Leak to Atmosphere or Drain','Leak to Atmosphere or Drain',1,1)
,(468,152,'FD Fan Increase','FD Fan Increase',1,1)
,(469,152,'ID Fan Increase','ID Fan Increase',1,1)
,(470,152,'CW Pump Increase','CW Pump Increase',1,1)
,(471,152,'Cond Pump Increase','Cond Pump Increase',1,1)
,(472,152,'Unaccounted Increase','Unaccounted Increase',1,1)
,(473,153,'HP FWH Emergency Drain Valve Open (Tube Leak?)','HP FWH Emergency Drain Valve Open (Tube Leak?)',1,1)
,(474,153,'HP FWH Emergency Drain Valve Leaking','HP FWH Emergency Drain Valve Leaking',1,1)
,(475,153,'Boiler Feed Pump Recirc Valve Open or Leaking','Boiler Feed Pump Recirc Valve Open or Leaking',1,1)
,(476,153,'Flow Measurement Error','Flow Measurement Error',1,1)
,(477,154,'Blowdown Valve Left Open','Blowdown Valve Left Open',1,1)
,(478,154,'Startup Blowdown Valve Leaking','Startup Blowdown Valve Leaking',1,1)
,(479,155,'Cycle Isolation problems','Cycle Isolation problems',1,1)
,(480,155,'Turbine Section Inefficiencies','Turbine Section Inefficiencies',1,1)
,(481,155,'Feedwater Heater Problems','Feedwater Heater Problems',1,1)
,(482,155,'False Alarm, Instrumentation','False Alarm, Instrumentation',1,1)
,(483,155,'Top Feedwater Heater Bypass','Top Feedwater Heater Bypass',1,1)
,(484,155,'Feedwater Heater Bypass','Feedwater Heater Bypass',1,1)
,(485,155,'Feedwater Heater Drain to Cond','Feedwater Heater Drain to Cond',1,1)
,(486,156,'Excessive Spray Flow (Spray valve issue)','Excessive Spray Flow (Spray valve issue)',1,1)
,(487,156,'Low RH or High WW/SH Section Cleanliness','Low RH or High WW/SH Section Cleanliness',1,1)
,(488,156,'Excessive Spray Flow (Control issue)','Excessive Spray Flow (Control issue)',1,1)
,(489,156,'Instrumentation','Instrumentation',1,1)
,(490,157,'Low Boiler Section Cleanliness','Low Boiler Section Cleanliness',1,1)
,(491,157,'Instrumentation Error','Instrumentation Error',1,1)
,(492,158,'Economizer Fouling','Economizer Fouling',1,1)
,(493,158,'Boiler Air Infiltration','Boiler Air Infiltration',1,1)
,(494,159,'SH or RH Fouling','SH or RH Fouling',1,1)
,(495,159,'Boiler Air Infiltration','Boiler Air Infiltration',1,1)
,(496,160,'Draft Loss Problem (pluggage of SSH, FSH, RH, PSH)','Draft Loss Problem (pluggage of SSH, FSH, RH, PSH)',1,1)
,(497,160,'Draft Loss Problem (pluggage of Econ)','Draft Loss Problem (pluggage of Econ)',1,1)
,(498,160,'Boiler Air Infiltration','Boiler Air Infiltration',1,1)
,(499,161,'Burner or Mill Switch or Problem','Burner or Mill Switch or Problem',1,1)
,(500,161,'Boiler Air Infiltration','Boiler Air Infiltration',1,1)
,(501,161,'Instrument Error','Instrument Error',1,1)
,(502,162,'Burner or Mill Switch or Problem','Burner or Mill Switch or Problem',1,1)
,(503,162,'Boiler Air Infiltration','Boiler Air Infiltration',1,1)
,(504,162,'Instrument Error','Instrument Error',1,1)
,(505,163,'Controls Tuning','Controls Tuning',1,1)
,(506,163,'Operator Bias (if no CombOpt)','Operator Bias (if no CombOpt)',1,1)
,(507,163,'Change in Selected O2','Change in Selected O2',1,1)
,(508,164,'Fouling','Fouling',1,1)
,(509,164,'Inadequate Sootblowing','Inadequate Sootblowing',1,1)
,(510,165,'Poor Pulverizer Performance','Poor Pulverizer Performance',1,1)
,(511,165,'Air Inleakage Before O2 Probes','Air Inleakage Before O2 Probes',1,1)
,(512,165,'Other Combustion Problems (stuck damper, slag on burners, etc.)','Other Combustion Problems (stuck damper, slag on burners, etc.)',1,1)
,(513,166,'Fouled Compressor','Fouled Compressor',1,1)
,(514,166,'Turbine Seal Damage','Turbine Seal Damage',1,1)
,(515,166,'Instrumentation Error','Instrumentation Error',1,1)
,(516,166,'Low fuel temperature or pressure','Low fuel temperature or pressure',1,1)
,(517,166,'Compressor or Turbine damage','Compressor or Turbine damage',1,1)
,(518,167,'Clogged Inlet Filters','Clogged Inlet Filters',1,1)
,(519,167,'Inlet Heating Valve Position Error','Inlet Heating Valve Position Error',1,1)
,(520,167,'Fogging System Issue','Fogging System Issue',1,1)
,(521,168,'Clogged Inlet Filters','Clogged Inlet Filters',1,1)
,(522,168,'Instrumentation Error','Instrumentation Error',1,1)
,(523,169,'Blocked Fuel Passages','Blocked Fuel Passages',1,1)
,(524,169,'Loose Fuel Nozzle Tip','Loose Fuel Nozzle Tip',1,1)
,(525,169,'Combustion Air Leak','Combustion Air Leak',1,1)
,(526,169,'Instrument Error','Instrument Error',1,1)
,(527,169,'Blocked Liner Hole','Blocked Liner Hole',1,1)
,(528,170,'Insufficient Cooling','Insufficient Cooling',1,1)
,(529,170,'Instrument Error','Instrument Error',1,1)
,(530,170,'Combustion Malfunction','Combustion Malfunction',1,1)
,(531,171,'Insufficient Cooling','Insufficient Cooling',1,1)
,(532,171,'Instrument Error','Instrument Error',1,1)
,(533,171,'Combustion Malfunction','Combustion Malfunction',1,1)
,(534,172,'Insufficient Cooling','Insufficient Cooling',1,1)
,(535,172,'Instrument Error','Instrument Error',1,1)
,(536,172,'Combustion Malfunction','Combustion Malfunction',1,1)
,(537,173,'Insufficient Cooling','Insufficient Cooling',1,1)
,(538,173,'Instrument Error','Instrument Error',1,1)
,(539,173,'Combustion Malfunction','Combustion Malfunction',1,1)
,(540,174,'Insufficient Cooling','Insufficient Cooling',1,1)
,(541,174,'Instrument Error','Instrument Error',1,1)
,(542,174,'Combustion Malfunction','Combustion Malfunction',1,1)
,(543,175,'Insufficient Cooling','Insufficient Cooling',1,1)
,(544,175,'Instrument Error','Instrument Error',1,1)
,(545,175,'Combustion Malfunction','Combustion Malfunction',1,1)
,(546,176,'Ash Carryover or Plugging','Ash Carryover or Plugging',1,1)
,(547,177,'Change in feed water quality','Change in feed water quality',1,1)
,(548,177,'Higher flux rate (flow rate)','Higher flux rate (flow rate)',1,1)
,(549,177,'Maintenance wash (cleaning) was not effective','Maintenance wash (cleaning) was not effective',1,1)
,(550,178,'Wrong type of cleaning was performed','Wrong type of cleaning was performed',1,1)
,(551,178,'Cleaning did not complete normally','Cleaning did not complete normally',1,1)
,(552,178,'Needed higher concentration of chemicals or longer cleaning period','Needed higher concentration of chemicals or longer cleaning period',1,1)
,(553,178,'Membrane is near CIP cycle and needs a CIP','Membrane is near CIP cycle and needs a CIP',1,1)
,(554,178,'Membrane has irrecoverable fouling','Membrane has irrecoverable fouling',1,1)
,(555,179,'Wrong type of cleaning was performed','Wrong type of cleaning was performed',1,1)
,(556,179,'Cleaning did not complete normally','Cleaning did not complete normally',1,1)
,(557,179,'Needed higher concentration of chemicals or longer cleaning period','Needed higher concentration of chemicals or longer cleaning period',1,1)
,(558,179,'Membrane has irrecoverable fouling','Membrane has irrecoverable fouling',1,1)
,(559,180,'Backwashing too frequent','Backwashing too frequent',1,1)
,(560,180,'Backwashing for too long a duration','Backwashing for too long a duration',1,1)
,(561,180,'Improper backwashing procedure','Improper backwashing procedure',1,1)
,(562,180,'Production too low','Production too low',1,1)

-- PBI 50814
,(563,284,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(564,284,'Housekeeping','Housekeeping',1,1)
,(565,284,'Operating off Design Window','Operating off Design Window',1,1)
,(566,284,'Lubrication','Lubrication',1,1)
,(567,284,'Base Foundation','Base Foundation',1,1)
,(568,284,'Electrical','Electrical',1,1)
,(569,284,'End of Life','End of Life',1,1)

,(570,285,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(571,285,'Housekeeping','Housekeeping',1,1)
,(572,285,'Operating off Design Window','Operating off Design Window',1,1)
,(573,285,'Lubrication','Lubrication',1,1)
,(574,285,'Base Foundation','Base Foundation',1,1)
,(575,285,'Electrical','Electrical',1,1)
,(576,285,'End of Life','End of Life',1,1)

,(577,286,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(578,286,'Housekeeping','Housekeeping',1,1)
,(579,286,'Operating off Design Window','Operating off Design Window',1,1)
,(580,286,'Lubrication','Lubrication',1,1)
,(581,286,'Base Foundation','Base Foundation',1,1)
,(582,286,'Electrical','Electrical',1,1)
,(583,286,'End of Life','End of Life',1,1)

,(584,287,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(585,287,'Housekeeping','Housekeeping',1,1)
,(586,287,'Operating off Design Window','Operating off Design Window',1,1)
,(587,287,'Lubrication','Lubrication',1,1)
,(588,287,'Base Foundation','Base Foundation',1,1)
,(589,287,'Electrical','Electrical',1,1)
,(590,287,'End of Life','End of Life',1,1)

,(591,288,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(592,288,'Housekeeping','Housekeeping',1,1)
,(593,288,'Operating off Design Window','Operating off Design Window',1,1)
,(594,288,'Lubrication','Lubrication',1,1)
,(595,288,'Base Foundation','Base Foundation',1,1)
,(596,288,'Electrical','Electrical',1,1)
,(597,288,'End of Life','End of Life',1,1)

,(598,289,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(599,289,'Housekeeping','Housekeeping',1,1)
,(600,289,'Operating off Design Window','Operating off Design Window',1,1)
,(601,289,'Lubrication','Lubrication',1,1)
,(602,289,'Base Foundation','Base Foundation',1,1)
,(603,289,'Electrical','Electrical',1,1)
,(604,289,'End of Life','End of Life',1,1)

,(605,290,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(606,290,'Housekeeping','Housekeeping',1,1)
,(607,290,'Operating off Design Window','Operating off Design Window',1,1)
,(608,290,'Lubrication','Lubrication',1,1)
,(609,290,'Base Foundation','Base Foundation',1,1)
,(610,290,'Electrical','Electrical',1,1)
,(611,290,'End of Life','End of Life',1,1)

,(612,291,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(613,291,'Housekeeping','Housekeeping',1,1)
,(614,291,'Operating off Design Window','Operating off Design Window',1,1)
,(615,291,'Lubrication','Lubrication',1,1)
,(616,291,'Base Foundation','Base Foundation',1,1)
,(617,291,'Electrical','Electrical',1,1)
,(618,291,'End of Life','End of Life',1,1)

,(619,292,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(620,292,'Housekeeping','Housekeeping',1,1)
,(621,292,'Operating off Design Window','Operating off Design Window',1,1)
,(622,292,'Lubrication','Lubrication',1,1)
,(623,292,'Base Foundation','Base Foundation',1,1)
,(624,292,'Electrical','Electrical',1,1)
,(625,292,'End of Life','End of Life',1,1)

,(626,293,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(627,293,'Housekeeping','Housekeeping',1,1)
,(628,293,'Operating off Design Window','Operating off Design Window',1,1)
,(629,293,'Lubrication','Lubrication',1,1)
,(630,293,'Base Foundation','Base Foundation',1,1)
,(631,293,'Electrical','Electrical',1,1)
,(632,293,'End of Life','End of Life',1,1)

,(633,294,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(634,294,'Housekeeping','Housekeeping',1,1)
,(635,294,'Operating off Design Window','Operating off Design Window',1,1)
,(636,294,'Lubrication','Lubrication',1,1)
,(637,294,'Base Foundation','Base Foundation',1,1)
,(638,294,'Electrical','Electrical',1,1)
,(639,294,'End of Life','End of Life',1,1)

,(640,295,'Craftsmanship & Work Execution','Craftsmanship & Work Execution',1,1)
,(641,295,'Housekeeping','Housekeeping',1,1)
,(642,295,'Operating off Design Window','Operating off Design Window',1,1)
,(643,295,'Lubrication','Lubrication',1,1)
,(644,295,'Base Foundation','Base Foundation',1,1)
,(645,295,'Electrical','Electrical',1,1)
,(646,295,'End of Life','End of Life',1,1)
)
AS SOURCE([IssueCauseTypeID],[IssueTypeID],[IssueCauseDesc],[IssueCauseAbbrev],[IsBVApproved],[CreatedByUserID])
ON Target.[IssueCauseTypeID]= Source.[IssueCauseTypeID]
WHEN MATCHED THEN UPDATE SET [IssueTypeID]=Source.[IssueTypeID],[IssueCauseDesc]=Source.[IssueCauseDesc],[IssueCauseAbbrev]=Source.[IssueCauseAbbrev],[IsBVApproved]=Source.[IsBVApproved],[CreatedByUserID]=Source.[CreatedByUserID]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([IssueCauseTypeID],[IssueTypeID],[IssueCauseDesc],[IssueCauseAbbrev],[IsBVApproved],[CreatedByUserID])
VALUES ([IssueCauseTypeID],[IssueTypeID],[IssueCauseDesc],[IssueCauseAbbrev],[IsBVApproved],[CreatedByUserID])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

ENABLE TRIGGER [tIssueCauseType].ValidateIssueCauseTypeInsertUpdate ON [Diagnostics].[tIssueCauseType]
GO

MERGE INTO [Diagnostics].[tCategoryNotificationType] AS TARGET USING (VALUES
(1, 'Send email to distribution list', 'Send Notification'),
(2, 'Update asset map with new issue status', 'Update Map'),
(3, 'Modify asset attribute', 'Modify Attribute'),
(4, 'Add and remove asset tags', 'Modify Tags'),
(5, 'Create blog entry', 'Create Blog Entry'),
(6, 'Assign action to a user', 'Assign User Action'),
(7, 'Create an issue', 'Create Issue'),
(8, 'Update activity status for issue', 'Update Activity Status'),
(9, 'Update issue status on resolution status change', 'Update Issue Status'),
(10, 'Updates resolution status of an issue', 'Change Resolution Status')
)
AS SOURCE([CategoryNotificationTypeID], [CategoryNotificationTypeDesc], [CategoryNotificationTypeAbbrev])
ON TARGET.[CategoryNotificationTypeID]=SOURCE.[CategoryNotificationTypeID]
WHEN MATCHED THEN UPDATE SET
[CategoryNotificationTypeDesc]=SOURCE.[CategoryNotificationTypeDesc]
,[CategoryNotificationTypeAbbrev]=SOURCE.[CategoryNotificationTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([CategoryNotificationTypeID], [CategoryNotificationTypeDesc], [CategoryNotificationTypeAbbrev])
VALUES ([CategoryNotificationTypeID], [CategoryNotificationTypeDesc], [CategoryNotificationTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
GO

INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Operations','Plant Operations',1,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('I&C','I&C',1,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Outage Maintenance','Outage Maintenance',1,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Online Maintenance','Online Maintenance',1,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Unassigned','Unassigned',1,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Unassigned','Unassigned',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Operations','Plant Operations',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('I&C','I&C',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Fuel Planning','Fuel Planning',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Fuel Purchasing','Fuel Purchasing',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Transportation Scheduling','Transportation Scheduling',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Yard Operations','Yard Operations',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Mine Operations','Mine Operations',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Plant AQC Operations','Plant AQC Operations',2,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Operations','Plant Operations',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('I&C','I&C',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Fuel Planning','Fuel Planning',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Yard Operations','Yard Operations',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Plant AQC Operations','Plant AQC Operations',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('AQC Planning','AQC Planning',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Unassigned','Unassigned',3,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Unassigned','Unassigned',4,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Arc Flash','Arc Flash',5,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Unassigned','Unassigned',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('PMO','PMO',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('RFI','Request For Information',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Deliverable','Deliverable',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('OT','OT',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('IT','IT',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Telecom','Telecom',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Safety','Safety',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Engineering','Engineering',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Procurement','Procurement',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Permitting','Permitting',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Construction','Construction',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Closeout','Closeout',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Scope','Scope',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Planning','Planning',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Design Review','Design Review',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Strategic Sourcing','Strategic Sourcing',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Equipment Procurement','Equipment Procurement',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Testing','Testing',6,1,1)
INSERT INTO Diagnostics.tAssetIssueCategoryType (CategoryAbbrev, CategoryDesc, IssueClassTypeID, CreatedBy, ChangedBy) VALUES ('Other',' Other',6,1,1)

MERGE INTO Diagnostics.tAssetIssueImpactCategoryType AS TARGET USING (VALUES
(1,'Heat Rate','An estimation of financial savings of taking action at the issue identification as compared to waiting until a future date.  The future date is determined by either a scheduled outage or an assumption of when the issue would have been evident to the plant.')
,(2,'Reliability','An estimation of the financial impact due to lost generation and costs to repair in the case that a failure occurs times the probability of the failure.')
,(3,'Derate','An estimation of financial savings of taking action at the issue identification as compared to waiting until a future date for generation impacts caused from performance limitations of the plant.')
,(4,'Emissions',null)
,(5,'Other Costs',null)
,(6,'Generation',null)
,(7,'Maintenance',null)
,(8,'Project Management',null)
)
AS SOURCE([AssetIssueImpactCategoryTypeID],[AssetIssueImpactCategoryTypeDesc],[AssetIssueImpactCategoryTypeCalcDesc])
ON Target.[AssetIssueImpactCategoryTypeID]= Source.[AssetIssueImpactCategoryTypeID]
WHEN MATCHED THEN UPDATE SET [AssetIssueImpactCategoryTypeDesc]=Source.[AssetIssueImpactCategoryTypeDesc]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssetIssueImpactCategoryTypeID],[AssetIssueImpactCategoryTypeDesc],[AssetIssueImpactCategoryTypeCalcDesc])
VALUES ([AssetIssueImpactCategoryTypeID],[AssetIssueImpactCategoryTypeDesc],[AssetIssueImpactCategoryTypeCalcDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;


MERGE INTO Diagnostics.tAssetIssueImpactType AS TARGET USING (VALUES
(1,1,'','Btu/kWh',1)
,(2,2,'','MW',2)
,(3,3,'','MW',3)
,(4,4,'SO2','lb/MBtu',4)
,(5,4,'NOx','lb/MBtu',5)
,(6,4,'Opacity','%',6)
,(7,4,'Hg','lb/TBtu',7)
,(8,4,'PM','lb/TBtu',8)
,(9,5,'','$',11)
,(10,7,'','$',10)
,(11,8,'Scope Impact','$',12)
,(12,8,'Schedule Impact','$',13)
,(13,8,'Budget Impact','$',14)
,(14,8,'Safety Impact','$',15)
)
AS SOURCE([AssetIssueImpactTypeID],[AssetIssueImpactCategoryTypeID],[AssetIssueImpactTypeDesc],[Units],[DisplayOrder])
ON Target.[AssetIssueImpactTypeID]= Source.[AssetIssueImpactTypeID]
WHEN MATCHED THEN UPDATE SET [AssetIssueImpactCategoryTypeID]=Source.[AssetIssueImpactCategoryTypeID],[AssetIssueImpactTypeDesc]=Source.[AssetIssueImpactTypeDesc],[Units]=Source.[Units],[DisplayOrder]=Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssetIssueImpactTypeID],[AssetIssueImpactCategoryTypeID],[AssetIssueImpactTypeDesc],[Units],[DisplayOrder])
VALUES ([AssetIssueImpactTypeID],[AssetIssueImpactCategoryTypeID],[AssetIssueImpactTypeDesc],[Units],[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

SET IDENTITY_INSERT [Diagnostics].[tIssueCauseTypeVariableTypeMap] ON
GO

PRINT 'Populating Diagnostics.tIssueCauseTypeVariableTypeMap';
MERGE INTO Diagnostics.tIssueCauseTypeVariableTypeMap AS TARGET USING (VALUES
(112,14,2438,1,'Higher than normal',NULL,1)
,(113,14,2456,1,'Low',NULL,1)
,(114,14,2439,1,'Different than Tsat',NULL,1)
,(116,14,2440,1,'Different than Tsat',NULL,1)
,(119,15,2438,1,'Higher than normal',NULL,1)
,(120,15,2456,1,'Low',NULL,1)
,(123,16,2438,1,'Higher than normal',NULL,1)
,(124,16,2456,1,'Low',NULL,1)
,(127,17,2438,1,'Higher than normal',NULL,1)
,(128,17,2456,1,'Low',NULL,1)
,(129,17,2445,1,'Higher than normal',NULL,1)
,(131,17,2446,1,'Normal or High',NULL,1)
,(132,17,2447,1,'Higher than normal',NULL,1)
,(135,18,2438,1,'Higher than normal',NULL,1)
,(136,18,2456,1,'Low',NULL,1)
,(143,19,2438,1,'Higher than normal',NULL,1)
,(144,19,2456,1,'Low',NULL,1)
,(150,20,2438,1,'Higher than normal',NULL,1)
,(151,20,2456,1,'Low',NULL,1)
,(154,21,2456,1,'Low',NULL,1)
,(211,32,2435,1,'Increasing',NULL,1)
,(212,32,2438,1,'Increasing',NULL,1)
,(213,32,2441,1,'Increasing',NULL,1)
,(216,33,2435,1,'Increasing',NULL,1)
,(217,33,2439,1,'Decreasing',NULL,1)
,(222,34,2435,1,'Increasing',NULL,1)
,(223,34,2439,1,'Decreasing',NULL,1)
,(236,35,2456,1,'Low',NULL,1)
,(237,35,2438,1,'Higher than Normal',NULL,1)
,(244,36,2456,1,'Low',NULL,1)
,(245,36,2438,1,'Higher than Normal',NULL,1)
,(248,37,2456,1,'Low',NULL,1)
,(249,37,2438,1,'Higher than Normal',NULL,1)
,(253,38,2438,1,'Higher than normal',NULL,1)
,(689,70,5015,1,'Low',NULL,1)
,(690,70,5017,1,'Higher than Actual',NULL,1)
,(691,70,5020,1,'High',NULL,1)
,(692,70,2374,1,'High',NULL,1)
,(693,70,2552,1,'Normal or Slightly High',6131,1)
,(694,70,2376,1,'Low',NULL,1)
,(695,70,2361,1,'High',NULL,1)
,(704,71,5015,1,'Low',NULL,1)
,(705,71,5020,1,'High',NULL,1)
,(706,71,2374,1,'High',NULL,1)
,(707,71,2552,1,'High',6131,1)
,(716,71,2376,1,'Low',NULL,1)
,(717,71,2361,1,'Low',NULL,1)
,(726,72,5015,1,'Low',NULL,1)
,(727,72,5017,1,'Low',NULL,1)
,(728,72,5020,1,'High',NULL,1)
,(729,72,2374,1,'High',NULL,1)
,(730,72,2552,1,'Normal or Slightly High',6131,1)
,(731,72,2376,1,'Low',NULL,1)
,(732,72,2361,1,'High',NULL,1)
,(741,73,5015,1,'Low',NULL,1)
,(742,73,5016,1,'High',NULL,1)
,(746,74,2378,1,'High',NULL,1)
,(747,74,2552,1,'Normal or Low',6131,1)
,(749,74,2361,1,'High',NULL,1)
,(760,75,2378,1,'Low',NULL,1)
,(761,75,2552,1,'High',6131,1)
,(771,75,2361,1,'Low',NULL,1)
,(783,76,2378,1,'High',NULL,1)
,(784,76,2552,1,'Normal or Low',6131,1)
,(786,76,2361,1,'High',NULL,1)
,(800,78,5003,1,'High',NULL,1)
,(801,78,2552,1,'Normal or Low',6131,1)
,(803,78,2361,1,'High',NULL,1)
,(814,79,5003,1,'Low',NULL,1)
,(815,79,2552,1,'High',6131,1)
,(825,79,2361,1,'Low',NULL,1)
,(837,80,5003,1,'High',NULL,1)
,(838,80,2552,1,'Normal or Low',6131,1)
,(840,80,2361,1,'High',NULL,1)
,(1057,100,2683,1,'Increasing',NULL,1)
,(1058,100,2685,1,'Decreasing',NULL,1)
,(1059,100,2688,1,'Increasing',NULL,1)
,(1060,100,2689,1,'Normal or open less than normal',NULL,1)
,(1061,100,2690,1,'Closed',NULL,1)
,(1062,100,2699,1,'Lower than normal',NULL,1)
,(1065,101,2683,1,'High',NULL,1)
,(1066,101,2685,1,'Decreasing',NULL,1)
,(1067,101,2689,1,'Open more than normal',NULL,1)
,(1068,101,2690,1,'Occasionally or Consistently Opening',NULL,1)
,(1069,101,2700,1,'High',NULL,1)
,(1070,101,2701,1,'Normal or High',NULL,1)
,(1071,101,2456,1,'Low',NULL,1)
,(1072,101,2699,1,'Complicated',NULL,1)
,(1075,102,2683,1,'Increasing',NULL,1)
,(1076,102,2685,1,'Decreasing',NULL,1)
,(1077,102,2689,1,'Normal or open less than normal',NULL,1)
,(1078,102,2690,1,'Closed',NULL,1)
,(1079,102,2700,1,'High',NULL,1)
,(1080,102,2699,1,'Lower than normal',NULL,1)
,(1083,103,2683,1,'High',NULL,1)
,(1084,103,2685,1,'Low',NULL,1)
,(1085,103,2685,7,'Normal or Low',NULL,1)
,(1086,103,2689,1,'Normal or open less than normal',NULL,1)
,(1087,103,2691,1,'Low',NULL,1)
,(1088,103,2690,1,'Closed',NULL,1)
,(1089,103,2699,1,'Lower than normal',NULL,1)
,(1090,103,5037,1,'Normal or Increasing',NULL,1)
,(1094,104,2683,1,'Increasing',NULL,1)
,(1095,104,2685,1,'Decreasing',NULL,1)
,(1096,104,2684,1,'Increasing',NULL,1)
,(1097,104,2690,1,'Occasionally or Consistently Opening',NULL,1)
,(1098,104,2701,1,'High',NULL,1)
,(1101,105,2683,1,'Increasing',NULL,1)
,(1102,105,2685,1,'Decreasing',NULL,1)
,(1103,105,2690,1,'Closed',NULL,1)
,(1106,106,2683,1,'Increasing',NULL,1)
,(1107,106,2685,1,'Decreasing',NULL,1)
,(1108,106,2689,1,'Normal or open less than normal',NULL,1)
,(1109,106,2690,1,'Closed',NULL,1)
,(1110,106,2699,1,'Lower than normal',NULL,1)
,(1113,107,2683,1,'Increasing',NULL,1)
,(1114,107,2685,1,'Decreasing',NULL,1)
,(1115,107,2689,1,'Normal or open less than normal',NULL,1)
,(1116,107,2690,1,'Closed',NULL,1)
,(1117,107,2699,1,'Lower than normal',NULL,1)
,(1120,108,2683,1,'Increasing',NULL,1)
,(1121,108,2685,1,'Decreasing',NULL,1)
,(1122,108,2689,1,'Normal or open less than normal',NULL,1)
,(1123,108,2690,1,'Closed',NULL,1)
,(1124,108,2702,1,'High',NULL,1)
,(1125,108,2699,1,'Lower than normal',NULL,1)
,(1128,109,2683,1,'Increasing',NULL,1)
,(1129,109,2685,1,'Decreasing',NULL,1)
,(1130,109,2692,1,'Normal or High',NULL,1)
,(1131,109,2689,1,'Normal or open less than normal',NULL,1)
,(1132,109,2690,1,'Closed',NULL,1)
,(1133,109,2699,1,'Lower than normal',NULL,1)
,(1293,149,20,4,NULL,NULL,1)
,(1294,149,28,4,NULL,NULL,1)
,(1295,149,29,11,NULL,NULL,1)
,(1296,149,30,1,NULL,NULL,1)
,(1297,149,17,1,NULL,NULL,1)
,(1298,149,33,1,NULL,NULL,1)
,(1299,149,34,1,NULL,NULL,1)
,(1300,149,31,1,NULL,NULL,1)
,(1301,149,10,1,NULL,NULL,1)
,(1302,149,7,1,NULL,NULL,1)
,(1303,149,32,1,NULL,NULL,1)
,(1304,150,21,1,NULL,NULL,1)
,(1305,150,20,4,NULL,NULL,1)
,(1306,150,17,1,NULL,NULL,1)
,(1307,150,18,11,NULL,NULL,1)
,(1308,150,16,1,NULL,NULL,1)
,(1309,150,19,1,NULL,NULL,1)
,(1310,150,22,1,NULL,NULL,1)
,(1311,150,10,1,NULL,NULL,1)
,(1312,150,7,1,NULL,NULL,1)
,(1313,150,23,1,NULL,NULL,1)
,(1314,150,24,1,NULL,NULL,1)
,(1315,151,21,1,NULL,NULL,1)
,(1316,151,20,4,NULL,NULL,1)
,(1317,151,17,1,NULL,NULL,1)
,(1318,151,19,1,NULL,NULL,1)
,(1319,151,25,1,NULL,NULL,1)
,(1320,151,26,11,NULL,NULL,1)
,(1321,151,22,1,NULL,NULL,1)
,(1322,151,10,1,NULL,NULL,1)
,(1323,151,7,1,NULL,NULL,1)
,(1324,151,23,1,NULL,NULL,1)
,(1325,151,27,1,NULL,NULL,1)
,(1326,151,24,1,NULL,NULL,1)
,(1327,152,143,1,NULL,NULL,1)
,(1328,152,142,1,NULL,NULL,1)
,(1329,152,144,11,NULL,NULL,1)
,(1330,152,145,1,NULL,NULL,1)
,(1331,152,148,1,NULL,NULL,1)
,(1332,152,147,1,NULL,NULL,1)
,(1333,152,146,1,NULL,NULL,1)
,(1334,152,149,1,NULL,NULL,1)
,(1335,153,130,1,NULL,NULL,1)
,(1336,153,31,1,NULL,NULL,1)
,(1339,153,125,1,NULL,NULL,1)
,(1340,153,124,1,NULL,NULL,1)
,(1341,153,126,11,NULL,NULL,1)
,(1342,153,57,1,NULL,NULL,1)
,(1343,153,56,1,NULL,NULL,1)
,(1346,153,52,1,NULL,NULL,1)
,(1347,153,54,1,NULL,NULL,1)
,(1348,153,55,1,NULL,NULL,1)
,(1350,153,60,1,NULL,NULL,1)
,(1351,153,136,1,NULL,NULL,1)
,(1352,153,132,1,NULL,NULL,1)
,(1353,153,133,1,NULL,NULL,1)
,(1355,154,130,1,NULL,NULL,1)
,(1356,154,31,1,NULL,NULL,1)
,(1359,154,125,1,NULL,NULL,1)
,(1360,154,138,1,NULL,NULL,1)
,(1361,154,126,11,NULL,NULL,1)
,(1362,154,57,1,NULL,NULL,1)
,(1363,154,56,1,NULL,NULL,1)
,(1366,154,52,1,NULL,NULL,1)
,(1367,154,54,1,NULL,NULL,1)
,(1368,154,55,1,NULL,NULL,1)
,(1370,154,60,1,NULL,NULL,1)
,(1371,154,136,1,NULL,NULL,1)
,(1372,154,132,1,NULL,NULL,1)
,(1373,154,133,1,NULL,NULL,1)
,(1375,155,130,1,NULL,NULL,1)
,(1376,155,31,1,NULL,NULL,1)
,(1379,155,125,1,NULL,NULL,1)
,(1380,155,137,1,NULL,NULL,1)
,(1381,155,126,11,NULL,NULL,1)
,(1382,155,57,1,NULL,NULL,1)
,(1383,155,56,1,NULL,NULL,1)
,(1386,155,52,1,NULL,NULL,1)
,(1387,155,54,1,NULL,NULL,1)
,(1388,155,55,1,NULL,NULL,1)
,(1390,155,60,1,NULL,NULL,1)
,(1391,155,136,1,NULL,NULL,1)
,(1392,155,132,1,NULL,NULL,1)
,(1393,155,133,1,NULL,NULL,1)
,(1396,156,125,1,NULL,NULL,1)
,(1397,156,57,1,NULL,NULL,1)
,(1398,156,140,1,NULL,NULL,1)
,(1399,156,139,1,NULL,NULL,1)
,(1400,156,141,11,NULL,NULL,1)
,(1401,157,61,1,NULL,NULL,1)
,(1402,157,57,1,NULL,NULL,1)
,(1403,157,56,1,NULL,NULL,1)
,(1404,157,53,11,NULL,NULL,1)
,(1405,157,51,1,NULL,NULL,1)
,(1406,157,52,1,NULL,NULL,1)
,(1407,157,54,1,NULL,NULL,1)
,(1408,157,55,1,NULL,NULL,1)
,(1411,157,60,1,NULL,NULL,1)
,(1412,158,12,1,NULL,NULL,1)
,(1413,158,10,1,NULL,NULL,1)
,(1414,158,7,1,NULL,NULL,1)
,(1415,158,9,1,NULL,NULL,1)
,(1416,158,8,11,NULL,NULL,1)
,(1417,158,13,1,NULL,NULL,1)
,(1418,158,14,1,NULL,NULL,1)
,(1419,158,15,1,NULL,NULL,1)
,(1420,158,11,1,NULL,NULL,1)
,(1421,158,6,1,NULL,NULL,1)
,(1422,159,17,1,NULL,NULL,1)
,(1423,159,36,11,NULL,NULL,1)
,(1424,159,10,1,NULL,NULL,1)
,(1425,159,7,1,NULL,NULL,1)
,(1426,159,35,1,NULL,NULL,1)
,(1427,159,14,1,NULL,NULL,1)
,(1428,159,37,1,NULL,NULL,1)
,(1429,159,15,1,NULL,NULL,1)
,(1430,159,38,1,NULL,NULL,1)
,(1431,159,39,1,NULL,NULL,1)
,(1432,160,72,1,NULL,NULL,1)
,(1433,160,71,1,NULL,NULL,1)
,(1434,160,77,1,NULL,NULL,1)
,(1435,160,75,1,NULL,NULL,1)
,(1436,160,76,1,NULL,NULL,1)
,(1438,161,17,1,NULL,NULL,1)
,(1439,161,10,1,NULL,NULL,1)
,(1440,161,7,1,NULL,NULL,1)
,(1441,161,44,1,NULL,NULL,1)
,(1442,161,37,1,NULL,NULL,1)
,(1443,161,15,1,NULL,NULL,1)
,(1444,161,38,1,NULL,NULL,1)
,(1445,161,47,11,NULL,NULL,1)
,(1446,161,45,1,NULL,NULL,1)
,(1447,161,46,11,NULL,NULL,1)
,(1448,162,17,1,NULL,NULL,1)
,(1449,162,10,1,NULL,NULL,1)
,(1450,162,7,1,NULL,NULL,1)
,(1451,162,48,1,NULL,NULL,1)
,(1452,162,37,1,NULL,NULL,1)
,(1453,162,15,1,NULL,NULL,1)
,(1454,162,38,1,NULL,NULL,1)
,(1455,162,47,11,NULL,NULL,1)
,(1456,162,45,1,NULL,NULL,1)
,(1457,162,46,11,NULL,NULL,1)
,(1458,163,102,1,NULL,NULL,1)
,(1459,163,94,1,NULL,NULL,1)
,(1460,163,106,1,NULL,NULL,1)
,(1461,163,107,11,NULL,NULL,1)
,(1462,163,113,1,NULL,NULL,1)
,(1463,163,112,1,NULL,NULL,1)
,(1464,163,111,1,NULL,NULL,1)
,(1466,163,110,1,NULL,NULL,1)
,(1467,163,108,1,NULL,NULL,1)
,(1468,163,109,1,NULL,NULL,1)
,(1469,164,115,1,NULL,NULL,1)
,(1470,164,114,1,NULL,NULL,1)
,(1471,164,116,11,NULL,NULL,1)
,(1472,164,123,1,NULL,NULL,1)
,(1473,164,122,1,NULL,NULL,1)
,(1474,164,121,1,NULL,NULL,1)
,(1475,164,119,1,NULL,NULL,1)
,(1476,164,117,1,NULL,NULL,1)
,(1477,164,118,1,NULL,NULL,1)
,(1478,164,120,1,NULL,NULL,1)
,(1479,165,17,1,NULL,NULL,1)
,(1480,165,10,1,NULL,NULL,1)
,(1481,165,7,1,NULL,NULL,1)
,(1482,165,14,1,NULL,NULL,1)
,(1483,165,37,1,NULL,NULL,1)
,(1484,165,42,11,NULL,NULL,1)
,(1485,165,15,1,NULL,NULL,1)
,(1486,165,40,1,NULL,NULL,1)
,(1487,165,41,11,NULL,NULL,1)
,(1488,165,38,1,NULL,NULL,1)
,(1489,166,83,1,NULL,NULL,1)
,(1490,166,82,1,NULL,NULL,1)
,(1491,166,84,1,NULL,NULL,1)
,(1492,166,85,1,NULL,NULL,1)
,(1493,166,81,1,NULL,NULL,1)
,(1494,166,86,1,NULL,NULL,1)
,(1495,166,78,1,NULL,NULL,1)
,(1496,166,79,1,NULL,NULL,1)
,(1497,166,80,11,NULL,NULL,1)
,(1498,167,83,1,NULL,NULL,1)
,(1499,167,82,1,NULL,NULL,1)
,(1500,167,84,1,NULL,NULL,1)
,(1501,167,85,1,NULL,NULL,1)
,(1502,167,81,1,NULL,NULL,1)
,(1503,167,86,1,NULL,NULL,1)
,(1504,167,87,1,NULL,NULL,1)
,(1505,167,79,1,NULL,NULL,1)
,(1506,167,80,11,NULL,NULL,1)
,(1507,168,17,1,NULL,NULL,1)
,(1508,168,10,1,NULL,NULL,1)
,(1509,168,7,1,NULL,NULL,1)
,(1510,168,37,1,NULL,NULL,1)
,(1511,168,15,1,NULL,NULL,1)
,(1512,168,49,1,NULL,NULL,1)
,(1513,168,38,1,NULL,NULL,1)
,(1514,168,47,11,NULL,NULL,1)
,(1515,168,45,1,NULL,NULL,1)
,(1516,168,46,11,NULL,NULL,1)
,(1517,169,17,1,NULL,NULL,1)
,(1518,169,10,1,NULL,NULL,1)
,(1519,169,7,1,NULL,NULL,1)
,(1520,169,37,1,NULL,NULL,1)
,(1521,169,15,1,NULL,NULL,1)
,(1522,169,50,1,NULL,NULL,1)
,(1523,169,38,1,NULL,NULL,1)
,(1524,169,47,11,NULL,NULL,1)
,(1525,169,45,1,NULL,NULL,1)
,(1526,169,46,11,NULL,NULL,1)
,(1527,170,17,1,NULL,NULL,1)
,(1528,170,10,1,NULL,NULL,1)
,(1529,170,7,1,NULL,NULL,1)
,(1530,170,14,1,NULL,NULL,1)
,(1531,170,37,1,NULL,NULL,1)
,(1532,170,42,11,NULL,NULL,1)
,(1533,170,15,1,NULL,NULL,1)
,(1534,170,41,11,NULL,NULL,1)
,(1535,170,38,1,NULL,NULL,1)
,(1536,170,43,1,NULL,NULL,1)
,(1537,171,96,1,NULL,NULL,1)
,(1538,171,95,1,NULL,NULL,1)
,(1539,171,94,1,NULL,NULL,1)
,(1541,171,92,1,NULL,NULL,1)
,(1542,171,89,1,NULL,NULL,1)
,(1543,171,88,1,NULL,NULL,1)
,(1544,171,90,11,NULL,NULL,1)
,(1545,171,91,1,NULL,NULL,1)
,(1546,172,52,1,NULL,NULL,1)
,(1547,172,70,1,NULL,NULL,1)
,(1548,172,66,1,NULL,NULL,1)
,(1549,172,65,1,NULL,NULL,1)
,(1550,172,63,1,NULL,NULL,1)
,(1551,172,62,1,NULL,NULL,1)
,(1552,172,68,1,NULL,NULL,1)
,(1553,172,69,1,NULL,NULL,1)
,(1554,172,67,1,NULL,NULL,1)
,(1555,173,102,1,NULL,NULL,1)
,(1557,173,92,1,NULL,NULL,1)
,(1558,173,100,1,NULL,NULL,1)
,(1559,173,101,1,NULL,NULL,1)
,(1560,173,98,1,NULL,NULL,1)
,(1561,173,97,1,NULL,NULL,1)
,(1562,173,99,11,NULL,NULL,1)
,(1566,174,136,1,'Abnormal or High',NULL,1)
,(1567,174,31,1,'Abnormal',NULL,1)
,(1569,175,10323,1,'Low',NULL,1)
,(1570,176,146,1,'Low',NULL,1)
,(1571,176,10423,1,'Off',NULL,1)
,(1573,176,10323,1,'Low',NULL,1)
,(1574,177,10323,1,'Changed Profile',NULL,1)
,(1575,178,136,1,'High',NULL,1)
,(1576,179,10242,1,'Increasing',NULL,1)
,(1577,179,10241,1,'Increasing',NULL,1)
,(1578,179,10247,1,'Decreasing',NULL,1)
,(1579,179,10256,1,'Low',NULL,1)
,(1580,180,10242,1,'Increasing',NULL,1)
,(1581,180,10241,1,'Normal',NULL,1)
,(1582,180,10247,1,'Normal',NULL,1)
,(1583,180,10256,1,'Normal',NULL,1)
,(1584,181,10243,1,'Low',NULL,1)
,(1585,181,10468,1,'Normal',6052,1)
,(1586,181,10473,1,'Normal',6052,1)
,(1587,181,10439,1,'Decreasing (Closing) or Changed Profile',NULL,1)
,(1588,182,10243,1,'Low',NULL,1)
,(1589,182,10468,1,'Low',6052,1)
,(1590,182,10473,1,'Low',6052,1)
,(1591,182,10439,1,'Normal',NULL,1)
,(1592,183,10243,1,'Low',NULL,1)
,(1593,183,10468,1,'Normal',6052,1)
,(1594,183,10473,1,'High',6052,1)
,(1595,183,10439,1,'Normal',NULL,1)
,(1597,184,10243,1,'Low',NULL,1)
,(1598,184,10468,1,'Normal',6052,1)
,(1599,184,10473,1,'Normal',6052,1)
,(1600,184,10439,1,'Normal or Increasing',NULL,1)
,(1602,185,10468,1,'Normal',6052,1)
,(1603,185,10473,1,'Normal',6052,1)
,(1604,185,10439,1,'Normal or Increasing',NULL,1)
,(1605,186,2015,1,'Normal',NULL,1)
,(1606,186,136,1,'Normal',NULL,1)
,(1607,186,10321,1,'Normal',NULL,1)
,(1608,186,10256,1,'Normal or High',NULL,1)
,(1609,186,10259,1,'Normal or Decreasing',NULL,1)
,(1610,186,10241,1,'High',NULL,1)
,(1611,186,10242,1,'High',NULL,1)
,(1612,186,10238,1,'Normal',NULL,1)
,(1613,186,10236,1,'Low',NULL,1)
,(1614,186,10237,1,'Normal or Low',NULL,1)
,(1615,187,2015,1,'Low',NULL,1)
,(1616,187,136,1,'Normal',NULL,1)
,(1617,187,10321,1,'Normal',NULL,1)
,(1618,187,10256,1,'Normal or High',NULL,1)
,(1619,187,10259,1,'Normal or Decreasing',NULL,1)
,(1620,187,10241,1,'Normal',NULL,1)
,(1621,187,10242,1,'Normal',NULL,1)
,(1622,187,10238,1,'Normal',NULL,1)
,(1623,187,10236,1,'Low',NULL,1)
,(1624,187,10237,1,'Normal or Low',NULL,1)
,(1625,188,2015,1,'Normal',NULL,1)
,(1626,188,10243,1,'Normal',NULL,1)
,(1627,188,10321,1,'Normal',NULL,1)
,(1628,188,10256,1,'Low',NULL,1)
,(1629,188,10259,1,'Low',NULL,1)
,(1630,188,10241,1,'Normal',NULL,1)
,(1631,188,10242,1,'Normal',NULL,1)
,(1632,188,10236,1,'Normal',NULL,1)
,(1633,188,10237,1,'Low',NULL,1)
,(1634,189,2015,1,'Normal',NULL,1)
,(1635,189,10243,1,'Normal',NULL,1)
,(1636,189,10321,1,'Normal',NULL,1)
,(1637,189,10256,1,'Low',NULL,1)
,(1638,189,10259,1,'Low',NULL,1)
,(1639,189,10241,1,'Normal',NULL,1)
,(1640,189,10242,1,'Normal',NULL,1)
,(1641,189,10236,1,'Normal',NULL,1)
,(1642,189,10237,1,'Low',NULL,1)
,(1643,190,2015,1,'Normal',NULL,1)
,(1644,190,10243,1,'Normal',NULL,1)
,(1645,190,10321,1,'Normal',NULL,1)
,(1646,190,10256,1,'Low',NULL,1)
,(1647,190,10259,1,'Low',NULL,1)
,(1648,190,10241,1,'Normal',NULL,1)
,(1649,190,10242,1,'Normal',NULL,1)
,(1650,190,10236,1,'Normal',NULL,1)
,(1651,190,10237,1,'Low',NULL,1)
,(1652,191,2015,1,'Normal',NULL,1)
,(1653,191,31,1,'Normal or High',NULL,1)
,(1654,191,10243,1,'Normal',NULL,1)
,(1655,191,10321,1,'Normal',NULL,1)
,(1656,191,10256,1,'Low',NULL,1)
,(1657,191,10259,1,'Low',NULL,1)
,(1658,191,10241,1,'Normal',NULL,1)
,(1659,191,10242,1,'Normal',NULL,1)
,(1660,191,10236,1,'Normal',NULL,1)
,(1661,191,10237,1,'Low',NULL,1)
,(1662,191,10439,1,'Inceasing ( Opening)',NULL,1)
,(1663,192,2015,1,'Normal or Decreasing',NULL,1)
,(1664,192,10243,1,'Normal',NULL,1)
,(1665,192,10321,1,'Normal or Low',NULL,1)
,(1666,192,10256,1,'Normal or Change',NULL,1)
,(1667,192,10259,1,'Normal or Low',NULL,1)
,(1668,192,10241,1,'Normal or High',NULL,1)
,(1669,192,10242,1,'Normal or High',NULL,1)
,(1670,192,10236,1,'Low',NULL,1)
,(1671,192,10237,1,'Low',NULL,1)
,(1672,193,2015,1,'Normal',NULL,1)
,(1673,193,10243,1,'Normal',NULL,1)
,(1674,193,10321,1,'Normal',NULL,1)
,(1675,193,10256,1,'Normal or Decreasing',NULL,1)
,(1676,193,10259,1,'Normal or Decreasing',NULL,1)
,(1677,193,10241,1,'Normal',NULL,1)
,(1678,193,10242,1,'Normal',NULL,1)
,(1679,193,10236,1,'Changed Profile',NULL,1)
,(1680,193,10237,1,'Low',NULL,1)
,(1682,194,2015,1,'Normal',NULL,1)
,(1683,194,10321,1,'Normal',NULL,1)
,(1684,194,10256,1,'Low',NULL,1)
,(1685,194,10259,1,'Normal or Low',NULL,1)
,(1686,194,10247,1,'Normal',NULL,1)
,(1687,194,10241,1,'Normal',NULL,1)
,(1688,194,10242,1,'Normal',NULL,1)
,(1689,194,10236,1,'Normal',NULL,1)
,(1690,194,10237,1,'Low',NULL,1)
,(1692,195,2015,1,'Normal',NULL,1)
,(1693,195,10321,1,'Normal',NULL,1)
,(1694,195,10256,1,'Low',NULL,1)
,(1695,195,10259,1,'Normal or Low',NULL,1)
,(1696,195,10247,1,'Normal',NULL,1)
,(1697,195,10241,1,'Normal',NULL,1)
,(1698,195,10242,1,'Normal',NULL,1)
,(1699,195,10236,1,'Normal',NULL,1)
,(1700,195,10237,1,'Low',NULL,1)
,(1702,196,2015,1,'Normal',NULL,1)
,(1703,196,10321,1,'Normal',NULL,1)
,(1704,196,10256,1,'Low',NULL,1)
,(1705,196,10259,1,'Normal or Low',NULL,1)
,(1706,196,10247,1,'Normal',NULL,1)
,(1707,196,10241,1,'Normal',NULL,1)
,(1708,196,10242,1,'Normal',NULL,1)
,(1709,196,10236,1,'Normal',NULL,1)
,(1710,196,10237,1,'Low',NULL,1)
,(1712,197,2015,1,'Normal',NULL,1)
,(1713,197,10321,1,'Normal',NULL,1)
,(1714,197,10256,1,'Low',NULL,1)
,(1715,197,10259,1,'Normal or Decreasing',NULL,1)
,(1716,197,10247,1,'Normal',NULL,1)
,(1717,197,10241,1,'Normal',NULL,1)
,(1718,197,10242,1,'Normal',NULL,1)
,(1719,197,10236,1,'Normal',NULL,1)
,(1720,197,10237,1,'Low',NULL,1)
,(1722,197,10439,1,'Inreasing ( Opening)',NULL,1)
,(1723,198,2015,1,'Normal or Decreasing',NULL,1)
,(1724,198,10243,1,'Normal',NULL,1)
,(1725,198,10321,1,'Normal or Low',NULL,1)
,(1726,198,10256,1,'Low',NULL,1)
,(1727,198,10259,1,'Normal or Low',NULL,1)
,(1728,198,10241,1,'Normal or High',NULL,1)
,(1729,198,10242,1,'Normal or High',NULL,1)
,(1730,198,10236,1,'Low',NULL,1)
,(1731,198,10237,1,'Low',NULL,1)
,(1732,199,2015,1,'Normal',NULL,1)
,(1733,199,10243,1,'Normal',NULL,1)
,(1734,199,10321,1,'Normal',NULL,1)
,(1735,199,10256,1,'Low',NULL,1)
,(1736,199,10259,1,'Normal or Decreasing',NULL,1)
,(1737,199,10241,1,'Normal',NULL,1)
,(1738,199,10242,1,'Normal',NULL,1)
,(1739,199,10236,1,'Changed Profile',NULL,1)
,(1740,199,10237,1,'Low',NULL,1)
,(1742,200,10257,1,'Increasing',NULL,1)
,(1743,200,10256,1,'Normal ( Constant )',NULL,1)
,(1744,200,10259,1,'Decreasing',NULL,1)
,(1745,201,2015,1,'Normal',NULL,1)
,(1746,201,10243,1,'Normal',NULL,1)
,(1747,201,10321,1,'Normal',NULL,1)
,(1748,201,10256,1,'Normal or Decreasing',NULL,1)
,(1749,201,10259,1,'Decreasing',NULL,1)
,(1750,201,10241,1,'Normal',NULL,1)
,(1751,201,10242,1,'Normal',NULL,1)
,(1752,201,10236,1,'Normal',NULL,1)
,(1753,201,10237,1,'Low',NULL,1)
,(1755,202,2015,1,'Normal',NULL,1)
,(1756,202,10243,1,'Normal',NULL,1)
,(1757,202,10321,1,'Normal',NULL,1)
,(1758,202,10256,1,'Normal or Decreasing',NULL,1)
,(1759,202,10259,1,'Decreasing',NULL,1)
,(1760,202,10241,1,'Normal',NULL,1)
,(1761,202,10242,1,'Normal',NULL,1)
,(1762,202,10236,1,'Normal',NULL,1)
,(1763,202,10237,1,'Low',NULL,1)
,(1765,203,2015,1,'Normal',NULL,1)
,(1766,203,10243,1,'Normal',NULL,1)
,(1767,203,10321,1,'Normal',NULL,1)
,(1768,203,10256,1,'Normal or Decreasing',NULL,1)
,(1769,203,10259,1,'Decreasing',NULL,1)
,(1770,203,10241,1,'Normal',NULL,1)
,(1771,203,10242,1,'Normal',NULL,1)
,(1772,203,10236,1,'Normal',NULL,1)
,(1773,203,10237,1,'Low',NULL,1)
,(1775,204,2015,1,'Normal',NULL,1)
,(1776,204,10321,1,'Normal',NULL,1)
,(1777,204,10256,1,'Low',NULL,1)
,(1778,204,10259,1,'Decreasing',NULL,1)
,(1779,204,10247,1,'Normal',NULL,1)
,(1780,204,10241,1,'Normal',NULL,1)
,(1781,204,10242,1,'Normal',NULL,1)
,(1782,204,10236,1,'Normal',NULL,1)
,(1783,204,10237,1,'Low',NULL,1)
,(1785,204,10439,1,'Inreasing ( Opening)',NULL,1)
,(1786,205,2015,1,'Normal or Decreasing',NULL,1)
,(1787,205,10243,1,'Normal',NULL,1)
,(1788,205,10321,1,'Normal or Low',NULL,1)
,(1789,205,10256,1,'Low',NULL,1)
,(1790,205,10259,1,'Decreasing',NULL,1)
,(1791,205,10241,1,'Normal or High',NULL,1)
,(1792,205,10242,1,'Normal or High',NULL,1)
,(1793,205,10236,1,'Low',NULL,1)
,(1794,205,10237,1,'Low',NULL,1)
,(1795,206,2015,1,'Normal',NULL,1)
,(1796,206,10243,1,'Normal',NULL,1)
,(1797,206,10321,1,'Normal',NULL,1)
,(1798,206,10256,1,'Low',NULL,1)
,(1799,206,10259,1,'Decreasing',NULL,1)
,(1800,206,10241,1,'Normal',NULL,1)
,(1801,206,10242,1,'Normal',NULL,1)
,(1802,206,10236,1,'Changed Profile',NULL,1)
,(1803,206,10237,1,'Low',NULL,1)
,(1805,207,10259,1,'Low or Changed Profile',NULL,1)
,(1806,207,10255,1,'Changed',NULL,1)
,(1807,207,10298,1,'Changed',NULL,1)
,(1808,207,2015,1,'Normal',NULL,1)
,(1809,207,10238,1,'Normal or Changed Profile',NULL,1)
,(1810,207,10321,1,'Normal',NULL,1)
,(1811,207,10256,1,'Normal',NULL,1)
,(1812,207,10241,1,'Normal',NULL,1)
,(1813,207,10242,1,'Normal',NULL,1)
,(1814,207,10236,1,'Normal or Changed Profile',NULL,1)
,(1815,207,10237,1,'Normal or Changed Profile',NULL,1)
,(1817,207,10267,1,'Related',NULL,1)
,(1818,208,10259,1,'Low',NULL,1)
,(1819,208,10255,1,'Changed Profile',NULL,1)
,(1820,208,2015,1,'Normal',NULL,1)
,(1821,208,10238,1,'Normal',NULL,1)
,(1822,208,10321,1,'Normal',NULL,1)
,(1823,208,10256,1,'Changed Profile',NULL,1)
,(1824,208,10241,1,'Normal',NULL,1)
,(1825,208,10242,1,'Normal',NULL,1)
,(1826,208,10236,1,'Normal',NULL,1)
,(1827,208,10237,1,'Changed Profile',NULL,1)
,(1829,208,10267,1,'Related',NULL,1)
,(1830,209,10256,1,'Normal or Low',NULL,1)
,(1831,209,10259,1,'Decreasing',NULL,1)
,(1832,209,10241,1,'Normal or High',NULL,1)
,(1833,209,10242,1,'Normal or High',NULL,1)
,(1834,209,10236,1,'Normal or Low',NULL,1)
,(1835,209,10237,1,'Low',NULL,1)
,(1837,209,10261,1,'Decreasing',NULL,1)
,(1838,209,2010,1,'Decreasing',NULL,1)
,(1839,210,10256,1,'Normal or Low',NULL,1)
,(1840,210,10259,1,'Decreasing',NULL,1)
,(1841,210,10241,1,'Normal or High',NULL,1)
,(1842,210,10242,1,'Normal or High',NULL,1)
,(1843,210,10236,1,'Normal or Low',NULL,1)
,(1844,210,10237,1,'Low',NULL,1)
,(1846,210,10261,1,'Decreasing',NULL,1)
,(1847,210,2010,1,'Decreasing',NULL,1)
,(1848,211,10256,1,'Normal',NULL,1)
,(1849,211,10259,1,'Normal',NULL,1)
,(1850,211,10241,1,'Normal',NULL,1)
,(1851,211,10242,1,'Normal',NULL,1)
,(1852,211,10236,1,'Normal',NULL,1)
,(1853,211,10237,1,'Normal',NULL,1)
,(1854,211,10261,1,'Decreasing',NULL,1)
,(1855,211,2010,1,'Decreasing',NULL,1)
,(1856,211,10244,1,'High',NULL,1)
,(1857,212,10256,1,'Normal',NULL,1)
,(1858,212,10259,1,'Normal',NULL,1)
,(1859,212,10241,1,'Normal',NULL,1)
,(1860,212,10242,1,'Normal',NULL,1)
,(1861,212,10236,1,'Normal',NULL,1)
,(1862,212,10237,1,'Normal',NULL,1)
,(1863,212,10261,1,'Decreasing',NULL,1)
,(1864,212,2010,1,'Decreasing',NULL,1)
,(1865,212,10244,1,'Normal',NULL,1)
,(1866,212,10257,1,'Low',NULL,1)
,(1867,213,10256,1,'Normal',NULL,1)
,(1868,213,10259,1,'Normal',NULL,1)
,(1869,213,10241,1,'Normal',NULL,1)
,(1870,213,10242,1,'Normal',NULL,1)
,(1871,213,10236,1,'Normal',NULL,1)
,(1872,213,10237,1,'Normal',NULL,1)
,(1873,213,10261,1,'Decreasing',NULL,1)
,(1874,213,2010,1,'Decreasing',NULL,1)
,(1875,213,10244,1,'Normal or High',NULL,1)
,(1876,213,10239,1,'High',NULL,1)
,(1877,214,10256,1,'Normal or Low',NULL,1)
,(1878,214,10259,1,'Decreasing',NULL,1)
,(1879,214,10241,1,'Normal or High',NULL,1)
,(1880,214,10242,1,'Normal or High',NULL,1)
,(1881,214,10236,1,'Normal or Low',NULL,1)
,(1882,214,10237,1,'Low',NULL,1)
,(1884,214,10261,1,'Decreasing',NULL,1)
,(1885,214,2010,1,'Decreasing',NULL,1)
,(1886,214,10260,1,'Decreasing',NULL,1)
,(1887,214,150,1,'Decreasing',NULL,1)
,(1888,215,10256,1,'Normal or Low',NULL,1)
,(1889,215,10259,1,'Decreasing',NULL,1)
,(1890,215,10241,1,'Normal or High',NULL,1)
,(1891,215,10242,1,'Normal or High',NULL,1)
,(1892,215,10236,1,'Normal or Low',NULL,1)
,(1893,215,10237,1,'Low',NULL,1)
,(1895,215,10261,1,'Decreasing',NULL,1)
,(1896,215,2010,1,'Decreasing',NULL,1)
,(1897,215,10260,1,'Decreasing',NULL,1)
,(1898,215,150,1,'Decreasing',NULL,1)
,(1899,216,10256,1,'Normal',NULL,1)
,(1900,216,10259,1,'Normal',NULL,1)
,(1901,216,10241,1,'Normal',NULL,1)
,(1902,216,10242,1,'Normal',NULL,1)
,(1903,216,10236,1,'Normal',NULL,1)
,(1904,216,10237,1,'Normal',NULL,1)
,(1905,216,10261,1,'Decreasing',NULL,1)
,(1906,216,2010,1,'Decreasing',NULL,1)
,(1907,216,10260,1,'Decreasing',NULL,1)
,(1908,216,150,1,'Decreasing',NULL,1)
,(1909,216,10244,1,'High',NULL,1)
,(1910,217,10256,1,'Normal',NULL,1)
,(1911,217,10259,1,'Normal',NULL,1)
,(1912,217,10241,1,'Normal',NULL,1)
,(1913,217,10242,1,'Normal',NULL,1)
,(1914,217,10236,1,'Normal',NULL,1)
,(1915,217,10237,1,'Normal',NULL,1)
,(1916,217,10261,1,'Decreasing',NULL,1)
,(1917,217,2010,1,'Decreasing',NULL,1)
,(1918,217,10260,1,'Decreasing',NULL,1)
,(1919,217,150,1,'Decreasing',NULL,1)
,(1920,217,10244,1,'Normal',NULL,1)
,(1921,217,10257,1,'Low',NULL,1)
,(1922,218,10256,1,'Normal',NULL,1)
,(1923,218,10259,1,'Normal',NULL,1)
,(1924,218,10241,1,'Normal',NULL,1)
,(1925,218,10242,1,'Normal',NULL,1)
,(1926,218,10236,1,'Normal',NULL,1)
,(1927,218,10237,1,'Normal',NULL,1)
,(1928,218,10261,1,'Decreasing',NULL,1)
,(1929,218,2010,1,'Decreasing',NULL,1)
,(1930,218,10260,1,'Decreasing',NULL,1)
,(1931,218,150,1,'Decreasing',NULL,1)
,(1932,218,10244,1,'Normal or High',NULL,1)
,(1933,218,10239,1,'High',NULL,1)
,(1934,219,10256,1,'Normal',NULL,1)
,(1935,219,10259,1,'Normal or Low',NULL,1)
,(1936,219,10241,1,'Normal',NULL,1)
,(1937,219,10242,1,'Normal',NULL,1)
,(1938,219,10236,1,'Normal',NULL,1)
,(1939,219,10237,1,'Normal',NULL,1)
,(1940,219,10260,1,'Decreasing',NULL,1)
,(1941,219,150,1,'Decreasing',NULL,1)
,(1942,219,10244,1,'Normal',NULL,1)
,(1943,219,10239,1,'Normal',NULL,1)
,(1944,219,10321,1,'Low',NULL,1)
,(1945,219,136,1,'Normal or High',NULL,1)
,(1946,220,10256,1,'Normal or Low',NULL,1)
,(1947,220,10259,1,'Normal or Low',NULL,1)
,(1948,220,10241,1,'Normal',NULL,1)
,(1949,220,10242,1,'Normal',NULL,1)
,(1950,220,10236,1,'Normal',NULL,1)
,(1951,220,10237,1,'Normal or Low',NULL,1)
,(1952,220,10261,1,'Normal',NULL,1)
,(1953,220,2010,1,'Normal',NULL,1)
,(1954,220,10260,1,'Decreasing',NULL,1)
,(1955,220,150,1,'Decreasing',NULL,1)
,(1956,220,10244,1,'Normal',NULL,1)
,(1957,220,10239,1,'Normal',NULL,1)
,(1958,220,10321,1,'Normal to Low',NULL,1)
,(1959,220,136,1,'Normal or High',NULL,1)
,(1960,220,31,1,'High',NULL,1)
,(1961,221,10256,1,'Normal',NULL,1)
,(1962,221,10259,1,'Normal or Low',NULL,1)
,(1963,221,10241,1,'Normal',NULL,1)
,(1964,221,10242,1,'Normal',NULL,1)
,(1965,221,10236,1,'Normal',NULL,1)
,(1966,221,10237,1,'Normal',NULL,1)
,(1967,221,10261,1,'Normal',NULL,1)
,(1968,221,2010,1,'Normal',NULL,1)
,(1969,221,10260,1,'Decreasing',NULL,1)
,(1970,221,150,1,'Decreasing',NULL,1)
,(1971,221,10244,1,'Normal',NULL,1)
,(1972,221,10239,1,'Normal',NULL,1)
,(1973,221,10321,1,'Low',NULL,1)
,(1974,221,136,1,'Normal or High',NULL,1)
,(1975,222,10256,1,'Normal or Low',NULL,1)
,(1976,222,10259,1,'Decreasing',NULL,1)
,(1977,222,10241,1,'Normal or High',NULL,1)
,(1978,222,10242,1,'Normal or High',NULL,1)
,(1979,222,10236,1,'Normal or Low',NULL,1)
,(1980,222,10237,1,'Low',NULL,1)
,(1982,222,10261,1,'Decreasing',NULL,1)
,(1983,222,2010,1,'Decreasing',NULL,1)
,(1984,223,10256,1,'Normal or Low',NULL,1)
,(1985,223,10259,1,'Decreasing',NULL,1)
,(1986,223,10241,1,'Normal or High',NULL,1)
,(1987,223,10242,1,'Normal or High',NULL,1)
,(1988,223,10236,1,'Normal or Low',NULL,1)
,(1989,223,10237,1,'Low',NULL,1)
,(1991,223,10261,1,'Decreasing',NULL,1)
,(1992,223,2010,1,'Decreasing',NULL,1)
,(1993,224,10256,1,'Normal',NULL,1)
,(1994,224,10259,1,'Normal',NULL,1)
,(1995,224,10241,1,'Normal',NULL,1)
,(1996,224,10242,1,'Normal',NULL,1)
,(1997,224,10236,1,'Normal',NULL,1)
,(1998,224,10237,1,'Normal',NULL,1)
,(1999,224,10261,1,'Decreasing',NULL,1)
,(2000,224,2010,1,'Decreasing',NULL,1)
,(2001,225,10244,1,'High',NULL,1)
,(2002,225,10256,1,'Normal',NULL,1)
,(2003,225,10259,1,'Normal',NULL,1)
,(2004,225,10241,1,'Normal',NULL,1)
,(2005,225,10242,1,'Normal',NULL,1)
,(2006,225,10236,1,'Normal',NULL,1)
,(2007,225,10237,1,'Normal',NULL,1)
,(2008,225,10261,1,'Decreasing',NULL,1)
,(2009,225,2010,1,'Decreasing',NULL,1)
,(2010,225,10244,1,'Normal',NULL,1)
,(2011,225,10257,1,'Low',NULL,1)
,(2012,226,10256,1,'Normal',NULL,1)
,(2013,226,10259,1,'Normal',NULL,1)
,(2014,226,10241,1,'Normal',NULL,1)
,(2015,226,10242,1,'Normal',NULL,1)
,(2016,226,10236,1,'Normal',NULL,1)
,(2017,226,10237,1,'Normal',NULL,1)
,(2018,226,10261,1,'Decreasing',NULL,1)
,(2019,226,2010,1,'Decreasing',NULL,1)
,(2020,226,10244,1,'Normal or High',NULL,1)
,(2021,226,10239,1,'High',NULL,1)
,(2022,227,10256,1,'Normal or Low',NULL,1)
,(2023,227,10259,1,'Decreasing',NULL,1)
,(2024,227,10241,1,'Normal or High',NULL,1)
,(2025,227,10242,1,'Normal or High',NULL,1)
,(2026,227,10236,1,'Normal or Low',NULL,1)
,(2027,227,10237,1,'Low',NULL,1)
,(2029,227,10260,1,'Decreasing',NULL,1)
,(2030,227,150,1,'Decreasing',NULL,1)
,(2031,227,10261,1,'Decreasing',NULL,1)
,(2032,227,2010,1,'Decreasing',NULL,1)
,(2033,228,10256,1,'Normal or Low',NULL,1)
,(2034,228,10259,1,'Decreasing',NULL,1)
,(2035,228,10241,1,'Normal or High',NULL,1)
,(2036,228,10242,1,'Normal or High',NULL,1)
,(2037,228,10236,1,'Normal or Low',NULL,1)
,(2038,228,10237,1,'Low',NULL,1)
,(2040,228,10260,1,'Decreasing',NULL,1)
,(2041,228,150,1,'Decreasing',NULL,1)
,(2042,228,10261,1,'Decreasing',NULL,1)
,(2043,228,2010,1,'Decreasing',NULL,1)
,(2044,229,10256,1,'Normal',NULL,1)
,(2045,229,10259,1,'Normal',NULL,1)
,(2046,229,10241,1,'Normal',NULL,1)
,(2047,229,10242,1,'Normal',NULL,1)
,(2048,229,10236,1,'Normal',NULL,1)
,(2049,229,10237,1,'Normal',NULL,1)
,(2050,229,10261,1,'Decreasing',NULL,1)
,(2051,229,2010,1,'Decreasing',NULL,1)
,(2052,229,10260,1,'Decreasing',NULL,1)
,(2053,229,150,1,'Decreasing',NULL,1)
,(2054,229,10244,1,'High',NULL,1)
,(2055,230,10256,1,'Normal',NULL,1)
,(2056,230,10259,1,'Normal',NULL,1)
,(2057,230,10241,1,'Normal',NULL,1)
,(2058,230,10242,1,'Normal',NULL,1)
,(2059,230,10236,1,'Normal',NULL,1)
,(2060,230,10237,1,'Normal',NULL,1)
,(2061,230,10261,1,'Decreasing',NULL,1)
,(2062,230,2010,1,'Decreasing',NULL,1)
,(2063,230,10260,1,'Decreasing',NULL,1)
,(2064,230,150,1,'Decreasing',NULL,1)
,(2065,230,10244,1,'Normal',NULL,1)
,(2066,230,10257,1,'Low',NULL,1)
,(2067,231,10256,1,'Normal',NULL,1)
,(2068,231,10259,1,'Normal',NULL,1)
,(2069,231,10241,1,'Normal',NULL,1)
,(2070,231,10242,1,'Normal',NULL,1)
,(2071,231,10236,1,'Normal',NULL,1)
,(2072,231,10237,1,'Normal',NULL,1)
,(2073,231,10261,1,'Decreasing',NULL,1)
,(2074,231,2010,1,'Decreasing',NULL,1)
,(2075,231,10260,1,'Decreasing',NULL,1)
,(2076,231,150,1,'Decreasing',NULL,1)
,(2077,231,10244,1,'Normal or High',NULL,1)
,(2078,231,10239,1,'High',NULL,1)
,(2079,232,10256,1,'Normal',NULL,1)
,(2080,232,10259,1,'Normal or Low',NULL,1)
,(2081,232,10241,1,'Normal',NULL,1)
,(2082,232,10242,1,'Normal',NULL,1)
,(2083,232,10236,1,'Normal',NULL,1)
,(2084,232,10237,1,'Normal',NULL,1)
,(2085,232,10260,1,'Decreasing',NULL,1)
,(2086,232,150,1,'Decreasing',NULL,1)
,(2087,232,10244,1,'Normal',NULL,1)
,(2088,232,10239,1,'Normal',NULL,1)
,(2089,232,10321,1,'Low',NULL,1)
,(2090,232,136,1,'Normal or High',NULL,1)
,(2091,233,10256,1,'Normal or Low',NULL,1)
,(2092,233,10259,1,'Normal or Low',NULL,1)
,(2093,233,10241,1,'Normal',NULL,1)
,(2094,233,10242,1,'Normal',NULL,1)
,(2095,233,10236,1,'Normal',NULL,1)
,(2096,233,10237,1,'Normal or Low',NULL,1)
,(2097,233,10261,1,'Normal',NULL,1)
,(2098,233,2010,1,'Normal',NULL,1)
,(2099,233,10260,1,'Decreasing',NULL,1)
,(2100,233,150,1,'Decreasing',NULL,1)
,(2101,233,10244,1,'Normal',NULL,1)
,(2102,233,10239,1,'Normal',NULL,1)
,(2103,233,10321,1,'Normal to Low',NULL,1)
,(2104,233,136,1,'Normal or High',NULL,1)
,(2105,233,31,1,'High',NULL,1)
,(2106,234,10256,1,'Normal',NULL,1)
,(2107,234,10259,1,'Normal or Low',NULL,1)
,(2108,234,10241,1,'Normal',NULL,1)
,(2109,234,10242,1,'Normal',NULL,1)
,(2110,234,10236,1,'Normal',NULL,1)
,(2111,234,10237,1,'Normal',NULL,1)
,(2112,234,10261,1,'Normal',NULL,1)
,(2113,234,2010,1,'Normal',NULL,1)
,(2114,234,10260,1,'Decreasing',NULL,1)
,(2115,234,150,1,'Decreasing',NULL,1)
,(2116,234,10244,1,'Normal',NULL,1)
,(2117,234,10239,1,'Normal',NULL,1)
,(2118,234,10321,1,'Low',NULL,1)
,(2119,234,136,1,'Normal or High',NULL,1)
,(2120,235,10239,1,'Normal',NULL,1)
,(2121,235,10244,1,'High',NULL,1)
,(2122,235,10265,1,'Decreasing',NULL,1)
,(2123,235,150,1,'Decreasing',NULL,1)
,(2124,235,10313,1,'Low',NULL,1)
,(2125,235,10312,1,'Normal',NULL,1)
,(2126,235,10315,1,'Low',NULL,1)
,(2128,235,2523,1,'Low',NULL,1)
,(2129,235,2526,1,'Low',NULL,1)
,(2130,236,10239,1,'Normal',NULL,1)
,(2131,236,10244,1,'High',NULL,1)
,(2132,236,10265,1,'Decreasing',NULL,1)
,(2133,236,150,1,'Decreasing',NULL,1)
,(2134,236,10313,1,'High',NULL,1)
,(2135,236,10312,1,'Normal',NULL,1)
,(2136,236,10315,1,'Low',NULL,1)
,(2138,237,10239,1,'Normal',NULL,1)
,(2139,237,10244,1,'High',NULL,1)
,(2140,237,10265,1,'Decreasing',NULL,1)
,(2141,237,150,1,'Decreasing',NULL,1)
,(2142,237,10313,1,'High',NULL,1)
,(2143,237,10312,1,'Normal or High',NULL,1)
,(2144,237,10315,1,'Low',NULL,1)
,(2146,237,2523,1,'High',NULL,1)
,(2147,237,2526,1,'Low',NULL,1)
,(2149,238,10296,1,'High',NULL,1)
,(2150,238,10302,1,'Low',NULL,1)
,(2151,238,10303,1,'Low',NULL,1)
,(2152,238,10304,1,'Normal',NULL,1)
,(2153,238,10312,1,'Normal',NULL,1)
,(2154,238,10313,1,'Normal',NULL,1)
,(2156,239,10296,1,'High',NULL,1)
,(2157,239,10302,1,'Low',NULL,1)
,(2158,239,10303,1,'Low',NULL,1)
,(2159,239,10304,1,'Normal',NULL,1)
,(2160,239,10312,1,'Normal',NULL,1)
,(2161,239,10313,1,'Normal',NULL,1)
,(2162,239,10273,1,'Decreasing',NULL,1)
,(2163,239,10274,1,'Decreasing',NULL,1)
,(2164,239,10275,1,'Decreasing',NULL,1)
,(2166,240,10296,1,'High',NULL,1)
,(2167,240,10302,1,'Low',NULL,1)
,(2168,240,10303,1,'Low',NULL,1)
,(2169,240,10304,1,'Normal',NULL,1)
,(2170,240,10312,1,'Normal',NULL,1)
,(2171,240,10313,1,'Normal',NULL,1)
,(2172,240,10273,1,'Decreasing',NULL,1)
,(2173,240,10274,1,'Decreasing',NULL,1)
,(2174,240,10275,1,'Decreasing',NULL,1)
,(2175,240,10292,1,'High',NULL,1)
,(2176,240,2387,1,'High',NULL,1)
,(2178,241,10296,1,'High',NULL,1)
,(2179,241,10302,1,'Low',NULL,1)
,(2180,241,10303,1,'Low',NULL,1)
,(2181,241,10304,1,'Normal',NULL,1)
,(2182,241,10312,1,'Normal',NULL,1)
,(2183,241,10313,1,'Normal',NULL,1)
,(2184,241,10273,1,'Decreasing',NULL,1)
,(2185,241,10274,1,'Decreasing',NULL,1)
,(2186,241,10275,1,'Decreasing',NULL,1)
,(2187,241,10292,1,'High',NULL,1)
,(2188,241,2387,1,'High',NULL,1)
,(2189,241,2391,1,'Normal',NULL,1)
,(2190,241,2392,1,'Normal',NULL,1)
,(2191,241,2393,1,'Normal',NULL,1)
,(2192,241,2394,1,'Normal',NULL,1)
,(2194,242,10296,1,'Normal or Low',NULL,1)
,(2195,242,10302,1,'Low',NULL,1)
,(2196,242,10303,1,'Low',NULL,1)
,(2197,242,10304,1,'Normal',NULL,1)
,(2198,242,10312,1,'Normal',NULL,1)
,(2199,242,10313,1,'Normal',NULL,1)
,(2200,242,10315,1,'Low',NULL,1)
,(2202,243,10296,1,'Normal to High',NULL,1)
,(2203,243,10302,1,'Low',NULL,1)
,(2204,243,10345,1,'Related',NULL,1)
,(2205,243,10346,1,'Related',NULL,1)
,(2206,243,10347,1,'Related',NULL,1)
,(2208,244,10275,1,'Normal or Low',NULL,1)
,(2209,244,10272,1,'High',NULL,1)
,(2210,244,10391,1,'Normal or Low',NULL,1)
,(2211,244,10301,1,'Low',NULL,1)
,(2212,244,10316,1,'High',NULL,1)
,(2213,245,10275,1,'Normal or Low',NULL,1)
,(2214,245,10272,1,'High',NULL,1)
,(2215,245,10391,1,'Normal or Low',NULL,1)
,(2216,245,10301,1,'Low',NULL,1)
,(2217,245,10316,1,'High',NULL,1)
,(2218,245,2387,1,'High',NULL,1)
,(2219,246,10275,1,'Normal or Low',NULL,1)
,(2220,246,10272,1,'Low',NULL,1)
,(2221,246,10391,1,'Low',NULL,1)
,(2222,246,10301,1,'Low',NULL,1)
,(2223,246,10316,1,'Normal',NULL,1)
,(2224,246,2387,1,'Normal',NULL,1)
,(2225,247,10275,1,'Low',NULL,1)
,(2226,247,10272,1,'Normal or High',NULL,1)
,(2227,247,10391,1,'Normal or Low',NULL,1)
,(2228,247,10301,1,'Low',NULL,1)
,(2229,247,10316,1,'High',NULL,1)
,(2230,248,10275,1,'Low',NULL,1)
,(2231,248,10272,1,'Normal or High',NULL,1)
,(2232,248,10391,1,'Normal or Low',NULL,1)
,(2233,248,10301,1,'Low',NULL,1)
,(2234,248,10316,1,'High',NULL,1)
,(2235,248,2387,1,'High',NULL,1)
,(2236,249,10275,1,'Low',NULL,1)
,(2237,249,10272,1,'Normal',NULL,1)
,(2238,249,10391,1,'Normal or Low',NULL,1)
,(2239,249,10301,1,'Low',NULL,1)
,(2240,249,10316,1,'Low',NULL,1)
,(2241,249,2387,1,'Normal',NULL,1)
,(2242,250,10274,1,'Normal or Low',NULL,1)
,(2243,250,10276,1,'High',NULL,1)
,(2244,250,10390,1,'Normal or Low',NULL,1)
,(2245,250,10300,1,'Low',NULL,1)
,(2246,250,10316,1,'High',NULL,1)
,(2247,251,10274,1,'Normal or Low',NULL,1)
,(2248,251,10276,1,'High',NULL,1)
,(2249,251,10390,1,'Normal or Low',NULL,1)
,(2250,251,10300,1,'Low',NULL,1)
,(2251,251,10316,1,'High',NULL,1)
,(2252,251,2387,1,'High',NULL,1)
,(2253,252,10274,1,'Normal or Low',NULL,1)
,(2254,252,10276,1,'Low',NULL,1)
,(2255,252,10390,1,'Low',NULL,1)
,(2256,252,10300,1,'Low',NULL,1)
,(2257,252,10316,1,'Normal',NULL,1)
,(2258,252,2387,1,'Normal',NULL,1)
,(2259,252,10311,1,'Low',NULL,1)
,(2260,253,10274,1,'Low',NULL,1)
,(2261,253,10276,1,'Normal or High',NULL,1)
,(2262,253,10390,1,'Normal or Low',NULL,1)
,(2263,253,10300,1,'Low',NULL,1)
,(2264,253,10316,1,'High',NULL,1)
,(2265,254,10274,1,'Low',NULL,1)
,(2266,254,10276,1,'Normal or High',NULL,1)
,(2267,254,10390,1,'Normal or Low',NULL,1)
,(2268,254,10300,1,'Low',NULL,1)
,(2269,254,10316,1,'High',NULL,1)
,(2270,255,2387,1,'High',NULL,1)
,(2271,255,10273,1,'Normal or Low',NULL,1)
,(2272,255,10271,1,'High',NULL,1)
,(2273,255,10389,1,'Normal or Low',NULL,1)
,(2274,255,10299,1,'Low',NULL,1)
,(2275,255,10316,1,'High',NULL,1)
,(2276,256,10273,1,'Normal or Low',NULL,1)
,(2277,256,10271,1,'High',NULL,1)
,(2278,256,10389,1,'Normal or Low',NULL,1)
,(2279,256,10299,1,'Low',NULL,1)
,(2280,256,10316,1,'High',NULL,1)
,(2281,256,2387,1,'High',NULL,1)
,(2282,257,10273,1,'Normal or Low',NULL,1)
,(2283,257,10271,1,'Low',NULL,1)
,(2284,257,10389,1,'Low',NULL,1)
,(2285,257,10299,1,'Low',NULL,1)
,(2286,257,10316,1,'Normal',NULL,1)
,(2287,257,2387,1,'Normal',NULL,1)
,(2288,257,10311,1,'Low',NULL,1)
,(2289,258,10273,1,'Low',NULL,1)
,(2290,258,10271,1,'Normal or High',NULL,1)
,(2291,258,10389,1,'Normal or Low',NULL,1)
,(2292,258,10299,1,'Low',NULL,1)
,(2293,258,10316,1,'High',NULL,1)
,(2294,259,10273,1,'Low',NULL,1)
,(2295,259,10271,1,'Normal or High',NULL,1)
,(2296,259,10389,1,'Normal or Low',NULL,1)
,(2297,259,10299,1,'Low',NULL,1)
,(2298,259,10316,1,'High',NULL,1)
,(2299,259,2387,1,'High',NULL,1)
,(2300,260,10273,1,'Normal or Low',NULL,1)
,(2301,260,10271,1,'Normal or High',NULL,1)
,(2302,260,10299,1,'Normal or Low',NULL,1)
,(2303,260,10316,1,'Normal or High',NULL,1)
,(2304,260,10239,1,'Normal',NULL,1)
,(2305,260,10244,1,'Normal',NULL,1)
,(2306,260,2698,1,'Low',NULL,1)
,(2307,260,10295,1,'Normal',NULL,1)
,(2308,260,10296,1,'High',NULL,1)
,(2309,260,10302,1,'Low',NULL,1)
,(2310,261,10316,1,'Normal or Low',NULL,1)
,(2311,261,10239,1,'Normal',NULL,1)
,(2312,261,10244,1,'Normal',NULL,1)
,(2313,261,2698,1,'Low',NULL,1)
,(2314,261,10295,1,'Normal',NULL,1)
,(2315,261,10296,1,'Normal or Low',NULL,1)
,(2316,261,10302,1,'Low',NULL,1)
,(2318,261,10315,1,'Low',NULL,1)
,(2320,262,10302,1,'Low',NULL,1)
,(2321,262,10295,1,'Low',NULL,1)
,(2322,262,10296,1,'Low',NULL,1)
,(2324,263,10292,1,'High',NULL,1)
,(2326,263,2387,1,'High',NULL,1)
,(2327,263,2388,1,'High',NULL,1)
,(2328,263,10239,1,'Normal',NULL,1)
,(2329,263,10244,1,'Normal',NULL,1)
,(2330,263,10265,1,'Normal',NULL,1)
,(2331,263,10295,1,'Normal',NULL,1)
,(2332,264,10292,1,'Normal',NULL,1)
,(2334,264,2387,1,'Normal',NULL,1)
,(2335,264,2388,1,'Normal',NULL,1)
,(2336,264,10239,1,'Normal',NULL,1)
,(2337,264,10244,1,'Normal',NULL,1)
,(2338,264,10265,1,'Normal',NULL,1)
,(2339,264,10295,1,'Normal',NULL,1)
,(2340,264,2698,1,'Low',NULL,1)
,(2342,264,10315,1,'Related',NULL,1)
,(2343,265,2694,1,'Low',NULL,1)
,(2344,265,5018,1,'Normal',NULL,1)
,(2345,265,5019,1,'Low',NULL,1)
,(2346,265,37,1,'High',NULL,1)
,(2347,265,40,1,'Low',NULL,1)
,(2348,266,10273,1,'Normal',NULL,1)
,(2349,266,10389,1,'Normal or Low',NULL,1)
,(2350,266,10299,1,'Low',NULL,1)
,(2351,266,10316,1,'Low',NULL,1)
,(2352,266,2387,1,'Normal',NULL,1)
,(2353,266,10239,1,'Low',NULL,1)
,(2354,266,10295,1,'Low',NULL,1)
,(2355,266,10296,1,'Low',NULL,1)
,(2356,267,2694,1,'Low',NULL,1)
,(2357,267,45,1,'Normal',NULL,1)
,(2358,267,38,1,'High',NULL,1)
,(2359,268,10316,1,'Low',NULL,1)
,(2360,268,2387,1,'Normal',NULL,1)
,(2361,268,10239,1,'Low',NULL,1)
,(2362,268,10295,1,'Low',NULL,1)
,(2363,268,10296,1,'Low',NULL,1)
,(2364,269,10285,1,'Normal',NULL,1)
,(2365,269,10286,1,'Low',NULL,1)
,(2366,269,10289,1,'Normal',NULL,1)
,(2367,269,10290,1,'High',NULL,1)
,(2368,269,10292,1,'Normal or High',NULL,1)
,(2369,269,10275,1,'Normal',NULL,1)
,(2370,269,10391,1,'Normal or Low',NULL,1)
,(2372,270,10285,1,'Normal',NULL,1)
,(2373,270,10286,1,'Low',NULL,1)
,(2374,270,10289,1,'Normal',NULL,1)
,(2375,270,10290,1,'High',NULL,1)
,(2376,270,10292,1,'High',NULL,1)
,(2377,270,10391,1,'Normal',NULL,1)
,(2379,271,10292,1,'High',NULL,1)
,(2380,271,10302,1,'Normal or low',NULL,1)
,(2381,271,2387,1,'Normal',NULL,1)
,(2382,271,2391,1,'Normal',NULL,1)
,(2383,272,10285,1,'Normal',NULL,1)
,(2384,272,10286,1,'Low',NULL,1)
,(2385,272,10289,1,'Normal',NULL,1)
,(2386,272,10290,1,'High',NULL,1)
,(2387,272,10292,1,'Normal',NULL,1)
,(2388,273,10285,1,'Normal',NULL,1)
,(2389,273,10286,1,'Low',NULL,1)
,(2390,273,10289,1,'Normal',NULL,1)
,(2391,273,10290,1,'High',NULL,1)
,(2392,273,10292,1,'High',NULL,1)
,(2395,274,2524,1,'High',NULL,1)
,(2396,274,2523,1,'Normal',NULL,1)
,(2397,274,2526,1,'Normal',NULL,1)
,(2398,274,2527,1,'Normal or High',NULL,1)
,(2399,274,2528,1,'Normal or High',NULL,1)
,(2402,275,2524,1,'High',NULL,1)
,(2403,275,2523,1,'Normal',NULL,1)
,(2404,275,2526,1,'Low',NULL,1)
,(2405,275,2527,1,'Normal or High',NULL,1)
,(2406,275,2528,1,'Normal',NULL,1)
,(2409,276,2524,1,'High',NULL,1)
,(2410,276,2523,1,'Normal',NULL,1)
,(2411,276,2526,1,'Normal',NULL,1)
,(2412,276,2527,1,'Low',NULL,1)
,(2413,276,2528,1,'Low',NULL,1)
,(2416,277,2524,1,'High',NULL,1)
,(2417,277,2523,1,'Normal',NULL,1)
,(2418,277,2526,1,'Normal',NULL,1)
,(2419,277,2527,1,'Normal',NULL,1)
,(2420,277,2528,1,'Normal',NULL,1)
,(2422,278,10296,1,'Low',NULL,1)
,(2423,278,10295,1,'Normal',NULL,1)
,(2424,278,10302,1,'Normal',NULL,1)
,(2425,278,10315,1,'Low',NULL,1)
,(2427,279,10296,1,'Low',NULL,1)
,(2428,279,10295,1,'Low',NULL,1)
,(2429,279,10302,1,'Normal',NULL,1)
,(2430,279,10315,1,'Low',NULL,1)
,(2431,279,10239,1,'Low',NULL,1)
,(2433,280,10296,1,'High',NULL,1)
,(2434,280,10295,1,'Normal',NULL,1)
,(2435,280,10302,1,'Normal',NULL,1)
,(2436,280,10239,1,'Normal',NULL,1)
,(2437,280,10245,1,'High',NULL,1)
,(2438,281,150,NULL,'Normal',NULL,1)
,(2439,281,151,NULL,'Normal',NULL,1)
,(2440,281,106,NULL,'Normal',NULL,1)
,(2441,281,94,NULL,'Low',NULL,1)
,(2442,281,107,NULL,'Normal',NULL,1)
,(2443,281,102,NULL,'Normal or Low',NULL,1)
,(2446,282,150,NULL,'Normal',NULL,1)
,(2447,282,151,NULL,'Normal',NULL,1)
,(2448,282,106,NULL,'Normal',NULL,1)
,(2449,282,94,NULL,'Decreasing',NULL,1)
,(2450,282,107,NULL,'Normal',NULL,1)
,(2451,282,102,NULL,'Normal or Low',NULL,1)
,(2454,283,150,NULL,'Normal',NULL,1)
,(2455,283,151,NULL,'Normal',NULL,1)
,(2456,283,106,NULL,'Normal',NULL,1)
,(2457,283,94,NULL,'Decreasing',NULL,1)
,(2458,283,107,NULL,'Normal',NULL,1)
,(2459,283,102,NULL,'Normal or Low',NULL,1)
,(2462,284,150,NULL,'Normal',NULL,1)
,(2463,284,151,NULL,'Normal',NULL,1)
,(2464,284,106,NULL,'Normal',NULL,1)
,(2465,284,94,NULL,'Low',NULL,1)
,(2466,284,107,NULL,'Normal',NULL,1)
,(2467,284,102,NULL,'Complicated',NULL,1)
,(2470,285,150,NULL,'Normal',NULL,1)
,(2471,285,151,NULL,'Normal',NULL,1)
,(2472,285,106,NULL,'Normal',NULL,1)
,(2473,285,94,NULL,'Decreasing',NULL,1)
,(2474,285,102,NULL,'Normal or Decreasing',NULL,1)
,(2477,285,107,NULL,'Normal',NULL,1)
,(2478,286,150,NULL,'Normal',NULL,1)
,(2479,286,151,NULL,'Normal',NULL,1)
,(2480,286,106,NULL,'Normal',NULL,1)
,(2481,286,94,NULL,'Decreasing',NULL,1)
,(2482,286,107,NULL,'Normal',NULL,1)
,(2483,286,102,NULL,'Normal or Decreasing',NULL,1)
,(2486,287,150,NULL,'Normal',NULL,1)
,(2487,287,151,NULL,'Normal',NULL,1)
,(2488,287,106,NULL,'Normal',NULL,1)
,(2489,287,94,NULL,'Low',NULL,1)
,(2490,287,107,NULL,'Normal',NULL,1)
,(2491,287,102,NULL,'Normal or Decreasing',NULL,1)
,(2494,288,150,NULL,'Normal',NULL,1)
,(2495,288,151,NULL,'Normal',NULL,1)
,(2496,288,106,NULL,'Normal',NULL,1)
,(2497,288,94,NULL,'Decreasing',NULL,1)
,(2498,288,107,NULL,'Normal',NULL,1)
,(2499,288,102,NULL,'Normal or Increasing',NULL,1)
,(2502,289,150,NULL,'Normal',NULL,1)
,(2503,289,151,NULL,'Normal',NULL,1)
,(2504,289,106,NULL,'Normal',NULL,1)
,(2505,289,94,NULL,'Suddenly Decreasing',NULL,1)
,(2506,289,107,NULL,'Normal',NULL,1)
,(2507,289,102,NULL,'Normal or Increasing',NULL,1)
,(2510,290,150,NULL,'Normal',NULL,1)
,(2511,290,151,NULL,'Normal',NULL,1)
,(2512,290,106,NULL,'Normal',NULL,1)
,(2513,290,94,NULL,'Low',NULL,1)
,(2514,290,107,NULL,'Normal',NULL,1)
,(2515,290,102,NULL,'Complicated',NULL,1)
,(2518,281,10204,NULL,'Related',NULL,1)
,(2519,281,2694,NULL,'Related',NULL,1)
,(2520,282,10204,NULL,'Related',NULL,1)
,(2521,282,2694,NULL,'Related',NULL,1)
,(2522,283,10204,NULL,'Related',NULL,1)
,(2523,283,2694,NULL,'Related',NULL,1)
,(2524,284,10204,NULL,'Related',NULL,1)
,(2525,284,2694,NULL,'Related',NULL,1)
,(2526,285,10204,NULL,'Related',NULL,1)
,(2527,285,2694,NULL,'Related',NULL,1)
,(2528,286,10204,NULL,'Related',NULL,1)
,(2529,286,2694,NULL,'Related',NULL,1)
,(2530,287,10204,NULL,'Related',NULL,1)
,(2531,287,2694,NULL,'Related',NULL,1)
,(2532,288,10204,NULL,'Related',NULL,1)
,(2533,288,2694,NULL,'Related',NULL,1)
,(2534,289,10204,NULL,'Related',NULL,1)
,(2535,289,2694,NULL,'Related',NULL,1)
,(2536,290,10204,NULL,'Related',NULL,1)
,(2537,290,2694,NULL,'Related',NULL,1)
,(2538,281,3237,NULL,'Normal or Low',NULL,1)
,(2539,282,3237,NULL,'Normal or Low',NULL,1)
,(2540,283,3237,NULL,'Normal',NULL,1)
,(2541,284,3237,NULL,'Normal',NULL,1)
,(2543,285,3237,NULL,'Normal',NULL,1)
,(2544,286,3237,NULL,'Normal',NULL,1)
,(2545,287,3237,NULL,'Normal',NULL,1)
,(2546,288,3237,NULL,'Normal',NULL,1)
,(2547,289,3237,NULL,'Normal',NULL,1)
,(2548,290,3237,NULL,'Normal',NULL,1)
,(2549,281,2693,NULL,'Related',NULL,1)
,(2550,281,10203,NULL,'Related',NULL,1)
,(2551,282,2693,NULL,'Related',NULL,1)
,(2552,282,10203,NULL,'Related',NULL,1)
,(2553,283,2693,NULL,'Related',NULL,1)
,(2554,283,10203,1,'Related',NULL,1)
,(2555,284,2693,NULL,'Related',NULL,1)
,(2556,284,10203,NULL,'Related',NULL,1)
,(2557,285,2693,NULL,'Related',NULL,1)
,(2558,285,10203,NULL,'Related',NULL,1)
,(2559,286,2693,NULL,'Related',NULL,1)
,(2560,286,10203,NULL,'Related',NULL,1)
,(2561,287,10203,NULL,'Related',NULL,1)
,(2562,287,2693,NULL,'Related',NULL,1)
,(2563,288,10203,NULL,'Related',NULL,1)
,(2564,288,2693,NULL,'Related',NULL,1)
,(2565,289,10203,NULL,'Related',NULL,1)
,(2566,289,2693,NULL,'Related',NULL,1)
,(2567,290,10203,NULL,'Related',NULL,1)
,(2568,290,2693,NULL,'Related',NULL,1)
,(2569,281,10509,NULL,NULL,NULL,1)
,(2570,281,10510,NULL,NULL,NULL,1)
,(2571,281,10508,NULL,NULL,NULL,1)
,(2572,281,10511,NULL,NULL,NULL,1)
,(2573,281,2778,NULL,'Normal',NULL,1)
,(2574,281,10505,NULL,NULL,NULL,1)
,(2575,282,10509,NULL,NULL,NULL,1)
,(2576,282,10510,NULL,NULL,NULL,1)
,(2577,282,10508,NULL,NULL,NULL,1)
,(2578,282,10511,NULL,NULL,NULL,1)
,(2579,282,2778,NULL,'Normal',NULL,1)
,(2580,282,10505,NULL,NULL,NULL,1)
,(2581,283,10509,NULL,NULL,NULL,1)
,(2582,283,10510,NULL,NULL,NULL,1)
,(2583,283,10508,NULL,NULL,NULL,1)
,(2584,283,10511,NULL,NULL,NULL,1)
,(2585,283,2778,NULL,'Normal',NULL,1)
,(2586,283,10505,NULL,NULL,NULL,1)
,(2587,284,10509,NULL,NULL,NULL,1)
,(2588,284,10510,NULL,NULL,NULL,1)
,(2589,284,10508,NULL,NULL,NULL,1)
,(2590,284,10511,NULL,NULL,NULL,1)
,(2591,284,2778,NULL,'Normal or High',NULL,1)
,(2592,284,10505,NULL,NULL,NULL,1)
,(2593,285,10509,NULL,NULL,NULL,1)
,(2594,285,10510,NULL,NULL,NULL,1)
,(2595,285,10508,NULL,NULL,NULL,1)
,(2596,285,10511,NULL,NULL,NULL,1)
,(2597,285,2778,NULL,'Normal or High',NULL,1)
,(2598,285,10505,NULL,NULL,NULL,1)
,(2599,286,10509,NULL,NULL,NULL,1)
,(2600,286,10510,NULL,NULL,NULL,1)
,(2601,286,10508,NULL,NULL,NULL,1)
,(2602,286,10511,NULL,NULL,NULL,1)
,(2603,286,2778,NULL,'Normal or High',NULL,1)
,(2604,286,10505,NULL,NULL,NULL,1)
,(2605,287,10509,NULL,NULL,NULL,1)
,(2606,287,10510,NULL,NULL,NULL,1)
,(2607,287,10508,NULL,NULL,NULL,1)
,(2608,287,10511,NULL,NULL,NULL,1)
,(2609,287,2778,NULL,'Normal or High',NULL,1)
,(2610,287,10505,NULL,NULL,NULL,1)
,(2611,288,10509,NULL,NULL,NULL,1)
,(2612,288,10510,NULL,NULL,NULL,1)
,(2613,288,10508,NULL,NULL,NULL,1)
,(2614,288,10511,NULL,NULL,NULL,1)
,(2615,288,2778,NULL,'Normal or High',NULL,1)
,(2616,288,10505,NULL,NULL,NULL,1)
,(2617,289,10509,NULL,NULL,NULL,1)
,(2618,289,10510,NULL,NULL,NULL,1)
,(2619,289,10508,NULL,NULL,NULL,1)
,(2620,289,10511,NULL,NULL,NULL,1)
,(2621,289,2778,NULL,'Normal or High',NULL,1)
,(2622,289,10505,NULL,NULL,NULL,1)
,(2623,290,10509,NULL,NULL,NULL,1)
,(2624,290,10510,NULL,NULL,NULL,1)
,(2625,290,10508,NULL,NULL,NULL,1)
,(2626,290,10511,NULL,NULL,NULL,1)
,(2627,290,2778,NULL,'High',NULL,1)
,(2628,290,10505,NULL,NULL,NULL,1)
,(2629,281,10474,NULL,'Related',NULL,1)
,(2630,282,10474,NULL,'Related',NULL,1)
,(2631,283,10474,NULL,'Related',NULL,1)
,(2632,284,10474,NULL,'Related',NULL,1)
,(2633,285,10474,NULL,'Related',NULL,1)
,(2634,286,10474,NULL,'Related',NULL,1)
,(2635,287,10474,NULL,'Related',NULL,1)
,(2636,288,10474,NULL,'Related',NULL,1)
,(2637,289,10474,NULL,'Related',NULL,1)
,(2638,290,10474,NULL,'Related',NULL,1)
,(2639,281,2698,NULL,'Related',NULL,1)
,(2640,282,2698,NULL,'Related',NULL,1)
,(2641,283,2698,NULL,'Related',NULL,1)
,(2642,284,2698,NULL,'Related',NULL,1)
,(2643,285,2698,NULL,'Related',NULL,1)
,(2644,286,2698,NULL,'Related',NULL,1)
,(2645,287,2698,NULL,'Related',NULL,1)
,(2646,288,2698,NULL,'Related',NULL,1)
,(2647,289,2698,NULL,'Related',NULL,1)
,(2648,290,2698,NULL,'Related',NULL,1)
,(2649,281,2774,NULL,'Related',NULL,1)
,(2650,282,2774,NULL,'High',NULL,1)
,(2651,283,2774,NULL,'Normal',NULL,1)
,(2652,284,2774,NULL,'Normal',NULL,1)
,(2653,285,2774,NULL,'Normal',NULL,1)
,(2654,286,2774,NULL,'Normal',NULL,1)
,(2655,287,2774,NULL,'Normal',NULL,1)
,(2656,288,2774,NULL,'Normal',NULL,1)
,(2657,289,2774,NULL,'Normal',NULL,1)
,(2658,290,2774,NULL,'Normal',NULL,1)
,(2659,291,150,NULL,'Normal',NULL,1)
,(2660,291,151,NULL,'Normal',NULL,1)
,(2661,291,4002,NULL,'High on One or All Fans',NULL,1)
,(2662,291,10439,NULL,'Normal or Low',NULL,1)
,(2663,291,2763,NULL,'Normal',NULL,1)
,(2664,291,2568,NULL,'Normal',NULL,1)
,(2666,291,1167,NULL,'Normal',NULL,1)
,(2668,291,2077,NULL,'Normal',NULL,1)
,(2669,291,19,NULL,'Normal',NULL,1)
,(2670,291,2273,NULL,'Related',NULL,1)
,(2671,292,150,NULL,'Normal',NULL,1)
,(2672,292,151,NULL,'Normal',NULL,1)
,(2673,292,4002,NULL,'High on One or All Fans',NULL,1)
,(2674,292,10439,NULL,'Normal or High',NULL,1)
,(2675,292,2763,NULL,'Normal',NULL,1)
,(2676,292,2568,NULL,'Normal',NULL,1)
,(2678,292,1167,NULL,'Normal',NULL,1)
,(2679,292,2077,NULL,'Normal',NULL,1)
,(2680,292,19,NULL,'Normal',NULL,1)
,(2681,292,2273,NULL,'Related',NULL,1)
,(2682,293,150,NULL,'Normal',NULL,1)
,(2683,293,151,NULL,'Normal',NULL,1)
,(2684,293,4002,NULL,'High on One Fan',NULL,1)
,(2685,293,10439,NULL,'Normal or High',NULL,1)
,(2686,293,2763,NULL,'High',NULL,1)
,(2687,293,2568,NULL,'Normal',NULL,1)
,(2688,293,1169,NULL,'Normal',NULL,1)
,(2689,293,1167,NULL,'Normal',NULL,1)
,(2690,293,2077,NULL,'Normal',NULL,1)
,(2691,293,19,NULL,'Normal',NULL,1)
,(2692,293,2273,NULL,'Related',NULL,1)
,(2693,294,150,NULL,'Normal',NULL,1)
,(2694,294,151,NULL,'Normal',NULL,1)
,(2695,294,10509,NULL,NULL,NULL,1)
,(2696,294,10510,NULL,NULL,NULL,1)
,(2697,294,10508,NULL,NULL,NULL,1)
,(2698,294,10511,NULL,NULL,NULL,1)
,(2699,294,2778,NULL,'Increasing',NULL,1)
,(2700,294,10505,NULL,'Increasing',NULL,1)
,(2701,294,2779,NULL,'Increasing',NULL,1)
,(2702,294,2777,NULL,'Increasing',NULL,1)
,(2703,294,2770,NULL,'Related',NULL,1)
,(2704,294,2786,NULL,'Normal',NULL,1)
,(2705,294,10439,NULL,'Normal',NULL,1)
,(2706,294,10472,NULL,'Normal',NULL,1)
,(2707,294,10473,NULL,'Normal',6052,1)
,(2708,294,10468,NULL,'Normal',6052,1)
,(2709,294,10469,NULL,'Normal',NULL,1)
,(2710,294,8058,NULL,'Normal',NULL,1)
,(2711,294,8030,NULL,'Normal',NULL,1)
,(2712,294,2764,NULL,'Normal',NULL,1)
,(2713,294,10425,NULL,'Normal',NULL,1)
,(2714,294,2768,NULL,'Normal',NULL,1)
,(2715,294,2776,NULL,'Normal',NULL,1)
,(2716,295,150,NULL,'Normal',NULL,1)
,(2717,295,151,NULL,'Normal',NULL,1)
,(2718,295,10509,NULL,NULL,NULL,1)
,(2719,295,10510,NULL,NULL,NULL,1)
,(2720,295,10508,NULL,NULL,NULL,1)
,(2721,295,10511,NULL,NULL,NULL,1)
,(2722,295,2778,NULL,'Increasing',NULL,1)
,(2723,295,10505,NULL,'Increasing',NULL,1)
,(2724,295,2779,NULL,'Increasing',NULL,1)
,(2725,295,2777,NULL,'Increasing',NULL,1)
,(2726,295,2770,NULL,'Related',NULL,1)
,(2727,295,2786,NULL,'Normal',NULL,1)
,(2728,295,10439,NULL,'Normal',NULL,1)
,(2729,295,10472,NULL,'Normal',NULL,1)
,(2730,295,10473,NULL,'Normal',6052,1)
,(2731,295,10469,NULL,'Normal',NULL,1)
,(2732,295,10468,NULL,'Normal',6052,1)
,(2733,295,2764,NULL,'Normal',NULL,1)
,(2734,295,10425,NULL,'Normal',NULL,1)
,(2735,295,8058,NULL,'Normal',NULL,1)
,(2736,295,8030,NULL,'Normal',NULL,1)
,(2737,295,2768,NULL,'Normal',NULL,1)
,(2738,295,2776,NULL,'Normal',NULL,1)
,(2739,296,150,NULL,'Normal',NULL,1)
,(2740,296,151,NULL,'Normal',NULL,1)
,(2741,296,10509,NULL,NULL,NULL,1)
,(2742,296,10510,NULL,NULL,NULL,1)
,(2743,296,10508,NULL,NULL,NULL,1)
,(2745,296,10511,NULL,NULL,NULL,1)
,(2746,296,2778,NULL,'Increasing',NULL,1)
,(2747,296,10505,NULL,'Increasing',NULL,1)
,(2748,296,2779,NULL,'Increasing',NULL,1)
,(2749,296,2777,NULL,'Increasing',NULL,1)
,(2750,296,2770,NULL,'Related',NULL,1)
,(2751,296,2786,NULL,'Normal',NULL,1)
,(2752,296,10439,NULL,'Normal',NULL,1)
,(2753,296,10473,NULL,'Normal',6052,1)
,(2754,296,10472,NULL,'Normal',NULL,1)
,(2755,296,8058,NULL,'Normal',NULL,1)
,(2756,296,10468,NULL,'Normal',6052,1)
,(2757,296,10469,NULL,'Normal',NULL,1)
,(2758,296,8030,NULL,'Normal',NULL,1)
,(2759,296,10425,NULL,'Normal',NULL,1)
,(2760,296,2764,NULL,'Normal',NULL,1)
,(2761,296,2768,NULL,'Normal',NULL,1)
,(2762,296,2776,NULL,'Normal',NULL,1)
,(2763,297,150,NULL,'Normal',NULL,1)
,(2764,297,151,NULL,'Normal',NULL,1)
,(2765,297,10509,NULL,NULL,NULL,1)
,(2766,297,10510,NULL,NULL,NULL,1)
,(2767,297,10508,NULL,NULL,NULL,1)
,(2768,297,10511,NULL,NULL,NULL,1)
,(2769,297,2778,NULL,'Increasing',NULL,1)
,(2770,297,10505,NULL,'Increasing',NULL,1)
,(2771,297,2779,NULL,'Increasing',NULL,1)
,(2772,297,2777,NULL,'Increasing',NULL,1)
,(2773,297,2770,NULL,'Related',NULL,1)
,(2774,297,2786,NULL,'Related',NULL,1)
,(2775,297,10439,NULL,'Related',NULL,1)
,(2776,297,10473,NULL,'Normal or Increasing',6052,1)
,(2777,297,10472,NULL,'Normal or Low',NULL,1)
,(2778,297,8058,NULL,'Related',NULL,1)
,(2779,297,8030,NULL,'Related',NULL,1)
,(2780,297,10469,NULL,'Normal',NULL,1)
,(2782,297,10468,NULL,'Normal',6052,1)
,(2783,297,10425,NULL,'Normal',NULL,1)
,(2784,297,2768,NULL,'Normal or Low',NULL,1)
,(2785,297,2776,NULL,'Normal or Low',NULL,1)
,(2786,283,10203,20,NULL,NULL,1)
,(2787,304,10259,NULL,'Decreasing',NULL,1)
,(2789,305,10259,NULL,'High or Low',NULL,1)
,(2790,306,10259,NULL,'Decreasing',NULL,1)
,(2792,307,10259,NULL,'Decreasing',NULL,1)
,(2794,310,2334,NULL,'Normal or Increasing',NULL,1)
,(2795,313,2334,NULL,'Normal or Increasing',NULL,1)
,(2796,314,2334,NULL,'Normal or Increasing',NULL,1)
,(2798,311,10237,NULL,'Normal or Decreasing',NULL,1)
,(2799,304,10237,NULL,'Normal or Decreasing',NULL,1)
,(2800,306,10237,NULL,'Normal or Low',NULL,1)
,(2801,307,10237,NULL,'Normal or Low',NULL,1)
,(2802,281,10474,20,'Related',NULL,1)
,(2803,282,10474,20,'Related',NULL,1)
,(2804,283,10474,20,'Related',NULL,1)
,(2805,284,10474,20,'Related',NULL,1)
,(2806,285,10474,20,'Related',NULL,1)
,(2807,286,10474,20,'Related',NULL,1)
,(2809,287,10474,20,'Related',NULL,1)
,(2810,288,10474,20,'Related',NULL,1)
,(2811,289,10474,20,'Related',NULL,1)
,(2812,290,10474,20,'Related',NULL,1)
,(2813,315,10443,NULL,'Increasing',NULL,1)
,(2814,316,10443,NULL,'Increasing',NULL,1)
,(2815,317,10443,NULL,'Increasing',NULL,1)
,(2816,281,10521,20,'High',NULL,1)
,(2817,281,10437,20,'High',NULL,1)
,(2818,282,10437,20,'Increasing',NULL,1)
,(2819,282,10521,20,'Increasing',NULL,1)
,(2820,283,10521,20,'Decreasing',NULL,1)
,(2821,284,10521,20,'Decreasing',NULL,1)
,(2822,285,10521,20,'Suddenly Increasing',NULL,1)
,(2823,302,4002,NULL,'Increasing',NULL,1)
,(2824,303,4002,NULL,'Decreasing',NULL,1)
,(2825,302,5626,NULL,'Normal or Increasing',NULL,1)
,(2826,303,5626,NULL,'Normal or Increasing',NULL,1)
,(2827,302,2354,NULL,'Normal or Increasing',NULL,1)
,(2828,303,2354,NULL,'Normal or Increasing',NULL,1)
,(2829,302,7020,NULL,'Normal or Increasing',NULL,1)
,(2830,303,7020,NULL,'Normal or Increasing',NULL,1)
,(2831,302,2355,NULL,'Normal or Decreasing',NULL,1)
,(2832,302,1315,NULL,'Normal or Decreasing',NULL,1)
,(2833,303,1315,NULL,'Increasing',NULL,1)
,(2834,322,1315,NULL,'High',NULL,1)
,(2835,323,1315,NULL,'High',NULL,1)
,(2836,322,4002,NULL,'Decreasing',NULL,1)
,(2837,323,4002,NULL,'Increasing',NULL,1)
,(2838,322,5626,NULL,'Normal or Increasing',NULL,1)
,(2839,322,2354,NULL,'Normal or Increasing',NULL,1)
,(2840,323,5626,NULL,'Normal or Increasing',NULL,1)
,(2841,323,2354,NULL,'Normal or Increasing',NULL,1)
,(2842,322,7020,NULL,'Normal or Increasing',NULL,1)
,(2843,323,7020,NULL,'Normal or Increasing',NULL,1)
,(2844,322,2355,NULL,'Related',NULL,1)
,(2845,323,2355,NULL,'Normal or Decreasing',NULL,1)
,(2846,324,2355,NULL,'Low',NULL,1)
,(2847,325,2355,NULL,'Low',NULL,1)
,(2848,326,2355,NULL,'Low',NULL,1)
,(2849,327,2355,NULL,'Low',NULL,1)
,(2850,325,1315,NULL,'High',NULL,1)
,(2851,326,1315,NULL,'High',NULL,1)
,(2852,326,4002,NULL,'High',NULL,1)
,(2853,326,5626,NULL,'High',NULL,1)
,(2854,326,2354,NULL,'High',NULL,1)
,(2855,326,7020,NULL,'Normal or High',NULL,1)
,(2856,328,2355,NULL,'High',NULL,1)
,(2857,329,2355,NULL,'High',NULL,1)
,(2858,328,4002,NULL,'Normal or High',NULL,1)
,(2859,328,2779,NULL,'Normal or High',NULL,1)
,(2860,328,2777,NULL,'Normal or High',NULL,1)
,(2861,330,4002,NULL,'Normal or Low',NULL,1)
,(2862,330,7020,NULL,'Normal or Low',NULL,1)
,(2863,331,2776,NULL,'Low',NULL,1)
,(2864,332,2776,NULL,'Low',NULL,1)
,(2865,333,2776,NULL,'Low',NULL,1)
,(2866,331,2768,NULL,'Low',NULL,1)
,(2867,332,2768,NULL,'Low',NULL,1)
,(2868,333,2768,NULL,'Low',NULL,1)
,(2869,331,10472,NULL,'Low',NULL,1)
,(2870,332,10472,NULL,'Low',NULL,1)
,(2871,333,10472,NULL,'Low',NULL,1)
,(2872,333,10473,NULL,'Slight Increasing to Normal',6052,1)
,(2873,334,2776,NULL,'Low',NULL,1)
,(2874,335,2776,NULL,'Low',NULL,1)
,(2875,334,2768,NULL,'Low',NULL,1)
,(2876,335,2768,NULL,'Low',NULL,1)
,(2877,334,10472,NULL,'Low',NULL,1)
,(2878,335,10472,NULL,'Low',NULL,1)
,(2879,335,10473,NULL,'Slight Increasing to Normal',6052,1)
,(2880,336,2776,NULL,'Low',NULL,1)
,(2881,337,2776,NULL,'Low',NULL,1)
,(2882,336,2768,NULL,'Low',NULL,1)
,(2883,337,2768,NULL,'Low',NULL,1)
,(2884,336,10472,NULL,'Low',NULL,1)
,(2885,337,10472,NULL,'Low',NULL,1)
,(2886,336,10473,NULL,'Slight Increasing to Normal',6052,1)
,(2887,338,2786,NULL,'Low',NULL,1)
,(2888,338,10439,NULL,'Low',NULL,1)
,(2889,338,8030,NULL,'Normal or High',NULL,1)
,(2890,338,8058,NULL,'Normal or High',NULL,1)
,(2891,338,10469,NULL,'Normal or Low',NULL,1)
,(2892,338,10472,NULL,'High',NULL,1)
,(2894,338,10473,NULL,'Normal or High',6052,1)
,(2896,340,10204,NULL,'Low',NULL,1)
,(2897,341,10204,NULL,'Low',NULL,1)
,(2898,342,10204,NULL,'Low',NULL,1)
,(2899,339,2768,NULL,'Slight Increasing to Normal',NULL,1)
,(2900,340,2768,NULL,'Low',NULL,1)
,(2901,341,2768,NULL,'Low',NULL,1)
,(2902,342,2768,NULL,'Low',NULL,1)
,(2903,341,2694,NULL,'Slight Increasing to Normal',NULL,1)
,(2904,339,2698,NULL,'Low',NULL,1)
,(2905,343,4002,NULL,'High',NULL,1)
,(2906,344,4002,NULL,'High',NULL,1)
,(2907,345,4002,NULL,'High',NULL,1)
,(2908,346,4002,NULL,'High',NULL,1)
,(2909,343,10439,NULL,'Normal or Low',NULL,1)
,(2910,344,10439,NULL,'Normal or High',NULL,1)
,(2911,345,10439,NULL,'Normal or High',NULL,1)
,(2912,346,10439,NULL,'Normal or High',NULL,1)
,(2913,346,2778,NULL,'High',NULL,1)
,(2914,345,10441,NULL,'Normal or Stratified',6131,1)
,(2915,345,1167,NULL,'Normal or Stratified',NULL,1)
,(2916,345,19,NULL,'Normal or High',NULL,1)
,(2917,343,2768,NULL,'Normal or Low',NULL,1)
,(2918,344,2768,NULL,'Normal or Low',NULL,1)
,(2919,346,2768,NULL,'Normal or Low',NULL,1)
,(2920,345,2273,NULL,'Normal or High',NULL,1)
,(2921,347,4002,NULL,'Increasing',NULL,1)
,(2922,348,4002,NULL,'Increasing',NULL,1)
,(2923,349,4002,NULL,'Increasing',NULL,1)
,(2924,350,4002,NULL,'Increasing',NULL,1)
,(2925,347,10439,NULL,'Normal or Low',NULL,1)
,(2926,348,10439,NULL,'Normal or High',NULL,1)
,(2927,349,10439,NULL,'Normal or High',NULL,1)
,(2928,350,10439,NULL,'Normal or High',NULL,1)
,(2929,350,2778,NULL,'High',NULL,1)
,(2930,349,10441,NULL,'Normal or Stratified',6131,1)
,(2931,349,1167,NULL,'Normal or Stratified',NULL,1)
,(2932,349,19,NULL,'Normal or Stratified',NULL,1)
,(2933,350,2768,NULL,'Normal or Low',NULL,1)
,(2934,347,2273,NULL,'Normal or High',NULL,1)
,(2935,348,2273,NULL,'Normal or High',NULL,1)
,(2936,349,2273,NULL,'Normal or High',NULL,1)
,(2937,351,4002,NULL,'Increasing',NULL,1)
,(2938,352,4002,NULL,'Increasing',NULL,1)
,(2939,353,4002,NULL,'Increasing',NULL,1)
,(2940,354,4002,NULL,'Increasing',NULL,1)
,(2941,355,4002,NULL,'Increasing',NULL,1)
,(2942,351,10439,NULL,'Normal or Low',NULL,1)
,(2943,352,10439,NULL,'Normal or High',NULL,1)
,(2944,353,10439,NULL,'Normal or High',NULL,1)
,(2945,354,10439,NULL,'Normal or High',NULL,1)
,(2946,355,10439,NULL,'Normal or High',NULL,1)
,(2947,352,22,NULL,'High',NULL,1)
,(2949,353,19,NULL,'Normal or High',NULL,1)
,(2950,353,2568,NULL,'Normal or High',NULL,1)
,(2951,354,2778,NULL,'High',NULL,1)
,(2952,355,2778,NULL,'High',NULL,1)
,(2953,353,10441,NULL,'Normal or Stratified',6131,1)
,(2954,353,1167,NULL,'Normal or Stratified',NULL,1)
,(2955,354,2768,NULL,'Normal or Low',NULL,1)
,(2956,355,2768,NULL,'Normal or Decreasing',NULL,1)
,(2957,351,2273,NULL,'Normal or High',NULL,1)
,(2958,352,2273,NULL,'Normal or High',NULL,1)
,(2959,353,2273,NULL,'Normal or High',NULL,1)
,(2960,355,2273,NULL,'Normal or Decreasing',NULL,1)
,(2961,356,10517,NULL,'Increasing',NULL,1)
,(2962,357,10517,NULL,'Increasing',NULL,1)
,(2963,358,10517,NULL,'Increasing',NULL,1)
,(2964,356,10505,NULL,'Normal or Increasing',NULL,1)
,(2965,357,2778,NULL,'Normal or Increasing',NULL,1)
,(2966,358,10517,NULL,'Normal or Increasing',NULL,1)
,(2967,356,2777,NULL,'Normal or Increasing',NULL,1)
,(2968,357,2777,NULL,'Normal or Increasing',NULL,1)
,(2969,358,2777,NULL,'Normal or Increasing',NULL,1)
,(2970,356,2768,NULL,'Normal or Low',NULL,1)
,(2971,357,2768,NULL,'Normal or Low',NULL,1)
,(2972,358,2768,NULL,'Normal or Low',NULL,1)
,(2973,359,2787,NULL,'Increasing',NULL,1)
,(2974,360,2787,NULL,'Increasing',NULL,1)
,(2975,360,4002,NULL,'Normal or High',NULL,1)
,(2976,360,2768,NULL,'Normal or Low',NULL,1)
,(2977,360,2273,NULL,'Normal or Low',NULL,1)
,(2978,359,2768,NULL,'Normal or Low',NULL,1)
,(2979,361,2273,NULL,'Increasing',NULL,1)
,(2980,362,2273,NULL,'Increasing',NULL,1)
,(2981,363,2273,NULL,'Increasing',NULL,1)
,(2982,364,2273,NULL,'Increasing',NULL,1)
,(2983,363,2768,NULL,'Decreasing',NULL,1)
,(2984,364,2768,NULL,'Decreasing',NULL,1)
,(2985,361,2786,NULL,'High',NULL,1)
,(2986,362,2786,NULL,'High',NULL,1)
,(2987,363,2786,NULL,'High',NULL,1)
,(2988,364,2786,NULL,'High',NULL,1)
,(2989,361,10439,NULL,'Normal or Less',NULL,1)
,(2990,362,10439,NULL,'Normal or More',NULL,1)
,(2991,363,10439,NULL,'Normal or More',NULL,1)
,(2992,364,10439,NULL,'Normal or More',NULL,1)
,(2993,362,2778,NULL,'High',NULL,1)
,(2994,364,10441,NULL,'Normal or Stratified',6131,1)
,(2995,364,1167,NULL,'Normal or Stratified',NULL,1)
,(2996,364,19,NULL,'Normal or High',NULL,1)
,(2997,365,2768,NULL,'Decreasing',NULL,1)
,(2998,366,2768,NULL,'Decreasing',NULL,1)
,(2999,367,2768,NULL,'Decreasing',NULL,1)
,(3000,368,2768,NULL,'Decreasing',NULL,1)
,(3001,367,2273,NULL,'Decreasing',NULL,1)
,(3002,368,2273,NULL,'Decreasing',NULL,1)
,(3003,365,2786,NULL,'High',NULL,1)
,(3004,366,2786,NULL,'High',NULL,1)
,(3005,367,2786,NULL,'High',NULL,1)
,(3006,368,2786,NULL,'High',NULL,1)
,(3007,365,10439,NULL,'Normal or Less',NULL,1)
,(3008,366,10439,NULL,'Normal or More',NULL,1)
,(3009,367,10439,NULL,'Normal or More',NULL,1)
,(3010,368,10439,NULL,'Normal or More',NULL,1)
,(3011,366,2778,NULL,'High',NULL,1)
,(3012,368,10441,NULL,'Normal or Stratified',6131,1)
,(3013,368,1167,NULL,'Normal or Stratified',NULL,1)
,(3014,368,19,NULL,'Normal or High',NULL,1)
,(3015,369,2786,NULL,'Normal or Low',NULL,1)
,(3016,369,2273,NULL,'Normal or Low',NULL,1)
,(3017,369,10439,NULL,'Normal or Low',NULL,1)
,(3018,375,2568,NULL,'Normal or High',NULL,1)
,(3019,375,19,NULL,'Normal or High',NULL,1)
,(3020,375,4002,NULL,'Normal or High',NULL,1)
,(3021,373,4002,NULL,'High',NULL,1)
,(3022,374,4002,NULL,'High',NULL,1)
,(3023,373,10439,NULL,'Normal or High',NULL,1)
,(3024,374,10439,NULL,'Normal or High',NULL,1)
,(3025,375,10439,NULL,'Normal or High',NULL,1)
,(3026,373,10441,NULL,'Normal or Stratified',6131,1)
,(3027,374,10441,NULL,'Normal or Stratified',6131,1)
,(3028,373,1167,NULL,'Normal or Stratified',NULL,1)
,(3029,374,1167,NULL,'Normal, High, or Stratified',NULL,1)
,(3030,375,1167,NULL,'Normal, Low, or Stratified',NULL,1)
,(3031,373,30,NULL,'Normal or Stratified',NULL,1)
,(3032,374,10445,NULL,'Normal or High',NULL,1)
,(3033,376,20,NULL,'High',NULL,1)
,(3034,376,29,NULL,'Lower than Actual',NULL,1)
,(3035,377,29,NULL,'Normal or Lower than Actual',NULL,1)
,(3036,377,20,NULL,'High',NULL,1)
,(3037,376,34,NULL,'High',NULL,1)
,(3038,377,34,NULL,'High',NULL,1)
,(3039,376,1167,NULL,'Normal or High',NULL,1)
,(3040,377,1167,NULL,'Normal or High',NULL,1)
,(3041,377,2081,NULL,'Normal or High',NULL,1)
,(3042,376,2081,NULL,'Normal or High',NULL,1)
,(3043,378,29,NULL,'Normal or Higher than Actual',NULL,1)
,(3044,378,20,NULL,'Low',NULL,1)
,(3045,378,34,NULL,'Normal or Low',NULL,1)
,(3046,378,2081,NULL,'Normal or Low',NULL,1)
,(3047,378,1167,NULL,'Normal or Low',NULL,1)
,(3048,379,2568,NULL,'High or Low',NULL,1)
,(3049,380,2568,NULL,'High',NULL,1)
,(3050,381,2568,NULL,'High',NULL,1)
,(3051,379,19,NULL,'High or Low',NULL,1)
,(3052,380,19,NULL,'High',NULL,1)
,(3053,381,19,NULL,'Normal or High',NULL,1)
,(3054,380,2338,NULL,'High',NULL,1)
,(3055,381,2338,NULL,'High',NULL,1)
,(3057,380,2786,NULL,'Normal or High',NULL,1)
,(3058,381,2786,NULL,'High',NULL,1)
,(3059,380,10439,NULL,'Normal or More',NULL,1)
,(3060,381,10439,NULL,'Normal or More',NULL,1)
,(3061,380,2342,NULL,'Normal or High',NULL,1)
,(3062,379,10441,NULL,'Normal or Stratified',6131,1)
,(3066,379,30,NULL,'Normal or Stratified',NULL,1)
,(3067,381,1167,NULL,'Normal or Low',NULL,1)
,(3068,382,2569,NULL,'Increasing',NULL,1)
,(3069,383,2569,NULL,'High',NULL,1)
,(3070,382,22,NULL,'High',NULL,1)
,(3071,383,22,NULL,'High',NULL,1)
,(3072,383,19,NULL,'Normal or High',NULL,1)
,(3073,382,2786,NULL,'High',NULL,1)
,(3074,383,2786,NULL,'High',NULL,1)
,(3075,382,10439,NULL,'Normal or More',NULL,1)
,(3076,383,10439,NULL,'Normal or More',NULL,1)
,(3077,382,10441,NULL,'Normal or Stratified',6131,1)
,(3079,383,10441,NULL,'Normal, High or Stratified',6131,1)
,(3080,382,1167,NULL,'Normal or Stratified',NULL,1)
,(3081,383,1167,NULL,'Normal, Low or Stratified',NULL,1)
,(3082,382,10445,NULL,'Normal or High',NULL,1)
,(3083,384,2568,NULL,'Normal or Decreasing',NULL,1)
,(3084,385,2779,NULL,'Increasing',NULL,1)
,(3085,386,2779,NULL,'Increasing',NULL,1)
,(3086,387,2779,NULL,'Increasing',NULL,1)
,(3087,385,2778,NULL,'Normal or Increasing',NULL,1)
,(3088,385,10509,NULL,NULL,NULL,1)
,(3089,385,10510,NULL,NULL,NULL,1)
,(3090,385,10508,NULL,NULL,NULL,1)
,(3091,385,10511,NULL,NULL,NULL,1)
,(3092,385,10505,NULL,NULL,NULL,1)
,(3093,385,2777,NULL,'Normal or Increasing',NULL,1)
,(3094,386,2778,NULL,'Normal or Increasing',NULL,1)
,(3095,386,10509,NULL,NULL,NULL,1)
,(3096,386,10510,NULL,NULL,NULL,1)
,(3097,386,10508,NULL,NULL,NULL,1)
,(3098,386,10511,NULL,NULL,NULL,1)
,(3099,386,10505,NULL,NULL,NULL,1)
,(3100,386,2777,NULL,'Normal or Increasing',NULL,1)
,(3101,387,2777,NULL,'Normal or Increasing',NULL,1)
,(3102,387,2778,NULL,'Normal or Increasing',NULL,1)
,(3103,387,10509,NULL,NULL,NULL,1)
,(3104,387,10510,NULL,NULL,NULL,1)
,(3105,387,10508,NULL,NULL,NULL,1)
,(3106,387,10511,NULL,NULL,NULL,1)
,(3107,387,10505,NULL,NULL,NULL,1)
,(3108,388,2684,NULL,'Decreasing',NULL,1)
,(3109,389,2684,NULL,'Increasing',NULL,1)
,(3110,390,2684,NULL,'Increasing',NULL,1)
,(3111,391,2684,NULL,'Increasing',NULL,1)
,(3112,392,2684,NULL,'Increasing',NULL,1)
,(3113,392,2683,NULL,'Normal or Increasing',NULL,1)
,(3114,392,2685,NULL,'Normal or Increasing',NULL,1)
,(3120,388,2689,NULL,'Open less than normal',NULL,1)
,(3121,389,2689,NULL,'Open less than normal',NULL,1)
,(3122,391,2689,NULL,'Normal or open more than normal',NULL,1)
,(3123,392,2689,NULL,'Normal or open less than normal',NULL,1)
,(3124,388,2690,NULL,'Open or Closed',NULL,1)
,(3125,389,2690,NULL,'Open or Closed',NULL,1)
,(3126,390,2690,NULL,'Closed',NULL,1)
,(3127,391,2690,NULL,'Closed',NULL,1)
,(3128,392,2690,NULL,'Closed',NULL,1)
,(3129,388,2700,NULL,'High',NULL,1)
,(3130,389,2700,NULL,'High',NULL,1)
,(3131,390,2700,NULL,'Low',NULL,1)
,(3132,391,2700,NULL,'Low',NULL,1)
,(3133,392,2700,NULL,'Low',NULL,1)
,(3134,388,2456,NULL,'Normal or Decreasing',NULL,1)
,(3135,389,2456,NULL,'Decreasing',NULL,1)
,(3136,390,2456,NULL,'Decreasing',NULL,1)
,(3137,391,2456,NULL,'Decreasing',NULL,1)
,(3138,392,8058,NULL,'Lower than normal',NULL,1)
,(3139,388,2692,NULL,'High',NULL,1)
,(3140,389,2692,NULL,'High',NULL,1)
,(3141,390,2692,NULL,'High',NULL,1)
,(3142,391,2692,NULL,'High',NULL,1)
,(3143,392,2692,NULL,'High',NULL,1)
,(3144,389,2701,NULL,'Low',NULL,1)
,(3145,391,2701,NULL,'Low',NULL,1)
,(3146,393,2072,NULL,'Increasing',NULL,1)
,(3147,394,2072,NULL,'Increasing',NULL,1)
,(3148,395,2072,NULL,'Decreasing',NULL,1)
,(3149,396,2072,NULL,'Increasing',NULL,1)
,(3152,395,2071,NULL,'Increasing',NULL,1)
,(3153,393,2071,NULL,'Decreasing',NULL,1)
,(3154,393,2786,NULL,'Increasing',NULL,1)
,(3155,395,2786,NULL,'Increasing',NULL,1)
,(3156,393,57,NULL,'Slightly Low or Normal',NULL,1)
,(3157,395,57,NULL,'Slightly Low or Normal',NULL,1)
,(3158,397,2702,NULL,'High',NULL,1)
,(3159,397,5037,NULL,'Low',NULL,1)
,(3160,397,2683,NULL,'Normal if Shell Pressure (after valve) is known, otherwise High',NULL,1)
,(3161,398,2683,NULL,'Decreasing',NULL,1)
,(3162,398,2685,NULL,'Increasing',NULL,1)
,(3163,398,2689,NULL,'Normal or open less than normal',NULL,1)
,(3164,398,2690,NULL,'Closed',NULL,1)
,(3165,398,2699,NULL,'Lower than normal',NULL,1)
,(3166,398,2688,NULL,'Increasing',NULL,1)
,(3169,399,2456,NULL,'Low',NULL,1)
,(3170,400,2456,NULL,'Low',NULL,1)
,(3171,401,2456,NULL,'Low',NULL,1)
,(3172,402,2456,NULL,'Low',NULL,1)
,(3173,403,2456,NULL,'Low',NULL,1)
,(3174,404,2456,NULL,'Low',NULL,1)
,(3175,405,2456,NULL,'Low',NULL,1)
,(3176,406,2456,NULL,'Low',NULL,1)
,(3177,399,2438,NULL,'Higher than normal',NULL,1)
,(3178,400,2438,NULL,'Higher than normal',NULL,1)
,(3179,401,2438,NULL,'Higher than normal',NULL,1)
,(3180,405,2438,NULL,'Higher than normal',NULL,1)
,(3181,404,2438,NULL,'Higher than normal',NULL,1)
,(3182,403,2438,NULL,'Higher than normal',NULL,1)
,(3183,402,2438,NULL,'Higher than normal',NULL,1)
,(3184,399,2439,NULL,'Tsat',NULL,1)
,(3185,399,2440,NULL,'Different than Tsat',NULL,1)
,(3186,402,2445,NULL,'Higher than normal',NULL,1)
,(3187,402,2446,NULL,'Normal or High',NULL,1)
,(3188,402,2447,NULL,'Higher than normal',NULL,1)
,(3189,403,2538,NULL,'Lower than top of condenser tube bundle',NULL,1)
,(3190,399,2455,NULL,'Complicated',NULL,1)
,(3191,400,2455,NULL,'Abnormal - up or down',NULL,1)
,(3192,401,2455,NULL,'Complicated',NULL,1)
,(3193,402,2455,NULL,'Complicated',NULL,1)
,(3194,403,2455,NULL,'Complicated',NULL,1)
,(3195,404,2455,NULL,'Complicated',NULL,1)
,(3196,405,2455,NULL,'Complicated',NULL,1)
,(3197,406,2455,NULL,'Complicated',NULL,1)
,(3198,406,10423,NULL,'Abnormal',NULL,1)
,(3199,406,8058,NULL,'Abnormal',NULL,1)
,(3200,403,8058,1,'Lower than normal (sometimes)',NULL,1)
,(3201,403,2776,NULL,'Normal or increase',NULL,1)
,(3202,406,2776,NULL,'Abnormal',NULL,1)
,(3203,406,2776,14,'Abnormal',NULL,1)
,(3204,406,2786,14,'Abnormal',NULL,1)
,(3205,407,8058,NULL,'Low',NULL,1)
,(3207,409,8058,NULL,'Low',NULL,1)
,(3208,410,8058,NULL,'Low',NULL,1)
,(3209,411,8058,NULL,'Decreasing',NULL,1)
,(3211,413,8058,NULL,'Low',NULL,1)
,(3213,409,10423,NULL,'Abnormal',NULL,1)
,(3214,409,2786,NULL,'Abnormal',NULL,1)
,(3215,411,2786,NULL,'Low or Decreasing',NULL,1)
,(3216,409,2776,1,'Abnormal',NULL,1)
,(3217,409,2776,14,'Abnormal',NULL,1)
,(3218,407,2538,NULL,'Lower than top of condenser tube bundle',NULL,1)
,(3219,407,2776,1,'Normal or Increasing',NULL,1)
,(3220,411,2776,1,'Normal or Decreasing',NULL,1)
,(3221,407,2455,NULL,'Complicated',NULL,1)
,(3222,409,2455,NULL,'Complicated',NULL,1)
,(3223,410,2455,NULL,'Complicated',NULL,1)
,(3224,411,2455,NULL,'Complicated',NULL,1)
,(3225,413,2455,NULL,'Complicated',NULL,1)
,(3226,407,2438,NULL,'Higher than Normal',NULL,1)
,(3227,410,2438,NULL,'Higher than Normal',NULL,1)
,(3228,411,2438,NULL,'Normal or Increasing',NULL,1)
,(3229,412,2438,NULL,'Higher than Normal',NULL,1)
,(3230,413,2438,NULL,'Higher than Normal',NULL,1)
,(3232,415,2445,NULL,'Higher than normal',NULL,1)
,(3233,415,2456,NULL,'Low',NULL,1)
,(3234,415,2438,NULL,'Higher than normal',NULL,1)
,(3235,415,2455,NULL,'Complicated',NULL,1)
,(3236,415,2447,NULL,'Higher than normal',NULL,1)
,(3237,416,2768,NULL,'Low',NULL,1)
,(3238,417,2768,NULL,'Decreasing',NULL,1)
,(3239,418,2768,NULL,'Decreasing',NULL,1)
,(3240,419,2768,NULL,'Low',NULL,1)
,(3241,420,2768,NULL,'Decreasing',NULL,1)
,(3242,421,2768,NULL,'Low',NULL,1)
,(3243,418,2778,NULL,'Normal or High',NULL,1)
,(3244,418,10509,NULL,NULL,NULL,1)
,(3245,418,10510,NULL,NULL,NULL,1)
,(3246,418,10508,NULL,NULL,NULL,1)
,(3247,418,10511,NULL,NULL,NULL,1)
,(3248,418,10505,NULL,NULL,NULL,1)
,(3249,419,2778,NULL,'Normal or High',NULL,1)
,(3250,419,10509,NULL,NULL,NULL,1)
,(3251,419,10510,NULL,NULL,NULL,1)
,(3252,419,10508,NULL,NULL,NULL,1)
,(3253,419,10511,NULL,NULL,NULL,1)
,(3254,419,10505,NULL,NULL,NULL,1)
,(3255,420,2778,NULL,'Normal or High',NULL,1)
,(3256,420,10509,NULL,NULL,NULL,1)
,(3257,420,10510,NULL,NULL,NULL,1)
,(3258,420,10508,NULL,NULL,NULL,1)
,(3259,420,10511,NULL,NULL,NULL,1)
,(3260,420,10505,NULL,NULL,NULL,1)
,(3261,421,2778,NULL,'Normal or High',NULL,1)
,(3262,421,10509,NULL,NULL,NULL,1)
,(3263,421,10510,NULL,NULL,NULL,1)
,(3264,421,10508,NULL,NULL,NULL,1)
,(3265,421,10511,NULL,NULL,NULL,1)
,(3266,421,10505,NULL,NULL,NULL,1)
,(3267,417,2774,NULL,'Increasing',NULL,1)
,(3268,422,2768,NULL,'Decreasing',NULL,1)
,(3269,423,2768,NULL,'Low',NULL,1)
,(3270,424,2768,NULL,'Decreasing',NULL,1)
,(3271,425,2768,NULL,'Low',NULL,1)
,(3272,423,2778,NULL,'Normal or High',NULL,1)
,(3273,423,10509,NULL,NULL,NULL,1)
,(3274,423,10510,NULL,NULL,NULL,1)
,(3275,423,10508,NULL,NULL,NULL,1)
,(3276,423,10511,NULL,NULL,NULL,1)
,(3277,423,10505,NULL,NULL,NULL,1)
,(3278,425,2778,NULL,'Normal or High',NULL,1)
,(3279,425,10509,NULL,NULL,NULL,1)
,(3280,425,10510,NULL,NULL,NULL,1)
,(3281,425,10508,NULL,NULL,NULL,1)
,(3282,425,10511,NULL,NULL,NULL,1)
,(3283,425,10505,NULL,NULL,NULL,1)
,(3284,426,2768,NULL,'Decreasing',NULL,1)
,(3285,427,2768,NULL,'Low',NULL,1)
,(3286,428,2768,NULL,'Decreasing',NULL,1)
,(3287,429,2768,NULL,'Low',NULL,1)
,(3288,427,2778,NULL,'Normal or High',NULL,1)
,(3289,429,2778,NULL,'Normal or High',NULL,1)
,(3290,427,10509,NULL,NULL,NULL,1)
,(3291,427,10510,NULL,NULL,NULL,1)
,(3292,427,10508,NULL,NULL,NULL,1)
,(3293,427,10511,NULL,NULL,NULL,1)
,(3294,427,10505,NULL,NULL,NULL,1)
,(3295,429,10509,NULL,NULL,NULL,1)
,(3296,429,10510,NULL,NULL,NULL,1)
,(3297,429,10508,NULL,NULL,NULL,1)
,(3298,429,10511,NULL,NULL,NULL,1)
,(3299,429,10505,NULL,NULL,NULL,1)
,(3300,430,2768,NULL,'Decreasing',NULL,1)
,(3301,431,2768,NULL,'Low',NULL,1)
,(3302,432,2768,NULL,'Decreasing',NULL,1)
,(3303,433,2768,NULL,'Low',NULL,1)
,(3304,431,2778,NULL,'Normal or High',NULL,1)
,(3305,431,10509,NULL,NULL,NULL,1)
,(3306,431,10510,NULL,NULL,NULL,1)
,(3307,431,10511,NULL,NULL,NULL,1)
,(3308,431,10508,NULL,NULL,NULL,1)
,(3309,431,10505,NULL,NULL,NULL,1)
,(3310,433,2778,NULL,'Normal or High',NULL,1)
,(3311,433,10509,NULL,NULL,NULL,1)
,(3312,433,10510,NULL,NULL,NULL,1)
,(3313,433,10508,NULL,NULL,NULL,1)
,(3314,433,10511,NULL,NULL,NULL,1)
,(3315,433,10505,NULL,NULL,NULL,1)
,(3316,434,2768,NULL,'Decreasing',NULL,1)
,(3317,435,2768,NULL,'Low',NULL,1)
,(3318,436,2768,NULL,'Decreasing',NULL,1)
,(3319,437,2768,NULL,'Low',NULL,1)
,(3320,435,2778,NULL,'Normal or High',NULL,1)
,(3321,435,10509,NULL,NULL,NULL,1)
,(3322,435,10510,NULL,NULL,NULL,1)
,(3323,435,10508,NULL,NULL,NULL,1)
,(3324,435,10511,NULL,NULL,NULL,1)
,(3325,435,10505,NULL,NULL,NULL,1)
,(3326,437,10509,NULL,NULL,NULL,1)
,(3327,437,10510,NULL,NULL,NULL,1)
,(3328,437,10508,NULL,NULL,NULL,1)
,(3330,437,10511,NULL,NULL,NULL,1)
,(3331,437,2778,NULL,'Normal or High',NULL,1)
,(3332,437,10505,NULL,NULL,NULL,1)
,(3333,438,2768,NULL,'Decreasing',NULL,1)
,(3334,439,2768,NULL,'Low',NULL,1)
,(3335,440,2768,NULL,'Decreasing',NULL,1)
,(3336,441,2768,NULL,'Low',NULL,1)
,(3337,439,10509,NULL,NULL,NULL,1)
,(3338,439,10510,NULL,NULL,NULL,1)
,(3339,439,10508,NULL,NULL,NULL,1)
,(3340,439,10511,NULL,NULL,NULL,1)
,(3341,439,2778,NULL,'Normal or High',NULL,1)
,(3342,439,10505,NULL,NULL,NULL,1)
,(3343,441,10509,NULL,NULL,NULL,1)
,(3344,441,10510,NULL,NULL,NULL,1)
,(3345,441,10508,NULL,NULL,NULL,1)
,(3346,441,10511,NULL,NULL,NULL,1)
,(3347,441,2778,NULL,'Normal or High',NULL,1)
,(3348,441,10505,NULL,NULL,NULL,1)
,(3349,442,2768,NULL,'Increasing',NULL,1)
,(3350,443,2768,NULL,'Decreasing',NULL,1)
,(3351,444,2768,NULL,'Low',NULL,1)
,(3352,445,2768,NULL,'Unreasonable (>95% or <70%)',NULL,1)
,(3353,446,2768,NULL,'Low',NULL,1)
,(3354,447,2768,NULL,'Decreasing',NULL,1)
,(3355,448,2768,NULL,'Low',NULL,1)
,(3356,449,2768,NULL,'Increasing or Decreasing',NULL,1)
,(3357,443,10509,NULL,NULL,NULL,1)
,(3358,443,10510,NULL,NULL,NULL,1)
,(3359,443,10508,NULL,NULL,NULL,1)
,(3360,443,10511,NULL,NULL,NULL,1)
,(3361,443,2778,NULL,'Normal or High',NULL,1)
,(3362,443,10505,NULL,NULL,NULL,1)
,(3363,444,10509,NULL,NULL,NULL,1)
,(3364,444,10510,NULL,NULL,NULL,1)
,(3365,444,10508,NULL,NULL,NULL,1)
,(3366,444,10511,NULL,NULL,NULL,1)
,(3367,444,2778,NULL,'Normal or High',NULL,1)
,(3368,444,10505,NULL,NULL,NULL,1)
,(3369,446,10509,NULL,NULL,NULL,1)
,(3370,446,10510,NULL,NULL,NULL,1)
,(3371,446,10508,NULL,NULL,NULL,1)
,(3372,446,10511,NULL,NULL,NULL,1)
,(3373,446,2778,NULL,'Normal or High',NULL,1)
,(3374,446,10505,NULL,NULL,NULL,1)
,(3375,447,10509,NULL,NULL,NULL,1)
,(3376,447,10510,NULL,NULL,NULL,1)
,(3377,447,10508,NULL,NULL,NULL,1)
,(3378,447,10511,NULL,NULL,NULL,1)
,(3379,447,2778,NULL,'Normal or High',NULL,1)
,(3380,447,10505,NULL,NULL,NULL,1)
,(3381,448,10509,NULL,NULL,NULL,1)
,(3382,448,10510,NULL,NULL,NULL,1)
,(3383,448,10508,NULL,NULL,NULL,1)
,(3384,448,10511,NULL,NULL,NULL,1)
,(3385,448,2778,NULL,'Normal or High',NULL,1)
,(3386,448,10505,NULL,NULL,NULL,1)
,(3387,443,150,20,'Decreasing',NULL,1)
,(3388,444,150,20,'Low',NULL,1)
,(3389,446,150,20,'Low',NULL,1)
,(3390,447,150,20,'Decreasing',NULL,1)
,(3391,448,150,20,'Low',NULL,1)
,(3392,449,150,20,'Increasing or Decreasing',NULL,1)
,(3393,450,2768,NULL,'Increasing',NULL,1)
,(3394,450,5037,NULL,'Increasing',NULL,1)
,(3395,450,5038,NULL,'Increasing',NULL,1)
,(3396,456,10509,NULL,NULL,NULL,1)
,(3397,456,10510,NULL,NULL,NULL,1)
,(3398,456,10508,NULL,NULL,NULL,1)
,(3399,456,10511,NULL,NULL,NULL,1)
,(3400,456,2778,NULL,'Increasing',NULL,1)
,(3401,456,10505,NULL,'Decreasing',NULL,1)
,(3402,457,10509,NULL,NULL,NULL,1)
,(3403,457,10510,NULL,NULL,NULL,1)
,(3404,457,10508,NULL,NULL,NULL,1)
,(3405,457,10511,NULL,NULL,NULL,1)
,(3406,457,2778,NULL,'Increasing',NULL,1)
,(3407,457,10505,NULL,NULL,NULL,1)
,(3408,458,10509,NULL,NULL,NULL,1)
,(3409,458,10510,NULL,NULL,NULL,1)
,(3410,458,10508,NULL,NULL,NULL,1)
,(3411,458,10511,NULL,NULL,NULL,1)
,(3412,458,2778,NULL,'Increasing',NULL,1)
,(3413,458,10505,NULL,NULL,NULL,1)
,(3414,459,10509,NULL,NULL,NULL,1)
,(3415,459,10510,NULL,NULL,NULL,1)
,(3416,459,10508,NULL,NULL,NULL,1)
,(3417,459,10511,NULL,NULL,NULL,1)
,(3418,459,2778,NULL,'Increasing',NULL,1)
,(3419,459,10505,NULL,NULL,NULL,1)
,(3420,456,2779,NULL,'Normal or Increasing',NULL,1)
,(3421,457,2779,NULL,'Normal or Increasing',NULL,1)
,(3422,458,2779,NULL,'Normal or Increasing',NULL,1)
,(3423,459,2779,NULL,'Normal or Increasing',NULL,1)
,(3424,456,2777,NULL,'Normal or Increasing',NULL,1)
,(3425,457,2777,NULL,'Normal or Increasing',NULL,1)
,(3426,458,2777,NULL,'Normal or Increasing',NULL,1)
,(3427,459,2777,NULL,'Normal or Increasing',NULL,1)
,(3428,456,2768,NULL,'Decreasing',NULL,1)
,(3429,460,2777,NULL,'Increasing',NULL,1)
,(3430,461,2777,NULL,'Increasing',NULL,1)
,(3431,461,10509,NULL,NULL,NULL,1)
,(3432,461,10510,NULL,NULL,NULL,1)
,(3433,461,10508,NULL,NULL,NULL,1)
,(3434,461,10511,NULL,NULL,NULL,1)
,(3435,461,2778,NULL,'Increasing',NULL,1)
,(3436,461,10505,NULL,NULL,NULL,1)
,(3437,460,2779,NULL,'Normal or Increasing',NULL,1)
,(3438,461,2779,NULL,'Normal or Increasing',NULL,1)
,(3439,460,10339,NULL,'Increasing',NULL,1)
,(3440,461,10339,NULL,'Normal or Slight Increasing',NULL,1)
,(3441,460,10340,NULL,'Increasing',NULL,1)
,(3442,461,10340,NULL,'Normal or Slight Increasing',NULL,1)
,(3447,462,1328,NULL,'Decreasing',NULL,1)
,(3448,463,1328,NULL,'Decreasing',NULL,1)
,(3449,464,1328,NULL,'Decreasing',NULL,1)
,(3450,465,1328,NULL,'Increasing or Decreasing',NULL,1)
,(3451,464,56,NULL,'Normal or Increasing',NULL,1)
,(3452,462,2683,NULL,'Normal or Decreasing',NULL,1)
,(3453,463,2683,NULL,'Normal or Decreasing',NULL,1)
,(3454,462,2438,NULL,'Normal or High',NULL,1)
,(3455,463,2438,NULL,'Normal or High',NULL,1)
,(3456,462,2456,NULL,'Decreasing',NULL,1)
,(3457,463,2456,NULL,'Decreasing',NULL,1)
,(3458,462,2689,NULL,'Normal or More Open than Normal',NULL,1)
,(3459,463,2689,NULL,'Normal or More Closed than Normal',NULL,1)
,(3460,462,2690,NULL,'Normal or More Open than Normal',NULL,1)
,(3461,463,2690,NULL,'Normal or More Open than Normal',NULL,1)
,(3462,462,2700,NULL,'Hot or High',NULL,1)
,(3463,463,2700,NULL,'Hot or High',NULL,1)
,(3464,466,56,NULL,'High',NULL,1)
,(3465,467,56,NULL,'High',NULL,1)
,(3466,466,1328,NULL,'Normal or High',NULL,1)
,(3467,467,1328,NULL,'Normal or High',NULL,1)
,(3468,467,2683,NULL,'Normal or High',NULL,1)
,(3469,467,2689,NULL,'Normal or Less than Normal',NULL,1)
,(3470,468,143,NULL,'High',NULL,1)
,(3471,469,143,NULL,'High',NULL,1)
,(3472,470,143,NULL,'High',NULL,1)
,(3473,471,143,NULL,'High',NULL,1)
,(3474,472,143,NULL,'High',NULL,1)
,(3475,468,4002,NULL,'High',NULL,1)
,(3476,469,4002,NULL,'High',NULL,1)
,(3477,470,4002,NULL,'High',NULL,1)
,(3478,471,4002,NULL,'High',NULL,1)
,(3479,473,55,NULL,'Decreasing',NULL,1)
,(3480,474,55,NULL,'Decreasing',NULL,1)
,(3481,475,55,NULL,'Decreasing',NULL,1)
,(3482,476,55,NULL,'Increasing or Decreasing',NULL,1)
,(3483,473,2456,NULL,'Decreasing',NULL,1)
,(3484,474,2456,NULL,'Decreasing',NULL,1)
,(3485,473,2438,NULL,'Normal or High',NULL,1)
,(3486,474,2438,NULL,'Normal or High',NULL,1)
,(3487,473,2689,NULL,'Normal or More Open than Normal',NULL,1)
,(3488,474,2689,NULL,'Normal or More Closed than Normal',NULL,1)
,(3489,473,2690,NULL,'Normal or More Open than Normal',NULL,1)
,(3490,474,2690,NULL,'Normal or More Open than Normal',NULL,1)
,(3491,473,2700,NULL,'Hot or High',NULL,1)
,(3492,474,2700,NULL,'Hot or High',NULL,1)
,(3493,477,2387,NULL,'Increasing',NULL,1)
,(3494,478,2387,NULL,'Normal or Increasing',NULL,1)
,(3495,479,2006,NULL,'Increasing',NULL,1)
,(3496,480,2006,NULL,'Increasing',NULL,1)
,(3497,481,2006,NULL,'Increasing',NULL,1)
,(3498,482,2006,NULL,'Increasing',NULL,1)
,(3499,483,2006,NULL,'Increasing',NULL,1)
,(3500,484,2006,NULL,'Increasing',NULL,1)
,(3501,485,2006,NULL,'Increasing',NULL,1)
,(3502,479,2006,20,'Steady',NULL,1)
,(3503,480,2006,20,'Increasing',NULL,1)
,(3504,481,2006,20,'Normal or Increasing',NULL,1)
,(3505,482,2006,20,'Normal or Increasing',NULL,1)
,(3506,483,2006,20,'Steady',NULL,1)
,(3507,484,2006,20,'Steady',NULL,1)
,(3508,479,2456,NULL,'Decreasing',NULL,1)
,(3509,483,2456,NULL,'Decreasing',NULL,1)
,(3510,484,2456,NULL,'Normal or Low',NULL,1)
,(3511,485,2456,NULL,'Possible Decreasing',NULL,1)
,(3512,479,2438,NULL,'Increasing',NULL,1)
,(3513,480,2438,NULL,'Normal or Increasing',NULL,1)
,(3514,483,2438,NULL,'Increasing',NULL,1)
,(3515,484,2438,NULL,'Normal or High',NULL,1)
,(3516,485,2438,NULL,'Possible Increasing',NULL,1)
,(3517,479,2456,14,'Normal or High',NULL,1)
,(3518,479,2439,NULL,'Increasing',NULL,1)
,(3519,480,2439,NULL,'Normal or Increasing',NULL,1)
,(3520,483,2439,NULL,'Increasing',NULL,1)
,(3521,484,2439,NULL,'Normal or High',NULL,1)
,(3522,485,2439,NULL,'Possible Increasing',NULL,1)
,(3523,479,2455,NULL,'Increasing or Decreasing',NULL,1)
,(3524,482,2455,NULL,'Normal or Increasing',NULL,1)
,(3525,485,2455,NULL,'Increasing or Decreasing',NULL,1)
,(3526,479,2440,NULL,'Increasing',NULL,1)
,(3527,480,2440,NULL,'Normal or Increasing',NULL,1)
,(3528,483,2440,NULL,'Increasing',NULL,1)
,(3529,484,2440,NULL,'Normal or High',NULL,1)
,(3530,485,2440,NULL,'Possible Increasing',NULL,1)
,(3531,479,10217,4,'Increasing',NULL,1)
,(3532,480,10217,4,'Normal or Increasing',NULL,1)
,(3533,483,10217,4,'Increasing',NULL,1)
,(3534,484,10217,4,'Normal or High',NULL,1)
,(3535,485,10217,4,'Possible Increasing',NULL,1)
,(3536,480,2683,NULL,'Normal or Increasing or Decreasing',NULL,1)
,(3537,481,2683,NULL,'Normal or Increasing',NULL,1)
,(3538,483,2683,14,'Calc Fail',NULL,1)
,(3539,484,2683,14,'Normal or High',NULL,1)
,(3540,485,2683,14,'Steady',NULL,1)
,(3541,298,91,NULL,'Low',NULL,1)
,(3542,299,91,NULL,'Low',NULL,1)
,(3543,300,91,NULL,'Low',NULL,1)
,(3544,301,91,NULL,'Low',NULL,1)
,(3545,298,101,11,'Higher than Actual',NULL,1)
,(3546,300,101,11,'Low',NULL,1)
,(3547,298,37,NULL,'High',NULL,1)
,(3548,299,37,NULL,'High',NULL,1)
,(3549,300,37,NULL,'High',NULL,1)
,(3550,298,30,NULL,'Normal or Slightly High',NULL,1)
,(3551,299,30,NULL,'High',NULL,1)
,(3552,300,30,NULL,'Normal or Slightly High',NULL,1)
,(3553,298,15,NULL,'Low',NULL,1)
,(3554,299,15,NULL,'Low',NULL,1)
,(3555,300,15,NULL,'Low',NULL,1)
,(3556,298,2456,NULL,'High',NULL,1)
,(3557,299,2456,NULL,'Low',NULL,1)
,(3558,300,2456,NULL,'High',NULL,1)
,(3559,486,14,NULL,'Low',NULL,1)
,(3560,487,14,NULL,'Low',NULL,1)
,(3561,488,14,NULL,'Low',NULL,1)
,(3562,489,14,NULL,'Low',NULL,1)
,(3563,486,2456,NULL,'High',NULL,1)
,(3564,487,2456,NULL,'Low',NULL,1)
,(3565,488,2456,NULL,'High',NULL,1)
,(3566,490,2552,NULL,'High',6131,1)
,(3567,491,2552,NULL,'High',6131,1)
,(3568,490,30,NULL,'Normal or High',NULL,1)
,(3569,491,30,NULL,'Normal or Inconsistent',NULL,1)
,(3570,490,14,NULL,'Normal or Low',NULL,1)
,(3571,490,15,NULL,'Normal or Low',NULL,1)
,(3572,493,10441,NULL,'Normal or Stratified',6131,1)
,(3573,493,2552,NULL,'Normal or Low',6131,1)
,(3574,493,9,NULL,'Normal or High',NULL,1)
,(3575,493,4002,NULL,'Normal or High',NULL,1)
,(3576,493,10439,NULL,'Normal or Open more than Normal',NULL,1)
,(3577,495,10441,NULL,'Normal or Stratified',6131,1)
,(3579,495,2552,NULL,'Normal or Low',6131,1)
,(3580,495,9,NULL,'Normal or High',NULL,1)
,(3581,495,4002,NULL,'Normal or High',NULL,1)
,(3582,495,10439,NULL,'Normal or Open more than Normal',NULL,1)
,(3583,498,10441,NULL,'Normal or Stratified',6131,1)
,(3584,498,2552,NULL,'Normal or Low',6131,1)
,(3585,498,9,NULL,'Normal or High',NULL,1)
,(3586,496,19,NULL,'Normal or High',NULL,1)
,(3587,497,19,NULL,'Normal or High',NULL,1)
,(3588,498,19,NULL,'Normal or Low',NULL,1)
,(3589,496,2338,NULL,'Normal or High',NULL,1)
,(3590,497,2338,NULL,'Normal or High',NULL,1)
,(3591,498,2338,NULL,'High',NULL,1)
,(3592,496,2456,NULL,'Normal or Low',NULL,1)
,(3593,497,2456,NULL,'Normal or Low',NULL,1)
,(3594,498,2456,NULL,'Normal or Low',NULL,1)
,(3595,498,4002,NULL,'Normal or High',NULL,1)
,(3596,498,10439,NULL,'Normal or Open more than Normal',NULL,1)
,(3597,499,30,NULL,'High',NULL,1)
,(3598,500,30,NULL,'High',NULL,1)
,(3599,501,30,NULL,'Low or High',NULL,1)
,(3601,500,4002,NULL,'High',NULL,1)
,(3602,500,2786,NULL,'High',NULL,1)
,(3603,500,10439,NULL,'Normal or More',NULL,1)
,(3604,499,10441,NULL,'Increase',6131,1)
,(3605,500,10441,NULL,'Increase',6131,1)
,(3606,501,10441,NULL,'Increase',6131,1)
,(3607,500,4013,NULL,'Normal or High',NULL,1)
,(3608,502,10441,NULL,'Low or High',6131,1)
,(3609,503,10441,NULL,'High',6131,1)
,(3610,504,10441,NULL,'Low or High',6131,1)
,(3611,502,30,NULL,'Low or High',NULL,1)
,(3612,503,30,NULL,'High',NULL,1)
,(3613,504,30,NULL,'Low or High',NULL,1)
,(3614,503,4002,NULL,'High',NULL,1)
,(3615,503,10439,NULL,'Normal or More',NULL,1)
,(3616,503,4013,NULL,'Normal or High',NULL,1)
,(3617,505,10441,NULL,'High',6131,1)
,(3618,506,10441,NULL,'High',6131,1)
,(3619,507,10441,NULL,'High',6131,1)
,(3620,505,19,NULL,'Normal or Low',NULL,1)
,(3621,506,19,NULL,'Normal or Low',NULL,1)
,(3622,505,2338,NULL,'Normal or Low',NULL,1)
,(3623,506,2338,NULL,'Normal or Low',NULL,1)
,(3624,505,2786,NULL,'Normal or High',NULL,1)
,(3625,506,2786,NULL,'Normal or High',NULL,1)
,(3626,505,10439,NULL,'Normal or High',NULL,1)
,(3627,506,10439,NULL,'Normal or High',NULL,1)
,(3628,505,4013,NULL,'Normal or High',NULL,1)
,(3629,506,4013,NULL,'Normal or High',NULL,1)
,(3630,508,2456,NULL,'Normal or Low',NULL,1)
,(3631,509,2456,NULL,'Normal or Low',NULL,1)
,(3632,508,2342,NULL,'Normal or High',NULL,1)
,(3633,509,2342,NULL,'Normal or High',NULL,1)
,(3634,511,19,NULL,'Normal or High',NULL,1)
,(3635,511,2338,NULL,'High',NULL,1)
,(3636,511,2786,NULL,'Normal or Low',NULL,1)
,(3637,511,2342,NULL,'Normal or Low',NULL,1)
,(3638,513,10260,NULL,'High or Increasing',NULL,1)
,(3639,514,10261,NULL,'High or Increasing',NULL,1)
,(3640,515,10260,NULL,'Normal or Inconsistent',NULL,1)
,(3641,516,10260,NULL,'High Increasing',NULL,1)
,(3642,517,10261,NULL,'High or Increasing',NULL,1)
,(3643,518,10238,NULL,'Increasing',NULL,1)
,(3644,519,10238,NULL,'Increasing',NULL,1)
,(3645,520,10238,NULL,'Increasing',NULL,1)
,(3646,519,10439,NULL,'Normal or High',NULL,1)
,(3647,518,10241,NULL,'Increasing',NULL,1)
,(3648,518,10236,NULL,'Decreasing',NULL,1)
,(3649,521,10241,NULL,'Increasing',NULL,1)
,(3650,522,10241,NULL,'Increasing or Decreasing',NULL,1)
,(3651,521,2768,NULL,'Normal or Decreasing',NULL,1)
,(3652,308,10243,NULL,'Decreasing',NULL,1)
,(3653,309,10243,NULL,'Decreasing',NULL,1)
,(3654,546,2526,NULL,'Increasing or High',NULL,1)
,(3655,38,1328,NULL,'Change',NULL,1)
,(3656,38,55,NULL,'Change',NULL,1)
,(3657,35,1328,NULL,'Change',NULL,1)
,(3658,35,1327,NULL,'Change',NULL,1)
,(3659,36,1328,NULL,'Change',NULL,1)
,(3660,36,55,NULL,'Change',NULL,1)
,(3661,37,1328,NULL,'Change',NULL,1)
,(3662,37,55,NULL,'Change',NULL,1)
,(3663,14,1328,NULL,'Change',NULL,1)
,(3664,14,55,NULL,'Change',NULL,1)
,(3665,15,1328,NULL,'Change',NULL,1)
,(3666,15,55,NULL,'Change',NULL,1)
,(3681,100,1328,NULL,'Change',NULL,1)
,(3682,100,55,NULL,'Change',NULL,1)
,(3683,101,1328,NULL,'Change',NULL,1)
,(3684,101,55,NULL,'Change',NULL,1)
,(3685,102,1328,NULL,'Change',NULL,1)
,(3686,102,55,NULL,'Change',NULL,1)
,(3687,105,1328,NULL,'Change',NULL,1)
,(3688,105,55,NULL,'Change',NULL,1)
,(3689,107,1328,NULL,'Change',NULL,1)
,(3690,107,55,NULL,'Change',NULL,1)
,(3691,109,1328,NULL,'Change',NULL,1)
,(3692,109,55,NULL,'Change',NULL,1)
,(3693,369,2054,NULL,'Normal or High',NULL,1)
,(3694,154,10473,1,NULL,6052,1)
,(3695,154,10468,1,NULL,6052,1)
,(3696,154,8030,1,NULL,NULL,1)
,(3697,154,10469,1,NULL,NULL,1)
,(3698,154,10472,1,NULL,NULL,1)
,(3699,154,8058,1,NULL,NULL,1)
,(3700,155,10468,1,NULL,6052,1)
,(3701,155,10469,1,NULL,NULL,1)
,(3702,155,8030,1,NULL,NULL,1)
,(3703,155,10473,1,NULL,6052,1)
,(3704,155,10472,1,NULL,NULL,1)
,(3705,155,8058,1,NULL,NULL,1)
,(3706,153,10468,1,NULL,6052,1)
,(3707,153,10469,1,NULL,NULL,1)
,(3708,153,8030,1,NULL,NULL,1)
,(3709,153,10473,1,NULL,6052,1)
,(3710,153,10472,1,NULL,NULL,1)
,(3711,153,8058,1,NULL,NULL,1)
,(3712,156,10468,1,NULL,6052,1)
,(3713,548,10526,NULL,'Decreasing',NULL,1)
,(3714,547,10526,NULL,'Decreasing',NULL,1)
,(3715,549,10526,NULL,'Decreasing',NULL,1)
,(3716,548,8025,NULL,'Increasing',NULL,1)
,(3717,548,10546,NULL,'Increasing',NULL,1)
)
AS SOURCE([IssueCauseTypeVariableTypeMapID],[IssueCauseTypeID],[VariableTypeID],[ValueTypeID],[Symptom],[AssetClassTypeID],[SymptomLevel])
ON Target.[IssueCauseTypeVariableTypeMapID]= Source.[IssueCauseTypeVariableTypeMapID]
WHEN MATCHED THEN UPDATE SET [IssueCauseTypeID]=Source.[IssueCauseTypeID],[VariableTypeID]=Source.[VariableTypeID],[ValueTypeID]=Source.[ValueTypeID],[Symptom]=Source.[Symptom],[AssetClassTypeID]=Source.[AssetClassTypeID],[SymptomLevel]=Source.[SymptomLevel]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([IssueCauseTypeVariableTypeMapID],[IssueCauseTypeID],[VariableTypeID],[ValueTypeID],[Symptom],[AssetClassTypeID],[SymptomLevel])
VALUES ([IssueCauseTypeVariableTypeMapID],[IssueCauseTypeID],[VariableTypeID],[ValueTypeID],[Symptom],[AssetClassTypeID],[SymptomLevel])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [Diagnostics].[tIssueCauseTypeVariableTypeMap] OFF
GO



SET IDENTITY_INSERT [Diagnostics].[tAssetClassTypeIssueTypeMap] ON
GO

PRINT 'Populating Diagnostics.tAssetClassTypeIssueTypeMap';
MERGE INTO Diagnostics.tAssetClassTypeIssueTypeMap AS TARGET USING (VALUES
(	6	,	6038	,	6	,	NULL	),
(	9	,	6038	,	9	,	NULL	),
(	10	,	6038	,	10	,	NULL	),
(	11	,	6038	,	11	,	NULL	),
(	21	,	6143	,	21	,	NULL	),
(	22	,	6143	,	22	,	NULL	),
(	23	,	6143	,	23	,	NULL	),
(	31	,	5028	,	31	,	NULL	),
(	39	,	6202	,	64	,	64	),
(	40	,	6187	,	65	,	65	),
(	41	,	6187	,	66	,	66	),
(	42	,	6187	,	67	,	67	),
(	43	,	6187	,	68	,	68	),
(	44	,	6187	,	69	,	69	),
(	45	,	6187	,	70	,	70	),
(	46	,	6187	,	71	,	71	),
(	47	,	6187	,	72	,	72	),
(	48	,	6187	,	73	,	73	),
(	49	,	6187	,	74	,	74	),
(	50	,	6187	,	75	,	75	),
(	51	,	6187	,	76	,	76	),
(	52	,	6188	,	77	,	77	),
(	53	,	6192	,	78	,	78	),
(	54	,	6192	,	79	,	79	),
(	55	,	6191	,	80	,	80	),
(	56	,	6191	,	81	,	81	),
(	57	,	6190	,	82	,	82	),
(	58	,	6190	,	83	,	83	),
(	59	,	6188	,	84	,	84	),
(	60	,	6188	,	85	,	85	),
(	61	,	6188	,	86	,	86	),
(	62	,	6188	,	87	,	87	),
(	63	,	6188	,	88	,	88	),
(	64	,	6188	,	89	,	89	),
(	65	,	6197	,	90	,	90	),
(	66	,	6197	,	91	,	91	),
(	67	,	5035	,	92	,	92	),
(	71	,	5002	,	127	,	0	),
(	72	,	5002	,	129	,	0	),
(	74	,	5003	,	128	,	0	),
(	75	,	5002	,	125	,	0	),
(	76	,	5002	,	124	,	0	),
(	77	,	5002	,	39	,	0	),
(	78	,	5002	,	40	,	0	),
(	79	,	5002	,	123	,	0	),
(	80	,	5002	,	41	,	0	),
(	81	,	5002	,	126	,	0	),
(	83	,	6176	,	152	,	0	),
(	84	,	6176	,	42	,	0	),
(	86	,	6038	,	135	,	0	),
(	87	,	6036	,	68	,	0	),
(	88	,	6036	,	71	,	0	),
(	89	,	6036	,	70	,	0	),
(	90	,	6036	,	67	,	0	),
(	91	,	6036	,	69	,	0	),
(	92	,	6038	,	150	,	0	),
(	93	,	6038	,	136	,	0	),
(	98	,	6187	,	101	,	0	),
(	99	,	6187	,	100	,	0	),
(	100	,	6187	,	168	,	0	),
(	101	,	6187	,	167	,	0	),
(	102	,	6187	,	103	,	0	),
(	103	,	6187	,	166	,	0	),
(	104	,	6187	,	104	,	0	),
(	105	,	6187	,	102	,	0	),
(	106	,	6187	,	171	,	0	),
(	107	,	6187	,	170	,	0	),
(	108	,	6187	,	173	,	0	),
(	109	,	6187	,	172	,	0	),
(	110	,	6187	,	175	,	0	),
(	111	,	6187	,	174	,	0	),
(	112	,	6187	,	169	,	0	),
(	113	,	6187	,	123	,	0	),
(	114	,	6094	,	117	,	0	),
(	115	,	6094	,	120	,	0	),
(	116	,	6094	,	118	,	0	),
(	117	,	6094	,	119	,	0	),
(	118	,	6104	,	117	,	0	),
(	119	,	6104	,	120	,	0	),
(	120	,	6104	,	118	,	0	),
(	121	,	6104	,	119	,	0	),
(	124	,	6052	,	153	,	0	),
(	125	,	6052	,	130	,	0	),
(	126	,	6052	,	131	,	0	),
(	127	,	6052	,	132	,	0	),
(	128	,	6052	,	31	,	0	),
(	129	,	6052	,	133	,	0	),
(	130	,	6179	,	66	,	0	),
(	132	,	6156	,	140	,	0	),
(	133	,	6156	,	95	,	0	),
(	135	,	6156	,	138	,	0	),
(	136	,	6156	,	139	,	0	),
(	139	,	6185	,	145	,	0	),
(	140	,	6158	,	145	,	0	),
(	141	,	6185	,	137	,	0	),
(	142	,	6185	,	143	,	0	),
(	143	,	6185	,	142	,	0	),
(	144	,	6185	,	141	,	0	),
(	145	,	6158	,	144	,	0	),
(	146	,	6038	,	134	,	0	),
(	148	,	6033	,	105	,	0	),
(	149	,	6033	,	107	,	0	),
(	150	,	6033	,	106	,	0	),
(	151	,	6033	,	99	,	0	),
(	152	,	6224	,	97	,	0	),
(	153	,	6224	,	111	,	0	),
(	154	,	6224	,	109	,	0	),
(	155	,	6224	,	110	,	0	),
(	157	,	6156	,	113	,	0	),
(	158	,	5043	,	176	,	0	),
(	159	,	6033	,	108	,	0	),
(	161	,	5022	,	121	,	0	),
(	162	,	5029	,	121	,	0	),
(	163	,	6200	,	149	,	0	),
(	164	,	6156	,	146	,	0	),
(	165	,	6156	,	147	,	0	),
(	166	,	6185	,	147	,	0	),
(	167	,	6158	,	147	,	0	),
(	169	,	6143	,	163	,	0	),
(	170	,	6143	,	154	,	0	),
(	171	,	6143	,	164	,	0	),
(	172	,	6143	,	165	,	0	),
(	173	,	6143	,	158	,	0	),
(	174	,	6143	,	159	,	0	),
(	175	,	6143	,	160	,	0	),
(	176	,	6143	,	162	,	0	),
(	177	,	6143	,	161	,	0	),
(	178	,	6143	,	157	,	0	),
(	179	,	6143	,	156	,	0	),
(	180	,	6143	,	98	,	0	),
(	181	,	6094	,	122	,	0	),
(	182	,	6104	,	122	,	0	),
(	183	,	6117	,	117	,	0	),
(	184	,	6117	,	120	,	0	),
(	185	,	6117	,	118	,	0	),
(	186	,	6117	,	119	,	0	),
(	187	,	6117	,	122	,	0	),
(	188	,	5029	,	114	,	0	),
(	189	,	5029	,	116	,	0	),
(	190	,	5022	,	96	,	0	),
(	191	,	5022	,	115	,	0	),
(	192	,	5033	,	96	,	0	),
(	193	,	5033	,	115	,	0	),
(	194	,	5026	,	96	,	0	),
(	195	,	5026	,	115	,	0	),
(	196	,	5040	,	155	,	0	),
(	197	,	6188	,	94	,	0	),
(	198	,	6188	,	93	,	0	),
(	199	,	6158	,	149	,	0	),
(	200	,	6158	,	148	,	0	),
(	201	,	6185	,	149	,	0	),
(	202	,	6185	,	148	,	0	),
(	203	,	6156	,	149	,	0	),
(	204	,	6156	,	148	,	0	),
(	205	,	6143	,	151	,	0	),
(	206	,	6217	,	112	,	0	),
(	207	,	5009	,	112	,	0	),
(	208	,	6023	,	97	,	0	),
(	209	,	6023	,	111	,	0	),
(	210	,	6023	,	109	,	0	),
(	211	,	6023	,	110	,	0	),
(	212	,	5027	,	112	,	0	),
(	213	,	6101	,	97	,	0	),
(	214	,	6101	,	111	,	0	),
(	215	,	6101	,	109	,	0	),
(	216	,	6101	,	110	,	0	),
(	217	,	5011	,	112	,	0	),
(	218	,	6037	,	97	,	0	),
(	219	,	6037	,	111	,	0	),
(	220	,	6037	,	109	,	0	),
(	221	,	6037	,	110	,	0	),
(	222	,	5007	,	112	,	0	),
(	223	,	6011	,	97	,	0	),
(	224	,	6011	,	111	,	0	),
(	225	,	6011	,	109	,	0	),
(	226	,	6011	,	110	,	0	),
(	227	,	6011	,	113	,	0	),
(	228	,	5006	,	112	,	0	),
(	229	,	6010	,	97	,	0	),
(	230	,	6010	,	111	,	0	),
(	231	,	6010	,	109	,	0	),
(	232	,	6010	,	110	,	0	),
(	233	,	6010	,	113	,	0	),
(	234	,	6226	,	177	,	0	),
(	235	,	6226	,	178	,	0	),
(	236	,	6226	,	179	,	0	),
(	237	,	6226	,	180	,	0	),
(	242	,	6229	,	182	,	0	),
(	243	,	6231	,	183	,	0	),
(	244	,	6229	,	181	,	0	),
(	245	,	4012	,	181	,	0	),
(	246	,	NULL	,	1	,	NULL	),
(	247	,	NULL	,	2	,	NULL	),
(	248	,	NULL	,	3	,	NULL	),
(	249	,	NULL	,	4	,	NULL	),
(	501	,	2000	,	6	,	NULL	),
(	502	,	3000	,	6	,	NULL	),
(	503	,	3000	,	201	,	NULL	),
(	504	,	4000	,	6	,	NULL	),
(	505	,	4000	,	186	,	NULL	),
(	506	,	4000	,	190	,	NULL	),
(	507	,	4000	,	200	,	NULL	),
(	508	,	4000	,	201	,	NULL	),
(	509	,	4001	,	186	,	NULL	),
(	510	,	4001	,	200	,	NULL	),
(	511	,	4001	,	201	,	NULL	),
(	512	,	4012	,	6	,	NULL	),
(	513	,	5000	,	6	,	NULL	),
(	514	,	5000	,	92	,	NULL	),
(	515	,	5000	,	97	,	NULL	),
(	516	,	5000	,	114	,	NULL	),
(	517	,	5000	,	117	,	NULL	),
(	518	,	5000	,	134	,	NULL	),
(	519	,	5000	,	148	,	NULL	),
(	520	,	5000	,	157	,	NULL	),
(	521	,	5000	,	186	,	NULL	),
(	522	,	5000	,	193	,	NULL	),
(	523	,	5000	,	196	,	NULL	),
(	524	,	5000	,	201	,	NULL	),
(	525	,	5000	,	202	,	NULL	),
(	526	,	5000	,	207	,	NULL	),
(	527	,	5000	,	223	,	NULL	),
(	528	,	5000	,	238	,	NULL	),
(	529	,	5000	,	243	,	NULL	),
(	530	,	5002	,	201	,	NULL	),
(	531	,	5003	,	201	,	NULL	),
(	532	,	5005	,	163	,	NULL	),
(	533	,	5005	,	201	,	NULL	),
(	534	,	5006	,	201	,	NULL	),
(	535	,	5006	,	206	,	NULL	),
(	536	,	5007	,	200	,	NULL	),
(	537	,	5007	,	201	,	NULL	),
(	538	,	5007	,	222	,	NULL	),
(	539	,	5008	,	201	,	NULL	),
(	540	,	5009	,	97	,	NULL	),
(	541	,	5009	,	200	,	NULL	),
(	542	,	5009	,	201	,	NULL	),
(	543	,	5012	,	186	,	NULL	),
(	544	,	5014	,	97	,	NULL	),
(	545	,	5014	,	187	,	NULL	),
(	546	,	5014	,	201	,	NULL	),
(	547	,	5014	,	240	,	NULL	),
(	548	,	5016	,	130	,	NULL	),
(	549	,	5016	,	201	,	NULL	),
(	550	,	5016	,	205	,	NULL	),
(	551	,	5017	,	193	,	NULL	),
(	552	,	5017	,	201	,	NULL	),
(	553	,	5018	,	200	,	NULL	),
(	554	,	5018	,	201	,	NULL	),
(	555	,	5022	,	201	,	NULL	),
(	556	,	5023	,	214	,	NULL	),
(	557	,	5025	,	201	,	NULL	),
(	558	,	5027	,	97	,	NULL	),
(	559	,	5027	,	206	,	NULL	),
(	560	,	5027	,	207	,	NULL	),
(	561	,	5028	,	200	,	NULL	),
(	562	,	5028	,	201	,	NULL	),
(	563	,	5028	,	233	,	NULL	),
(	564	,	5029	,	117	,	NULL	),
(	565	,	5029	,	201	,	NULL	),
(	566	,	5029	,	202	,	NULL	),
(	567	,	5030	,	201	,	NULL	),
(	568	,	5031	,	149	,	NULL	),
(	569	,	5031	,	201	,	NULL	),
(	570	,	5031	,	203	,	NULL	),
(	571	,	5032	,	200	,	NULL	),
(	572	,	5032	,	201	,	NULL	),
(	573	,	5033	,	200	,	NULL	),
(	574	,	5033	,	201	,	NULL	),
(	575	,	5035	,	201	,	NULL	),
(	576	,	5035	,	223	,	NULL	),
(	577	,	5035	,	238	,	NULL	),
(	578	,	5035	,	243	,	NULL	),
(	579	,	5037	,	193	,	NULL	),
(	580	,	5037	,	201	,	NULL	),
(	581	,	5039	,	92	,	NULL	),
(	582	,	5039	,	200	,	NULL	),
(	583	,	5039	,	201	,	NULL	),
(	584	,	5040	,	11	,	NULL	),
(	585	,	5040	,	148	,	NULL	),
(	586	,	5040	,	201	,	NULL	),
(	587	,	5043	,	187	,	NULL	),
(	588	,	5043	,	193	,	NULL	),
(	589	,	5043	,	201	,	NULL	),
(	590	,	6007	,	201	,	NULL	),
(	591	,	6009	,	97	,	NULL	),
(	592	,	6009	,	148	,	NULL	),
(	593	,	6009	,	186	,	NULL	),
(	594	,	6009	,	193	,	NULL	),
(	595	,	6009	,	201	,	NULL	),
(	596	,	6010	,	200	,	NULL	),
(	597	,	6010	,	201	,	NULL	),
(	598	,	6010	,	206	,	NULL	),
(	599	,	6011	,	148	,	NULL	),
(	600	,	6011	,	188	,	NULL	),
(	601	,	6011	,	193	,	NULL	),
(	602	,	6011	,	195	,	NULL	),
(	603	,	6011	,	200	,	NULL	),
(	604	,	6011	,	201	,	NULL	),
(	605	,	6011	,	206	,	NULL	),
(	606	,	6011	,	220	,	NULL	),
(	607	,	6011	,	221	,	NULL	),
(	608	,	6011	,	222	,	NULL	),
(	609	,	6011	,	227	,	NULL	),
(	610	,	6011	,	233	,	NULL	),
(	611	,	6023	,	186	,	NULL	),
(	612	,	6023	,	193	,	NULL	),
(	613	,	6023	,	201	,	NULL	),
(	614	,	6023	,	240	,	NULL	),
(	615	,	6026	,	201	,	NULL	),
(	616	,	6032	,	201	,	NULL	),
(	617	,	6033	,	117	,	NULL	),
(	618	,	6033	,	148	,	NULL	),
(	619	,	6033	,	186	,	NULL	),
(	620	,	6033	,	190	,	NULL	),
(	621	,	6033	,	191	,	NULL	),
(	622	,	6033	,	192	,	NULL	),
(	623	,	6033	,	200	,	NULL	),
(	624	,	6033	,	201	,	NULL	),
(	625	,	6033	,	202	,	NULL	),
(	626	,	6036	,	200	,	NULL	),
(	627	,	6036	,	201	,	NULL	),
(	628	,	6036	,	218	,	NULL	),
(	629	,	6038	,	194	,	NULL	),
(	630	,	6038	,	196	,	NULL	),
(	631	,	6038	,	200	,	NULL	),
(	632	,	6038	,	201	,	NULL	),
(	633	,	6038	,	207	,	NULL	),
(	634	,	6038	,	232	,	NULL	),
(	635	,	6038	,	233	,	NULL	),
(	636	,	6040	,	200	,	NULL	),
(	637	,	6040	,	201	,	NULL	),
(	638	,	6044	,	200	,	NULL	),
(	639	,	6044	,	201	,	NULL	),
(	640	,	6044	,	207	,	NULL	),
(	641	,	6049	,	201	,	NULL	),
(	642	,	6050	,	201	,	NULL	),
(	643	,	6052	,	147	,	NULL	),
(	644	,	6052	,	200	,	NULL	),
(	645	,	6052	,	201	,	NULL	),
(	646	,	6052	,	205	,	NULL	),
(	647	,	6052	,	209	,	NULL	),
(	648	,	6052	,	230	,	NULL	),
(	649	,	6058	,	188	,	NULL	),
(	650	,	6058	,	201	,	NULL	),
(	651	,	6058	,	202	,	NULL	),
(	652	,	6064	,	201	,	NULL	),
(	653	,	6067	,	117	,	NULL	),
(	654	,	6069	,	97	,	NULL	),
(	655	,	6069	,	187	,	NULL	),
(	656	,	6069	,	201	,	NULL	),
(	657	,	6091	,	201	,	NULL	),
(	658	,	6094	,	129	,	NULL	),
(	659	,	6094	,	186	,	NULL	),
(	660	,	6094	,	201	,	NULL	),
(	661	,	6094	,	202	,	NULL	),
(	662	,	6095	,	148	,	NULL	),
(	663	,	6095	,	201	,	NULL	),
(	664	,	6095	,	214	,	NULL	),
(	665	,	6095	,	241	,	NULL	),
(	666	,	6101	,	186	,	NULL	),
(	667	,	6101	,	193	,	NULL	),
(	668	,	6101	,	201	,	NULL	),
(	669	,	6101	,	206	,	NULL	),
(	670	,	6101	,	207	,	NULL	),
(	671	,	6101	,	227	,	NULL	),
(	672	,	6102	,	201	,	NULL	),
(	673	,	6102	,	213	,	NULL	),
(	674	,	6102	,	214	,	NULL	),
(	675	,	6104	,	116	,	NULL	),
(	676	,	6104	,	200	,	NULL	),
(	677	,	6104	,	201	,	NULL	),
(	678	,	6104	,	202	,	NULL	),
(	679	,	6117	,	186	,	NULL	),
(	680	,	6117	,	200	,	NULL	),
(	681	,	6117	,	201	,	NULL	),
(	682	,	6117	,	202	,	NULL	),
(	683	,	6118	,	123	,	NULL	),
(	684	,	6118	,	124	,	NULL	),
(	685	,	6118	,	125	,	NULL	),
(	686	,	6118	,	128	,	NULL	),
(	687	,	6118	,	129	,	NULL	),
(	688	,	6118	,	201	,	NULL	),
(	689	,	6129	,	31	,	NULL	),
(	690	,	6129	,	129	,	NULL	),
(	691	,	6129	,	200	,	NULL	),
(	692	,	6129	,	201	,	NULL	),
(	693	,	6131	,	158	,	NULL	),
(	694	,	6131	,	162	,	NULL	),
(	695	,	6131	,	163	,	NULL	),
(	696	,	6131	,	200	,	NULL	),
(	697	,	6131	,	201	,	NULL	),
(	698	,	6134	,	22	,	NULL	),
(	699	,	6134	,	185	,	NULL	),
(	700	,	6134	,	200	,	NULL	),
(	701	,	6134	,	201	,	NULL	),
(	702	,	6136	,	163	,	NULL	),
(	703	,	6136	,	201	,	NULL	),
(	704	,	6143	,	200	,	NULL	),
(	705	,	6143	,	201	,	NULL	),
(	706	,	6143	,	207	,	NULL	),
(	707	,	6143	,	234	,	NULL	),
(	708	,	6150	,	129	,	NULL	),
(	709	,	6150	,	201	,	NULL	),
(	710	,	6154	,	201	,	NULL	),
(	711	,	6156	,	200	,	NULL	),
(	712	,	6156	,	201	,	NULL	),
(	713	,	6156	,	203	,	NULL	),
(	714	,	6156	,	209	,	NULL	),
(	715	,	6156	,	229	,	NULL	),
(	716	,	6158	,	201	,	NULL	),
(	717	,	6171	,	201	,	NULL	),
(	718	,	6174	,	193	,	NULL	),
(	719	,	6174	,	200	,	NULL	),
(	720	,	6175	,	200	,	NULL	),
(	721	,	6176	,	200	,	NULL	),
(	722	,	6176	,	201	,	NULL	),
(	723	,	6177	,	201	,	NULL	),
(	724	,	6177	,	207	,	NULL	),
(	725	,	6182	,	200	,	NULL	),
(	726	,	6182	,	201	,	NULL	),
(	727	,	6182	,	207	,	NULL	),
(	728	,	6183	,	200	,	NULL	),
(	729	,	6185	,	22	,	NULL	),
(	730	,	6185	,	200	,	NULL	),
(	731	,	6185	,	239	,	NULL	),
(	732	,	6198	,	201	,	NULL	),
(	733	,	6200	,	201	,	NULL	),
(	734	,	6211	,	114	,	NULL	),
(	735	,	6211	,	117	,	NULL	),
(	736	,	6211	,	148	,	NULL	),
(	737	,	6211	,	149	,	NULL	),
(	738	,	6211	,	186	,	NULL	),
(	739	,	6211	,	190	,	NULL	),
(	740	,	6211	,	200	,	NULL	),
(	741	,	6211	,	201	,	NULL	),
(	742	,	6211	,	202	,	NULL	),
(	743	,	6211	,	203	,	NULL	),
(	744	,	6211	,	204	,	NULL	),
(	745	,	6211	,	221	,	NULL	),
(	746	,	6211	,	227	,	NULL	),
(	747	,	6214	,	201	,	NULL	),
(	748	,	NULL	,	260	,	NULL	),
(	749	,	NULL	,	261	,	NULL	),

(	750	,	6493	,	262	,	NULL	),
(	751 ,	6493	,	263	,	NULL	),
(	752 ,	6493	,	264	,	NULL	),
(	753 ,	6493	,	265	,	NULL	),
(	754 ,	6493	,	266	,	NULL	),
(	755 ,	6493	,	267	,	NULL	),
(	756 ,	6493	,	268	,	NULL	),
(	757 ,	6493	,	269	,	NULL	),
(	758 ,	6493	,	270	,	NULL	),
(	759 ,	6493	,	271	,	NULL	),
(	760 ,	6493	,	272	,	NULL	),
(	761 ,	6493	,	273	,	NULL	),
(	762 ,	6493	,	274	,	NULL	),
(	763 ,	6493	,	275	,	NULL	),

(	764	,	6494	,	262	,	NULL	),
(	765 ,	6494	,	263	,	NULL	),
(	766 ,	6494	,	264	,	NULL	),
(	767 ,	6494	,	265	,	NULL	),
(	768 ,	6494	,	266	,	NULL	),
(	769 ,	6494	,	267	,	NULL	),
(	770 ,	6494	,	268	,	NULL	),
(	771 ,	6494	,	269	,	NULL	),
(	772 ,	6494	,	270	,	NULL	),
(	773 ,	6494	,	271	,	NULL	),
(	774 ,	6494	,	272	,	NULL	),
(	775 ,	6494	,	273	,	NULL	),
(	776 ,	6494	,	274	,	NULL	),
(	777 ,	6494	,	275	,	NULL	),

(	778	,	6495	,	262	,	NULL	),
(	779 ,	6495	,	263	,	NULL	),
(	780 ,	6495	,	264	,	NULL	),
(	781 ,	6495	,	265	,	NULL	),
(	782 ,	6495	,	266	,	NULL	),
(	783 ,	6495	,	267	,	NULL	),
(	784 ,	6495	,	268	,	NULL	),
(	785 ,	6495	,	269	,	NULL	),
(	786 ,	6495	,	270	,	NULL	),
(	787 ,	6495	,	271	,	NULL	),
(	788 ,	6495	,	272	,	NULL	),
(	789 ,	6495	,	273	,	NULL	),
(	790 ,	6495	,	274	,	NULL	),
(	791 ,	6495	,	275	,	NULL	),

(	792	,	6496	,	262	,	NULL	),
(	793 ,	6496	,	263	,	NULL	),
(	794 ,	6496	,	264	,	NULL	),
(	795 ,	6496	,	265	,	NULL	),
(	796 ,	6496	,	266	,	NULL	),
(	797 ,	6496	,	267	,	NULL	),
(	798 ,	6496	,	268	,	NULL	),
(	799 ,	6496	,	269	,	NULL	),
(	800 ,	6496	,	270	,	NULL	),
(	801 ,	6496	,	271	,	NULL	),
(	802 ,	6496	,	272	,	NULL	),
(	803 ,	6496	,	273	,	NULL	),
(	804 ,	6496	,	274	,	NULL	),
(	805 ,	6496	,	275	,	NULL	),

(	806	,	6497	,	262	,	NULL	),
(	807 ,	6497	,	263	,	NULL	),
(	808 ,	6497	,	264	,	NULL	),
(	809 ,	6497	,	265	,	NULL	),
(	810 ,	6497	,	266	,	NULL	),
(	811 ,	6497	,	267	,	NULL	),
(	812 ,	6497	,	268	,	NULL	),
(	813 ,	6497	,	269	,	NULL	),
(	814 ,	6497	,	270	,	NULL	),
(	815 ,	6497	,	271	,	NULL	),
(	816 ,	6497	,	272	,	NULL	),
(	817 ,	6497	,	273	,	NULL	),
(	818 ,	6497	,	274	,	NULL	),
(	819 ,	6497	,	275	,	NULL	),

(	820	,	6498	,	262	,	NULL	),
(	821 ,	6498	,	263	,	NULL	),
(	822 ,	6498	,	264	,	NULL	),
(	823 ,	6498	,	265	,	NULL	),
(	824 ,	6498	,	266	,	NULL	),
(	825 ,	6498	,	267	,	NULL	),
(	826 ,	6498	,	268	,	NULL	),
(	827 ,	6498	,	269	,	NULL	),
(	828 ,	6498	,	270	,	NULL	),
(	829 ,	6498	,	271	,	NULL	),
(	830 ,	6498	,	272	,	NULL	),
(	831 ,	6498	,	273	,	NULL	),
(	832 ,	6498	,	274	,	NULL	),
(	833 ,	6498	,	275	,	NULL	),

(	834	,	6499	,	262	,	NULL	),
(	835 ,	6499	,	263	,	NULL	),
(	836 ,	6499	,	264	,	NULL	),
(	837 ,	6499	,	265	,	NULL	),
(	838 ,	6499	,	266	,	NULL	),
(	839 ,	6499	,	267	,	NULL	),
(	840 ,	6499	,	268	,	NULL	),
(	841 ,	6499	,	269	,	NULL	),
(	842 ,	6499	,	270	,	NULL	),
(	843 ,	6499	,	271	,	NULL	),
(	844 ,	6499	,	272	,	NULL	),
(	845 ,	6499	,	273	,	NULL	),
(	846 ,	6499	,	274	,	NULL	),
(	847 ,	6499	,	275	,	NULL	),

(	848	,	6500	,	262	,	NULL	),
(	849 ,	6500	,	263	,	NULL	),
(	850 ,	6500	,	264	,	NULL	),
(	851 ,	6500	,	265	,	NULL	),
(	852 ,	6500	,	266	,	NULL	),
(	853 ,	6500	,	267	,	NULL	),
(	854 ,	6500	,	268	,	NULL	),
(	855 ,	6500	,	269	,	NULL	),
(	856 ,	6500	,	270	,	NULL	),
(	857 ,	6500	,	271	,	NULL	),
(	858 ,	6500	,	272	,	NULL	),
(	859 ,	6500	,	273	,	NULL	),
(	860 ,	6500	,	274	,	NULL	),
(	861 ,	6500	,	275	,	NULL	),

(	862	,	6501	,	262	,	NULL	),
(	863 ,	6501	,	263	,	NULL	),
(	864 ,	6501	,	264	,	NULL	),
(	865 ,	6501	,	265	,	NULL	),
(	866 ,	6501	,	266	,	NULL	),
(	867 ,	6501	,	267	,	NULL	),
(	868 ,	6501	,	268	,	NULL	),
(	869 ,	6501	,	269	,	NULL	),
(	870 ,	6501	,	270	,	NULL	),
(	871 ,	6501	,	271	,	NULL	),
(	872 ,	6501	,	272	,	NULL	),
(	873 ,	6501	,	273	,	NULL	),
(	874 ,	6501	,	274	,	NULL	),
(	875 ,	6501	,	275	,	NULL	),

(	876	,	6502	,	262	,	NULL	),
(	877 ,	6502	,	263	,	NULL	),
(	878 ,	6502	,	264	,	NULL	),
(	879 ,	6502	,	265	,	NULL	),
(	880 ,	6502	,	266	,	NULL	),
(	881 ,	6502	,	267	,	NULL	),
(	882 ,	6502	,	268	,	NULL	),
(	883 ,	6502	,	269	,	NULL	),
(	884 ,	6502	,	270	,	NULL	),
(	885 ,	6502	,	271	,	NULL	),
(	886 ,	6502	,	272	,	NULL	),
(	887 ,	6502	,	273	,	NULL	),
(	888 ,	6502	,	274	,	NULL	),
(	889 ,	6502	,	275	,	NULL	),

(	890	,	6503	,	262	,	NULL	),
(	891 ,	6503	,	263	,	NULL	),
(	892 ,	6503	,	264	,	NULL	),
(	893 ,	6503	,	265	,	NULL	),
(	894 ,	6503	,	266	,	NULL	),
(	895 ,	6503	,	267	,	NULL	),
(	896 ,	6503	,	268	,	NULL	),
(	897 ,	6503	,	269	,	NULL	),
(	898	,	6503	,	270	,	NULL	),
(	899 ,	6503	,	271	,	NULL	),
(	900 ,	6503	,	272	,	NULL	),
(	901 ,	6503	,	273	,	NULL	),
(	902 ,	6503	,	274	,	NULL	),
(	903 ,	6503	,	275	,	NULL	),

(	904	,	6437	,	262	,	NULL	)	,
(	905	,	6438	,	262	,	NULL	)	,
(	906	,	6439	,	262	,	NULL	)	,
(	907	,	6440	,	262	,	NULL	)	,
(	908	,	6455	,	262	,	NULL	)	,
(	909	,	6456	,	262	,	NULL	)	,
(	910	,	6458	,	262	,	NULL	)	,
(	911	,	6459	,	262	,	NULL	)	,
(	912	,	6509	,	262	,	NULL	)	,
(	913	,	6510	,	262	,	NULL	)	,
(	914	,	6511	,	262	,	NULL	)	,
(	915	,	6512	,	262	,	NULL	)	,
(	916	,	6513	,	262	,	NULL	)	,
(	917	,	6514	,	262	,	NULL	)	,
(	918	,	6515	,	262	,	NULL	)	,
(	919	,	6516	,	262	,	NULL	)	,
(	920	,	6517	,	262	,	NULL	)	,
(	921	,	6518	,	262	,	NULL	)	,
(	922	,	6519	,	262	,	NULL	)	,
(	923	,	6520	,	262	,	NULL	)	,
(	924	,	6521	,	262	,	NULL	)	,
(	925	,	6522	,	262	,	NULL	)	,
(	926	,	6523	,	262	,	NULL	)	,
(	927	,	6524	,	262	,	NULL	)	,
(	928	,	6525	,	262	,	NULL	)	,
(	929	,	6526	,	262	,	NULL	)	,
(	930	,	6527	,	262	,	NULL	)	,
(	931	,	6528	,	262	,	NULL	)	,
(	932	,	6529	,	262	,	NULL	)	,
(	933	,	6530	,	262	,	NULL	)	,
(	934	,	6531	,	262	,	NULL	)	,
(	935	,	6532	,	262	,	NULL	)	,
(	936	,	6533	,	262	,	NULL	)	,
(	937	,	6534	,	262	,	NULL	)	,
(	938	,	6535	,	262	,	NULL	)	,
(	939	,	6536	,	262	,	NULL	)	,
(	940	,	6537	,	262	,	NULL	)	,
(	941	,	6538	,	262	,	NULL	)	,
(	942	,	6539	,	262	,	NULL	)	,
(	943	,	6540	,	262	,	NULL	)	,
(	944	,	6541	,	262	,	NULL	)	,
(	945	,	6542	,	262	,	NULL	)	,
(	946	,	6543	,	262	,	NULL	)	,

(	947	,	6437	,	263	,	NULL	)	,
(	948	,	6438	,	263	,	NULL	)	,
(	949	,	6439	,	263	,	NULL	)	,
(	950	,	6440	,	263	,	NULL	)	,
(	951	,	6455	,	263	,	NULL	)	,
(	952	,	6456	,	263	,	NULL	)	,
(	953	,	6458	,	263	,	NULL	)	,
(	954	,	6459	,	263	,	NULL	)	,
(	955	,	6509	,	263	,	NULL	)	,
(	956	,	6510	,	263	,	NULL	)	,
(	957	,	6511	,	263	,	NULL	)	,
(	958	,	6512	,	263	,	NULL	)	,
(	959	,	6513	,	263	,	NULL	)	,
(	960	,	6514	,	263	,	NULL	)	,
(	961	,	6515	,	263	,	NULL	)	,
(	962	,	6516	,	263	,	NULL	)	,
(	963	,	6517	,	263	,	NULL	)	,
(	964	,	6518	,	263	,	NULL	)	,
(	965	,	6519	,	263	,	NULL	)	,
(	966	,	6520	,	263	,	NULL	)	,
(	967	,	6521	,	263	,	NULL	)	,
(	968	,	6522	,	263	,	NULL	)	,
(	969	,	6523	,	263	,	NULL	)	,
(	970	,	6524	,	263	,	NULL	)	,
(	971	,	6525	,	263	,	NULL	)	,
(	972	,	6526	,	263	,	NULL	)	,
(	973	,	6527	,	263	,	NULL	)	,
(	974	,	6528	,	263	,	NULL	)	,
(	975	,	6529	,	263	,	NULL	)	,
(	976	,	6530	,	263	,	NULL	)	,
(	977	,	6531	,	263	,	NULL	)	,
(	978	,	6532	,	263	,	NULL	)	,
(	979	,	6533	,	263	,	NULL	)	,
(	980	,	6534	,	263	,	NULL	)	,
(	981	,	6535	,	263	,	NULL	)	,
(	982	,	6536	,	263	,	NULL	)	,
(	983	,	6537	,	263	,	NULL	)	,
(	984	,	6538	,	263	,	NULL	)	,
(	985	,	6539	,	263	,	NULL	)	,
(	986	,	6540	,	263	,	NULL	)	,
(	987	,	6541	,	263	,	NULL	)	,
(	988	,	6542	,	263	,	NULL	)	,
(	989	,	6543	,	263	,	NULL	)	,

(	990	,	6437	,	264	,	NULL	)	,
(	991	,	6438	,	264	,	NULL	)	,
(	992	,	6439	,	264	,	NULL	)	,
(	993	,	6440	,	264	,	NULL	)	,
(	994	,	6455	,	264	,	NULL	)	,
(	995	,	6456	,	264	,	NULL	)	,
(	996	,	6458	,	264	,	NULL	)	,
(	997	,	6459	,	264	,	NULL	)	,
(	998	,	6509	,	264	,	NULL	)	,
(	999	,	6510	,	264	,	NULL	)	,
(	1000	,	6511	,	264	,	NULL	)	,
(	1001	,	6512	,	264	,	NULL	)	,
(	1002	,	6513	,	264	,	NULL	)	,
(	1003	,	6514	,	264	,	NULL	)	,
(	1004	,	6515	,	264	,	NULL	)	,
(	1005	,	6516	,	264	,	NULL	)	,
(	1006	,	6517	,	264	,	NULL	)	,
(	1007	,	6518	,	264	,	NULL	)	,
(	1008	,	6519	,	264	,	NULL	)	,
(	1009	,	6520	,	264	,	NULL	)	,
(	1010	,	6521	,	264	,	NULL	)	,
(	1011	,	6522	,	264	,	NULL	)	,
(	1012	,	6523	,	264	,	NULL	)	,
(	1013	,	6524	,	264	,	NULL	)	,
(	1014	,	6525	,	264	,	NULL	)	,
(	1015	,	6526	,	264	,	NULL	)	,
(	1016	,	6527	,	264	,	NULL	)	,
(	1017	,	6528	,	264	,	NULL	)	,
(	1018	,	6529	,	264	,	NULL	)	,
(	1019	,	6530	,	264	,	NULL	)	,
(	1020	,	6531	,	264	,	NULL	)	,
(	1021	,	6532	,	264	,	NULL	)	,
(	1022	,	6533	,	264	,	NULL	)	,
(	1023	,	6534	,	264	,	NULL	)	,
(	1024	,	6535	,	264	,	NULL	)	,
(	1025	,	6536	,	264	,	NULL	)	,
(	1026	,	6537	,	264	,	NULL	)	,
(	1027	,	6538	,	264	,	NULL	)	,
(	1028	,	6539	,	264	,	NULL	)	,
(	1029	,	6540	,	264	,	NULL	)	,
(	1030	,	6541	,	264	,	NULL	)	,
(	1031	,	6542	,	264	,	NULL	)	,
(	1032	,	6543	,	264	,	NULL	)	,

(	1033	,	6437	,	265	,	NULL	)	,
(	1034	,	6438	,	265	,	NULL	)	,
(	1035	,	6439	,	265	,	NULL	)	,
(	1036	,	6440	,	265	,	NULL	)	,
(	1037	,	6455	,	265	,	NULL	)	,
(	1038	,	6456	,	265	,	NULL	)	,
(	1039	,	6458	,	265	,	NULL	)	,
(	1040	,	6459	,	265	,	NULL	)	,
(	1041	,	6509	,	265	,	NULL	)	,
(	1042	,	6510	,	265	,	NULL	)	,
(	1043	,	6511	,	265	,	NULL	)	,
(	1044	,	6512	,	265	,	NULL	)	,
(	1045	,	6513	,	265	,	NULL	)	,
(	1046	,	6514	,	265	,	NULL	)	,
(	1047	,	6515	,	265	,	NULL	)	,
(	1048	,	6516	,	265	,	NULL	)	,
(	1049	,	6517	,	265	,	NULL	)	,
(	1050	,	6518	,	265	,	NULL	)	,
(	1051	,	6519	,	265	,	NULL	)	,
(	1052	,	6520	,	265	,	NULL	)	,
(	1053	,	6521	,	265	,	NULL	)	,
(	1054	,	6522	,	265	,	NULL	)	,
(	1055	,	6523	,	265	,	NULL	)	,
(	1056	,	6524	,	265	,	NULL	)	,
(	1057	,	6525	,	265	,	NULL	)	,
(	1058	,	6526	,	265	,	NULL	)	,
(	1059	,	6527	,	265	,	NULL	)	,
(	1060	,	6528	,	265	,	NULL	)	,
(	1061	,	6529	,	265	,	NULL	)	,
(	1062	,	6530	,	265	,	NULL	)	,
(	1063	,	6531	,	265	,	NULL	)	,
(	1064	,	6532	,	265	,	NULL	)	,
(	1065	,	6533	,	265	,	NULL	)	,
(	1066	,	6534	,	265	,	NULL	)	,
(	1067	,	6535	,	265	,	NULL	)	,
(	1068	,	6536	,	265	,	NULL	)	,
(	1069	,	6537	,	265	,	NULL	)	,
(	1070	,	6538	,	265	,	NULL	)	,
(	1071	,	6539	,	265	,	NULL	)	,
(	1072	,	6540	,	265	,	NULL	)	,
(	1073	,	6541	,	265	,	NULL	)	,
(	1074	,	6542	,	265	,	NULL	)	,
(	1075	,	6543	,	265	,	NULL	)	,

(	1076	,	6437	,	266	,	NULL	)	,
(	1077	,	6438	,	266	,	NULL	)	,
(	1078	,	6439	,	266	,	NULL	)	,
(	1079	,	6440	,	266	,	NULL	)	,
(	1080	,	6455	,	266	,	NULL	)	,
(	1081	,	6456	,	266	,	NULL	)	,
(	1082	,	6458	,	266	,	NULL	)	,
(	1083	,	6459	,	266	,	NULL	)	,
(	1084	,	6509	,	266	,	NULL	)	,
(	1085	,	6510	,	266	,	NULL	)	,
(	1086	,	6511	,	266	,	NULL	)	,
(	1087	,	6512	,	266	,	NULL	)	,
(	1088	,	6513	,	266	,	NULL	)	,
(	1089	,	6514	,	266	,	NULL	)	,
(	1090	,	6515	,	266	,	NULL	)	,
(	1091	,	6516	,	266	,	NULL	)	,
(	1092	,	6517	,	266	,	NULL	)	,
(	1093	,	6518	,	266	,	NULL	)	,
(	1094	,	6519	,	266	,	NULL	)	,
(	1095	,	6520	,	266	,	NULL	)	,
(	1096	,	6521	,	266	,	NULL	)	,
(	1097	,	6522	,	266	,	NULL	)	,
(	1098	,	6523	,	266	,	NULL	)	,
(	1099	,	6524	,	266	,	NULL	)	,
(	1100	,	6525	,	266	,	NULL	)	,
(	1101	,	6526	,	266	,	NULL	)	,
(	1102	,	6527	,	266	,	NULL	)	,
(	1103	,	6528	,	266	,	NULL	)	,
(	1104	,	6529	,	266	,	NULL	)	,
(	1105	,	6530	,	266	,	NULL	)	,
(	1106	,	6531	,	266	,	NULL	)	,
(	1107	,	6532	,	266	,	NULL	)	,
(	1108	,	6533	,	266	,	NULL	)	,
(	1109	,	6534	,	266	,	NULL	)	,
(	1110	,	6535	,	266	,	NULL	)	,
(	1111	,	6536	,	266	,	NULL	)	,
(	1112	,	6537	,	266	,	NULL	)	,
(	1113	,	6538	,	266	,	NULL	)	,
(	1114	,	6539	,	266	,	NULL	)	,
(	1115	,	6540	,	266	,	NULL	)	,
(	1116	,	6541	,	266	,	NULL	)	,
(	1117	,	6542	,	266	,	NULL	)	,
(	1118	,	6543	,	266	,	NULL	)	,

(	1119	,	6437	,	267	,	NULL	)	,
(	1120	,	6438	,	267	,	NULL	)	,
(	1121	,	6439	,	267	,	NULL	)	,
(	1122	,	6440	,	267	,	NULL	)	,
(	1123	,	6455	,	267	,	NULL	)	,
(	1124	,	6456	,	267	,	NULL	)	,
(	1125	,	6458	,	267	,	NULL	)	,
(	1126	,	6459	,	267	,	NULL	)	,
(	1127	,	6509	,	267	,	NULL	)	,
(	1128	,	6510	,	267	,	NULL	)	,
(	1129	,	6511	,	267	,	NULL	)	,
(	1130	,	6512	,	267	,	NULL	)	,
(	1131	,	6513	,	267	,	NULL	)	,
(	1132	,	6514	,	267	,	NULL	)	,
(	1133	,	6515	,	267	,	NULL	)	,
(	1134	,	6516	,	267	,	NULL	)	,
(	1135	,	6517	,	267	,	NULL	)	,
(	1136	,	6518	,	267	,	NULL	)	,
(	1137	,	6519	,	267	,	NULL	)	,
(	1138	,	6520	,	267	,	NULL	)	,
(	1139	,	6521	,	267	,	NULL	)	,
(	1140	,	6522	,	267	,	NULL	)	,
(	1141	,	6523	,	267	,	NULL	)	,
(	1142	,	6524	,	267	,	NULL	)	,
(	1143	,	6525	,	267	,	NULL	)	,
(	1144	,	6526	,	267	,	NULL	)	,
(	1145	,	6527	,	267	,	NULL	)	,
(	1146	,	6528	,	267	,	NULL	)	,
(	1147	,	6529	,	267	,	NULL	)	,
(	1148	,	6530	,	267	,	NULL	)	,
(	1149	,	6531	,	267	,	NULL	)	,
(	1150	,	6532	,	267	,	NULL	)	,
(	1151	,	6533	,	267	,	NULL	)	,
(	1152	,	6534	,	267	,	NULL	)	,
(	1153	,	6535	,	267	,	NULL	)	,
(	1154	,	6536	,	267	,	NULL	)	,
(	1155	,	6537	,	267	,	NULL	)	,
(	1156	,	6538	,	267	,	NULL	)	,
(	1157	,	6539	,	267	,	NULL	)	,
(	1158	,	6540	,	267	,	NULL	)	,
(	1159	,	6541	,	267	,	NULL	)	,
(	1160	,	6542	,	267	,	NULL	)	,
(	1161	,	6543	,	267	,	NULL	)	,

(	1162	,	6437	,	268	,	NULL	)	,
(	1163	,	6438	,	268	,	NULL	)	,
(	1164	,	6439	,	268	,	NULL	)	,
(	1165	,	6440	,	268	,	NULL	)	,
(	1166	,	6455	,	268	,	NULL	)	,
(	1167	,	6456	,	268	,	NULL	)	,
(	1168	,	6458	,	268	,	NULL	)	,
(	1169	,	6459	,	268	,	NULL	)	,
(	1170	,	6509	,	268	,	NULL	)	,
(	1171	,	6510	,	268	,	NULL	)	,
(	1172	,	6511	,	268	,	NULL	)	,
(	1173	,	6512	,	268	,	NULL	)	,
(	1174	,	6513	,	268	,	NULL	)	,
(	1175	,	6514	,	268	,	NULL	)	,
(	1176	,	6515	,	268	,	NULL	)	,
(	1177	,	6516	,	268	,	NULL	)	,
(	1178	,	6517	,	268	,	NULL	)	,
(	1179	,	6518	,	268	,	NULL	)	,
(	1180	,	6519	,	268	,	NULL	)	,
(	1181	,	6520	,	268	,	NULL	)	,
(	1182	,	6521	,	268	,	NULL	)	,
(	1183	,	6522	,	268	,	NULL	)	,
(	1184	,	6523	,	268	,	NULL	)	,
(	1185	,	6524	,	268	,	NULL	)	,
(	1186	,	6525	,	268	,	NULL	)	,
(	1187	,	6526	,	268	,	NULL	)	,
(	1188	,	6527	,	268	,	NULL	)	,
(	1189	,	6528	,	268	,	NULL	)	,
(	1190	,	6529	,	268	,	NULL	)	,
(	1191	,	6530	,	268	,	NULL	)	,
(	1192	,	6531	,	268	,	NULL	)	,
(	1193	,	6532	,	268	,	NULL	)	,
(	1194	,	6533	,	268	,	NULL	)	,
(	1195	,	6534	,	268	,	NULL	)	,
(	1196	,	6535	,	268	,	NULL	)	,
(	1197	,	6536	,	268	,	NULL	)	,
(	1198	,	6537	,	268	,	NULL	)	,
(	1199	,	6538	,	268	,	NULL	)	,
(	1200	,	6539	,	268	,	NULL	)	,
(	1201	,	6540	,	268	,	NULL	)	,
(	1202	,	6541	,	268	,	NULL	)	,
(	1203	,	6542	,	268	,	NULL	)	,
(	1204	,	6543	,	268	,	NULL	)	,

(	1205	,	6437	,	269	,	NULL	)	,
(	1206	,	6438	,	269	,	NULL	)	,
(	1207	,	6439	,	269	,	NULL	)	,
(	1208	,	6440	,	269	,	NULL	)	,
(	1209	,	6455	,	269	,	NULL	)	,
(	1210	,	6456	,	269	,	NULL	)	,
(	1211	,	6458	,	269	,	NULL	)	,
(	1212	,	6459	,	269	,	NULL	)	,
(	1213	,	6509	,	269	,	NULL	)	,
(	1214	,	6510	,	269	,	NULL	)	,
(	1215	,	6511	,	269	,	NULL	)	,
(	1216	,	6512	,	269	,	NULL	)	,
(	1217	,	6513	,	269	,	NULL	)	,
(	1218	,	6514	,	269	,	NULL	)	,
(	1219	,	6515	,	269	,	NULL	)	,
(	1220	,	6516	,	269	,	NULL	)	,
(	1221	,	6517	,	269	,	NULL	)	,
(	1222	,	6518	,	269	,	NULL	)	,
(	1223	,	6519	,	269	,	NULL	)	,
(	1224	,	6520	,	269	,	NULL	)	,
(	1225	,	6521	,	269	,	NULL	)	,
(	1226	,	6522	,	269	,	NULL	)	,
(	1227	,	6523	,	269	,	NULL	)	,
(	1228	,	6524	,	269	,	NULL	)	,
(	1229	,	6525	,	269	,	NULL	)	,
(	1230	,	6526	,	269	,	NULL	)	,
(	1231	,	6527	,	269	,	NULL	)	,
(	1232	,	6528	,	269	,	NULL	)	,
(	1233	,	6529	,	269	,	NULL	)	,
(	1234	,	6530	,	269	,	NULL	)	,
(	1235	,	6531	,	269	,	NULL	)	,
(	1236	,	6532	,	269	,	NULL	)	,
(	1237	,	6533	,	269	,	NULL	)	,
(	1238	,	6534	,	269	,	NULL	)	,
(	1239	,	6535	,	269	,	NULL	)	,
(	1240	,	6536	,	269	,	NULL	)	,
(	1241	,	6537	,	269	,	NULL	)	,
(	1242	,	6538	,	269	,	NULL	)	,
(	1243	,	6539	,	269	,	NULL	)	,
(	1244	,	6540	,	269	,	NULL	)	,
(	1245	,	6541	,	269	,	NULL	)	,
(	1246	,	6542	,	269	,	NULL	)	,
(	1247	,	6543	,	269	,	NULL	)	,

(	1248	,	6437	,	270	,	NULL	)	,
(	1249	,	6438	,	270	,	NULL	)	,
(	1250	,	6439	,	270	,	NULL	)	,
(	1251	,	6440	,	270	,	NULL	)	,
(	1252	,	6455	,	270	,	NULL	)	,
(	1253	,	6456	,	270	,	NULL	)	,
(	1254	,	6458	,	270	,	NULL	)	,
(	1255	,	6459	,	270	,	NULL	)	,
(	1256	,	6509	,	270	,	NULL	)	,
(	1257	,	6510	,	270	,	NULL	)	,
(	1258	,	6511	,	270	,	NULL	)	,
(	1259	,	6512	,	270	,	NULL	)	,
(	1260	,	6513	,	270	,	NULL	)	,
(	1261	,	6514	,	270	,	NULL	)	,
(	1262	,	6515	,	270	,	NULL	)	,
(	1263	,	6516	,	270	,	NULL	)	,
(	1264	,	6517	,	270	,	NULL	)	,
(	1265	,	6518	,	270	,	NULL	)	,
(	1266	,	6519	,	270	,	NULL	)	,
(	1267	,	6520	,	270	,	NULL	)	,
(	1268	,	6521	,	270	,	NULL	)	,
(	1269	,	6522	,	270	,	NULL	)	,
(	1270	,	6523	,	270	,	NULL	)	,
(	1271	,	6524	,	270	,	NULL	)	,
(	1272	,	6525	,	270	,	NULL	)	,
(	1273	,	6526	,	270	,	NULL	)	,
(	1274	,	6527	,	270	,	NULL	)	,
(	1275	,	6528	,	270	,	NULL	)	,
(	1276	,	6529	,	270	,	NULL	)	,
(	1277	,	6530	,	270	,	NULL	)	,
(	1278	,	6531	,	270	,	NULL	)	,
(	1279	,	6532	,	270	,	NULL	)	,
(	1280	,	6533	,	270	,	NULL	)	,
(	1281	,	6534	,	270	,	NULL	)	,
(	1282	,	6535	,	270	,	NULL	)	,
(	1283	,	6536	,	270	,	NULL	)	,
(	1284	,	6537	,	270	,	NULL	)	,
(	1285	,	6538	,	270	,	NULL	)	,
(	1286	,	6539	,	270	,	NULL	)	,
(	1287	,	6540	,	270	,	NULL	)	,
(	1288	,	6541	,	270	,	NULL	)	,
(	1289	,	6542	,	270	,	NULL	)	,
(	1290	,	6543	,	270	,	NULL	)	,

(	1291	,	6437	,	271	,	NULL	)	,
(	1292	,	6438	,	271	,	NULL	)	,
(	1293	,	6439	,	271	,	NULL	)	,
(	1294	,	6440	,	271	,	NULL	)	,
(	1295	,	6455	,	271	,	NULL	)	,
(	1296	,	6456	,	271	,	NULL	)	,
(	1297	,	6458	,	271	,	NULL	)	,
(	1298	,	6459	,	271	,	NULL	)	,
(	1299	,	6509	,	271	,	NULL	)	,
(	1300	,	6510	,	271	,	NULL	)	,
(	1301	,	6511	,	271	,	NULL	)	,
(	1302	,	6512	,	271	,	NULL	)	,
(	1303	,	6513	,	271	,	NULL	)	,
(	1304	,	6514	,	271	,	NULL	)	,
(	1305	,	6515	,	271	,	NULL	)	,
(	1306	,	6516	,	271	,	NULL	)	,
(	1307	,	6517	,	271	,	NULL	)	,
(	1308	,	6518	,	271	,	NULL	)	,
(	1309	,	6519	,	271	,	NULL	)	,
(	1310	,	6520	,	271	,	NULL	)	,
(	1311	,	6521	,	271	,	NULL	)	,
(	1312	,	6522	,	271	,	NULL	)	,
(	1313	,	6523	,	271	,	NULL	)	,
(	1314	,	6524	,	271	,	NULL	)	,
(	1315	,	6525	,	271	,	NULL	)	,
(	1316	,	6526	,	271	,	NULL	)	,
(	1317	,	6527	,	271	,	NULL	)	,
(	1318	,	6528	,	271	,	NULL	)	,
(	1319	,	6529	,	271	,	NULL	)	,
(	1320	,	6530	,	271	,	NULL	)	,
(	1321	,	6531	,	271	,	NULL	)	,
(	1322	,	6532	,	271	,	NULL	)	,
(	1323	,	6533	,	271	,	NULL	)	,
(	1324	,	6534	,	271	,	NULL	)	,
(	1325	,	6535	,	271	,	NULL	)	,
(	1326	,	6536	,	271	,	NULL	)	,
(	1327	,	6537	,	271	,	NULL	)	,
(	1328	,	6538	,	271	,	NULL	)	,
(	1329	,	6539	,	271	,	NULL	)	,
(	1330	,	6540	,	271	,	NULL	)	,
(	1331	,	6541	,	271	,	NULL	)	,
(	1332	,	6542	,	271	,	NULL	)	,
(	1333	,	6543	,	271	,	NULL	)	,

(	1334	,	6437	,	272	,	NULL	)	,
(	1335	,	6438	,	272	,	NULL	)	,
(	1336	,	6439	,	272	,	NULL	)	,
(	1337	,	6440	,	272	,	NULL	)	,
(	1338	,	6455	,	272	,	NULL	)	,
(	1339	,	6456	,	272	,	NULL	)	,
(	1340	,	6458	,	272	,	NULL	)	,
(	1341	,	6459	,	272	,	NULL	)	,
(	1342	,	6509	,	272	,	NULL	)	,
(	1343	,	6510	,	272	,	NULL	)	,
(	1344	,	6511	,	272	,	NULL	)	,
(	1345	,	6512	,	272	,	NULL	)	,
(	1346	,	6513	,	272	,	NULL	)	,
(	1347	,	6514	,	272	,	NULL	)	,
(	1348	,	6515	,	272	,	NULL	)	,
(	1349	,	6516	,	272	,	NULL	)	,
(	1350	,	6517	,	272	,	NULL	)	,
(	1351	,	6518	,	272	,	NULL	)	,
(	1352	,	6519	,	272	,	NULL	)	,
(	1353	,	6520	,	272	,	NULL	)	,
(	1354	,	6521	,	272	,	NULL	)	,
(	1355	,	6522	,	272	,	NULL	)	,
(	1356	,	6523	,	272	,	NULL	)	,
(	1357	,	6524	,	272	,	NULL	)	,
(	1358	,	6525	,	272	,	NULL	)	,
(	1359	,	6526	,	272	,	NULL	)	,
(	1360	,	6527	,	272	,	NULL	)	,
(	1361	,	6528	,	272	,	NULL	)	,
(	1362	,	6529	,	272	,	NULL	)	,
(	1363	,	6530	,	272	,	NULL	)	,
(	1364	,	6531	,	272	,	NULL	)	,
(	1365	,	6532	,	272	,	NULL	)	,
(	1366	,	6533	,	272	,	NULL	)	,
(	1367	,	6534	,	272	,	NULL	)	,
(	1368	,	6535	,	272	,	NULL	)	,
(	1369	,	6536	,	272	,	NULL	)	,
(	1370	,	6537	,	272	,	NULL	)	,
(	1371	,	6538	,	272	,	NULL	)	,
(	1372	,	6539	,	272	,	NULL	)	,
(	1373	,	6540	,	272	,	NULL	)	,
(	1374	,	6541	,	272	,	NULL	)	,
(	1375	,	6542	,	272	,	NULL	)	,
(	1376	,	6543	,	272	,	NULL	)	,

(	1377	,	6437	,	273	,	NULL	)	,
(	1378	,	6438	,	273	,	NULL	)	,
(	1379	,	6439	,	273	,	NULL	)	,
(	1380	,	6440	,	273	,	NULL	)	,
(	1381	,	6455	,	273	,	NULL	)	,
(	1382	,	6456	,	273	,	NULL	)	,
(	1383	,	6458	,	273	,	NULL	)	,
(	1384	,	6459	,	273	,	NULL	)	,
(	1385	,	6509	,	273	,	NULL	)	,
(	1386	,	6510	,	273	,	NULL	)	,
(	1387	,	6511	,	273	,	NULL	)	,
(	1388	,	6512	,	273	,	NULL	)	,
(	1389	,	6513	,	273	,	NULL	)	,
(	1390	,	6514	,	273	,	NULL	)	,
(	1391	,	6515	,	273	,	NULL	)	,
(	1392	,	6516	,	273	,	NULL	)	,
(	1393	,	6517	,	273	,	NULL	)	,
(	1394	,	6518	,	273	,	NULL	)	,
(	1395	,	6519	,	273	,	NULL	)	,
(	1396	,	6520	,	273	,	NULL	)	,
(	1397	,	6521	,	273	,	NULL	)	,
(	1398	,	6522	,	273	,	NULL	)	,
(	1399	,	6523	,	273	,	NULL	)	,
(	1400	,	6524	,	273	,	NULL	)	,
(	1401	,	6525	,	273	,	NULL	)	,
(	1402	,	6526	,	273	,	NULL	)	,
(	1403	,	6527	,	273	,	NULL	)	,
(	1404	,	6528	,	273	,	NULL	)	,
(	1405	,	6529	,	273	,	NULL	)	,
(	1406	,	6530	,	273	,	NULL	)	,
(	1407	,	6531	,	273	,	NULL	)	,
(	1408	,	6532	,	273	,	NULL	)	,
(	1409	,	6533	,	273	,	NULL	)	,
(	1410	,	6534	,	273	,	NULL	)	,
(	1411	,	6535	,	273	,	NULL	)	,
(	1412	,	6536	,	273	,	NULL	)	,
(	1413	,	6537	,	273	,	NULL	)	,
(	1414	,	6538	,	273	,	NULL	)	,
(	1415	,	6539	,	273	,	NULL	)	,
(	1416	,	6540	,	273	,	NULL	)	,
(	1417	,	6541	,	273	,	NULL	)	,
(	1418	,	6542	,	273	,	NULL	)	,
(	1419	,	6543	,	273	,	NULL	)	,

(	1420	,	6437	,	274	,	NULL	)	,
(	1421	,	6438	,	274	,	NULL	)	,
(	1422	,	6439	,	274	,	NULL	)	,
(	1423	,	6440	,	274	,	NULL	)	,
(	1424	,	6455	,	274	,	NULL	)	,
(	1425	,	6456	,	274	,	NULL	)	,
(	1426	,	6458	,	274	,	NULL	)	,
(	1427	,	6459	,	274	,	NULL	)	,
(	1428	,	6509	,	274	,	NULL	)	,
(	1429	,	6510	,	274	,	NULL	)	,
(	1430	,	6511	,	274	,	NULL	)	,
(	1431	,	6512	,	274	,	NULL	)	,
(	1432	,	6513	,	274	,	NULL	)	,
(	1433	,	6514	,	274	,	NULL	)	,
(	1434	,	6515	,	274	,	NULL	)	,
(	1435	,	6516	,	274	,	NULL	)	,
(	1436	,	6517	,	274	,	NULL	)	,
(	1437	,	6518	,	274	,	NULL	)	,
(	1438	,	6519	,	274	,	NULL	)	,
(	1439	,	6520	,	274	,	NULL	)	,
(	1440	,	6521	,	274	,	NULL	)	,
(	1441	,	6522	,	274	,	NULL	)	,
(	1442	,	6523	,	274	,	NULL	)	,
(	1443	,	6524	,	274	,	NULL	)	,
(	1444	,	6525	,	274	,	NULL	)	,
(	1445	,	6526	,	274	,	NULL	)	,
(	1446	,	6527	,	274	,	NULL	)	,
(	1447	,	6528	,	274	,	NULL	)	,
(	1448	,	6529	,	274	,	NULL	)	,
(	1449	,	6530	,	274	,	NULL	)	,
(	1450	,	6531	,	274	,	NULL	)	,
(	1451	,	6532	,	274	,	NULL	)	,
(	1452	,	6533	,	274	,	NULL	)	,
(	1453	,	6534	,	274	,	NULL	)	,
(	1454	,	6535	,	274	,	NULL	)	,
(	1455	,	6536	,	274	,	NULL	)	,
(	1456	,	6537	,	274	,	NULL	)	,
(	1457	,	6538	,	274	,	NULL	)	,
(	1458	,	6539	,	274	,	NULL	)	,
(	1459	,	6540	,	274	,	NULL	)	,
(	1460	,	6541	,	274	,	NULL	)	,
(	1461	,	6542	,	274	,	NULL	)	,
(	1462	,	6543	,	274	,	NULL	)	,

(	1463	,	6437	,	275	,	NULL	)	,
(	1464	,	6438	,	275	,	NULL	)	,
(	1465	,	6439	,	275	,	NULL	)	,
(	1466	,	6440	,	275	,	NULL	)	,
(	1467	,	6455	,	275	,	NULL	)	,
(	1468	,	6456	,	275	,	NULL	)	,
(	1469	,	6458	,	275	,	NULL	)	,
(	1470	,	6459	,	275	,	NULL	)	,
(	1471	,	6509	,	275	,	NULL	)	,
(	1472	,	6510	,	275	,	NULL	)	,
(	1473	,	6511	,	275	,	NULL	)	,
(	1474	,	6512	,	275	,	NULL	)	,
(	1475	,	6513	,	275	,	NULL	)	,
(	1476	,	6514	,	275	,	NULL	)	,
(	1477	,	6515	,	275	,	NULL	)	,
(	1478	,	6516	,	275	,	NULL	)	,
(	1479	,	6517	,	275	,	NULL	)	,
(	1480	,	6518	,	275	,	NULL	)	,
(	1481	,	6519	,	275	,	NULL	)	,
(	1482	,	6520	,	275	,	NULL	)	,
(	1483	,	6521	,	275	,	NULL	)	,
(	1484	,	6522	,	275	,	NULL	)	,
(	1485	,	6523	,	275	,	NULL	)	,
(	1486	,	6524	,	275	,	NULL	)	,
(	1487	,	6525	,	275	,	NULL	)	,
(	1488	,	6526	,	275	,	NULL	)	,
(	1489	,	6527	,	275	,	NULL	)	,
(	1490	,	6528	,	275	,	NULL	)	,
(	1491	,	6529	,	275	,	NULL	)	,
(	1492	,	6530	,	275	,	NULL	)	,
(	1493	,	6531	,	275	,	NULL	)	,
(	1494	,	6532	,	275	,	NULL	)	,
(	1495	,	6533	,	275	,	NULL	)	,
(	1496	,	6534	,	275	,	NULL	)	,
(	1497	,	6535	,	275	,	NULL	)	,
(	1498	,	6536	,	275	,	NULL	)	,
(	1499	,	6537	,	275	,	NULL	)	,
(	1500	,	6538	,	275	,	NULL	)	,
(	1501	,	6539	,	275	,	NULL	)	,
(	1502	,	6540	,	275	,	NULL	)	,
(	1503	,	6541	,	275	,	NULL	)	,
(	1504	,	6542	,	275	,	NULL	)	,
(	1505	,	6543	,	275	,	NULL	)	,










(	1506	,	6437	,	276	,	NULL	)	,
(	1507	,	6438	,	276	,	NULL	)	,
(	1508	,	6439	,	276	,	NULL	)	,
(	1509	,	6440	,	276	,	NULL	)	,
(	1510	,	6455	,	276	,	NULL	)	,
(	1511	,	6456	,	276	,	NULL	)	,
(	1512	,	6458	,	276	,	NULL	)	,
(	1513	,	6459	,	276	,	NULL	)	,
(	1514	,	6493	,	276	,	NULL	)	,
(	1515	,	6494	,	276	,	NULL	)	,
(	1516	,	6495	,	276	,	NULL	)	,
(	1517	,	6496	,	276	,	NULL	)	,
(	1518	,	6497	,	276	,	NULL	)	,
(	1519	,	6498	,	276	,	NULL	)	,
(	1520	,	6499	,	276	,	NULL	)	,
(	1521	,	6500	,	276	,	NULL	)	,
(	1522	,	6501	,	276	,	NULL	)	,
(	1523	,	6502	,	276	,	NULL	)	,
(	1524	,	6503	,	276	,	NULL	)	,
(	1525	,	6504	,	276	,	NULL	)	,
(	1526	,	6505	,	276	,	NULL	)	,
(	1527	,	6506	,	276	,	NULL	)	,
(	1528	,	6509	,	276	,	NULL	)	,
(	1529	,	6510	,	276	,	NULL	)	,
(	1530	,	6511	,	276	,	NULL	)	,
(	1531	,	6512	,	276	,	NULL	)	,
(	1532	,	6513	,	276	,	NULL	)	,
(	1533	,	6514	,	276	,	NULL	)	,
(	1534	,	6515	,	276	,	NULL	)	,
(	1535	,	6516	,	276	,	NULL	)	,
(	1536	,	6517	,	276	,	NULL	)	,
(	1537	,	6518	,	276	,	NULL	)	,
(	1538	,	6519	,	276	,	NULL	)	,
(	1539	,	6520	,	276	,	NULL	)	,
(	1540	,	6521	,	276	,	NULL	)	,
(	1541	,	6522	,	276	,	NULL	)	,
(	1542	,	6523	,	276	,	NULL	)	,
(	1543	,	6524	,	276	,	NULL	)	,
(	1544	,	6525	,	276	,	NULL	)	,
(	1545	,	6526	,	276	,	NULL	)	,
(	1546	,	6527	,	276	,	NULL	)	,
(	1547	,	6528	,	276	,	NULL	)	,
(	1548	,	6529	,	276	,	NULL	)	,
(	1549	,	6530	,	276	,	NULL	)	,
(	1550	,	6531	,	276	,	NULL	)	,
(	1551	,	6532	,	276	,	NULL	)	,
(	1552	,	6533	,	276	,	NULL	)	,
(	1553	,	6534	,	276	,	NULL	)	,
(	1554	,	6535	,	276	,	NULL	)	,
(	1555	,	6536	,	276	,	NULL	)	,
(	1556	,	6537	,	276	,	NULL	)	,
(	1557	,	6538	,	276	,	NULL	)	,
(	1558	,	6539	,	276	,	NULL	)	,
(	1559	,	6540	,	276	,	NULL	)	,
(	1560	,	6541	,	276	,	NULL	)	,
(	1561	,	6542	,	276	,	NULL	)	,
(	1562	,	6543	,	276	,	NULL	)	,

(	1563	,	6437	,	277	,	NULL	)	,
(	1564	,	6438	,	277	,	NULL	)	,
(	1565	,	6439	,	277	,	NULL	)	,
(	1566	,	6440	,	277	,	NULL	)	,
(	1567	,	6455	,	277	,	NULL	)	,
(	1568	,	6456	,	277	,	NULL	)	,
(	1569	,	6458	,	277	,	NULL	)	,
(	1570	,	6459	,	277	,	NULL	)	,
(	1571	,	6493	,	277	,	NULL	)	,
(	1572	,	6494	,	277	,	NULL	)	,
(	1573	,	6495	,	277	,	NULL	)	,
(	1574	,	6496	,	277	,	NULL	)	,
(	1575	,	6497	,	277	,	NULL	)	,
(	1576	,	6498	,	277	,	NULL	)	,
(	1577	,	6499	,	277	,	NULL	)	,
(	1578	,	6500	,	277	,	NULL	)	,
(	1579	,	6501	,	277	,	NULL	)	,
(	1580	,	6502	,	277	,	NULL	)	,
(	1581	,	6503	,	277	,	NULL	)	,
(	1582	,	6504	,	277	,	NULL	)	,
(	1583	,	6505	,	277	,	NULL	)	,
(	1584	,	6506	,	277	,	NULL	)	,
(	1585	,	6509	,	277	,	NULL	)	,
(	1586	,	6510	,	277	,	NULL	)	,
(	1587	,	6511	,	277	,	NULL	)	,
(	1588	,	6512	,	277	,	NULL	)	,
(	1589	,	6513	,	277	,	NULL	)	,
(	1590	,	6514	,	277	,	NULL	)	,
(	1591	,	6515	,	277	,	NULL	)	,
(	1592	,	6516	,	277	,	NULL	)	,
(	1593	,	6517	,	277	,	NULL	)	,
(	1594	,	6518	,	277	,	NULL	)	,
(	1595	,	6519	,	277	,	NULL	)	,
(	1596	,	6520	,	277	,	NULL	)	,
(	1597	,	6521	,	277	,	NULL	)	,
(	1598	,	6522	,	277	,	NULL	)	,
(	1599	,	6523	,	277	,	NULL	)	,
(	1600	,	6524	,	277	,	NULL	)	,
(	1601	,	6525	,	277	,	NULL	)	,
(	1602	,	6526	,	277	,	NULL	)	,
(	1603	,	6527	,	277	,	NULL	)	,
(	1604	,	6528	,	277	,	NULL	)	,
(	1605	,	6529	,	277	,	NULL	)	,
(	1606	,	6530	,	277	,	NULL	)	,
(	1607	,	6531	,	277	,	NULL	)	,
(	1608	,	6532	,	277	,	NULL	)	,
(	1609	,	6533	,	277	,	NULL	)	,
(	1610	,	6534	,	277	,	NULL	)	,
(	1611	,	6535	,	277	,	NULL	)	,
(	1612	,	6536	,	277	,	NULL	)	,
(	1613	,	6537	,	277	,	NULL	)	,
(	1614	,	6538	,	277	,	NULL	)	,
(	1615	,	6539	,	277	,	NULL	)	,
(	1616	,	6540	,	277	,	NULL	)	,
(	1617	,	6541	,	277	,	NULL	)	,
(	1618	,	6542	,	277	,	NULL	)	,
(	1619	,	6543	,	277	,	NULL	)	,

(	1620	,	6437	,	278	,	NULL	)	,
(	1621	,	6438	,	278	,	NULL	)	,
(	1622	,	6439	,	278	,	NULL	)	,
(	1623	,	6440	,	278	,	NULL	)	,
(	1624	,	6455	,	278	,	NULL	)	,
(	1625	,	6456	,	278	,	NULL	)	,
(	1626	,	6458	,	278	,	NULL	)	,
(	1627	,	6459	,	278	,	NULL	)	,
(	1628	,	6493	,	278	,	NULL	)	,
(	1629	,	6494	,	278	,	NULL	)	,
(	1630	,	6495	,	278	,	NULL	)	,
(	1631	,	6496	,	278	,	NULL	)	,
(	1632	,	6497	,	278	,	NULL	)	,
(	1633	,	6498	,	278	,	NULL	)	,
(	1634	,	6499	,	278	,	NULL	)	,
(	1635	,	6500	,	278	,	NULL	)	,
(	1636	,	6501	,	278	,	NULL	)	,
(	1637	,	6502	,	278	,	NULL	)	,
(	1638	,	6503	,	278	,	NULL	)	,
(	1639	,	6504	,	278	,	NULL	)	,
(	1640	,	6505	,	278	,	NULL	)	,
(	1641	,	6506	,	278	,	NULL	)	,
(	1642	,	6509	,	278	,	NULL	)	,
(	1643	,	6510	,	278	,	NULL	)	,
(	1644	,	6511	,	278	,	NULL	)	,
(	1645	,	6512	,	278	,	NULL	)	,
(	1646	,	6513	,	278	,	NULL	)	,
(	1647	,	6514	,	278	,	NULL	)	,
(	1648	,	6515	,	278	,	NULL	)	,
(	1649	,	6516	,	278	,	NULL	)	,
(	1650	,	6517	,	278	,	NULL	)	,
(	1651	,	6518	,	278	,	NULL	)	,
(	1652	,	6519	,	278	,	NULL	)	,
(	1653	,	6520	,	278	,	NULL	)	,
(	1654	,	6521	,	278	,	NULL	)	,
(	1655	,	6522	,	278	,	NULL	)	,
(	1656	,	6523	,	278	,	NULL	)	,
(	1657	,	6524	,	278	,	NULL	)	,
(	1658	,	6525	,	278	,	NULL	)	,
(	1659	,	6526	,	278	,	NULL	)	,
(	1660	,	6527	,	278	,	NULL	)	,
(	1661	,	6528	,	278	,	NULL	)	,
(	1662	,	6529	,	278	,	NULL	)	,
(	1663	,	6530	,	278	,	NULL	)	,
(	1664	,	6531	,	278	,	NULL	)	,
(	1665	,	6532	,	278	,	NULL	)	,
(	1666	,	6533	,	278	,	NULL	)	,
(	1667	,	6534	,	278	,	NULL	)	,
(	1668	,	6535	,	278	,	NULL	)	,
(	1669	,	6536	,	278	,	NULL	)	,
(	1670	,	6537	,	278	,	NULL	)	,
(	1671	,	6538	,	278	,	NULL	)	,
(	1672	,	6539	,	278	,	NULL	)	,
(	1673	,	6540	,	278	,	NULL	)	,
(	1674	,	6541	,	278	,	NULL	)	,
(	1675	,	6542	,	278	,	NULL	)	,
(	1676	,	6543	,	278	,	NULL	)	,

(	1677	,	6437	,	279	,	NULL	)	,
(	1678	,	6438	,	279	,	NULL	)	,
(	1679	,	6439	,	279	,	NULL	)	,
(	1680	,	6440	,	279	,	NULL	)	,
(	1681	,	6455	,	279	,	NULL	)	,
(	1682	,	6456	,	279	,	NULL	)	,
(	1683	,	6458	,	279	,	NULL	)	,
(	1684	,	6459	,	279	,	NULL	)	,
(	1685	,	6493	,	279	,	NULL	)	,
(	1686	,	6494	,	279	,	NULL	)	,
(	1687	,	6495	,	279	,	NULL	)	,
(	1688	,	6496	,	279	,	NULL	)	,
(	1689	,	6497	,	279	,	NULL	)	,
(	1690	,	6498	,	279	,	NULL	)	,
(	1691	,	6499	,	279	,	NULL	)	,
(	1692	,	6500	,	279	,	NULL	)	,
(	1693	,	6501	,	279	,	NULL	)	,
(	1694	,	6502	,	279	,	NULL	)	,
(	1695	,	6503	,	279	,	NULL	)	,
(	1696	,	6504	,	279	,	NULL	)	,
(	1697	,	6505	,	279	,	NULL	)	,
(	1698	,	6506	,	279	,	NULL	)	,
(	1699	,	6509	,	279	,	NULL	)	,
(	1700	,	6510	,	279	,	NULL	)	,
(	1701	,	6511	,	279	,	NULL	)	,
(	1702	,	6512	,	279	,	NULL	)	,
(	1703	,	6513	,	279	,	NULL	)	,
(	1704	,	6514	,	279	,	NULL	)	,
(	1705	,	6515	,	279	,	NULL	)	,
(	1706	,	6516	,	279	,	NULL	)	,
(	1707	,	6517	,	279	,	NULL	)	,
(	1708	,	6518	,	279	,	NULL	)	,
(	1709	,	6519	,	279	,	NULL	)	,
(	1710	,	6520	,	279	,	NULL	)	,
(	1711	,	6521	,	279	,	NULL	)	,
(	1712	,	6522	,	279	,	NULL	)	,
(	1713	,	6523	,	279	,	NULL	)	,
(	1714	,	6524	,	279	,	NULL	)	,
(	1715	,	6525	,	279	,	NULL	)	,
(	1716	,	6526	,	279	,	NULL	)	,
(	1717	,	6527	,	279	,	NULL	)	,
(	1718	,	6528	,	279	,	NULL	)	,
(	1719	,	6529	,	279	,	NULL	)	,
(	1720	,	6530	,	279	,	NULL	)	,
(	1721	,	6531	,	279	,	NULL	)	,
(	1722	,	6532	,	279	,	NULL	)	,
(	1723	,	6533	,	279	,	NULL	)	,
(	1724	,	6534	,	279	,	NULL	)	,
(	1725	,	6535	,	279	,	NULL	)	,
(	1726	,	6536	,	279	,	NULL	)	,
(	1727	,	6537	,	279	,	NULL	)	,
(	1728	,	6538	,	279	,	NULL	)	,
(	1729	,	6539	,	279	,	NULL	)	,
(	1730	,	6540	,	279	,	NULL	)	,
(	1731	,	6541	,	279	,	NULL	)	,
(	1732	,	6542	,	279	,	NULL	)	,
(	1733	,	6543	,	279	,	NULL	)	,

(	1734	,	6437	,	280	,	NULL	)	,
(	1735	,	6438	,	280	,	NULL	)	,
(	1736	,	6439	,	280	,	NULL	)	,
(	1737	,	6440	,	280	,	NULL	)	,
(	1738	,	6455	,	280	,	NULL	)	,
(	1739	,	6456	,	280	,	NULL	)	,
(	1740	,	6458	,	280	,	NULL	)	,
(	1741	,	6459	,	280	,	NULL	)	,
(	1742	,	6493	,	280	,	NULL	)	,
(	1743	,	6494	,	280	,	NULL	)	,
(	1744	,	6495	,	280	,	NULL	)	,
(	1745	,	6496	,	280	,	NULL	)	,
(	1746	,	6497	,	280	,	NULL	)	,
(	1747	,	6498	,	280	,	NULL	)	,
(	1748	,	6499	,	280	,	NULL	)	,
(	1749	,	6500	,	280	,	NULL	)	,
(	1750	,	6501	,	280	,	NULL	)	,
(	1751	,	6502	,	280	,	NULL	)	,
(	1752	,	6503	,	280	,	NULL	)	,
(	1753	,	6504	,	280	,	NULL	)	,
(	1754	,	6505	,	280	,	NULL	)	,
(	1755	,	6506	,	280	,	NULL	)	,
(	1756	,	6509	,	280	,	NULL	)	,
(	1757	,	6510	,	280	,	NULL	)	,
(	1758	,	6511	,	280	,	NULL	)	,
(	1759	,	6512	,	280	,	NULL	)	,
(	1760	,	6513	,	280	,	NULL	)	,
(	1761	,	6514	,	280	,	NULL	)	,
(	1762	,	6515	,	280	,	NULL	)	,
(	1763	,	6516	,	280	,	NULL	)	,
(	1764	,	6517	,	280	,	NULL	)	,
(	1765	,	6518	,	280	,	NULL	)	,
(	1766	,	6519	,	280	,	NULL	)	,
(	1767	,	6520	,	280	,	NULL	)	,
(	1768	,	6521	,	280	,	NULL	)	,
(	1769	,	6522	,	280	,	NULL	)	,
(	1770	,	6523	,	280	,	NULL	)	,
(	1771	,	6524	,	280	,	NULL	)	,
(	1772	,	6525	,	280	,	NULL	)	,
(	1773	,	6526	,	280	,	NULL	)	,
(	1774	,	6527	,	280	,	NULL	)	,
(	1775	,	6528	,	280	,	NULL	)	,
(	1776	,	6529	,	280	,	NULL	)	,
(	1777	,	6530	,	280	,	NULL	)	,
(	1778	,	6531	,	280	,	NULL	)	,
(	1779	,	6532	,	280	,	NULL	)	,
(	1780	,	6533	,	280	,	NULL	)	,
(	1781	,	6534	,	280	,	NULL	)	,
(	1782	,	6535	,	280	,	NULL	)	,
(	1783	,	6536	,	280	,	NULL	)	,
(	1784	,	6537	,	280	,	NULL	)	,
(	1785	,	6538	,	280	,	NULL	)	,
(	1786	,	6539	,	280	,	NULL	)	,
(	1787	,	6540	,	280	,	NULL	)	,
(	1788	,	6541	,	280	,	NULL	)	,
(	1789	,	6542	,	280	,	NULL	)	,
(	1790	,	6543	,	280	,	NULL	)	,

(	1791	,	6437	,	281	,	NULL	)	,
(	1792	,	6438	,	281	,	NULL	)	,
(	1793	,	6439	,	281	,	NULL	)	,
(	1794	,	6440	,	281	,	NULL	)	,
(	1795	,	6455	,	281	,	NULL	)	,
(	1796	,	6456	,	281	,	NULL	)	,
(	1797	,	6458	,	281	,	NULL	)	,
(	1798	,	6459	,	281	,	NULL	)	,
(	1799	,	6493	,	281	,	NULL	)	,
(	1800	,	6494	,	281	,	NULL	)	,
(	1801	,	6495	,	281	,	NULL	)	,
(	1802	,	6496	,	281	,	NULL	)	,
(	1803	,	6497	,	281	,	NULL	)	,
(	1804	,	6498	,	281	,	NULL	)	,
(	1805	,	6499	,	281	,	NULL	)	,
(	1806	,	6500	,	281	,	NULL	)	,
(	1807	,	6501	,	281	,	NULL	)	,
(	1808	,	6502	,	281	,	NULL	)	,
(	1809	,	6503	,	281	,	NULL	)	,
(	1810	,	6504	,	281	,	NULL	)	,
(	1811	,	6505	,	281	,	NULL	)	,
(	1812	,	6506	,	281	,	NULL	)	,
(	1813	,	6509	,	281	,	NULL	)	,
(	1814	,	6510	,	281	,	NULL	)	,
(	1815	,	6511	,	281	,	NULL	)	,
(	1816	,	6512	,	281	,	NULL	)	,
(	1817	,	6513	,	281	,	NULL	)	,
(	1818	,	6514	,	281	,	NULL	)	,
(	1819	,	6515	,	281	,	NULL	)	,
(	1820	,	6516	,	281	,	NULL	)	,
(	1821	,	6517	,	281	,	NULL	)	,
(	1822	,	6518	,	281	,	NULL	)	,
(	1823	,	6519	,	281	,	NULL	)	,
(	1824	,	6520	,	281	,	NULL	)	,
(	1825	,	6521	,	281	,	NULL	)	,
(	1826	,	6522	,	281	,	NULL	)	,
(	1827	,	6523	,	281	,	NULL	)	,
(	1828	,	6524	,	281	,	NULL	)	,
(	1829	,	6525	,	281	,	NULL	)	,
(	1830	,	6526	,	281	,	NULL	)	,
(	1831	,	6527	,	281	,	NULL	)	,
(	1832	,	6528	,	281	,	NULL	)	,
(	1833	,	6529	,	281	,	NULL	)	,
(	1834	,	6530	,	281	,	NULL	)	,
(	1835	,	6531	,	281	,	NULL	)	,
(	1836	,	6532	,	281	,	NULL	)	,
(	1837	,	6533	,	281	,	NULL	)	,
(	1838	,	6534	,	281	,	NULL	)	,
(	1839	,	6535	,	281	,	NULL	)	,
(	1840	,	6536	,	281	,	NULL	)	,
(	1841	,	6537	,	281	,	NULL	)	,
(	1842	,	6538	,	281	,	NULL	)	,
(	1843	,	6539	,	281	,	NULL	)	,
(	1844	,	6540	,	281	,	NULL	)	,
(	1845	,	6541	,	281	,	NULL	)	,
(	1846	,	6542	,	281	,	NULL	)	,
(	1847	,	6543	,	281	,	NULL	)	,

(	1848	,	6437	,	282	,	NULL	)	,
(	1849	,	6438	,	282	,	NULL	)	,
(	1850	,	6439	,	282	,	NULL	)	,
(	1851	,	6440	,	282	,	NULL	)	,
(	1852	,	6455	,	282	,	NULL	)	,
(	1853	,	6456	,	282	,	NULL	)	,
(	1854	,	6458	,	282	,	NULL	)	,
(	1855	,	6459	,	282	,	NULL	)	,
(	1856	,	6493	,	282	,	NULL	)	,
(	1857	,	6494	,	282	,	NULL	)	,
(	1858	,	6495	,	282	,	NULL	)	,
(	1859	,	6496	,	282	,	NULL	)	,
(	1860	,	6497	,	282	,	NULL	)	,
(	1861	,	6498	,	282	,	NULL	)	,
(	1862	,	6499	,	282	,	NULL	)	,
(	1863	,	6500	,	282	,	NULL	)	,
(	1864	,	6501	,	282	,	NULL	)	,
(	1865	,	6502	,	282	,	NULL	)	,
(	1866	,	6503	,	282	,	NULL	)	,
(	1867	,	6504	,	282	,	NULL	)	,
(	1868	,	6505	,	282	,	NULL	)	,
(	1869	,	6506	,	282	,	NULL	)	,
(	1870	,	6509	,	282	,	NULL	)	,
(	1871	,	6510	,	282	,	NULL	)	,
(	1872	,	6511	,	282	,	NULL	)	,
(	1873	,	6512	,	282	,	NULL	)	,
(	1874	,	6513	,	282	,	NULL	)	,
(	1875	,	6514	,	282	,	NULL	)	,
(	1876	,	6515	,	282	,	NULL	)	,
(	1877	,	6516	,	282	,	NULL	)	,
(	1878	,	6517	,	282	,	NULL	)	,
(	1879	,	6518	,	282	,	NULL	)	,
(	1880	,	6519	,	282	,	NULL	)	,
(	1881	,	6520	,	282	,	NULL	)	,
(	1882	,	6521	,	282	,	NULL	)	,
(	1883	,	6522	,	282	,	NULL	)	,
(	1884	,	6523	,	282	,	NULL	)	,
(	1885	,	6524	,	282	,	NULL	)	,
(	1886	,	6525	,	282	,	NULL	)	,
(	1887	,	6526	,	282	,	NULL	)	,
(	1888	,	6527	,	282	,	NULL	)	,
(	1889	,	6528	,	282	,	NULL	)	,
(	1890	,	6529	,	282	,	NULL	)	,
(	1891	,	6530	,	282	,	NULL	)	,
(	1892	,	6531	,	282	,	NULL	)	,
(	1893	,	6532	,	282	,	NULL	)	,
(	1894	,	6533	,	282	,	NULL	)	,
(	1895	,	6534	,	282	,	NULL	)	,
(	1896	,	6535	,	282	,	NULL	)	,
(	1897	,	6536	,	282	,	NULL	)	,
(	1898	,	6537	,	282	,	NULL	)	,
(	1899	,	6538	,	282	,	NULL	)	,
(	1900	,	6539	,	282	,	NULL	)	,
(	1901	,	6540	,	282	,	NULL	)	,
(	1902	,	6541	,	282	,	NULL	)	,
(	1903	,	6542	,	282	,	NULL	)	,
(	1904	,	6543	,	282	,	NULL	)	,

-- PBI 50814
(	1905	,	NULL	,	284	,	NULL	)	,
(	1906	,	NULL	,	285	,	NULL	)	,
(	1907	,	NULL	,	286	,	NULL	)	,
(	1908	,	NULL	,	287	,	NULL	)	,
(	1909	,	NULL	,	288	,	NULL	)	,
(	1910	,	NULL	,	289	,	NULL	)	,
(	1911	,	NULL	,	290	,	NULL	)	,
(	1912	,	NULL	,	291	,	NULL	)	,
(	1913	,	NULL	,	292	,	NULL	)	,
(	1914	,	NULL	,	293	,	NULL	)	,
(	1915	,	NULL	,	294	,	NULL	)	,
(	1916	,	NULL	,	295	,	NULL	)
)
AS SOURCE([AssetClassTypeIssueTypeMapID],[AssetClassTypeID],[IssueTypeID],[DisplayOrder])
ON Target.[AssetClassTypeIssueTypeMapID]= Source.[AssetClassTypeIssueTypeMapID]
WHEN MATCHED THEN UPDATE SET [AssetClassTypeID]=Source.[AssetClassTypeID],[IssueTypeID]=Source.[IssueTypeID],[DisplayOrder]=Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssetClassTypeIssueTypeMapID],[AssetClassTypeID],[IssueTypeID],[DisplayOrder])
VALUES ([AssetClassTypeIssueTypeMapID],[AssetClassTypeID],[IssueTypeID],[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT [Diagnostics].[tAssetClassTypeIssueTypeMap] OFF
GO

SET IDENTITY_INSERT Diagnostics.tAssetIssueActivityStatusType ON
MERGE INTO Diagnostics.tAssetIssueActivityStatusType AS TARGET USING (VALUES
(1,'Open', 1, 1)
,(2,'Closed', 0, 0)
)
AS SOURCE([ID],[Desc],[IsOpen], [IsDefault])
ON Target.[AssetIssueActivityStatusTypeID]= Source.[ID]
WHEN MATCHED THEN UPDATE SET [AssetIssueActivityStatusTypeDesc]=Source.[Desc],[IsOpen]=Source.[IsOpen],[IsDefault]=Source.[IsDefault]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssetIssueActivityStatusTypeID],[AssetIssueActivityStatusTypeDesc],[IsOpen], [IsDefault])
VALUES ([ID],[Desc],[IsOpen], [IsDefault])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT Diagnostics.tAssetIssueActivityStatusType OFF
GO

INSERT INTO Diagnostics.tCategoryAssetIssueActivityStatusTypeMap
(AssetIssueActivityStatusTypeID, AssetIssueCategoryTypeID)
SELECT NULL, cat.AssetIssueCategoryTypeID
FROM Diagnostics.tAssetIssueCategoryType cat

SET IDENTITY_INSERT Diagnostics.tAssetIssueResolutionStatusType ON
MERGE INTO Diagnostics.tAssetIssueResolutionStatusType AS TARGET USING (VALUES
(1, 'Diagnosing', 1),(2, 'Site Reviewing', 1),(3, 'Resolved', 1),(4, 'Abandoned', 1),(5,'Mitigated', 1),(6,'Not Mitigated', 1),(7,'Not Applicable', 1),(8,'Work Defined', 1),(9,'Work Completed', 1)
)
AS SOURCE([ID],[Desc],[CreatedBy])
ON Target.[AssetIssueResolutionStatusTypeID]= Source.[ID]
WHEN MATCHED THEN UPDATE SET [AssetIssueResolutionStatusTypeDesc]=Source.[Desc], [CreatedBy] = SOURCE.[CreatedBy]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([AssetIssueResolutionStatusTypeID],[AssetIssueResolutionStatusTypeDesc],[CreatedBy])
VALUES ([ID], [Desc],[CreatedBy])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
SET IDENTITY_INSERT Diagnostics.tAssetIssueResolutionStatusType OFF
GO

INSERT INTO Diagnostics.tCategoryAssetIssueResolutionStatusTypeMap
(AssetIssueCategoryTypeID, AssetIssueResolutionStatusTypeID, ChangedBy, DisplayOrder)
SELECT c.AssetIssueCategoryTypeID, r.AssetIssueResolutionStatusTypeID, 1, r.AssetIssueResolutionStatusTypeID FROM Diagnostics.tAssetIssueCategoryType c
CROSS JOIN Diagnostics.tAssetIssueResolutionStatusType r
WHERE c.IssueClassTypeID in (1,2,3,4,6) AND r.AssetIssueResolutionStatusTypeID in (1,2,3,4,8,9)

INSERT INTO Diagnostics.tCategoryAssetIssueResolutionStatusTypeMap
(AssetIssueCategoryTypeID, AssetIssueResolutionStatusTypeID, ChangedBy, DisplayOrder)
SELECT c.AssetIssueCategoryTypeID, r.AssetIssueResolutionStatusTypeID, 1, 8 - r.AssetIssueResolutionStatusTypeID FROM Diagnostics.tAssetIssueCategoryType c
CROSS JOIN Diagnostics.tAssetIssueResolutionStatusType r
WHERE c.IssueClassTypeID =5 AND r.AssetIssueResolutionStatusTypeID in (5,6,7)


INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 3 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 10 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 3 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 10 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Outage Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Outage Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 3 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Outage Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType WHERE CategoryDesc = 'Outage Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 10 FROM
Diagnostics.tAssetIssueCategoryType  WHERE CategoryDesc = 'Outage Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Online Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Online Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 3 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Online Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Online Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 10 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Online Maintenance' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 3 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 10 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 1
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Purchasing' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Purchasing' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Purchasing' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Purchasing' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Transportation Scheduling' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Transportation Scheduling' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Transportation Scheduling' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Transportation Scheduling' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Mine Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Mine Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Mine Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Mine Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 2
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'I&C' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Fuel Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Yard Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Plant AQC Operations' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'AQC Planning' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 1 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 2 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 4 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 5 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 6 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 7 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 8 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 3
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Unassigned' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'PMO' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'PMO' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'PMO' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'PMO' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'PMO' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Request For Information' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Request For Information' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Request For Information' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Request For Information' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Request For Information' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Deliverable' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Deliverable' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Deliverable' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Deliverable' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Deliverable' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'OT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'OT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'OT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'OT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'OT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'IT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'IT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'IT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'IT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'IT' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Telecom' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Telecom' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Telecom' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Telecom' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Telecom' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Safety' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Safety' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Safety' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Safety' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Safety' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Engineering' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Engineering' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Engineering' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Engineering' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Engineering' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Permitting' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Permitting' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Permitting' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Permitting' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Permitting' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Construction' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Construction' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Construction' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Construction' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Construction' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Closeout' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Closeout' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Closeout' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Closeout' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Closeout' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Scope' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Scope' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Scope' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Scope' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Scope' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Planning' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Planning' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Planning' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Planning' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Planning' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Design Review' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Design Review' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Design Review' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Design Review' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Design Review' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Strategic Sourcing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Strategic Sourcing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Strategic Sourcing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Strategic Sourcing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Strategic Sourcing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Equipment Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Equipment Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Equipment Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Equipment Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Equipment Procurement' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Testing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Testing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Testing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Testing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Testing' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 9 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Other' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 11 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Other' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 12 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Other' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 13 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Other' AND IssueClassTypeID = 6
INSERT INTO Diagnostics.tCategoryAssetIssueImpactTypeMap(AssetIssueCategoryTypeID, AssetIssueImpactTypeID) SELECT AssetIssueCategoryTypeID, 14 FROM
Diagnostics.tAssetIssueCategoryType
WHERE CategoryDesc = 'Other' AND IssueClassTypeID = 6

