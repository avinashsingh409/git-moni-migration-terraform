﻿MERGE INTO Projection.tScenarioObjectiveType AS TARGET USING (Values
(1,'Fuel Focus','FuelFocus'),(2,'Regulatory Administration','RegAdmin'))
AS SOURCE([ScenarioObjectiveTypeID],[ScenarioObjectiveTypeDesc],[ScenarioObjectiveTypeAbbrev])
ON TARGET.ScenarioObjectiveTypeID = Source.ScenarioObjectiveTypeID
WHEN MATCHED THEN UPDATE SET [ScenarioObjectiveTypeDesc]=Source.[ScenarioObjectiveTypeDesc],[ScenarioObjectiveTypeAbbrev]=Source.[ScenarioObjectiveTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([ScenarioObjectiveTypeID],[ScenarioObjectiveTypeDesc],[ScenarioObjectiveTypeAbbrev])
VALUES ([ScenarioObjectiveTypeID],[ScenarioObjectiveTypeDesc],[ScenarioObjectiveTypeAbbrev]);

MERGE INTO Projection.tConfidenceType AS TARGET USING (VALUES 
(1,'SO2','SO2')
,(2,'NOx','NOx')
,(3,'Mercury','Hg')
,(4,'CO2','CO2')
,(5,'Opacity','Opacity')
,(6,'Forced Draft Fan','FD Fan')
,(7,'Primary Air Fan','PA Fan')
,(8,'Induced Draft Fan','ID Fan')
,(9,'Scrubber Booster Fan','SB Fan')
,(10,'Mill Capacity','Mill Capacity')
,(11,'Mill Drying','Mill Drying')
,(12,'Slagging','Slagging')
,(13,'Fouling','Fouling')
,(14,'Cyclone Acceptability','CA')
,(15,'Equivalent Availablity Factor','EAF')
,(16,'Ash Production Rate','Ash')
,(17,'Gypsum Production Rate','Gypsum')
)
AS SOURCE([ConfidenceTypeID],[ConfidenceTypeDesc],[ConfidenceTypeAbbrev])
ON Target.[ConfidenceTypeID]= Source.[ConfidenceTypeID]
WHEN MATCHED THEN UPDATE SET [ConfidenceTypeDesc]=Source.[ConfidenceTypeDesc],[ConfidenceTypeAbbrev]=Source.[ConfidenceTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([ConfidenceTypeID],[ConfidenceTypeDesc],[ConfidenceTypeAbbrev])
VALUES ([ConfidenceTypeID],[ConfidenceTypeDesc],[ConfidenceTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Projection.tCQIRunType AS TARGET USING (VALUES 
(1,'Vista')
)
AS SOURCE([CQIRunTypeID],[CQIRunTypeDesc])
ON Target.[CQIRunTypeID]= Source.[CQIRunTypeID]
WHEN MATCHED THEN UPDATE SET [CQIRunTypeDesc]=Source.[CQIRunTypeDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([CQIRunTypeID],[CQIRunTypeDesc])
VALUES ([CQIRunTypeID],[CQIRunTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
MERGE INTO Projection.tPlanType AS TARGET USING (VALUES 
(0,'Undefined')
,(1,'Annual')
,(2,'Monthly')
,(3,'Quarterly')
)
AS SOURCE([PlanTypeID],[PlanTypeDesc])
ON Target.[PlanTypeID]= Source.[PlanTypeID]
WHEN MATCHED THEN UPDATE SET [PlanTypeDesc]=Source.[PlanTypeDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([PlanTypeID],[PlanTypeDesc])
VALUES ([PlanTypeID],[PlanTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
MERGE INTO Projection.tDataVersion AS TARGET USING (VALUES 
(1,'Annual Budget',1,12)
,(2,'1+11',2,1)
,(3,'2+10',2,2)
,(4,'3+9',2,3)
,(5,'4+8',2,4)
,(6,'5+7',2,5)
,(7,'6+6',2,6)
,(8,'7+5',2,7)
,(9,'8+4',2,8)
,(10,'9+3',2,9)
,(11,'10+2',2,10)
,(12,'11+1',2,11)
)
AS SOURCE([DataVersionID],[DataVersionDesc],[PlanTypeID],[PlanPeriod])
ON Target.[DataVersionID]= Source.[DataVersionID]
WHEN MATCHED THEN UPDATE SET [DataVersionDesc]=Source.[DataVersionDesc],[PlanTypeID]=Source.[PlanTypeID],[PlanPeriod]=Source.[PlanPeriod]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([DataVersionID],[DataVersionDesc],[PlanTypeID],[PlanPeriod])
VALUES ([DataVersionID],[DataVersionDesc],[PlanTypeID],[PlanPeriod])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Projection.tFanType AS TARGET USING (VALUES 
(0,'Primary Air Fan','PA')
,(1,'Hot Primary Air Fan','PA_HOT')
,(2,'Forced Draft Fan','FD')
,(3,'Induced Draft Fan','ID')
,(4,'Scrubber Booster Fan','SB')
)
AS SOURCE([FanTypeID],[FanTypeDesc],[FanTypeAbbrev])
ON Target.[FanTypeID]= Source.[FanTypeID]
WHEN MATCHED THEN UPDATE SET [FanTypeDesc]=Source.[FanTypeDesc],[FanTypeAbbrev]=Source.[FanTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([FanTypeID],[FanTypeDesc],[FanTypeAbbrev])
VALUES ([FanTypeID],[FanTypeDesc],[FanTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Projection.tPollutantType AS TARGET USING (VALUES 
(1,'SO2','SO2',1)
,(2,'NOX - Annual','NOx',2)
,(3,'NOX - Seasonal','NOx-S',3)
,(4,'Mercury','Hg',4)
,(5,'CO2','CO2',5)
)
AS SOURCE([PollutantTypeID],[PollutantTypeDesc],[PollutantTypeAbbrev],[DisplayOrder])
ON Target.[PollutantTypeID]= Source.[PollutantTypeID]
WHEN MATCHED THEN UPDATE SET [PollutantTypeDesc]=Source.[PollutantTypeDesc],[PollutantTypeAbbrev]=Source.[PollutantTypeAbbrev],[DisplayOrder]=Source.[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([PollutantTypeID],[PollutantTypeDesc],[PollutantTypeAbbrev],[DisplayOrder])
VALUES ([PollutantTypeID],[PollutantTypeDesc],[PollutantTypeAbbrev],[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Projection.tSensitivityImpactType AS TARGET USING (VALUES 
(1,'Higher Emissions','H')
,(2,'Lower Emissions','L')
)
AS SOURCE([SensitivityImpactTypeID],[SensitivityImpactTypeDesc],[SensitivityImpactTypeAbbrev])
ON Target.[SensitivityImpactTypeID]= Source.[SensitivityImpactTypeID]
WHEN MATCHED THEN UPDATE SET [SensitivityImpactTypeDesc]=Source.[SensitivityImpactTypeDesc],[SensitivityImpactTypeAbbrev]=Source.[SensitivityImpactTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([SensitivityImpactTypeID],[SensitivityImpactTypeDesc],[SensitivityImpactTypeAbbrev])
VALUES ([SensitivityImpactTypeID],[SensitivityImpactTypeDesc],[SensitivityImpactTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Projection.tSensitivityType AS TARGET USING (VALUES 
(1,'Load')
,(2,'Fuel')
,(3,'Unit')
,(4,'Removal Efficiency')
)
AS SOURCE([SensitivityTypeID],[SensitivityTypeDesc])
ON Target.[SensitivityTypeID]= Source.[SensitivityTypeID]
WHEN MATCHED THEN UPDATE SET [SensitivityTypeDesc]=Source.[SensitivityTypeDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([SensitivityTypeID],[SensitivityTypeDesc])
VALUES ([SensitivityTypeID],[SensitivityTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
PRINT 'Populating Projection.tUIItem';
MERGE INTO Projection.tUIItem AS TARGET USING (VALUES 
(1,'SO2_ton','SO2')
,(2,'NOx_ton','NOx')
,(3,'NOxSeasonal_ton','NOx-S')
,(4,'Hg_lbm','Hg')
,(5,'CO2_ton','CO2')
,(6,'NetGeneration_mwh','MWH')
,(7,'NetPlantHeatRate_btukwh','HR')
,(8,'SO2InRate_lbmmbtu','SO2in')
,(9,'SO2OutRate_lbmmbtu','SO2out')
,(10,'SO2Removal_percent','SO2rem')
,(11,'NOXRate_lbmmbtu','NOXout')
,(12,'CO2Rate_lbmmbtu','CO2out')
,(13,'HgOutRate_lbmtbtu','HgOut')
,(14,'HgRateMonth_oztbtu','Hg1Mo')
,(15,'HgRate3Month_oztbtu','Hg3Mo')
,(16,'HgRate12Month_oztbtu','Hg12Mo')
,(17,'HgInRate_oztbtu','HgIn')
,(18,'HgRemoval_percent','HgRem')
,(19,'Opacity_percent','Opactiy')
,(20,'FDFanMargin_percent','FDFan')
,(21,'PAFanMargin_percent','PAFan')
,(22,'IDFanMargin_percent','IDFan')
,(23,'SBFanMargin_percent','SBFan')
,(24,'MillCapacity_percent','MillCapacity')
,(25,'MillDrying_percent','MillDrying')
,(26,'SlaggingMargin_percent','Slagging')
,(27,'FoulingMargin_percent','Fouling')
,(28,'CycloneAcceptability_percent','CycloneAccept')
,(29,'EquivalentAvailability_percent','EAF')
,(30,'AshProduction_tonhr','Ash')
,(31,'GypsumProduction_tonhr','Gypsum')
,(32,'FurnaceCleanliness_fraction','FurnaceCF')
,(33,'EconomizerCleanliness_fraction','EconCF')
,(34,'PrimarySuperheaterCleanliness_fraction','PrimarySHCF')
,(35,'PlatenSuperheaterCleanliness_fraction','PlatenSHCF')
,(36,'SecondarySuperheaterCleanliness_fraction','SecSHCF')
,(37,'ReheaterCleanliness_fraction','RHCleanFrac')
,(38,'SecondaryReheaterCleanliness_fraction','SecRHCleanFrac')
,(39,'CO_ton','CO')
,(40,'VOC_ton','VOC')
,(41,'PM_ton','PM')
,(42,'SO2Rate_lbmmwh','SO2Out2')
,(43,'PMRate_lbmmbtu','PMOut1')
,(44,'PMRate_lbmmwh','PMOut2')
,(45,'HgOutRate_lbmtbtu (low rank)','HgOut1 (low rank)')
,(46,'HgOutRate_lbmgwh (low rank)','HgOut2 (low rank)')
,(47,'HgOutRate_lbmtbtu (high rank)','HgOut1 (high rank)')
,(48,'HgOutRate_lbmgwh (high rank)','HgOut2 (high rank)')
,(49,'HHV_btulbm','HHV')
,(50,'ShutDown GrossMargin_MWh','ShutDown GM_MWh')
,(51,'HCl_lbmmbtu','HCl_lbmmbtu')
,(52,'HCl_lbmmwh','HCl_lbmmwh')
,(53,'StokerAcceptability_percent','StokerAccept')
,(54,'StokerMargin_percent','StokerMargin')
,(55,'CFBMargin_percent','CFBMargin')
,(56,'SCRMargin_percent','SCRMargin')
,(57,'ESPMargin_percent','ESPMargin')
,(58,'FabricFilterMargin_percent','FabricFilterMargin')
,(59,'ScrubberMargin_percent','ScrubberMargin')
,(60,'CORate_lbmmbtu','CO_lbmmbtu')
)
AS SOURCE([UIItemID],[UIItemDesc],[UIItemAbbrev])
ON Target.[UIItemID]= Source.[UIItemID]
WHEN MATCHED THEN UPDATE SET [UIItemDesc]=Source.[UIItemDesc],[UIItemAbbrev]=Source.[UIItemAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([UIItemID],[UIItemDesc],[UIItemAbbrev])
VALUES ([UIItemID],[UIItemDesc],[UIItemAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
PRINT 'Populating Projection.tUITargetType';
MERGE INTO Projection.tUITargetType AS TARGET USING (VALUES 
(0,'Annual Total Limit','Annual')
,(1,'12-Month Rolling Average Rate','12-Month Rate')
,(2,'3-Month Rolling Average Rate','3-Month Rate')
,(3,'1-Month Rolling Average Rate','1-Month Rate')
,(4,'Forward Fuel Margin','Forward Fuel')
,(5,'Historical Fuel','Historical Fuel')
,(6,'CSAPR Limit','CSAPR')
,(7,'Economic Thresholds','Econ Thresholds')
,(8,'CSAPR Allocation','CSAPR Allocation')
,(9,'CSAPR Account Balance','CSAPR Account Balance')
)
AS SOURCE([UITargetTypeID],[UITargetTypeDesc],[UITargetTypeAbbrev])
ON Target.[UITargetTypeID]= Source.[UITargetTypeID]
WHEN MATCHED THEN UPDATE SET [UITargetTypeDesc]=Source.[UITargetTypeDesc],[UITargetTypeAbbrev]=Source.[UITargetTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([UITargetTypeID],[UITargetTypeDesc],[UITargetTypeAbbrev])
VALUES ([UITargetTypeID],[UITargetTypeDesc],[UITargetTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
PRINT 'Populating Projection.tUITimeFrame';
MERGE INTO Projection.tUITimeFrame AS TARGET USING (VALUES 
(1,'EOY','EOY')
,(2,'MTD','MTD')
,(3,'YTD','YTD')
)
AS SOURCE([UITimeFrameID],[UITimeFrameDesc],[UITimeFrameAbbrev])
ON Target.[UITimeFrameID]= Source.[UITimeFrameID]
WHEN MATCHED THEN UPDATE SET [UITimeFrameDesc]=Source.[UITimeFrameDesc],[UITimeFrameAbbrev]=Source.[UITimeFrameAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([UITimeFrameID],[UITimeFrameDesc],[UITimeFrameAbbrev])
VALUES ([UITimeFrameID],[UITimeFrameDesc],[UITimeFrameAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

PRINT 'Populating Projection.tUnitEquipmentStateType';
MERGE INTO Projection.tUnitEquipmentStateType AS TARGET USING (VALUES 
(1,'Net Turbine Heat Rate','Btu/kWh',0)
,(2,'Primary Air to Flue Gas Leakage','%',0)
,(3,'Secondary Air to Flue Gas Leakage','%',0)
,(4,'FGD Removal Efficiency','%',0)
,(5,'SCR Removal Efficiency','%',0)
,(6,'SNCR Removal Efficiency','%',0)
,(7,'PAC Removal Efficiency','%',0)
,(8,'Mercury Stack Rate','lbm/TBtu',0)
,(9,'NOx Stack Rate','lbm/MBtu',0)
,(10,'Fuel Additive SO2 Removal Efficiency','%',1)
,(11,'Fuel Additive SO3 Removal Efficiency','%',1)
,(12,'Fuel Additive NOx Removal Efficiency','%',1)
,(13,'Fuel Additive Hg Removal Efficiency','%',1)
,(14,'Fuel Additive HCl Removal Efficiency','%',1)
,(17,'Stack SO2 Limit','lbm/MBtu',0)
,(18,'FGD Bypass Level','%',0)
,(19,'ESP Additive Injection Rate','ppm',0)
,(20,'ESP Removal Efficiency','%',0)
,(21,'CFB Boiler Target SO2 Outlet','lbm/MBtu',0)
,(22,'CFB Boiler Target SO2 Removal Efficiency','%',0)
,(23,'Baghouse Bypass Option: Fixed Bypass Level','%',0)
,(24,'Baghouse Bypass Option: Adjust to Meet Target dp (wg)','wg',0)
,(25,'Baghouse Number of Compartments','',0)
,(26,'SNCR Off','OnOff',0)
,(27,'SCR Off','OnOff',0)
,(28,'Baghouse Gas Pressure Drop','inwg',0)
)
AS SOURCE([UnitEquipmentStateTypeID],[UnitEquipmentStateTypeDesc],[DefaultUnits],[IsAppliedToFuel])
ON Target.[UnitEquipmentStateTypeID]= Source.[UnitEquipmentStateTypeID]
WHEN MATCHED THEN UPDATE SET [UnitEquipmentStateTypeDesc]=Source.[UnitEquipmentStateTypeDesc],[DefaultUnits]=Source.[DefaultUnits],[IsAppliedToFuel]=Source.[IsAppliedToFuel]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([UnitEquipmentStateTypeID],[UnitEquipmentStateTypeDesc],[DefaultUnits],[IsAppliedToFuel])
VALUES ([UnitEquipmentStateTypeID],[UnitEquipmentStateTypeDesc],[DefaultUnits],[IsAppliedToFuel])
WHEN NOT MATCHED BY SOURCE THEN DELETE; 


PRINT 'Populating Projection.tScenarioType';
MERGE INTO Projection.tScenarioType AS TARGET USING(VALUES
(1,	'Full'),
(2,	'Partial'),
(3,	'Lite')
)
AS SOURCE(ScenarioTypeID,ScenarioTypeDesc)
ON Target.ScenarioTypeID= Source.ScenarioTypeID
WHEN MATCHED THEN UPDATE SET ScenarioTypeDesc=Source.ScenarioTypeDesc
WHEN NOT MATCHED BY TARGET THEN 
INSERT (ScenarioTypeID,ScenarioTypeDesc)
VALUES (ScenarioTypeID,ScenarioTypeDesc);
GO

PRINT 'Populating [Projection].[tInputTypeLegacyDomain]';
MERGE INTO [Projection].[tInputTypeLegacyDomain] AS TARGET USING ( VALUES
 (1,'Regulatory Administration')
,(2,'Unit Plan')
,(3,'Demand Plan')
,(4,'Fuel Plan')
)
AS SOURCE ([InputTypeLegacyDomainID],[InputTypeLegacyDomainDesc])
ON TARGET.InputTypeLegacyDomainID = SOURCE.InputTypeLegacyDomainID
WHEN MATCHED THEN UPDATE SET [InputTypeLegacyDomainDesc]=SOURCE.[InputTypeLegacyDomainDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT([InputTypeLegacyDomainID],[InputTypeLegacyDomainDesc])
VALUES([InputTypeLegacyDomainID],[InputTypeLegacyDomainDesc])
;




DECLARE @DollarsPerMWhVariableTypeID int;
SELECT @DollarsPerMWhVariableTypeID = PDVariableID
FROM ProcessData.tVariableType
WHERE EngUnits = '$/MWh'

DECLARE @SO2ProdVarTypeID int, @NOXProdVarTypeID int;
SELECT top 1 @SO2ProdVarTypeID = [PDVariableID]
FROM [ProcessData].[tVariableType]
WHERE EngUnits like 'tons' AND VariableName like 'SO2Production';
SELECT top 1 @NOXProdVarTypeID = [PDVariableID]
FROM [ProcessData].[tVariableType]
WHERE EngUnits like 'tons' AND VariableName like 'NOXProduction';


--Load tInputType with the new regulatory types.
MERGE INTO [Projection].[tInputType] AS TARGET USING (VALUES
		   ( 1,	2, 'PACRemovalEfficiency',					'PAC Removal Efficiency',				NULL,	4001,	NULL)
		  ,( 2,	2, 'FGDRemovalEfficiency',					'FGD: Removal Efficiency',				NULL,	4001,	NULL)
		  ,( 3,	2, 'FGDBypassLevel',						'FGD: Fixed Bypass',					NULL,	4001,	NULL)
		  ,( 4,	2, 'ESPRemovalEfficiency',					'ESP: Removal Efficiency',				NULL,	4001,	NULL)
		  ,( 5,	2, 'BaghouseBypassFixedLevel',				'Baghouse Bypass Option: Fixed Bypass',	NULL,	4001,	NULL)
		  ,( 6,	2, 'BaghouseGasPressureDrop',				'Baghouse: Gas Pressure Drop',			NULL,	4001,	NULL)
		  ,( 7,	2, 'BaghouseNumberCompartmentsOOS',			'Baghouse: Number of Bags OOS',			NULL,	4001,	NULL)
		  ,( 8,	3, 'Demand',								'Demand',								NULL,	4001,	NULL)
		  ,( 9, 1, 'CsaprSO2AccountBalance_Coal',			'CSAPR Account Balance - SO2',		@SO2ProdVarTypeID,4001,null)
		  ,(10, 1, 'CsaprNOxAccountBalance_Coal',			'CSAPR Account Balance - NOx ',		@NOXProdVarTypeID,4001,null)
		  ,(11, 1, 'CsaprSeasonalNOxAccountBalance_Coal',	'CSAPR Account Balance - NOx-S',	@NOXProdVarTypeID,4001,null)
		  ,(12, 1, 'CsaprSO2AccountBalance_GasTurb',		'CSAPR Account Balance - SO2',		@SO2ProdVarTypeID,4003,null)
		  ,(13, 1, 'CsaprNOxAccountBalance_GasTurb',		'CSAPR Account Balance - NOx ',		@NOXProdVarTypeID,4003,null)
		  ,(14, 1, 'CsaprSeasonalNOxAccountBalance_GasTurb','CSAPR Account Balance - NOx-S',	@NOXProdVarTypeID,4003,null)
		  ,(15, 1, 'CsaprSO2AccountBalance_CCGasTurb',		'CSAPR Account Balance - SO2',		@SO2ProdVarTypeID,4002,null)
		  ,(16, 1, 'CsaprNOxAccountBalance_CCGasTurb',		'CSAPR Account Balance - NOx ',		@NOXProdVarTypeID,4002,null)
		  ,(17, 1, 'CsaprSeasonalNOxAccountBalance_CCGasTur','CSAPR Account Balance - NOx-S',	@NOXProdVarTypeID,4002,null)
		  ,(18, 1, 'CsaprSO2AllocationEPA_Coal',			'CSAPR EPA Allocation - SO2',		@SO2ProdVarTypeID,4001,null)
		  ,(19, 1, 'CsaprNOxAllocationEPA_Coal',			'CSAPR EPA Allocation - NOx ',		@NOXProdVarTypeID,4001,null)
		  ,(20, 1, 'CsaprSeasonalNOxAllocationEPA_Coal',	'CSAPR EPA Allocation - NOx-S',		@NOXProdVarTypeID,4001,null)
		  ,(21, 1, 'CsaprSO2AllocationEPA_GasTurb',			'CSAPR EPA Allocation - SO2',		@SO2ProdVarTypeID,4003,null)
		  ,(22, 1, 'CsaprNOxAllocationEPA_GasTurb',			'CSAPR EPA Allocation - NOx ',		@NOXProdVarTypeID,4003,null)
		  ,(23, 1, 'CsaprSeasonalNOxAllocationEPA_GasTurb',	'CSAPR EPA Allocation - NOx-S',		@NOXProdVarTypeID,4003,null)
		  ,(24, 1, 'CsaprSO2AllocationEPA_CCGasTurb',		'CSAPR EPA Allocation - SO2',		@SO2ProdVarTypeID,4002,null)
		  ,(25, 1, 'CsaprNOxAllocationEPA_CCGasTurb',		'CSAPR EPA Allocation - NOx ',		@NOXProdVarTypeID,4002,null)
		  ,(26, 1, 'CsaprSeasonalNOxAllocationEPA_CCGasTurb','CSAPR EPA Allocation - NOx-S',	@NOXProdVarTypeID,4002,null)
		  ,(27, 1, 'MatsRollingAverageLimitSO2_heat',		'MATS 30-Day Rolling Average Limit - SO2',null,4001,null)
		  ,(28, 1, 'MatsRollingAverageLimitHg_lowRank_heat','MATS 30-Day Rolling Average Limit - Hg (Low Rank)',null,4001,null)
		  ,(29, 1, 'MatsRollingAverageLimitHg_highRank_heat','MATS 30-Day Rolling Average Limit - Hg (High Rank)',null,4001,null)
		  ,(30, 1, 'MatsRollingAverageLimitPM_heat',		'MATS 30-Day Rolling Average Limit - PM',null,4001,null)
		  ,(31, 1, 'MatsRollingAverageLimitHCl_heat',		'MATS 30-Day Rolling Average Limit - HCl',null,4001,null)		  
		  ,(32, 2, 'AmbientHumidity',					'Ambient Humidity',							null,				3000,null)
          ,(33, 2, 'AmbientTemperature',				'Ambient Temperature',						null,				3000,null)
          ,(34, 2, 'BaghouseBypassMeetTargetdP',		'Baghouse Bypass Option: Meet Target dP',	null,				4001,null)
          ,(35, 2, 'CfbSO2OutletTarget',				'CFB: Target SO2 at Outlet',				null,				4001,null)
          ,(36, 2, 'CfbSO2RemovalEfficiencyTarget',		'CFB: Target SO2 Removal Efficiency',		null,				4001,null)
          ,(37, 2, 'CirculatingWaterTemperature',		'Circulating Water Temperature',			null,				4001,null)
          ,(38, 2, 'NetTurbineHeatRate',				'Net Turbine Heat Rate',					null,				4001,null)
          ,(39, 2, 'PrimaryAirToFlueGasLeakage',		'Primary Air to Flue Gas Leakage',			null,				4001,null)
          ,(40, 2, 'SecondaryAirToFlueGasLeakage',		'Secondary Air to Flue Gas Leakage',		null,				4001,null)
          ,(41, 2, 'ESPAdditiveInjectionRate',			'ESP: Additive Injection Rate',				null,				4001,null)
          --,(42,'RamModelBaselineFuel',				'Baseline RAM Fuel',						null,				4001,null)
          ,(42, 2, 'RamModel',							'RAM Model',								null,				4001,null)
          ,(43, 2, 'VistaModel',						'Vista Model',								null,				4001,null)
          ,(44, 2, 'FlyAshPercentSaleable',				'Fly Ash Sold',								null,				4001,null)
          ,(45, 2, 'BottomAshPercentSaleable',			'Bottom Ash Sold',							null,				4001,null)
          ,(46, 2, 'GypsumPercentSaleable',				'Gypsum Sold',								null,				4001,null)
          ,(47, 2, 'FlyAshSaleableMoisture',			'Saleable Fly Ash Moisture Content',		null,				4001,null)
          ,(48, 2, 'FlyAshNonSaleableMoisture',			'Non-Saleable Fly Ash Moisture Content',	null,				4001,null)
          ,(49, 2, 'FGDAshMoisture',					'FGD Ash Moisture Content',					null,				4001,null)
          ,(50, 2, 'BottomAshMoisture',					'Bottom Ash Moisture Content',				null,				4001,null)
          ,(51, 2, 'FlyAshSaleableMaxAnnualTons',		'Maximum Annual Fly Ash Sold',				null,				4001,null)
          ,(52, 2, 'BottomAshSaleableMaxAnnualTons',	'Maximum Annual Bottom Ash Sold',			null,				4001,null)
          ,(53, 2, 'GypsumSaleableMaxAnnualTons',		'Maximum Annual Gypsum Sold',				null,				4001,null)
          ,(54, 2, 'FuelAdditiveHClRemovalEfficiency',	'Fuel Additive: HCl Removal Efficiency',	null,				4001,null)
          ,(55, 2, 'FuelAdditiveHgRemovalEfficiency',	'Fuel Additive: Hg Removal Efficiency',		null,				4001,null)
          ,(56, 2, 'FuelAdditiveNOxRemovalEfficiency',	'Fuel Additive: NOx Removal Efficiency',	null,				4001,null)
          ,(57, 2, 'FuelAdditiveSO2RemovalEfficiency',	'Fuel Additive: SO2 Removal Efficiency',	null,				4001,null)
          ,(58, 2, 'FuelAdditiveSO3RemovalEfficiency',	'Fuel Additive: SO3 Removal Efficiency',	null,				4001,null)
          ,(59, 2, 'MercuryStackRate',					'Stack: Mercury Limit',						null,				4001,null)
          ,(60, 2, 'NOxStackRate',						'Stack: NOx Limit',							null,				4001,null)
          ,(61, 2, 'SCROff',							'SCR: Is Turned Off?',						null,				4001,null)
          ,(62, 2, 'SNCROff',							'SNCR: Is Turned Off?',						null,				4001,null)
          ,(63, 2, 'StackSO2Limit',						'Stack: SO2 Limit',							null,				4001,null)
		  ,(64,	4,	'Fuel',								'Fuel',										NULL,				4001,	NULL)
		  ,(65, 1, 'UnallocatedAllowancesSO2',			'Unallocated Allowances - SO2',				@DollarsPerMWhVariableTypeID,				1000,null)
		  ,(66, 1, 'UnallocatedAllowancesNOx',			'Unallocated Allowances - NOx',				@DollarsPerMWhVariableTypeID,				1000,null)
		  ,(67, 1, 'UnallocatedAllowancesNOxS',			'Unallocated Allowances - NOx-S',			@DollarsPerMWhVariableTypeID,				1000,null)
)
AS SOURCE  ([InputTypeID],[InputTypeLegacyDomainID],[InputTypeAbbrev],[InputTypeDesc],[VariableTypeID],[AssetClassTypeID],[ValueTypeID])
ON TARGET.[InputTypeID] = SOURCE.[InputTypeID]
WHEN MATCHED THEN UPDATE SET
[InputTypeID]=Source.[InputTypeID],
[InputTypeAbbrev]=Source.[InputTypeAbbrev],
[InputTypeDesc]=Source.[InputTypeDesc],
[VariableTypeID]=Source.[VariableTypeID],
[AssetClassTypeID]=Source.[AssetClassTypeID],
[ValueTypeID]=Source.[ValueTypeID]
WHEN NOT MATCHED BY TARGET THEN
INSERT([InputTypeID],[InputTypeAbbrev],[InputTypeDesc],[VariableTypeID],[AssetClassTypeID],[ValueTypeID])
VALUES([InputTypeID],[InputTypeAbbrev],[InputTypeDesc],[VariableTypeID],[AssetClassTypeID],[ValueTypeID]);


MERGE INTO [Projection].[tInputOptionType] AS TARGET USING (VALUES
		   (1,'Absolute','Absolute'),
		   (2,'Delta','Delta'),
		   (3,'Percent','Percent')
		   )
AS SOURCE  ([InputOptionTypeID],[InputOptionTypeAbbrev],[InputOptionTypeDesc])
ON TARGET.[InputOptionTypeID] = SOURCE.[InputOptionTypeID]
WHEN MATCHED THEN UPDATE SET
[InputOptionTypeID]=Source.[InputOptionTypeID],
[InputOptionTypeAbbrev]=Source.[InputOptionTypeAbbrev],
[InputOptionTypeDesc]=Source.[InputOptionTypeDesc]
WHEN NOT MATCHED BY TARGET THEN
INSERT([InputOptionTypeID],[InputOptionTypeAbbrev],[InputOptionTypeDesc])
VALUES([InputOptionTypeID],[InputOptionTypeAbbrev],[InputOptionTypeDesc]);


MERGE INTO [Projection].[tInputTypeInputOptionTypeMap] AS TARGET USING (VALUES
		    ( 1,	 1,	1,	0,	100,	NULL,	0,	0,	0,	0)
		   ,( 2,	 2,	1,	0,	100,	NULL,	0,	0,	0,	0)
		   ,( 3,	 3,	1,	0,	100,	NULL,	0,	0,	0,	0)
		   ,( 4,	 4,	1,	0,	100,	NULL,	0,	0,	0,	0)
		   ,( 5,	 5,	1,	0,	100,	NULL,	0,	0,	0,	0)
		   ,( 6,	 6,	1,	0,	NULL,	NULL,	0,	0,	0,	0)
		   ,( 7,	 7,	1,	-1,	NULL,	-1,		1,	0,	0,	0)	 
		   ,( 8,	 8,	1,	0,	NULL,	0,		0,	0,	0,	0)	 
		   ,( 9, 9,1,0,null,0,0,0,0,0)
		   ,(10,10,1,0,null,0,0,0,0,0)
		   ,(11,11,1,0,null,0,0,0,0,0)
		   ,(12,12,1,0,null,0,0,0,0,0)
		   ,(13,13,1,0,null,0,0,0,0,0)
		   ,(14,14,1,0,null,0,0,0,0,0)
		   ,(15,15,1,0,null,0,0,0,0,0)
		   ,(16,16,1,0,null,0,0,0,0,0)
		   ,(17,17,1,0,null,0,0,0,0,0)
		   ,(18,18,1,0,null,0,0,0,0,0)
		   ,(19,19,1,0,null,0,0,0,0,0)
		   ,(20,20,1,0,null,0,0,0,0,0)
		   ,(21,21,1,0,null,0,0,0,0,0)
		   ,(22,22,1,0,null,0,0,0,0,0)
		   ,(23,23,1,0,null,0,0,0,0,0)
		   ,(24,24,1,0,null,0,0,0,0,0)
		   ,(25,25,1,0,null,0,0,0,0,0)
		   ,(26,26,1,0,null,0,0,0,0,0)
		   ,(27,27,1,0,null,0,0,0,0,0)
		   ,(28,28,1,0,null,0,0,0,0,0)
		   ,(29,29,1,0,null,0,0,0,0,0)
		   ,(30,30,1,0,null,0,0,0,0,0)
		   ,(31,31,1,0,null,0,0,0,0,0)
           ,(32,32,1,0,100,0,1,1,0,0)
           ,(33,33,1,null,null,0,0,0,0,0)
           ,(34,34,1,0,null,0,0,0,0,0)
           ,(35,35,1,0,null,0,0,0,0,0)
           ,(36,36,1,0,100,0,1,1,0,0)
           ,(37,37,1,null,null,0,0,0,0,0)
           ,(38,38,1,0,null,0,0,0,0,0)
           ,(39,39,1,0,100,0,1,1,0,0)
           ,(40,40,1,0,100,0,1,1,0,0)
           ,(41,41,1,0,null,0,1,0,0,0)
           --,(42,42,1,null,null,0,0,0,1,1) --\
           ,(42,42,1,null,null,0,0,0,1,1) ---these three are drop-down selectors that need special handling.
           ,(43,43,1,null,null,0,0,0,1,1) --/
           ,(44,44,1,0,null,0,1,0,0,0)
           ,(45,45,1,0,null,0,1,0,0,0)
           ,(46,46,1,0,null,0,1,0,0,0)
           ,(47,47,1,0,null,0,1,0,0,0)
           ,(48,48,1,0,null,0,1,0,0,0)
           ,(49,49,1,0,null,0,1,0,0,0)
           ,(50,50,1,0,null,0,1,0,0,0)
           ,(51,51,1,0,null,0,1,0,0,0)
           ,(52,52,1,0,null,0,1,0,0,0)
           ,(53,53,1,0,null,0,1,0,0,0)
           ,(54,54,1,0,100,0,1,1,0,0)
           ,(55,55,1,0,100,0,1,1,0,0)
           ,(56,56,1,0,100,0,1,1,0,0)
           ,(57,57,1,0,100,0,1,1,0,0)
           ,(58,58,1,0,100,0,1,1,0,0)
           ,(59,59,1,0,null,0,0,0,0,0)
           ,(60,60,1,0,null,0,0,0,0,0)
           ,(61,61,1,null,null,2,0,0,1,0)---these two are drop-down selectors that don't need special handling.
           ,(62,62,1,null,null,2,0,0,1,0)--/
           ,(63,63,1,0,null,0,0,0,0,0)
		   ,(64,	64,	1,	1,	NULL,	1,	1,	0,	0,	0)	 
           ,(65,65,1,null,null,0,0,0,0,0)
           ,(66,66,1,null,null,0,0,0,0,0)
           ,(67,67,1,null,null,0,0,0,0,0)
)
AS SOURCE  ([InputTypeInputOptionTypeMapID],[InputTypeID],[InputOptionTypeID],[MinValue],[MaxValue],[DefaultValue],[MinIsInclusive],[MaxIsInclusive],[UsesDropDown],[SpecialDropDownHandling])
ON TARGET.[InputTypeInputOptionTypeMapID] = SOURCE.[InputTypeInputOptionTypeMapID]
WHEN MATCHED THEN UPDATE SET
[InputTypeID]=Source.[InputTypeID],
[InputOptionTypeID]=Source.[InputOptionTypeID],
[MinValue]=Source.[MinValue],
[MaxValue]=Source.[MaxValue],
[DefaultValue]=Source.[DefaultValue],
[MinIsInclusive]=Source.[MinIsInclusive],
[MaxIsInclusive]=Source.[MaxIsInclusive],
[UsesDropDown]=Source.[UsesDropDown],
[SpecialDropDownHandling]=Source.[SpecialDropDownHandling]
WHEN NOT MATCHED BY TARGET THEN
INSERT([InputTypeInputOptionTypeMapID],[InputTypeID],[InputOptionTypeID],[MinValue],[MaxValue],[DefaultValue],[MinIsInclusive],[MaxIsInclusive],[UsesDropDown],[SpecialDropDownHandling])
VALUES([InputTypeInputOptionTypeMapID],[InputTypeID],[InputOptionTypeID],[MinValue],[MaxValue],[DefaultValue],[MinIsInclusive],[MaxIsInclusive],[UsesDropDown],[SpecialDropDownHandling]);
GO

INSERT INTO [Projection].[tRegulatoryInputTypeUIItemUITargetTypeMap]
		   ([InputTypeID]
		   ,[UIItemID]
		   ,[UITargetTypeID])
	 VALUES
		   ( 9,           1           ,9)
		  ,(10,           2           ,9)
		  ,(11,           3           ,9)
		  ,(12,           1           ,9)
		  ,(13,           2           ,9)
		  ,(14,           3           ,9)
		  ,(15,           1           ,9)
		  ,(16,           2           ,9)
		  ,(17,           3           ,9)
		  ,(18,           1           ,8)
		  ,(19,           2           ,8)
		  ,(20,           3           ,8)
		  ,(21,           1           ,8)
		  ,(22,           2           ,8)
		  ,(23,           3           ,8)
		  ,(24,           1           ,8)
		  ,(25,           2           ,8)
		  ,(26,           3           ,8)
		  ,(27,           9           ,3)
		  ,(28,           45          ,3)
		  ,(29,           47          ,3)
		  ,(30,           43          ,3)
		  ,(31,           51          ,3)
          ,(65,			  1			  ,9)
          ,(66,			  2			  ,9)
          ,(67,			  3			  ,9)
GO
--Here's the necessary insertions for the new unit equipment states; with these mappings added, the 
--unit config adjustment engine should seamlessly incorporate these new inputs. The rest will require
--more specialized handling, I fear.
--INSERT INTO [Projection].[tInputTypeUnitEquipmentStateTypeMap]
--			([InputTypeID]	,[UnitEquipmentStateTypeID])
--     VALUES
--			(34			,24	),
--			(35			,21	),
--			(36			,22	),
--			(38			,1	),
--			(39			,2	),
--			(40			,3	),
--			(41			,19	),
--			(54			,14	),
--			(55			,13	),
--			(56			,12	),
--			(57			,10	),
--			(58			,11	),
--			(59			,8	),
--			(60			,9	),
--			(61			,27	),
--			(62			,26	),
--			(63			,17	),
--			(32			,30 ),
--			(33			,29 ),
--			(37			,31 )
--GO

---- Kpi Mapping Table
--SET IDENTITY_INSERT [ProcessData].[tKpiMap] ON

--MERGE INTO [ProcessData].[tKpiMap] AS Target
--USING (VALUES
-- (4,4001,150,1,'Gross MW',NULL,NULL)
-- ,(5,4001,193,2,'Net Unit Heat Rate using HeatLoss Boiler Efficiency',NULL,NULL)
-- ,(6,4001,2005,3,'Net Unit Heat Rate using Input-Output Boiler Efficiency',NULL,NULL)
--) AS Source ([KpiMapID],[AssetClassTypeID],[PDVariableID],[DisplayOrder],[DisplayText],[Min],[Max])
--ON (Target.[KpiMapID] = Source.[KpiMapID])
--WHEN MATCHED THEN UPDATE SET
--  [AssetClassTypeID] = Source.[AssetClassTypeID], 
--  [PDVariableID] = Source.[PDVariableID], 
--  [DisplayOrder] = Source.[DisplayOrder], 
--  [DisplayText] = Source.[DisplayText], 
--  [Min] = Source.[Min], 
--  [Max] = Source.[Max]
--WHEN NOT MATCHED BY TARGET THEN
-- INSERT([KpiMapID],[AssetClassTypeID],[PDVariableID],[DisplayOrder],[DisplayText],[Min],[Max])
-- VALUES(Source.[KpiMapID],Source.[AssetClassTypeID],Source.[PDVariableID],Source.[DisplayOrder],Source.[DisplayText],Source.[Min],Source.[Max])
--WHEN NOT MATCHED BY SOURCE THEN 
-- DELETE
--;
--GO

--SET IDENTITY_INSERT [ProcessData].[tKpiMap] OFF