﻿SET IDENTITY_INSERT AnalytX.tAXGroup ON
GO

PRINT 'Populating AnalytX.tAXGroup';
MERGE INTO AnalytX.tAXGroup AS TARGET USING (VALUES 
(1,'Black and Veatch, Inc./BV Software Developers'),
(2,'Black and Veatch, Inc./BV Solution Leads'),
(3,'Black and Veatch, Inc./BV Application Designers'),
(4,'Black and Veatch, Inc./BV Engineers')
)
AS SOURCE([AXGroupID],[AXGroup])
ON Target.[AXGroupID]= Source.[AXGroupID]
WHEN MATCHED THEN UPDATE SET [AXGroup]=Source.[AXGroup]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([AXGroupID],[AXGroup])
VALUES ([AXGroupID],[AXGroup]);
GO

SET IDENTITY_INSERT AnalytX.tAXGroup OFF
GO

