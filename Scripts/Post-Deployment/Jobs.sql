﻿MERGE INTO [Jobs].[tJobType] AS Target
USING (VALUES
  (1, 'SEKOIA Import', 'SEKOIA Import', 1),
  (2, 'Asset Hierarchy Sync', 'Asset Hierarchy Sync', 2),
  (3, 'Append OPM Data To nD', 'Append OPM Data To nD', 3),
  (4, 'Scenario Calcs Financial Model', 'Scenario Calcs Financial Model', 4),
  (5, 'Risk Scenario Calcs', 'Risk Scenario Calcs', 5),
  (6, 'Sekoia Export', 'Sekoia Export', 6)
) AS Source ([JobTypeId],[JobTypeAbbrev],[JobTypeDesc],[DisplayOrder])
ON (Target.[JobTypeId] = Source.[JobTypeId])
WHEN NOT MATCHED BY TARGET THEN
 INSERT([JobTypeId],[JobTypeAbbrev],[JobTypeDesc],[DisplayOrder])
 VALUES(Source.[JobTypeId],Source.[JobTypeAbbrev],Source.[JobTypeDesc],Source.[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN
 DELETE
;