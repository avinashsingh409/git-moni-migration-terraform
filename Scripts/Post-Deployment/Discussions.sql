﻿IF NOT EXISTS (SELECT * FROM Discussions.tDiscussionAttachmentType WHERE DiscussionAttachmentTypeID = 1)
BEGIN
INSERT INTO Discussions.tDiscussionAttachmentType(DiscussionAttachmentTypeID, DiscussionAttachmentTypeAbbrev, DiscussionAttachmentTypeDesc)
VALUES(1, 'Image', 'Image')
END
GO

PRINT 'Discussions.tDiscussionAttachmentType';
IF NOT EXISTS (SELECT * FROM Discussions.tDiscussionAttachmentType WHERE DiscussionAttachmentTypeID = 2)
BEGIN
INSERT INTO Discussions.tDiscussionAttachmentType(DiscussionAttachmentTypeID, DiscussionAttachmentTypeAbbrev, DiscussionAttachmentTypeDesc)
VALUES(2, 'File', 'File')
END
GO

IF NOT EXISTS (SELECT * FROM Discussions.tDiscussionAttachmentType WHERE DiscussionAttachmentTypeID = 3)
BEGIN
INSERT INTO Discussions.tDiscussionAttachmentType(DiscussionAttachmentTypeID, DiscussionAttachmentTypeAbbrev, DiscussionAttachmentTypeDesc)
VALUES(3, 'Chart', 'Chart')
END
GO 

IF NOT EXISTS (SELECT * FROM Discussions.tDiscussionAttachmentType WHERE DiscussionAttachmentTypeID = 4)
BEGIN
INSERT INTO Discussions.tDiscussionAttachmentType(DiscussionAttachmentTypeID, DiscussionAttachmentTypeAbbrev, DiscussionAttachmentTypeDesc)
VALUES(4, 'Link', 'Link')
END
GO

IF NOT EXISTS (SELECT * FROM Discussions.tDiscussionAttachmentType WHERE DiscussionAttachmentTypeID = 5)
BEGIN
INSERT INTO Discussions.tDiscussionAttachmentType(DiscussionAttachmentTypeID, DiscussionAttachmentTypeAbbrev, DiscussionAttachmentTypeDesc)
VALUES(5, 'Video', 'Video')
END
GO