﻿SET IDENTITY_INSERT [GeoSpa].[tAttributeSourceType] ON;
GO
MERGE INTO [GeoSpa].[tAttributeSourceType] AS TARGET
USING (VALUES
	(1, 'Sql', 'Data stored in sql table')
	, (2, 'GeoSpa', 'Data embedded within GeoSpa source')
) AS Source([AttributeSourceTypeID], [AttributeSourceTypeKey], [AttributeSourceTypeDesc])
ON (Target.[AttributeSourceTypeID] = Source.[AttributeSourceTypeID])
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AttributeSourceTypeID], [AttributeSourceTypeKey], [AttributeSourceTypeDesc])
	VALUES (Source.[AttributeSourceTypeID], Source.[AttributeSourceTypeKey], Source.[AttributeSourceTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;
SET IDENTITY_INSERT [GeoSpa].[tAttributeSourceType] OFF;
GO


SET IDENTITY_INSERT [GeoSpa].[tBaseMapType] ON;
GO
MERGE INTO [GeoSpa].[tBaseMapType] AS TARGET
USING (VALUES
	(1, 'satellite', 'satellite')
	, (2, 'hybrid', 'hybrid')
	, (3, 'topo', 'topo')
	, (4, 'gray', 'gray')
	, (5, 'oceans', 'oceans')
	, (6, 'osm', 'osm')
	, (7, 'national-geographic', 'national-geographic')
	, (8, 'streets', 'streets')
	, (9, 'NotApplicable', 'Not Applicable')
	, (10, 'dark-gray', 'dark-gray')
) AS Source([BaseMapID], [BaseMapKey], [BaseMapDesc])
ON (Target.[BaseMapID] = Source.[BaseMapID])
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([BaseMapID], [BaseMapKey], [BaseMapDesc])
	VALUES (Source.[BaseMapID], Source.[BaseMapKey], Source.[BaseMapDesc])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;
SET IDENTITY_INSERT [GeoSpa].[tBaseMapType] OFF;
GO


SET IDENTITY_INSERT [GeoSpa].[tGeometryType] ON;
GO
MERGE INTO [GeoSpa].[tGeometryType] AS Target
USING (VALUES
  (1,'Point','Point')
 ,(2,'Polyline','Polyline')
 ,(3,'Polygon','Polygon')
) AS Source ([GeometryTypeID],[GeometryTypeKey],[GeometryTypeDesc])
ON (Target.[GeometryTypeID] = Source.[GeometryTypeID])
WHEN NOT MATCHED BY TARGET THEN
 INSERT([GeometryTypeID],[GeometryTypeKey],[GeometryTypeDesc])
 VALUES(Source.[GeometryTypeID],Source.[GeometryTypeKey],Source.[GeometryTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;
SET IDENTITY_INSERT [GeoSpa].[tGeometryType] OFF;
GO





MERGE INTO [GeoSpa].[tGeoSpaRestLayerType] AS TARGET
USING (VALUES
	(1, 'Map Service', 'Map Service')
	, (2, 'AutodeskLMV', 'Autodesk LMV Resource')
) AS Source([GeoSpaRestLayerTypeID], [GeoSpaRestLayerKey], [GeoSpaRestLayerDesc])
ON (Target.[GeoSpaRestLayerTypeID] = Source.[GeoSpaRestLayerTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[GeoSpaRestLayerTypeID] = Source.[GeoSpaRestLayerTypeID]
		, [GeoSpaRestLayerKey] = Source.[GeoSpaRestLayerKey]
		, [GeoSpaRestLayerDesc] = Source.[GeoSpaRestLayerDesc]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([GeoSpaRestLayerTypeID], [GeoSpaRestLayerKey], [GeoSpaRestLayerDesc])
	VALUES (Source.[GeoSpaRestLayerTypeID], Source.[GeoSpaRestLayerKey], Source.[GeoSpaRestLayerDesc])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;




SET IDENTITY_INSERT [GeoSpa].[tGeoSpaSourceType] ON;
GO
MERGE INTO [GeoSpa].[tGeoSpaSourceType] AS TARGET
USING (VALUES
	(1, 'GeoJson', 'Geospatial data stored in GeoJson standard')
	, (2, 'Sql', 'Geospatial data stored in sql table')
	, (3, 'Rest', 'Geospatial data available at rest endpoint')
) AS Source([GeoSpaSourceTypeID], [GeoSpaSourceTypeKey], [GeoSpaSourceTypeDesc])
ON (Target.[GeoSpaSourceTypeID] = Source.[GeoSpaSourceTypeID])
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([GeoSpaSourceTypeID], [GeoSpaSourceTypeKey], [GeoSpaSourceTypeDesc])
	VALUES (Source.[GeoSpaSourceTypeID], Source.[GeoSpaSourceTypeKey], Source.[GeoSpaSourceTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;
SET IDENTITY_INSERT [GeoSpa].[tGeoSpaSourceType] OFF;
GO




SET IDENTITY_INSERT [GeoSpa].[tMapType] ON;
GO
MERGE INTO [GeoSpa].[tMapType] AS TARGET
USING (VALUES
	(1, 'EsriJSAPI', 'Esri JavaScript API', NULL)
	, (2, 'AutodeskLMV', 'Autodesk LMV Viewer', NULL)
	, (3, 'Static Raster', 'Static Raster', NULL)
	, (4, 'GeoVis', 'Geo Visualization', NULL)
) AS Source([MapTypeID], [MapTypeKey], [MapTypeDesc], [Display])
ON (Target.[MapTypeID] = Source.[MapTypeID])
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MapTypeID], [MapTypeKey], [MapTypeDesc], [Display])
	VALUES (Source.[MapTypeID], Source.[MapTypeKey], Source.[MapTypeDesc], Source.[Display])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;
SET IDENTITY_INSERT [GeoSpa].[tMapType] OFF;
GO




MERGE INTO [GeoSpa].[tWidgetType] AS TARGET
USING (VALUES
	(1, 'Clickable Node', 'Clickable Node', 'Displays symbol and opens Asset Info Tray when clicked')
	, (2, 'Process Data Label', 'Process Data Label', 'Displays timstamp-driven process data actual value and metadata')
) AS Source([WidgetTypeID], [WidgetAbbrev], [WidgetDesc], [WidgetBehavior])
ON (Target.[WidgetTypeID] = Source.[WidgetTypeID])
WHEN MATCHED THEN
	UPDATE SET
		[WidgetTypeID] = Source.[WidgetTypeID]
		, [WidgetAbbrev] = Source.[WidgetAbbrev]
		, [WidgetDesc] = Source.[WidgetDesc]
		, [WidgetBehavior] = Source.[WidgetBehavior]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([WidgetTypeID], [WidgetAbbrev], [WidgetDesc], [WidgetBehavior])
	VALUES (Source.[WidgetTypeID], Source.[WidgetAbbrev], Source.[WidgetDesc], Source.[WidgetBehavior])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;