﻿SET IDENTITY_INSERT [Audit].[tAuditCategory] ON

MERGE INTO [Audit].[tAuditCategory] AS Target
USING (VALUES
  (100,'Cashflow')
 ,(200,'Import from data file')
) AS Source ([AuditCategoryId],[AuditCategoryDescription])
ON (Target.[AuditCategoryId] = Source.[AuditCategoryId])
WHEN MATCHED THEN
 UPDATE SET
  [AuditCategoryDescription] = Source.[AuditCategoryDescription]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([AuditCategoryId],[AuditCategoryDescription])
 VALUES(Source.[AuditCategoryId],Source.[AuditCategoryDescription])
WHEN NOT MATCHED BY SOURCE THEN
 DELETE
;
GO

SET IDENTITY_INSERT [Audit].[tAuditCategory] OFF
GO


SET IDENTITY_INSERT [Audit].[tAuditType] ON

MERGE INTO [Audit].[tAuditType] AS Target
USING (VALUES
  (101,100,'Cashflow record modified')
 ,(102,100,'Cashflow record deleted')
 ,(201,200,'Sekoia import file uploaded')
 ,(202,200,'Ad hoc process data import file uploaded')
 ,(203,200,'Arcflash import file uploaded by user')
) AS Source ([AuditTypeId],[AuditCategoryId],[AuditTypeDesc])
ON (Target.[AuditTypeId] = Source.[AuditTypeId])
WHEN MATCHED THEN
 UPDATE SET
  [AuditCategoryId] = Source.[AuditCategoryId], 
  [AuditTypeDesc] = Source.[AuditTypeDesc]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([AuditTypeId],[AuditCategoryId],[AuditTypeDesc])
 VALUES(Source.[AuditTypeId],Source.[AuditCategoryId],Source.[AuditTypeDesc])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;
GO
DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [Audit].[tAuditType]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[Audit].[tAuditType] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO

SET IDENTITY_INSERT [Audit].[tAuditType] OFF
GO