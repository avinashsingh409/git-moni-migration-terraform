﻿SET IDENTITY_INSERT [FuelTracker].[tNodeType] ON
GO
 
MERGE INTO FuelTracker.tNodeType AS TARGET USING (VALUES 
(1,'Source Mine',1.000000000000000e+000)
,(2,'Transloading Facility',2.000000000000000e+000)
,(3,'Unloading Station',3.000000000000000e+000)
,(4,'Pile',4.000000000000000e+000)
,(5,'Limestone Hopper',5.000000000000000e+000)
,(6,'Reclaim Hopper',6.000000000000000e+000)
,(7,'Silo/Bunker',7.000000000000000e+000)
,(8,'Pulverizer',8.000000000000000e+000)
,(9,'Boiler Firebox',9.000000000000000e+000)
,(11,'In-Transit (Barge or Train)',1.000000000000000e+001)
,(13,'Burned',9.500000000000000e+000)
,(14,'Purge from FuelTracker',1.100000000000000e+001)
)
AS SOURCE([FTNodeTypeID],[FTNodeTypeDesc],[FTNodeTypeSortOrder])
ON Target.[FTNodeTypeID]= Source.[FTNodeTypeID]
WHEN MATCHED THEN UPDATE SET [FTNodeTypeDesc]=Source.[FTNodeTypeDesc],[FTNodeTypeSortOrder]=Source.[FTNodeTypeSortOrder]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([FTNodeTypeID],[FTNodeTypeDesc],[FTNodeTypeSortOrder])
VALUES ([FTNodeTypeID],[FTNodeTypeDesc],[FTNodeTypeSortOrder])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

SET IDENTITY_INSERT [FuelTracker].[tNodeType] OFF
GO