﻿MERGE INTO CQIExperience.tCQIExperienceType AS TARGET USING (VALUES 
(1,'Manipulated Variable')
,(2,'Supporting Variable')
,(3,'Control Variable')
)
AS SOURCE([CQIExperienceTypeID],[CQIExperienceDesc])
ON Target.[CQIExperienceTypeID]= Source.[CQIExperienceTypeID]
WHEN MATCHED THEN UPDATE SET [CQIExperienceDesc]=Source.[CQIExperienceDesc]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([CQIExperienceTypeID],[CQIExperienceDesc])
VALUES ([CQIExperienceTypeID],[CQIExperienceDesc])
WHEN NOT MATCHED BY SOURCE THEN DELETE;