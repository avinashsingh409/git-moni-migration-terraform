﻿MERGE INTO Scenario.tScenarioType AS TARGET
USING (VALUES
	(1, 'Financial Model'),
	(2, 'Risk Assessment')
)
AS SOURCE(ScenarioTypeID, ScenarioTypeDesc)
ON TARGET.ScenarioTypeID = Source.ScenarioTypeID
WHEN MATCHED THEN
UPDATE SET
	ScenarioTypeDesc = Source.ScenarioTypeDesc
WHEN NOT MATCHED BY TARGET THEN
INSERT (ScenarioTypeID, ScenarioTypeDesc)
VALUES (ScenarioTypeID, ScenarioTypeDesc)
WHEN NOT MATCHED BY SOURCE THEN DELETE;
