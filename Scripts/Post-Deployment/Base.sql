﻿PRINT 'Started Base.sql.'


MERGE INTO Base.tEventEndPointType AS TARGET USING (VALUES 
(1,'Accounting','Accounting')
,(2,'CMMS','CMMS')
,(3,'File Logger','File')
,(4,'SMTP','Email')
)
AS SOURCE([EventEndPointTypeID],[EventEndPointTypeDesc],[EventEndPointTypeAbbrev])
ON Target.[EventEndPointTypeID]= Source.[EventEndPointTypeID]
WHEN MATCHED THEN UPDATE SET [EventEndPointTypeDesc]=Source.[EventEndPointTypeDesc],[EventEndPointTypeAbbrev]=Source.[EventEndPointTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([EventEndPointTypeID],[EventEndPointTypeDesc],[EventEndPointTypeAbbrev])
VALUES ([EventEndPointTypeID],[EventEndPointTypeDesc],[EventEndPointTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Base.tEventType AS TARGET USING (VALUES 
(1,'PowerRAM Evaluation','PowerRAM',0,0,NULL,NULL,NULL)
,(2,'Scenario','Scenario',0,0,NULL,NULL,NULL)
,(3,'CQI','CQI',0,0,NULL,NULL,NULL)
,(4,'Company Processing','Company',1,30,NULL,NULL,NULL)
,(5,'Invoice Processing','Invoice',1,30,NULL,NULL,NULL)
,(6,'Invoice Check Processing','Invoice Check',1,30,NULL,NULL,NULL)
,(7,'General Ledger Transaction Processing','GL Transaction',1,30,NULL,NULL,NULL)
,(8,'General Ledger Account Processing','GL Account',1,30,NULL,NULL,NULL)
,(9,'Event Queue Notification Processing','Email',0,0,4,'',NULL)
)
AS SOURCE([EventTypeID],[EventTypeDesc],[EventTypeAbbrev],[Archive],[ArchiveDuration_day],[EventEndPointTypeID],[EventEndPointConnection],[ArchiveExpiration_day])
ON Target.[EventTypeID]= Source.[EventTypeID]
WHEN MATCHED THEN UPDATE SET [EventTypeDesc]=Source.[EventTypeDesc],[EventTypeAbbrev]=Source.[EventTypeAbbrev],[Archive]=Source.[Archive],[ArchiveDuration_day]=Source.[ArchiveDuration_day],[EventEndPointTypeID]=Source.[EventEndPointTypeID],[EventEndPointConnection]=Source.[EventEndPointConnection],[ArchiveExpiration_day]=Source.[ArchiveExpiration_day]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([EventTypeID],[EventTypeDesc],[EventTypeAbbrev],[Archive],[ArchiveDuration_day],[EventEndPointTypeID],[EventEndPointConnection],[ArchiveExpiration_day])
VALUES ([EventTypeID],[EventTypeDesc],[EventTypeAbbrev],[Archive],[ArchiveDuration_day],[EventEndPointTypeID],[EventEndPointConnection],[ArchiveExpiration_day])
WHEN NOT MATCHED BY SOURCE THEN DELETE;
 
 
MERGE INTO Base.tEventStatusType AS TARGET USING (VALUES 
(1,'Canceled','Canceled')
,(2,'Queued For Processing','Queued')
,(3,'In Progress','Processing')
,(4,'Success','Success')
,(5,'Failed','Failed')
)
AS SOURCE([EventStatusTypeID],[EventStatusTypeDesc],[EventStatusTypeAbbrev])
ON Target.[EventStatusTypeID]= Source.[EventStatusTypeID]
WHEN MATCHED THEN UPDATE SET [EventStatusTypeDesc]=Source.[EventStatusTypeDesc],[EventStatusTypeAbbrev]=Source.[EventStatusTypeAbbrev]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([EventStatusTypeID],[EventStatusTypeDesc],[EventStatusTypeAbbrev])
VALUES ([EventStatusTypeID],[EventStatusTypeDesc],[EventStatusTypeAbbrev])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

PRINT 'Populating Base.tUnitMeasure';
MERGE INTO Base.tUnitMeasure AS TARGET USING(VALUES
	(1 , 'cost', '$'),
	(2 , 'cost_day', '$/day'),
	(3 , 'cost_ton', '$/ton'),
	(4 , 'cent_Mbtu', '¢/Mbtu'),
	(5 , 'cost_lb', '$/lb'),
	(6 , 'cost_gal', '$/gal'),
	(7 , 'cost_kgal', '$/kgal'),
	(8 , 'cost_Wh', '$/W·h'),
	(9 , 'cost_kWh', '$/kW·h'),
	(10 , 'cost_MWh', '$/MW·h'),
	(11 , 'degF', '°F'),
	(12 , 'acfm', 'acfm'),
	(13 , 'amp', 'amp'),
	(14 , 'btu_ft2degF', 'btu/ft²·°F'),
	(15 , 'btu_kWh', 'btu/kW·h'),
	(16 , 'btu_lb', 'btu/lb'),
	(17 , 'cfm', 'cfm'),
	(18 , 'ft', 'ft'),
	(19 , 'ft_sec', 'ft/s'),
	(20 , 'gal', 'gal'),
	(21 , 'gal_hr', 'gal/h'),
	(22 , 'gal_min', 'gal/min'),
	(23 , 'GWh', 'GW·h'),
	(24 , 'hp', 'hp'),
	(25 , 'kgal', 'kgal'),
	(26 , 'klb_h', 'klb/h'),
	(27 , 'kton', 'kton'),
	(28 , 'lb_hr', 'lb/h'),
	(29 , 'lb', 'lb'),
	(30 , 'lb_gal', 'lb/gal'),
	(31 , 'lb_Mbtu', 'lb/Mbtu'),
	(32 , 'Mbtu', 'Mbtu'),
	(33 , 'Mbtu_hr', 'mbtu/h'),
	(34 , 'MW', 'MW'),
	(35 , 'MWh', 'MW·h'),
	(36 , 'ppm', 'ppm'),
	(37 , 'psia', 'psia'),
	(38 , 'ton', 'ton'),
	(39 , 'ton_hr', 'ton/h'),
	(40 , 'W', 'W'))
AS SOURCE(ID,CommonDesc,DisplayDesc)
ON Target.ID= Source.ID
WHEN MATCHED THEN UPDATE SET CommonDesc=Source.CommonDesc, DisplayDesc=Source.DisplayDesc
WHEN NOT MATCHED BY TARGET THEN 
INSERT (ID,CommonDesc,DisplayDesc)
VALUES (ID,CommonDesc,DisplayDesc);

PRINT 'Populating Base.tCertifiedQueryParamType';
MERGE INTO Base.tCertifiedQueryParamType AS TARGET USING (VALUES 
	 (1, 'string')
	,(2, 'Guid')
	,(3, 'DateTime')
	,(4, 'int')
	,(5, 'bool')
	,(6, 'float')
)
AS SOURCE([TypeID],[Description])
ON Target.[TypeID]= Source.[TypeID]
WHEN MATCHED THEN UPDATE SET [Description]=Source.[Description]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([TypeID],[Description])
VALUES ([TypeID],[Description])
WHEN NOT MATCHED BY SOURCE THEN DELETE;

PRINT 'Finished Base.sql.'