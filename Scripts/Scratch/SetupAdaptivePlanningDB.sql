﻿/*
This script was created by Visual Studio on 11/19/2012 at 9:44 AM.
Run this script on [NADL214753.UnitTestDBBlank] to make it the same as [NADL214753.TestDB].
This script performs its actions in the following order:
1. Disable foreign-key constraints.
2. Perform DELETE commands. 
3. Perform UPDATE commands.
4. Perform INSERT commands.
5. Re-enable foreign-key constraints.
Please back up your target database before running this script.
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
/*Pointer used for text / image updates. This might not be needed, but is declared here just in case*/
DECLARE @pv binary(16)
BEGIN TRANSACTION
ALTER TABLE [Projection].[tDataCommentDetail] DROP CONSTRAINT [FK_tDataCommentDetail_DataCommentSetID_tDataCommentSet]
ALTER TABLE [Projection].[tCQIResultFabricFilterPerformance] DROP CONSTRAINT [FK_tCQIResultFabricFilterPerformance_CQIResultID_tCQIResult]
ALTER TABLE [CQIIPM].[tCQIResultIPMCyclonePerformance] DROP CONSTRAINT [FK_tCQIResultIPMCyclonePerformance_CQIResultID_tCQIResult]
ALTER TABLE [FuelTracker].[tFuelQualityCollectorThermoSourceMap] DROP CONSTRAINT [FK_FuelTrackertFuelQualityCollectorThermoSourceMap_FTFuelQualityCollectorID_FuelTrackertFTFuelQualityCollector]
ALTER TABLE [FuelTracker].[tFuelQualityCollectorThermoSourceMap] DROP CONSTRAINT [FK_FuelTrackertFuelQualityCollectorThermoSourceMap_FuelSourceID_FueltFuelSource]
ALTER TABLE [Projection].[tScenarioRun] DROP CONSTRAINT [FK_tScenarioRun_ScenarioID_tScenario]
ALTER TABLE [PowerRAM].[tRAMSystemFile] DROP CONSTRAINT [FK_tRAMSystemFile_RAMModelID_tRAMModel]
ALTER TABLE [Projection].[tCQIResultBoilerPerformance] DROP CONSTRAINT [FK_tCQIResultBoilerPerformance_CQIResultID_tCQIResult]
ALTER TABLE [CQIIPM].[tCQIResultIPMTurbinePerformance] DROP CONSTRAINT [FK_tCQIResultIPMTurbinePerformance_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tGenerationSetRoleMap] DROP CONSTRAINT [FK_tGenerationSetRoleMap_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationSetRoleMap] DROP CONSTRAINT [FK_tGenerationSetRoleMap_SecurityRoleID_tSecurityRole]
ALTER TABLE [FuelTracker].[tFuelQualityOPMMap] DROP CONSTRAINT [FK_tFTFuelQualityOPMMap_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tUINavTreeNodeMap] DROP CONSTRAINT [FK_tUIFTNavTreeNodeMap_FTNodeID_tFTNode]
ALTER TABLE [FuelTracker].[tUINavTreeNodeMap] DROP CONSTRAINT [FK_tUIFTNavTreeNodeMap_UIFTNavTreeID_tUIFTNavTree]
ALTER TABLE [FuelTracker].[tNodePathExternalSource] DROP CONSTRAINT [FK_tFTNodePathExternalSource_FTNodePathID_tFTNodePath]
ALTER TABLE [FuelTracker].[tNodePathExternalSource] DROP CONSTRAINT [FK_tFTNodePathExternalSource_FuelSourceID_tFuelSource]
ALTER TABLE [EnergyStorage].[tPerformanceTimeslice] DROP CONSTRAINT [FKey_tPerformanceSet_SetID]
ALTER TABLE [Projection].[tCQIResultMillPerformance] DROP CONSTRAINT [FK_tCQIResultMillPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Diagnostics].[tAssetIssueAssetIssueImpactTypeMap] DROP CONSTRAINT [FK_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueAssetIssueImpactTypeMap_tAssetIssue]
ALTER TABLE [Diagnostics].[tAssetIssueAssetIssueImpactTypeMap] DROP CONSTRAINT [FK_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueImpactType]
ALTER TABLE [Diagnostics].[tAssetIssueImpactType] DROP CONSTRAINT [FK_tAssetIssueImpactType_tAssetIssueImpactCategoryType]
ALTER TABLE [FuelTracker].[tFuelQualityCollectorStatusSchedule] DROP CONSTRAINT [FK_tFTFuelQualityCollectorStatusSchedule_FTFuelQualityCollectorID_tFTFuelQualityCollector]
ALTER TABLE [Projection].[tUITargetStationDetail] DROP CONSTRAINT [FK_tUITargetStationDetail_StationID_tStation]
ALTER TABLE [Projection].[tUITargetStationDetail] DROP CONSTRAINT [FK_tUITargetStationDetail_UIItemID_tUIItem]
ALTER TABLE [Projection].[tUITargetStationDetail] DROP CONSTRAINT [FK_tUITargetStationDetail_UITargetSetID_tUITargetSet]
ALTER TABLE [Projection].[tUITargetStationDetail] DROP CONSTRAINT [FK_tUITargetStationDetail_UITargetTypeID_tUITargetType]
ALTER TABLE [Projection].[tUITargetUnitDetail] DROP CONSTRAINT [FK_tUITargetUnitDetail_UIItemID_tUIItem]
ALTER TABLE [Projection].[tUITargetUnitDetail] DROP CONSTRAINT [FK_tUITargetUnitDetail_UITargetSetID_tUITargetSet]
ALTER TABLE [Projection].[tUITargetUnitDetail] DROP CONSTRAINT [FK_tUITargetUnitDetail_UITargetTypeID_tUITargetType]
ALTER TABLE [Projection].[tUITargetUnitDetail] DROP CONSTRAINT [FK_tUITargetUnitDetail_UnitID_tUnit]
ALTER TABLE [CQIIPM].[tCQIIPMServerFile] DROP CONSTRAINT [FK_tCQIIPMServerFile_PDServerID_tPDServer]
ALTER TABLE [CQIIPM].[tCQIIPMServerFile] DROP CONSTRAINT [FK_tCQIIPMServerFile_UnitID_tUnit]
ALTER TABLE [Actuals].[tFGDPerformance] DROP CONSTRAINT [FK_tActualFGDPerformance_UnitID_tUnit]
ALTER TABLE [CQIIPM].[tCQIIPMInterfaceTag] DROP CONSTRAINT [FK_tCQIIPMInterfaceTag_UnitID_tUnit]
ALTER TABLE [AccessControl].[tAlias] DROP CONSTRAINT [FK_AccessControl_tAlias_SecurityUserID_AccessControl_tUser]
ALTER TABLE [FuelTracker].[tFuelQualityCollectorTagMap] DROP CONSTRAINT [FK_tFTFuelQualityCollectorTagMap_FTFuelQualityCollectorID_tFTFuelQualityCollector]
ALTER TABLE [FuelTracker].[tInTransitSource] DROP CONSTRAINT [FK_tFTInTransitSource_FTInTransitID_tFTInTransit]
ALTER TABLE [FuelTracker].[tInTransitSource] DROP CONSTRAINT [FK_tFTInTransitSource_FuelSourceID_tFuelSource]
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndTwoBySeason] DROP CONSTRAINT [FK_tMaintenancePlanAddAndTwoBySeason_MaintenancePlanSetID_tMaintenancePlan]
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndTwoBySeason] DROP CONSTRAINT [FK_tMaintenancePlanAddAndTwoBySeason_UnitID_tUnit]
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndTwoBySeason] DROP CONSTRAINT [FK_tRAMModelComponent_tMaintenancePlanAddAndTwoBySeason_tRAMModel]
ALTER TABLE [Projection].[tScoreboardThresholds] DROP CONSTRAINT [FK_tUIScoreBoard_PlanTypeID_tPlanType]
ALTER TABLE [Projection].[tScoreboardThresholds] DROP CONSTRAINT [FK_tUIScoreBoard_UIItemID_tUIItem]
ALTER TABLE [Projection].[tScoreboardThresholds] DROP CONSTRAINT [FK_tUIScoreBoard_UITimeFrameID_tUITimeFrame]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] DROP CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tFuelPlanAdjustment]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] DROP CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tFuelSource]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] DROP CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tUnit]
ALTER TABLE [Diagnostics].[tAssetIssueRelatedAssetIssue] DROP CONSTRAINT [FK_Diagnostics_tAssetIssueRelatedAssetIssue_AssetIssueID_Diagnostics_tAssetIssue]
ALTER TABLE [Diagnostics].[tAssetIssueRelatedAssetIssue] DROP CONSTRAINT [FK_Diagnostics_tAssetIssueRelatedAssetIssue_RelatedAssetIssueID_Diagnostics_tAssetIssue]
ALTER TABLE [CQIIPM].[tCQIResultIPMAirHeaterPerformance] DROP CONSTRAINT [FK_tCQIResultIPMAirHeaterPerformance_CQIResultID_tCQIResult]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentSet] DROP CONSTRAINT [FK_tFuelPlanAdjustmentSet_tAnalysisSet]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentSet] DROP CONSTRAINT [FK_tFuelPlanAdjustmentSet_tFuelPlanSet]
ALTER TABLE [Monitoring].[tCalculationDataFilter] DROP CONSTRAINT [FK_Monitoring_tCalculationDataFilter_UnitAssetID_tAsset]
ALTER TABLE [Projection].[tGenerationDetail_Adjusted] DROP CONSTRAINT [FK_tGenerationDetail_Adjusted_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationDetail_Adjusted] DROP CONSTRAINT [FK_tGenerationDetail_Adjusted_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tRightAngleNodeMap] DROP CONSTRAINT [FK_tFTRightAngleNodeMap_FTNodeID_LocaleID]
ALTER TABLE [Monitoring].[tModelSnapshot] DROP CONSTRAINT [FK_Monitoring_tModelSnapshot_ModelID_tModel]
ALTER TABLE [ProcessData].[tServerLogin] DROP CONSTRAINT [FK_tServerLogin_ServerID_tServer]
ALTER TABLE [Actuals].[tSCRPerformance] DROP CONSTRAINT [FK_tActualSCRPerformance_UnitID_tUnit]
ALTER TABLE [CQIIPM].[tCQIResultIPMMillPerformance] DROP CONSTRAINT [FK_tCQIResultIPMMillPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tCQIResultESPPerformance] DROP CONSTRAINT [FK_tCQIResultESPPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Monitoring].[tFilterRuleAssociation] DROP CONSTRAINT [FK_Monitoring_tFilterRuleAssociation_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap]
ALTER TABLE [Monitoring].[tFilterRuleAssociation] DROP CONSTRAINT [FK_Monitoring_tFilterRuleAssociation_FilterRuleID_tFilterRule]
ALTER TABLE [CQIIPM].[tCQIResultIPMBoilerSlicePerformance] DROP CONSTRAINT [FK_tCQIResultIPMBoilerSlicePerformance_CQIResultID_tCQIResult]

--ALTER TABLE [Base].[tDataCacheKeyValue] DROP CONSTRAINT [FK_tDataCacheKeyValue_DataCacheID_tDataCache]

ALTER TABLE [Asset].[tExternalSystem] DROP CONSTRAINT [FK_Asset_tExternalSystem_ExternalSystemTypeID_Asset_tExternalSystemType]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputFiles] DROP CONSTRAINT [FK_ttUnitRAMEvaluationOutputFiles_UnitRAMEvaluationPeriodKeyID_tUnitRAMEvaluationPeriodKey]
ALTER TABLE [FuelTracker].[tUINavTree] DROP CONSTRAINT [FK_tUIFTNavTree_UIFTNavTreeSetID_tUIFTNavTreeSet]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputResultsEAFDistribution] DROP CONSTRAINT [FK_tUnitRAMEvaluationOutputResultsEAFDistribution_PowerRAM_tUnitRAMEvaluationPeriodKey]
ALTER TABLE [Projection].[tUITargetStationGroupDetail] DROP CONSTRAINT [FK_tUITargetStationGroupDetail_StationGroupID_tStationGroup]
ALTER TABLE [Projection].[tUITargetStationGroupDetail] DROP CONSTRAINT [FK_tUITargetStationGroupDetail_UIItemID_tUIItem]
ALTER TABLE [Projection].[tUITargetStationGroupDetail] DROP CONSTRAINT [FK_tUITargetStationGroupDetail_UITargetSetID_tUITargetSet]
ALTER TABLE [Projection].[tUITargetStationGroupDetail] DROP CONSTRAINT [FK_tUITargetStationGroupDetail_UITargetTypeID_tUITargetType]
ALTER TABLE [Projection].[tUIThresholdUnitDetail] DROP CONSTRAINT [FK_tUIThresholdUnitDetail_UIItemID_tUIItem]
ALTER TABLE [Projection].[tUIThresholdUnitDetail] DROP CONSTRAINT [FK_tUIThresholdUnitDetail_UITargetSetID_tUITargetSet]
ALTER TABLE [Projection].[tUIThresholdUnitDetail] DROP CONSTRAINT [FK_tUIThresholdUnitDetail_UITargetTypeID_tUITargetType]
ALTER TABLE [Projection].[tUIThresholdUnitDetail] DROP CONSTRAINT [FK_tUIThresholdUnitDetail_UnitID_tUnit]
ALTER TABLE [CQIIPM].[tCQIResultIPMFanPerformance] DROP CONSTRAINT [FK_tCQIResultIPMFanPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Fuel].[tSolidFuelQualityStatus] DROP CONSTRAINT [FK_tSolidFuelQualityStatus_FuelQualityID_tSolidFuelQuality]
ALTER TABLE [ProcessData].[tTrendAxis] DROP CONSTRAINT [FK_tPDTrendAxis_PDTrendID_tPDTrend]
ALTER TABLE [FuelTracker].[tNodeContentSourceDefault] DROP CONSTRAINT [FK_tFTNodeContentSourceDefault_FTNodeID_tFTNode]
ALTER TABLE [FuelTracker].[tNodeContentSourceDefault] DROP CONSTRAINT [FK_tFTNodeContentSourceDefault_FuelSourceID_tFuelSource]
ALTER TABLE [FuelTracker].[tNodePathExternalQuality] DROP CONSTRAINT [FK_tFTNodePathExternalQuality_FTNodePathID_tFTNodePath]
ALTER TABLE [FuelTracker].[tNodePathExternalQuality] DROP CONSTRAINT [FK_tFTNodePathExternalQuality_FuelQualityID_tFuelQuality]
ALTER TABLE [PowerRAM].[tRAMModelComponent] DROP CONSTRAINT [FK_tRAMModelComponent_RAMModelID_tRAMModel]
ALTER TABLE [Projection].[tCQIResultUnitPerformance] DROP CONSTRAINT [FK_tCQIResultUnitPerformance_CQIResultID_tCQIResult]
ALTER TABLE [FuelTracker].[tTransactionMovementMap] DROP CONSTRAINT [FK_tFTTransactionMovementMap_FTMovementID_tFTMovement]
ALTER TABLE [FuelTracker].[tTransactionMovementMap] DROP CONSTRAINT [FK_tFTTransactionMovementMap_FTTransactionID_tFTTransaction]
ALTER TABLE [Actuals].[tFuelQuality] DROP CONSTRAINT [FK_tActualFuelQuality_FuelQualityID_tFuelQuality]
ALTER TABLE [Actuals].[tFuelQuality] DROP CONSTRAINT [FK_tActualFuelQuality_FuelSourceID_tFuelSource]
ALTER TABLE [Actuals].[tFuelQuality] DROP CONSTRAINT [FK_tActualFuelQuality_UnitID_tUnit]
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] DROP CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_AssetClassTypeID_tAssetClassType]
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] DROP CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_IssueCauseTypeID_tIssueCauseType]
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] DROP CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_tAttributeType]
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] DROP CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_ValueTypeID_tValueType]
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] DROP CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_VariableTypeID_tVariableType]
ALTER TABLE [Actuals].[tDailyEmissionResult] DROP CONSTRAINT [FK_tActualDailyEmissionResult_UnitID_tUnit]
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_AssetClassTypeID_tAssetClassType]
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_IssueTypeID_tIssueType]
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tAttributeType]
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tValueType]
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tVariableType]
ALTER TABLE [Projection].[tCQIResultStatusDetail] DROP CONSTRAINT [FK_tCQIResultStatusDetail_CQIResultID_tCQIResult]
ALTER TABLE [Fuel].[tFuelBasinBlendDefinition] DROP CONSTRAINT [FK_tFuelBasinBlendDefinition_FuelBasinID_tFuelBasin]
ALTER TABLE [Fuel].[tFuelBasinBlendDefinition] DROP CONSTRAINT [FK_tFuelBasinBlendDefinition_ParentBasin1ID_tFuelBasin]
ALTER TABLE [Fuel].[tFuelBasinBlendDefinition] DROP CONSTRAINT [FK_tFuelBasinBlendDefinition_ParentBasin2ID_tFuelBasin]
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentSet] DROP CONSTRAINT [FK_tMaintenancePlanAdjustmentSet_tAnalysisSet]
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentSet] DROP CONSTRAINT [FK_tMaintenancePlanAdjustmentSet_tMaintenancePlanSet]
ALTER TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] DROP CONSTRAINT [FK_tVariableTypeIssueTypeMap_IssueTypeID_tIssueType]
ALTER TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] DROP CONSTRAINT [FK_tVariableTypeIssueTypeMap_ValueTypeID_tValueType]
ALTER TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] DROP CONSTRAINT [FK_tVariableTypeIssueTypeMap_VariableTypeID_tVariableTypeID]
ALTER TABLE [Asset].[tAssetAttribute] DROP CONSTRAINT [FK_tAssetAttribute_AssetID_tAsset]
ALTER TABLE [Asset].[tAssetAttribute] DROP CONSTRAINT [FK_tAssetAttribute_AttributeOptionTypeID_tAttributeOptionType]
ALTER TABLE [Asset].[tAssetAttribute] DROP CONSTRAINT [FK_tAssetAttribute_AttributeTypeID_tAttributeType]
ALTER TABLE [Asset].[tAttributeOptionType] DROP CONSTRAINT [FK_tAttributeOptionType_AttributeTypeID_tAttributeType]
ALTER TABLE [ProcessData].[tTrendSeries] DROP CONSTRAINT [FK_tPDTrendSeries_PDTagID_tPDTag]
ALTER TABLE [ProcessData].[tTrendSeries] DROP CONSTRAINT [FK_tPDTrendSeries_PDTrendID_tPDTrend]
ALTER TABLE [ProcessData].[tTrendSeries] DROP CONSTRAINT [FK_tPDTrendSeries_PDVariableID_tPDVariable]
ALTER TABLE [ProcessData].[tTrendSeries] DROP CONSTRAINT [FK_tTrendSeries_ValueTypeID_tValueType]
ALTER TABLE [Projection].[tDataVersion] DROP CONSTRAINT [FK_tDataVersion_PlanTypeID_tPlanType]
ALTER TABLE [FuelTracker].[tFuelQualityCollectorNodePathMap] DROP CONSTRAINT [FK_tFTFuelQualityCollectorNodePathMap_FTFuelQualityCollectorID_tFTFuelQualityCollector]
ALTER TABLE [FuelTracker].[tFuelQualityCollectorNodePathMap] DROP CONSTRAINT [FK_tFTFuelQualityCollectorNodePathMap_FTNodePathID_tFTNodePath]
ALTER TABLE [CQIExperience].[tCQIExperienceControlVariable] DROP CONSTRAINT [FK_tCQIExperienceControlVariable_ControlCQIExperienceID_tCQIExperience]
ALTER TABLE [CQIExperience].[tCQIExperienceControlVariable] DROP CONSTRAINT [FK_tCQIExperienceControlVariable_ManipulatedCQIExperienceID_tCQIExperience]
ALTER TABLE [FuelTracker].[tSiloModelTagMap] DROP CONSTRAINT [FK_tSiloModelTagMap_SiloModelID_tSiloModel]
ALTER TABLE [AnalytX].[tAssetIDExternalIDItemMap] DROP CONSTRAINT [FK_AnalytX_tAXAssetIDExternalIDAXCallMap_AssetID_Asset_tAsset]
ALTER TABLE [PowerRAM].[tMaintenanceOutageDetail] DROP CONSTRAINT [FK_tMaintenanceOutageDetail_tMaintenanceOutage]
ALTER TABLE [PowerRAM].[tMaintenanceOutageDetail] DROP CONSTRAINT [FK_tMaintenanceOutageDetail_tMaintenancePlanSet]
ALTER TABLE [PowerRAM].[tMaintenanceOutageDetail] DROP CONSTRAINT [FK_tMaintenanceOutageDetail_tRAMModel]
ALTER TABLE [Monitoring].[tModelConditionDefault] DROP CONSTRAINT [FK_Monitoring_tModelConditionDefault_ModelDefaultID_tModelDefault]
ALTER TABLE [Projection].[tGenerationDetail] DROP CONSTRAINT [FK_tGenerationDetail_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationDetail] DROP CONSTRAINT [FK_tGenerationDetail_UnitID_tUnit]
ALTER TABLE [Monitoring].[tLoadBucket] DROP CONSTRAINT [FK_Monitoring_tLoadBucket_UnitAssetID_tAsset]
ALTER TABLE [Fuel].[tFuelSourceGroupMap] DROP CONSTRAINT [FK_tFuelSourceGroupMap_FuelSourceGroupID_tFuelSourceGroup]
ALTER TABLE [Fuel].[tFuelSourceGroupMap] DROP CONSTRAINT [FK_tFuelSourceGroupMap_FuelSourceID_tFuelSource]
ALTER TABLE [Monitoring].[tEvent] DROP CONSTRAINT [FK_tEvent_tAsset]
ALTER TABLE [Monitoring].[tEvent] DROP CONSTRAINT [FK_tEvent_tEventType]
ALTER TABLE [Projection].[tGenerationAdjustmentSet] DROP CONSTRAINT [FK_tGenerationAdjustmentSet_DataVersionID_tDataVersion]
ALTER TABLE [Diagnostics].[tIssueCauseType] DROP CONSTRAINT [FK_tIssueCauseType_IssueTypeID_tIssueType]
ALTER TABLE [Projection].[tScenarioResultMetrics] DROP CONSTRAINT [FK_tScenarioResultMetrics_tAsset]
ALTER TABLE [Projection].[tScenarioResultMetrics] DROP CONSTRAINT [FK_tScenarioResultMetrics_tScenario]
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] DROP CONSTRAINT [FK_tMaintenancePlanAddAndOneBySeason_MaintenancePlanSetID_tMaintenancePlan]
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] DROP CONSTRAINT [FK_tMaintenancePlanAddAndOneBySeason_UnitID_tUnit]
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] DROP CONSTRAINT [FK_tRAMModelComponent_tMaintenancePlanAddAndOneBySeason_tRAMModel]
ALTER TABLE [PowerRAM].[tRAMModel] DROP CONSTRAINT [FK_tRAMModel_UnitID_tUnit]
ALTER TABLE [Fuel].[tAdditiveTypeChemicalMap] DROP CONSTRAINT [FK_Fuel_tAdditiveTypeChemicalMap_AdditiveChemicalTypeID_Fuel_tAdditiveChemicalType]
ALTER TABLE [Fuel].[tAdditiveTypeChemicalMap] DROP CONSTRAINT [FK_Fuel_tAdditiveTypeChemicalMap_AdditiveTypeID_Fuel_tAdditiveType]
ALTER TABLE [ProcessData].[tValueType] DROP CONSTRAINT [FK_tVariableType_ValueTypeID_tValueType]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationSystemFile] DROP CONSTRAINT [FK_tUnitRAMEvaluationSystemFile_UnitRAMEvaluationPeriodKeyID_tUnitRAMEvaluationPeriodKey]
ALTER TABLE [Asset].[tAssetClassTypeAttributeTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeAttributeTypeMap_tAssetClassType]
ALTER TABLE [Asset].[tAssetClassTypeAttributeTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeAttributeTypeMap_tAttributeType]
ALTER TABLE [Fuel].[tAdditive] DROP CONSTRAINT [FK_Fuel_tAdditive_AdditiveTypeID_Fuel_tAdditiveType]
ALTER TABLE [Fuel].[tAdditive] DROP CONSTRAINT [FK_Fuel_tAdditive_FuelQualityID_Fuel_tFuelQuality]
ALTER TABLE [Projection].[tVistaModelConfidence] DROP CONSTRAINT [FK_tVistaModelConfidence_ConfidenceTypeID_tConfidenceType]
ALTER TABLE [Projection].[tVistaModelConfidence] DROP CONSTRAINT [FK_tVistaModelConfidence_VistaModelID_tVistaModel]
ALTER TABLE [Projection].[tFuelChangeEvent] DROP CONSTRAINT [FK_tFuelChangeEvent_FuelPlanSetID_tFuelPlanSet]
ALTER TABLE [Projection].[tFuelChangeEvent] DROP CONSTRAINT [FK_tFuelChangeEvent_UnitID_tUnit]
ALTER TABLE [Actuals].[tCoalYardPerformance] DROP CONSTRAINT [FK_tActualCoalYardPerformance_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tSiloModelNodePathMap] DROP CONSTRAINT [FK_tSiloModelNodePathMap_NodePathID_tNodePath]
ALTER TABLE [FuelTracker].[tSiloModelNodePathMap] DROP CONSTRAINT [FK_tSiloModelNodePathMap_SiloModelID_tSiloModel]
ALTER TABLE [FuelTracker].[tSiloModel] DROP CONSTRAINT [FK_tSiloModel_UnitID_tUnit]
ALTER TABLE [AnalytX].[tAssetAppContextDetail] DROP CONSTRAINT [FK_AnalytX_tAssetAppContextDetail_AssetAppContextSetID_AnalytX_tAssetAppContextSet]
ALTER TABLE [AnalytX].[tAssetAppContextDetail] DROP CONSTRAINT [FK_AnalytX_tAssetAppContextDetail_AssetID_Asset_tAsset]
ALTER TABLE [Projection].[tGenerationAdjustmentDetail] DROP CONSTRAINT [FK_tGenerationAdjustmentDetail_GenerationAdjustmentSetID_tGenerationAdjustmentSet]
ALTER TABLE [Projection].[tGenerationAdjustmentDetail] DROP CONSTRAINT [FK_tGenerationAdjustmentDetail_UnitID_tUnit]
ALTER TABLE [AccessControl].[tAssetAccessRule] DROP CONSTRAINT [FK_AccessControl_tAssetAccessRule_AssetID_Asset_tAsset]
ALTER TABLE [FuelTracker].[tNodeContent] DROP CONSTRAINT [FK_tFTNodeContent_FTNodeID_tFTNode]
ALTER TABLE [FuelTracker].[tNodeContent] DROP CONSTRAINT [FK_tFTNodeContent_FTTransactionID_tFTTransaction]
ALTER TABLE [Actuals].[tFanPerformance] DROP CONSTRAINT [FK_tActualFanPerformance_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tInTransit] DROP CONSTRAINT [FK_tFTInTransit_FTNodePathID_tFTNodePath]
ALTER TABLE [Actuals].[tDefaultFuelQuality] DROP CONSTRAINT [FK_tDefaultFuelQuality_FuelQualityID_tFuelQuality]
ALTER TABLE [Actuals].[tDefaultFuelQuality] DROP CONSTRAINT [FK_tDefaultFuelQuality_FuelSourceID_tFuelSource]
ALTER TABLE [Actuals].[tDefaultFuelQuality] DROP CONSTRAINT [FK_tDefaultFuelQuality_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tMovement] DROP CONSTRAINT [FK_tFTMovement_FTInTransitID]
ALTER TABLE [FuelTracker].[tMovement] DROP CONSTRAINT [FK_tFTMovement_FTNodePathID_tFTNodePath]
ALTER TABLE [FuelTracker].[tNodePath] DROP CONSTRAINT [FK_tFTNodePath_FromFTNodeID_tFTNode]
ALTER TABLE [FuelTracker].[tNodePath] DROP CONSTRAINT [FK_tFTNodePath_ToFTNodeID_tFTNode]
ALTER TABLE [Projection].[tScenarioUnitPerformance] DROP CONSTRAINT [FK_tScenarioUnitPerformance_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tScenarioUnitPerformance] DROP CONSTRAINT [FK_tScenarioUnitPerformance_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tScenarioUnitPerformance] DROP CONSTRAINT [FK_tScenarioUnitPerformance_ScenarioID_tScenario]
ALTER TABLE [Projection].[tScenarioUnitPerformance] DROP CONSTRAINT [FK_tScenarioUnitPerformance_UnitConfigID_tUnitConfig]
ALTER TABLE [Projection].[tScenarioUnitPerformance] DROP CONSTRAINT [FK_tScenarioUnitPerformance_UnitID_tUnit]
ALTER TABLE [Asset].[tAssetTreeNode] DROP CONSTRAINT [FK_tAssetTreeNode_tAsset]
ALTER TABLE [Asset].[tAssetTreeNode] DROP CONSTRAINT [FK_tAssetTreeNode_tAsset1]
ALTER TABLE [Asset].[tAssetTreeNode] DROP CONSTRAINT [FK_tAssetTreeNode_tAssetTree]
ALTER TABLE [Asset].[tAssetTreeNode] DROP CONSTRAINT [FK_tAssetTreeNode_tAssetTreeNodeChildConfigurationType]
ALTER TABLE [Actuals].[tAirHeaterPerformance] DROP CONSTRAINT [FK_tActualAirHeaterPerformance_UnitID_tUnit]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] DROP CONSTRAINT [FK_tFuelPlanAdjustmentSetUnitMap_tFuelPlanAdjustment]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] DROP CONSTRAINT [FK_tFuelPlanAdjustmentUnitMap_tUnit]
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustment] DROP CONSTRAINT [FK_tFuelPlanAdjustment_tFuelPlanAdjustmentSe]
ALTER TABLE [Asset].[tAssetClassTypeAssetClassTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeAssetClassTypeMap_tAssetClassType]
ALTER TABLE [Asset].[tAssetClassTypeAssetClassTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeAssetClassTypeMap_tAssetClassType1]
ALTER TABLE [PowerRAM].[tMaintenanceOutage] DROP CONSTRAINT [FK_tMaintenanceOutage_MaintenancePlanSetID_tMaintenancePlanSet]
ALTER TABLE [PowerRAM].[tMaintenanceOutage] DROP CONSTRAINT [FK_tMaintenanceOutage_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tTransactionQuality] DROP CONSTRAINT [FK_tFTTransactionQuality_FTTransactionID_tFTTransaction]
ALTER TABLE [FuelTracker].[tTransactionQuality] DROP CONSTRAINT [FK_tFTTransactionQuality_FTTransactionSourceID_tFTTransactionSource]
ALTER TABLE [FuelTracker].[tTransactionQuality] DROP CONSTRAINT [FK_tFTTransactionQuality_FuelQualityID_tFuelQuality]
ALTER TABLE [FuelTracker].[tTransaction] DROP CONSTRAINT [FK_tFTTransaction_FTNodeID_tFTNode]
ALTER TABLE [FuelTracker].[tTransaction] DROP CONSTRAINT [FK_tFTTransaction_FTTransactionSummaryID_tFTTransactionSummary]
ALTER TABLE [CurrentPerformance].[tSectionAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tSectionAssetVariableTypeTagMap_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap]
ALTER TABLE [CurrentPerformance].[tSectionAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tSectionAssetVariableTypeTagMap_SectionAssetID_AssetID_tAsset]
ALTER TABLE [Administration].[tUserEditDetail] DROP CONSTRAINT [FK_tUserEditDetail_UserEditTableID_tUserEditTable]
ALTER TABLE [Monitoring].[tEventTypeAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tEventTypeAssetVariableTypeTagMap_tAssetVariableTypeTagMap]
ALTER TABLE [Monitoring].[tEventTypeAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tEventTypeAssetVariableTypeTagMap_tEventType]
ALTER TABLE [Diagnostics].[tAssetIssue] DROP CONSTRAINT [FK_Asset_tAsset_IssueTypeID_Diagnostircs_tAssetIssue]
ALTER TABLE [Diagnostics].[tAssetIssue] DROP CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_AssetIssueCategoryTypeID_Diagnostircs_tAssetIssue]
ALTER TABLE [Diagnostics].[tAssetIssue] DROP CONSTRAINT [FK_Diagnostics_tIssueCauseType_IssueCauseTypeID_Diagnostircs_tAssetIssue]
ALTER TABLE [Diagnostics].[tAssetIssue] DROP CONSTRAINT [FK_Diagnostics_tIssueType_IssueTypeID_Diagnostics_tAssetIssue]
ALTER TABLE [Asset].[tAssetClassType] DROP CONSTRAINT [FK_tAssetClassType_tAssetType]
ALTER TABLE [Projection].[tGenerationAdjustmentMap] DROP CONSTRAINT [FK_tGenerationAdjustmentMap_GenerationAdjustmentSetID_tGenerationAdjustmentSet]
ALTER TABLE [Projection].[tGenerationAdjustmentMap] DROP CONSTRAINT [FK_tGenerationAdjustmentMap_GenerationSetID_tGenerationSet]
ALTER TABLE [Actuals].[tAssetBlendedFuelQualityStatus] DROP CONSTRAINT [FK_tAssetBlendedFuelQualityStatus_AssetID_tAsset]
ALTER TABLE [Monitoring].[tModelConditionNote] DROP CONSTRAINT [FK_ModelConditionNote_ModelConditionID_ModelConditionID]
ALTER TABLE [Monitoring].[tModelConditionNote] DROP CONSTRAINT [FK_Monitoring_tModelConditionNode_ModelConditionID_tModelCondition]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentSet] DROP CONSTRAINT [FK_tUnitPlanAdjustmentSet_tAnalysisSet]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentSet] DROP CONSTRAINT [FK_tUnitPlanAdjustmentSet_tUnitConfigScheduleSet]
ALTER TABLE [CurrentPerformance].[tCPNavTree] DROP CONSTRAINT [FK_tPDNavTree_PDTrendID_tPDTrend]
ALTER TABLE [Asset].[tUnitSeasonMap] DROP CONSTRAINT [FK_tUnitSeasonMap_UnitID_tUnit]
ALTER TABLE [Projection].[tFuelPlanDetail] DROP CONSTRAINT [FK_tFuelPlanDetail_FuelPlanSetID_tFuelPlanSet]
ALTER TABLE [Projection].[tFuelPlanDetail] DROP CONSTRAINT [FK_tFuelPlanDetail_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tFuelPlanDetail] DROP CONSTRAINT [FK_tFuelPlanDetail_UnitID_tUnit]
ALTER TABLE [FuelTracker].[tTransactionSource] DROP CONSTRAINT [FK_tFTTransactionSource_FTTransactionID_tFTTransaction]
ALTER TABLE [FuelTracker].[tTransactionSource] DROP CONSTRAINT [FK_tFTTransactionSource_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tGenerationProductionCostDetail] DROP CONSTRAINT [FK_tGenerationProductionCostDetail_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationProductionCostDetail] DROP CONSTRAINT [FK_tGenerationProductionCostDetail_UnitID_tUnit]
ALTER TABLE [Monitoring].[tModelTrendMap] DROP CONSTRAINT [FK_Monitoring_tModelTrendMap_ModelID_tModel]
ALTER TABLE [Monitoring].[tModelTrendMap] DROP CONSTRAINT [FK_Monitoring_tModelTrendMap_PDTrendID_tPDTrend]
ALTER TABLE [Actuals].[tFinalMonthlyEmission] DROP CONSTRAINT [FK_tFinalMonthlyEmission_UnitID_tUnit]
ALTER TABLE [Projection].[tCQIResultConfidence] DROP CONSTRAINT [FK_tCQIResultConfidence_ConfidenceTypeID_tConfidenceType]
ALTER TABLE [Projection].[tCQIResultConfidence] DROP CONSTRAINT [FK_tCQIResultConfidence_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tScenarioFuelQuality] DROP CONSTRAINT [FK_tScenarioFuelQuality_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tScenarioFuelQuality] DROP CONSTRAINT [FK_tScenarioFuelQuality_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tScenarioFuelQuality] DROP CONSTRAINT [FK_tScenarioFuelQuality_ScenarioID_tScenario]
ALTER TABLE [Projection].[tScenarioFuelQuality] DROP CONSTRAINT [FK_tScenarioFuelQuality_UnitConfigID_tUnitConfig]
ALTER TABLE [Projection].[tScenarioFuelQuality] DROP CONSTRAINT [FK_tScenarioFuelQuality_UnitID_tUnit]
ALTER TABLE [CQIExperience].[tCQIResultExperience] DROP CONSTRAINT [FK_tCQIResultExperience_CQIExperienceID_tCQIExperience]
ALTER TABLE [CQIExperience].[tCQIResultExperience] DROP CONSTRAINT [FK_tCQIResultExperience_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tGenerationDailyBucket] DROP CONSTRAINT [FK_tGenerationDailyBucket_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationDailyBucket] DROP CONSTRAINT [FK_tGenerationDailyBucket_UnitID_tUnit]
ALTER TABLE [CQIIPM].[tCQIResultIPMBoilerPerformance] DROP CONSTRAINT [FK_tCQIResultIPMBoilerPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tFuelPlanSet] DROP CONSTRAINT [FK_tFuelPlanSet_DataVersionID_tDataVersion]
ALTER TABLE [Projection].[tFuelPlanSet] DROP CONSTRAINT [FK_tFuelPlanSet_SensitivityImpactTypeID_tSensitivityImpactType]
ALTER TABLE [Actuals].[tOfficialUnitPerformance] DROP CONSTRAINT [FK_tActualOfficialUnitPerformance_UnitID_tUnit]
ALTER TABLE [Monitoring].[tModelCondition] DROP CONSTRAINT [FK_Monitoring_tModelCondition_ModelID_tModel]
ALTER TABLE [AdaptivePlanning].[tAnalysisSetScenarioMap] DROP CONSTRAINT [FK_tAnalysisSetScenarioMap_tAnalysisSet]
ALTER TABLE [AdaptivePlanning].[tAnalysisSetScenarioMap] DROP CONSTRAINT [FK_tAnalysisSetScenarioMap_tScenario]
ALTER TABLE [AdaptivePlanning].[tAnalysisSet] DROP CONSTRAINT [FK_tAnalysisSet_tScenario]
ALTER TABLE [Asset].[tStationGroup] DROP CONSTRAINT [FK_tStationGroup_tAsset]
ALTER TABLE [Actuals].[tMillPerformance] DROP CONSTRAINT [FK_tActualMillPerformance_UnitID_tUnit]
ALTER TABLE [FuelPro].[tEvaluationFuelSourceMap] DROP CONSTRAINT [FK_tEvalOfferMap_EvaluationID_tEvaluation]
ALTER TABLE [FuelPro].[tEvaluationFuelSourceMap] DROP CONSTRAINT [FK_tEvalOfferMap_FuelSourceID_tFuelSource]
ALTER TABLE [FuelPro].[tEvaluationFuelSourceMap] DROP CONSTRAINT [FK_tEvalOfferMap_StationID_tStation]
ALTER TABLE [CQIExperience].[tCQIExperienceTimestampData] DROP CONSTRAINT [FK_tCQIExperienceTimestampData_CQIExperienceID_tCQIExperience]
ALTER TABLE [CQIExperience].[tCQIExperienceTimestampData] DROP CONSTRAINT [FK_tCQIExperienceTimestampData_CQIExperienceTimestampID_tCQIExperienceTimestamp]
ALTER TABLE [CQIExperience].[tCQIExperience] DROP CONSTRAINT [FK_tCQIExperience_CQIExperienceTypeID_tCQIExperienceType]
ALTER TABLE [CQIExperience].[tCQIExperience] DROP CONSTRAINT [FK_tCQIExperience_PDTagID_tPDTag]
ALTER TABLE [CQIExperience].[tCQIExperience] DROP CONSTRAINT [FK_tCQIExperience_UnitID_tUnit]
ALTER TABLE [EnergyStorage].[tGenerationDataTimeslice] DROP CONSTRAINT [FKey_tGenerationDataSets_SetID]
ALTER TABLE [Monitoring].[tModelDefault] DROP CONSTRAINT [FK_Monitoring_tModelDefault_AssetClassTypeVariableTypeMapID_tAssetClassTypeVariableTypeMapID]
ALTER TABLE [Monitoring].[tModelDefault] DROP CONSTRAINT [FK_Monitoring_tModelDefault_ModelTypeID_tModelType]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] DROP CONSTRAINT [FK_tUnitPlanAdjustmentDetail_ttUnitEquipmentStateType]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] DROP CONSTRAINT [FK_tUnitPlanAdjustmentDetail_tUnit]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] DROP CONSTRAINT [FK_tUnitPlanAdjustmentDetail_tUnitPlanAdjustment]
ALTER TABLE [CQIExperience].[tCQIExperienceTimestamp] DROP CONSTRAINT [FK_tCQIExperienceTimestamp_FuelBasinID_tFuelBasin]
ALTER TABLE [CQIExperience].[tCQIExperienceTimestamp] DROP CONSTRAINT [FK_tCQIExperienceTimestamp_UnitID_tUnit]
ALTER TABLE [Fuel].[tFuelBlendDefinition] DROP CONSTRAINT [FK_tFuelBlendDefinition_FuelSourceID_tFuelSource]
ALTER TABLE [Fuel].[tFuelBlendDefinition] DROP CONSTRAINT [FK_tFuelBlendDefinition_ParentSourceID_tFuelSource]
ALTER TABLE [Projection].[tGenerationPattern] DROP CONSTRAINT [FK_tGenerationPattern_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationPattern] DROP CONSTRAINT [FK_tGenerationPattern_UnitID_tUnit]
ALTER TABLE [Projection].[tCQIResultFanPerformance] DROP CONSTRAINT [FK_tCQIResultFanPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tCQIResultFanPerformance] DROP CONSTRAINT [FK_tCQIResultFanPerformance_FanTypeID_tFanType]
ALTER TABLE [ProcessData].[tServer] DROP CONSTRAINT [FK_tPDServer_PDServerTypeID_tPDServerType]
ALTER TABLE [Projection].[tAssetBlendedFuelQuality] DROP CONSTRAINT [FK_ProjectiontAssetBlendedFuelQuality_AssetID_AssettAsset]
ALTER TABLE [Projection].[tAssetBlendedFuelQuality] DROP CONSTRAINT [FK_ProjectiontAssetBlendedFuelQuality_FuelBasinID_FueltFuelBasin]
ALTER TABLE [Projection].[tAssetBlendedFuelQuality] DROP CONSTRAINT [FK_ProjectiontAssetBlendedFuelQuality_UnitID_AssettUnit]
ALTER TABLE [Monitoring].[tUnit] DROP CONSTRAINT [FK_Monitoring_tUnit_PDServerID_tServer]
ALTER TABLE [Monitoring].[tUnit] DROP CONSTRAINT [FK_Monitoring_tUnit_tAssetVariableTypeTagMapID_AssetVariableTypeTagMap]
ALTER TABLE [Monitoring].[tUnit] DROP CONSTRAINT [FK_Monitoring_tUnit_UnitAssetID_tAsset]
ALTER TABLE [Actuals].[tBoilerPerformance] DROP CONSTRAINT [FK_tActualBoilerPerformance_UnitID_tUnit]
ALTER TABLE [Fuel].[tSolidFuelQualityThermoConstant] DROP CONSTRAINT [FK_tFuelQualityThermoConstant_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tScenarioResultStatusDetail] DROP CONSTRAINT [FK_tScenarioResultStatusDetail_ScenarioResultStatusID_tScenarioResultStatus]
ALTER TABLE [Projection].[tScenarioResultStatus] DROP CONSTRAINT [FK_tScenarioResultStatus_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tScenarioResultStatus] DROP CONSTRAINT [FK_tScenarioResultStatus_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tScenarioResultStatus] DROP CONSTRAINT [FK_tScenarioResultStatus_ScenarioID_tScenario]
ALTER TABLE [Projection].[tScenarioResultStatus] DROP CONSTRAINT [FK_tScenarioResultStatus_UnitConfigID_tUnitConfig]
ALTER TABLE [Projection].[tScenarioResultStatus] DROP CONSTRAINT [FK_tScenarioResultStatus_UnitID_tUnit]
ALTER TABLE [EnergyStorage].[tPerformanceSet] DROP CONSTRAINT [FK_tPerformanceSet_tBattery]
ALTER TABLE [EnergyStorage].[tPerformanceSet] DROP CONSTRAINT [FK_tPerformanceSet_tGenerationDataSet]
ALTER TABLE [Projection].[tCQIRun] DROP CONSTRAINT [FK_tCQIRun_CQIRunTypeID_tCQIRunType]
ALTER TABLE [Projection].[tCQIRun] DROP CONSTRAINT [FK_tCQIRun_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tCQIRun] DROP CONSTRAINT [FK_tCQIRun_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tCQIRun] DROP CONSTRAINT [FK_tCQIRun_UnitConfigID_tUnitConfig]
ALTER TABLE [CQIIPM].[tCQIResultIPMUnitPerformance] DROP CONSTRAINT [FK_tCQIResultIPMUnitPerformance_CQIResultID_tCQIResult]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_DataVersionID_tDataVersion]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_ForecastSetID_tForecastSet]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_FuelBurnSetID_tFuelBurnSet]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_SensitivityImpactTypeID_tSensitivityImpactType]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_SensitivityTypeID_tSensitivityType]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenario_UnitConfigScheduleSetID_tUnitConfigScheduleSet]
ALTER TABLE [Projection].[tScenario] DROP CONSTRAINT [FK_tScenarioMaintenancePlanSetID_tMaintenancePlanSet]
ALTER TABLE [PowerRAM].[tRAMEvaluation] DROP CONSTRAINT [FK_tRAMEvaluation_MaintenancePlanSetID_tMaintenancePlanSet]
ALTER TABLE [Asset].[tAssetAttributeSchedule] DROP CONSTRAINT [FK_tAssetAttributeSchedule_AssetID_tAsset]
ALTER TABLE [Asset].[tAssetAttributeSchedule] DROP CONSTRAINT [FK_tAssetAttributeSchedule_AttributeTypeID_tAttributeType]
ALTER TABLE [Monitoring].[tModelInputDefault] DROP CONSTRAINT [FK_Monitoring_tModelInputDefault_AssetClassTypeVariableTypeMapID_tAssetClassTypeVariableTypeMap]
ALTER TABLE [Monitoring].[tModelInputDefault] DROP CONSTRAINT [FK_Monitoring_tModelInputDefault_ModelDefaultID_tModelDefault]
ALTER TABLE [Economic].[tForecastDetail] DROP CONSTRAINT [FK_tForecastDetail_ForecastSetID_tForecastSet]
ALTER TABLE [Economic].[tFuelSourceStationForecastMap] DROP CONSTRAINT [FK_tSrcStaFcstMap_FuelSourceID_tFuelSource]
ALTER TABLE [Economic].[tFuelSourceStationForecastMap] DROP CONSTRAINT [FK_tSrcStaFcstMap_StationID_tStation]
ALTER TABLE [ProcessData].[tTag] DROP CONSTRAINT [FK_tPDTag_PDServerID_tPDServer]
ALTER TABLE [Base].[tEventQueue] DROP CONSTRAINT [FK_Base_tEventQueue_EventStatusTypeID_Base_tEventStatusType]
ALTER TABLE [Base].[tEventQueue] DROP CONSTRAINT [FK_Base_tEventQueue_EventTypeID_Base_tEventType]
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] DROP CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_MaintenancePlanSetID_tMaintenancePlan]
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] DROP CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_RAMModelID_tRAMModel]
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] DROP CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_UnitConfigID_tUnitConfig]
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] DROP CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_UnitID_tUnit]
ALTER TABLE [PowerRAM].[tMaintenancePlanSet] DROP CONSTRAINT [FK_tMaintenancePlanSet_DataVersionID_tDataVersion]
ALTER TABLE [PowerRAM].[tMaintenancePlanSet] DROP CONSTRAINT [FK_tMaintenancePlanSet_UnitConfigScheduleSetID_tUnitConfigScheduleSet]
ALTER TABLE [Projection].[tUnitAttributes] DROP CONSTRAINT [FK_Projection_tUnitAttributes_FuelSourceID_Fuel_FuelSource_FuelSourceID]
ALTER TABLE [Projection].[tUnitAttributes] DROP CONSTRAINT [FK_Projection_tUnitAttributes_UnitID_Asset_tUnit_UnitID]
ALTER TABLE [Base].[tEventQueueNotification] DROP CONSTRAINT [FK_Base_tEventQueueNotification_EventStatusTypeID_Base_tEventStatusType]
ALTER TABLE [Base].[tEventQueueNotification] DROP CONSTRAINT [FK_Base_tEventQueueNotification_EventTypeID_Base_tEventType]
ALTER TABLE [Projection].[tCQIResult] DROP CONSTRAINT [FK_tCQIResult_CQIRunTypeID_tCQIRunType]
ALTER TABLE [Projection].[tCQIResult] DROP CONSTRAINT [FK_tCQIResult_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tCQIResult] DROP CONSTRAINT [FK_tCQIResult_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tCQIResult] DROP CONSTRAINT [FK_tCQIResult_UnitConfigID_tUnitConfig]
ALTER TABLE [Fuel].[tFuelSource] DROP CONSTRAINT [FK_tFuelSource_FuelBasinID_tFuelBasin]
ALTER TABLE [Fuel].[tFuelSource] DROP CONSTRAINT [FK_tFuelSource_FuelSupplierID_tFuelSupplier]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentUnitMap] DROP CONSTRAINT [FK_tUnitPlanAdjustmentSetUnitMap_tUnitPlanAdjustment]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentUnitMap] DROP CONSTRAINT [FK_tUnitPlanAdjustmentUnitMap_tUnit]
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustment] DROP CONSTRAINT [FK_tUnitPlanAdjustment_tUnitPlanAdjustmentSe]
ALTER TABLE [Projection].[tUITargetClientDetail] DROP CONSTRAINT [FK_tUITargetClientDetail_UIItemID_tUIItem]
ALTER TABLE [Projection].[tUITargetClientDetail] DROP CONSTRAINT [FK_tUITargetClientDetail_UITargetSetID_tUITargetSet]
ALTER TABLE [Projection].[tUITargetClientDetail] DROP CONSTRAINT [FK_tUITargetClientDetail_UITargetTypeID_tUITargetType]
ALTER TABLE [Projection].[tUITargetSet] DROP CONSTRAINT [FK_tUITargetSet_DataVersionID_tDataVersion]
ALTER TABLE [Actuals].[tUnitPerformance] DROP CONSTRAINT [FK_tActualUnitPerformance_UnitID_tUnit]
ALTER TABLE [Actuals].[tBlendedFuelQuality] DROP CONSTRAINT [FK_tActualBlendedFuelQuality_UnitID_tUnit]
ALTER TABLE [Monitoring].[tFilterRule] DROP CONSTRAINT [FK_Monitoring_tFilterRule_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap]
ALTER TABLE [Monitoring].[tFilterRule] DROP CONSTRAINT [FK_Monitoring_tFilterRule_FilterTypeID_tFilterType]
ALTER TABLE [Monitoring].[tFilterRule] DROP CONSTRAINT [FK_Monitoring_tFilterRule_UnitAssetID_tAsset]
ALTER TABLE [Monitoring].[tModelInput] DROP CONSTRAINT [FK_Monitoring_tModelInput_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap]
ALTER TABLE [Monitoring].[tModelInput] DROP CONSTRAINT [FK_Monitoring_tModelInput_ModelID_tModel]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluation] DROP CONSTRAINT [FK_tUnitRAMEvaluation_RAMEvaluationID_tRAMEvaluation]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluation] DROP CONSTRAINT [FK_tUnitRAMEvaluation_UnitID_tUnit]
ALTER TABLE [Projection].[tCQILoadBucket] DROP CONSTRAINT [FK_tCQILoadBucket_UnitID_tUnit]
ALTER TABLE [Asset].[tUnit] DROP CONSTRAINT [FK_tUnit_StationID_tStation]
ALTER TABLE [Asset].[tUnit] DROP CONSTRAINT [FK_tUnit_tAsset]
ALTER TABLE [Projection].[tGenerationRegulationSchedule] DROP CONSTRAINT [FK_tRegulationSchedule_GenerationSetID_tGenerationSet]
ALTER TABLE [Projection].[tGenerationRegulationSchedule] DROP CONSTRAINT [FK_tRegulationSchedule_UnitID_tUnit]
ALTER TABLE [Projection].[tGenerationSet] DROP CONSTRAINT [FK_tGenerationSet_DataVersionID_tDataVersion]
ALTER TABLE [Projection].[tGenerationSet] DROP CONSTRAINT [FK_tGenerationSet_SensitivityImpactTypeID_tSensitivityImpactType]
ALTER TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_AssetClassTypeID_tAssetClassType]
ALTER TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_VariableTypeID_tVariableType]
ALTER TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] DROP CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_WeightingVariableTypeID_tVariableType]
ALTER TABLE [Actuals].[tAssetBlendedFuelQuality] DROP CONSTRAINT [FK_Actuals_AssetBlendedFuelQuality_AssetID_Asset_tAsset]
ALTER TABLE [CurrentPerformance].[tCPNavTreeVariableMap] DROP CONSTRAINT [FK_tCPNavTreeVariableMap_ValueTypeID_tValueType]
ALTER TABLE [CurrentPerformance].[tCPNavTreeVariableMap] DROP CONSTRAINT [FK_tPDNavTreeVariableMap_PDNavTreeID_tPDNavTree]
ALTER TABLE [CurrentPerformance].[tCPNavTreeVariableMap] DROP CONSTRAINT [FK_tPDNavTreeVariableMap_PDVariableID_tPDVariable]
ALTER TABLE [ProcessData].[tVariableType] DROP CONSTRAINT [FK_tVariableType_FunctionTypeID_tFunctionType]
ALTER TABLE [ProcessData].[tVariableType] DROP CONSTRAINT [FK_tVariableType_WeightingVariableTypeID_tVariableType]
ALTER TABLE [ProcessData].[tVariableType] DROP CONSTRAINT [UK_tVariableType_DefaultValueTypeID_ValueTypeID]
ALTER TABLE [Projection].[tScenarioMargin] DROP CONSTRAINT [FK_tScenarioMargin_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tScenarioMargin] DROP CONSTRAINT [FK_tScenarioMargin_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tScenarioMargin] DROP CONSTRAINT [FK_tScenarioMargin_ScenarioID_tScenario]
ALTER TABLE [Projection].[tScenarioMargin] DROP CONSTRAINT [FK_tScenarioMargin_UnitConfigID_tUnitConfig]
ALTER TABLE [Projection].[tScenarioMargin] DROP CONSTRAINT [FK_tScenarioMargin_UnitID_tUnit]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputResultsCapacityDistribution] DROP CONSTRAINT [FK_tUnitRAMEvaluationOutputResultsCapacityDistribution_PowerRAM_tUnitRAMEvaluationPeriodKey]
ALTER TABLE [Monitoring].[tModel] DROP CONSTRAINT [FK_Monitoring_tModel_AssetVariableTypeTagMapID_tAssetVariableTypeTagMapID]
ALTER TABLE [Monitoring].[tModel] DROP CONSTRAINT [FK_Monitoring_tModel_ModelTypeID_tModelType]
ALTER TABLE [Monitoring].[tModel] DROP CONSTRAINT [FK_Monitoring_tModel_UnitAsset_tAsset]
ALTER TABLE [Projection].[tUnitConfig] DROP CONSTRAINT [FK_tUnitConfig_RAMModelID_tRAMModel]
ALTER TABLE [Projection].[tUnitConfig] DROP CONSTRAINT [FK_tUnitConfig_SensitivityImpactTypeID_tSensitivityImpactType]
ALTER TABLE [Projection].[tUnitConfig] DROP CONSTRAINT [FK_tUnitConfig_UnitEquipmentStateSetID_tUnitEquipmentStateSet]
ALTER TABLE [Projection].[tUnitConfig] DROP CONSTRAINT [FK_tUnitConfig_UnitID_tUnit]
ALTER TABLE [Projection].[tUnitConfig] DROP CONSTRAINT [FK_tUnitConfig_VistModelID_tVistaModel]
ALTER TABLE [FuelPro].[tEvaluation] DROP CONSTRAINT [FK_tEvaluation_ForecastSetID_tForecastSet]
ALTER TABLE [FuelPro].[tEvaluation] DROP CONSTRAINT [FK_tEvaluation_FuelPlanSetID_tFuelPlanSet]
ALTER TABLE [FuelPro].[tEvaluation] DROP CONSTRAINT [FK_tEvaluation_GenerationSetID_tGenerationSet]
ALTER TABLE [FuelPro].[tEvaluation] DROP CONSTRAINT [FK_tEvaluation_UnitConfigScheduleSetID_tUnitConfigScheduleSet]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputSystemResults] DROP CONSTRAINT [FK_tUnitRAMEvaluationOutputSystemResults_PowerRAM_tUnitRAMEvaluationPeriodKey]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationPeriodKey] DROP CONSTRAINT [FK_tUnitRAMEvaluationPeriodKey_UnitID_tAsset]
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationPeriodKey] DROP CONSTRAINT [FK_tUnitRAMEvaluationPeriodKey_UnitRAMEvaluationID_ttUnitRAMEvaluation]
ALTER TABLE [Fuel].[tAdditiveChemical] DROP CONSTRAINT [FK_Fuel_tAdditiveChemical_AdditiveChemicalTypeID_Fuel_tAdditiveChemicalType]
ALTER TABLE [Fuel].[tAdditiveChemical] DROP CONSTRAINT [FK_Fuel_tAdditiveChemical_FuelQualityID_Fuel_tAdditive]
ALTER TABLE [Asset].[tExternalSystemKeyAssetMap] DROP CONSTRAINT [FK_Asset_tExternalSystemKeyAssetMap_ExternalSystemID_Asset_tExternalSystem]
ALTER TABLE [Asset].[tExternalSystemKeyAssetMap] DROP CONSTRAINT [FK_Asset_tExternalSystemKeyAssetMap_ExternalSystemTypeID_Asset_tAsset]
ALTER TABLE [Asset].[tAsset] DROP CONSTRAINT [FK_Asset_tAsset_ParentAssetID_Asset_tAsset_AssetID]
ALTER TABLE [Asset].[tAsset] DROP CONSTRAINT [FK_Asset_tAsset_tAssetTreeNodeChildConfigurationTypeID_tAssetTreeNodeChildConfigurationType]
ALTER TABLE [Asset].[tAsset] DROP CONSTRAINT [FK_tAsset_tAssetClassType]
ALTER TABLE [CurrentPerformance].[tCLEventLogItem] DROP CONSTRAINT [UK_CLEventLogItem_AssetVariableTypeTagMapID_AssetVariableTypeTagMapID]
ALTER TABLE [CurrentPerformance].[tCLEventLogItem] DROP CONSTRAINT [UK_CLEventLogItem_CLEventLogItemStatusID_CLEventLogItemStatusID]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_StatusValueTypeID_tValueType]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_StatusVariableTypeID_tVariableType]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_tAsset]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_tTag]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_tVariableType]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_ValueTypeID_tValueType]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_WeightingValueTypeID_tValueType]
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] DROP CONSTRAINT [FK_tAssetVariableTypeTagMap_WeightingVariableTypeID_tVariableType]
ALTER TABLE [FuelTracker].[tInTransitQuality] DROP CONSTRAINT [FK_tFTInTransitQuality_FTInTransitID_tFTInTransit]
ALTER TABLE [FuelTracker].[tInTransitQuality] DROP CONSTRAINT [FK_tFTInTransitQuality_FuelQualityID_tFuelQuality]
ALTER TABLE [Fuel].[tFuelQuality] DROP CONSTRAINT [FK_Fuel_tFuelQuality_FuelQualityTypeID_Fuel_tFuelQualityType]
ALTER TABLE [Fuel].[tFuelQuality] DROP CONSTRAINT [FK_Fuel_tFuelQuality_FuelSourceID_Fuel_tFuelSource]
ALTER TABLE [Projection].[tUnitEquipmentStateDetail] DROP CONSTRAINT [FK_tUnitEquipmentStateDetail_UnitEquipmentStateSetID_tUnitEquipmentStateSet]
ALTER TABLE [Projection].[tUnitEquipmentStateDetail] DROP CONSTRAINT [FK_tUnitEquipmentStateDetail_UnitEquipmentStateTypeID_tUnitEquipmentStateType]
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentUnitMap] DROP CONSTRAINT [FK_tMaintenancePlanAdjustmentSetUnitMap_tMaintenancePlanAdjustment]
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentUnitMap] DROP CONSTRAINT [FK_tMaintenancePlanAdjustmentUnitMap_tUnit]
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustment] DROP CONSTRAINT [FK_tMaintenancePlanAdjustment_tMaintenancePlanAdjustmentSe]
ALTER TABLE [FuelTracker].[tNode] DROP CONSTRAINT [FK_FuelTracker_tNode_AssetID_Asset_tAsset]
ALTER TABLE [FuelTracker].[tNode] DROP CONSTRAINT [FK_FuelTracker_tNode_StationGroupID_Asset_tStationGroup]
ALTER TABLE [FuelTracker].[tNode] DROP CONSTRAINT [FK_FuelTracker_tNode_StationID_Asset_tStation]
ALTER TABLE [FuelTracker].[tNode] DROP CONSTRAINT [FK_FuelTracker_tNode_UnitID_Asset_tUnit]
ALTER TABLE [FuelTracker].[tNode] DROP CONSTRAINT [FK_tFTNode_FTNodeTypeID_tFTNodeType]
ALTER TABLE [FuelTracker].[tNode] DROP CONSTRAINT [FK_tFTNode_FTNodeWithdrawalTypeID_tFTNodeWithdrawalType]
ALTER TABLE [Asset].[tStation] DROP CONSTRAINT [FK_tStation_StationGroupID_tStationGroup]
ALTER TABLE [Asset].[tStation] DROP CONSTRAINT [FK_tStation_tAsset]
ALTER TABLE [Projection].[tUnitConfigScheduleDetail] DROP CONSTRAINT [FK_tUnitConfigScheduleDetail_UnitConfigID_tUnitConfig]
ALTER TABLE [Projection].[tUnitConfigScheduleDetail] DROP CONSTRAINT [FK_tUnitConfigScheduleDetail_UnitConfigScheduleSetID_tUnitConfigScheduleSet]
ALTER TABLE [Projection].[tUnitConfigScheduleSet] DROP CONSTRAINT [FK_tUnitConfigScheduleSet_DataVersionID_tDataVersion]
ALTER TABLE [Projection].[tUnitConfigScheduleSet] DROP CONSTRAINT [FK_tUnitConfigScheduleSet_SensitivityImpactTypeID_tSensitivityImpactType]
ALTER TABLE [Fuel].[tSolidFuelQuality] DROP CONSTRAINT [FK_Fuel_tSolidFuelQuality_FuelQualityID_Fuel_tFuelQuality]
ALTER TABLE [Base].[tEventType] DROP CONSTRAINT [FK_Base_tEventType_EventEndPointTypeID_Base_tEventEndPointType]
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] DROP CONSTRAINT [FK_tScenarioFuelBurnSchedule_FuelQualityID_tFuelQuality]
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] DROP CONSTRAINT [FK_tScenarioFuelBurnSchedule_FuelSourceID_tFuelSource]
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] DROP CONSTRAINT [FK_tScenarioFuelBurnSchedule_ScenarioID_tScenario]
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] DROP CONSTRAINT [FK_tScenarioFuelBurnSchedule_UnitConfigID_tUnitConfig]
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] DROP CONSTRAINT [FK_tScenarioFuelBurnSchedule_UnitID_tUnit]
DELETE FROM [Projection].[tUnitConfigScheduleSet] WHERE [UnitConfigScheduleSetID] in (1,2,3,4)
SET IDENTITY_INSERT [Projection].[tUnitConfigScheduleSet] ON
INSERT INTO [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID], [UnitConfigScheduleSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [Comment], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (1, N'Analysis Test 1', 1, 2012, 1, 0, N'', N'Test', N'Test', '20121119 08:19:02.493', NULL, '20121119 08:19:02.493', '20121119 08:19:02.493', '20121119 08:19:02.493', 1, 1, 0)
INSERT INTO [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID], [UnitConfigScheduleSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [Comment], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (2, N'Analysis Test 2', 1, 2012, 1, 0, N'', N'Test', N'Test', '20121119 08:19:02.530', NULL, '20121119 08:19:02.530', '20121119 08:19:02.530', '20121119 08:19:02.530', 1, 1, 0)
INSERT INTO [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID], [UnitConfigScheduleSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [Comment], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (3, N'Analysis Test 3', 1, 2012, 1, 0, N'', N'Test', N'Test', '20121119 08:19:02.540', NULL, '20121119 08:19:02.540', '20121119 08:19:02.540', '20121119 08:19:02.540', 1, 1, 0)
INSERT INTO [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID], [UnitConfigScheduleSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [Comment], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (4, N'Analysis Test 4', 1, 2012, 1, 0, N'', N'Test', N'Test', '20121119 08:19:02.547', NULL, '20121119 08:19:02.547', '20121119 08:19:02.547', '20121119 08:19:02.547', 1, 1, 0)
SET IDENTITY_INSERT [Projection].[tUnitConfigScheduleSet] OFF
DELETE FROM [Asset].[tStation] WHERE [StationID] in (1,4,6,9,10,13,14,15,16,17,18,19)
SET IDENTITY_INSERT [Asset].[tStation] ON
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (1, 1, N'Martin Lake', N'ML', NULL, 20, N'BV', N'BV', '20110411 14:06:10.917', '20110411 14:06:10.917', 4)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (4, 1, N'Big Brown', N'BB', NULL, 10, N'BV', N'BV', '20110422 00:00:00.000', '20110422 00:00:00.000', 6)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (6, 1, N'Monticello', N'MON', NULL, 30, N'BV', N'BV', '20110422 00:00:00.000', '20110422 00:00:00.000', 7)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (9, 1, N'Oak Grove', N'OG', NULL, 40, N'BV', N'BV', '20110422 00:00:00.000', '20110422 00:00:00.000', 8)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (10, 1, N'Sandow', N'SAN', NULL, 50, N'BV', N'BV', '20110422 00:00:00.000', '20110422 00:00:00.000', 9)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (13, 4, N'Decordova', N'DEC', NULL, 60, N'BV', N'BV', '20120315 17:38:54.710', '20120315 17:38:54.710', 30)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (14, 4, N'Graham', N'GRM', NULL, 36, N'BV', N'BV', '20120319 11:17:52.327', '20120319 11:17:52.327', 36)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (15, 4, N'Lake Hubbard', N'LHB', NULL, 39, N'BV', N'BV', '20120319 11:18:13.183', '20120319 11:18:13.183', 39)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (16, 4, N'Morgan Creek', N'MOR', NULL, 42, N'BV', N'BV', '20120319 11:18:13.183', '20120319 11:18:13.183', 42)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (17, 4, N'Permian Basin', N'PRB', NULL, 51, N'BV', N'BV', '20120319 11:18:13.183', '20120319 11:18:13.183', 51)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (18, 4, N'Stryker Creek', N'STR', NULL, 57, N'BV', N'BV', '20120319 11:18:13.183', '20120319 11:18:13.183', 57)
INSERT INTO [Asset].[tStation] ([StationID], [StationGroupID], [StationDesc], [StationAbbrev], [DeliveryMethod], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [AssetID]) VALUES (19, 4, N'Trinidad', N'TND', NULL, 60, N'BV', N'BV', '20120319 11:18:13.183', '20120319 11:18:13.183', 60)
SET IDENTITY_INSERT [Asset].[tStation] OFF
ALTER TABLE [Asset].[tAsset] DISABLE TRIGGER AssetInsertUpdate
GO
DELETE FROM [Asset].[tAsset] WHERE [AssetID] in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,23,24,25,26,30,31,32,34,35,36,37,38,39,40,41,42,43,44,46,47,48,49,51,52,53,54,55,56,57,58,59,60,61,62)
SET IDENTITY_INSERT [Asset].[tAsset] ON
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (1, 4, N'MLK1', N'Martin Lake Unit 1', 4001, NULL, 301, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'e9a31744-e09e-4820-97f3-7fcc1c616ad2')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (2, 4, N'MLK2', N'Martin Lake Unit 2', 4001, NULL, 302, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'd87525d8-35dc-409d-b5b6-61ee28d6381f')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (3, 4, N'MLK3', N'Martin Lake Unit 3', 4001, NULL, 303, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'72f4066f-865f-48c8-bee6-a473432ee31d')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (4, 25, N'MLK', N'Martin Lake', 3000, NULL, 20, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'946e1a32-fd8f-455e-86ad-03309bdfb6c7')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (5, NULL, N'Luminant', N'Luminant Fleet', 1000, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'4e25eed0-4c43-4cce-b745-263763ea4d7e')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (6, 25, N'BB', N'Big Brown', 3000, NULL, 10, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'e078faad-6830-4117-8e29-6cac0db15096')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (7, 25, N'MON', N'Monticello', 3000, NULL, 30, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'94d2a18a-1132-4dc7-bd67-c0151611a20a')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (8, 25, N'OG', N'Oak Grove', 3000, NULL, 40, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'28e843f1-2357-4817-b250-12319a2cb358')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (9, 25, N'SAN', N'Sandow', 3000, NULL, 50, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'3fbdcdd0-b4ef-4d91-b386-cf70f6600519')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (10, 6, N'BB1', N'Big Brown Unit 1', 4001, NULL, 101, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'c0f98135-c492-4bfe-b0be-2e45ad667505')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (11, 6, N'BB2', N'Big Brown Unit 2', 4001, NULL, 102, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'1366aaf1-aabc-45af-8043-c6b5cb0e043f')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (12, 7, N'MON1', N'Monticello Unit 1', 4001, NULL, 201, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'33030e9d-ec0c-4269-864f-6fd22b189ca0')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (13, 7, N'MON2', N'Monticello Unit 2', 4001, NULL, 202, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'1394df9f-bccc-40a1-8cdb-3a4ad60b52cf')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (14, 7, N'MON3', N'Monticello Unit 3', 4001, NULL, 203, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'7898ee80-0059-417c-b58f-610a3d98e724')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (15, 8, N'OG1', N'Oak Grove Unit 1', 4001, NULL, 401, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'07f824c0-0b05-4dbc-9925-3776cbca8095')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (16, 8, N'OG2', N'Oak Grove Unit 2', 4001, NULL, 402, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'73f223d6-1899-4f3a-9793-4013b965c2ef')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (17, 9, N'SAN4', N'Sandow 4', 4001, NULL, 501, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'dee189d4-d304-46de-bb3d-3fafe9decbed')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (23, 9, N'SAN5A', N'Sandow 5A', 4001, NULL, 502, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'38738bcf-1701-46fc-9e2c-6efb64fa9ccf')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (24, 9, N'SAN5B', N'Sandow 5B', 4001, NULL, 503, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'fd1c722b-fd67-45a0-b1d7-bf12393177a9')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (25, 5, N'Coal Fleet', N'Coal Fleet', 2000, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'7498013e-92c3-4abb-8dda-8eae25d70ce1')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (26, 5, N'Gas Fleet', N'Gas Fleet', 2000, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'11a22307-5592-4a2c-9bdb-a00f187cf0b3')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (30, 26, N'DEC', N'Decordova', 3000, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'5218b1d5-e8cc-47ab-ba4c-0cc59e53d39c')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (31, 30, N'DECCT1', N'Decordova CT1', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'd77d7ae9-0524-430a-b4f3-67d29a67d947')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (32, 30, N'DECCT2', N'Decordova CT2', 4003, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'56616c57-f840-4fb6-9ce0-a3d381f0b367')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (34, 30, N'DECCT3', N'Decordova CT3', 4003, NULL, 3, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'1718ea65-4620-4f01-9af6-ed51a6302742')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (35, 30, N'DECCT4', N'Decordova CT4', 4003, NULL, 4, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'33af1cb3-1120-41ec-a248-6200c40ea17e')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (36, 26, N'GRM', N'Graham', 3000, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'231e7054-1344-4b36-8fa1-2786d9516509')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (37, 36, N'GRM1', N'Graham 1', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'ddd7643c-ee19-4ef8-a025-5e6f3d8e922d')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (38, 36, N'GRM2', N'Graham 2', 4003, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'61f10282-67b5-41a8-a995-79462e2ce17c')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (39, 26, N'LHB', N'Lake Hubbard', 3000, NULL, 3, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'b4ef7f16-5a6c-418e-9ccd-f9c3f617d3de')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (40, 39, N'LHB1', N'Lake Hubbard 1', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'9566f1f6-cce6-415e-826c-260eb3a0ffeb')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (41, 39, N'LHB2', N'Lake Hubbard 2', 4003, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'552d748c-5034-446e-b267-d30e63871e12')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (42, 26, N'MOR', N'Morgan Creek', 3000, NULL, 4, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'6c2fcd96-b6a3-4d88-8c97-786c2544c9d7')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (43, 42, N'MRCT1', N'Morgan Creen CT1', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'c9dd4223-4ff1-4f93-ac04-60b499bc735e')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (44, 42, N'MRCT2', N'Morgan Creen CT2', 4003, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'6060caa0-64fe-4fa2-8d49-db0f02973aa9')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (46, 42, N'MRCT3', N'Morgan Creen CT3', 4003, NULL, 3, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'8ced0db2-53e8-4fae-b0e7-63f03f40f426')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (47, 42, N'MRCT4', N'Morgan Creen CT4', 4003, NULL, 4, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'8603807e-5b42-49cb-9cd4-cbbb07acb6bc')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (48, 42, N'MRCT5', N'Morgan Creen CT5', 4003, NULL, 5, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'6d040d6c-2f7d-441d-ae90-4c473cb41001')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (49, 42, N'MRCT6', N'Morgan Creen CT6', 4003, NULL, 6, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'66c6050b-787e-464e-ad45-bbafb85f571c')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (51, 26, N'PRB', N'Permian Basin', 3000, NULL, 5, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'd7d876de-7651-4b08-a10e-2d10b6f6058c')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (52, 51, N'PRBCT1', N'Permian Basin CT1', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'159e9ce7-8ee8-41b9-b7d9-fccac356761c')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (53, 51, N'PRBCT2', N'Permian Basin CT2', 4003, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'e0d155a0-2b5f-410f-a1c0-5b26b5bcfc57')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (54, 51, N'PRBCT3', N'Permian Basin CT3', 4003, NULL, 3, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'6d89e871-d6b0-44eb-905c-c2f763360978')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (55, 51, N'PRBCT4', N'Permian Basin CT4', 4003, NULL, 4, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'cf5af98f-c3ef-44e7-afd3-50cc19a9c573')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (56, 51, N'PRBCT4', N'Permian Basin CT5', 4003, NULL, 5, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'3ff2420a-865f-408e-9cad-cc7355d55340')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (57, 26, N'STR', N'Stryker Creek', 3000, NULL, 6, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'5fac022f-6057-49c1-9ced-fa0c012e2cd1')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (58, 57, N'STR1', N'Stryker Creek 1', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'e0cf353d-81ec-4379-830d-3a7e15ef1083')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (59, 57, N'STR2', N'Stryker Creek 2', 4003, NULL, 2, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'9f59e042-a65f-48da-8be9-847ce017be49')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (60, 26, N'TND', N'Trinidad', 3000, NULL, 7, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'26528be9-1a50-40f0-bd5b-08e6410e07b4')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (61, 60, N'TND6', N'Trinidad 6', 4003, NULL, 1, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'a97bc15d-a791-4cf4-a1b4-e2770c449fdb')
INSERT INTO [Asset].[tAsset] ([AssetID], [ParentAssetID], [AssetAbbrev], [AssetDesc], [AssetClassTypeID], [AssetTreeNodeChildConfigurationTypeID], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [GlobalID]) VALUES (62, 10, N'BLW', N'Economizer', 6131, NULL, 5, N'BV', N'BV', '20121119 08:18:49.527', '20121119 08:18:49.527', N'f6cd4e80-76bd-4b63-b236-fc270bb8fa84')
SET IDENTITY_INSERT [Asset].[tAsset] OFF
ALTER TABLE [Asset].[tAsset] ENABLE TRIGGER AssetInsertUpdate
GO
DELETE FROM [Projection].[tGenerationSet] WHERE [GenerationSetID] in (1,2,3,4)
SET IDENTITY_INSERT [Projection].[tGenerationSet] ON
INSERT INTO [Projection].[tGenerationSet] ([GenerationSetID], [GenerationSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [Comment], [IsPublic], [IsValid], [IsArchived]) VALUES (1, N'Analysis Test 1', 1, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.493', NULL, '20121119 08:19:02.493', '20121119 08:19:02.493', NULL, NULL, 1, 1, 0)
INSERT INTO [Projection].[tGenerationSet] ([GenerationSetID], [GenerationSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [Comment], [IsPublic], [IsValid], [IsArchived]) VALUES (2, N'Analysis Test 2', 1, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.530', NULL, '20121119 08:19:02.530', '20121119 08:19:02.530', NULL, NULL, 1, 1, 0)
INSERT INTO [Projection].[tGenerationSet] ([GenerationSetID], [GenerationSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [Comment], [IsPublic], [IsValid], [IsArchived]) VALUES (3, N'Analysis Test 3', 1, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.537', NULL, '20121119 08:19:02.537', '20121119 08:19:02.537', NULL, NULL, 1, 1, 0)
INSERT INTO [Projection].[tGenerationSet] ([GenerationSetID], [GenerationSetDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [Comment], [IsPublic], [IsValid], [IsArchived]) VALUES (4, N'Analysis Test 4', 1, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.543', NULL, '20121119 08:19:02.543', '20121119 08:19:02.543', NULL, NULL, 1, 1, 0)
SET IDENTITY_INSERT [Projection].[tGenerationSet] OFF
DELETE FROM [Asset].[tUnit] where [UnitID] in (2,3,4,10,11,22,23,24,30,31,39,40,41,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67)
SET IDENTITY_INSERT [Asset].[tUnit] ON
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (2, 1, N'Martin Lake Unit 1', N'MLK1', 100, 815, N'BV', N'NA\and10992', '20110412 14:39:09.333', '20110412 14:39:09.333', NULL, N'ML - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'ML - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'ML - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 1, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (3, 1, N'Martin Lake Unit 2', N'MLK2', 101, 820, N'BV', N'NA\and10992', '20110412 14:42:32.480', '20110412 14:42:32.480', NULL, N'ML - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'ML - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'ML - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 2, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (4, 1, N'Martin Lake Unit 3', N'MLK3', 102, 820, N'BV', N'NA\and10992', '20110412 14:42:32.483', '20110412 14:42:32.483', NULL, N'ML - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'ML - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'ML - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 3, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (10, 4, N'Big Brown Unit 1', N'BB1', 11, 606, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'BB - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'BB - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'BB - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 10, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (11, 4, N'Big Brown Unit 2', N'BB2', 12, 602, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'BB - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'BB - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'BB - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 11, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (22, 6, N'Monticello Unit 1', N'MON1', 21, 580, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'MON - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'MON - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'MON - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 12, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (23, 6, N'Monticello Unit 2', N'MON2', 22, 580, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'MON - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'MON - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'MON - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 13, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (24, 6, N'Monticello Unit 3', N'MON3', 23, 790, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'MON - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'ML - FGD Sludge Disposal', NULL, N'MON - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'MON - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 14, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'ML - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (30, 9, N'Oak Grove Unit 1', N'OG1', 401, 840, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'OAK - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'OAK - FGD Sludge Disposal', NULL, N'OAK - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'OAK - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 15, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'OAK - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (31, 9, N'Oak Grove Unit 2', N'OG2', 402, 825, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'OAK - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'OAK - FGD Sludge Disposal', NULL, N'OAK - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'OAK - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 16, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'OAK - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (39, 10, N'Sandow Unit 4', N'SAN4', 501, 600, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'SAN - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'SAN - FGD Sludge Disposal', NULL, N'SAN - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'SAN - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 17, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'SAN - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (40, 10, N'Sandow Unit 5A', N'SAN5A', 502, 285, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'SAN - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'SAN - FGD Sludge Disposal', NULL, N'SAN - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'SAN - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 23, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'SAN - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (41, 10, N'Sandow Unit 5B', N'SAN5B', 503, 285, N'BV', N'NA\and10992', '20110422 00:00:00.000', '20110422 00:00:00.000', NULL, N'SAN - Generation', NULL, N'Urea', NULL, N'FGD Additive', NULL, N'PAC', NULL, N'SAN - FGD Sludge Disposal', NULL, N'SAN - Fly Ash Disposal', NULL, N'Fly Ash Re-Use', NULL, N'SAN - Bottom Ash Disposal', NULL, N'Bottom Ash Re-Use', NULL, N'Limestone', NULL, N'Lime', NULL, N'Gypsum Re-Use', 24, N'Cox Creek', N'FGD Fixative', N'FGD Ash', N'SAN - Limestone', N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (46, 13, N'Decordova CT1', N'DECCT1', 30, 94, N'BV', N'BV', '20120319 11:14:49.087', '20120319 11:14:49.087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (47, 13, N'Decordova CT2', N'DECCT2', 30, 95, N'BV', N'BV', '20120319 11:15:05.357', '20120319 11:15:05.357', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (48, 13, N'Decordova CT3', N'DECCT3', 30, 95, N'BV', N'BV', '20120319 11:15:05.357', '20120319 11:15:05.357', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (49, 13, N'Decordova CT4', N'DECCT4', 30, 95, N'BV', N'BV', '20120319 11:15:05.373', '20120319 11:15:05.373', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (50, 14, N'Graham 1', N'GRM1', 36, 225, N'BV', N'BV', '20120319 11:19:18.237', '20120319 11:19:18.237', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (51, 14, N'Graham 2', N'GRM2', 36, 390, N'BV', N'BV', '20120319 11:19:18.237', '20120319 11:19:18.237', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (52, 15, N'Lake Hubbard 1', N'LHB1', 39, 392, N'BV', N'BV', '20120319 11:19:32.010', '20120319 11:19:32.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (53, 15, N'Lake Hubbard 2', N'LHB2', 39, 524, N'BV', N'BV', '20120319 11:19:32.010', '20120319 11:19:32.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (54, 16, N'Morgan Creen CT1', N'MRCT1', 42, 89, N'BV', N'BV', '20120319 11:19:39.060', '20120319 11:19:39.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 43, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (55, 16, N'Morgan Creen CT2', N'MRCT2', 42, 88, N'BV', N'BV', '20120319 11:19:39.060', '20120319 11:19:39.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (56, 16, N'Morgan Creen CT3', N'MRCT3', 42, 92, N'BV', N'BV', '20120319 11:19:39.060', '20120319 11:19:39.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (57, 16, N'Morgan Creen CT4', N'MRCT4', 42, 88, N'BV', N'BV', '20120319 11:19:39.060', '20120319 11:19:39.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 47, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (58, 16, N'Morgan Creen CT5', N'MRCT5', 42, 89, N'BV', N'BV', '20120319 11:19:39.060', '20120319 11:19:39.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (59, 16, N'Morgan Creen CT6', N'MRCT6', 42, 90, N'BV', N'BV', '20120319 11:19:39.077', '20120319 11:19:39.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 49, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (60, 17, N'Permian Basin CT1', N'PRBCT1', 51, 85, N'BV', N'BV', '20120319 11:19:46.800', '20120319 11:19:46.800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (61, 17, N'Permian Basin CT2', N'PRBCT2', 51, 85, N'BV', N'BV', '20120319 11:19:46.800', '20120319 11:19:46.800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (62, 17, N'Permian Basin CT3', N'PRBCT3', 51, 85, N'BV', N'BV', '20120319 11:19:46.800', '20120319 11:19:46.800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (63, 17, N'Permian Basin CT4', N'PRBCT4', 51, 85, N'BV', N'BV', '20120319 11:19:46.800', '20120319 11:19:46.800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (64, 17, N'Permian Basin CT5', N'PRBCT4', 51, 85, N'BV', N'BV', '20120319 11:19:46.813', '20120319 11:19:46.813', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (65, 18, N'Stryker Creek 1', N'STR1', 57, 167, N'BV', N'BV', '20120319 11:19:54.410', '20120319 11:19:54.410', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (66, 18, N'Stryker Creek 2', N'STR2', 57, 502, N'BV', N'BV', '20120319 11:19:54.427', '20120319 11:19:54.427', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
INSERT INTO [Asset].[tUnit] ([UnitID], [StationID], [UnitDesc], [UnitAbbrev], [DisplayOrder], [RatedNetCapacity_mw], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [EnergyFcstSubCat], [EnergyFcstVar], [SCR_SNCRFcstSubCat], [SCR_SNCRFcstVar], [FGDFcstSubCat], [FGDFcstVar], [MercuryFcstSubCat], [MercuryFcstVar], [ScrubberFcstSubCat], [ScrubberFcstVar], [FlyAshDisposalFcstSubCat], [FlyAshDisposalFcstVar], [FlyAshSalesFcstSubCat], [FlyAshSalesFcstVar], [BottomAshDisposalFcstSubCat], [BottomAshDisposalFcstVar], [BottomAshSalesFcstSubCat], [BottomAshSalesFcstVar], [CoalAddFcstSubCat], [CoalAddFcstVar], [SO3AddFcstSubCat], [SO3AddFcstVar], [GypsumSalesFcstSubCat], [GypsumSalesFcstVar], [AssetID], [FGDWaterFcstVar], [FGDFixativeFcstVar], [FGDAshFcstVar], [FGDReagentFcstVar], [FGDDBAAddFcstVar], [CFBBedReagentFcstVar], [NH3AddFcstVar], [OMFixedFcstVar], [OMVariableFcstVar]) VALUES (67, 19, N'Trinidad 6', N'TND6', 60, 226, N'BV', N'BV', '20120319 11:20:00.747', '20120319 11:20:00.747', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, NULL, NULL, NULL, NULL, N'DBA', N'CFB Bed', N'Ammonia', N'O&M Fixed', N'O&M Variable')
SET IDENTITY_INSERT [Asset].[tUnit] OFF

DELETE FROM [Economic].[tForecastSet] WHERE [ForecastSetID] in (1,2,3,4)
SET IDENTITY_INSERT [Economic].[tForecastSet] ON
INSERT INTO [Economic].[tForecastSet] (
			[ForecastSetID]
		   ,[ForecastSetDesc]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[ImportDate]
           ,[IsConsensus]
           ,[IsPublic]
           ,[EffectiveYear]
           ,[IsArchived])
		   VALUES (1,'Forecast Set 1','JCB','JCB','2013-01-09 15:08:00.130','2013-01-09 15:08:00.130',NULL,1,1,2013,0)
		   INSERT INTO [Economic].[tForecastSet] (
			[ForecastSetID]
		   ,[ForecastSetDesc]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[ImportDate]
           ,[IsConsensus]
           ,[IsPublic]
           ,[EffectiveYear]
           ,[IsArchived])
		   VALUES (2,'Forecast Set 2','JCB','JCB','2013-01-09 15:08:00.130','2013-01-09 15:08:00.130',NULL,0,1,2013,0)
		   INSERT INTO [Economic].[tForecastSet] (
			[ForecastSetID]
		   ,[ForecastSetDesc]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[ImportDate]
           ,[IsConsensus]
           ,[IsPublic]
           ,[EffectiveYear]
           ,[IsArchived])
		   VALUES (3,'Forecast Set 3','JCB','JCB','2013-01-09 15:08:00.130','2013-01-09 15:08:00.130',NULL,0,1,2013,0)
		   INSERT INTO [Economic].[tForecastSet] (
			[ForecastSetID]
		   ,[ForecastSetDesc]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[ImportDate]
           ,[IsConsensus]
           ,[IsPublic]
           ,[EffectiveYear]
           ,[IsArchived])
		   VALUES (4,'Forecast Set 4','JCB','JCB','2013-01-09 15:08:00.130','2013-01-09 15:08:00.130',NULL,0,1,2013,0)
SET IDENTITY_INSERT [Economic].[tForecastSet] OFF

DELETE FROM [PowerRAM].[tMaintenancePlanSet] WHERE [MaintenancePlanSetID] in (1,2,3,4)
SET IDENTITY_INSERT [PowerRAM].[tMaintenancePlanSet] ON
INSERT INTO [PowerRAM].[tMaintenancePlanSet]
           ([MaintenancePlanSetID],[MaintenancePlanSetDesc]
           ,[Comment],[StartDate],[DataVersionID],[Duration]
           ,[DurationType],[PeriodType],[UnitConfigScheduleSetID],[IsConsensus]
           ,[IsSensitivity],[IsPublic],[IsArchived],[HasOverwrites]
           ,[CreateDate],[CreatedBy],[ChangeDate],[ChangedBy])
     VALUES
           (1,'Maint Plan 1','AP Prog','2012-01-01 00:00:00.000','1','1','Y','M',1,1,0,1,0,0,GETDATE(),'JCB',GETDATE(),'JCB')
INSERT INTO [PowerRAM].[tMaintenancePlanSet]
           ([MaintenancePlanSetID],[MaintenancePlanSetDesc]
           ,[Comment],[StartDate],[DataVersionID],[Duration]
           ,[DurationType],[PeriodType],[UnitConfigScheduleSetID],[IsConsensus]
           ,[IsSensitivity],[IsPublic],[IsArchived],[HasOverwrites]
           ,[CreateDate],[CreatedBy],[ChangeDate],[ChangedBy])
     VALUES
           (2,'Maint Plan 2','AP Prog','2012-01-01 00:00:00.000','1','1','Y','M',2,0,0,1,0,0,GETDATE(),'JCB',GETDATE(),'JCB')
INSERT INTO [PowerRAM].[tMaintenancePlanSet]
           ([MaintenancePlanSetID],[MaintenancePlanSetDesc]
           ,[Comment],[StartDate],[DataVersionID],[Duration]
           ,[DurationType],[PeriodType],[UnitConfigScheduleSetID],[IsConsensus]
           ,[IsSensitivity],[IsPublic],[IsArchived],[HasOverwrites]
           ,[CreateDate],[CreatedBy],[ChangeDate],[ChangedBy])
     VALUES
           (3,'Maint Plan 3','AP Prog','2012-01-01 00:00:00.000','1','1','Y','M',3,0,0,1,0,0,GETDATE(),'JCB',GETDATE(),'JCB')
INSERT INTO [PowerRAM].[tMaintenancePlanSet]
           ([MaintenancePlanSetID],[MaintenancePlanSetDesc]
           ,[Comment],[StartDate],[DataVersionID],[Duration]
           ,[DurationType],[PeriodType],[UnitConfigScheduleSetID],[IsConsensus]
           ,[IsSensitivity],[IsPublic],[IsArchived],[HasOverwrites]
           ,[CreateDate],[CreatedBy],[ChangeDate],[ChangedBy])
     VALUES
           (4,'Maint Plan 4','AP Prog','2012-01-01 00:00:00.000','1','1','Y','M',4,0,0,1,0,0,GETDATE(),'JCB',GETDATE(),'JCB')
SET IDENTITY_INSERT [PowerRAM].[tMaintenancePlanSet] OFF

IF NOT EXISTS (SELECT * FROM [Economic].[tForecastSetRoleMap] WHERE [ForecastSetID] = 1 AND [SecurityRoleID] = 1)
	INSERT INTO [Economic].[tForecastSetRoleMap] ([ForecastSetID], [SecurityRoleID]) VALUES (1, 1)
IF NOT EXISTS (SELECT * FROM [Economic].[tForecastSetRoleMap] WHERE [ForecastSetID] = 2 AND [SecurityRoleID] = 1)
	INSERT INTO [Economic].[tForecastSetRoleMap] ([ForecastSetID], [SecurityRoleID]) VALUES (2, 1)
IF NOT EXISTS (SELECT * FROM [Economic].[tForecastSetRoleMap] WHERE [ForecastSetID] = 3 AND [SecurityRoleID] = 1)
	INSERT INTO [Economic].[tForecastSetRoleMap] ([ForecastSetID], [SecurityRoleID]) VALUES (3, 1)
IF NOT EXISTS (SELECT * FROM [Economic].[tForecastSetRoleMap] WHERE [ForecastSetID] = 4 AND [SecurityRoleID] = 1)
	INSERT INTO [Economic].[tForecastSetRoleMap] ([ForecastSetID], [SecurityRoleID]) VALUES (4, 1)


DELETE FROM [Fuel].[tFuelBasin] WHERE [FuelBasinID] in (1)
SET IDENTITY_INSERT [Fuel].[tFuelBasin] ON
INSERT INTO [Fuel].[tFuelBasin] ([FuelBasinID], [FuelBasinDesc], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [BasinColorARGB]) VALUES (1, N'Analysis Test 1', N'Test', N'Test', '20121119 08:19:02.517', '20121119 08:19:02.517', -16777216)
SET IDENTITY_INSERT [Fuel].[tFuelBasin] OFF
DELETE FROM [Fuel].[tFuelSource] WHERE [FuelSourceID] in (1,2,3,4)
SET IDENTITY_INSERT [Fuel].[tFuelSource] ON
INSERT INTO [Fuel].[tFuelSource] ([FuelSourceID], [FuelBasinID], [FuelSupplierID], [FuelSourceDesc], [CreatedBy], [ChangedBy], [CreateDate], [IsAdditive], [ChangeDate], [FuelSourceAbbrev], [IsGas], [ERPQuoteUniqueKey], [WasAutoCreated]) VALUES (1, 1, NULL, N'Analysis Test 1', N'Test', N'Test', '20121119 08:19:02.517', 0, '20121119 08:19:02.517', N'Analysis Test 1', 0, NULL, 0)
INSERT INTO [Fuel].[tFuelSource] ([FuelSourceID], [FuelBasinID], [FuelSupplierID], [FuelSourceDesc], [CreatedBy], [ChangedBy], [CreateDate], [IsAdditive], [ChangeDate], [FuelSourceAbbrev], [IsGas], [ERPQuoteUniqueKey], [WasAutoCreated]) VALUES (2, 1, NULL, N'Analysis Test 2', N'Test', N'Test', '20121119 08:19:02.537', 0, '20121119 08:19:02.537', N'Analysis Test 2', 0, NULL, 0)
INSERT INTO [Fuel].[tFuelSource] ([FuelSourceID], [FuelBasinID], [FuelSupplierID], [FuelSourceDesc], [CreatedBy], [ChangedBy], [CreateDate], [IsAdditive], [ChangeDate], [FuelSourceAbbrev], [IsGas], [ERPQuoteUniqueKey], [WasAutoCreated]) VALUES (3, 1, NULL, N'Analysis Test 3', N'Test', N'Test', '20121119 08:19:02.543', 0, '20121119 08:19:02.543', N'Analysis Test 3', 0, NULL, 0)
INSERT INTO [Fuel].[tFuelSource] ([FuelSourceID], [FuelBasinID], [FuelSupplierID], [FuelSourceDesc], [CreatedBy], [ChangedBy], [CreateDate], [IsAdditive], [ChangeDate], [FuelSourceAbbrev], [IsGas], [ERPQuoteUniqueKey], [WasAutoCreated]) VALUES (4, 1, NULL, N'Analysis Test 4', N'Test', N'Test', '20121119 08:19:02.547', 0, '20121119 08:19:02.547', N'Analysis Test 4', 0, NULL, 0)
SET IDENTITY_INSERT [Fuel].[tFuelSource] OFF
SET IDENTITY_INSERT [Projection].[tScenario] ON

declare @scenarioID as int
set @scenarioID = 1
while @scenarioID<=100
  begin
  DELETE FROM [Projection].[tScenario] WHERE [ScenarioID] = @scenarioID
  INSERT INTO [Projection].[tScenario] ([ScenarioID], [ScenarioDesc], [DataVersionID], [PlanYear], [IsConsensus], [IsSensitivity], [SensitivityTypeID], [GenerationSetID], [FuelPlanSetID], [UnitConfigScheduleSetID], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [RunDate], [RunStatus], [IsModified], [ChangeDate], [IsPublic], [ForecastSetID], [IsArchived], [MaintenancePlanSetID], [ScenarioComment], [UseDeratedPower]) VALUES (@scenarioID, N'Analysis Test ' + convert(varchar(10),@scenarioID), 1, 2012, 1, 0, NULL, 1, 1, 1, N'Test', N'Test', '20121119 08:19:02.497', NULL, NULL, NULL, 0, '20121119 08:19:02.497', 1, 1, 0, 1, NULL, 0)
  set @scenarioID = @scenarioID + 1
  end

SET IDENTITY_INSERT [Projection].[tScenario] OFF
DELETE FROM [Asset].[tStationGroup] WHERE [StationGroupID] in (1,4)
SET IDENTITY_INSERT [Asset].[tStationGroup] ON
INSERT INTO [Asset].[tStationGroup] ([StationGroupID], [StationGroupDesc], [StationGroupAbbrev], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [SO2AllowanceFcstSubCat], [SO2AllowanceFcstVar], [NOxAllowanceFcstSubCat], [NOxAllowanceFcstVar], [CO2AllowanceFcstSubCat], [CO2AllowanceFcstVar], [MercuryAllowanceFcstSubCat], [MercuryAllowanceFcstVar], [AssetID]) VALUES (1, N'Coal Fleet', N'Coal Fleet', 1, N'BV', N'NA\and10992', '20110411 13:50:44.233', '20110411 13:50:44.233', NULL, N'SO2 Allowance', NULL, N'NOx Allowance', NULL, N'CO2 Allowance', NULL, N'Mercury Allowance', 25)
INSERT INTO [Asset].[tStationGroup] ([StationGroupID], [StationGroupDesc], [StationGroupAbbrev], [DisplayOrder], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [SO2AllowanceFcstSubCat], [SO2AllowanceFcstVar], [NOxAllowanceFcstSubCat], [NOxAllowanceFcstVar], [CO2AllowanceFcstSubCat], [CO2AllowanceFcstVar], [MercuryAllowanceFcstSubCat], [MercuryAllowanceFcstVar], [AssetID]) VALUES (4, N'Gas Fleet', N'Gas Fleet', 2, N'BV', N'BV', '20120315 17:37:48.207', '20120315 17:37:48.207', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26)
SET IDENTITY_INSERT [Asset].[tStationGroup] OFF
DELETE FROM [AdaptivePlanning].[tAnalysisSet] WHERE [AnalysisSetID] in (2)
SET IDENTITY_INSERT [AdaptivePlanning].[tAnalysisSet] ON
INSERT INTO [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID], [Description], [Purpose], [BaseScenarioID], [StartDate], [EndDate], [IsArchived], [IsPublic], [IsForMargin], [IsForGeneration], [IsForEmissions], [IsForFuelBlends], [IsForReliability], [IsForHeatRate], [IsForFuelQuality], [IsForAllMetrics], [IsDynamicUpdate], [RunDate], [RunStatus], [CreatedBy], [CreateDate], [ChangedBy], [ChangeDate]) VALUES (2, N'Analysis Test 1', N'Analysis Test Purpose 1', 1, '20120101 00:00:00.000', '20121231 00:00:00.000', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, N'NA\bow68346', '20121119 09:41:36.000', N'NA\bow68346', '20121119 09:41:36.000')
SET IDENTITY_INSERT [AdaptivePlanning].[tAnalysisSet] OFF
DELETE FROM [AdaptivePlanning].[tAnalysisSetScenarioMap] WHERE [AnalysisSetScenarioMapID] in (5,6,7,8)
SET IDENTITY_INSERT [AdaptivePlanning].[tAnalysisSetScenarioMap] ON
INSERT INTO [AdaptivePlanning].[tAnalysisSetScenarioMap] ([AnalysisSetScenarioMapID], [AnalysisSetID], [ScenarioID]) VALUES (5, 2, 1)
INSERT INTO [AdaptivePlanning].[tAnalysisSetScenarioMap] ([AnalysisSetScenarioMapID], [AnalysisSetID], [ScenarioID]) VALUES (6, 2, 2)
INSERT INTO [AdaptivePlanning].[tAnalysisSetScenarioMap] ([AnalysisSetScenarioMapID], [AnalysisSetID], [ScenarioID]) VALUES (7, 2, 3)
INSERT INTO [AdaptivePlanning].[tAnalysisSetScenarioMap] ([AnalysisSetScenarioMapID], [AnalysisSetID], [ScenarioID]) VALUES (8, 2, 4)
SET IDENTITY_INSERT [AdaptivePlanning].[tAnalysisSetScenarioMap] OFF 
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 0 AND [SecurityRoleID] = 0)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (0, 0)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 1 AND [SecurityRoleID] = 1)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (1, 1)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 2 AND [SecurityRoleID] = 2)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (2, 2)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 3 AND [SecurityRoleID] = 3)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (3, 3)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 49 AND [SecurityRoleID] = 1)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (49, 1)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 54 AND [SecurityRoleID] = 1)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (54, 1)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 57 AND [SecurityRoleID] = 1)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (57, 1)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 55 AND [SecurityRoleID] = 1)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (55, 1)
IF NOT EXISTS (SELECT * FROM [AccessControl].[tUserRoleMap] WHERE [SecurityUserID] = 22 AND [SecurityRoleID] = 1)
	INSERT INTO [AccessControl].[tUserRoleMap] ([SecurityUserID], [SecurityRoleID]) VALUES (22, 1)

DELETE FROM [Projection].[tFuelPlanSet] WHERE [FuelPlanSetID] in (1,2,3,4)
SET IDENTITY_INSERT [Projection].[tFuelPlanSet] ON
INSERT INTO [Projection].[tFuelPlanSet] ([FuelPlanSetID], [FuelPlanSetDesc], [DataVersionID], [Comment], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (1, N'Analysis Test 1', 1, NULL, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.493', NULL, '20121119 08:19:02.493', '20121119 08:19:02.493', NULL, 1, 1, 0)
INSERT INTO [Projection].[tFuelPlanSet] ([FuelPlanSetID], [FuelPlanSetDesc], [DataVersionID], [Comment], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (2, N'Analysis Test 2', 1, NULL, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.530', NULL, '20121119 08:19:02.530', '20121119 08:19:02.530', NULL, 1, 1, 0)
INSERT INTO [Projection].[tFuelPlanSet] ([FuelPlanSetID], [FuelPlanSetDesc], [DataVersionID], [Comment], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (3, N'Analysis Test 3', 1, NULL, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.540', NULL, '20121119 08:19:02.540', '20121119 08:19:02.540', NULL, 1, 1, 0)
INSERT INTO [Projection].[tFuelPlanSet] ([FuelPlanSetID], [FuelPlanSetDesc], [DataVersionID], [Comment], [PlanYear], [IsConsensus], [IsSensitivity], [CreatedBy], [ChangedBy], [CreateDate], [SensitivityImpactTypeID], [ModifiedDate], [ChangeDate], [ImportDate], [IsPublic], [IsValid], [IsArchived]) VALUES (4, N'Analysis Test 4', 1, NULL, 2012, 1, 0, N'Test', N'Test', '20121119 08:19:02.547', NULL, '20121119 08:19:02.547', '20121119 08:19:02.547', NULL, 1, 1, 0)
SET IDENTITY_INSERT [Projection].[tFuelPlanSet] OFF

DELETE FROM [AdaptivePlanning].[tFuelPlanAdjustment] WHERE [FuelPlanAdjustmentID] in (1,2,3,4,5,6)
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustment] ON
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID], [Description], [FuelPlanAdjustmentSetID], [StartDate], [EndDate], [IsWeekDayOnPeak], [IsWeekDayOffPeak], [IsWeekEndOnPeak], [IsWeekEndOffPeak], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (1, N'BB Jan-Dec 2012', 1, '20120101 00:00:00.000', '20121231 00:00:00.000', 1, 1, 0, 0, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID], [Description], [FuelPlanAdjustmentSetID], [StartDate], [EndDate], [IsWeekDayOnPeak], [IsWeekDayOffPeak], [IsWeekEndOnPeak], [IsWeekEndOffPeak], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (2, N'MON Jan-Dec 2012', 1, '20120101 00:00:00.000', '20121231 00:00:00.000', 0, 0, 1, 1, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID], [Description], [FuelPlanAdjustmentSetID], [StartDate], [EndDate], [IsWeekDayOnPeak], [IsWeekDayOffPeak], [IsWeekEndOnPeak], [IsWeekEndOffPeak], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (3, N'ML May-Sep 2012', 2, '20120501 00:00:00.000', '20120930 00:00:00.000', 1, 1, 1, 1, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID], [Description], [FuelPlanAdjustmentSetID], [StartDate], [EndDate], [IsWeekDayOnPeak], [IsWeekDayOffPeak], [IsWeekEndOnPeak], [IsWeekEndOffPeak], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (4, N'SAN Jun-Aug 2012', 2, '20120601 00:00:00.000', '20120830 00:00:00.000', 1, 0, 0, 0, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID], [Description], [FuelPlanAdjustmentSetID], [StartDate], [EndDate], [IsWeekDayOnPeak], [IsWeekDayOffPeak], [IsWeekEndOnPeak], [IsWeekEndOffPeak], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (5, N'BB2 Feb-Apr 2012', 3, '20120201 00:00:00.000', '20120401 00:00:00.000', 1, 1, 1, 1, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID], [Description], [FuelPlanAdjustmentSetID], [StartDate], [EndDate], [IsWeekDayOnPeak], [IsWeekDayOffPeak], [IsWeekEndOnPeak], [IsWeekEndOffPeak], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (6, N'BB1 Feb-Apr 2012', 3, '20120201 00:00:00.000', '20120401 00:00:00.000', 1, 1, 1, 1, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustment] OFF

DELETE FROM [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] WHERE [FuelPlanAdjustmentUnitMapID] in (1,2,3,4,5,6,7,8,9,10,11,12,13)
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ON
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (1, 1, 10)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (2, 1, 11)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (3, 2, 22)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (4, 2, 23)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (5, 2, 24)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (6, 3, 2)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (7, 3, 3)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (8, 3, 4)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (9, 4, 39)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (10, 4, 40)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (11, 4, 41)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (12, 5, 11)
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ([FuelPlanAdjustmentUnitMapID], [FuelPlanAdjustmentID], [UnitID]) VALUES (13, 6, 10)
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] OFF

DELETE FROM [AccessControl].[tAssetAccessRule] WHERE [AssetAccessRuleID] in (0,1,2,3)
SET IDENTITY_INSERT [AccessControl].[tAssetAccessRule] ON
INSERT INTO [AccessControl].[tAssetAccessRule] ([AssetAccessRuleID], [SecurityRoleID], [AssetID], [Allowed]) VALUES (0, 0, 5, 1)
INSERT INTO [AccessControl].[tAssetAccessRule] ([AssetAccessRuleID], [SecurityRoleID], [AssetID], [Allowed]) VALUES (1, 1, 25, 1)
INSERT INTO [AccessControl].[tAssetAccessRule] ([AssetAccessRuleID], [SecurityRoleID], [AssetID], [Allowed]) VALUES (2, 2, 26, 1)
INSERT INTO [AccessControl].[tAssetAccessRule] ([AssetAccessRuleID], [SecurityRoleID], [AssetID], [Allowed]) VALUES (3, 3, 7, 0)
SET IDENTITY_INSERT [AccessControl].[tAssetAccessRule] OFF

DELETE FROM [Projection].[tFuelChangeEvent] WHERE [FuelChangeEventID] <= 140
SET IDENTITY_INSERT [Projection].[tFuelChangeEvent] ON
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (1, 1, 2, '20121030 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (2, 1, 3, '20121020 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (3, 1, 4, '20121010 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (4, 1, 10, '20120811 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (5, 1, 11, '20120801 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (6, 1, 22, '20120413 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (7, 1, 23, '20120403 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (8, 1, 24, '20120324 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (9, 1, 30, '20120124 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (10, 1, 31, '20120114 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (11, 1, 39, '20111026 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (12, 1, 40, '20111016 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (13, 1, 41, '20111006 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (14, 1, 46, '20110817 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (15, 1, 47, '20110807 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (16, 1, 48, '20110728 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (17, 1, 49, '20110718 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (18, 1, 50, '20110708 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (19, 1, 51, '20110628 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (20, 1, 52, '20110618 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (21, 1, 53, '20110608 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (22, 1, 54, '20110529 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (23, 1, 55, '20110519 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (24, 1, 56, '20110509 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (25, 1, 57, '20110429 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (26, 1, 58, '20110419 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (27, 1, 59, '20110409 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (28, 1, 60, '20110330 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (29, 1, 61, '20110320 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (30, 1, 62, '20110310 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (31, 1, 63, '20110228 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (32, 1, 64, '20110218 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (33, 1, 65, '20110208 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (34, 1, 66, '20110129 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (35, 1, 67, '20110119 08:19:02.5270000', 1)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (36, 2, 2, '20121030 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (37, 2, 3, '20121020 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (38, 2, 4, '20121010 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (39, 2, 10, '20120811 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (40, 2, 11, '20120801 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (41, 2, 22, '20120413 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (42, 2, 23, '20120403 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (43, 2, 24, '20120324 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (44, 2, 30, '20120124 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (45, 2, 31, '20120114 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (46, 2, 39, '20111026 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (47, 2, 40, '20111016 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (48, 2, 41, '20111006 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (49, 2, 46, '20110817 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (50, 2, 47, '20110807 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (51, 2, 48, '20110728 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (52, 2, 49, '20110718 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (53, 2, 50, '20110708 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (54, 2, 51, '20110628 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (55, 2, 52, '20110618 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (56, 2, 53, '20110608 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (57, 2, 54, '20110529 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (58, 2, 55, '20110519 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (59, 2, 56, '20110509 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (60, 2, 57, '20110429 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (61, 2, 58, '20110419 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (62, 2, 59, '20110409 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (63, 2, 60, '20110330 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (64, 2, 61, '20110320 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (65, 2, 62, '20110310 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (66, 2, 63, '20110228 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (67, 2, 64, '20110218 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (68, 2, 65, '20110208 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (69, 2, 66, '20110129 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (70, 2, 67, '20110119 08:19:02.5370000', 2)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (71, 3, 2, '20121030 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (72, 3, 3, '20121020 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (73, 3, 4, '20121010 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (74, 3, 10, '20120811 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (75, 3, 11, '20120801 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (76, 3, 22, '20120413 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (77, 3, 23, '20120403 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (78, 3, 24, '20120324 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (79, 3, 30, '20120124 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (80, 3, 31, '20120114 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (81, 3, 39, '20111026 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (82, 3, 40, '20111016 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (83, 3, 41, '20111006 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (84, 3, 46, '20110817 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (85, 3, 47, '20110807 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (86, 3, 48, '20110728 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (87, 3, 49, '20110718 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (88, 3, 50, '20110708 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (89, 3, 51, '20110628 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (90, 3, 52, '20110618 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (91, 3, 53, '20110608 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (92, 3, 54, '20110529 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (93, 3, 55, '20110519 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (94, 3, 56, '20110509 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (95, 3, 57, '20110429 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (96, 3, 58, '20110419 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (97, 3, 59, '20110409 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (98, 3, 60, '20110330 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (99, 3, 61, '20110320 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (100, 3, 62, '20110310 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (101, 3, 63, '20110228 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (102, 3, 64, '20110218 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (103, 3, 65, '20110208 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (104, 3, 66, '20110129 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (105, 3, 67, '20110119 08:19:02.5430000', 3)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (106, 4, 2, '20121030 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (107, 4, 3, '20121020 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (108, 4, 4, '20121010 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (109, 4, 10, '20120811 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (110, 4, 11, '20120801 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (111, 4, 22, '20120413 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (112, 4, 23, '20120403 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (113, 4, 24, '20120324 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (114, 4, 30, '20120124 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (115, 4, 31, '20120114 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (116, 4, 39, '20111026 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (117, 4, 40, '20111016 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (118, 4, 41, '20111006 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (119, 4, 46, '20110817 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (120, 4, 47, '20110807 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (121, 4, 48, '20110728 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (122, 4, 49, '20110718 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (123, 4, 50, '20110708 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (124, 4, 51, '20110628 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (125, 4, 52, '20110618 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (126, 4, 53, '20110608 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (127, 4, 54, '20110529 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (128, 4, 55, '20110519 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (129, 4, 56, '20110509 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (130, 4, 57, '20110429 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (131, 4, 58, '20110419 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (132, 4, 59, '20110409 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (133, 4, 60, '20110330 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (134, 4, 61, '20110320 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (135, 4, 62, '20110310 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (136, 4, 63, '20110228 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (137, 4, 64, '20110218 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (138, 4, 65, '20110208 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (139, 4, 66, '20110129 08:19:02.5470000', 4)
INSERT INTO [Projection].[tFuelChangeEvent] ([FuelChangeEventID], [FuelPlanSetID], [UnitID], [EventTime], [FuelSourceID]) VALUES (140, 4, 67, '20110119 08:19:02.5470000', 4)
SET IDENTITY_INSERT [Projection].[tFuelChangeEvent] OFF

DELETE FROM [AccessControl].[tRole] WHERE [SecurityRoleID] in (0,1,2,3)
SET IDENTITY_INSERT [AccessControl].[tRole] ON
INSERT INTO [AccessControl].[tRole] ([SecurityRoleID], [ParentSecurityRoleID], [Left], [Right], [BeingMoved], [Text], [IsReadOnly], [IsDeleted]) VALUES (0, -1, 0, 0, 0, N'TEST', 1, 0)
INSERT INTO [AccessControl].[tRole] ([SecurityRoleID], [ParentSecurityRoleID], [Left], [Right], [BeingMoved], [Text], [IsReadOnly], [IsDeleted]) VALUES (1, -1, 0, 0, 0, N'COAL', 0, 0)
INSERT INTO [AccessControl].[tRole] ([SecurityRoleID], [ParentSecurityRoleID], [Left], [Right], [BeingMoved], [Text], [IsReadOnly], [IsDeleted]) VALUES (2, -1, 0, 0, 0, N'GAS', 0, 0)
INSERT INTO [AccessControl].[tRole] ([SecurityRoleID], [ParentSecurityRoleID], [Left], [Right], [BeingMoved], [Text], [IsReadOnly], [IsDeleted]) VALUES (3, 1, 0, 0, 0, N'CNM_ROLE', 0, 0)
SET IDENTITY_INSERT [AccessControl].[tRole] OFF

DELETE FROM [Asset].[tAssetAttribute] WHERE [AssetAttributeID] in (0)
SET IDENTITY_INSERT [Asset].[tAssetAttribute] ON
INSERT INTO [Asset].[tAssetAttribute] ([AssetAttributeID], [AssetID], [AttributeTypeID], [AttributeOptionTypeID], [Attribute_int], [Attribute_string], [Attribute_float]) VALUES (0, 62, 1, 2, 42, N'TEST,STRING', 0.42)
SET IDENTITY_INSERT [Asset].[tAssetAttribute] OFF

DELETE FROM [AdaptivePlanning].[tFuelPlanAdjustmentSet] WHERE [FuelPlanAdjustmentSetID] in (1,2,3)
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustmentSet] ON
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentSet] ([FuelPlanAdjustmentSetID], [Description], [AnalysisSetID], [BaseFuelPlanSetID], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (1, N'BB,MON for Jan-Dec 2012', 2, 1, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentSet] ([FuelPlanAdjustmentSetID], [Description], [AnalysisSetID], [BaseFuelPlanSetID], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (2, N'ML,SAN for May-Sep 2012', 2, 2, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentSet] ([FuelPlanAdjustmentSetID], [Description], [AnalysisSetID], [BaseFuelPlanSetID], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (3, N'BB2,BB1 for Feb-Apr 2012', 2, 3, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustmentSet] OFF

DELETE FROM [AdaptivePlanning].[tFuelPlanAdjustmentDetail] WHERE [FuelPlanAdjustmentDetailID] in (1,2,3,4,5,6,7,8)
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ON
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (1, 1, 10, 1, NULL, NULL, NULL, NULL, 1, 20, NULL, 30, NULL, 40, NULL, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (2, 1, 11, 1, NULL, 15, NULL, NULL, 0, NULL, 25, NULL, 35, NULL, 45, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (3, 2, 22, 1, NULL, NULL, 1, 2, NULL, 3, 4, 5, 6, 7, 8, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (4, 2, 23, 1, NULL, NULL, 9, 10, 0, 11, 12, 13, 14, 15, 16, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (5, 2, 24, 1, NULL, NULL, 17, 18, 1, 19, 20, 21, 22, 23, 24, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (6, 4, 39, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (7, 4, 40, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
INSERT INTO [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ([FuelPlanAdjustmentDetailID], [FuelPlanAdjustmentID], [UnitID], [FuelSourceID], [WeekdaysOnPeak_pct], [WeekdaysOffPeak_pct], [WeekendsOnPeak_pct], [WeekendsOffPeak_pct], [IsAnnuallyRecurring], [HHV_btu_per_lbm], [SO2_pct], [Ash_pct], [Moisture_pct], [Iron_pct], [Hg_ppm], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate]) VALUES (8, 4, 41, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'NA\bow68346', N'NA\bow68346', '20121119 09:41:36.000', '20121119 09:41:36.000')
SET IDENTITY_INSERT [AdaptivePlanning].[tFuelPlanAdjustmentDetail] OFF

delete from projection.tScenarioResultMetrics
go

insert into 
projection.tScenarioResultMetrics (scenarioid,AssetID,year, 
	[OperatingHours]
      ,[FuelBurn_mbtu]
      ,[FuelBurn_tons]
      ,[Particulate_tons]
      ,[TotalRevenue_dollars]
      ,[FuelCost_dollars]
      ,[TotalVariableOandM_dollars]
      ,[Generation_mwh]
      ,[EAF_pct]
      ,[ReliabilityIndex_pct]
      ,[PlannedMaintenance_days]
      ,[BalanceOfPlant_pct]
      ,[BoilerTubeFailureRate_pct]
      ,[SO2Emissions_tons]
      ,[SO2InletEmissions_tons]
      ,[NOxEmissions_tons]
      ,[HgEmissions_lbm]
      ,[CO2Emissions_ktons]
      ,[HHV_btu]

      ,[CreatedBy]

      ,[ChangedBy]
)
select b.ScenarioID, a.AssetID, 2013, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
'BV','BV'
 from (select assetid from asset.tasset a join asset.tassetclasstype b on a.assetclasstypeid=b.assetclasstypeid join asset.tAssetType c on b.AssetTypeID=c.AssetTypeID  where 
c.AssetTypeAbbrev='UN' and b.AssetClassTypeID = 4001) a, projection.tScenario b

insert into 
projection.tScenarioResultMetrics (scenarioid,AssetID,year, 
	[OperatingHours]
      ,[FuelBurn_mbtu]
      ,[FuelBurn_tons]
      ,[Particulate_tons]
      ,[TotalRevenue_dollars]
      ,[FuelCost_dollars]
      ,[TotalVariableOandM_dollars]
      ,[Generation_mwh]
      ,[EAF_pct]
      ,[ReliabilityIndex_pct]
      ,[PlannedMaintenance_days]
      ,[BalanceOfPlant_pct]
      ,[BoilerTubeFailureRate_pct]
      ,[SO2Emissions_tons]
      ,[SO2InletEmissions_tons]
      ,[NOxEmissions_tons]
      ,[HgEmissions_lbm]
      ,[CO2Emissions_ktons]
      ,[HHV_btu]

      ,[CreatedBy]

      ,[ChangedBy]
)
select distinct b.ScenarioID, a.AssetID, 2013, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
'BV','BV'
 from (select parentasset.assetid from asset.tasset a join asset.tAsset parentasset on a.ParentAssetID=parentasset.assetid join asset.tassetclasstype b on a.assetclasstypeid=b.assetclasstypeid join asset.tAssetType c on b.AssetTypeID=c.AssetTypeID  where 
c.AssetTypeAbbrev='UN' and b.AssetClassTypeID = 4001) a, projection.tScenario b

insert into 
projection.tScenarioResultMetrics (scenarioid,AssetID,year, 
	[OperatingHours]
      ,[FuelBurn_mbtu]
      ,[FuelBurn_tons]
      ,[Particulate_tons]
      ,[TotalRevenue_dollars]
      ,[FuelCost_dollars]
      ,[TotalVariableOandM_dollars]
      ,[Generation_mwh]
      ,[EAF_pct]
      ,[ReliabilityIndex_pct]
      ,[PlannedMaintenance_days]
      ,[BalanceOfPlant_pct]
      ,[BoilerTubeFailureRate_pct]
      ,[SO2Emissions_tons]
      ,[SO2InletEmissions_tons]
      ,[NOxEmissions_tons]
      ,[HgEmissions_lbm]
      ,[CO2Emissions_ktons]
      ,[HHV_btu]

      ,[CreatedBy]

      ,[ChangedBy]
)
select distinct b.ScenarioID, a.AssetID, 2013, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
'BV','BV'
 from (select parentparentasset.assetid from asset.tasset a join asset.tAsset parentasset on a.ParentAssetID=parentasset.assetid 
 join asset.tAsset parentparentasset on parentasset.parentassetid=parentparentasset.assetid
 join asset.tassetclasstype b on a.assetclasstypeid=b.assetclasstypeid join asset.tAssetType c on b.AssetTypeID=c.AssetTypeID  where 
c.AssetTypeAbbrev='UN' and b.AssetClassTypeID = 4001) a, projection.tScenario b

insert into 
projection.tScenarioResultMetrics (scenarioid,AssetID,year, 
	[OperatingHours]
      ,[FuelBurn_mbtu]
      ,[FuelBurn_tons]
      ,[Particulate_tons]
      ,[TotalRevenue_dollars]
      ,[FuelCost_dollars]
      ,[TotalVariableOandM_dollars]
      ,[Generation_mwh]
      ,[EAF_pct]
      ,[ReliabilityIndex_pct]
      ,[PlannedMaintenance_days]
      ,[BalanceOfPlant_pct]
      ,[BoilerTubeFailureRate_pct]
      ,[SO2Emissions_tons]
      ,[SO2InletEmissions_tons]
      ,[NOxEmissions_tons]
      ,[HgEmissions_lbm]
      ,[CO2Emissions_ktons]
      ,[HHV_btu]

      ,[CreatedBy]

      ,[ChangedBy]
)
select distinct b.ScenarioID, a.AssetID, 2013, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
'BV','BV'
 from (select parentparentparentasset.assetid from asset.tasset a join asset.tAsset parentasset on a.ParentAssetID=parentasset.assetid 
 join asset.tAsset parentparentasset on parentasset.parentassetid=parentparentasset.assetid
 join asset.tAsset parentparentparentasset on parentparentasset.parentassetid=parentparentparentasset.assetid
 join asset.tassetclasstype b on a.assetclasstypeid=b.assetclasstypeid join asset.tAssetType c on b.AssetTypeID=c.AssetTypeID  where 
c.AssetTypeAbbrev='UN' and b.AssetClassTypeID = 4001) a, projection.tScenario b

declare @assetid as int
declare @scenarioid as int

declare curs cursor for 
select scenarioid,assetid from (select assetid from asset.tasset a join asset.tassetclasstype b on a.assetclasstypeid=b.assetclasstypeid join asset.tAssetType c on b.AssetTypeID=c.AssetTypeID  where 
c.AssetTypeAbbrev='UN' and b.AssetClassTypeID = 4001) a, projection.tScenario b

open curs
fetch next from curs into @scenarioid,@assetid
while @@FETCH_STATUS=0
  begin
  
  update 
projection.tScenarioResultMetrics 
set [OperatingHours] = [OperatingHours] + 6720 * RAND(), [FuelBurn_mbtu] = [FuelBurn_mbtu] + 10000000 + 500000*RAND(),[FuelBurn_tons] = [FuelBurn_tons] + 5000000 + 5000000 * RAND(),
[Particulate_tons] = [Particulate_tons] + 2500  + 2500*RAND(), 
[TotalRevenue_dollars] = [TotalRevenue_dollars] + 200000000 + 200000000 * RAND(),
[FuelCost_dollars] = [FuelCost_dollars] + 100000000 + 200000000 * RAND(), [TotalVariableOandM_dollars] = [TotalVariableOandM_dollars] + 100000000 + 200000000 * RAND(), 
[Generation_mwh] = [Generation_mwh] + 10000000 + 10000000*RAND(), [EAF_pct] = [EAF_pct] + 75 + 25 * RAND(),[ReliabilityIndex_pct] = [ReliabilityIndex_pct] + 75 + 25 * RAND(), 
[PlannedMaintenance_days] = [PlannedMaintenance_days] + 30 * RAND(), 
[BalanceOfPlant_pct] = [BalanceOfPlant_pct] + 10 * RAND(), [BoilerTubeFailureRate_pct] = [BoilerTubeFailureRate_pct] + 5 * RAND(),
[SO2Emissions_tons] = [SO2Emissions_tons] + 20000 + 5000 * RAND(),[SO2InletEmissions_tons] = [SO2InletEmissions_tons] +  10000 + 5000 * RAND(), [NOxEmissions_tons] = [NOxEmissions_tons] + 500 + 500 * RAND(), 
[HgEmissions_lbm] = [HgEmissions_lbm] + 15000 + 5000 * RAND(), [CO2Emissions_ktons] = [CO2Emissions_ktons] + 150000 + 50000 * RAND(), [HHV_btu] = [HHV_btu] + 1 + 1*RAND(),
[CreatedBy] = 'BV',[ChangedBy] = 'BV'
where scenarioid=@scenarioid and assetid=@assetid and year=2013
  fetch next from curs into @scenarioid,@assetid
  end
close curs
deallocate curs

update 
projection.tScenarioResultMetrics 
set [OperatingHours] = a.OperatingHours,
	[FuelBurn_mbtu] = a.FuelBurn_mbtu
      ,[FuelBurn_tons] = a.FuelBurn_tons
      ,[Particulate_tons] = a.Particulate_tons
      ,[GrossMargin_dollars] = a.GrossMargin_dollars
      ,[TotalRevenue_dollars] = a.TotalRevenue_dollars
      ,[FuelCost_dollars] = a.FuelCost_dollars
      ,[TotalVariableOandM_dollars] = a.TotalVariableOandM_dollars
      ,[Generation_mwh] = a.Generation_mwh
      ,[MaximumAchievable_Load_mw] = a.MaximumAchievable_Load_mw
      ,[EAF_pct] = a.EAF_pct
      ,[ReliabilityIndex_pct] = a.ReliabilityIndex_pct
      ,[PlannedMaintenance_days] = a.PlannedMaintenance_days
      ,[BalanceOfPlant_pct] = a.BalanceOfPlant_pct
      ,[BoilerTubeFailureRate_pct] = a.BoilerTubeFailureRate_pct
      ,[SO2Emissions_tons] = a.SO2Emissions_tons
      ,[SO2InletEmissions_tons] = a.SO2InletEmissions_tons
      ,[NOxEmissions_tons] = a.NOxEmissions_tons
      ,[HgEmissions_lbm] = a.HgEmissions_lbm
      ,[CO2Emissions_ktons] = a.CO2Emissions_ktons
      ,[HHV_btu] = a.HHV_btu
 FROM 
(select a.ScenarioID,b.parentassetid as assetid,a.year, SUM(a.OperatingHours) as operatinghours,
Sum(FuelBurn_mbtu) as FuelBurn_mbtu
      ,Sum(FuelBurn_tons) as FuelBurn_tons
      ,Sum(Particulate_tons) as Particulate_tons
      ,Sum(GrossMargin_dollars) as GrossMargin_dollars
      ,Sum(TotalRevenue_dollars) as TotalRevenue_dollars
      ,Sum(FuelCost_dollars) as FuelCost_dollars
      ,Sum(TotalVariableOandM_dollars) as TotalVariableOandM_dollars
      ,Sum(Generation_mwh) as Generation_mwh
      ,Sum(MaximumAchievable_Load_mw) as MaximumAchievable_Load_mw
      ,Sum(EAF_pct) as EAF_pct
      ,Sum(ReliabilityIndex_pct) as ReliabilityIndex_pct
      ,Sum(PlannedMaintenance_days) as PlannedMaintenance_days
      ,Sum(BalanceOfPlant_pct) as BalanceOfPlant_pct
      ,Sum(BoilerTubeFailureRate_pct) as BoilerTubeFailureRate_pct
      ,Sum(SO2Emissions_tons) as SO2Emissions_tons
      ,Sum(SO2InletEmissions_tons) as SO2InletEmissions_tons
      ,Sum(NOxEmissions_tons) as NOxEmissions_tons
      ,Sum(HgEmissions_lbm) as HgEmissions_lbm
      ,Sum(CO2Emissions_ktons) as CO2Emissions_ktons
      ,Sum(HHV_btu) as HHV_btu
 from projection.tScenarioResultMetrics a JOIN asset.tasset b on a.AssetID = b.assetid 
 join asset.tAssetClassType c on b.AssetClassTypeID = c.AssetClassTypeID join 
 asset.tAssetType d on c.assettypeid=d.assettypeid
 where d.AssetTypeAbbrev='UN'
 group by 
a.ScenarioID,b.parentassetid,a.year
) a
where 
projection.tScenarioResultMetrics.ScenarioID = a.ScenarioID and projection.tScenarioResultMetrics.AssetID = a.assetid and projection.tScenarioResultMetrics.Year=a.Year

update 
projection.tScenarioResultMetrics 
set [OperatingHours] = a.OperatingHours,
	[FuelBurn_mbtu] = a.FuelBurn_mbtu
      ,[FuelBurn_tons] = a.FuelBurn_tons
      ,[Particulate_tons] = a.Particulate_tons
      ,[GrossMargin_dollars] = a.GrossMargin_dollars
      ,[TotalRevenue_dollars] = a.TotalRevenue_dollars
      ,[FuelCost_dollars] = a.FuelCost_dollars
      ,[TotalVariableOandM_dollars] = a.TotalVariableOandM_dollars
      ,[Generation_mwh] = a.Generation_mwh
      ,[MaximumAchievable_Load_mw] = a.MaximumAchievable_Load_mw
      ,[EAF_pct] = a.EAF_pct
      ,[ReliabilityIndex_pct] = a.ReliabilityIndex_pct
      ,[PlannedMaintenance_days] = a.PlannedMaintenance_days
      ,[BalanceOfPlant_pct] = a.BalanceOfPlant_pct
      ,[BoilerTubeFailureRate_pct] = a.BoilerTubeFailureRate_pct
      ,[SO2Emissions_tons] = a.SO2Emissions_tons
      ,[SO2InletEmissions_tons] = a.SO2InletEmissions_tons
      ,[NOxEmissions_tons] = a.NOxEmissions_tons
      ,[HgEmissions_lbm] = a.HgEmissions_lbm
      ,[CO2Emissions_ktons] = a.CO2Emissions_ktons
      ,[HHV_btu] = a.HHV_btu
 FROM 
(select a.ScenarioID,b.parentassetid as assetid,a.year, SUM(a.OperatingHours) as operatinghours,
Sum(FuelBurn_mbtu) as FuelBurn_mbtu
      ,Sum(FuelBurn_tons) as FuelBurn_tons
      ,Sum(Particulate_tons) as Particulate_tons
      ,Sum(GrossMargin_dollars) as GrossMargin_dollars
      ,Sum(TotalRevenue_dollars) as TotalRevenue_dollars
      ,Sum(FuelCost_dollars) as FuelCost_dollars
      ,Sum(TotalVariableOandM_dollars) as TotalVariableOandM_dollars
      ,Sum(Generation_mwh) as Generation_mwh
      ,Sum(MaximumAchievable_Load_mw) as MaximumAchievable_Load_mw
      ,Sum(EAF_pct) as EAF_pct
      ,Sum(ReliabilityIndex_pct) as ReliabilityIndex_pct
      ,Sum(PlannedMaintenance_days) as PlannedMaintenance_days
      ,Sum(BalanceOfPlant_pct) as BalanceOfPlant_pct
      ,Sum(BoilerTubeFailureRate_pct) as BoilerTubeFailureRate_pct
      ,Sum(SO2Emissions_tons) as SO2Emissions_tons
      ,Sum(SO2InletEmissions_tons) as SO2InletEmissions_tons
      ,Sum(NOxEmissions_tons) as NOxEmissions_tons
      ,Sum(HgEmissions_lbm) as HgEmissions_lbm
      ,Sum(CO2Emissions_ktons) as CO2Emissions_ktons
      ,Sum(HHV_btu) as HHV_btu
 from projection.tScenarioResultMetrics a JOIN asset.tasset b on a.AssetID = b.assetid 
 join asset.tAssetClassType c on b.AssetClassTypeID = c.AssetClassTypeID join 
 asset.tAssetType d on c.assettypeid=d.assettypeid
 where d.AssetTypeAbbrev='ST'
 group by 
a.ScenarioID,b.parentassetid,a.year
) a
where 
projection.tScenarioResultMetrics.ScenarioID = a.ScenarioID and projection.tScenarioResultMetrics.AssetID = a.assetid and projection.tScenarioResultMetrics.Year=a.Year

update 
projection.tScenarioResultMetrics 
set [OperatingHours] = a.OperatingHours,
	[FuelBurn_mbtu] = a.FuelBurn_mbtu
      ,[FuelBurn_tons] = a.FuelBurn_tons
      ,[Particulate_tons] = a.Particulate_tons
      ,[GrossMargin_dollars] = a.GrossMargin_dollars
      ,[TotalRevenue_dollars] = a.TotalRevenue_dollars
      ,[FuelCost_dollars] = a.FuelCost_dollars
      ,[TotalVariableOandM_dollars] = a.TotalVariableOandM_dollars
      ,[Generation_mwh] = a.Generation_mwh
      ,[MaximumAchievable_Load_mw] = a.MaximumAchievable_Load_mw
      ,[EAF_pct] = a.EAF_pct
      ,[ReliabilityIndex_pct] = a.ReliabilityIndex_pct
      ,[PlannedMaintenance_days] = a.PlannedMaintenance_days
      ,[BalanceOfPlant_pct] = a.BalanceOfPlant_pct
      ,[BoilerTubeFailureRate_pct] = a.BoilerTubeFailureRate_pct
      ,[SO2Emissions_tons] = a.SO2Emissions_tons
      ,[SO2InletEmissions_tons] = a.SO2InletEmissions_tons
      ,[NOxEmissions_tons] = a.NOxEmissions_tons
      ,[HgEmissions_lbm] = a.HgEmissions_lbm
      ,[CO2Emissions_ktons] = a.CO2Emissions_ktons
      ,[HHV_btu] = a.HHV_btu
 FROM 
(select a.ScenarioID,b.parentassetid as assetid,a.year, SUM(a.OperatingHours) as operatinghours,
Sum(FuelBurn_mbtu) as FuelBurn_mbtu
      ,Sum(FuelBurn_tons) as FuelBurn_tons
      ,Sum(Particulate_tons) as Particulate_tons
      ,Sum(GrossMargin_dollars) as GrossMargin_dollars
      ,Sum(TotalRevenue_dollars) as TotalRevenue_dollars
      ,Sum(FuelCost_dollars) as FuelCost_dollars
      ,Sum(TotalVariableOandM_dollars) as TotalVariableOandM_dollars
      ,Sum(Generation_mwh) as Generation_mwh
      ,Sum(MaximumAchievable_Load_mw) as MaximumAchievable_Load_mw
      ,Sum(EAF_pct) as EAF_pct
      ,Sum(ReliabilityIndex_pct) as ReliabilityIndex_pct
      ,Sum(PlannedMaintenance_days) as PlannedMaintenance_days
      ,Sum(BalanceOfPlant_pct) as BalanceOfPlant_pct
      ,Sum(BoilerTubeFailureRate_pct) as BoilerTubeFailureRate_pct
      ,Sum(SO2Emissions_tons) as SO2Emissions_tons
      ,Sum(SO2InletEmissions_tons) as SO2InletEmissions_tons
      ,Sum(NOxEmissions_tons) as NOxEmissions_tons
      ,Sum(HgEmissions_lbm) as HgEmissions_lbm
      ,Sum(CO2Emissions_ktons) as CO2Emissions_ktons
      ,Sum(HHV_btu) as HHV_btu
 from projection.tScenarioResultMetrics a JOIN asset.tasset b on a.AssetID = b.assetid 
 join asset.tAssetClassType c on b.AssetClassTypeID = c.AssetClassTypeID join 
 asset.tAssetType d on c.assettypeid=d.assettypeid
 where d.AssetTypeAbbrev='SG'
 group by 
a.ScenarioID,b.parentassetid,a.year
) a
where 
projection.tScenarioResultMetrics.ScenarioID = a.ScenarioID and projection.tScenarioResultMetrics.AssetID = a.assetid and projection.tScenarioResultMetrics.Year=a.Year

DELETE FROM [AccessControl].[tUser] WHERE [SecurityUserID] <=57
SET IDENTITY_INSERT [AccessControl].[tUser] ON
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (0, N'TEST', N'4e0c6a8c-5a05-436d-9c76-894b3de1e287', N'TEST@test.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (1, N'COAL', N'4c8fd5db-9fe1-4719-8e18-dd521d94d237', N'Coal@test.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (2, N'GAS', N'd7a85978-7062-4ee8-b97c-5966d895bd06', N'GAS@test.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (3, N'COAL_NO_MONTICELLO', N'9b696bfc-76f2-4ec9-bad0-18166875f279', N'COAL_NO_MONTICELLO@test.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (6, N'NA\sch13824', N'1c221214-7fff-4fd8-a227-bbbf06312c7f', N'schumacherbj@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (7, N'NA\woo17439', N'925b072f-9667-4244-849b-19366fa07d6a', N'woodmm@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (8, N'MD-DT-01\opm', N'7ac8ff34-ad42-41ec-b5b2-aebf5e8233bc', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (9, N'MD-DT-02\opm', N'02c73676-9b98-40e5-8fc9-b002515ca80e', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (10, N'MD-DT-03\opm', N'8cb60345-c511-4891-baf2-b5acdb70cb6d', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (11, N'MD-DT-04\opm', N'df461f53-925a-4f62-8322-aa90c4842b27', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (12, N'MD-DT-05\opm', N'3deaa625-84ed-4a9d-a217-c00a416d6cd9', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (13, N'MD-DT-06\opm', N'ddfb49a4-c9c1-47e7-a1c8-dcad6502c4e2', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (14, N'MD-DT-07\opm', N'ae203560-95cd-464c-9107-fac10ab68cfc', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (15, N'NA\mer57646', N'4ef1b087-52f4-42f6-b1de-bc3aa846bdb9', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (16, N'NA\cas58784', N'7c6fded7-0e01-4ee5-ae44-a380f46f2bde', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (17, N'NA\new51415', N'90abeeae-881a-413f-b92b-968752e97f27', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (18, N'NA\kir43148', N'131dacb2-01d7-445a-a217-77a8d2b79902', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (19, N'NA\klo45545', N'c41d3888-1d0e-4360-be36-c6d59f4974d8', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (20, N'NA\tru49231', N'9aec742d-6da9-48cb-b617-6d3d6e00d89a', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (21, N'NA\tan26162', N'9afa8781-b253-48f8-9f0e-0dd388bf091b', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (22, N'NA\des23053', N'05ac7756-a3e3-4945-a53f-b69b2d2ca364', N'deschainerp@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (23, N'NA\cam15624', N'e07c29ec-2b92-4917-8db9-c500094adf61', N'campbellca@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (24, N'NA\pie13283', N'da4b924f-bcec-4087-ad92-ee07353d9233', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (25, N'NA\whi16867', N'66553df7-557d-4fce-9f1d-3163b434eeda', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (26, N'PPMD\new51415', N'1f9a6d64-7918-475f-aa4a-bf51607ef1d7', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (27, N'PPMD\cam15624', N'8e4b8258-cde3-4875-bc16-38a64909f0e7', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (28, N'PPMD\des23053', N'3d29bc16-bccb-4e26-9ee1-64cdcf972ebb', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (29, N'PPMD\kir43148', N'b69e71e1-f8fe-4218-9c43-f7c47be556d9', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (30, N'PPMD\klo45545', N'80e63014-fc89-4487-9443-e5500aaba057', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (31, N'PPMD\mer57646', N'60619732-39c1-4a1d-ae82-817d0a882091', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (32, N'PPMD\pie13283', N'0cc33968-8365-4d40-997f-cbcd86e4c5b8', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (33, N'PPMD\sch13824', N'798df063-6cfb-42e0-8f6a-84f5564aa3d4', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (34, N'PPMD\tan26162', N'721ba0e4-f9e5-4084-8dd9-fb508a3bfbd8', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (35, N'PPMD\tru49231', N'214f8d14-9344-4ff8-bfb5-1ebfc883bec0', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (36, N'PPMD\whi16867', N'a8259159-9eb5-426d-a56a-5357c08883a3', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (37, N'PPMD\woo17439', N'ad6cb9a0-d5e1-413e-8703-c388f3dab951', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (38, N'PPMD\cas58784', N'797477b7-e0b2-4f30-9958-02f6c3ba2886', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (39, N'PPMD\hus47238', N'2c3e8bc8-faa2-4561-b20a-dbf7e68fed3e', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (40, N'PPMD\roa64580', N'ab25265e-4025-47b2-86a4-cd114116714f', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (41, N'NA\roa64580', N'e95462c6-d5d5-4fab-a39b-9925288340df', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (42, N'ppmd-ts-cac\Administrator', N'135816ca-86e9-4e27-a1f0-697842e82000', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (43, N'PPMD\leo65431', N'57411118-7850-42ab-bea8-1f3f230056bd', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (44, N'NA\leo65431', N'13a3fd05-760a-462f-9c36-e4d781d02fdf', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (45, N'NA\hel45418', N'c6b19d09-9637-4782-b06c-0b0dde1a914e', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (46, N'PPMD\hel45418', N'aa0eb55a-9b52-4854-8537-98c6d46606ee', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (47, N'PPMD\str66428 ', N'2a348e22-c8b0-445f-a6a1-d189f9bd1771', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (48, N'PPMD\bak31726', N'538332b4-1b52-49f1-9ef4-298f38b53a6b', N'bakerbk_test@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (49, N'NA\bak31726', N'b124d128-31cd-4468-9add-3f368e579532', N'bakerbk@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (50, N'NA\nee20927', N'672a6f6b-ff96-40f8-b72e-11c307764ba3', N'neemanjj@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (51, N'asia\kha64736', N'c7b3c804-bef2-4080-8528-718c4d5322f3', NULL)
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (52, N'NA\hat68841', N'087cf443-60d2-4176-bbd1-db020bb661e4', N'hattrupg@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (53, N'NA\oeh67661', N'aefa6240-5281-459f-9ed1-fa92f7a1bea7', N'oehlerkingl@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (54, N'NA\bow68346', N'2d1ae1f0-ca7e-4dda-a2e3-c425e13e3f94', N'bowenjc@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (55, N'NA\lef63250', N'cb1df906-0e87-468e-a202-b148ef54c874', N'lefebvre@analytXbook.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (56, N'NA\sta07146', N'07981a33-2c83-4c92-8925-a85d622c465d', N'stallardgs@bv.com')
INSERT INTO [AccessControl].[tUser] ([SecurityUserID], [Username], [GlobalID], [Email]) VALUES (57, N'LUMINANT_DEMO', N'2b40da13-3b64-4141-8942-c0c1385e2be4', N'demo@luminant.com')
SET IDENTITY_INSERT [AccessControl].[tUser] OFF
ALTER TABLE [Projection].[tDataCommentDetail] ADD CONSTRAINT [FK_tDataCommentDetail_DataCommentSetID_tDataCommentSet] FOREIGN KEY ([DataCommentSetID]) REFERENCES [Projection].[tDataCommentSet] ([DataCommentSetID])
ALTER TABLE [Projection].[tCQIResultFabricFilterPerformance] ADD CONSTRAINT [FK_tCQIResultFabricFilterPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [CQIIPM].[tCQIResultIPMCyclonePerformance] ADD CONSTRAINT [FK_tCQIResultIPMCyclonePerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tFuelQualityCollectorThermoSourceMap] ADD CONSTRAINT [FK_FuelTrackertFuelQualityCollectorThermoSourceMap_FTFuelQualityCollectorID_FuelTrackertFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID])
ALTER TABLE [FuelTracker].[tFuelQualityCollectorThermoSourceMap] ADD CONSTRAINT [FK_FuelTrackertFuelQualityCollectorThermoSourceMap_FuelSourceID_FueltFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tScenarioRun] ADD CONSTRAINT [FK_tScenarioRun_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tRAMSystemFile] ADD CONSTRAINT [FK_tRAMSystemFile_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tCQIResultBoilerPerformance] ADD CONSTRAINT [FK_tCQIResultBoilerPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [CQIIPM].[tCQIResultIPMTurbinePerformance] ADD CONSTRAINT [FK_tCQIResultIPMTurbinePerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationSetRoleMap] ADD CONSTRAINT [FK_tGenerationSetRoleMap_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationSetRoleMap] ADD CONSTRAINT [FK_tGenerationSetRoleMap_SecurityRoleID_tSecurityRole] FOREIGN KEY ([SecurityRoleID]) REFERENCES [AccessControl].[tRole] ([SecurityRoleID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tFuelQualityOPMMap] ADD CONSTRAINT [FK_tFTFuelQualityOPMMap_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tUINavTreeNodeMap] ADD CONSTRAINT [FK_tUIFTNavTreeNodeMap_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [FuelTracker].[tUINavTreeNodeMap] ADD CONSTRAINT [FK_tUIFTNavTreeNodeMap_UIFTNavTreeID_tUIFTNavTree] FOREIGN KEY ([UIFTNavTreeID]) REFERENCES [FuelTracker].[tUINavTree] ([UIFTNavTreeID])
ALTER TABLE [FuelTracker].[tNodePathExternalSource] ADD CONSTRAINT [FK_tFTNodePathExternalSource_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
ALTER TABLE [FuelTracker].[tNodePathExternalSource] ADD CONSTRAINT [FK_tFTNodePathExternalSource_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [EnergyStorage].[tPerformanceTimeslice] ADD CONSTRAINT [FKey_tPerformanceSet_SetID] FOREIGN KEY ([SetID]) REFERENCES [EnergyStorage].[tPerformanceSet] ([ID])
ALTER TABLE [Projection].[tCQIResultMillPerformance] ADD CONSTRAINT [FK_tCQIResultMillPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssueAssetIssueImpactTypeMap] ADD CONSTRAINT [FK_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueAssetIssueImpactTypeMap_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssueAssetIssueImpactTypeMap] ADD CONSTRAINT [FK_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueImpactType] FOREIGN KEY ([AssetIssueImpactTypeID]) REFERENCES [Diagnostics].[tAssetIssueImpactType] ([AssetIssueImpactTypeID]) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssueImpactType] ADD CONSTRAINT [FK_tAssetIssueImpactType_tAssetIssueImpactCategoryType] FOREIGN KEY ([AssetIssueImpactCategoryTypeID]) REFERENCES [Diagnostics].[tAssetIssueImpactCategoryType] ([AssetIssueImpactCategoryTypeID]) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE [FuelTracker].[tFuelQualityCollectorStatusSchedule] ADD CONSTRAINT [FK_tFTFuelQualityCollectorStatusSchedule_FTFuelQualityCollectorID_tFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID])
ALTER TABLE [Projection].[tUITargetStationDetail] ADD CONSTRAINT [FK_tUITargetStationDetail_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID])
ALTER TABLE [Projection].[tUITargetStationDetail] ADD CONSTRAINT [FK_tUITargetStationDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID])
ALTER TABLE [Projection].[tUITargetStationDetail] ADD CONSTRAINT [FK_tUITargetStationDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID])
ALTER TABLE [Projection].[tUITargetStationDetail] ADD CONSTRAINT [FK_tUITargetStationDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
ALTER TABLE [Projection].[tUITargetUnitDetail] ADD CONSTRAINT [FK_tUITargetUnitDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID])
ALTER TABLE [Projection].[tUITargetUnitDetail] ADD CONSTRAINT [FK_tUITargetUnitDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID])
ALTER TABLE [Projection].[tUITargetUnitDetail] ADD CONSTRAINT [FK_tUITargetUnitDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
ALTER TABLE [Projection].[tUITargetUnitDetail] ADD CONSTRAINT [FK_tUITargetUnitDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [CQIIPM].[tCQIIPMServerFile] ADD CONSTRAINT [FK_tCQIIPMServerFile_PDServerID_tPDServer] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID])
ALTER TABLE [CQIIPM].[tCQIIPMServerFile] ADD CONSTRAINT [FK_tCQIIPMServerFile_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Actuals].[tFGDPerformance] ADD CONSTRAINT [FK_tActualFGDPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [CQIIPM].[tCQIIPMInterfaceTag] ADD CONSTRAINT [FK_tCQIIPMInterfaceTag_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [AccessControl].[tAlias] ADD CONSTRAINT [FK_AccessControl_tAlias_SecurityUserID_AccessControl_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tFuelQualityCollectorTagMap] ADD CONSTRAINT [FK_tFTFuelQualityCollectorTagMap_FTFuelQualityCollectorID_tFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID])
ALTER TABLE [FuelTracker].[tInTransitSource] ADD CONSTRAINT [FK_tFTInTransitSource_FTInTransitID_tFTInTransit] FOREIGN KEY ([FTInTransitID]) REFERENCES [FuelTracker].[tInTransit] ([FTInTransitID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tInTransitSource] ADD CONSTRAINT [FK_tFTInTransitSource_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndTwoBySeason] ADD CONSTRAINT [FK_tMaintenancePlanAddAndTwoBySeason_MaintenancePlanSetID_tMaintenancePlan] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID])
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndTwoBySeason] ADD CONSTRAINT [FK_tMaintenancePlanAddAndTwoBySeason_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndTwoBySeason] ADD CONSTRAINT [FK_tRAMModelComponent_tMaintenancePlanAddAndTwoBySeason_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
ALTER TABLE [Projection].[tScoreboardThresholds] ADD CONSTRAINT [FK_tUIScoreBoard_PlanTypeID_tPlanType] FOREIGN KEY ([PlanTypeID]) REFERENCES [Projection].[tPlanType] ([PlanTypeID])
ALTER TABLE [Projection].[tScoreboardThresholds] ADD CONSTRAINT [FK_tUIScoreBoard_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID])
ALTER TABLE [Projection].[tScoreboardThresholds] ADD CONSTRAINT [FK_tUIScoreBoard_UITimeFrameID_tUITimeFrame] FOREIGN KEY ([UITimeFrameID]) REFERENCES [Projection].[tUITimeFrame] ([UITimeFrameID])
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ADD CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tFuelPlanAdjustment] FOREIGN KEY ([FuelPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ADD CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentDetail] ADD CONSTRAINT [FK_tFuelPlanAdjustmentDetail_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssueRelatedAssetIssue] ADD CONSTRAINT [FK_Diagnostics_tAssetIssueRelatedAssetIssue_AssetIssueID_Diagnostics_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssueRelatedAssetIssue] ADD CONSTRAINT [FK_Diagnostics_tAssetIssueRelatedAssetIssue_RelatedAssetIssueID_Diagnostics_tAssetIssue] FOREIGN KEY ([RelatedAssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID])
ALTER TABLE [CQIIPM].[tCQIResultIPMAirHeaterPerformance] ADD CONSTRAINT [FK_tCQIResultIPMAirHeaterPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentSet] ADD CONSTRAINT [FK_tFuelPlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentSet] ADD CONSTRAINT [FK_tFuelPlanAdjustmentSet_tFuelPlanSet] FOREIGN KEY ([BaseFuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tCalculationDataFilter] ADD CONSTRAINT [FK_Monitoring_tCalculationDataFilter_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationDetail_Adjusted] ADD CONSTRAINT [FK_tGenerationDetail_Adjusted_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationDetail_Adjusted] ADD CONSTRAINT [FK_tGenerationDetail_Adjusted_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tRightAngleNodeMap] ADD CONSTRAINT [FK_tFTRightAngleNodeMap_FTNodeID_LocaleID] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [Monitoring].[tModelSnapshot] ADD CONSTRAINT [FK_Monitoring_tModelSnapshot_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE
ALTER TABLE [ProcessData].[tServerLogin] ADD CONSTRAINT [FK_tServerLogin_ServerID_tServer] FOREIGN KEY ([ServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]) ON DELETE CASCADE
ALTER TABLE [Actuals].[tSCRPerformance] ADD CONSTRAINT [FK_tActualSCRPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [CQIIPM].[tCQIResultIPMMillPerformance] ADD CONSTRAINT [FK_tCQIResultIPMMillPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tCQIResultESPPerformance] ADD CONSTRAINT [FK_tCQIResultESPPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tFilterRuleAssociation] ADD CONSTRAINT [FK_Monitoring_tFilterRuleAssociation_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tFilterRuleAssociation] ADD CONSTRAINT [FK_Monitoring_tFilterRuleAssociation_FilterRuleID_tFilterRule] FOREIGN KEY ([FilterRuleID]) REFERENCES [Monitoring].[tFilterRule] ([FilterRuleID]) ON DELETE CASCADE
ALTER TABLE [CQIIPM].[tCQIResultIPMBoilerSlicePerformance] ADD CONSTRAINT [FK_tCQIResultIPMBoilerSlicePerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE

--ALTER TABLE [Base].[tDataCacheKeyValue] ADD CONSTRAINT [FK_tDataCacheKeyValue_DataCacheID_tDataCache] FOREIGN KEY ([DataCacheID]) REFERENCES [Base].[tDataCache] ([DataCacheID]) ON DELETE CASCADE ON UPDATE CASCADE

ALTER TABLE [Asset].[tExternalSystem] ADD CONSTRAINT [FK_Asset_tExternalSystem_ExternalSystemTypeID_Asset_tExternalSystemType] FOREIGN KEY ([ExternalSystemTypeID]) REFERENCES [Asset].[tExternalSystemType] ([ExternalSystemTypeID])
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputFiles] ADD CONSTRAINT [FK_ttUnitRAMEvaluationOutputFiles_UnitRAMEvaluationPeriodKeyID_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tUINavTree] ADD CONSTRAINT [FK_tUIFTNavTree_UIFTNavTreeSetID_tUIFTNavTreeSet] FOREIGN KEY ([UIFTNavTreeSetID]) REFERENCES [FuelTracker].[tUINavTreeSet] ([UIFTNavTreeSetID])
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputResultsEAFDistribution] ADD CONSTRAINT [FK_tUnitRAMEvaluationOutputResultsEAFDistribution_PowerRAM_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tUITargetStationGroupDetail] ADD CONSTRAINT [FK_tUITargetStationGroupDetail_StationGroupID_tStationGroup] FOREIGN KEY ([StationGroupID]) REFERENCES [Asset].[tStationGroup] ([StationGroupID])
ALTER TABLE [Projection].[tUITargetStationGroupDetail] ADD CONSTRAINT [FK_tUITargetStationGroupDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID])
ALTER TABLE [Projection].[tUITargetStationGroupDetail] ADD CONSTRAINT [FK_tUITargetStationGroupDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID])
ALTER TABLE [Projection].[tUITargetStationGroupDetail] ADD CONSTRAINT [FK_tUITargetStationGroupDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
ALTER TABLE [Projection].[tUIThresholdUnitDetail] ADD CONSTRAINT [FK_tUIThresholdUnitDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID])
ALTER TABLE [Projection].[tUIThresholdUnitDetail] ADD CONSTRAINT [FK_tUIThresholdUnitDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID])
ALTER TABLE [Projection].[tUIThresholdUnitDetail] ADD CONSTRAINT [FK_tUIThresholdUnitDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
ALTER TABLE [Projection].[tUIThresholdUnitDetail] ADD CONSTRAINT [FK_tUIThresholdUnitDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [CQIIPM].[tCQIResultIPMFanPerformance] ADD CONSTRAINT [FK_tCQIResultIPMFanPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Fuel].[tSolidFuelQualityStatus] ADD CONSTRAINT [FK_tSolidFuelQualityStatus_FuelQualityID_tSolidFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
ALTER TABLE [ProcessData].[tTrendAxis] ADD CONSTRAINT [FK_tPDTrendAxis_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])
ALTER TABLE [FuelTracker].[tNodeContentSourceDefault] ADD CONSTRAINT [FK_tFTNodeContentSourceDefault_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [FuelTracker].[tNodeContentSourceDefault] ADD CONSTRAINT [FK_tFTNodeContentSourceDefault_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [FuelTracker].[tNodePathExternalQuality] ADD CONSTRAINT [FK_tFTNodePathExternalQuality_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
ALTER TABLE [FuelTracker].[tNodePathExternalQuality] ADD CONSTRAINT [FK_tFTNodePathExternalQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [PowerRAM].[tRAMModelComponent] ADD CONSTRAINT [FK_tRAMModelComponent_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
ALTER TABLE [Projection].[tCQIResultUnitPerformance] ADD CONSTRAINT [FK_tCQIResultUnitPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tTransactionMovementMap] ADD CONSTRAINT [FK_tFTTransactionMovementMap_FTMovementID_tFTMovement] FOREIGN KEY ([FTMovementID]) REFERENCES [FuelTracker].[tMovement] ([FTMovementID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tTransactionMovementMap] ADD CONSTRAINT [FK_tFTTransactionMovementMap_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE
ALTER TABLE [Actuals].[tFuelQuality] ADD CONSTRAINT [FK_tActualFuelQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Actuals].[tFuelQuality] ADD CONSTRAINT [FK_tActualFuelQuality_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Actuals].[tFuelQuality] ADD CONSTRAINT [FK_tActualFuelQuality_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] ADD CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] ADD CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_IssueCauseTypeID_tIssueCauseType] FOREIGN KEY ([IssueCauseTypeID]) REFERENCES [Diagnostics].[tIssueCauseType] ([IssueCauseTypeID])
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] ADD CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] ADD CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] ADD CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_VariableTypeID_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [Actuals].[tDailyEmissionResult] ADD CONSTRAINT [FK_tActualDailyEmissionResult_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_IssueTypeID_tIssueType] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID])
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tCQIResultStatusDetail] ADD CONSTRAINT [FK_tCQIResultStatusDetail_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Fuel].[tFuelBasinBlendDefinition] ADD CONSTRAINT [FK_tFuelBasinBlendDefinition_FuelBasinID_tFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
ALTER TABLE [Fuel].[tFuelBasinBlendDefinition] ADD CONSTRAINT [FK_tFuelBasinBlendDefinition_ParentBasin1ID_tFuelBasin] FOREIGN KEY ([ParentBasin1ID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
ALTER TABLE [Fuel].[tFuelBasinBlendDefinition] ADD CONSTRAINT [FK_tFuelBasinBlendDefinition_ParentBasin2ID_tFuelBasin] FOREIGN KEY ([ParentBasin2ID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentSet] ADD CONSTRAINT [FK_tMaintenancePlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentSet] ADD CONSTRAINT [FK_tMaintenancePlanAdjustmentSet_tMaintenancePlanSet] FOREIGN KEY ([BaseMaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE
ALTER TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] ADD CONSTRAINT [FK_tVariableTypeIssueTypeMap_IssueTypeID_tIssueType] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID])
ALTER TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] ADD CONSTRAINT [FK_tVariableTypeIssueTypeMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [CurrentPerformance].[tVariableTypeIssueTypeMap] ADD CONSTRAINT [FK_tVariableTypeIssueTypeMap_VariableTypeID_tVariableTypeID] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [Asset].[tAssetAttribute] ADD CONSTRAINT [FK_tAssetAttribute_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Asset].[tAssetAttribute] ADD CONSTRAINT [FK_tAssetAttribute_AttributeOptionTypeID_tAttributeOptionType] FOREIGN KEY ([AttributeOptionTypeID]) REFERENCES [Asset].[tAttributeOptionType] ([AttributeOptionTypeID])
ALTER TABLE [Asset].[tAssetAttribute] ADD CONSTRAINT [FK_tAssetAttribute_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID])
ALTER TABLE [Asset].[tAttributeOptionType] ADD CONSTRAINT [FK_tAttributeOptionType_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID])
ALTER TABLE [ProcessData].[tTrendSeries] ADD CONSTRAINT [FK_tPDTrendSeries_PDTagID_tPDTag] FOREIGN KEY ([PDTagID]) REFERENCES [ProcessData].[tTag] ([PDTagID])
ALTER TABLE [ProcessData].[tTrendSeries] ADD CONSTRAINT [FK_tPDTrendSeries_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])
ALTER TABLE [ProcessData].[tTrendSeries] ADD CONSTRAINT [FK_tPDTrendSeries_PDVariableID_tPDVariable] FOREIGN KEY ([PDVariableID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [ProcessData].[tTrendSeries] ADD CONSTRAINT [FK_tTrendSeries_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [Projection].[tDataVersion] ADD CONSTRAINT [FK_tDataVersion_PlanTypeID_tPlanType] FOREIGN KEY ([PlanTypeID]) REFERENCES [Projection].[tPlanType] ([PlanTypeID])
ALTER TABLE [FuelTracker].[tFuelQualityCollectorNodePathMap] ADD CONSTRAINT [FK_tFTFuelQualityCollectorNodePathMap_FTFuelQualityCollectorID_tFTFuelQualityCollector] FOREIGN KEY ([FTFuelQualityCollectorID]) REFERENCES [FuelTracker].[tFuelQualityCollector] ([FTFuelQualityCollectorID])
ALTER TABLE [FuelTracker].[tFuelQualityCollectorNodePathMap] ADD CONSTRAINT [FK_tFTFuelQualityCollectorNodePathMap_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
ALTER TABLE [CQIExperience].[tCQIExperienceControlVariable] ADD CONSTRAINT [FK_tCQIExperienceControlVariable_ControlCQIExperienceID_tCQIExperience] FOREIGN KEY ([ControlCQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID])
ALTER TABLE [CQIExperience].[tCQIExperienceControlVariable] ADD CONSTRAINT [FK_tCQIExperienceControlVariable_ManipulatedCQIExperienceID_tCQIExperience] FOREIGN KEY ([ManipulatedCQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID])
ALTER TABLE [FuelTracker].[tSiloModelTagMap] ADD CONSTRAINT [FK_tSiloModelTagMap_SiloModelID_tSiloModel] FOREIGN KEY ([SiloModelID]) REFERENCES [FuelTracker].[tSiloModel] ([SiloModelID])
ALTER TABLE [AnalytX].[tAssetIDExternalIDItemMap] ADD CONSTRAINT [FK_AnalytX_tAXAssetIDExternalIDAXCallMap_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [PowerRAM].[tMaintenanceOutageDetail] ADD CONSTRAINT [FK_tMaintenanceOutageDetail_tMaintenanceOutage] FOREIGN KEY ([MaintenanceOutageID]) REFERENCES [PowerRAM].[tMaintenanceOutage] ([MaintenanceOutageID])
ALTER TABLE [PowerRAM].[tMaintenanceOutageDetail] ADD CONSTRAINT [FK_tMaintenanceOutageDetail_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tMaintenanceOutageDetail] ADD CONSTRAINT [FK_tMaintenanceOutageDetail_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModelConditionDefault] ADD CONSTRAINT [FK_Monitoring_tModelConditionDefault_ModelDefaultID_tModelDefault] FOREIGN KEY ([ModelDefaultID]) REFERENCES [Monitoring].[tModelDefault] ([ModelDefaultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationDetail] ADD CONSTRAINT [FK_tGenerationDetail_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationDetail] ADD CONSTRAINT [FK_tGenerationDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Monitoring].[tLoadBucket] ADD CONSTRAINT [FK_Monitoring_tLoadBucket_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Fuel].[tFuelSourceGroupMap] ADD CONSTRAINT [FK_tFuelSourceGroupMap_FuelSourceGroupID_tFuelSourceGroup] FOREIGN KEY ([FuelSourceGroupID]) REFERENCES [Fuel].[tFuelSourceGroup] ([FuelSourceGroupID])
ALTER TABLE [Fuel].[tFuelSourceGroupMap] ADD CONSTRAINT [FK_tFuelSourceGroupMap_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Monitoring].[tEvent] ADD CONSTRAINT [FK_tEvent_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Monitoring].[tEvent] ADD CONSTRAINT [FK_tEvent_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Monitoring].[tEventType] ([EventTypeID])
ALTER TABLE [Projection].[tGenerationAdjustmentSet] ADD CONSTRAINT [FK_tGenerationAdjustmentSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [Diagnostics].[tIssueCauseType] ADD CONSTRAINT [FK_tIssueCauseType_IssueTypeID_tIssueType] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID])
ALTER TABLE [Projection].[tScenarioResultMetrics] ADD CONSTRAINT [FK_tScenarioResultMetrics_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioResultMetrics] ADD CONSTRAINT [FK_tScenarioResultMetrics_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] ADD CONSTRAINT [FK_tMaintenancePlanAddAndOneBySeason_MaintenancePlanSetID_tMaintenancePlan] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID])
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] ADD CONSTRAINT [FK_tMaintenancePlanAddAndOneBySeason_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [PowerRAM].[tMaintenancePlanAddAndOneBySeason] ADD CONSTRAINT [FK_tRAMModelComponent_tMaintenancePlanAddAndOneBySeason_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
ALTER TABLE [PowerRAM].[tRAMModel] ADD CONSTRAINT [FK_tRAMModel_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Fuel].[tAdditiveTypeChemicalMap] ADD CONSTRAINT [FK_Fuel_tAdditiveTypeChemicalMap_AdditiveChemicalTypeID_Fuel_tAdditiveChemicalType] FOREIGN KEY ([AdditiveChemicalTypeID]) REFERENCES [Fuel].[tAdditiveChemicalType] ([AdditiveChemicalTypeID]) ON DELETE CASCADE
ALTER TABLE [Fuel].[tAdditiveTypeChemicalMap] ADD CONSTRAINT [FK_Fuel_tAdditiveTypeChemicalMap_AdditiveTypeID_Fuel_tAdditiveType] FOREIGN KEY ([AdditiveTypeID]) REFERENCES [Fuel].[tAdditiveType] ([AdditiveTypeID]) ON DELETE CASCADE
ALTER TABLE [ProcessData].[tValueType] ADD CONSTRAINT [FK_tVariableType_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationSystemFile] ADD CONSTRAINT [FK_tUnitRAMEvaluationSystemFile_UnitRAMEvaluationPeriodKeyID_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetClassTypeAttributeTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeAttributeTypeMap_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetClassTypeAttributeTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeAttributeTypeMap_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID])
ALTER TABLE [Fuel].[tAdditive] ADD CONSTRAINT [FK_Fuel_tAdditive_AdditiveTypeID_Fuel_tAdditiveType] FOREIGN KEY ([AdditiveTypeID]) REFERENCES [Fuel].[tAdditiveType] ([AdditiveTypeID])
ALTER TABLE [Fuel].[tAdditive] ADD CONSTRAINT [FK_Fuel_tAdditive_FuelQualityID_Fuel_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tVistaModelConfidence] ADD CONSTRAINT [FK_tVistaModelConfidence_ConfidenceTypeID_tConfidenceType] FOREIGN KEY ([ConfidenceTypeID]) REFERENCES [Projection].[tConfidenceType] ([ConfidenceTypeID])
ALTER TABLE [Projection].[tVistaModelConfidence] ADD CONSTRAINT [FK_tVistaModelConfidence_VistaModelID_tVistaModel] FOREIGN KEY ([VistaModelID]) REFERENCES [Projection].[tVistaModel] ([VistaModelID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tFuelChangeEvent] ADD CONSTRAINT [FK_tFuelChangeEvent_FuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tFuelChangeEvent] ADD CONSTRAINT [FK_tFuelChangeEvent_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Actuals].[tCoalYardPerformance] ADD CONSTRAINT [FK_tActualCoalYardPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tSiloModelNodePathMap] ADD CONSTRAINT [FK_tSiloModelNodePathMap_NodePathID_tNodePath] FOREIGN KEY ([NodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
ALTER TABLE [FuelTracker].[tSiloModelNodePathMap] ADD CONSTRAINT [FK_tSiloModelNodePathMap_SiloModelID_tSiloModel] FOREIGN KEY ([SiloModelID]) REFERENCES [FuelTracker].[tSiloModel] ([SiloModelID])
ALTER TABLE [FuelTracker].[tSiloModel] ADD CONSTRAINT [FK_tSiloModel_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [AnalytX].[tAssetAppContextDetail] ADD CONSTRAINT [FK_AnalytX_tAssetAppContextDetail_AssetAppContextSetID_AnalytX_tAssetAppContextSet] FOREIGN KEY ([AssetAppContextSetID]) REFERENCES [AnalytX].[tAssetAppContextSet] ([AssetAppContextSetID])
ALTER TABLE [AnalytX].[tAssetAppContextDetail] ADD CONSTRAINT [FK_AnalytX_tAssetAppContextDetail_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Projection].[tGenerationAdjustmentDetail] ADD CONSTRAINT [FK_tGenerationAdjustmentDetail_GenerationAdjustmentSetID_tGenerationAdjustmentSet] FOREIGN KEY ([GenerationAdjustmentSetID]) REFERENCES [Projection].[tGenerationAdjustmentSet] ([GenerationAdjustmentSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationAdjustmentDetail] ADD CONSTRAINT [FK_tGenerationAdjustmentDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [AccessControl].[tAssetAccessRule] ADD CONSTRAINT [FK_AccessControl_tAssetAccessRule_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tNodeContent] ADD CONSTRAINT [FK_tFTNodeContent_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [FuelTracker].[tNodeContent] ADD CONSTRAINT [FK_tFTNodeContent_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE
ALTER TABLE [Actuals].[tFanPerformance] ADD CONSTRAINT [FK_tActualFanPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tInTransit] ADD CONSTRAINT [FK_tFTInTransit_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
ALTER TABLE [Actuals].[tDefaultFuelQuality] ADD CONSTRAINT [FK_tDefaultFuelQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Actuals].[tDefaultFuelQuality] ADD CONSTRAINT [FK_tDefaultFuelQuality_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Actuals].[tDefaultFuelQuality] ADD CONSTRAINT [FK_tDefaultFuelQuality_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tMovement] ADD CONSTRAINT [FK_tFTMovement_FTInTransitID] FOREIGN KEY ([FTInTransitID]) REFERENCES [FuelTracker].[tInTransit] ([FTInTransitID])
ALTER TABLE [FuelTracker].[tMovement] ADD CONSTRAINT [FK_tFTMovement_FTNodePathID_tFTNodePath] FOREIGN KEY ([FTNodePathID]) REFERENCES [FuelTracker].[tNodePath] ([FTNodePathID])
ALTER TABLE [FuelTracker].[tNodePath] ADD CONSTRAINT [FK_tFTNodePath_FromFTNodeID_tFTNode] FOREIGN KEY ([FromFTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [FuelTracker].[tNodePath] ADD CONSTRAINT [FK_tFTNodePath_ToFTNodeID_tFTNode] FOREIGN KEY ([ToFTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [Projection].[tScenarioUnitPerformance] ADD CONSTRAINT [FK_tScenarioUnitPerformance_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tScenarioUnitPerformance] ADD CONSTRAINT [FK_tScenarioUnitPerformance_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tScenarioUnitPerformance] ADD CONSTRAINT [FK_tScenarioUnitPerformance_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioUnitPerformance] ADD CONSTRAINT [FK_tScenarioUnitPerformance_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Projection].[tScenarioUnitPerformance] ADD CONSTRAINT [FK_tScenarioUnitPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Asset].[tAssetTreeNode] ADD CONSTRAINT [FK_tAssetTreeNode_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetTreeNode] ADD CONSTRAINT [FK_tAssetTreeNode_tAsset1] FOREIGN KEY ([ParentAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Asset].[tAssetTreeNode] ADD CONSTRAINT [FK_tAssetTreeNode_tAssetTree] FOREIGN KEY ([AssetTreeID]) REFERENCES [Asset].[tAssetTree] ([AssetTreeID])
ALTER TABLE [Asset].[tAssetTreeNode] ADD CONSTRAINT [FK_tAssetTreeNode_tAssetTreeNodeChildConfigurationType] FOREIGN KEY ([AssetTreeNodeChildConfigurationTypeID]) REFERENCES [Asset].[tAssetTreeNodeChildConfigurationType] ([AssetTreeNodeChildConfigurationTypeID])
ALTER TABLE [Actuals].[tAirHeaterPerformance] ADD CONSTRAINT [FK_tActualAirHeaterPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ADD CONSTRAINT [FK_tFuelPlanAdjustmentSetUnitMap_tFuelPlanAdjustment] FOREIGN KEY ([FuelPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustment] ([FuelPlanAdjustmentID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustmentUnitMap] ADD CONSTRAINT [FK_tFuelPlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tFuelPlanAdjustment] ADD CONSTRAINT [FK_tFuelPlanAdjustment_tFuelPlanAdjustmentSe] FOREIGN KEY ([FuelPlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tFuelPlanAdjustmentSet] ([FuelPlanAdjustmentSetID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetClassTypeAssetClassTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeAssetClassTypeMap_tAssetClassType] FOREIGN KEY ([ParentAssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetClassTypeAssetClassTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeAssetClassTypeMap_tAssetClassType1] FOREIGN KEY ([ChildAssetClassTypeId]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID])
ALTER TABLE [PowerRAM].[tMaintenanceOutage] ADD CONSTRAINT [FK_tMaintenanceOutage_MaintenancePlanSetID_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tMaintenanceOutage] ADD CONSTRAINT [FK_tMaintenanceOutage_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tTransactionQuality] ADD CONSTRAINT [FK_tFTTransactionQuality_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tTransactionQuality] ADD CONSTRAINT [FK_tFTTransactionQuality_FTTransactionSourceID_tFTTransactionSource] FOREIGN KEY ([FTTransactionSourceID]) REFERENCES [FuelTracker].[tTransactionSource] ([FTTransactionSourceID])
ALTER TABLE [FuelTracker].[tTransactionQuality] ADD CONSTRAINT [FK_tFTTransactionQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [FuelTracker].[tTransaction] ADD CONSTRAINT [FK_tFTTransaction_FTNodeID_tFTNode] FOREIGN KEY ([FTNodeID]) REFERENCES [FuelTracker].[tNode] ([FTNodeID])
ALTER TABLE [FuelTracker].[tTransaction] ADD CONSTRAINT [FK_tFTTransaction_FTTransactionSummaryID_tFTTransactionSummary] FOREIGN KEY ([FTTransactionSummaryID]) REFERENCES [FuelTracker].[tTransactionSummary] ([FTTransactionSummaryID])
ALTER TABLE [CurrentPerformance].[tSectionAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tSectionAssetVariableTypeTagMap_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])
ALTER TABLE [CurrentPerformance].[tSectionAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tSectionAssetVariableTypeTagMap_SectionAssetID_AssetID_tAsset] FOREIGN KEY ([SectionAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Administration].[tUserEditDetail] ADD CONSTRAINT [FK_tUserEditDetail_UserEditTableID_tUserEditTable] FOREIGN KEY ([UserEditTableID]) REFERENCES [Administration].[tUserEditTable] ([UserEditTableID])
ALTER TABLE [Monitoring].[tEventTypeAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tEventTypeAssetVariableTypeTagMap_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])
ALTER TABLE [Monitoring].[tEventTypeAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tEventTypeAssetVariableTypeTagMap_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Monitoring].[tEventType] ([EventTypeID])
ALTER TABLE [Diagnostics].[tAssetIssue] ADD CONSTRAINT [FK_Asset_tAsset_IssueTypeID_Diagnostircs_tAssetIssue] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssue] ADD CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_AssetIssueCategoryTypeID_Diagnostircs_tAssetIssue] FOREIGN KEY ([AssetIssueCategoryTypeID]) REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssue] ADD CONSTRAINT [FK_Diagnostics_tIssueCauseType_IssueCauseTypeID_Diagnostircs_tAssetIssue] FOREIGN KEY ([IssueCauseTypeID]) REFERENCES [Diagnostics].[tIssueCauseType] ([IssueCauseTypeID]) ON DELETE CASCADE
ALTER TABLE [Diagnostics].[tAssetIssue] ADD CONSTRAINT [FK_Diagnostics_tIssueType_IssueTypeID_Diagnostics_tAssetIssue] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetClassType] ADD CONSTRAINT [FK_tAssetClassType_tAssetType] FOREIGN KEY ([AssetTypeID]) REFERENCES [Asset].[tAssetType] ([AssetTypeID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationAdjustmentMap] ADD CONSTRAINT [FK_tGenerationAdjustmentMap_GenerationAdjustmentSetID_tGenerationAdjustmentSet] FOREIGN KEY ([GenerationAdjustmentSetID]) REFERENCES [Projection].[tGenerationAdjustmentSet] ([GenerationAdjustmentSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationAdjustmentMap] ADD CONSTRAINT [FK_tGenerationAdjustmentMap_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Actuals].[tAssetBlendedFuelQualityStatus] ADD CONSTRAINT [FK_tAssetBlendedFuelQualityStatus_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Monitoring].[tModelConditionNote] ADD CONSTRAINT [FK_ModelConditionNote_ModelConditionID_ModelConditionID] FOREIGN KEY ([ModelConditionID]) REFERENCES [Monitoring].[tModelCondition] ([ModelConditionID])
ALTER TABLE [Monitoring].[tModelConditionNote] ADD CONSTRAINT [FK_Monitoring_tModelConditionNode_ModelConditionID_tModelCondition] FOREIGN KEY ([ModelConditionID]) REFERENCES [Monitoring].[tModelCondition] ([ModelConditionID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentSet] ADD CONSTRAINT [FK_tUnitPlanAdjustmentSet_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentSet] ADD CONSTRAINT [FK_tUnitPlanAdjustmentSet_tUnitConfigScheduleSet] FOREIGN KEY ([BaseUnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]) ON DELETE CASCADE
ALTER TABLE [CurrentPerformance].[tCPNavTree] ADD CONSTRAINT [FK_tPDNavTree_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])
ALTER TABLE [Asset].[tUnitSeasonMap] ADD CONSTRAINT [FK_tUnitSeasonMap_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Projection].[tFuelPlanDetail] ADD CONSTRAINT [FK_tFuelPlanDetail_FuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tFuelPlanDetail] ADD CONSTRAINT [FK_tFuelPlanDetail_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tFuelPlanDetail] ADD CONSTRAINT [FK_tFuelPlanDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tTransactionSource] ADD CONSTRAINT [FK_tFTTransactionSource_FTTransactionID_tFTTransaction] FOREIGN KEY ([FTTransactionID]) REFERENCES [FuelTracker].[tTransaction] ([FTTransactionID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tTransactionSource] ADD CONSTRAINT [FK_tFTTransactionSource_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tGenerationProductionCostDetail] ADD CONSTRAINT [FK_tGenerationProductionCostDetail_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationProductionCostDetail] ADD CONSTRAINT [FK_tGenerationProductionCostDetail_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Monitoring].[tModelTrendMap] ADD CONSTRAINT [FK_Monitoring_tModelTrendMap_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModelTrendMap] ADD CONSTRAINT [FK_Monitoring_tModelTrendMap_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
ALTER TABLE [Actuals].[tFinalMonthlyEmission] ADD CONSTRAINT [FK_tFinalMonthlyEmission_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Projection].[tCQIResultConfidence] ADD CONSTRAINT [FK_tCQIResultConfidence_ConfidenceTypeID_tConfidenceType] FOREIGN KEY ([ConfidenceTypeID]) REFERENCES [Projection].[tConfidenceType] ([ConfidenceTypeID])
ALTER TABLE [Projection].[tCQIResultConfidence] ADD CONSTRAINT [FK_tCQIResultConfidence_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioFuelQuality] ADD CONSTRAINT [FK_tScenarioFuelQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tScenarioFuelQuality] ADD CONSTRAINT [FK_tScenarioFuelQuality_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tScenarioFuelQuality] ADD CONSTRAINT [FK_tScenarioFuelQuality_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioFuelQuality] ADD CONSTRAINT [FK_tScenarioFuelQuality_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Projection].[tScenarioFuelQuality] ADD CONSTRAINT [FK_tScenarioFuelQuality_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [CQIExperience].[tCQIResultExperience] ADD CONSTRAINT [FK_tCQIResultExperience_CQIExperienceID_tCQIExperience] FOREIGN KEY ([CQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID]) ON DELETE CASCADE
ALTER TABLE [CQIExperience].[tCQIResultExperience] ADD CONSTRAINT [FK_tCQIResultExperience_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationDailyBucket] ADD CONSTRAINT [FK_tGenerationDailyBucket_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationDailyBucket] ADD CONSTRAINT [FK_tGenerationDailyBucket_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [CQIIPM].[tCQIResultIPMBoilerPerformance] ADD CONSTRAINT [FK_tCQIResultIPMBoilerPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tFuelPlanSet] ADD CONSTRAINT [FK_tFuelPlanSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [Projection].[tFuelPlanSet] ADD CONSTRAINT [FK_tFuelPlanSet_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
ALTER TABLE [Actuals].[tOfficialUnitPerformance] ADD CONSTRAINT [FK_tActualOfficialUnitPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Monitoring].[tModelCondition] ADD CONSTRAINT [FK_Monitoring_tModelCondition_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tAnalysisSetScenarioMap] ADD CONSTRAINT [FK_tAnalysisSetScenarioMap_tAnalysisSet] FOREIGN KEY ([AnalysisSetID]) REFERENCES [AdaptivePlanning].[tAnalysisSet] ([AnalysisSetID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tAnalysisSetScenarioMap] ADD CONSTRAINT [FK_tAnalysisSetScenarioMap_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tAnalysisSet] ADD CONSTRAINT [FK_tAnalysisSet_tScenario] FOREIGN KEY ([BaseScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID])
ALTER TABLE [Asset].[tStationGroup] ADD CONSTRAINT [FK_tStationGroup_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Actuals].[tMillPerformance] ADD CONSTRAINT [FK_tActualMillPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelPro].[tEvaluationFuelSourceMap] ADD CONSTRAINT [FK_tEvalOfferMap_EvaluationID_tEvaluation] FOREIGN KEY ([EvaluationID]) REFERENCES [FuelPro].[tEvaluation] ([EvaluationID]) ON DELETE CASCADE
ALTER TABLE [FuelPro].[tEvaluationFuelSourceMap] ADD CONSTRAINT [FK_tEvalOfferMap_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [FuelPro].[tEvaluationFuelSourceMap] ADD CONSTRAINT [FK_tEvalOfferMap_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID]) ON DELETE CASCADE
ALTER TABLE [CQIExperience].[tCQIExperienceTimestampData] ADD CONSTRAINT [FK_tCQIExperienceTimestampData_CQIExperienceID_tCQIExperience] FOREIGN KEY ([CQIExperienceID]) REFERENCES [CQIExperience].[tCQIExperience] ([CQIExperienceID])
ALTER TABLE [CQIExperience].[tCQIExperienceTimestampData] ADD CONSTRAINT [FK_tCQIExperienceTimestampData_CQIExperienceTimestampID_tCQIExperienceTimestamp] FOREIGN KEY ([CQIExperienceTimestampID]) REFERENCES [CQIExperience].[tCQIExperienceTimestamp] ([CQIExperienceTimestampID]) ON DELETE CASCADE
ALTER TABLE [CQIExperience].[tCQIExperience] ADD CONSTRAINT [FK_tCQIExperience_CQIExperienceTypeID_tCQIExperienceType] FOREIGN KEY ([CQIExperienceTypeID]) REFERENCES [CQIExperience].[tCQIExperienceType] ([CQIExperienceTypeID])
ALTER TABLE [CQIExperience].[tCQIExperience] ADD CONSTRAINT [FK_tCQIExperience_PDTagID_tPDTag] FOREIGN KEY ([PDTagID]) REFERENCES [ProcessData].[tTag] ([PDTagID])
ALTER TABLE [CQIExperience].[tCQIExperience] ADD CONSTRAINT [FK_tCQIExperience_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [EnergyStorage].[tGenerationDataTimeslice] ADD CONSTRAINT [FKey_tGenerationDataSets_SetID] FOREIGN KEY ([SetID]) REFERENCES [EnergyStorage].[tGenerationDataSet] ([SetID])
ALTER TABLE [Monitoring].[tModelDefault] ADD CONSTRAINT [FK_Monitoring_tModelDefault_AssetClassTypeVariableTypeMapID_tAssetClassTypeVariableTypeMapID] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModelDefault] ADD CONSTRAINT [FK_Monitoring_tModelDefault_ModelTypeID_tModelType] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tModelType] ([ModelTypeID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] ADD CONSTRAINT [FK_tUnitPlanAdjustmentDetail_ttUnitEquipmentStateType] FOREIGN KEY ([UnitEquipmentStateTypeID]) REFERENCES [Projection].[tUnitEquipmentStateType] ([UnitEquipmentStateTypeID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] ADD CONSTRAINT [FK_tUnitPlanAdjustmentDetail_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentDetail] ADD CONSTRAINT [FK_tUnitPlanAdjustmentDetail_tUnitPlanAdjustment] FOREIGN KEY ([UnitPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustment] ([UnitPlanAdjustmentID]) ON DELETE CASCADE
ALTER TABLE [CQIExperience].[tCQIExperienceTimestamp] ADD CONSTRAINT [FK_tCQIExperienceTimestamp_FuelBasinID_tFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
ALTER TABLE [CQIExperience].[tCQIExperienceTimestamp] ADD CONSTRAINT [FK_tCQIExperienceTimestamp_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Fuel].[tFuelBlendDefinition] ADD CONSTRAINT [FK_tFuelBlendDefinition_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Fuel].[tFuelBlendDefinition] ADD CONSTRAINT [FK_tFuelBlendDefinition_ParentSourceID_tFuelSource] FOREIGN KEY ([ParentSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tGenerationPattern] ADD CONSTRAINT [FK_tGenerationPattern_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationPattern] ADD CONSTRAINT [FK_tGenerationPattern_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Projection].[tCQIResultFanPerformance] ADD CONSTRAINT [FK_tCQIResultFanPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tCQIResultFanPerformance] ADD CONSTRAINT [FK_tCQIResultFanPerformance_FanTypeID_tFanType] FOREIGN KEY ([FanTypeID]) REFERENCES [Projection].[tFanType] ([FanTypeID])
ALTER TABLE [ProcessData].[tServer] ADD CONSTRAINT [FK_tPDServer_PDServerTypeID_tPDServerType] FOREIGN KEY ([PDServerTypeID]) REFERENCES [ProcessData].[tServerType] ([PDServerTypeID])
ALTER TABLE [Projection].[tAssetBlendedFuelQuality] ADD CONSTRAINT [FK_ProjectiontAssetBlendedFuelQuality_AssetID_AssettAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Projection].[tAssetBlendedFuelQuality] ADD CONSTRAINT [FK_ProjectiontAssetBlendedFuelQuality_FuelBasinID_FueltFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
ALTER TABLE [Projection].[tAssetBlendedFuelQuality] ADD CONSTRAINT [FK_ProjectiontAssetBlendedFuelQuality_UnitID_AssettUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Monitoring].[tUnit] ADD CONSTRAINT [FK_Monitoring_tUnit_PDServerID_tServer] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID])
ALTER TABLE [Monitoring].[tUnit] ADD CONSTRAINT [FK_Monitoring_tUnit_tAssetVariableTypeTagMapID_AssetVariableTypeTagMap] FOREIGN KEY ([LoadSensorID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])
ALTER TABLE [Monitoring].[tUnit] ADD CONSTRAINT [FK_Monitoring_tUnit_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Actuals].[tBoilerPerformance] ADD CONSTRAINT [FK_tActualBoilerPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Fuel].[tSolidFuelQualityThermoConstant] ADD CONSTRAINT [FK_tFuelQualityThermoConstant_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioResultStatusDetail] ADD CONSTRAINT [FK_tScenarioResultStatusDetail_ScenarioResultStatusID_tScenarioResultStatus] FOREIGN KEY ([ScenarioResultStatusID]) REFERENCES [Projection].[tScenarioResultStatus] ([ScenarioResultStatusID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioResultStatus] ADD CONSTRAINT [FK_tScenarioResultStatus_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tScenarioResultStatus] ADD CONSTRAINT [FK_tScenarioResultStatus_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tScenarioResultStatus] ADD CONSTRAINT [FK_tScenarioResultStatus_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioResultStatus] ADD CONSTRAINT [FK_tScenarioResultStatus_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Projection].[tScenarioResultStatus] ADD CONSTRAINT [FK_tScenarioResultStatus_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [EnergyStorage].[tPerformanceSet] ADD CONSTRAINT [FK_tPerformanceSet_tBattery] FOREIGN KEY ([BatteryID]) REFERENCES [EnergyStorage].[tBattery] ([BatteryID])
ALTER TABLE [EnergyStorage].[tPerformanceSet] ADD CONSTRAINT [FK_tPerformanceSet_tGenerationDataSet] FOREIGN KEY ([GenDataSetID]) REFERENCES [EnergyStorage].[tGenerationDataSet] ([SetID])
ALTER TABLE [Projection].[tCQIRun] ADD CONSTRAINT [FK_tCQIRun_CQIRunTypeID_tCQIRunType] FOREIGN KEY ([CQIRunTypeID]) REFERENCES [Projection].[tCQIRunType] ([CQIRunTypeID])
ALTER TABLE [Projection].[tCQIRun] ADD CONSTRAINT [FK_tCQIRun_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tCQIRun] ADD CONSTRAINT [FK_tCQIRun_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tCQIRun] ADD CONSTRAINT [FK_tCQIRun_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [CQIIPM].[tCQIResultIPMUnitPerformance] ADD CONSTRAINT [FK_tCQIResultIPMUnitPerformance_CQIResultID_tCQIResult] FOREIGN KEY ([CQIResultID]) REFERENCES [Projection].[tCQIResult] ([CQIResultID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_ForecastSetID_tForecastSet] FOREIGN KEY ([ForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_FuelBurnSetID_tFuelBurnSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_SensitivityTypeID_tSensitivityType] FOREIGN KEY ([SensitivityTypeID]) REFERENCES [Projection].[tSensitivityType] ([SensitivityTypeID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenario_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID])
ALTER TABLE [Projection].[tScenario] ADD CONSTRAINT [FK_tScenarioMaintenancePlanSetID_tMaintenancePlanSet] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID])
ALTER TABLE [PowerRAM].[tRAMEvaluation] ADD CONSTRAINT [FK_tRAMEvaluation_MaintenancePlanSetID_tMaintenancePlanSet] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tAssetAttributeSchedule] ADD CONSTRAINT [FK_tAssetAttributeSchedule_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Asset].[tAssetAttributeSchedule] ADD CONSTRAINT [FK_tAssetAttributeSchedule_AttributeTypeID_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID])
ALTER TABLE [Monitoring].[tModelInputDefault] ADD CONSTRAINT [FK_Monitoring_tModelInputDefault_AssetClassTypeVariableTypeMapID_tAssetClassTypeVariableTypeMap] FOREIGN KEY ([AssetClassTypeVariableTypeMapID]) REFERENCES [ProcessData].[tAssetClassTypeVariableTypeMap] ([AssetClassTypeVariableTypeMapID])
ALTER TABLE [Monitoring].[tModelInputDefault] ADD CONSTRAINT [FK_Monitoring_tModelInputDefault_ModelDefaultID_tModelDefault] FOREIGN KEY ([ModelDefaultID]) REFERENCES [Monitoring].[tModelDefault] ([ModelDefaultID]) ON DELETE CASCADE
ALTER TABLE [Economic].[tForecastDetail] ADD CONSTRAINT [FK_tForecastDetail_ForecastSetID_tForecastSet] FOREIGN KEY ([ForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID])
ALTER TABLE [Economic].[tFuelSourceStationForecastMap] ADD CONSTRAINT [FK_tSrcStaFcstMap_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Economic].[tFuelSourceStationForecastMap] ADD CONSTRAINT [FK_tSrcStaFcstMap_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID])
ALTER TABLE [ProcessData].[tTag] ADD CONSTRAINT [FK_tPDTag_PDServerID_tPDServer] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID])
ALTER TABLE [Base].[tEventQueue] ADD CONSTRAINT [FK_Base_tEventQueue_EventStatusTypeID_Base_tEventStatusType] FOREIGN KEY ([EventStatusTypeID]) REFERENCES [Base].[tEventStatusType] ([EventStatusTypeID])
ALTER TABLE [Base].[tEventQueue] ADD CONSTRAINT [FK_Base_tEventQueue_EventTypeID_Base_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Base].[tEventType] ([EventTypeID])
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] ADD CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_MaintenancePlanSetID_tMaintenancePlan] FOREIGN KEY ([MaintenancePlanSetID]) REFERENCES [PowerRAM].[tMaintenancePlanSet] ([MaintenancePlanSetID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] ADD CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID])
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] ADD CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [PowerRAM].[tMaintenancePlanTimeSliceValues] ADD CONSTRAINT [FK_tMaintenancePlanTimeSliceValues_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [PowerRAM].[tMaintenancePlanSet] ADD CONSTRAINT [FK_tMaintenancePlanSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [PowerRAM].[tMaintenancePlanSet] ADD CONSTRAINT [FK_tMaintenancePlanSet_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID])
ALTER TABLE [Projection].[tUnitAttributes] ADD CONSTRAINT [FK_Projection_tUnitAttributes_FuelSourceID_Fuel_FuelSource_FuelSourceID] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tUnitAttributes] ADD CONSTRAINT [FK_Projection_tUnitAttributes_UnitID_Asset_tUnit_UnitID] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
ALTER TABLE [Base].[tEventQueueNotification] ADD CONSTRAINT [FK_Base_tEventQueueNotification_EventStatusTypeID_Base_tEventStatusType] FOREIGN KEY ([EventStatusTypeID]) REFERENCES [Base].[tEventStatusType] ([EventStatusTypeID])
ALTER TABLE [Base].[tEventQueueNotification] ADD CONSTRAINT [FK_Base_tEventQueueNotification_EventTypeID_Base_tEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [Base].[tEventType] ([EventTypeID])
ALTER TABLE [Projection].[tCQIResult] ADD CONSTRAINT [FK_tCQIResult_CQIRunTypeID_tCQIRunType] FOREIGN KEY ([CQIRunTypeID]) REFERENCES [Projection].[tCQIRunType] ([CQIRunTypeID])
ALTER TABLE [Projection].[tCQIResult] ADD CONSTRAINT [FK_tCQIResult_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tCQIResult] ADD CONSTRAINT [FK_tCQIResult_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tCQIResult] ADD CONSTRAINT [FK_tCQIResult_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Fuel].[tFuelSource] ADD CONSTRAINT [FK_tFuelSource_FuelBasinID_tFuelBasin] FOREIGN KEY ([FuelBasinID]) REFERENCES [Fuel].[tFuelBasin] ([FuelBasinID])
ALTER TABLE [Fuel].[tFuelSource] ADD CONSTRAINT [FK_tFuelSource_FuelSupplierID_tFuelSupplier] FOREIGN KEY ([FuelSupplierID]) REFERENCES [Fuel].[tFuelSupplier] ([FuelSupplierID])
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentUnitMap] ADD CONSTRAINT [FK_tUnitPlanAdjustmentSetUnitMap_tUnitPlanAdjustment] FOREIGN KEY ([UnitPlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustment] ([UnitPlanAdjustmentID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustmentUnitMap] ADD CONSTRAINT [FK_tUnitPlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tUnitPlanAdjustment] ADD CONSTRAINT [FK_tUnitPlanAdjustment_tUnitPlanAdjustmentSe] FOREIGN KEY ([UnitPlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tUnitPlanAdjustmentSet] ([UnitPlanAdjustmentSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tUITargetClientDetail] ADD CONSTRAINT [FK_tUITargetClientDetail_UIItemID_tUIItem] FOREIGN KEY ([UIItemID]) REFERENCES [Projection].[tUIItem] ([UIItemID])
ALTER TABLE [Projection].[tUITargetClientDetail] ADD CONSTRAINT [FK_tUITargetClientDetail_UITargetSetID_tUITargetSet] FOREIGN KEY ([UITargetSetID]) REFERENCES [Projection].[tUITargetSet] ([UITargetSetID])
ALTER TABLE [Projection].[tUITargetClientDetail] ADD CONSTRAINT [FK_tUITargetClientDetail_UITargetTypeID_tUITargetType] FOREIGN KEY ([UITargetTypeID]) REFERENCES [Projection].[tUITargetType] ([UITargetTypeID])
ALTER TABLE [Projection].[tUITargetSet] ADD CONSTRAINT [FK_tUITargetSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [Actuals].[tUnitPerformance] ADD CONSTRAINT [FK_tActualUnitPerformance_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Actuals].[tBlendedFuelQuality] ADD CONSTRAINT [FK_tActualBlendedFuelQuality_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Monitoring].[tFilterRule] ADD CONSTRAINT [FK_Monitoring_tFilterRule_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])
ALTER TABLE [Monitoring].[tFilterRule] ADD CONSTRAINT [FK_Monitoring_tFilterRule_FilterTypeID_tFilterType] FOREIGN KEY ([FilterTypeID]) REFERENCES [Monitoring].[tFilterType] ([FilterTypeID])
ALTER TABLE [Monitoring].[tFilterRule] ADD CONSTRAINT [FK_Monitoring_tFilterRule_UnitAssetID_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModelInput] ADD CONSTRAINT [FK_Monitoring_tModelInput_AssetVariableTypeTagMapID_tAssetVariableTypeTagMap] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])
ALTER TABLE [Monitoring].[tModelInput] ADD CONSTRAINT [FK_Monitoring_tModelInput_ModelID_tModel] FOREIGN KEY ([ModelID]) REFERENCES [Monitoring].[tModel] ([ModelID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tUnitRAMEvaluation] ADD CONSTRAINT [FK_tUnitRAMEvaluation_RAMEvaluationID_tRAMEvaluation] FOREIGN KEY ([RAMEvaluationID]) REFERENCES [PowerRAM].[tRAMEvaluation] ([RAMEvaluationID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tUnitRAMEvaluation] ADD CONSTRAINT [FK_tUnitRAMEvaluation_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Projection].[tCQILoadBucket] ADD CONSTRAINT [FK_tCQILoadBucket_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Asset].[tUnit] ADD CONSTRAINT [FK_tUnit_StationID_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID])
ALTER TABLE [Asset].[tUnit] ADD CONSTRAINT [FK_tUnit_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Projection].[tGenerationRegulationSchedule] ADD CONSTRAINT [FK_tRegulationSchedule_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tGenerationRegulationSchedule] ADD CONSTRAINT [FK_tRegulationSchedule_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Projection].[tGenerationSet] ADD CONSTRAINT [FK_tGenerationSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [Projection].[tGenerationSet] ADD CONSTRAINT [FK_tGenerationSet_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
ALTER TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
ALTER TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_VariableTypeID_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] ADD CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_WeightingVariableTypeID_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [Actuals].[tAssetBlendedFuelQuality] ADD CONSTRAINT [FK_Actuals_AssetBlendedFuelQuality_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [CurrentPerformance].[tCPNavTreeVariableMap] ADD CONSTRAINT [FK_tCPNavTreeVariableMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [CurrentPerformance].[tCPNavTreeVariableMap] ADD CONSTRAINT [FK_tPDNavTreeVariableMap_PDNavTreeID_tPDNavTree] FOREIGN KEY ([PDNavTreeID]) REFERENCES [CurrentPerformance].[tCPNavTree] ([PDNavTreeID])
ALTER TABLE [CurrentPerformance].[tCPNavTreeVariableMap] ADD CONSTRAINT [FK_tPDNavTreeVariableMap_PDVariableID_tPDVariable] FOREIGN KEY ([PDVariableID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [ProcessData].[tVariableType] ADD CONSTRAINT [FK_tVariableType_FunctionTypeID_tFunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [ProcessData].[tFunctionType] ([FunctionTypeID])
ALTER TABLE [ProcessData].[tVariableType] ADD CONSTRAINT [FK_tVariableType_WeightingVariableTypeID_tVariableType] FOREIGN KEY ([WeightingVariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [ProcessData].[tVariableType] ADD CONSTRAINT [UK_tVariableType_DefaultValueTypeID_ValueTypeID] FOREIGN KEY ([DefaultValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [Projection].[tScenarioMargin] ADD CONSTRAINT [FK_tScenarioMargin_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tScenarioMargin] ADD CONSTRAINT [FK_tScenarioMargin_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tScenarioMargin] ADD CONSTRAINT [FK_tScenarioMargin_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioMargin] ADD CONSTRAINT [FK_tScenarioMargin_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Projection].[tScenarioMargin] ADD CONSTRAINT [FK_tScenarioMargin_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputResultsCapacityDistribution] ADD CONSTRAINT [FK_tUnitRAMEvaluationOutputResultsCapacityDistribution_PowerRAM_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModel] ADD CONSTRAINT [FK_Monitoring_tModel_AssetVariableTypeTagMapID_tAssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModel] ADD CONSTRAINT [FK_Monitoring_tModel_ModelTypeID_tModelType] FOREIGN KEY ([ModelTypeID]) REFERENCES [Monitoring].[tModelType] ([ModelTypeID]) ON DELETE CASCADE
ALTER TABLE [Monitoring].[tModel] ADD CONSTRAINT [FK_Monitoring_tModel_UnitAsset_tAsset] FOREIGN KEY ([UnitAssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tUnitConfig] ADD CONSTRAINT [FK_tUnitConfig_RAMModelID_tRAMModel] FOREIGN KEY ([RAMModelID]) REFERENCES [PowerRAM].[tRAMModel] ([RAMModelID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tUnitConfig] ADD CONSTRAINT [FK_tUnitConfig_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
ALTER TABLE [Projection].[tUnitConfig] ADD CONSTRAINT [FK_tUnitConfig_UnitEquipmentStateSetID_tUnitEquipmentStateSet] FOREIGN KEY ([UnitEquipmentStateSetID]) REFERENCES [Projection].[tUnitEquipmentStateSet] ([UnitEquipmentStateSetID])
ALTER TABLE [Projection].[tUnitConfig] ADD CONSTRAINT [FK_tUnitConfig_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [Projection].[tUnitConfig] ADD CONSTRAINT [FK_tUnitConfig_VistModelID_tVistaModel] FOREIGN KEY ([VistaModelID]) REFERENCES [Projection].[tVistaModel] ([VistaModelID])
ALTER TABLE [FuelPro].[tEvaluation] ADD CONSTRAINT [FK_tEvaluation_ForecastSetID_tForecastSet] FOREIGN KEY ([ForecastSetID]) REFERENCES [Economic].[tForecastSet] ([ForecastSetID])
ALTER TABLE [FuelPro].[tEvaluation] ADD CONSTRAINT [FK_tEvaluation_FuelPlanSetID_tFuelPlanSet] FOREIGN KEY ([FuelPlanSetID]) REFERENCES [Projection].[tFuelPlanSet] ([FuelPlanSetID])
ALTER TABLE [FuelPro].[tEvaluation] ADD CONSTRAINT [FK_tEvaluation_GenerationSetID_tGenerationSet] FOREIGN KEY ([GenerationSetID]) REFERENCES [Projection].[tGenerationSet] ([GenerationSetID])
ALTER TABLE [FuelPro].[tEvaluation] ADD CONSTRAINT [FK_tEvaluation_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID])
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationOutputSystemResults] ADD CONSTRAINT [FK_tUnitRAMEvaluationOutputSystemResults_PowerRAM_tUnitRAMEvaluationPeriodKey] FOREIGN KEY ([UnitRAMEvaluationPeriodKeyID]) REFERENCES [PowerRAM].[tUnitRAMEvaluationPeriodKey] ([UnitRAMEvaluationPeriodKeyID]) ON DELETE CASCADE
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationPeriodKey] ADD CONSTRAINT [FK_tUnitRAMEvaluationPeriodKey_UnitID_tAsset] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [PowerRAM].[tUnitRAMEvaluationPeriodKey] ADD CONSTRAINT [FK_tUnitRAMEvaluationPeriodKey_UnitRAMEvaluationID_ttUnitRAMEvaluation] FOREIGN KEY ([UnitRAMEvaluationID]) REFERENCES [PowerRAM].[tUnitRAMEvaluation] ([UnitRAMEvaluationID]) ON DELETE CASCADE
ALTER TABLE [Fuel].[tAdditiveChemical] ADD CONSTRAINT [FK_Fuel_tAdditiveChemical_AdditiveChemicalTypeID_Fuel_tAdditiveChemicalType] FOREIGN KEY ([AdditiveChemicalTypeID]) REFERENCES [Fuel].[tAdditiveChemicalType] ([AdditiveChemicalTypeID])
ALTER TABLE [Fuel].[tAdditiveChemical] ADD CONSTRAINT [FK_Fuel_tAdditiveChemical_FuelQualityID_Fuel_tAdditive] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tAdditive] ([FuelQualityID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tExternalSystemKeyAssetMap] ADD CONSTRAINT [FK_Asset_tExternalSystemKeyAssetMap_ExternalSystemID_Asset_tExternalSystem] FOREIGN KEY ([ExternalSystemID]) REFERENCES [Asset].[tExternalSystem] ([ExternalSystemID]) ON DELETE CASCADE
ALTER TABLE [Asset].[tExternalSystemKeyAssetMap] ADD CONSTRAINT [FK_Asset_tExternalSystemKeyAssetMap_ExternalSystemTypeID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Asset].[tAsset] ADD CONSTRAINT [FK_Asset_tAsset_ParentAssetID_Asset_tAsset_AssetID] FOREIGN KEY ([ParentAssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Asset].[tAsset] ADD CONSTRAINT [FK_Asset_tAsset_tAssetTreeNodeChildConfigurationTypeID_tAssetTreeNodeChildConfigurationType] FOREIGN KEY ([AssetTreeNodeChildConfigurationTypeID]) REFERENCES [Asset].[tAssetTreeNodeChildConfigurationType] ([AssetTreeNodeChildConfigurationTypeID])
ALTER TABLE [Asset].[tAsset] ADD CONSTRAINT [FK_tAsset_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE
ALTER TABLE [CurrentPerformance].[tCLEventLogItem] ADD CONSTRAINT [UK_CLEventLogItem_AssetVariableTypeTagMapID_AssetVariableTypeTagMapID] FOREIGN KEY ([AssetVariableTypeTagMapID]) REFERENCES [ProcessData].[tAssetVariableTypeTagMap] ([AssetVariableTypeTagMapID])
ALTER TABLE [CurrentPerformance].[tCLEventLogItem] ADD CONSTRAINT [UK_CLEventLogItem_CLEventLogItemStatusID_CLEventLogItemStatusID] FOREIGN KEY ([CLEventLogItemStatusID]) REFERENCES [CurrentPerformance].[tCLEventLogItemStatus] ([CLEventLogItemStatusID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_StatusValueTypeID_tValueType] FOREIGN KEY ([StatusValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_StatusVariableTypeID_tVariableType] FOREIGN KEY ([StatusVariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_tTag] FOREIGN KEY ([TagID]) REFERENCES [ProcessData].[tTag] ([PDTagID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_WeightingValueTypeID_tValueType] FOREIGN KEY ([WeightingValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
ALTER TABLE [ProcessData].[tAssetVariableTypeTagMap] ADD CONSTRAINT [FK_tAssetVariableTypeTagMap_WeightingVariableTypeID_tVariableType] FOREIGN KEY ([WeightingVariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
ALTER TABLE [FuelTracker].[tInTransitQuality] ADD CONSTRAINT [FK_tFTInTransitQuality_FTInTransitID_tFTInTransit] FOREIGN KEY ([FTInTransitID]) REFERENCES [FuelTracker].[tInTransit] ([FTInTransitID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tInTransitQuality] ADD CONSTRAINT [FK_tFTInTransitQuality_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Fuel].[tFuelQuality] ADD CONSTRAINT [FK_Fuel_tFuelQuality_FuelQualityTypeID_Fuel_tFuelQualityType] FOREIGN KEY ([FuelQualityTypeID]) REFERENCES [Fuel].[tFuelQualityType] ([FuelQualityTypeID])
ALTER TABLE [Fuel].[tFuelQuality] ADD CONSTRAINT [FK_Fuel_tFuelQuality_FuelSourceID_Fuel_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tUnitEquipmentStateDetail] ADD CONSTRAINT [FK_tUnitEquipmentStateDetail_UnitEquipmentStateSetID_tUnitEquipmentStateSet] FOREIGN KEY ([UnitEquipmentStateSetID]) REFERENCES [Projection].[tUnitEquipmentStateSet] ([UnitEquipmentStateSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tUnitEquipmentStateDetail] ADD CONSTRAINT [FK_tUnitEquipmentStateDetail_UnitEquipmentStateTypeID_tUnitEquipmentStateType] FOREIGN KEY ([UnitEquipmentStateTypeID]) REFERENCES [Projection].[tUnitEquipmentStateType] ([UnitEquipmentStateTypeID])
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentUnitMap] ADD CONSTRAINT [FK_tMaintenancePlanAdjustmentSetUnitMap_tMaintenancePlanAdjustment] FOREIGN KEY ([MaintenancePlanAdjustmentID]) REFERENCES [AdaptivePlanning].[tMaintenancePlanAdjustment] ([MaintenancePlanAdjustmentID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustmentUnitMap] ADD CONSTRAINT [FK_tMaintenancePlanAdjustmentUnitMap_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID]) ON DELETE CASCADE
ALTER TABLE [AdaptivePlanning].[tMaintenancePlanAdjustment] ADD CONSTRAINT [FK_tMaintenancePlanAdjustment_tMaintenancePlanAdjustmentSe] FOREIGN KEY ([MaintenancePlanAdjustmentSetID]) REFERENCES [AdaptivePlanning].[tMaintenancePlanAdjustmentSet] ([MaintenancePlanAdjustmentSetID]) ON DELETE CASCADE
ALTER TABLE [FuelTracker].[tNode] ADD CONSTRAINT [FK_FuelTracker_tNode_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [FuelTracker].[tNode] ADD CONSTRAINT [FK_FuelTracker_tNode_StationGroupID_Asset_tStationGroup] FOREIGN KEY ([StationGroupID]) REFERENCES [Asset].[tStationGroup] ([StationGroupID])
ALTER TABLE [FuelTracker].[tNode] ADD CONSTRAINT [FK_FuelTracker_tNode_StationID_Asset_tStation] FOREIGN KEY ([StationID]) REFERENCES [Asset].[tStation] ([StationID])
ALTER TABLE [FuelTracker].[tNode] ADD CONSTRAINT [FK_FuelTracker_tNode_UnitID_Asset_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
ALTER TABLE [FuelTracker].[tNode] ADD CONSTRAINT [FK_tFTNode_FTNodeTypeID_tFTNodeType] FOREIGN KEY ([FTNodeTypeID]) REFERENCES [FuelTracker].[tNodeType] ([FTNodeTypeID])
ALTER TABLE [FuelTracker].[tNode] ADD CONSTRAINT [FK_tFTNode_FTNodeWithdrawalTypeID_tFTNodeWithdrawalType] FOREIGN KEY ([FTNodeWithdrawalTypeID]) REFERENCES [FuelTracker].[tNodeWithdrawalType] ([FTNodeWithdrawalTypeID])
ALTER TABLE [Asset].[tStation] ADD CONSTRAINT [FK_tStation_StationGroupID_tStationGroup] FOREIGN KEY ([StationGroupID]) REFERENCES [Asset].[tStationGroup] ([StationGroupID])
ALTER TABLE [Asset].[tStation] ADD CONSTRAINT [FK_tStation_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
ALTER TABLE [Projection].[tUnitConfigScheduleDetail] ADD CONSTRAINT [FK_tUnitConfigScheduleDetail_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Projection].[tUnitConfigScheduleDetail] ADD CONSTRAINT [FK_tUnitConfigScheduleDetail_UnitConfigScheduleSetID_tUnitConfigScheduleSet] FOREIGN KEY ([UnitConfigScheduleSetID]) REFERENCES [Projection].[tUnitConfigScheduleSet] ([UnitConfigScheduleSetID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tUnitConfigScheduleSet] ADD CONSTRAINT [FK_tUnitConfigScheduleSet_DataVersionID_tDataVersion] FOREIGN KEY ([DataVersionID]) REFERENCES [Projection].[tDataVersion] ([DataVersionID])
ALTER TABLE [Projection].[tUnitConfigScheduleSet] ADD CONSTRAINT [FK_tUnitConfigScheduleSet_SensitivityImpactTypeID_tSensitivityImpactType] FOREIGN KEY ([SensitivityImpactTypeID]) REFERENCES [Projection].[tSensitivityImpactType] ([SensitivityImpactTypeID])
ALTER TABLE [Fuel].[tSolidFuelQuality] ADD CONSTRAINT [FK_Fuel_tSolidFuelQuality_FuelQualityID_Fuel_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID]) ON DELETE CASCADE
ALTER TABLE [Base].[tEventType] ADD CONSTRAINT [FK_Base_tEventType_EventEndPointTypeID_Base_tEventEndPointType] FOREIGN KEY ([EventEndPointTypeID]) REFERENCES [Base].[tEventEndPointType] ([EventEndPointTypeID])
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] ADD CONSTRAINT [FK_tScenarioFuelBurnSchedule_FuelQualityID_tFuelQuality] FOREIGN KEY ([FuelQualityID]) REFERENCES [Fuel].[tFuelQuality] ([FuelQualityID])
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] ADD CONSTRAINT [FK_tScenarioFuelBurnSchedule_FuelSourceID_tFuelSource] FOREIGN KEY ([FuelSourceID]) REFERENCES [Fuel].[tFuelSource] ([FuelSourceID])
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] ADD CONSTRAINT [FK_tScenarioFuelBurnSchedule_ScenarioID_tScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [Projection].[tScenario] ([ScenarioID]) ON DELETE CASCADE
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] ADD CONSTRAINT [FK_tScenarioFuelBurnSchedule_UnitConfigID_tUnitConfig] FOREIGN KEY ([UnitConfigID]) REFERENCES [Projection].[tUnitConfig] ([UnitConfigID])
ALTER TABLE [Projection].[tScenarioFuelBurnSchedule] ADD CONSTRAINT [FK_tScenarioFuelBurnSchedule_UnitID_tUnit] FOREIGN KEY ([UnitID]) REFERENCES [Asset].[tUnit] ([UnitID])
COMMIT TRANSACTION
