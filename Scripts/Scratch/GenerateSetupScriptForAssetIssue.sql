﻿-- =============================================
-- Script Template
-- =============================================
PRINT 'DELETE [TestDB].[Asset].[tAsset]'
EXEC [dbo].[sp_generate_merges] @table_name='tAttributeType', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAttributeOptionType', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetTreeNodeChildConfigurationType', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetType', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetClassType', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetClassTypeAssetClassTypeMap', @schema='Asset'
PRINT 'ALTER TABLE [Asset].[tAsset] DISABLE TRIGGER AssetInsertUpdate'
PRINT 'GO'
EXEC [dbo].[sp_generate_merges] @table_name='tAsset', @schema='Asset'
PRINT 'ALTER TABLE [Asset].[tAsset] ENABLE TRIGGER AssetInsertUpdate'
PRINT 'GO'

EXEC [dbo].[sp_generate_merges] @table_name='tAssetClassTypeAttributeTypeMap', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetTree', @schema='Asset'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetTreeNodeChildConfigurationType', @schema='Asset'


EXEC [dbo].[sp_generate_merges] @table_name='tIssueType', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tIssueCauseType', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetIssueCategoryType', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetIssue', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetIssueRelatedAssetIssue', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetClassTypeIssueTypeMap', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetIssueAssetIssueImpactTypeMap', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetIssueImpactCategoryType', @schema='Diagnostics'
EXEC [dbo].[sp_generate_merges] @table_name='tAssetIssueImpactType', @schema='Diagnostics'