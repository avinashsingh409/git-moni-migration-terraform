﻿/****** Object:  Table [EnergyStorage].[tGenerationDataSets]    Script Date: 07/31/2012 10:40:47 ******/
CREATE TABLE [EnergyStorage].[tGenerationDataSet] (
    [SetID]             INT            IDENTITY (1, 1) NOT NULL,
    [SetName]           VARCHAR (255)  NOT NULL,
    [Locale]            VARCHAR (255)  NOT NULL,
    [Interval]          TIME (7)       NOT NULL,
    [ChangeDate]        DATETIME2 (7)  DEFAULT (sysdatetime()) NOT NULL,
    [IntpBaseStdDev]    REAL           NULL,
    [IntpBaseThreshold] REAL           NULL,
    [VariabilityHours]  INT            NULL,
    [Variance]          REAL           NULL,
    [StandardDeviation] REAL           NULL,
    [AssetID]           INT            NULL,
    [Precision]         INT            DEFAULT ((5)) NOT NULL,
    [EvalGenTimes]      BIT            NULL,
    [CalcFacRating]     BIGINT         NULL,
    [Mean]              REAL           NULL,
    [DataSetType]       INT            NULL,
    [CreatedBy]         NVARCHAR (MAX) CONSTRAINT [DF_tGenerationDataSet_CreatedBy] DEFAULT ('BV') NOT NULL,
    [ChangedBy]         NVARCHAR (MAX) CONSTRAINT [DF_tGenerationDataSet_ChangedBy] DEFAULT ('BV') NOT NULL,
    [CreateDate]        DATETIME       CONSTRAINT [DF_tGenerationDataSet_CreateDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PKey_tGenerationDataSet] PRIMARY KEY CLUSTERED ([SetID] ASC),
    CONSTRAINT [FK_tGenerationDataSet_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [Unique_tGenerationDataSet_SetName] UNIQUE NONCLUSTERED ([SetName] ASC)
);











 






GO

CREATE TRIGGER [EnergyStorage].[TR_LastGenTouch]
ON [EnergyStorage].[tGenerationDataSet]
AFTER UPDATE
AS BEGIN
  SET NOCOUNT ON;

  UPDATE [EnergyStorage].[tLastActivity]
  SET [LastTouched] = CURRENT_TIMESTAMP
  WHERE [GenSetID] IN (SELECT [SetID] FROM inserted)
END;
GO

-- 
-- Alter triggers to handle batch updates
--
CREATE TRIGGER [EnergyStorage].[TR_LastGenDelete]
ON [EnergyStorage].[tGenerationDataSet]
AFTER DELETE
AS BEGIN
  SET NOCOUNT ON;

  DELETE FROM [EnergyStorage].[tLastActivity]
  WHERE [GenSetID] IN (SELECT [SetID] FROM deleted)
END;
GO


-- Triggers to simplify updating
CREATE TRIGGER [EnergyStorage].TR_LastGenInsert
ON [EnergyStorage].[tGenerationDataSet]
AFTER INSERT 
AS BEGIN
  SET NOCOUNT ON;

  INSERT [EnergyStorage].[tLastActivity]
  ( GenSetID, LastTouched )
  SELECT [SetID] as GenSetID, CURRENT_TIMESTAMP as LastTouched FROM inserted 
END;
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tGenerationDataSet';

