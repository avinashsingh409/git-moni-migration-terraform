﻿CREATE TABLE [EnergyStorage].[tFrequencyDataSet] (
    [FrequencyDataSetID] INT            IDENTITY (1, 1) NOT NULL,
    [SetName]            NVARCHAR (255) NOT NULL,
    [Locale]             NVARCHAR (255) NULL,
    [Interval]           TIME (7)       NOT NULL,
    [ChangeDate]         DATETIME       NOT NULL,
    [NominalFrequency]   INT            NOT NULL,
    [AssetID]            INT            NULL,
    [CreatedBy]          NVARCHAR (MAX) CONSTRAINT [DF_tFrequencyDataSet_CreatedBy] DEFAULT ('BV') NOT NULL,
    [ChangedBy]          NVARCHAR (MAX) CONSTRAINT [DF_tFrequencyDataSet_ChangedBy] DEFAULT ('BV') NOT NULL,
    [CreateDate]         DATETIME       CONSTRAINT [DF_tFrequencyDataSet_CreateDate] DEFAULT (getdate()) NOT NULL,
    [DataSetType]        INT            NULL,
    CONSTRAINT [PK_[EnergyStorage]].tFreqDataSet] PRIMARY KEY CLUSTERED ([FrequencyDataSetID] ASC),
    CONSTRAINT [FK_tFrequencyDataSet_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [IX_tFrequencyDataSetUniqueSetName] UNIQUE NONCLUSTERED ([SetName] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tFrequencyDataSet';

