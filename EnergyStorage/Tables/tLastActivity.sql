-- Step 5
-- Creates LastActivity table
--
CREATE TABLE [EnergyStorage].[tLastActivity] (
    [PerfSetID]   INT      NULL,
    [GenSetID]    INT      NULL,
    [LastTouched] DATETIME NOT NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tLastActivity';

