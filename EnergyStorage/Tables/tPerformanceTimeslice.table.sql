﻿CREATE TABLE [EnergyStorage].[tPerformanceTimeslice] (
    [SetID]         INT      NOT NULL,
    [Timestamp]     DATETIME NOT NULL,
    [StateOfCharge] REAL     DEFAULT ((0)) NOT NULL,
    [ScaledCharge]  REAL     DEFAULT ((0)) NOT NULL,
    [ExpectedOut_W] INT      NULL,
    [ActualOut_W]   INT      NULL,
    [ExpBattResp_W] INT      NULL,
    [ActBattResp_W] INT      NULL,
    CONSTRAINT [PKey_tPerformanceTimeslice] PRIMARY KEY CLUSTERED ([SetID] ASC, [Timestamp] ASC),
    CONSTRAINT [FKey_tPerformanceSet_SetID] FOREIGN KEY ([SetID]) REFERENCES [EnergyStorage].[tPerformanceSet] ([ID]) ON DELETE CASCADE
);







 






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tPerformanceTimeslice';

