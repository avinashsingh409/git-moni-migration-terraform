﻿/****** Object:  Table [EnergyStorage].[tGenerationDataTimeslices]    Script Date: 07/31/2012 10:41:06 ******/
CREATE TABLE [EnergyStorage].[tGenerationDataTimeslice] (
    [SetID]      INT      NOT NULL,
    [Timestamp]  DATETIME NOT NULL,
    [NetPower_W] BIGINT   NOT NULL,
    CONSTRAINT [PKey_tGEnerationDataTimeslice] PRIMARY KEY CLUSTERED ([SetID] ASC, [Timestamp] ASC),
    CONSTRAINT [FKey_tGenerationDataSets_SetID] FOREIGN KEY ([SetID]) REFERENCES [EnergyStorage].[tGenerationDataSet] ([SetID]) ON DELETE CASCADE
);







 






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tGenerationDataTimeslice';

