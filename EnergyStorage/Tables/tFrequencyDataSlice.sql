CREATE TABLE [EnergyStorage].[tFrequencyDataSlice] (
    [FrequencyDataSetID] INT        NOT NULL,
    [Timestamp]          DATETIME   NOT NULL,
    [Frequency]          FLOAT (53) NOT NULL,
    CONSTRAINT [PK_tFrequencyDataSlice] PRIMARY KEY CLUSTERED ([FrequencyDataSetID] ASC, [Timestamp] ASC),
    CONSTRAINT [FK_tFrequencyDataSlice_tFrequencyDataSet] FOREIGN KEY ([FrequencyDataSetID]) REFERENCES [EnergyStorage].[tFrequencyDataSet] ([FrequencyDataSetID]) ON DELETE CASCADE ON UPDATE CASCADE
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tFrequencyDataSlice';

