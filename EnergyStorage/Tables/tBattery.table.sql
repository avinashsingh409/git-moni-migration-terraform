﻿/****** Object:  Table [EnergyStorage].[tBattery]    Script Date: 07/31/2012 10:40:37 ******/
CREATE TABLE [EnergyStorage].[tBattery] (
    [BatteryID]           INT            IDENTITY (1, 1) NOT NULL,
    [BatteryName]         VARCHAR (255)  NOT NULL,
    [PowerRating]         INT            NOT NULL,
    [EnergyRating]        INT            NOT NULL,
    [RampRateMax]         INT            NOT NULL,
    [RampRateUnits]       VARCHAR (10)   NOT NULL,
    [ChargingEfficiency]  REAL           NOT NULL,
    [RoundTripEfficiency] REAL           NOT NULL,
    [ChangeDate]          DATETIME2 (7)  DEFAULT (sysdatetime()) NOT NULL,
    [AssetID]             INT            NULL,
    [PCSChargeEff]        REAL           NULL,
    [PCSDischargeEff]     REAL           NULL,
    [XFormerEff]          REAL           NULL,
    [CreatedBy]           NVARCHAR (MAX) CONSTRAINT [DF_tBattery_CreatedBy] DEFAULT ('BV') NOT NULL,
    [ChangedBy]           NVARCHAR (MAX) CONSTRAINT [DF_tBattery_ChangedBy] DEFAULT ('BV') NOT NULL,
    [CreateDate]          DATETIME       CONSTRAINT [DF_tBattery_CreateDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PKey_tBattery] PRIMARY KEY CLUSTERED ([BatteryID] ASC),
    CONSTRAINT [FK_tBattery_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [Unique_tBattery_BatteryName] UNIQUE NONCLUSTERED ([BatteryName] ASC)
);









 






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tBattery';

