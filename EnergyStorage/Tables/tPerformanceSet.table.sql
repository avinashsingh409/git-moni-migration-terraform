﻿CREATE TABLE [EnergyStorage].[tPerformanceSet] (
    [ID]                   INT            IDENTITY (1, 1) NOT NULL,
    [Name]                 VARCHAR (255)  NULL,
    [RunAt]                DATETIME2 (7)  NULL,
    [Preserve]             BIT            DEFAULT ((0)) NULL,
    [GenDataSetID]         INT            NULL,
    [BatteryID]            INT            NULL,
    [FirmWindowPower]      BIGINT         DEFAULT ((0)) NULL,
    [FirmWindowStart]      TIME (7)       DEFAULT ('00:00:00.000') NULL,
    [FirmWindowStop]       TIME (7)       DEFAULT ('23:59:59.999') NULL,
    [ChargeWindowPower]    BIGINT         DEFAULT ((0)) NULL,
    [ChargeWindowStart]    TIME (7)       DEFAULT ('00:00:00.000') NULL,
    [ChargeWindowStop]     TIME (7)       DEFAULT ('23:59:59.999') NULL,
    [SoCSoftMax]           REAL           DEFAULT ((1.0)) NULL,
    [SoCSoftMin]           REAL           DEFAULT ((0.0)) NULL,
    [HitSoCMax]            INT            DEFAULT ((0)) NULL,
    [HitSoCMin]            INT            DEFAULT ((0)) NULL,
    [HitSoCSoftMax]        INT            DEFAULT ((0)) NULL,
    [HitSoCSoftMin]        INT            DEFAULT ((0)) NULL,
    [PeakPrice]            REAL           DEFAULT ((0)) NULL,
    [OffPeakPrice]         REAL           DEFAULT ((0)) NULL,
    [BatterySetPoint]      REAL           DEFAULT ((0)) NULL,
    [ShapeRate]            REAL           DEFAULT ((0)) NULL,
    [ShapeRateUnits]       NCHAR (10)     DEFAULT ('None') NULL,
    [Degradation]          REAL           DEFAULT ((0)) NULL,
    [GridCharge]           BIGINT         DEFAULT ((0)) NULL,
    [ProvidedEnergy]       BIGINT         DEFAULT ((0)) NULL,
    [UnmetEnergy]          BIGINT         DEFAULT ((0)) NULL,
    [OutputFunction]       INT            DEFAULT ((10)) NULL,
    [FacilityRating]       BIGINT         DEFAULT ((0)) NULL,
    [ShapePercentage]      REAL           DEFAULT ((0.0)) NULL,
    [TotalGeneratedEnergy] BIGINT         DEFAULT ((0)) NULL,
    [TotalLostEnergy]      BIGINT         DEFAULT ((0)) NULL,
    [RampUpChargeVio]      INT            DEFAULT ((0)) NULL,
    [RampDownChargeVio]    INT            DEFAULT ((0)) NULL,
    [RampUpPowerVio]       INT            DEFAULT ((0)) NULL,
    [RampDownPowerVio]     INT            DEFAULT ((0)) NULL,
    [ChargeRespCount]      INT            DEFAULT ((0)) NULL,
    [DischargeRespCount]   INT            DEFAULT ((0)) NULL,
    [BatterySetPoint2]     REAL           DEFAULT ((0)) NULL,
    [LastUpdated]          DATETIME2 (7)  NULL,
    [ChargeFuncDef]        INT            NULL,
    [AssetID]              INT            NULL,
    [Precision]            INT            DEFAULT ((5)) NOT NULL,
    [DischargeToSetPt]     BIT            NULL,
    [UseHardOperLimits]    BIT            NULL,
    [InitialSOC]           REAL           NULL,
    [BattInputEnergy]      BIGINT         NULL,
    [UnmetECapability]     BIGINT         NULL,
    [UnmetECapacity]       BIGINT         NULL,
    [LostECapability]      BIGINT         NULL,
    [LostECapacity]        BIGINT         NULL,
    [DroopPct]             REAL           NULL,
    [DeadbandPct]          REAL           NULL,
    [DischargeLimitPct]    REAL           NULL,
    [ChargeLimitPct]       REAL           NULL,
    [NominalFrequency]     INT            NULL,
    [FreqDataSetID]        INT            NULL,
    [PeakLoadLimitW]       BIGINT         NULL,
    [CreatedBy]            NVARCHAR (MAX) CONSTRAINT [DF_tPerformanceSet_CreatedBy] DEFAULT ('BV') NOT NULL,
    [ChangedBy]            NVARCHAR (MAX) CONSTRAINT [DF_tPerformanceSet_ChangedBy] DEFAULT ('BV') NOT NULL,
    [CreateDate]           DATETIME       CONSTRAINT [DF_tPerformanceSet_CreateDate] DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tPerformanceSet_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tPerformanceSet_tBattery] FOREIGN KEY ([BatteryID]) REFERENCES [EnergyStorage].[tBattery] ([BatteryID]),
    CONSTRAINT [FK_tPerformanceSet_tFrequencyDataSet] FOREIGN KEY ([FreqDataSetID]) REFERENCES [EnergyStorage].[tFrequencyDataSet] ([FrequencyDataSetID]),
    CONSTRAINT [FK_tPerformanceSet_tGenerationDataSet] FOREIGN KEY ([GenDataSetID]) REFERENCES [EnergyStorage].[tGenerationDataSet] ([SetID]),
    UNIQUE NONCLUSTERED ([Name] ASC)
);














GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Connects a data set, a battery definition, and energy windows for analysis' , @level0type=N'SCHEMA',@level0name=N'EnergyStorage', @level1type=N'TABLE',@level1name=N'tPerformanceSet'




GO
/*EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Connects a data set, a battery definition, and energy windows for analysis' , @level0type=N'SCHEMA',@level0name=N'EnergyStorage', @level1type=N'TABLE',@level1name=N'tPerformanceSet'*/


GO

CREATE TRIGGER [EnergyStorage].[TR_LastPerfTouch]
ON [EnergyStorage].[tPerformanceSet]
AFTER UPDATE
AS BEGIN
  SET NOCOUNT ON;

  UPDATE [EnergyStorage].[tLastActivity]
  SET [LastTouched] = CURRENT_TIMESTAMP
  WHERE [PerfSetID] IN (SELECT [ID] FROM inserted)
END;
GO

CREATE TRIGGER [EnergyStorage].[TR_LastPerfDelete]
ON [EnergyStorage].[tPerformanceSet]
AFTER DELETE
AS BEGIN
  SET NOCOUNT ON;

  DELETE FROM [EnergyStorage].[tLastActivity]
  WHERE [PerfSetID] IN (SELECT [ID] FROM deleted)
END;
GO

CREATE TRIGGER [EnergyStorage].TR_LastPerfInsert
ON [EnergyStorage].[tPerformanceSet]
AFTER INSERT 
AS BEGIN
  SET NOCOUNT ON;

  INSERT [EnergyStorage].[tLastActivity]
  ( [PerfSetID], [LastTouched] )
  SELECT [ID] as PerfSetID, CURRENT_TIMESTAMP as LastTouched FROM inserted
END;
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tPerformanceSet';

