﻿CREATE TABLE [EnergyStorage].[tSetCounts] (
    [PerfSetID]  INT NULL,
    [GenSetID]   INT NULL,
    [CountType]  INT NOT NULL,
    [Percentage] INT NOT NULL,
    [Count]      INT NOT NULL,
    CONSTRAINT [FK_tSetCounts_tGenDataSet] FOREIGN KEY ([GenSetID]) REFERENCES [EnergyStorage].[tGenerationDataSet] ([SetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSetCounts_tPerformanceSet] FOREIGN KEY ([PerfSetID]) REFERENCES [EnergyStorage].[tPerformanceSet] ([ID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tSetCounts';

