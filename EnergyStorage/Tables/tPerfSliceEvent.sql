/* EnergyStorage changes within this script:
1. Creates tPerfSliceEvent table
2. Migrate events from tPerformanceTimeslice into tPerfSliceEvent
3. Drops old columns from tPerformanceTimeslice
4. Drops unused columns from tGenerationDataTimeslice
5. Creates LastActivity table
*/

-- Step 1
-- Creates tPerfSliceEvent table
--
CREATE TABLE [EnergyStorage].[tPerfSliceEvent] (
    [SetID]             INT      NOT NULL,
    [Timestamp]         DATETIME NOT NULL,
    [LostEnergy]        INT      NULL,
    [UnmetEnergy]       INT      NULL,
    [RampUpPowerVio]    BIT      NULL,
    [RampDownPowerVio]  BIT      NULL,
    [RampUpChargeVio]   BIT      NULL,
    [RampDownChargeVio] BIT      NULL,
    [ChargeBlock]       TINYINT  NULL,
    [DischargeBlock]    TINYINT  NULL,
    [UnmetECapability]  INT      NULL,
    [UnmetECapacity]    INT      NULL,
    [LostECapability]   INT      NULL,
    [LostECapacity]     INT      NULL,
    CONSTRAINT [PKey_tPerfSliceEvent] PRIMARY KEY CLUSTERED ([SetID] ASC, [Timestamp] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'EnergyStorage', @level1type = N'TABLE', @level1name = N'tPerfSliceEvent';

