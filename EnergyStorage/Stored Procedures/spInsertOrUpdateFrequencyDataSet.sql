﻿CREATE PROCEDURE [EnergyStorage].[spInsertOrUpdateFrequencyDataSet]
	@FrequencyDataSetID int output,
	@SetName nvarchar(255) = null,
	@Locale nvarchar(255) = null,
	@Interval time(7) = null,
	@ChangeDate datetime = null,
	@NominalFrequency int = null,
	@AssetID int = null,
	@CreatedBy nvarchar(MAX) = 'BV',
	@ChangedBy nvarchar(MAX) = 'BV',
	@DataSetType int = null	
AS
SET NOCOUNT ON;
IF @FrequencyDataSetID = 0
BEGIN
	INSERT INTO EnergyStorage.tFrequencyDataSet (
		SetName,
		Locale,
		Interval,
		ChangeDate,
		NominalFrequency,
		AssetID,
		CreatedBy,
		ChangedBy,
		DataSetType )
	VALUES (
		@SetName,
		@Locale,
		@Interval,
		@ChangeDate,
		@NominalFrequency,
		@AssetID,
		@CreatedBy,
		@ChangedBy,
		@DataSetType )
		
	SET @FrequencyDataSetID = (SELECT SCOPE_IDENTITY());		
END
ELSE
BEGIN
	UPDATE EnergyStorage.tFrequencyDataSet 
	SET
		SetName = @SetName,
		Locale  = @Locale,
		Interval = @Interval,
		ChangeDate = @ChangeDate,
		NominalFrequency = @NominalFrequency,
		AssetID = @AssetID,
		CreatedBy = @CreatedBy,
		ChangedBy = @ChangedBy,
		DataSetType = @DataSetType
	WHERE
		FrequencyDataSetID = @FrequencyDataSetID
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spInsertOrUpdateFrequencyDataSet] TO [TEUser]
    AS [dbo];

