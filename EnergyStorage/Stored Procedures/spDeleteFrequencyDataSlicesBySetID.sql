﻿
CREATE PROCEDURE [EnergyStorage].[spDeleteFrequencyDataSlicesBySetID]
@FrequencyDataSetID int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [EnergyStorage].[tFrequencyDataSlice]
	WHERE FrequencyDataSetID = @FrequencyDataSetID
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spDeleteFrequencyDataSlicesBySetID] TO [TEUser]
    AS [dbo];

