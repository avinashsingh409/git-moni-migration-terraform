﻿

CREATE PROCEDURE [EnergyStorage].[spGetAllFrequencyDataSets]
    @SecurityUserID int = -1,
    @DataSetType int = 0
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		F.FrequencyDataSetID,
		F.SetName,
		F.Locale,
		F.Interval,		
		( SELECT MIN([Timestamp]) 
		  FROM [EnergyStorage].[tFrequencyDataSlice]
		  WHERE FrequencyDataSetID = F.FrequencyDataSetID
		 ) As BeginDate,
		( SELECT MAX([Timestamp]) 
		  FROM [EnergyStorage].[tFrequencyDataSlice]
		  WHERE FrequencyDataSetID = F.FrequencyDataSetID
		 ) As EndDate,
		F.ChangeDate,
		F.NominalFrequency,
		F.AssetID,
		F.CreateDate,
		F.ChangedBy,
		F.CreatedBy,
		F.DataSetType,
		A.AssetDesc
	FROM
		[EnergyStorage].[tFrequencyDataSet] F 
		LEFT JOIN Asset.tAsset A ON F.AssetID = A.AssetID
		WHERE (@SecurityUserID=-1 
		   OR F.AssetID IN (SELECT AssetID FROM Asset.ufnGetAssetIdsForUser(@SecurityUserID)))
		AND F.DataSetType = @DataSetType		
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spGetAllFrequencyDataSets] TO [TEUser]
    AS [dbo];

