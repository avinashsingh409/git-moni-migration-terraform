﻿
CREATE PROCEDURE [EnergyStorage].[spDeleteFrequencyDataSet]
@FrequencyDataSetID int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM EnergyStorage.tFrequencyDataSet
	WHERE FrequencyDataSetID = @FrequencyDataSetID
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spDeleteFrequencyDataSet] TO [TEUser]
    AS [dbo];

