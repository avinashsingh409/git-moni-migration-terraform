﻿CREATE PROCEDURE [EnergyStorage].[spGetFrequencyDataSetBySetID]
@FrequencyDataSetID int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		F.FrequencyDataSetID,
		F.SetName,
		F.Locale,
		F.Interval,
		( SELECT MIN([Timestamp]) 
		  FROM [EnergyStorage].[tFrequencyDataSlice]
		  WHERE FrequencyDataSetID = F.FrequencyDataSetID
		 ) As BeginDate,
		( SELECT MAX([Timestamp]) 
		  FROM [EnergyStorage].[tFrequencyDataSlice]
		  WHERE FrequencyDataSetID = F.FrequencyDataSetID
		 ) As EndDate,
		F.ChangeDate,
		F.NominalFrequency,
		F.AssetID,
		F.CreateDate,
		F.ChangedBy,
		F.CreatedBy,
		F.DataSetType
	FROM
		[EnergyStorage].[tFrequencyDataSet] F		
	WHERE
		F.FrequencyDataSetID = @FrequencyDataSetID
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spGetFrequencyDataSetBySetID] TO [TEUser]
    AS [dbo];

