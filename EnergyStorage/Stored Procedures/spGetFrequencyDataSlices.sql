﻿
CREATE PROCEDURE [EnergyStorage].[spGetFrequencyDataSlices]
@FrequencyDataSetID int
AS
BEGIN
	SELECT
		[FrequencyDataSetID],
		[Timestamp],
		[Frequency]		
	FROM 
		EnergyStorage.tFrequencyDataSlice
	WHERE 
		FrequencyDataSetID = @FrequencyDataSetID
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spGetFrequencyDataSlices] TO [TEUser]
    AS [dbo];

