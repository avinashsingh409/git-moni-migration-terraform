﻿
CREATE PROCEDURE [EnergyStorage].[spGetFrequencyDataSlicesForDateRange]
@FrequencyDataSetID int,
@BeginDate datetime,
@EndDate datetime
AS
BEGIN
	SELECT
		[FrequencyDataSetID],
		[Timestamp],
		[Frequency]		
	FROM 
		EnergyStorage.tFrequencyDataSlice
	WHERE 
		FrequencyDataSetID = @FrequencyDataSetID
		AND [Timestamp] >= @BeginDate
		AND [Timestamp] <= @EndDate
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spGetFrequencyDataSlicesForDateRange] TO [TEUser]
    AS [dbo];

