﻿
CREATE PROCEDURE [EnergyStorage].[spDeleteFrequencyDataSetByName]
@SetName nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM EnergyStorage.tFrequencyDataSet
	WHERE SetName LIKE '%' + @SetName + '%'
END
GO
GRANT EXECUTE
    ON OBJECT::[EnergyStorage].[spDeleteFrequencyDataSetByName] TO [TEUser]
    AS [dbo];

