﻿CREATE TYPE [Configuration].[tpDeployedApp] AS TABLE
(
    [ApplicationID]		UNIQUEIDENTIFIER NULL,
    [AppTypeID]			INT              NOT NULL,
    [DisplayName]		NVARCHAR (255)   NOT NULL,
    [InternalName]		NVARCHAR (255)   NOT NULL,
    [AppDescription]	NVARCHAR (MAX)   NULL,
    [AppReference]		UNIQUEIDENTIFIER NULL,
    [RequestedState]	INT NULL,
    [RequestedStateID]		UNIQUEIDENTIFIER NULL,
    [RequestedStateDate]	DATETIME NULL,
    [RespondedState]		INT NULL,
    [RespondedStateID]		UNIQUEIDENTIFIER NULL,
    [RespondedStateDate]	DATETIME NULL
);
GO

GRANT EXECUTE
    ON TYPE::[Configuration].[tpDeployedApp] TO [TEUser];
GO