Create PROCEDURE [Configuration].[spUpsertAppSettingsByAssetID]
	@AssetID int,
	@AppTypeID int = NULL,
	@KeyName nvarchar(50),
	@Value nvarchar(255),
	@Comment nvarchar(255) = NULL,
	@LocalOverRide bit = 0
AS
BEGIN
	declare @AssetGUID nvarchar(50)
	set @AssetGUID = RTRIM((select GlobalID from Asset.tAsset where AssetID = @AssetID))
	IF Exists(Select * from Configuration.tAppSettings where GlobalID = @AssetGUID and KeyName = @KeyName)
	Begin
		UPDATE Configuration.tAppSettings SET
			AppTypeID = @AppTypeID,
			Value = @Value,
			Comment = @Comment,
			LocalOverRide = @LocalOverRide
		where GlobalID = @AssetGUID and KeyName = @KeyName
	End
	ELSE
	Begin
		INSERT INTO Configuration.tAppSettings(GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide)
		VALUES(@AssetGuid, @AppTypeID, @KeyName, @Value, @Comment, @LocalOverRide);
	End
END

GO

GRANT EXECUTE
    ON OBJECT::[Configuration].[spUpsertAppSettingsByAssetID] TO [TEUser]
    AS [dbo];
GO


