﻿


CREATE PROCEDURE [Configuration].[spGetAppSettingsByGUIDandKey] 
	@GUID nvarchar(50),
	@Key nvarchar(50),
	@AppTypeID int = NULL
AS
BEGIN
	select
		AppSettingsID
		, GlobalID
		, AppTypeID
		, KeyName
		, [Value]
		, Comment
		, LocalOverRide
	from [Configuration].tAppSettings
	where
		GlobalID = @GUID
		and KeyName = @Key
		and case when @AppTypeID is null or @AppTypeID = 0 then 1 else case when AppTypeID = @AppTypeID then 1 else 0 end end = 1
	;
END
GO

GRANT EXECUTE 
    ON OBJECT::[Configuration].[spGetAppSettingsByGUIDandKey] TO [TEUser]
    AS [dbo];
GO

