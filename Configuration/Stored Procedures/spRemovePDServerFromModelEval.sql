CREATE PROCEDURE [Configuration].[spRemovePDServerFromModelEval] 
	-- Add the parameters for the stored procedure here
	@PDServerID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Status INT = 0

	DECLARE @AppSettingsID INT, @ExistingPDServerIDs VARCHAR(MAX)

    -- Insert statements for procedure here
	SELECT
		@AppSettingsID = AppSettingsID,
		@ExistingPDServerIDs = Value
	FROM
		Configuration.tAppSettings
	WHERE
		AppTypeID = 5 and
		KeyName = 'PDServerIDs' and 
		((Value like cast(@PDServerID AS VARCHAR(MAX)) + ',%') OR (Value like '%,' + cast(@PDServerID AS VARCHAR(MAX)) + ',%') OR (Value like '%,' + cast(@PDServerID AS VARCHAR(MAX))) OR (Value = cast(@PDServerID AS VARCHAR(MAX))))

	IF @AppSettingsID IS NOT NULL
		BEGIN
			DECLARE @UpdatedPDServerIDs VARCHAR(MAX)
			-- Remove the PDServerID from the value.
			SELECT @UpdatedPDServerIDs = [Configuration].[ufnCreateModelEvalValueWithoutPDServerID](cast(@PDServerID as VARCHAR(MAX)), @ExistingPDServerIDs)

			IF @UpdatedPDServerIDs IS NOT NULL
				-- Update the AppSettingsID's PDServerIDs key with the new value.
				UPDATE [Configuration].[tAppSettings] SET Value = @UpdatedPDServerIDs WHERE AppSettingsID = @AppSettingsID AND KeyName = 'PDServerIDs'
		END
	ELSE
		SET @Status = 1
	
	RETURN @Status
END
GO

GRANT EXECUTE
    ON OBJECT::[Configuration].[spRemovePDServerFromModelEval] TO [TEUser]
    AS [dbo];