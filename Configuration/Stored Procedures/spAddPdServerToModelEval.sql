﻿CREATE PROCEDURE [Configuration].[spAddPdServerToModelEval]
	@pdServerId int
AS
BEGIN
	DECLARE @appSettingsIdToUse int;
	DECLARE @newPdIds nvarchar(max);
	DECLARE @previousLowestModelCount int;
	DECLARE @pdServerArchitectureType int;
	DECLARE @appSettingsId int;
	DECLARE @pdIds nvarchar(max);
	DECLARE @myCursor CURSOR;

	SET @previousLowestModelCount = 2147483647;
	SET @myCursor = CURSOR FOR SELECT tAS.AppSettingsID, tAS.Value FROM Configuration.tAppSettings tAS WHERE AppTypeID = 5 and KeyName = 'PDServerIDs';
	SET @pdServerArchitectureType = (SELECT NDNewArchitecture FROM ProcessData.tServer WHERE PDServerID = @pdServerId)
	IF @pdServerArchitectureType IS NULL
	BEGIN  
		RAISERROR( 'Not a valid PD Server',16, 1);
		RETURN 0;
	END;

	OPEN @myCursor
	FETCH NEXT FROM @myCursor INTO @appSettingsId, @pdIds	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @modelCount int;
		DECLARE @isValidModelEval int;
		DECLARE @isDuplicatePDServerId int;

		IF @pdIds IS NULL OR DATALENGTH(@pdIds) = 0
		BEGIN
			SET @previousLowestModelCount = 0;
			SET @appSettingsIdToUse = @appSettingsId;
			SET @newPdIds = @pdIds;
			BREAK;
		END

		--Check if the PD Server Id already exists in AppSettings table, if so return True.
		SET @isDuplicatePDServerId = (SELECT DISTINCT COUNT(value) FROM STRING_SPLIT(@pdIds, ',') WHERE value = @pdServerId)
		IF @isDuplicatePDServerId > 0
		BEGIN
			RETURN 1;
		END;

		--Determine if the model eval has old or new arch PD Servers
		SET @isValidModelEval = (Select COUNT(*) from ProcessData.tServer WHERE NDNewArchitecture = @pdServerArchitectureType 
								AND PDServerID IN (SELECT DISTINCT value FROM STRING_SPLIT(@pdIds, ',')))

		IF @isValidModelEval > 0
		BEGIN
			--Determine model count for the given list of PD Servers
			EXEC @modelCount = Monitoring.spGetNDModelCount @pdIds 
			
			IF @modelCount < @previousLowestModelCount
			BEGIN
				SET @previousLowestModelCount = @modelCount;
				SET @appSettingsIdToUse = @appSettingsId;
				SET @newPdIds = @pdIds;
			END;
		END;

		FETCH NEXT FROM @myCursor INTO @appSettingsId, @pdIds
	END;

	CLOSE @myCursor
	DEALLOCATE @myCursor

	--Append the new PD  Server id in the AppSettings for Model Eval
	IF @appSettingsIdToUse IS NOT NULL
	BEGIN
		IF @newPdIds is NULL OR DATALENGTH(@newPdIds) = 0
		BEGIN
			SET @newPdIds = CONVERT(nvarchar(max), @pdServerId);
		END
		ELSE
		BEGIN
			SET @newPdIds = @newPdIds + ',' + CONVERT(nvarchar(max), @pdServerId);
		END
		UPDATE Configuration.tAppSettings SET Value = @newPdIds WHERE AppSettingsID = @appSettingsIdToUse;
	END

	RETURN 1;
END
GO

GRANT EXECUTE
    ON OBJECT::[Configuration].[spAddPdServerToModelEval] TO [TEUser]
    AS [dbo];
GO
