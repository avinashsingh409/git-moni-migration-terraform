﻿CREATE PROCEDURE [Configuration].[spSaveDeployedApp]
      @tblDeployedApp Configuration.tpDeployedApp READONLY
AS

BEGIN
    SET NOCOUNT ON; 
	INSERT INTO [Configuration].[tDeployedApp](AppTypeID, DisplayName, InternalName, AppDescription, AppReference, RequestedState, RequestedStateID, RequestedStateDate, RespondedState, RespondedStateID, RespondedStateDate)
	SELECT appTypeID, displayName, internalName, appDescription, appReference, requestedState, requestedStateID, requestedStateDate, respondedState, respondedStateID, respondedStateDate
	FROM @tblDeployedApp WHERE applicationID NOT IN (SELECT ApplicationID FROM [Configuration].[tDeployedApp])

	UPDATE b  SET
		AppTypeID = a.appTypeID,
		DisplayName = a.displayName,
		InternalName = a.internalName,
		AppDescription = a.appDescription,
		AppReference = a.appReference,
		RequestedState = a.requestedState,
		RequestedStateID = a.requestedStateID,
		RequestedStateDate = a.requestedStateDate,
		RespondedState = a.respondedState,
		RespondedStateID = a.respondedStateID,
		RespondedStateDate = a.respondedStateDate
	FROM @tblDeployedApp a JOIN [Configuration].[tDeployedApp] b ON a.applicationID = b.ApplicationID
END
GO

GRANT EXECUTE
    ON OBJECT::[Configuration].[spSaveDeployedApp] TO [TEUser]
    AS [dbo];
GO
