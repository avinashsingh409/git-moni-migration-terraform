﻿CREATE PROCEDURE [Monitoring].[spGetNDModelCount]
	@pdServerIds nvarchar(max)
AS
BEGIN
	DECLARE @result int

	SET @result = (Select Count(*)
	FROM ProcessData.tCalcPoint cp
	JOIN Monitoring.tNDModel model ON cp.CalcPointID = model.CalcPointID
	JOIN ProcessData.tServer pds ON pds.PDServerID = cp.PDServerID
	WHERE cp.NDCreated = 1 AND model.Active = 1
	AND pds.PDServerID IN (SELECT DISTINCT value FROM STRING_SPLIT(@pdServerIds, ',')))

	RETURN @result;
END
GO

GRANT EXECUTE
    ON OBJECT::[Monitoring].[spGetNDModelCount] TO [TEUser]
    AS [dbo];
GO