﻿CREATE TABLE [Configuration].[tAppStateType]
(
	[AppStateTypeID] [int] NOT NULL,
    [AppStateTypeAbbrev] NVARCHAR(50) NOT NULL, 
    [AppStateTypeDesc] NVARCHAR(255) NOT NULL,
 CONSTRAINT [PK_tAppStateTypeID] PRIMARY KEY CLUSTERED 
(
	[AppStateTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Configuration', @level1type=N'TABLE',@level1name=N'tAppStateType'
GO