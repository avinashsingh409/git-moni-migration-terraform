CREATE TABLE [Configuration].[tModuleFormTabInfo] (
    [ModuleFormTabInfoID] INT           IDENTITY (1, 1) NOT NULL,
    [ModuleType]          NVARCHAR (50) NOT NULL,
    [FormName]            NVARCHAR (50) NOT NULL,
    [TabName]             NVARCHAR (50) NOT NULL,
    [IsVisible]           BIT           NULL,
    [TabOrder]            INT           NULL,
    [OverrideTabText]     NVARCHAR (50) NULL,
    CONSTRAINT [PK_ModuleFormTabInfo] PRIMARY KEY CLUSTERED ([ModuleFormTabInfoID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Configuration', @level1type = N'TABLE', @level1name = N'tModuleFormTabInfo';

