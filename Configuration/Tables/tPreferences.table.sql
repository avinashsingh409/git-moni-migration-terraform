CREATE TABLE [Configuration].[tPreferences] (
    [PreferencesID] INT            IDENTITY (1, 1) NOT NULL,
    [Key]           NVARCHAR (255) NOT NULL,
    [Value]         NVARCHAR (255) NOT NULL,
    [UserID]        NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tPreferences] PRIMARY KEY CLUSTERED ([PreferencesID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Configuration', @level1type = N'TABLE', @level1name = N'tPreferences';

