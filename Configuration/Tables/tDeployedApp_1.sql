﻿CREATE TABLE [Configuration].[tDeployedApp] (
    [ApplicationID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tDeployedApp_ApplicationID] DEFAULT (newid()) NOT NULL,
    [AppTypeID]          INT              NOT NULL,
    [DisplayName]        NVARCHAR (255)   NOT NULL,
    [InternalName]       NVARCHAR (255)   NOT NULL,
    [AppDescription]     NVARCHAR (MAX)   NULL,
    [AppReference]       UNIQUEIDENTIFIER NULL,
    [RequestedState]     INT              NULL,
    [RequestedStateID]   UNIQUEIDENTIFIER NULL,
    [RequestedStateDate] DATETIME         NULL,
    [RespondedState]     INT              NULL,
    [RespondedStateID]   UNIQUEIDENTIFIER NULL,
    [RespondedStateDate] DATETIME         NULL,
    CONSTRAINT [PK_tDeployedApp] PRIMARY KEY CLUSTERED ([ApplicationID] ASC),
    CONSTRAINT [CK_tDeployedApp_RequestedState] CHECK ([RequestedStateID] IS NULL AND [RequestedStateDate] IS NULL AND [RequestedState] IS NULL OR [RequestedStateID] IS NOT NULL AND [RequestedStateDate] IS NOT NULL AND [RequestedState] IS NOT NULL),
    CONSTRAINT [CK_tDeployedApp_RespondedState] CHECK ([RespondedStateID] IS NULL AND [RespondedStateDate] IS NULL AND [RespondedState] IS NULL OR [RespondedStateID] IS NOT NULL AND [RespondedStateDate] IS NOT NULL AND [RespondedState] IS NOT NULL),
    CONSTRAINT [FK_tDeployedApp_tAppStateType_RequestedState] FOREIGN KEY ([RequestedState]) REFERENCES [Configuration].[tAppStateType] ([AppStateTypeID]),
    CONSTRAINT [FK_tDeployedApp_tAppStateType_RespondedState] FOREIGN KEY ([RespondedState]) REFERENCES [Configuration].[tAppStateType] ([AppStateTypeID]),
    CONSTRAINT [FK_tDeployedApp_tAppType_AppTypeID] FOREIGN KEY ([AppTypeID]) REFERENCES [Configuration].[tAppType] ([AppTypeID])
);
GO

CREATE TRIGGER [Configuration].[DeployedAppUpdate]
    ON [Configuration].[tDeployedApp]
    AFTER INSERT, UPDATE
AS
BEGIN
	INSERT INTO [Configuration].tDeployedAppStateArchive 
	([ApplicationID],[RequestedState],[RequestedStateID],[RequestedStateDate],[RespondedState],[RespondedStateID],[RespondedStateDate])
	SELECT a.[ApplicationID],a.[RequestedState],a.[RequestedStateID],a.[RequestedStateDate],a.[RespondedState],a.[RespondedStateID],a.[RespondedStateDate]
	FROM [Configuration].[tDeployedApp] a
	JOIN inserted b
    ON a.ApplicationID = b.ApplicationID
END
GO