﻿CREATE TABLE [Configuration].[tDeployedAppStateArchive]
(
	[DeployedAppStateArchiveID] INT IDENTITY(1,1) NOT NULL,
    [ApplicationID] UNIQUEIDENTIFIER NOT NULL,
    [RequestedState]	INT NULL,
    [RequestedStateID]   UNIQUEIDENTIFIER NULL,
    [RequestedStateDate]   DATETIME NULL,
    [RespondedState]   INT NULL,
    [RespondedStateID]   UNIQUEIDENTIFIER NULL,
    [RespondedStateDate]   DATETIME NULL,
    CONSTRAINT [PK_tDeployedAppStateArchiveID] PRIMARY KEY CLUSTERED ([DeployedAppStateArchiveID] ASC),
    CONSTRAINT [FK_tDeployedAppStateArchiveID_tDeployedApp_ApplicationID] FOREIGN KEY ([ApplicationID]) REFERENCES [Configuration].[tDeployedApp] ([ApplicationID]) ON DELETE CASCADE,
	CONSTRAINT [CK_tDeployedAppStateArchive_RequestedState] CHECK (([RequestedStateID] IS NULL AND [RequestedStateDate] IS NULL AND [RequestedState] IS NULL)
														OR ([RequestedStateID] IS NOT NULL AND [RequestedStateDate] IS NOT NULL AND [RequestedState] IS NOT NULL)),
	CONSTRAINT [CK_tDeployedAppStateArchive_RespondedState] CHECK (([RespondedStateID] IS NULL AND [RespondedStateDate] IS NULL AND [RespondedState] IS NULL)
														OR ([RespondedStateID] IS NOT NULL AND [RespondedStateDate] IS NOT NULL AND [RespondedState] IS NOT NULL))
);
GO