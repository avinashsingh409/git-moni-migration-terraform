CREATE TABLE [Configuration].[tModuleInfo] (
    [ModuleType]   NVARCHAR (50)  NOT NULL,
    [ClassName]    NVARCHAR (MAX) NOT NULL,
    [AssemblyName] NVARCHAR (MAX) NOT NULL,
    [Path]         NVARCHAR (MAX) NULL,
    [ShouldLoad]   BIT            CONSTRAINT [DF_tModuleInfo_ShouldLoad] DEFAULT ((1)) NOT NULL,
    [DisplayOrder] INT            NOT NULL,
    CONSTRAINT [PK_tModuleInfo] PRIMARY KEY CLUSTERED ([ModuleType] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Configuration', @level1type = N'TABLE', @level1name = N'tModuleInfo';

