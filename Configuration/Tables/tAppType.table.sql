﻿CREATE TABLE [Configuration].[tAppType] (
    [ApplicationName] NVARCHAR (255) NULL,
    [AppTypeID]       INT            NOT NULL,
    CONSTRAINT [PK_tAppType] PRIMARY KEY NONCLUSTERED ([AppTypeID] ASC)
);


GO


GO


GO

GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Configuration', @level1type=N'TABLE',@level1name=N'tAppType'
GO

