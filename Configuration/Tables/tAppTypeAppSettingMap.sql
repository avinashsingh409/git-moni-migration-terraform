﻿CREATE TABLE [Configuration].[tAppTypeAppSettingMap] (
    [AppTypeAppSettingMapID] INT           IDENTITY (1, 1) NOT NULL,
    [AppTypeID]              INT           NOT NULL,
    [Key]                    NVARCHAR (50) NOT NULL,
    [IsRequired]             BIT           CONSTRAINT [DF_tAppTypeAppSettingMap_IsRequired] DEFAULT ((1)) NOT NULL,
    [LocalOverRide]          BIT           CONSTRAINT [DF_tAppTypeAppSettingMap_LocalOverRide] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tAppTypeAppSettingMap] PRIMARY KEY CLUSTERED ([AppTypeAppSettingMapID] ASC),
    CONSTRAINT [FK_tAppTypeAppSettingMap_tAppSetting_Key] FOREIGN KEY ([Key]) REFERENCES [Configuration].[tAppSetting] ([Key]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAppTypeAppSettingMap_tAppType_AppTypeID] FOREIGN KEY ([AppTypeID]) REFERENCES [Configuration].[tAppType] ([AppTypeID]),
    CONSTRAINT [UK_tAppTypeAppSettingMap_AppTypeID_Key] UNIQUE NONCLUSTERED ([AppTypeID] ASC, [Key] ASC)
);

