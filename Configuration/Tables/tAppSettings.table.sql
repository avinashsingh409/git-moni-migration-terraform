﻿CREATE TABLE [Configuration].[tAppSettings] (
    [AppSettingsID] INT              IDENTITY (1, 1) NOT NULL,
    [GlobalID]      UNIQUEIDENTIFIER NOT NULL,
    [AppTypeID]     INT              NOT NULL,
    [KeyName]       NVARCHAR (50)    NOT NULL,
    [Value]         NVARCHAR (MAX)   NOT NULL,
    [Comment]       NVARCHAR (255)   NULL,
    [LocalOverRide] BIT              NOT NULL,
    CONSTRAINT [PK_tAppSettings] PRIMARY KEY NONCLUSTERED ([AppSettingsID] ASC),
    CONSTRAINT [FK_Configuration_tAppSettings_AppSettingsTypeID_Configuration_tAppType] FOREIGN KEY ([AppTypeID]) REFERENCES [Configuration].[tAppType] ([AppTypeID]),
    CONSTRAINT [UK_Configuration_tAppSettings_GlobalID_AppTypeID_KeyName] UNIQUE NONCLUSTERED ([GlobalID] ASC, [AppTypeID] ASC, [KeyName] ASC)
);



GO


