﻿CREATE TABLE [Configuration].[tAlertsEngine]
(
	PDServerID int NOT NULL,
	PredictActive  bit NOT NULL DEFAULT((1)),
	EvalActive  bit NOT NULL DEFAULT((1)),
	MaxBackfillMinutes int NULL, 
    MaxDurationMinutes int NULL, 
    PredictJobBatchSize int NULL, 
    CONSTRAINT [PK_tAlertsEngine] PRIMARY KEY ([PDServerID]),
	CONSTRAINT [FK_Configuration_tAlertsEngine_PDServerID_ProcessData_tServer] FOREIGN KEY (PDServerID) REFERENCES ProcessData.tServer(PDServerID) ON DELETE CASCADE
);