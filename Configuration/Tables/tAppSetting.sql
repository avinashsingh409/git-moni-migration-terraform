﻿CREATE TABLE [Configuration].[tAppSetting] (
    [Key]     NVARCHAR (50)  NOT NULL,
    [Value]   NVARCHAR (MAX) NULL,
    [Comment] NVARCHAR (255) NULL,
    CONSTRAINT [PK_tAppSetting] PRIMARY KEY CLUSTERED ([Key] ASC)
);

