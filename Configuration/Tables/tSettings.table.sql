﻿CREATE TABLE [Configuration].[tSettings] (
    [Key]     NVARCHAR (50)  NOT NULL,
    [Value]   NVARCHAR (500) NULL,
    [Comment] NVARCHAR (255) NULL,
    CONSTRAINT [PK_tSettings] PRIMARY KEY CLUSTERED ([Key] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Configuration', @level1type = N'TABLE', @level1name = N'tSettings';

