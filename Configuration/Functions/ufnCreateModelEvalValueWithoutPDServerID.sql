-- =============================================
-- Author:		Nahush Chaturvedi
-- Create date: December 4, 2019
-- Description:	Removes @PDServerID from @PDServerIDs.
--              Returns a non-NULL valued VARCHAR(MAX) if and only if the removal succeeded. 
--              Returns NULL for any one of the following conditions:
--              a. @PDServerID is NULL or empty with or without whitespaces.
--              b. @PDServerIDs is NULL or empty with or without whitespaces.
--              c. @PDServerID does not exist in @PDServerIDs 
-- =============================================
CREATE FUNCTION [Configuration].[ufnCreateModelEvalValueWithoutPDServerID]
(
	@PDServerID VARCHAR(MAX),
	@PDServerIDs VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	IF @PDServerID IS NULL OR [Configuration].[ufnRemoveWhitespaces](@PDServerID) = ''
		RETURN NULL
	
	IF @PDServerIDs IS NULL OR [Configuration].[ufnRemoveWhitespaces](@PDServerIDs) = ''
		RETURN NULL	

	DECLARE @UpdatedPDServerIDs VARCHAR(MAX)
	
	-- The PDServer is the initial entry in @PDServerIDs.
	IF @PDServerIDs LIKE @PDServerID + ',%'
		SELECT @UpdatedPDServerIDs = REPLACE(@PDServerIDs, @PDServerID + ',', '')
	-- The PDServer is the medial entry in @PDServerIDs.
	ELSE IF @PDServerIDs LIKE '%,' + @PDServerID + ',%'
		SELECT @UpdatedPDServerIDs = REPLACE(@PDServerIDs, ',' + @PDServerID, '')
	-- The PDServer is the final entry in @PDServerIDs.
	ELSE IF @PDServerIDs LIKE '%,' + @PDServerID
		SELECT @UpdatedPDServerIDs = REPLACE(@PDServerIDs, ',' + @PDServerID, '')
	-- The PDServer is the only entry in @PDServerIDs.
	ELSE IF @PDServerIDs = @PDServerID
		SET @UpdatedPDServerIDs = ''
	ELSE
		SET @UpdatedPDServerIDs = NULL
		
	RETURN @UpdatedPDServerIDs;
END
GO

GRANT EXECUTE
	ON OBJECT::[Configuration].[ufnCreateModelEvalValueWithoutPDServerID] TO [TEUser]
	AS [dbo]
GO