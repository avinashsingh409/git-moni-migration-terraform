CREATE FUNCTION [Configuration].[ufnRemoveWhitespaces]
(
	@Text VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX) = @Text

	    --NULL
    Set @Result = Replace(@Result,CHAR(0),'');
    --Horizontal Tab
    Set @Result = Replace(@Result,CHAR(9),'');
    --Line Feed
    Set @Result = Replace(@Result,CHAR(10),'');
    --Vertical Tab
    Set @Result = Replace(@Result,CHAR(11),'');
    --Form Feed
    Set @Result = Replace(@Result,CHAR(12),'');
    --Carriage Return
    Set @Result = Replace(@Result,CHAR(13),'');
    --Column Break
    Set @Result = Replace(@Result,CHAR(14),'');
	--Single Space
	Set @Result = Replace(@Result,CHAR(32),'');
    --Non-breaking space
    Set @Result = Replace(@Result,CHAR(160),'');

	Set @Result = TRIM(@Result)

	Return @Result
END
GO

GRANT EXECUTE
	ON OBJECT::[Configuration].[ufnRemoveWhitespaces] TO [TEUser]
	AS [dbo]
GO