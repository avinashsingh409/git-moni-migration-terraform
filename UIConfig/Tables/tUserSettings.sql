﻿CREATE TABLE [UIConfig].[tUserSettings] (
[UserID] INT NOT NULL,
[Theme]	NVARCHAR (255)	NULL
PRIMARY KEY ([UserID]),
CONSTRAINT [FK_AccessControl_tUser_UIConfig_tUserSettings_UserID] FOREIGN KEY ([UserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);
GO