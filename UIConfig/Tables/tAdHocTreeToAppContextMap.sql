﻿CREATE TABLE [UIConfig].[tAdHocTreeToAppContextMap] (
    [TreeToAppContextMapId] INT              IDENTITY (1, 1) NOT NULL,
    [TreeId]                UNIQUEIDENTIFIER NOT NULL,
    [AppContext]            NVARCHAR (255)   DEFAULT ('Asset 360') NOT NULL,
    CONSTRAINT [PK_tAdHocTreeToAppContextMap] PRIMARY KEY CLUSTERED ([TreeToAppContextMapId] ASC),
    CONSTRAINT [FK_tAdHocTreeToAppContextMap_tAdHocTree] FOREIGN KEY ([TreeId]) REFERENCES [UIConfig].[tAdHocTree] ([TreeId]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAdHocTreeToAppContextMap_tAppContextDisplayName] FOREIGN KEY ([AppContext]) REFERENCES [UIConfig].[tAppContextDisplayName] ([Name]) ON DELETE CASCADE ON UPDATE CASCADE
);



