﻿CREATE TABLE [UIConfig].[tAppContextTab](
	[AppContextTabID] [int] NOT NULL,
	[AppContextID] [int] NOT NULL,
	[DisplayName] [nvarchar](255) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[SecurityResourceID] INT NULL, 
 CONSTRAINT [PK_tAppContextTabID] PRIMARY KEY CLUSTERED 
(
	[AppContextTabID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)
GO

ALTER TABLE [UIConfig].[tAppContextTab]  ADD  CONSTRAINT [FK_UIConfig_tAppContextTab_UIConfig_tAppContext] FOREIGN KEY([AppContextID])
REFERENCES [UIConfig].[tAppContext] ([AppContextID])
ON DELETE CASCADE
GO

ALTER TABLE [UIConfig].[tAppContextTab]  ADD  CONSTRAINT [FK_UIConfig_tAppContextTab_AccessControl_tResource] FOREIGN KEY([SecurityResourceID])
REFERENCES [AccessControl].[tResource] ([SecurityResourceID])
ON DELETE CASCADE
GO



