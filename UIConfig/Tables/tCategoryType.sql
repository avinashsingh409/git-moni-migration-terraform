﻿CREATE TABLE [UIConfig].[tCategoryType] (
    [ID]   INT           NOT NULL,
    [Name] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_UIConfig_tCategoryType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

