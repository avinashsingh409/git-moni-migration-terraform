﻿CREATE TABLE [UIConfig].[tAdHocTreeToClientMap] (
    [TreeToClientMapId] INT              IDENTITY (1, 1) NOT NULL,
    [TreeId]            UNIQUEIDENTIFIER NOT NULL,
    [ClientId]          INT              NOT NULL,
    CONSTRAINT [PK_tAdHocTreeToClientMap] PRIMARY KEY CLUSTERED ([TreeToClientMapId] ASC),
    CONSTRAINT [FK_tAdHocTreeToClientMap_tAdHocTree] FOREIGN KEY ([TreeId]) REFERENCES [UIConfig].[tAdHocTree] ([TreeId]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAdHocTreeToClientMap_tClient] FOREIGN KEY ([ClientId]) REFERENCES [Asset].[tClient] ([ClientID]) ON DELETE CASCADE
);

