﻿CREATE TABLE [UIConfig].[tAssetViewSelection] (
    [SecurityUserID] INT NOT NULL,
    [AssetID]        INT NOT NULL,
    [ViewID]         INT NOT NULL,
    CONSTRAINT [PK_UIConfig.tAssetViewSelection] PRIMARY KEY CLUSTERED ([SecurityUserID] ASC, [AssetID] ASC)
);

