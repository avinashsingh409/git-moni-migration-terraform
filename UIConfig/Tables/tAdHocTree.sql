﻿CREATE TABLE [UIConfig].[tAdHocTree] (
    [TreeId]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [TreeName]      NVARCHAR (255)   NOT NULL,
    [Global]        BIT              CONSTRAINT [DF_tAdHocTree_IsGlobal] DEFAULT ((1)) NOT NULL,
    [CreatedBy]     INT              NOT NULL,
    [ChangedBy]     INT              NOT NULL,
    [CreateDate]    DATETIME         NOT NULL,
    [ChangeDate]    DATETIME         NOT NULL,
    [IsSearchable]  BIT              DEFAULT ((0)) NOT NULL,
    [IsDefaultTree] BIT              DEFAULT ((0)) NOT NULL,
    [AllApp]        BIT              DEFAULT ((1)) NOT NULL,
    [IsPrivate]     BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tAdHocTree] PRIMARY KEY CLUSTERED ([TreeId] ASC)
);



GO

--Create trigger that will map any new trees to Asset Explorer
CREATE TRIGGER [UIConfig].[tr_tAdHocTree_Insert]
   ON  [UIConfig].[tAdhocTree] 
   AFTER INSERT
AS 
BEGIN
    --Retrieve the TreeID
	DECLARE @treeID UNIQUEIDENTIFIER
	SELECT @treeID = TreeID FROM inserted

	--Map this tree to Asset Explorer
	INSERT INTO [UIConfig].[tAdHocTreeToAppContextMap]
           ([TreeId]
           ,[AppContext])
     VALUES
           (@treeID
           ,'Asset Explorer')


END