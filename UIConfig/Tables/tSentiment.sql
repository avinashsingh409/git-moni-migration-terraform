﻿CREATE TABLE [UIConfig].[tSentiment]
(
	[SentimentID] INT NOT NULL PRIMARY KEY IDENTITY,
	[UserID] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Disposition] [nvarchar](255) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[State] [nvarchar](max) NOT NULL
)
