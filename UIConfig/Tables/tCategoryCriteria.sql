﻿CREATE TABLE [UIConfig].[tCategoryCriteria] (
    [ID]             INT IDENTITY (1, 1) NOT NULL,
    [CategoryTypeID] INT NOT NULL,
    [CriteriaID]     INT NOT NULL,
    [AppContextID]   INT NULL,
    [TabID]          INT NULL,
    [DisplayOrder]   INT NOT NULL,
    [RangeFlag]      BIT NULL,
    [IndicatorFlag]  BIT NULL,
    CONSTRAINT [PK_UIConfig_tCategoryCriteria] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tCategoryCriteria_tAppContext] FOREIGN KEY ([AppContextID]) REFERENCES [UIConfig].[tAppContext] ([AppContextID]),
    CONSTRAINT [FK_tCategoryCriteria_tAppContextTab] FOREIGN KEY ([TabID]) REFERENCES [UIConfig].[tAppContextTab] ([AppContextTabID]),
    CONSTRAINT [FK_tCategoryCriteria_tCategoryType] FOREIGN KEY ([CategoryTypeID]) REFERENCES [UIConfig].[tCategoryType] ([ID]),
    CONSTRAINT [FK_tCategoryCriteria_tCo] FOREIGN KEY ([CriteriaID]) REFERENCES [Criteria].[tCo] ([CoID])
);


GO


GO


GO


GO


GO


GO


GO


GO


GO