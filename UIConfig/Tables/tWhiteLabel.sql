﻿CREATE Table [UIConfig].[tWhiteLabel] (
	[CustomerID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar] (24) NULL,
	[Logo] [nvarchar] (255) NULL,
	CONSTRAINT [PK_UIConfig_tWhiteLabel] PRIMARY KEY CLUSTERED ([CustomerID] ASC),
	CONSTRAINT [FK_AccessControl_tCustomer_UIConfig_tWhiteLabel_CustomerID] FOREIGN KEY([CustomerID]) REFERENCES [AccessControl].[tCustomer] ([CustomerID])
)
GO