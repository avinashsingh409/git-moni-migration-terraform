
create TABLE [UIConfig].[tAdHocTreeCategoryMap](
	[CategoryMapID] [int] IDENTITY(1,1) NOT NULL,
	[TreeID] [uniqueidentifier] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_tAdHocTreeCategoryMap] PRIMARY KEY CLUSTERED 
(
	[CategoryMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tAdHocTreeCategoryMap] UNIQUE NONCLUSTERED 
(
	[CategoryID] ASC,
	[TreeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [UIConfig].[tAdHocTreeCategoryMap]  ADD FOREIGN KEY([CategoryID])
REFERENCES [Asset].[tCategory] ([CategoryID])
GO

ALTER TABLE [UIConfig].[tAdHocTreeCategoryMap]  ADD FOREIGN KEY([TreeID])
REFERENCES [UIConfig].[tAdHocTree] ([TreeID])
GO


ALTER TABLE [UIConfig].[tAdHocTreeCategoryMap]  ADD  CONSTRAINT [FK_tAdHocTreeToCategoryMap_tCategoryMapTreeID] FOREIGN KEY([TreeID])
REFERENCES [UIConfig].[tAdHocTree] ([TreeID])
ON DELETE CASCADE
GO
