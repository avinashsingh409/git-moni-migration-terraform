﻿CREATE TABLE [UIConfig].[tAdHocCoNodeBehavior](
	[CoNodeBehaviorId] INT NOT NULL,
	[CoNodeBehaviorAbbrev] NVARCHAR(50) NOT NULL,
	[CoNodeBehaviorDesc] NVARCHAR(255) NOT NULL,
	CONSTRAINT [PK_tAdHocCoNodeBehavior] PRIMARY KEY CLUSTERED ([CoNodeBehaviorId] ASC)
);