﻿CREATE TABLE [UIConfig].[tListConfig]
(
	[ListConfigID] INT NOT NULL PRIMARY KEY IDENTITY,
	[UserID] [int] NOT NULL,
	[ListType] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Content] [nvarchar](max) NOT NULL
)

