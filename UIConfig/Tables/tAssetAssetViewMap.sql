﻿CREATE TABLE [UIConfig].[tAssetAssetViewMap] (
    [AssetAssetViewMapID]      INT            IDENTITY (1, 1) NOT NULL,
    [AssetViewID]              INT            NOT NULL,
    [AssetID]                  INT            NULL,
    [AssetClassTypeID]         INT            NULL,
    [AssetTypeID]              INT            NULL,
    [UserRoleID]               INT            NULL,
    [AncestorAssetClassTypeID] INT            NULL,
    [AppContext]               NVARCHAR (255) NULL,
    [AppContextName]           NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([AssetAssetViewMapID] ASC),
    CONSTRAINT [FK_tAssetAssetViewMap_Ancestor_tAssetClassType] FOREIGN KEY ([AncestorAssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tAssetAssetViewMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetAssetViewMap_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tAssetAssetViewMap_tAssetType] FOREIGN KEY ([AssetTypeID]) REFERENCES [Asset].[tAssetType] ([AssetTypeID]),
    CONSTRAINT [FK_tAssetAssetViewMap_tAssetView] FOREIGN KEY ([AssetViewID]) REFERENCES [UIConfig].[tAssetView] ([AssetViewID]),
    CONSTRAINT [FK_tAssetAssetViewMap_tRole] FOREIGN KEY ([UserRoleID]) REFERENCES [AccessControl].[tRole] ([SecurityRoleID])
);






