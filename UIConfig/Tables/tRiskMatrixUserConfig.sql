﻿CREATE TABLE [UIConfig].[tRiskMatrixUserConfig] (
    [RiskMatrixUserConfigID]    INT             IDENTITY (1, 1) NOT NULL,
    [AppContextID]              INT             NOT NULL,
    [OwningUserID]              INT             NOT NULL,
    [AssetID]                   INT             NOT NULL,
    [JSONConfigString]          NVARCHAR (MAX)  NOT NULL,
    [IsPublic]                  BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tRiskMatrixUserConfig] PRIMARY KEY CLUSTERED ([RiskMatrixUserConfigID] ASC),
    CONSTRAINT [FK_tRiskMatrixUserConfig_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tRiskMatrixUserConfig_OwningUserID_tUser] FOREIGN KEY ([OwningUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UIConfig_tRiskMatrixUserConfig_UIConfig_tAppContext] FOREIGN KEY ([AppContextID]) REFERENCES [UIConfig].[tAppContext] ([AppContextID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IDX_tRiskMatrixUserConfig_AppContextID_OwningUserID_AssetID_IsPublic]
    ON [UIConfig].[tRiskMatrixUserConfig]([AppContextID] ASC, [OwningUserID] ASC, [AssetID] ASC, [IsPublic] ASC)
    INCLUDE([RiskMatrixUserConfigID], [JSONConfigString]);
GO
