﻿CREATE TABLE [UIConfig].[tAssetProperties] (
    [AssetID]    INT            NOT NULL,
    [Name]       NVARCHAR (255) NULL,
    [Properties] NVARCHAR (MAX) NOT NULL,
    [Protected]  BIT            DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([AssetID] ASC),
    CONSTRAINT [FK_tAssetProperties_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);



