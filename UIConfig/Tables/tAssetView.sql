﻿CREATE TABLE [UIConfig].[tAssetView] (
    [AssetViewID]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (255) NOT NULL,
    [Path]         NVARCHAR (255) DEFAULT ('loading') NOT NULL,
    [DisplayOrder]	INT            DEFAULT ((100)) NOT NULL,
	[StopAtLevel]	[int]	NULL,
	[ShowFuture]	[bit]	NULL,
	[TimeRange]		[nvarchar](255)		NULL,
	[Refresh]		int		NULL,
    PRIMARY KEY CLUSTERED ([AssetViewID] ASC)
);


