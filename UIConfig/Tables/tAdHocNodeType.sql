﻿CREATE TABLE [UIConfig].[tAdHocNodeType] (
    [NodeTypeId]     INT            NOT NULL,
    [NodeTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [NodeTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAdHocNodeType] PRIMARY KEY CLUSTERED ([NodeTypeId] ASC)
);

