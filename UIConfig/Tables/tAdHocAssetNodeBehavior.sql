﻿CREATE TABLE [UIConfig].[tAdHocAssetNodeBehavior] (
    [AssetNodeBehaviorId]     INT            NOT NULL,
    [AssetNodeBehaviorAbbrev] NVARCHAR (50)  NOT NULL,
    [AssetNodeBehaviorDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAdHocAssetNodeBehavior] PRIMARY KEY CLUSTERED ([AssetNodeBehaviorId] ASC)
);

