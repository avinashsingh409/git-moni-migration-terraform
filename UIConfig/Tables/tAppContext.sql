﻿CREATE TABLE [UIConfig].[tAppContext] (
    [AppContextID]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (255) NOT NULL,
    [Icon]               NVARCHAR (255) NOT NULL,
    [DisplayName]        NVARCHAR (255) NOT NULL,
    [DisplayOrder]       INT            DEFAULT ((1)) NOT NULL,
    [SecurityResourceID] INT            NOT NULL,
    [Path]               NVARCHAR (255) CONSTRAINT [DF_tAppContext_Path] DEFAULT ('Asset360') NOT NULL,
    [OpenInNew]          BIT            CONSTRAINT [DF_tAppContext_OpenInNew] DEFAULT ((0)) NOT NULL,
    [StopAtLevel]        INT            CONSTRAINT [DF_tAppContext_StopAtLevel] DEFAULT ((9)) NOT NULL,
    [ShowFuture]         BIT            CONSTRAINT [DF_tAppContext_ShowFuture] DEFAULT ((0)) NOT NULL,
    [TimeRange]          NVARCHAR (255) NULL,
    [TimeSelection]      NVARCHAR (255) NULL,
    [Locale]             NVARCHAR (255) NULL,
    [StartAsset]         INT            NULL,
    [Refresh]            INT            NULL,
    PRIMARY KEY CLUSTERED ([AppContextID] ASC),
    CONSTRAINT [FK_tAppContext_tAppContextDisplayName] FOREIGN KEY ([DisplayName]) REFERENCES [UIConfig].[tAppContextDisplayName] ([Name]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_tAppContext_tResource] FOREIGN KEY ([SecurityResourceID]) REFERENCES [AccessControl].[tResource] ([SecurityResourceID])
);









