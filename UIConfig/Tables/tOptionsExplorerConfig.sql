﻿CREATE TABLE [UIConfig].[tOptionsExplorerConfig] (
    [OptionsExplorerConfigID] INT            IDENTITY (1, 1) NOT NULL,
    [AppContextID]            INT            NOT NULL,
    [SecurityUserID]          INT            NOT NULL,
    [AssetID]                 INT            NOT NULL,
    [FurtherID]               INT            NULL,
    [JSONConfigString]        NVARCHAR (MAX) NOT NULL,
	[IsPublic]				  BIT			 DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tOptionsExplorerConfig] PRIMARY KEY CLUSTERED ([OptionsExplorerConfigID] ASC),
    CONSTRAINT [FK_tOptionsExplorerConfig_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tOptionsExplorerConfig_SecurityUserID_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UIConfig_tOptionsExplorerConfig_UIConfig_tAppContext] FOREIGN KEY ([AppContextID]) REFERENCES [UIConfig].[tAppContext] ([AppContextID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IDX_tOptionsExplorerConfig_AppContextID_SecurityUserID_AssetID_FurtherID_IsPublic]
    ON [UIConfig].[tOptionsExplorerConfig]([AppContextID] ASC, [SecurityUserID] ASC, [AssetID] ASC, [FurtherID] ASC, [IsPublic] ASC)
    INCLUDE([OptionsExplorerConfigID], [JSONConfigString]);

