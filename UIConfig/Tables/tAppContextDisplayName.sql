﻿CREATE TABLE [UIConfig].[tAppContextDisplayName] (
    [Name]                NVARCHAR (255) NOT NULL,
    [AdHocNavReady]       BIT            CONSTRAINT [DF_tAppContextDisplayName_AdHocNavReady] DEFAULT ((0)) NOT NULL,
    [AllowAllApps]        BIT            DEFAULT ((1)) NOT NULL,
    [IncludePhysicalTree] BIT            CONSTRAINT [DF_tAppContextDisplayName_IncludePhysicalTree] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tAppContextDisplayName] PRIMARY KEY CLUSTERED ([Name] ASC)
);




GO


GO

