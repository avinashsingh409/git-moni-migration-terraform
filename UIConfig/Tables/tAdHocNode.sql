﻿CREATE TABLE [UIConfig].[tAdHocNode] (
    [NodeId]                         UNIQUEIDENTIFIER CONSTRAINT [DF__tAdHocNod__NodeI__29A20B3F] DEFAULT (newid()) NOT NULL,
    [TreeId]                         UNIQUEIDENTIFIER NOT NULL,
    [NodeAbbrev]                     NVARCHAR (50)    NOT NULL,
    [NodeDesc]                       NVARCHAR (255)   NOT NULL,
    [ParentNodeId]                   UNIQUEIDENTIFIER NULL,
    [NodeTypeId]                     INT              NOT NULL,
    [DisplayOrder]                   INT              NOT NULL,
    [CreatedBy]                      INT              NOT NULL,
    [ChangedBy]                      INT              NOT NULL,
    [CreateDate]                     DATETIME         NOT NULL,
    [ChangeDate]                     DATETIME         NOT NULL,
    [AssetId]                        INT              NULL,
    [AssetNodeBehaviorId]            INT              NULL,
    [CriteriaObjectId]               INT              NULL,
    [CoNodeBehaviorId]               INT              NULL,
    CONSTRAINT [PK_tAdHocNode] PRIMARY KEY CLUSTERED ([NodeId] ASC),
    CONSTRAINT [FK_tAdHocNode_tAdHocAssetNodeBehavior] FOREIGN KEY ([AssetNodeBehaviorId]) REFERENCES [UIConfig].[tAdHocAssetNodeBehavior] ([AssetNodeBehaviorId]),
    CONSTRAINT [FK_tAdHocNode_tAdHocNode] FOREIGN KEY ([ParentNodeId]) REFERENCES [UIConfig].[tAdHocNode] ([NodeId]),
    CONSTRAINT [FK_tAdHocNode_tAdHocNodeType] FOREIGN KEY ([NodeTypeId]) REFERENCES [UIConfig].[tAdHocNodeType] ([NodeTypeId]),
    CONSTRAINT [FK_tAdHocNode_tAdHocTree] FOREIGN KEY ([TreeId]) REFERENCES [UIConfig].[tAdHocTree] ([TreeId]),
    CONSTRAINT [FK_tAdHocNode_tAsset] FOREIGN KEY ([AssetId]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAdHocNode_tCo] FOREIGN KEY ([CriteriaObjectId]) REFERENCES [Criteria].[tCo] ([CoID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAdHocNode_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tAdHocNode_tUser1] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
	CONSTRAINT [FK_tAdHocNode_tAdHocCoNodeBehavior] FOREIGN KEY ([CoNodeBehaviorId]) REFERENCES [UIConfig].[tAdHocCoNodeBehavior] ([CoNodeBehaviorId])
);








GO
CREATE NONCLUSTERED INDEX [IDX_tAdHocNode_TreeId_ParentNodeId]
    ON [UIConfig].[tAdHocNode]([TreeId] ASC, [ParentNodeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tAdHocNode_ParentNodeId]
    ON [UIConfig].[tAdHocNode]([ParentNodeId] ASC);

