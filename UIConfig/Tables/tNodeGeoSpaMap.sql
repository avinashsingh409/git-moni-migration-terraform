﻿CREATE TABLE [UIConfig].[tNodeGeoSpaMap] (
    [NodeGeoSpaMapID] INT              IDENTITY (1, 1) NOT NULL,
    [GeoSpaID]        INT              NOT NULL,
    [NodeID]          UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tNodeGeoSpaMap] PRIMARY KEY CLUSTERED ([NodeGeoSpaMapID] ASC),
    CONSTRAINT [FK_tNodeGeoSpaMap_tGeoSpa] FOREIGN KEY ([GeoSpaID]) REFERENCES [GeoSpa].[tGeoSpa] ([GeoSpaID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UIConfig_tNodeGeoSpaMap_NodeID_UIConfig_tAdHocNode] FOREIGN KEY ([NodeID]) REFERENCES [UIConfig].[tAdHocNode] ([NodeId])
);

