﻿
CREATE FUNCTION [UIConfig].[ufnGetOptionsExplorerConfig]
(
	-- Add the parameters for the function here
	@OptionsExplorerConfigID int = -1,
	@AppContextID int = -1,
	@SecurityUserID int = -1,
	@AssetID int = -1,
    @FurtherID int = -1
)
RETURNS TABLE
AS
RETURN
(
	SELECT *
	FROM [UIConfig].[tOptionsExplorerConfig]
	WHERE (@OptionsExplorerConfigID < 0 OR OptionsExplorerConfigID = @OptionsExplorerConfigID)
		AND (@AppContextID < 0 OR AppContextID = @AppContextID)
		AND (@AssetID < 0 OR AssetID = @AssetID)
		AND (@SecurityUserID < 0 OR ((SecurityUserID <> @SecurityUserID AND IsPublic = 1) OR (SecurityUserID = @SecurityUserID)))
        AND	(@FurtherID < 0 OR FurtherID = @FurtherID)
		AND [Asset].[ufnDoesUserHaveAccessToAsset](@AssetID, @SecurityUserID, @AppContextID) = 1
)
GO