﻿

CREATE FUNCTION [UIConfig].[ufnGetAppContextIdsAllowedForUserId]
(
   @SecurityUserID int
)
RETURNS @appContextIdTable TABLE
(
  AppContextID int PRIMARY KEY not NULL
)
AS
BEGIN

INSERT INTO @appContextIdTable (AppContextID)
SELECT DISTINCT appContext.AppContextID  
  FROM [UIConfig].[tAppContext] appContext
  INNER JOIN AccessControl.tAccessRule rul
  ON appContext.SecurityResourceID = rul.SecurityResourceID
  INNER JOIN AccessControl.tUserRoleMap map
  ON rul.SecurityRoleID = map.SecurityRoleID
  WHERE map.SecurityUserID = @SecurityUserID

RETURN
END