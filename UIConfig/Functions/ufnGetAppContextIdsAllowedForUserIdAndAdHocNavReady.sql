﻿CREATE FUNCTION [UIConfig].[ufnGetAppContextIdsAllowedForUserIdAndAdHocNavReady]
(
   @SecurityUserID int
)
RETURNS @appContextIdTable TABLE
(
  AppContextID int PRIMARY KEY not NULL
)
AS
BEGIN

INSERT INTO @appContextIdTable (AppContextID)
SELECT DISTINCT appContext.AppContextID  
  FROM [UIConfig].[tAppContext] appContext
  INNER JOIN AccessControl.tAccessRule rul
  ON appContext.SecurityResourceID = rul.SecurityResourceID
  INNER JOIN AccessControl.tUserRoleMap map
  ON rul.SecurityRoleID = map.SecurityRoleID
  INNER JOIN UIConfig.tAppContextDisplayName names on appContext.DisplayName = names.Name
  WHERE map.SecurityUserID = @SecurityUserID and names.AdHocNavReady=1

RETURN
END

GO