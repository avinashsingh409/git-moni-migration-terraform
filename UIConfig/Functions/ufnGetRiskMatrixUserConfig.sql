﻿
CREATE FUNCTION [UIConfig].[ufnGetRiskMatrixUserConfig]
(
	-- Add the parameters for the function here
	@AppContextID int,
	@OwningUserID int,
	@AssetID int
)
RETURNS TABLE
AS
RETURN
(
	SELECT *
	FROM [UIConfig].[tRiskMatrixUserConfig]
	WHERE AppContextID = @AppContextID
		AND AssetID = @AssetID
		AND (OwningUserID = @OwningUserID OR (OwningUserID <> @OwningUserID AND IsPublic = 1))
		AND [Asset].[ufnDoesUserHaveAccessToAsset](@AssetID, @OwningUserID, @AppContextID) = 1
)
GO

GRANT SELECT
	ON OBJECT::[UIConfig].[ufnGetRiskMatrixUserConfig] TO [TEUser]
	AS [dbo];
GO