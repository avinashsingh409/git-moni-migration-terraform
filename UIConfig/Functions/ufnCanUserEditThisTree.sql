﻿--Historically this check was done with a query built in the C#, which pulled all the assetIDs
--specified in all the tAdHocNode records for the tree into a tpIntList, counted that list, 
--then called ufnUserHasAccessToAssets with the list and counted how many it got back. If the
--two counts matched, the user was considered to have access. But this was very slow. 
--This function instead calls ufnDoesUserHaveAccessToAsset on each asset in that same list,
--without actually pulling them into a tpIntList, and returns the Min value. Since that call
--returns a bit, the min will either be 1 (if every single asset returned 1) or 0 (if any of
--the assets returned 0). This is much faster for small trees, and was still faster even for
--the 3000-asset adhoc tree I had available for testing. 
CREATE FUNCTION [UIConfig].[ufnCanUserEditThisTree]
(
    @treeID uniqueidentifier,
	@securityUserID int,
	@appContextID int = -1
)
RETURNS BIT
AS
BEGIN
declare @result bit = 0;
select @result = Min(CAST(Authorized as int)) from(
select asset.ufnDoesUserHaveAccessToAsset(assetID, @securityUserID, @appContextID) Authorized
	from uiconfig.tAdHocNode 
	where TreeId = @treeID 
	and AssetId is not null
) sub 
option(recompile)
if @result is null set @result = 1;
return @result
END
GO

GRANT EXECUTE ON  OBJECT::[UIConfig].[ufnCanUserEditThisTree] to [TEUser] as dbo;
GO