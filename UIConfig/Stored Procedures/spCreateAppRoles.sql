﻿
CREATE PROCEDURE [UIConfig].[spCreateAppRoles]
	@editingUserID int,
	@customerID uniqueidentifier
As
BEGIN
  
declare @appContextResourceId as int

select @appContextResourceId=SecurityResourceID  from AccessControl.tResource where [TEXT] = 'AppContext'

declare @applicationsRoleId as int
select @applicationsRoleId = SecurityRoleID from AccessControl.tRole where [TEXT] = 'Applications'

declare @rootRoleId as int
select @rootRoleId = SecurityRoleID from AccessControl.tRole where ParentSecurityRoleID = 0

declare @adminroleid as int
select @adminroleid = securityroleid from AccessControl.tRole where ParentSecurityRoleID = @rootRoleID and [text] = 'Administration'
if @adminroleid is null 
  begin
  exec [AccessControl].[spInsertRole] @rootRoleID,'Administration', @editingUserID, @customerID
  end
  
set @adminroleid = null
select @adminroleid = securityroleid from AccessControl.tRole where ParentSecurityRoleID = @rootRoleID and [text] = 'Administration'

declare @trendddefid as int
select @trendddefid = securityroleid from AccessControl.tRole where ParentSecurityRoleID = @adminroleid and [text] = 'Asset360Config'
if @trendddefid is null
  begin
  exec [AccessControl].[spInsertRole] @adminroleid,'Asset360Config', @editingUserID, @customerID
  end
 
set @trendddefid = null 
select @trendddefid = securityroleid from AccessControl.tRole where ParentSecurityRoleID = @adminroleid and [text] = 'Asset360Config'

declare @dataid as int
select @dataid = securityresourceid from AccessControl.tResource where [Text] = 'Data'
if @dataid is not null
  begin
  declare @trenddefresourceid as int
  select @trenddefresourceid = securityresourceid from AccessControl.tResource where ParentSecurityResourceID = @dataid and [TEXT] = 'TrendDefinitions'
  if @trenddefresourceid is null
    begin
    exec AccessControl.spInsertResource @dataid, 'TrendDefinitions'
    end
  
  set @trenddefresourceid = null
  select @trenddefresourceid = securityresourceid from AccessControl.tResource where ParentSecurityResourceID = @dataid and [TEXT] = 'TrendDefinitions'    
  if @trenddefresourceid is not null and @trendddefid is not null
    begin    
    insert into AccessControl.tAccessRule(SecurityRoleID,SecurityResourceID,SecurityAccessID,Allowed,CreatedBy,ChangedBy) values (@trendddefid,@trenddefresourceid,2,1,@editingUserID,@editingUserID)
    end
  end

declare @solutionLeadID as int
declare @developerID as int

if @rootRoleId is not null
  begin
  declare @id as int
  set @id = null
  select @id = SecurityRoleID from AccessControl.tRole where [TEXT] = 'Solution Leads'
  if @id is null
    begin
    exec [AccessControl].[spInsertRole] @rootRoleId,'Solution Leads', @editingUserID, @customerID
    end
    
  set @id = null
  select @id = SecurityRoleID from AccessControl.tRole where [TEXT] = 'Developers'
  if @id is null 
    begin
    exec [AccessControl].[spInsertRole] @rootRoleId,'Developers', @editingUserID, @customerID
    end
  end
  
set @solutionLeadID = null
select @solutionLeadID = SecurityRoleID from AccessControl.tRole where [TEXT] = 'Solution Leads'

set @developerID = null
select @developerID = SecurityRoleID from AccessControl.tRole  where [TEXT] = 'Developers'

declare @appResourceID as int
declare @appText as varchar(80)

if @appContextResourceId is not null and @applicationsRoleId is not null
  begin
  
  DECLARE myCursor CURSOR LOCAL FOR 
  select SecurityResourceID,[Text] from AccessControl.tResource where ParentSecurityResourceID = @appContextResourceId

  OPEN myCursor;
  FETCH NEXT FROM myCursor INTO @appResourceID,@appText
  WHILE @@FETCH_STATUS =  0
	BEGIN	
	
	declare @appRoleID as int
	set @appRoleID = null
	select @appRoleID = SecurityRoleID FROM AccessControl.tRole where ParentSecurityRoleID = @applicationsRoleId and [TEXT] = @appText
	if @appRoleID is null
	  begin
	  print 'Added ' + convert(varchar(25),@appResourceID) + ' ' + @appText
	  exec [AccessControl].[spInsertRole] @applicationsRoleId, @appText, @editingUserID, @customerID
	  end
	
	select @appRoleID = SecurityRoleID FROM AccessControl.tRole where ParentSecurityRoleID = @applicationsRoleId and [TEXT] = @appText
	declare @ruleID as int
	
	-- add rules to bind app roles and app resources
	set @ruleID = null
	select @ruleID = SecurityPermissionID from AccessControl.tAccessRule where SecurityRoleID=@appRoleID and SecurityResourceID = @appResourceID
	if @ruleID is null
	  begin
	  insert into AccessControl.tAccessRule(SecurityRoleID,SecurityResourceID,SecurityAccessID,Allowed,CreatedBy,ChangedBy) values 
	    (@appRoleID,@appResourceID,1,1,@editingUserID,@editingUserID)
	  
	  end
	  
	-- add rules to bind developer roles and app resources
	set @ruleID = null
	select @ruleID = SecurityPermissionID from AccessControl.tAccessRule where SecurityRoleID=@developerID and SecurityResourceID = @appResourceID
	if @ruleID is null
	  begin
	  insert into AccessControl.tAccessRule(SecurityRoleID,SecurityResourceID,SecurityAccessID,Allowed,CreatedBy,ChangedBy) values 
	    (@developerID,@appResourceID,1,1,@editingUserID,@editingUserID)
	  
	  end
	  
	FETCH NEXT FROM myCursor INTO @appResourceID,@appText
    END
  CLOSE myCursor;
  DEALLOCATE myCursor;
  end
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spCreateAppRoles] TO [TEUser]
    AS [dbo];

