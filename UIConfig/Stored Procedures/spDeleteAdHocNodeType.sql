﻿----------------------------------------------------
CREATE PROCEDURE [UIConfig].[spDeleteAdHocNodeType]
	@NodeTypeId int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [UIConfig].[tAdHocNodeType] WHERE NodeTypeId = @NodeTypeId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocNodeType] TO [TEUser]
    AS [dbo];

