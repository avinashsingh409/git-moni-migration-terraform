﻿
CREATE PROCEDURE [UIConfig].[spSetSelectedViewForAsset]
	@SecurityUserID int,
	@AssetID int,
	@ViewID int
AS
IF EXISTS (
	SELECT TOP 1 * FROM UIConfig.tAssetViewSelection
	WHERE UIConfig.tAssetViewSelection.SecurityUserID = @SecurityUserID
	AND UIConfig.tAssetViewSelection.AssetID = @AssetID
)
BEGIN
	UPDATE UIConfig.tAssetViewSelection
	SET ViewID = @ViewID
	WHERE UIConfig.tAssetViewSelection.SecurityUserID = @SecurityUserID
	AND UIConfig.tAssetViewSelection.AssetID = @AssetID
END
ELSE
BEGIN
	INSERT INTO [UIConfig].[tAssetViewSelection]
           ([SecurityUserID]
           ,[AssetID]
           ,[ViewID])
     VALUES
           (@SecurityUserID
           ,@AssetID
           ,@ViewID)
END

RETURN 0
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSetSelectedViewForAsset] TO [TEUser]
    AS [dbo];

