﻿
CREATE PROCEDURE [UIConfig].[spGetSelectedViewForAsset]
	@SecurityUserID int,
	@AssetID int
AS
SELECT ViewID FROM UIConfig.tAssetViewSelection 
WHERE UIConfig.tAssetViewSelection.SecurityUserID = @SecurityUserID
AND UIConfig.tAssetViewSelection.AssetID = @AssetID 

RETURN 0
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spGetSelectedViewForAsset] TO [TEUser]
    AS [dbo];

