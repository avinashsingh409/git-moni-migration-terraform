﻿----------------------------------------------------
CREATE PROCEDURE [UIConfig].[spDeleteAdHocAssetNodeBehavior]
	@AssetNodeBehaviorId int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [UIConfig].[tAdHocAssetNodeBehavior] WHERE AssetNodeBehaviorId = @AssetNodeBehaviorId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocAssetNodeBehavior] TO [TEUser]
    AS [dbo];

