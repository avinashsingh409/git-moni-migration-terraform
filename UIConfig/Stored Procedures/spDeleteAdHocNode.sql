﻿

CREATE PROCEDURE [UIConfig].[spDeleteAdHocNode]
	@NodeId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NodeIdTable TABLE (
		 NodeId uniqueidentifier PRIMARY KEY 
	);

	WITH parentage (nodeid)
	AS 
	(
		SELECT a.nodeid FROM UIConfig.tAdHocNode a where a.parentNodeId = @NodeId
		UNION all
		SELECT a.nodeid FROM UIConfig.tAdHocNode a INNER JOIN parentage c ON c.nodeId=a.ParentNodeId             
	)  
	INSERT INTO @NodeIdTable SELECT a.nodeid from parentage a UNION SELECT @NodeId;


	UPDATE UIConfig.tAdHocNode SET ParentNodeId = null
	FROM UIConfig.tAdHocNode tahn
	INNER JOIN @NodeIdTable a on a.NodeId=tahn.NodeId

	DELETE UIConfig.tAdHocNode
	FROM UIConfig.tAdHocNode tahn
	INNER JOIN @NodeIdTable a on a.NodeId=tahn.NodeId

END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocNode] TO [TEUser]
    AS [dbo];

