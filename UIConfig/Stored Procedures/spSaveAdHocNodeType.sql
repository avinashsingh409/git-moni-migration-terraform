﻿CREATE PROCEDURE [UIConfig].[spSaveAdHocNodeType]
	@NodeTypeId int,
	@NodeTypeAbbrev nvarchar(255),
	@NodeTypeDesc nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	UPDATE [UIConfig].[tAdHocNodeType] 
	SET NodeTypeAbbrev = @NodeTypeAbbrev,
		NodeTypeDesc = @NodeTypeDesc
	WHERE NodeTypeId = @NodeTypeId
	IF @@ROWCOUNT = 0 BEGIN
		INSERT INTO [UIConfig].[tAdHocNodeType] (NodeTypeId,NodeTypeAbbrev,NodeTypeDesc)
		VALUES (@NodeTypeId,@NodeTypeAbbrev,@NodeTypeDesc)
	END
	SELECT @NodeTypeId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocNodeType] TO [TEUser]
    AS [dbo];

