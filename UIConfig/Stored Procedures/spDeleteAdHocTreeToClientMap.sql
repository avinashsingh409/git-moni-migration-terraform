﻿----------------------------------------------------
CREATE PROCEDURE [UIConfig].[spDeleteAdHocTreeToClientMap]
	@TreeToClientMapId int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [UIConfig].[tAdHocTreeToClientMap] WHERE TreeToClientMapId = @TreeToClientMapId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocTreeToClientMap] TO [TEUser]
    AS [dbo];

