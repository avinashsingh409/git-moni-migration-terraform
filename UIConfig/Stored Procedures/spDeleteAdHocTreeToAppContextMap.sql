﻿----------------------------------------------------
CREATE PROCEDURE [UIConfig].[spDeleteAdHocTreeToAppContextMap]
	@TreeToAppContextMapId int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [UIConfig].[tAdHocTreeToAppContextMap] WHERE TreeToAppContextMapId = @TreeToAppContextMapId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocTreeToAppContextMap] TO [TEUser]
    AS [dbo];

