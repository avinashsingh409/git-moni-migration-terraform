CREATE PROCEDURE [UIConfig].[spSaveAdHocTreeToCategoryMap]
	@CategoryMapId int,
	@TreeId uniqueidentifier,
	@CategoryId INT,
    @NewMapId INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @CategoryMapId IS NULL BEGIN
		INSERT INTO [UIConfig].[tAdHocTreeCategoryMap] (TreeId,CategoryID)
		VALUES (@TreeId,@CategoryId)
		SET @NewMapId = (SELECT SCOPE_IDENTITY());
		RETURN;
	END
	UPDATE [UIConfig].[tAdHocTreeCategoryMap] 
	SET TreeId = @TreeId,
		CategoryId = @CategoryId
	WHERE CategoryMapId = @CategoryMapId
	SET @NewMapId = (SELECT @CategoryMapId);
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocTreeToCategoryMap] TO [TEUser]
    AS [dbo];

