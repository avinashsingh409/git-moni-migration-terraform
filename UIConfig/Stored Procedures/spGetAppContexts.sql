﻿
CREATE PROCEDURE [UIConfig].[spGetAppContexts]
	@SecurityUserID int
AS
SELECT appContext.[AppContextID]
      ,appContext.[Name]
      ,appContext.[Icon]
      ,appContext.[DisplayName]
      ,appContext.[DisplayOrder]
      ,appContext.[SecurityResourceID]
      ,appContext.[Path]
      ,appContext.[OpenInNew]
      ,appContext.[StopAtLevel]
      ,appContext.[ShowFuture]
	  ,appContext.[TimeRange]
	  ,appContext.[TimeSelection]
	  ,appContext.[Locale]
	  ,appContext.[StartAsset]
	  ,appContext.[Refresh]
  FROM [UIConfig].[tAppContext] appContext
  INNER JOIN AccessControl.tAccessRule rul
  ON appContext.SecurityResourceID = rul.SecurityResourceID
  INNER JOIN AccessControl.tUserRoleMap map
  ON rul.SecurityRoleID = map.SecurityRoleID
  WHERE map.SecurityUserID = @SecurityUserID AND rul.Allowed = 1

RETURN 0
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spGetAppContexts] TO [TEUser]
    AS [dbo];

