﻿CREATE PROCEDURE [UIConfig].[spSaveAdHocTreeToClientMap]
	@TreeToClientMapId int,
	@TreeId uniqueidentifier,
	@ClientId int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @TreeToClientMapId IS NULL BEGIN
		INSERT INTO [UIConfig].[tAdHocTreeToClientMap] (TreeId,ClientId)
		VALUES (@TreeId,@ClientId)
		SELECT SCOPE_IDENTITY();
		RETURN;
	END
	UPDATE [UIConfig].[tAdHocTreeToClientMap] 
	SET TreeId = @TreeId,
		ClientId = @ClientId
	WHERE TreeToClientMapId = @TreeToClientMapId
	SELECT @TreeToClientMapId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocTreeToClientMap] TO [TEUser]
    AS [dbo];

