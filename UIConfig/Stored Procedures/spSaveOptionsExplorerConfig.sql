﻿
CREATE PROCEDURE [UIConfig].[spSaveOptionsExplorerConfig]
    @OptionsExplorerConfigID int = -1,
    @AppContextID int,
	@SecurityUserID int,
	@AssetID int,
	@FurtherID int,
	@JSONConfigString nvarchar(max),
    @IsPublic bit,
	@NewOptionsExplorerConfigID int OUTPUT
AS
	
	DECLARE @InsertedID TABLE (ID int);

    IF @OptionsExplorerConfigID <= 0
	BEGIN
		INSERT INTO UIConfig.tOptionsExplorerConfig (AppContextID, SecurityUserID, AssetID, FurtherID, JSONConfigString, IsPublic)
		OUTPUT inserted.[OptionsExplorerConfigID] INTO @InsertedID ( ID )
		VALUES (@AppContextID, @SecurityUserID, @AssetID, @FurtherID, @JSONConfigString, @IsPublic)

		Select top 1  @NewOptionsExplorerConfigID = ID from @InsertedID;
	END
	ELSE --@OptionsExplorerConfigID >= 0
	BEGIN
		UPDATE UIConfig.tOptionsExplorerConfig
		SET AppContextID = @AppContextID,
			SecurityUserID = @SecurityUserID,
			AssetID = @AssetID,
			FurtherID = @FurtherID,
			JSONConfigString = @JSONConfigString,
            IsPublic = @IsPublic
		WHERE OptionsExplorerConfigID = @OptionsExplorerConfigID
		
		set @NewOptionsExplorerConfigID = @OptionsExplorerConfigID
	END
	
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveOptionsExplorerConfig] TO [TEUser]
    AS [dbo];
GO