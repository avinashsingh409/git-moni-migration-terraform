﻿CREATE PROCEDURE [UIConfig].[spAdHocAutoCompleteSearch] 
	-- Add the parameters for the stored procedure here
	@search NVARCHAR(255),
	@count int = 12,
	@securityuserID int,
	@treeid UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  
	DECLARE @baseIDs as BASE.tpIntList
	DECLARE @filteredIDs as TABLE ( AssetID int )
  
	INSERT INTO @baseIDs 
	SELECT a.[AssetID]
	FROM   
	[Asset].[tAsset] a
	WHERE a.IsHidden = 0 AND (a.AssetDesc like '%' + @search + '%' OR a.AssetAbbrev like '%' + @search + '%')
    
	INSERT INTO @filteredIDs SELECT AssetID FROM Asset.ufnUserHasAccessToAssets(@securityuserID, @baseIDs) 

	DECLARE @nodeIDS TABLE(NodeId UNIQUEIDENTIFIER, DisplayOrder int, AssetId INT NULL, NodeAbbrev NVARCHAR(50),NodeDesc NVARCHAR(255), NodeTypeID INT, NodeBehaviorID INT)
	INSERT INTO @nodeIDS(NodeId, DisplayOrder,AssetId,NodeAbbrev, NodeDesc, NodeTypeID, NodeBehaviorID) 
	SELECT NodeID, DisplayOrder,AssetId,NodeAbbrev, NodeDesc, NodeTypeID, AssetNodeBehaviorId FROM UIConfig.tAdHocNode WHERE TreeID = @treeID 

	SELECT TOP (@count) r.nodeOrder, r.NodeId, r.AssetId, r.NodeDesc,
	(case when parent.AssetDesc IS null then asset.AssetDesc else parent.AssetDesc + '\' + asset.AssetDesc END) as AssetDesc
	FROM
	(
		SELECT 1 + treeNode.DisplayOrder as nodeOrder, NodeId as NodeId, AssetId as AssetId, NodeDesc as NodeDesc
		FROM @nodeIDS treeNode
		WHERE (treeNode.AssetID IS NULL OR treeNode.AssetID in (select AssetId FROM @filteredIDs))
		AND (treeNode.NodeAbbrev like @search OR treeNode.NodeDesc like @search)

		UNION ALL

		SELECT 100 as nodeOrder, treeNode.NodeID as NodeId, a.AssetID as AssetId, a.AssetDesc  as NodeDesc
		FROM Asset.tAsset a 
		INNER JOIN @nodeIDS treeNode ON treeNode.AssetId = a.ParentAssetID
		INNER JOIN @filteredIDs f ON a.AssetID = f.AssetID
		WHERE treeNode.NodeTypeId = 2 
		AND treeNode.NodeBehaviorId = 2

		UNION ALL

		SELECT 1000 - hop.AncestryLevel as nodeOrder, treeNode.NodeID as NodeId, a.AssetID as AssetId, a.AssetDesc as NodeDesc
		FROM 
		@nodeIDS treeNode 
		INNER JOIN Asset.tAssetHop hop ON treeNode.AssetId = hop.HopAssetId
		INNER JOIN Asset.tAsset a ON hop.EndingAssetId = a.AssetID 
		INNER JOIN @filteredIDs f ON a.AssetID = f.AssetID
		WHERE treeNode.NodeTypeId = 2 
		AND treeNode.NodeBehaviorId = 3
		AND hop.AncestryLevel < 0
	) as r 
	LEFT JOIN Asset.tAsset asset ON r.AssetID = asset.AssetID 
	LEFT JOIN Asset.tAsset parent ON asset.ParentAssetID = parent.AssetID 
	ORDER BY r.nodeOrder, r.NodeDesc, r.AssetId
	
END

GO

GRANT EXECUTE
    ON OBJECT::[UIConfig].[spAdHocAutoCompleteSearch] TO [TEUser]
    AS [dbo];

