﻿



CREATE PROCEDURE [UIConfig].[spDeleteAdHocTree]
	-- Ensure UIConfigRepository.UserCanEditThisTree(treeId, securityUserId) = True before executing spDeleteAdHocTree
	@TreeId uniqueidentifier
AS
BEGIN
	DECLARE @Result INT;
	DECLARE @TranName VARCHAR(40);

	-- Result code for failure	
	SET @Result = -100;
	SET @TranName = 'Delete Adhoc Tree';
	
	BEGIN TRANSACTION @TranName
	
	BEGIN TRY
		SET NOCOUNT ON;

		DELETE r FROM [BI].[tReport] r JOIN [UIConfig].[tAdHocNode] n ON r.[NodeId] = n.[NodeId] WHERE n.[TreeId] = @TreeId;
		DELETE g FROM [UIConfig].[tNodeGeoSpaMap] g JOIN [UIConfig].[tAdHocNode] n ON g.[NodeId] = n.[NodeId] WHERE n.[TreeId] = @TreeId;
		DELETE FROM [UIConfig].[tAdHocNode] WHERE TreeId = @TreeId;
		DELETE FROM [UIConfig].[tAdHocTree] WHERE TreeId = @TreeId;
		-- The below should cascade delete
		--   UIConfig.tAdHocTreeToAppContextMap
		--   UIConfig.tAdHocTreeToClientMap
		--   BI.tReportSection
		--   BI.tReportSectionMap
		--   tReportSection widgets: Chart, FixedChart, Gauge, KPI, Map, Table, and Text
		COMMIT TRANSACTION @TranName;
		-- Result code for success
		SET @Result = 0;
	END TRY

	BEGIN CATCH
		PRINT N'Script failed, rolling back';
		ROLLBACK TRANSACTION @TranName;
	END CATCH

	SELECT @Result
END
GO

GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocTree] TO [TEUser]
    AS [dbo];