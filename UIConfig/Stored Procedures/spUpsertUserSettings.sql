﻿CREATE PROCEDURE [UIConfig].[spUpsertUserSettings]
	@userID int,
	@theme NVARCHAR(255)
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM UIConfig.tUserSettings  WHERE UserID = @userID)
	BEGIN
		INSERT INTO UIConfig.tUserSettings(UserID, Theme)
		VALUES (@userID, @theme);
	END
	ELSE
	BEGIN
		UPDATE UIConfig.tUserSettings 
		SET Theme = @theme
		WHERE UserID = @userID;
	END 
END

GO

GRANT EXECUTE
    ON OBJECT::[UIConfig].[spUpsertUserSettings] TO [TEUser]
    AS [dbo];
GO