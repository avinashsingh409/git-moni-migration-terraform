﻿CREATE PROCEDURE [UIConfig].[spSaveAdHocTreeToAppContextMap]
	@TreeToAppContextMapId int,
	@TreeId uniqueidentifier,
	@AppContext nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @TreeToAppContextMapId IS NULL BEGIN
		INSERT INTO [UIConfig].[tAdHocTreeToAppContextMap] (TreeId,AppContext)
		VALUES (@TreeId,@AppContext)
		SELECT SCOPE_IDENTITY();
		RETURN;
	END
	UPDATE [UIConfig].[tAdHocTreeToAppContextMap] 
	SET TreeId = @TreeId,
		AppContext = @AppContext
	WHERE TreeToAppContextMapId = @TreeToAppContextMapId
	SELECT @TreeToAppContextMapId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocTreeToAppContextMap] TO [TEUser]
    AS [dbo];

