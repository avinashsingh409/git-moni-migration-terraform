﻿CREATE PROCEDURE [UIConfig].[spSaveAdHocNode]
	@NodeId uniqueidentifier,
	@TreeId uniqueidentifier,
	@NodeAbbrev nvarchar(255),
	@NodeDesc nvarchar(255),
	@ParentNodeId uniqueidentifier,
	@NodeTypeId int,
	@DisplayOrder int,
	@CreatedBy int,
	@ChangedBy int,
	@AssetId int,
	@AssetNodeBehaviorId int,
	@CriteriaObjectId int,
	@CoNodeBehaviorId int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @NodeId IS NULL BEGIN

		DECLARE @nextDisplayOrder int;
		IF (@ParentNodeId IS NULL) BEGIN
			 SELECT @nextDisplayOrder = ISNULL(MAX(DisplayOrder), 0) + 1
			 FROM [UIConfig].[tAdHocNode] 
			 WHERE TreeId = @TreeId
			 AND ParentNodeId IS NULL
		END ELSE BEGIN
			 SELECT @nextDisplayOrder = ISNULL(MAX(DisplayOrder), 0) + 1
			 FROM [UIConfig].[tAdHocNode] 
			 WHERE TreeId = @TreeId
			 AND ParentNodeId = @ParentNodeId
		END

		DECLARE @newId uniqueidentifier;
		
		INSERT INTO [UIConfig].[tAdHocNode] (TreeId,NodeAbbrev,NodeDesc,ParentNodeId,NodeTypeId,DisplayOrder,CreatedBy,ChangedBy,CreateDate,ChangeDate,AssetId,AssetNodeBehaviorId,CriteriaObjectId,CoNodeBehaviorId)
		OUTPUT inserted.NodeId 
		VALUES (@TreeId,@NodeAbbrev,@NodeDesc,@ParentNodeId,@NodeTypeId,@nextDisplayOrder,@CreatedBy,@ChangedBy,@now,@now,@AssetId,@AssetNodeBehaviorId,@CriteriaObjectId,@CoNodeBehaviorId)
		
		RETURN;
	END
	UPDATE [UIConfig].[tAdHocNode] 
	SET TreeId = @TreeId,
		NodeAbbrev = @NodeAbbrev,
		NodeDesc = @NodeDesc,
		ParentNodeId = @ParentNodeId,
		NodeTypeId = @NodeTypeId,
		DisplayOrder = @DisplayOrder,
		ChangedBy = @ChangedBy,
		ChangeDate = @now,
		AssetId = @AssetId,
		AssetNodeBehaviorId = @AssetNodeBehaviorId,
		CriteriaObjectId = @CriteriaObjectId,
		CoNodeBehaviorId = @CoNodeBehaviorId
	WHERE NodeId = @NodeId
	SELECT @NodeId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocNode] TO [TEUser]
    AS [dbo];

