﻿CREATE PROCEDURE [UIConfig].[spSaveAdHocAssetNodeBehavior]
	@AssetNodeBehaviorId int,
	@AssetNodeBehaviorAbbrev nvarchar(255),
	@AssetNodeBehaviorDesc nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [UIConfig].[tAdHocAssetNodeBehavior] 
	SET AssetNodeBehaviorAbbrev = @AssetNodeBehaviorAbbrev,
		AssetNodeBehaviorDesc = @AssetNodeBehaviorDesc
	WHERE AssetNodeBehaviorId = @AssetNodeBehaviorId
	IF @@ROWCOUNT = 0 BEGIN
		INSERT INTO [UIConfig].[tAdHocAssetNodeBehavior] (AssetNodeBehaviorId,AssetNodeBehaviorAbbrev,AssetNodeBehaviorDesc)
		VALUES (@AssetNodeBehaviorId,@AssetNodeBehaviorAbbrev,@AssetNodeBehaviorDesc)
	END
	SELECT @AssetNodeBehaviorId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocAssetNodeBehavior] TO [TEUser]
    AS [dbo];

