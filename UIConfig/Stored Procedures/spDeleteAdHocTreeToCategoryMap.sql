CREATE PROCEDURE [UIConfig].[spDeleteAdHocTreeToCategoryMap]
	@CategoryMapId int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [UIConfig].[tAdHocTreeCategoryMap] WHERE CategoryMapID = @CategoryMapId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spDeleteAdHocTreeToCategoryMap] TO [TEUser]
    AS [dbo];

