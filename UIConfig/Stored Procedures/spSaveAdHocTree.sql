﻿--Modify the save routine to allow the trigger to retrieve the inserted TreeId
CREATE PROCEDURE [UIConfig].[spSaveAdHocTree]
	@TreeId uniqueidentifier,
	@TreeName nvarchar(255),
	@Global bit,
	@AllApp bit,
	@IsPrivate bit,
	@IsSearchable bit,
	@CreatedBy int,
	@ChangedBy int,
	@NewTreeId uniqueidentifier OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @now DateTime = GetDate();
	IF @TreeId IS NULL BEGIN
		DECLARE @newId uniqueidentifier;

		DECLARE @MyNewIdTable table (
                       id uniqueidentifier not null
               )
		
		INSERT INTO [UIConfig].[tAdHocTree] (TreeName,Global,AllApp, IsPrivate, CreatedBy,ChangedBy,CreateDate,ChangeDate,IsSearchable)
		OUTPUT inserted.TreeId INTO @MyNewIdTable
		VALUES (@TreeName,@Global,@AllApp,@IsPrivate,@CreatedBy,@ChangedBy,@now,@now,@IsSearchable)
		
		SET @NewTreeId = (SELECT TOP 1 id FROM @MyNewIdTable);

		RETURN;
	END
	UPDATE [UIConfig].[tAdHocTree] 
	SET TreeName = @TreeName,
		Global = @Global,
		AllApp = @AllApp,
		IsPrivate = @IsPrivate,
		ChangedBy = @ChangedBy,
		ChangeDate = @now,
		IsSearchable = @IsSearchable
	WHERE TreeId = @TreeId
	SET @NewTreeId = @TreeId;
END
GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAdHocTree] TO [TEUser]
    AS [dbo];

