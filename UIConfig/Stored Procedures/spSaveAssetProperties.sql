﻿
CREATE PROCEDURE [UIConfig].[spSaveAssetProperties] 
	-- Add the parameters for the stored procedure here
	@AssetID INT,
	@Name NVARCHAR(255) = NULL,
	@Properties NVARCHAR(MAX),
	@securityUserID int,
	@ignoreProtected int = 0
AS
BEGIN
	SET NOCOUNT ON;
	--Make sure that the user has access to the asset. 

	DECLARE @rights as BIT = 0
	SELECT @rights = Asset.ufnDoesUserHaveAccessToAsset(@AssetID,@SecurityUserID,-1)

	IF @rights = 1
	BEGIN
		--Make sure that the property is not protected
		IF NOT EXISTS(SELECT AssetID FROM UIConfig.tAssetProperties WHERE (Protected=1 and @ignoreProtected=0) AND AssetID = @AssetID AND ((@Name IS NULL AND Name IS NULL) OR (@Name = Name))) 
		BEGIN
			--If the property exists then update, otherwise insert. 
			IF EXISTS(SELECT AssetID FROM UIConfig.tAssetProperties WHERE AssetID = @AssetID AND ((@Name IS NULL AND Name IS NULL) OR (@Name = Name))) 
			BEGIN
				UPDATE UIConfig.tAssetProperties
				SET Properties = @Properties
				WHERE AssetID = @AssetID AND ((@Name IS NULL AND Name IS NULL) OR (@Name = Name))
			END
			ELSE
			BEGIN
				INSERT INTO UIConfig.tAssetProperties
				(AssetID, Name, Properties)
				VALUES 
				(@AssetID, @Name, @Properties)
			END
		END
	END
END


GO

GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveAssetProperties] TO [TEUser]
    AS [dbo];

