﻿
CREATE PROCEDURE [UIConfig].[spSaveRiskMatrixUserConfig]
    @RiskMatrixUserConfigID int = -1,
    @AppContextID int,
	@OwningUserID int,
	@AssetID int,
	@JSONConfigString nvarchar(max),
    @IsPublic bit,
	@NewRiskMatrixUserConfigID int OUTPUT
AS
	DECLARE @InsertedID TABLE (ID int);

	IF @RiskMatrixUserConfigID <= 0
	BEGIN
		INSERT INTO UIConfig.tRiskMatrixUserConfig (AppContextID, OwningUserID, AssetID, JSONConfigString, IsPublic)
		OUTPUT inserted.[RiskMatrixUserConfigID] INTO @InsertedID ( ID )
		VALUES (@AppContextID, @OwningUserID, @AssetID, @JSONConfigString, @IsPublic)

		Select top 1  @NewRiskMatrixUserConfigID = ID from @InsertedID;
	END
	ELSE --@RiskMatrixUserConfigID >= 0
	BEGIN
		DECLARE @currOwningUserID INT;
		DECLARE @currIsPublic BIT;
		SELECT @currOwningUserID=OwningUserID, @currIsPublic=IsPublic FROM UIConfig.tRiskMatrixUserConfig
				WHERE RiskMatrixUserConfigID=@RiskMatrixUserConfigID;

		IF(@OwningUserID <> @currOwningUserID ANd @currIsPublic = 0)
		BEGIN
			RAISERROR('User does not have access to update this config.', 11, -1, -1);
		END
		ELSE
		BEGIN
			UPDATE UIConfig.tRiskMatrixUserConfig
			SET JSONConfigString = @JSONConfigString,
				IsPublic = @IsPublic
			WHERE RiskMatrixUserConfigID = @RiskMatrixUserConfigID

			set @NewRiskMatrixUserConfigID = @RiskMatrixUserConfigID
		END
	END

GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spSaveRiskMatrixUserConfig] TO [TEUser]
    AS [dbo];
GO