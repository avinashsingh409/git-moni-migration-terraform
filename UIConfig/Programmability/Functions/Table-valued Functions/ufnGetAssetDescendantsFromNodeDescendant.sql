﻿CREATE FUNCTION [UIConfig].[ufnGetAssetDescendantsFromNodeDescendant]
(
  @securityuserID  INT,
  @nodeID UniqueIdentifier,
  @assetID INT
)
RETURNS @results TABLE
(
	AssetID int
)
BEGIN
	DECLARE @node_treeID UNIQUEIDENTIFIER
	DECLARE @node_asset INT
	DECLARE @node_behavior INT
	DECLARE @node_type INT

	SELECT @node_treeID = TreeId, @node_asset = AssetId, @node_type = NodeTypeID, @node_behavior = AssetNodeBehaviorId FROM UIConfig.tAdHocNode WHERE NodeID = @nodeID 

	-- If there is no asset defined then the existing method to get the asset descendants from 
	-- a node is adequate.
	IF (@assetID IS NULL OR @assetID = @node_asset)
	BEGIN 
		INSERT INTO @results (AssetID) 
		SELECT AssetID FROM UIConfig.ufnGetAssetDescendantsFromNode(@nodeID, @securityuserid)
	END 
	ELSE IF (@node_type = 2 AND @node_behavior = 3 )
	BEGIN
	-- If there is an asset defined and it is a descendant of the specified node and the 
	-- parent node is an asset w descendantes node then we need to treat this as a root node and get all descendants
		INSERT INTO @results(AssetID)
		SELECT AssetID FROM Asset.ufnGetAssetIdsForUserStartingAt(@securityuserid, @assetID)
	END
	ELSE IF(Asset.ufnDoesUserHaveAccessToAsset(@assetID, @securityuserid, -1) = 1) 
	BEGIN
	-- If this is a descendant of anything else (probably an asset w children) then we just add this asset to the list
		INSERT INTO @results(AssetID) VALUES (@assetID)
	END

	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[UIConfig].[ufnGetAssetDescendantsFromNodeDescendant] TO [TEUser]
    AS [dbo];