﻿CREATE FUNCTION [UIConfig].[ufnGetAssetIdsAllowedForUserIdAndAppContextStartingAt]
(
   @securityuserid int,
   @appContextId int, 
   @startingAssetId int
)
RETURNS TABLE
AS
RETURN
(
select a.EndingAssetId AS AssetID from Asset.tAssetHop a join AccessControl.tAssetAccessRule b on a.HopAssetId = b.AssetID JOIN 
AccessControl.tUserRoleMap c on b.SecurityRoleID = c.SecurityRoleID 
JOIN Asset.tAsset d on a.EndingAssetId = d.AssetID
JOIN Asset.tAssetHop e on d.AssetID = e.EndingAssetId 

WHERE (c.SecurityUserID = @securityUserID) AND d.IsHidden = 0 AND (e.HopAssetID = @startingAssetID)
and not exists 
(
SELECT hop.EndingAssetId as AssetID
	FROM AccessControl.tRole tfr
	INNER JOIN AccessControl.tAssetAccessRule taar ON taar.securityroleid = tfr.SecurityRoleID
	INNER JOIN Asset.tAssetHop hop ON hop.HopAssetId = taar.AssetID JOIN Asset.tAsset ast on hop.EndingAssetId = ast.AssetID
	JOIN AccessControl.tAccessRule ar ON taar.SecurityRoleID = ar.SecurityRoleID
	JOIN UIConfig.tAppContext ap on ar.SecurityResourceID = ap.SecurityResourceID
	WHERE ap.AppContextID = @appcontextid and ar.Allowed = 0 and ast.IsHidden = 0 and ast.AssetID = a.EndingAssetId

)

GROUP BY a.EndingAssetId 
		HAVING 
		MIN(CAST(B.Allowed as INT)) = 1
)

GO


GRANT SELECT
    ON OBJECT::[UIConfig].[ufnGetAssetIdsAllowedForUserIdAndAppContextStartingAt] TO [TEUSER]
    AS [dbo];

GO