﻿
CREATE PROCEDURE [UIConfig].[spGetViewsForAsset]
	@AssetID int = 0,
	@SecurityUserID int,
	@AppContext NVARCHAR(255) = NULL
AS
DECLARE @a int = null;
DECLARE @c int = null;
DECLARE @sg int = null;
DECLARE @st int = null;
DECLARE @u int = null;
DECLARE @s int = null;
DECLARE @eq int = null;

SET @a = @AssetID;
SET @c = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@a, 'CLT');
SET @sg = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@a, 'SG');
SET @st = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@a, 'ST');
SET @u = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@a, 'UN');
SET @s = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@a, 'SYS');
SET @eq = aSSET.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(@a, 'EQU');

DECLARE @ancestry table(
	AssetID int
)
INSERT INTO @ancestry (AssetID) VALUES (@a);
INSERT INTO @ancestry (AssetID) VALUES (@c);
INSERT INTO @ancestry (AssetID) VALUES (@sg);
INSERT INTO @ancestry (AssetID) VALUES (@st);
INSERT INTO @ancestry (AssetID) VALUES (@u);
INSERT INTO @ancestry (AssetID) VALUES (@s);
INSERT INTO @ancestry (AssetID) VALUES (@eq);

DECLARE @maps TABLE(
	AssetViewID int,
	Name nvarchar(255),
	AppContext nvarchar(255)
)

DECLARE @allowedRoleAssetViewIDs TABLE(
	AssetViewID int
)
INSERT INTO @allowedRoleAssetViewIDs (AssetViewID)
(
	SELECT UIConfig.tAssetAssetViewMap.AssetViewID
	FROM UIConfig.tAssetAssetViewMap
	INNER JOIN AccessControl.tRole 
	ON UIConfig.tAssetAssetViewMap.UserRoleID = AccessControl.tRole.SecurityRoleID
	INNER JOIN AccessControl.tUserRoleMap 
	ON AccessControl.tRole.SecurityRoleID = AccessControl.tUserRoleMap.SecurityRoleID
	WHERE AccessControl.tUserRoleMap.SecurityUserID = @SecurityUserID 
)
UNION
(
	SELECT UIConfig.tAssetAssetViewMap.AssetViewID
	FROM UIConfig.tAssetAssetViewMap
	WHERE UIConfig.tAssetAssetViewMap.UserRoleID IS NULL
)

DECLARE @appContexts TABLE(
	[AppContextID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Icon] [nvarchar](255) NOT NULL,
	[DisplayName] [nvarchar](255) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[SecurityResourceID] [int] NOT NULL,
	[Path] [nvarchar](255) NOT NULL ,
	[OpenInNew] [bit] NOT NULL,
	[StopAtLevel] [int] NOT NULL,
	[ShowFuture] [bit] NOT NULL,
	[TimeRange] [nvarchar](255) NULL,
	[TimeSelection] [nvarchar](255) NULL,
	[Locale] [nvarchar](255) NULL,
	[StartAsset] [int] NULL,
	[Refresh] [int] NULL
)

INSERT INTO @appContexts
EXEC UIConfig.spGetAppContexts @SecurityUserID

DECLARE @allowedAppContextAssetViewIDs TABLE(
	AssetViewID int
)
INSERT INTO @allowedAppContextAssetViewIDs (AssetViewID)
SELECT UIConfig.tAssetAssetViewMap.AssetViewID
FROM UIConfig.tAssetAssetViewMap
WHERE (@AppContext IS NULL OR 
	  UIConfig.tAssetAssetViewMap.AppContext IS NULL OR 
	  UIConfig.tAssetAssetViewMap.AppContext = @AppContext)
	  AND (
	  UIConfig.tAssetAssetViewMap.AppContextName IS NULL OR 
	  UIConfig.tAssetAssetViewMap.AppContextName IN (SELECT Name FROM @appContexts)
	  )

-- (A) views for the specific asset
INSERT INTO @maps (AssetViewID, Name, AppContext)
SELECT map.AssetViewID, v.Name, map.AppContext
FROM UIConfig.tAssetAssetViewMap map
JOIN UIConfig.tAssetView v ON map.AssetViewID = v.AssetViewID
WHERE map.AssetID = @AssetID

-- (B) views for the specific asset's ac type unless they're overriden by something in (A)
INSERT INTO @maps (AssetViewID, Name, AppContext)
SELECT map.AssetViewID, v.Name, map.AppContext
FROM UIConfig.tAssetAssetViewMap map
JOIN Asset.tAssetClassType actype ON map.AssetClassTypeID = actype.AssetClassTypeID
JOIN Asset.tAsset ast ON actype.AssetClassTypeID = ast.AssetClassTypeID
JOIN UIConfig.tAssetView v ON map.AssetViewID = v.AssetViewID
WHERE
	ast.AssetID = @AssetID
	AND NOT EXISTS(SELECT AssetViewID FROM @maps maps WHERE (v.Name = maps.Name AND map.AppContext = maps.AppContext))


-- (C) views for the specified asset's ancestry ac types unless they're overriden by something in (A) or (B)
INSERT INTO @maps (AssetViewID, Name, AppContext)
SELECT map.AssetViewID, v.Name, map.AppContext
FROM UIConfig.tAssetAssetViewMap map
JOIN Asset.tAssetClassType actype ON map.AncestorAssetClassTypeID = actype.AssetClassTypeID
JOIN Asset.tAsset ast ON actype.AssetClassTypeID = ast.AssetClassTypeID
JOIN @ancestry anc ON anc.AssetID = ast.AssetID
JOIN UIConfig.tAssetView v ON map.AssetViewID = v.AssetViewID
WHERE NOT EXISTS(SELECT AssetViewID FROM @maps maps WHERE (v.Name = maps.Name AND map.AppContext = maps.AppContext))

-- (D) views for the specified asset's asset type unless they're overriden by something in (A - C)
INSERT INTO @maps (AssetViewID, Name, AppContext)
SELECT map.AssetViewID, v.Name, map.AppContext
FROM UIConfig.tAssetAssetViewMap map
JOIN Asset.tAssetType atype ON map.AssetTypeID = atype.AssetTypeID
JOIN Asset.tAssetClassType actype ON atype.AssetTypeID = actype.AssetTypeID
JOIN Asset.tAsset ast ON actype.AssetClassTypeID = ast.AssetClassTypeID
JOIN UIConfig.tAssetView v ON map.AssetViewID = v.AssetViewID
WHERE
	ast.AssetID = @AssetID
	AND NOT EXISTS(SELECT AssetViewID FROM @maps maps WHERE (v.Name = maps.Name AND map.AppContext = maps.AppContext))

-- (E) default views unless they're overriden by something in (A - D)
INSERT INTO @maps (AssetViewID, Name, AppContext)
SELECT map.AssetViewID, v.Name, map.AppContext
FROM UIConfig.tAssetAssetViewMap map
JOIN UIConfig.tAssetView v ON map.AssetViewID = v.AssetViewID
WHERE
	map.AssetID IS NULL
	AND map.AssetClassTypeID IS NULL
	AND map.AssetTypeID IS NULL
	AND map.AncestorAssetClassTypeID IS NULL
	AND NOT EXISTS(SELECT AssetViewID FROM @maps maps WHERE (v.Name = maps.Name AND map.AppContext = maps.AppContext))


SELECT DISTINCT
	v.AssetViewID
	, v.Name
	, v.[Path]
	, v.DisplayOrder
	, v.[StopAtLevel]
	, v.[ShowFuture]
	, v.[TimeRange]
	, v.[Refresh]
FROM @maps maps
JOIN UIConfig.tAssetView v ON maps.AssetViewID = v.AssetViewID
JOIN @allowedRoleAssetViewIDs ro ON maps.AssetViewID = ro.AssetViewID
JOIN @allowedAppContextAssetViewIDs ap ON maps.AssetViewID = ap.AssetViewID

RETURN 0

GO
GRANT EXECUTE
    ON OBJECT::[UIConfig].[spGetViewsForAsset] TO [TEUser]
    AS [dbo];

