﻿CREATE PROCEDURE [UIConfig].[spGetAllNodesForAdHocTree]
	-- Add the parameters for the stored procedure here
	@securityuserid int,
	@treeGuid uniqueidentifier	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	[UIConfig].[tAdHocNode].[NodeId] AS NodeId, 
	[UIConfig].[tAdHocNode].[TreeId] AS TreeId, 
	[UIConfig].[tAdHocNode].[NodeAbbrev] AS NodeAbbrev, 
	[UIConfig].[tAdHocNode].[NodeDesc] AS NodeDesc, 
	[UIConfig].[tAdHocNode].[ParentNodeId] AS ParentNodeId, 
	[UIConfig].[tAdHocNode].[NodeTypeId] AS NodeTypeId, 
	[UIConfig].[tAdHocNode].[DisplayOrder] AS DisplayOrder, 
	[UIConfig].[tAdHocNode].[CreateDate] AS CreateDate, 
	[UIConfig].[tAdHocNode].[ChangeDate] AS ChangeDate, 
	[UIConfig].[tAdHocNode].[AssetId] AS AssetId, 
	[UIConfig].[tAdHocNode].[AssetNodeBehaviorId] AS AssetNodeBehaviorId, 
	[UIConfig].[tAdHocNode].[CriteriaObjectId] AS CriteriaObjectId, 
	[UIConfig].[tAdHocNode].[CoNodeBehaviorId] AS CoNodeBehaviorId,
	createdBy.UserName as CreatedBy, 
	changedBy.UserName as ChangedBy, 
	ta.AssetID as AssetAssetID,
	ta.ParentAssetID as AssetParentAssetID,
	ta.AssetAbbrev as AssetAssetAbbrev,
	ta.AssetDesc as AssetAssetDesc,
	ta.AssetClassTypeID as AssetAssetClassTypeID,
	ta.AssetTreeNodeChildConfigurationTypeID as AssetAssetTreeNodeChildConfigurationTypeID,
	ta.DisplayOrder as AssetDisplayOrder,
	ta.CreatedBy as AssetCreatedBy,
	ta.ChangedBy as AssetChangedBy,
	ta.CreateDate as AssetCreateDate,
	ta.ChangeDate as AssetChangeDate,
	ta.GlobalID as AssetGlobalID,
	ta.IsHidden as AssetIsHidden,
	ta.Track as AssetTrack,
	tc.CoID as CriteriaCoID,
	tc.GlobalID as CriteriaGlobalID,
	tc.CoTitle as CriteriaCoTitle,
	tc.CoDescription as CriteriaCoDescription,
	coCreatedBy.UserName as CriteriaCreatedBy,
	coChangedBy.UserName as CriteriaChangedBy,
	tc.CreateDate as CriteriaCreateDate,
	tc.ChangeDate as CriteriaChangeDate,
	tree.IsDefaultTree as CameFromDefaultTree,
	CASE 
		WHEN [UIConfig].[tAdHocNode].[NodeTypeId] = 3 THEN 1 
     WHEN [UIConfig].[tAdHocNode].[NodeTypeId] = 2 THEN 
         CASE WHEN [UIConfig].[tAdHocNode].[AssetNodeBehaviorId] = 1 THEN 0 
         ELSE count(children.assetId)
         END
		ELSE 0
		END 
	AS ChildCount, 
	Count(childNode.NodeId) as ContainerChildrenCount,
	case when [UIConfig].[tAdHocNode].[AssetId] is null then 1 else  asset.ufnDoesUserHaveAccessToAsset([UIConfig].[tAdHocNode].[AssetId], @securityuserid, -1) end as HasRightsToRootNode
INTO #BKB_TEMP_NODES
FROM [UIConfig].[tAdHocNode] 
INNER JOIN AccessControl.tUser createdBy ON createdBy.SecurityUserId = UIConfig.tAdHocNode.CreatedBy
INNER JOIN AccessControl.tUser changedBy ON changedBy.SecurityUserId = UIConfig.tAdHocNode.ChangedBy
LEFT JOIN [UIConfig].[tAdHocNode] childNode ON childNode.ParentNodeId = [UIConfig].[tAdHocNode].NodeId
LEFT JOIN Asset.tAsset ta ON UIConfig.tAdHocNode.AssetId = ta.AssetID
LEFT JOIN Criteria.tCo tc ON [UIConfig].[tAdHocNode].[NodeTypeId] = 3 AND UIConfig.tAdHocNode.CriteriaObjectId = tc.CoID
INNER JOIN AccessControl.tUser coCreatedBy ON coCreatedBy.SecurityUserId = UIConfig.tAdHocNode.CreatedBy
INNER JOIN AccessControl.tUser coChangedBy ON coChangedBy.SecurityUserId = UIConfig.tAdHocNode.ChangedBy
LEFT JOIN Asset.tAsset children ON ta.AssetID = children.ParentAssetID AND children.IsHidden = 0 
LEFT JOIN UIConfig.tAdHocTree tree ON UIConfig.tAdHocNode.TreeId = tree.TreeId  
--AND (asset.ufnDoesUserHaveAccessToAsset(children.[AssetId], @securityUserId, -1) = 1)

WHERE 
ISNULL(ta.IsHidden,0)=0	

AND [UIConfig].[tAdHocNode].[treeID] = @treeGuid
GROUP BY [UIConfig].[tAdHocNode].[NodeId], 
	[UIConfig].[tAdHocNode].[TreeId], 
	[UIConfig].[tAdHocNode].[NodeAbbrev], 
	[UIConfig].[tAdHocNode].[NodeDesc], 
	[UIConfig].[tAdHocNode].[ParentNodeId], 
	[UIConfig].[tAdHocNode].[NodeTypeId], 
	[UIConfig].[tAdHocNode].[DisplayOrder], 
	[UIConfig].[tAdHocNode].[CreatedBy], 
	[UIConfig].[tAdHocNode].[ChangedBy], 
	[UIConfig].[tAdHocNode].[CreateDate], 
	[UIConfig].[tAdHocNode].[ChangeDate], 
	[UIConfig].[tAdHocNode].[AssetId], 
	[UIConfig].[tAdHocNode].[AssetNodeBehaviorId], 
	[UIConfig].[tAdHocNode].[CriteriaObjectId], 
	[UIConfig].[tAdHocNode].[CoNodeBehaviorId], 
	createdBy.UserName, 
	changedBy.UserName, 
	ta.AssetID,
	ta.ParentAssetID,
	ta.AssetAbbrev,
	ta.AssetDesc,
	ta.AssetClassTypeID,
	ta.AssetTreeNodeChildConfigurationTypeID,
	ta.DisplayOrder,
	ta.CreatedBy,
	ta.ChangedBy,
	ta.CreateDate,
	ta.ChangeDate,
	ta.GlobalID,
	ta.IsHidden,
	ta.Track,
	tc.CoID,
	tc.GlobalID,
	tc.CoTitle,
	tc.CoDescription,
	coCreatedBy.UserName,
	coChangedBy.UserName,
	tc.CreateDate,
	tc.ChangeDate,
	tree.IsDefaultTree
ORDER BY ParentNodeId, DisplayOrder, NodeAbbrev

SELECT * INTO #BKB_TEMP_NODES2 FROM #BKB_TEMP_NODES WHERE 1 = 0

if exists (select * from #BKB_TEMP_NODES WHERE HasRightsToRootNode=0 and NodeTypeId = 2 And AssetNodeBehaviorId = 3)
  BEGIN
  declare @processed as table (rootassetid int, assetid int null, processed bit null)
  insert into @processed select assetid,assetid,0 from #BKB_TEMP_NODES WHERE HasRightsToRootNode = 0 And NodeTypeId = 2 and AssetNodeBehaviorId = 3
  --select * from @processed
  --select * from Asset.tAsset where 
  declare @assetids as base.tpintlist
  insert into @assetids 
  select distinct c.assetid as id from AccessControl.tUserRoleMap a join AccessControl.tRole b on a.SecurityRoleID = b.SecurityRoleID 
  join AccessControl.tAssetAccessRule c on b.SecurityRoleID = c.SecurityRoleID   
  where c.Allowed = 1 and a.SecurityUserID = @securityuserid    
  
  insert INTO #BKB_TEMP_NODES2
  SELECT 
  d.GlobalID as NodeId,e.TreeId,d.AssetAbbrev as NodeAbbrev,d.AssetDesc as NodeDesc,e.ParentNodeId,e.NodeTypeId,d.DisplayOrder,
  d.CreateDate,d.ChangeDate,a.id as AssetId,
  e.AssetNodeBehaviorId,e.CriteriaObjectId,e.CoNodeBehaviorId,e.CreatedBy,e.ChangedBy,d.AssetID as AssetAssetID,null as AssetParentAssetID,
  d.AssetAbbrev as AssetAssetAbbrev, d.AssetDesc as AssetAssetDesc,d.AssetClassTypeID as AssetAssetClassTypeID,d.AssetTreeNodeChildConfigurationTypeID as 
  AssetAssetTreeNodeChildConfigurationTypeID, d.DisplayOrder as AssetDisplayOrder, d.CreatedBy as AssetCreatedBy, 
  d.ChangedBy as AssetChangedBy, d.CreateDate as AssetCreateDate, d.ChangeDate as AssetChangeDate,
  d.GlobalID as AssetGlobalID, d.IsHidden as AssetIsHidden, d.Track as AssetTrack, e.CriteriaCoID, e.CriteriaGlobalID, 
  e.CriteriaCoTitle, e.CriteriaCoDescription, e.CriteriaCreatedBy, e.CriteriaChangedBy, e.CriteriaCreateDate, e.CriteriaChangeDate, e.CameFromDefaultTree,
  e.ChildCount,e.ContainerChildrenCount, 1 as HasRightsToRootNode
   
   from @assetids a cross apply asset.ufnAssetAncestorsWithLevel(a.id)  b join @processed c on b.AssetID = c.rootassetid    
  join Asset.tAsset d on a.id = d.AssetID
  join #BKB_TEMP_NODES e on c.rootassetid = e.assetid WHERE HasRightsToRootNode=0 and NodeTypeId = 2 And AssetNodeBehaviorId = 3
  update a set a.ChildCount = b.ChildCount from 
  #BKB_TEMP_NODES2 a JOIN 
  (
  select b.ParentAssetID,COUNT(*) as childcount from #BKB_TEMP_NODES2 a join Asset.tAsset b on a.AssetId = b.ParentAssetID   
  WHERE b.IsHidden = 0 and Asset.ufnDoesUserHaveAccessToAsset(b.AssetID,@securityuserid,-1)=1
  GROUP BY b.ParentAssetID 
  ) b ON a.AssetID = b.ParentAssetID

  END

SELECT * FROM #BKB_TEMP_NODES WHERE HasRightsToRootNode = 1
UNION
SELECT * FROM #BKB_TEMP_NODES2 WHERE HasRightsToRootNode = 1
ORDER BY ParentNodeId, DisplayOrder, NodeAbbrev

DROP TABLE #BKB_TEMP_NODES
DROP TABLE #BKB_TEMP_NODES2

END
GO


GRANT EXECUTE
	ON OBJECT::[UIConfig].[spGetAllNodesForAdHocTree]  TO [TEUser]
	AS [dbo];
