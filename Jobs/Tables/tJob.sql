﻿CREATE TABLE [Jobs].[tJob] (
    [JobID]      INT              IDENTITY (1, 1) NOT NULL,
    [JobExtId]   UNIQUEIDENTIFIER NOT NULL,
    [JobTypeId]  INT              NOT NULL,
    [JobName]    VARCHAR (MAX)    NULL,
    [Record]     VARCHAR (MAX)    NULL,
    [UserId]     INT              NOT NULL,
    [Success]    BIT              CONSTRAINT [DF_tJob_Success] DEFAULT ((0)) NOT NULL,
    [Complete]   BIT              CONSTRAINT [DF_tJob_Complete] DEFAULT ((0)) NOT NULL,
    [OpenedDate] DATETIME         CONSTRAINT [DF_tJob_OpenedDate] DEFAULT (getdate()) NULL,
    [ClosedDate] DATETIME         NULL,
    CONSTRAINT [PK_tJob] PRIMARY KEY CLUSTERED ([JobExtId] ASC),
    CONSTRAINT [FK_tJob_tJobType_JobTypeId] FOREIGN KEY ([JobTypeId]) REFERENCES [Jobs].[tJobType] ([JobTypeId])
);



