﻿CREATE TABLE [Jobs].[tJobType] (
    [JobTypeId]     INT           NOT NULL,
    [JobTypeAbbrev] VARCHAR (50)  NOT NULL,
    [JobTypeDesc]   VARCHAR (255) NOT NULL,
    [DisplayOrder]  INT           NOT NULL,
    CONSTRAINT [PK_tJobType] PRIMARY KEY CLUSTERED ([JobTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Jobs', @level1type = N'TABLE', @level1name = N'tJobType';

