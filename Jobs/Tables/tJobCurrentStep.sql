﻿CREATE TABLE [Jobs].[tJobCurrentStep] (
    [JobExtId]         UNIQUEIDENTIFIER NOT NULL,
    [StepOrder]        INT              NOT NULL,
    [StepName]         VARCHAR (MAX)    NULL,
    [WorkerId]         VARCHAR (MAX)    NULL,
    [Success]          BIT              CONSTRAINT [DF_tJobCurrentStep_Success] DEFAULT ((0)) NOT NULL,
    [Complete]         BIT              CONSTRAINT [DF_tJobCurrentStep_Complete] DEFAULT ((0)) NOT NULL,
    [LastStatusTypeId] INT              NULL,
    [LastStatus]       VARCHAR (MAX)    NULL,
    [OpenedDate]       DATETIME         CONSTRAINT [DF_tJobCurrentStep_OpenedDate] DEFAULT (getdate()) NULL,
    [ClosedDate]       DATETIME         NULL,
    CONSTRAINT [PK_tJobCurrentStep] PRIMARY KEY CLUSTERED ([JobExtId] ASC),
    CONSTRAINT [FK_tJobCurrentStep_tJob_JobExtId] FOREIGN KEY ([JobExtId]) REFERENCES [Jobs].[tJob] ([JobExtId]) ON DELETE CASCADE
);



