﻿CREATE TYPE [Jobs].[tpJobCurrentStep] AS TABLE (
    [JobExtId]         UNIQUEIDENTIFIER NOT NULL,
    [StepOrder]        INT              NOT NULL,
    [StepName]         VARCHAR (MAX)    NULL,
    [WorkerId]         VARCHAR (MAX)    NULL,
    [Success]          BIT              NOT NULL,
    [Complete]         BIT              NOT NULL,
    [LastStatusTypeId] INT              NULL,
    [LastStatus]       VARCHAR (MAX)    NULL,
    [OpenedDate]       DATETIME         NULL,
    [ClosedDate]       DATETIME         NULL);




GO
GRANT EXECUTE
    ON TYPE::[Jobs].[tpJobCurrentStep] TO [TEUser];

