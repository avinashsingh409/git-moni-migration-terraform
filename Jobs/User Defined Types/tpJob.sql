﻿CREATE TYPE [Jobs].[tpJob] AS TABLE (
    [JobID]      INT              NULL,
    [JobExtId]   UNIQUEIDENTIFIER NOT NULL,
    [JobTypeId]  INT              NOT NULL,
    [JobName]    VARCHAR (MAX)    NULL,
    [Record]     VARCHAR (MAX)    NULL,
    [UserId]     INT              NOT NULL,
    [Success]    BIT              NOT NULL,
    [Complete]   BIT              NOT NULL,
    [OpenedDate] DATETIME         NULL,
    [ClosedDate] DATETIME         NULL);




GO
GRANT EXECUTE
    ON TYPE::[Jobs].[tpJob] TO [TEUser];

