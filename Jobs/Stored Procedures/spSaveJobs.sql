﻿



CREATE PROCEDURE [Jobs].[spSaveJobs]
	@tblJobs Jobs.tpJob READONLY,
	@UserId int,
	@SavedJobsCount int output
AS
BEGIN

	SET NOCOUNT ON;

	declare @unownedExistingCount int = 0;
	set @unownedExistingCount = (select count (*) from Jobs.tJob b join @tblJobs a on b.JobExtId = a.JobExtId where b.UserId <> @UserId);
	if @unownedExistingCount <> 0
	begin
		declare @msg varchar(255) = 'User does not have permission to update ' + cast(@unownedExistingCount as varchar(max)) + ' jobs';
		raiserror (@msg, 11, -1, -1);
		return
	end

	declare @updated int = 0;
	declare @inserted int = 0;
	set @SavedJobsCount = 0;

	update b
	set
		b.JobTypeId = a.JobTypeId,
		b.JobName = a.JobName,
		b.Record = a.Record,
		b.UserId = @UserId,
		b.Success = a.Success,
		b.Complete = a.Complete,
		b.OpenedDate = a.OpenedDate,
		b.ClosedDate = a.ClosedDate
	from @tblJobs a join Jobs.tJob b on a.JobExtId = b.JobExtId;
	set @updated = @@ROWCOUNT;

	insert into Jobs.tJob (JobExtId, JobTypeId, JobName, Record, UserId, Success, Complete, OpenedDate, ClosedDate)
	select j.JobExtId, j.JobTypeId, j.JobName, j.Record, @UserId, j.Success, j.Complete, j.OpenedDate, j.ClosedDate
	from @tblJobs j where j.JobExtId not in (select JobExtId from Jobs.tJob);
	set @inserted = @@ROWCOUNT;

	set @SavedJobsCount = @updated + @inserted;

END
GO
GRANT EXECUTE
    ON OBJECT::[Jobs].[spSaveJobs] TO [TEUser]
    AS [dbo];

