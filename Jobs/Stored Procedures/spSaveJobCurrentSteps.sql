﻿


CREATE PROCEDURE [Jobs].[spSaveJobCurrentSteps]
	@tblSteps Jobs.tpJobCurrentStep READONLY,
	@UserId int,
	@SavedStepsCount int output
AS
BEGIN

	SET NOCOUNT ON;

	declare @unownedExistingCount int = 0;
	set @unownedExistingCount = (select count (*) from Jobs.tJobCurrentStep s join Jobs.tJob b on s.JobExtId = b.JobExtId join @tblSteps a on s.JobExtId = a.JobExtId where b.UserId <> @UserId);
	if @unownedExistingCount <> 0
	begin
		declare @msg varchar(255) = 'User does not have permission to update ' + cast(@unownedExistingCount as varchar(max)) + ' job steps';
		raiserror (@msg, 11, -1, -1);
		return
	end

	declare @updated int = 0;
	declare @inserted int = 0;
	set @SavedStepsCount = 0;

	update b
	set
		b.StepOrder = a.StepOrder,
		b.StepName = a.StepName,
		b.WorkerId = a.WorkerId,
		b.Success = a.Success,
		b.Complete = a.Complete,
		b.LastStatusTypeId = a.LastStatusTypeId,
		b.LastStatus = a.LastStatus,
		b.OpenedDate = a.OpenedDate,
		b.ClosedDate = a.ClosedDate
	from @tblSteps a join Jobs.tJobCurrentStep b on a.JobExtId = b.JobExtId;
	set @updated = @@ROWCOUNT;

	insert into Jobs.tJobCurrentStep (JobExtId, StepOrder, StepName, WorkerId, Success, Complete, LastStatusTypeId, LastStatus, OpenedDate, ClosedDate)
	select j.JobExtId, j.StepOrder, j.StepName, j.WorkerId, j.Success, j.Complete, j.LastStatusTypeId, j.LastStatus, j.OpenedDate, j.ClosedDate
	from @tblSteps j where j.JobExtId not in (select JobExtId from Jobs.tJobCurrentStep);
	set @inserted = @@ROWCOUNT;

	set @SavedStepsCount = @updated + @inserted;

END
GO
GRANT EXECUTE
    ON OBJECT::[Jobs].[spSaveJobCurrentSteps] TO [TEUser]
    AS [dbo];

