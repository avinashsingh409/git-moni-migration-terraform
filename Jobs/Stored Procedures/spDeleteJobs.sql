﻿




CREATE PROCEDURE [Jobs].[spDeleteJobs]
	@tblJobs Base.tpGuidList READONLY,
	@UserId int,
	@DeletedCount int output
AS
BEGIN

	SET NOCOUNT ON;

	declare @unownedExistingCount int = 0;
	set @unownedExistingCount = (select count (*) from Jobs.tJob b join @tblJobs a on b.JobExtId = a.id where b.UserId <> @UserId);
	if @unownedExistingCount <> 0
	begin
		declare @msg varchar(255) = 'User does not have permission to delete ' + cast(@unownedExistingCount as varchar(max)) + ' jobs';
		raiserror (@msg, 11, -1, -1);
		return
	end

	delete from a
	from Jobs.tJob a
	join @tblJobs b on a.JobExtId = b.id;
	set @DeletedCount = @@ROWCOUNT;

END
GO
GRANT EXECUTE
    ON OBJECT::[Jobs].[spDeleteJobs] TO [TEUser]
    AS [dbo];

