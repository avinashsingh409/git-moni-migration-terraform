﻿




CREATE PROCEDURE [Jobs].[spDeleteJobCurrentSteps]
	@tblSteps Base.tpGuidList READONLY,
	@UserId int,
	@DeletedCount int output
AS
BEGIN

	SET NOCOUNT ON;

	declare @unownedExistingCount int = 0;
	set @unownedExistingCount = (select count (*) from Jobs.tJobCurrentStep s join Jobs.tJob b on s.JobExtId = b.JobExtId join @tblSteps a on s.JobExtId = a.id where b.UserId <> @UserId);
	if @unownedExistingCount <> 0
	begin
		declare @msg varchar(255) = 'User does not have permission to delete ' + cast(@unownedExistingCount as varchar(max)) + ' job steps';
		raiserror (@msg, 11, -1, -1);
		return
	end

	delete from a
	from Jobs.tJobCurrentStep a
	join @tblSteps b on a.JobExtId = b.id
	join Jobs.tJob c on a.JobExtId = c.JobExtId;
	set @DeletedCount = @@ROWCOUNT;

END
GO
GRANT EXECUTE
    ON OBJECT::[Jobs].[spDeleteJobCurrentSteps] TO [TEUser]
    AS [dbo];

