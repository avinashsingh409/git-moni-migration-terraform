﻿
--Alter [Criteria].[spDeleteDS] to delete mapping in Criteria.tCo
CREATE PROCEDURE [Criteria].[spDeleteDS]
	@SecurityUserID INT
	, @CoDSID INT
	, @DeleteCount INT OUTPUT
AS
BEGIN

	DECLARE @AssetID int = NULL;
	DECLARE @IsPrivate bit = NULL;
	SELECT @AssetID = AssetID, @IsPrivate = IsPrivate FROM Criteria.tCoDS WHERE CoDSID = @CoDSID;

	IF @AssetID IS NOT NULL AND @IsPrivate = 1
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
		BEGIN
			SET @DeleteCount = -1;
			RAISERROR( 'User cannot access asset',16, 1);
		END
	END
	UPDATE Criteria.tCo Set CoDSID = NULL WHERE CoDSID = @CoDSID;
	SET NOCOUNT ON;
	DELETE FROM Criteria.tCoDS WHERE CoDSID = @CoDSID;
	SET @DeleteCount = @@ROWCOUNT;

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spDeleteDS] TO [TEUser]
    AS [dbo];

