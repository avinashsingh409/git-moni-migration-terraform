﻿

--Alter [Criteria].[spDeleteDA] to delete mapping in Criteria.tCo
CREATE PROCEDURE [Criteria].[spDeleteDA]
	@SecurityUserID INT
	, @CoDAID INT
	, @DeleteCount INT OUTPUT
AS
BEGIN


	DECLARE @AssetID int = NULL;
	DECLARE @IsPrivate bit = NULL;
	SELECT @AssetID = AssetID, @IsPrivate = IsPrivate FROM Criteria.tCoDA WHERE CoDAID = @CoDAID;

	IF @AssetID IS NOT NULL AND @IsPrivate = 1
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
		BEGIN
			SET @DeleteCount = -1;
			RAISERROR( 'User cannot access asset',16, 1);
		END
	END
	UPDATE Criteria.tCo Set CoDAID = NULL WHERE CoDAID = @CoDAID;
	SET NOCOUNT ON;
	DELETE FROM Criteria.tCoDA WHERE CoDAID = @CoDAID;
	SET @DeleteCount = @@ROWCOUNT;

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spDeleteDA] TO [TEUser]
    AS [dbo];

