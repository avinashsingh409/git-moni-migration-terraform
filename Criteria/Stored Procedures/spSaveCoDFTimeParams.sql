﻿


CREATE PROCEDURE [Criteria].[spSaveCoDFTimeParams]
	@SecurityUserID INT
	, @TimeFilterID INT
    , @tblParams Base.tpKVP READONLY
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [Criteria].[tCoDFTimeParams] WHERE CoDFTimeFilterID = @TimeFilterID

	INSERT INTO Criteria.tCoDFTimeParams (CoDFTimeFilterID, PName, PValue)
	SELECT  @TimeFilterID, pkey, pvalue FROM @tblParams              

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spSaveCoDFTimeParams] TO [TEUser]
    AS [dbo];

