﻿

--Alter [Criteria].[spDeleteCriteria] to delete mapping in Criteria.tCoCoMap
CREATE PROCEDURE [Criteria].[spDeleteCriteria]
	@SecurityUserID INT
	, @CoID INT
	, @DeleteCount INT OUTPUT
AS
BEGIN
	DELETE FROM Criteria.tCoCoMap wHERE ContainerCoID = @CoID;
	DELETE FROM Criteria.tCoCoMap wHERE CoID = @CoID;
	SET NOCOUNT ON;

	DELETE FROM Criteria.tCo WHERE CoID = @CoID;
	SET @DeleteCount = @@ROWCOUNT;

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spDeleteCriteria] TO [TEUser]
    AS [dbo];

