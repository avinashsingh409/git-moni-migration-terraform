﻿


CREATE PROCEDURE [Criteria].[spDeleteDFTimeFilter]
	@SecurityUserID INT
	, @CoDFTimeFilterID INT
	, @DeleteCount INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM Criteria.tCoDFTimeFilter WHERE CoDFTimeFilterID = @CoDFTimeFilterID;
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spDeleteDFTimeFilter] TO [TEUser]
    AS [dbo];

