﻿

CREATE PROCEDURE [Criteria].[spSaveDS]
	@SecurityUserID INT
	, @CoDSID INT = NULL
	, @CoDSTitle NVARCHAR(255)
	, @CoDSDesc NVARCHAR(MAX) = NULL
	, @IsPrivate bit
	, @AssetID INT = NULL
	, @CoDSSourceTypeID INT
	, @SearchString VARCHAR(MAX)
	, @SearchStringCodecVersion nvarchar(50)
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @AssetID IS NOT NULL AND @IsPrivate = 1
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
		BEGIN
			RAISERROR( 'User cannot access asset',16, 1);
		END
	END

	IF EXISTS (SELECT * FROM Criteria.tCoDS WHERE CoDSID = @CoDSID)
	BEGIN
		UPDATE Criteria.tCoDS
		SET
			CoDSTitle = @CoDSTitle
			, CoDSDescription = @CoDSDesc
			, IsPrivate = @IsPrivate
			, AssetID = @AssetID
			, ChangedByUserID = @SecurityUserID
			, ChangeDate = GETDATE()
			, CoDSSourceTypeID = @CoDSSourceTypeID
			, SearchString = @SearchString
			, SearchStringCodecVersion = @SearchStringCodecVersion
		WHERE Criteria.tCoDS.CoDSID = @CoDSID 
		SET @NewID = @CoDSID;
	END
	ELSE
	BEGIN
		INSERT INTO Criteria.tCoDS (CoDSTitle, CoDSDescription, IsPrivate, AssetID, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate, CoDSSourceTypeID, SearchString, SearchStringCodecVersion)
		VALUES (@CoDSTitle, @CoDSDesc, @IsPrivate, @AssetID, @SecurityUserID, @SecurityUserID, GETDATE(), GETDATE(), @CoDSSourceTypeID, @SearchString, @SearchStringCodecVersion);
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spSaveDS] TO [TEUser]
    AS [dbo];

