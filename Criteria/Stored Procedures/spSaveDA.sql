﻿

CREATE PROCEDURE [Criteria].[spSaveDA]
	@SecurityUserID INT
	, @CoDAID INT = NULL
	, @CoDATitle NVARCHAR(255)
	, @CoDADesc NVARCHAR(MAX) = NULL
	, @IsPrivate bit
	, @AssetID INT = NULL
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @AssetID IS NOT NULL AND @IsPrivate = 1
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
		BEGIN
			RAISERROR( 'User cannot access asset',16, 1);
		END
	END

	IF EXISTS (SELECT * FROM Criteria.tCoDA WHERE CoDAID = @CoDAID)
	BEGIN
		UPDATE Criteria.tCoDA
		SET
			CoDATitle = @CoDATitle
			, CoDADescription = @CoDADesc
			, IsPrivate = @IsPrivate
			, AssetID = @AssetID
			, ChangedByUserID = @SecurityUserID
			, ChangeDate = GETDATE()
		WHERE Criteria.tCoDA.CoDAID = @CoDAID 
		SET @NewID = @CoDAID;
	END
	ELSE
	BEGIN
		INSERT INTO Criteria.tCoDA (CoDATitle, CoDADescription, IsPrivate, AssetID, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
		VALUES (@CoDATitle, @CoDADesc, @IsPrivate, @AssetID, @SecurityUserID, @SecurityUserID, GETDATE(), GETDATE());
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spSaveDA] TO [TEUser]
    AS [dbo];

