﻿
--Update store procedure for [spSaveCriteria]
CREATE PROCEDURE [Criteria].[spSaveCriteria]
	@SecurityUserID INT
	, @CoID INT = NULL
	, @CoTitle NVARCHAR(255)
	, @CoDesc NVARCHAR(MAX) = NULL
	, @CoDSID INT = NULL
	, @CoDFID INT = NULL
	, @CoDAID INT = NULL
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM Criteria.tCo WHERE CoID = @CoID)
	BEGIN
		UPDATE Criteria.tCo
		SET
			CoTitle = @CoTitle
			, CoDescription = @CoDesc
			, ChangedByUserID = @SecurityUserID
			, ChangeDate = GETDATE()
			, CoDSID = @CoDSID
			, CoDFID = @CoDFID
			, CoDAID = @CoDAID
		WHERE Criteria.tCo.CoID = @CoID 
		SET @NewID = @CoID;
	END
	ELSE
	BEGIN
		INSERT INTO Criteria.tCo (CoTitle, CoDescription, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate,CoDSID,CoDFID,CoDAID)
		VALUES (@CoTitle, @CoDesc, @SecurityUserID, @SecurityUserID, GETDATE(), GETDATE(),@CoDSID,@CoDFID,@CoDAID);
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spSaveCriteria] TO [TEUser]
    AS [dbo];

