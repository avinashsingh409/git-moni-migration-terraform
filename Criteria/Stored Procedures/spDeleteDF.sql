﻿

--Alter [Criteria].[spDeleteDF] to delete mapping in Criteria.tCo
CREATE PROCEDURE [Criteria].[spDeleteDF]
	@SecurityUserID INT
	, @CoDFID INT
	, @DeleteCount INT OUTPUT
AS
BEGIN

	DECLARE @AssetID int = NULL;
	DECLARE @IsPrivate bit = NULL;
	SELECT @AssetID = AssetID, @IsPrivate = IsPrivate FROM Criteria.tCoDF WHERE CoDFID = @CoDFID;

	IF @AssetID IS NOT NULL AND @IsPrivate = 1
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
		BEGIN
			SET @DeleteCount = -1;
			RAISERROR( 'User cannot access asset',16, 1);
		END
	END
	UPDATE Criteria.tCo Set CoDFID = NULL WHERE CoDFID = @CoDFID;
	SET NOCOUNT ON;
	DELETE FROM Criteria.tCoDF WHERE CoDFID = @CoDFID;
	SET @DeleteCount = @@ROWCOUNT;

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spDeleteDF] TO [TEUser]
    AS [dbo];

