﻿

CREATE PROCEDURE [Criteria].[spSaveDF]
	@SecurityUserID INT
	, @CoDFID INT = NULL
	, @CoDFTitle NVARCHAR(255)
	, @CoDFDesc NVARCHAR(MAX) = NULL
	, @IsPrivate bit
	, @AssetID INT = NULL
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @AssetID IS NOT NULL AND @IsPrivate = 1
	BEGIN
		IF Asset.ufnDoesUserHaveAccessToAsset(@AssetID, @SecurityUserID, -1) = 0
		BEGIN
			RAISERROR( 'User cannot access asset',16, 1);
		END
	END

	IF EXISTS (SELECT * FROM Criteria.tCoDF WHERE CoDFID = @CoDFID)
	BEGIN
		UPDATE Criteria.tCoDF
		SET
			CoDFTitle = @CoDFTitle
			, CoDFDescription = @CoDFDesc
			, IsPrivate = @IsPrivate
			, AssetID = @AssetID
			, ChangedByUserID = @SecurityUserID
			, ChangeDate = GETDATE()
		WHERE Criteria.tCoDF.CoDFID = @CoDFID 
		SET @NewID = @CoDFID;
	END
	ELSE
	BEGIN
		INSERT INTO Criteria.tCoDF (CoDFTitle, CoDFDescription, IsPrivate, AssetID, CreatedByUserID, ChangedByUserID, CreateDate, ChangeDate)
		VALUES (@CoDFTitle, @CoDFDesc, @IsPrivate, @AssetID, @SecurityUserID, @SecurityUserID, GETDATE(), GETDATE());
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spSaveDF] TO [TEUser]
    AS [dbo];

