﻿CREATE PROCEDURE [Criteria].[spGetCriteriaWithDataTimeFiltersAndParams] 
	@assetId INT,
	@includeGlobals BIT
AS
BEGIN
	
	DECLARE @ancestry TABLE(assetId int primary key);
	IF @assetId IS NOT NULL
	BEGIN
		INSERT INTO @ancestry(assetId)
		VALUES(@assetId);

		INSERT INTO @ancestry(assetId)
		SELECT assetid FROM [Asset].[ufnAssetAncestors] (@assetId)
	END

	IF @includeGlobals = 0
		BEGIN
			SELECT 	
				co.CoID 	
				, co.CoTitle 	
				, co.CoDescription 
				, co.CoDFID
				, ta.AssetID
				, ta.GlobalID AS AssetGuid
				, codftf.CoDFTimeFilterID
				, codftf.CoDFTimeFilterTypeID
				, codftp.CoDFTimeParamsID
				, codftp.PName
				, codftp.PValue
				, u.Username AS ChangedBy
				, codf.ChangeDate
			FROM Criteria.tCo co 	
			 INNER JOIN Criteria.tCoDF codf ON co.CoDFID = codf.CoDFID 	
			 INNER JOIN Criteria.tCoDFTimeFilter codftf ON codf.CoDFID = codftf.CoDFID 
			 INNER JOIN Criteria.tCoDFTimeParams codftp ON codftf.CoDFTimeFilterID = codftp.CoDFTimeFilterID
			 INNER JOIN @ancestry ancestry ON codf.AssetID = ancestry.AssetID
			 INNER JOIN Asset.tAsset ta ON codf.AssetID = ta.AssetID
			 INNER JOIN AccessControl.tUser u ON codf.ChangedByUserID = u.SecurityUserID
			 WHERE  co.CoID NOT IN (SELECT CriteriaID FROM UIConfig.tCategoryCriteria)
		END
	
	ELSE
	
		BEGIN
			SELECT 	
				co.CoID 	
				, co.CoTitle 	
				, co.CoDescription 
				, co.CoDFID
				, ta.AssetID
				, ta.GlobalID AS AssetGuid
				, codftf.CoDFTimeFilterID
				, codftf.CoDFTimeFilterTypeID
				, codftp.CoDFTimeParamsID
				, codftp.PName
				, codftp.PValue
				, u.Username AS ChangedBy
				, codf.ChangeDate
			FROM Criteria.tCo co 	
			 INNER JOIN Criteria.tCoDF codf ON co.CoDFID = codf.CoDFID 	
			 INNER JOIN Criteria.tCoDFTimeFilter codftf ON codf.CoDFID = codftf.CoDFID 
			 INNER JOIN Criteria.tCoDFTimeParams codftp ON codftf.CoDFTimeFilterID = codftp.CoDFTimeFilterID
			 LEFT JOIN Asset.tAsset ta ON codf.AssetID = ta.AssetID
			 INNER JOIN AccessControl.tUser u ON codf.ChangedByUserID = u.SecurityUserID
			 WHERE  co.CoID NOT IN (SELECT CriteriaID FROM UIConfig.tCategoryCriteria)
			 AND (codf.AssetID is null OR codf.AssetID IN (SELECT assetId FROM @ancestry))
		END

END
GO

GRANT EXECUTE
    ON OBJECT::[Criteria].[spGetCriteriaWithDataTimeFiltersAndParams] TO [TEUser]
    AS [dbo];
GO