﻿




CREATE PROCEDURE [Criteria].[spSaveDFTimeFilter]
	@SecurityUserID INT
	, @CoDFTimeFilterID INT = NULL
	, @CoDFID INT
	, @CoDFTimeFilterTypeID INT
	, @ExecuteOrder INT
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM Criteria.tCoDFTimeFilter WHERE CoDFTimeFilterID = @CoDFTimeFilterID)
	BEGIN
		UPDATE Criteria.tCoDFTimeFilter
		SET
			CoDFID = @CoDFID
			, CoDFTimeFilterTypeID = @CoDFTimeFilterTypeID
			, ExecuteOrder = @ExecuteOrder
		WHERE Criteria.tCoDFTimeFilter.CoDFTimeFilterID = @CoDFTimeFilterID 
		SET @NewID = @CoDFTimeFilterID;
	END
	ELSE
	BEGIN
		INSERT INTO Criteria.tCoDFTimeFilter (CoDFID, CoDFTimeFilterTypeID, ExecuteOrder)
		VALUES (@CoDFID, @CoDFTimeFilterTypeID, @ExecuteOrder);
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END
GO
GRANT EXECUTE
    ON OBJECT::[Criteria].[spSaveDFTimeFilter] TO [TEUser]
    AS [dbo];

