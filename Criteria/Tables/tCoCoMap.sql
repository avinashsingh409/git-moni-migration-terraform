﻿CREATE TABLE [Criteria].[tCoCoMap] (
    [CoCoMapID]     INT IDENTITY (1, 1) NOT NULL,
    [ContainerCoID] INT NOT NULL,
    [CoID]          INT NOT NULL,
    CONSTRAINT [PK_tCoCoMap] PRIMARY KEY CLUSTERED ([CoCoMapID] ASC),
    CONSTRAINT [FK_tCoCoMap_tCo] FOREIGN KEY ([CoID]) REFERENCES [Criteria].[tCo] ([CoID]),
    CONSTRAINT [FK_tCoCoMap_tCoContainer] FOREIGN KEY ([ContainerCoID]) REFERENCES [Criteria].[tCo] ([CoID])
);





