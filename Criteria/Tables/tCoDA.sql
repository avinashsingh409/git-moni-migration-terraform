﻿CREATE TABLE [Criteria].[tCoDA] (
    [CoDAID]          INT              IDENTITY (1, 1) NOT NULL,
    [GlobalID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tCoDA_GlobalID] DEFAULT (newid()) NOT NULL,
    [CoDATitle]       NVARCHAR (255)   NOT NULL,
    [CoDADescription] NVARCHAR (MAX)   NULL,
    [IsPrivate]       BIT              CONSTRAINT [DF_tCoDA_IsPrivate] DEFAULT ((1)) NOT NULL,
    [AssetID]         INT              NULL,
    [CreatedByUserID] INT              NOT NULL,
    [ChangedByUserID] INT              NOT NULL,
    [CreateDate]      DATETIME         CONSTRAINT [DF_tCoDA_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME         CONSTRAINT [DF_tCoDA_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tCoDA] PRIMARY KEY CLUSTERED ([CoDAID] ASC),
    CONSTRAINT [FK_tCoDA_ChangedByUserID_tUser_SecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCoDA_CreatedByUserID_tUser_SecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCoDA_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);

