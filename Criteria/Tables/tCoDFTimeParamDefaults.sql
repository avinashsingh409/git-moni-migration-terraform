﻿CREATE TABLE [Criteria].[tCoDFTimeParamDefaults] (
    [CoDFTimeParamDefaultsID] INT            IDENTITY (1, 1) NOT NULL,
    [CoDFTimeFilterTypeID]    INT            NOT NULL,
    [AssetID]                 INT            NULL,
    [PName]                   NVARCHAR (MAX) NOT NULL,
    [PValue]                  NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tCoDFTimeParamDefaults] PRIMARY KEY CLUSTERED ([CoDFTimeParamDefaultsID] ASC),
    CONSTRAINT [FK_tCoDFTimeParamDefaults_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tCoDFTimeParamDefaults_tCoDFTimeFilterType_CoDFTimeFilterTypeID] FOREIGN KEY ([CoDFTimeFilterTypeID]) REFERENCES [Criteria].[tCoDFTimeFilterType] ([CoDFTimeFilterTypeID]) ON DELETE CASCADE
);

