﻿CREATE TABLE [Criteria].[tCoDFTimeFilterType] (
    [CoDFTimeFilterTypeID] INT            NOT NULL,
    [Title]                NVARCHAR (255) NOT NULL,
    [Abbrev]               NVARCHAR (255) NOT NULL,
    [IsPreDataRetrieval]   BIT            CONSTRAINT [DF_tCoDFTimeFilterType_IsPreDataRetrieval] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]         INT            NOT NULL,
    CONSTRAINT [PK_tCoDFTimeFilterType] PRIMARY KEY CLUSTERED ([CoDFTimeFilterTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Criteria', @level1type = N'TABLE', @level1name = N'tCoDFTimeFilterType';

