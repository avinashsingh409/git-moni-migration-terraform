﻿CREATE TABLE [Criteria].[tCoDFTimeFilter] (
    [CoDFTimeFilterID]     INT IDENTITY (1, 1) NOT NULL,
    [CoDFID]               INT NOT NULL,
    [CoDFTimeFilterTypeID] INT NOT NULL,
    [ExecuteOrder]         INT CONSTRAINT [DF_tCoDFTimeFilter_ExecuteOrder] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tCoDFTimeFilter] PRIMARY KEY CLUSTERED ([CoDFTimeFilterID] ASC),
    CONSTRAINT [FK_tCoDFTimeFilter_tCoDF_CoDFID] FOREIGN KEY ([CoDFID]) REFERENCES [Criteria].[tCoDF] ([CoDFID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tCoDFTimeFilter_tCoDFTimeFilterType_coDFTimeFilterTypeID] FOREIGN KEY ([CoDFTimeFilterTypeID]) REFERENCES [Criteria].[tCoDFTimeFilterType] ([CoDFTimeFilterTypeID])
);

