﻿CREATE TABLE [Criteria].[tCo] (
    [CoID]            INT              IDENTITY (1, 1) NOT NULL,
    [GlobalID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tCo_GlobalID] DEFAULT (newid()) NOT NULL,
    [CoTitle]         NVARCHAR (255)   NOT NULL,
    [CoDescription]   NVARCHAR (MAX)   NOT NULL,
    [CreatedByUserID] INT              NOT NULL,
    [ChangedByUserID] INT              NOT NULL,
    [CreateDate]      DATETIME         CONSTRAINT [DF_tCo_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME         CONSTRAINT [DF_tCo_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CoDSID]          INT              NULL,
    [CoDFID]          INT              NULL,
    [CoDAID]          INT              NULL,
    CONSTRAINT [PK_tCo] PRIMARY KEY CLUSTERED ([CoID] ASC),
    CONSTRAINT [FK_tCo_ChangedByUserID_tUser_SecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCo_CreatedByUserID_tUser_SecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCo_tCoDA] FOREIGN KEY ([CoDAID]) REFERENCES [Criteria].[tCoDA] ([CoDAID]),
    CONSTRAINT [FK_tCo_tCoDF] FOREIGN KEY ([CoDFID]) REFERENCES [Criteria].[tCoDF] ([CoDFID]),
    CONSTRAINT [FK_tCo_tCoDS] FOREIGN KEY ([CoDSID]) REFERENCES [Criteria].[tCoDS] ([CoDSID])
);





