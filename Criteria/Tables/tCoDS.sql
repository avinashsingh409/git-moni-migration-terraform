﻿CREATE TABLE [Criteria].[tCoDS] (
    [CoDSID]                   INT              IDENTITY (1, 1) NOT NULL,
    [GlobalID]                 UNIQUEIDENTIFIER CONSTRAINT [DF_tCoDS_GlobalID] DEFAULT (newid()) NOT NULL,
    [CoDSTitle]                NVARCHAR (255)   NOT NULL,
    [CoDSDescription]          NVARCHAR (MAX)   NULL,
    [IsPrivate]                BIT              CONSTRAINT [DF_tCoDS_IsPrivate] DEFAULT ((1)) NOT NULL,
    [AssetID]                  INT              NULL,
    [CreatedByUserID]          INT              NOT NULL,
    [ChangedByUserID]          INT              NOT NULL,
    [CreateDate]               DATETIME         CONSTRAINT [DF_tCoDS_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]               DATETIME         CONSTRAINT [DF_tCoDS_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CoDSSourceTypeID]         INT              NOT NULL,
    [SearchString]             VARCHAR (MAX)    NOT NULL,
    [SearchStringCodecVersion] NVARCHAR (50)    CONSTRAINT [DF_tCoDS_SearchStringCodecVersion] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_tCoDS] PRIMARY KEY CLUSTERED ([CoDSID] ASC),
    CONSTRAINT [FK_tCoDS_ChangedByUserID_tUser_SecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCoDS_CreatedByUserID_tUser_SecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCoDS_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tCoDS_tCoDSSourceType_CoDSSourceTypeID] FOREIGN KEY ([CoDSSourceTypeID]) REFERENCES [Criteria].[tCoDSSourceType] ([CoDSSourceTypeID])
);





