﻿CREATE TABLE [Criteria].[tCoDSSourceType] (
    [CoDSSourceTypeID] INT            NOT NULL,
    [Title]            NVARCHAR (255) NOT NULL,
    [Abbrev]           NVARCHAR (50)  NOT NULL,
    [DisplayOrder]     INT            NOT NULL,
    CONSTRAINT [PK_tCoDSSourceType] PRIMARY KEY CLUSTERED ([CoDSSourceTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Criteria', @level1type = N'TABLE', @level1name = N'tCoDSSourceType';

