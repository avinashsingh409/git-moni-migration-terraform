﻿CREATE TABLE [Criteria].[tTrendCoMap] (
    [TrendCoMapId]   INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendId]      INT            NOT NULL,
    [CoId]           INT            NOT NULL,
    [TrendCoMapName] NVARCHAR (255) NULL,
    [Hidden]         BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [FK_tTrendCoMap_tCo] FOREIGN KEY ([CoId]) REFERENCES [Criteria].[tCo] ([CoID])  ON DELETE CASCADE,
    CONSTRAINT [FK_tTrendCoMap_tTrend] FOREIGN KEY ([PDTrendId]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])  ON DELETE CASCADE
);


GO

CREATE INDEX [IDX_PDTrendId] ON [Criteria].[tTrendCoMap] ([PDTrendId])  WITH (FILLFACTOR=100);
