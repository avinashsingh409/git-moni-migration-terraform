﻿CREATE TABLE [Criteria].[tCoDF] (
    [CoDFID]          INT              IDENTITY (1, 1) NOT NULL,
    [GlobalID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tCoDF_GlobalID] DEFAULT (newid()) NOT NULL,
    [CoDFTitle]       NVARCHAR (255)   NOT NULL,
    [CoDFDescription] NVARCHAR (MAX)   NULL,
    [IsPrivate]       BIT              CONSTRAINT [DF_tCoDF_IsPrivate] DEFAULT ((1)) NOT NULL,
    [AssetID]         INT              NULL,
    [CreatedByUserID] INT              NOT NULL,
    [ChangedByUserID] INT              NOT NULL,
    [CreateDate]      DATETIME         CONSTRAINT [DF_tCoDF_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]      DATETIME         CONSTRAINT [DF_tCoDF_ChangeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tCoDF] PRIMARY KEY CLUSTERED ([CoDFID] ASC),
    CONSTRAINT [FK_tCoDF_ChangedByUserID_tUser_SecurityUserID] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCoDF_CreatedByUserID_tUser_SecurityUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tCoDF_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);

