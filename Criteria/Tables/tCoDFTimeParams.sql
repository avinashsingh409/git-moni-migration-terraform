﻿CREATE TABLE [Criteria].[tCoDFTimeParams] (
    [CoDFTimeParamsID] INT            IDENTITY (1, 1) NOT NULL,
    [CoDFTimeFilterID] INT            NOT NULL,
    [PName]            NVARCHAR (MAX) NOT NULL,
    [PValue]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tCoDFTimeParams] PRIMARY KEY CLUSTERED ([CoDFTimeParamsID] ASC),
    CONSTRAINT [FK_tCoDFTimeParams_tCoDFTimeFilter_CoDFTimeFilterID] FOREIGN KEY ([CoDFTimeFilterID]) REFERENCES [Criteria].[tCoDFTimeFilter] ([CoDFTimeFilterID]) ON DELETE CASCADE
);

