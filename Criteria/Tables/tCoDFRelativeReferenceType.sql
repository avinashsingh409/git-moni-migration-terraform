﻿CREATE TABLE [Criteria].[tCoDFRelativeReferenceType] (
    [CoDFRelativeReferenceTypeID] INT            NOT NULL,
    [Title]                       NVARCHAR (255) NOT NULL,
    [Abbrev]                      NVARCHAR (50)  NOT NULL,
    [DisplayOrder]                INT            NOT NULL,
    CONSTRAINT [PK_tCoDFRelativeReferenceType] PRIMARY KEY CLUSTERED ([CoDFRelativeReferenceTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Criteria', @level1type = N'TABLE', @level1name = N'tCoDFRelativeReferenceType';

