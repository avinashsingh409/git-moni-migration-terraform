﻿CREATE TABLE [Diagnostics].[tCategoryAssetIssueResolutionStatusTypeTransition] (
    [CategoryAssetIssueResolutionStatusTypeTransitionID] INT      IDENTITY (1, 1) NOT NULL,
    [AssetIssueCategoryTypeID]                           INT      NOT NULL,
    [StartAssetIssueResolutionStatusTypeID]              INT      NULL,
    [EndAssetIssueResolutionStatusTypeID]                INT      NULL,
    [CreatedBy]                                          INT      NOT NULL,
    [CreateDate]                                         DATETIME CONSTRAINT [DF_tCategoryAssetIssueResolutionStatusTypeTransition_CreateDate] DEFAULT (sysdatetime()) NULL,
    CONSTRAINT [PK_CategoryAssetIssueResolutionStatusTypeTransition] PRIMARY KEY NONCLUSTERED ([CategoryAssetIssueResolutionStatusTypeTransitionID] ASC),
    CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryAssetIssueResolutionStatusTypeTransition_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID]) REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Diagnostics_tAssetIssueResStatusType_Diagnostics_tCategoryAssetIssueResStatusTypeTransition_EndAssetIssueResStatusTypeID] FOREIGN KEY ([EndAssetIssueResolutionStatusTypeID]) REFERENCES [Diagnostics].[tAssetIssueResolutionStatusType] ([AssetIssueResolutionStatusTypeID]),
    CONSTRAINT [FK_Diagnostics_tAssetIssueResStatusType_Diagnostics_tCategoryAssetIssueResStatusTypeTransition_StartAssetIssueResStatusTypeID] FOREIGN KEY ([StartAssetIssueResolutionStatusTypeID]) REFERENCES [Diagnostics].[tAssetIssueResolutionStatusType] ([AssetIssueResolutionStatusTypeID]),
    CONSTRAINT [FK_tCategoryAssetIssueResolutionStatusTypeTransition_tUser_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);


