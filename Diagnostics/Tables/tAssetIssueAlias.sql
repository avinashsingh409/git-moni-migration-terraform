﻿CREATE TABLE [Diagnostics].[tAssetIssueAlias]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [AssetIssueID] UNIQUEIDENTIFIER NOT NULL, 
    [ExpirationDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NULL, 
    [CreateDate] DATETIME NULL
)
