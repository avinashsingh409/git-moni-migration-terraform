﻿CREATE TABLE [Diagnostics].[tAssetIssueThread] (
    [ThreadID]     INT NOT NULL,
    [AssetIssueID] INT NOT NULL,
    CONSTRAINT [PK_tAssetIssueThread] PRIMARY KEY CLUSTERED ([ThreadID] ASC)
);

