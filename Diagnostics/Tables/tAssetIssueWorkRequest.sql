﻿create table [Diagnostics].[tAssetIssueWorkRequest] (
	[AssetIssueWorkRequestID] [int] identity(1,1) not null,
	[GlobalID] [uniqueidentifier] not null,
	[Title] [nvarchar](50) not null,
	[Description] [nvarchar](max) not null,
	[ExternalSystemAssetID] [int] not null,
	[WorkReferenceID] [nvarchar](50),
	[AssetIssueID] [int] not null,
	[CreateDate] [datetimeoffset] not null,
	[CreatedBy] [int] not null
constraint [PK_tAssetIssueWorkRequest] primary key clustered ( 	[AssetIssueWorkRequestID] asc ),
constraint [UK_tAssetIssueWorkRequest_GlobalID] unique nonclustered ( [GlobalID] asc )
)
go

alter table [Diagnostics].[tAssetIssueWorkRequest] add default (newid()) for [GlobalID]
go

alter table [Diagnostics].[tAssetIssueWorkRequest] with check add constraint [FK_tAssetIssueWorkRequest_tAssetIssue_AssetIssueID] foreign key([AssetIssueID])
references [Diagnostics].[tAssetIssue] ([AssetIssueID])
go

alter table [Diagnostics].[tAssetIssueWorkRequest] check constraint [FK_tAssetIssueWorkRequest_tAssetIssue_AssetIssueID]
go

alter table [Diagnostics].[tAssetIssueWorkRequest] with check add constraint [FK_tAssetIssueWorkRequest_tUser_CreatedBy] foreign key([CreatedBy])
references [AccessControl].[tUser] ([SecurityUserID])
go

alter table [Diagnostics].[tAssetIssueWorkRequest] check constraint [FK_tAssetIssueWorkRequest_tUser_CreatedBy]
go

ALTER TABLE [Diagnostics].[tAssetIssueWorkRequest]  WITH CHECK ADD  CONSTRAINT [FK_Diagnostics_tAssetIssueWorkRequest_ExternalSystemAssetID_Asset_tExternalSystemAsset] FOREIGN KEY([ExternalSystemAssetID])
REFERENCES [Asset].[tExternalSystemAsset] ([ExternalSystemAssetID])
ON DELETE CASCADE
GO

ALTER TABLE [Diagnostics].[tAssetIssueWorkRequest] CHECK CONSTRAINT [FK_Diagnostics_tAssetIssueWorkRequest_ExternalSystemAssetID_Asset_tExternalSystemAsset]
GO
