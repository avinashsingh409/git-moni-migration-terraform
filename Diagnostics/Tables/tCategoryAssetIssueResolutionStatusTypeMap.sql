﻿CREATE TABLE [Diagnostics].[tCategoryAssetIssueResolutionStatusTypeMap] (
    [CategoryAssetIssueResolutionStatusTypeMapID] INT      IDENTITY (1, 1) NOT NULL,
    [AssetIssueCategoryTypeID]                    INT      NOT NULL,
    [AssetIssueResolutionStatusTypeID]            INT      NULL,
    [DisplayOrder]                                INT      CONSTRAINT [DF_Diagnostics_tCategoryAssetIssueResolutionStatusTypeMap_DisplayOrder_Default] DEFAULT ((0)) NOT NULL,
    [ChangedBy]                                   INT      NOT NULL,
    [ChangeDate]                                  DATETIME CONSTRAINT [DF_tCategoryAssetIssueResolutionStatusTypeMap_ChangeDate] DEFAULT (sysdatetime()) NULL,
    CONSTRAINT [PK_CategoryAssetIssueResolutionStatusTypeMap] PRIMARY KEY NONCLUSTERED ([CategoryAssetIssueResolutionStatusTypeMapID] ASC),
    CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryAssetIssueResolutionStatusTypeMap_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID]) REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Diagnostics_tAssetIssueResStatusType_Diagnostics_tCategoryAssetIssueResStatusTypeMap_AssetIssueResStatusTypeID] FOREIGN KEY ([AssetIssueResolutionStatusTypeID]) REFERENCES [Diagnostics].[tAssetIssueResolutionStatusType] ([AssetIssueResolutionStatusTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tCategoryAssetIssueResolutionStatusTypeMap_tUser_ChangedBy] FOREIGN KEY ([ChangedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);


