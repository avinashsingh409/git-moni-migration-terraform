--Diagnostics.tAssetIssueAssetAssessmentMap table
CREATE TABLE [Diagnostics].[tAssetIssueAssetAssessmentMap] (
    [AssetIssueAssetAssessmentMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]                   INT NOT NULL,
    [AssetAssessmentID]              INT NOT NULL,
    CONSTRAINT [PK_tAssetIssueAssetAssessmentMap] PRIMARY KEY CLUSTERED ([AssetIssueAssetAssessmentMapID] ASC),
    CONSTRAINT [FK_tAssetIssueAssetAssessmentMap_tAssetAssessment] FOREIGN KEY ([AssetAssessmentID]) REFERENCES [Diagnostics].[tAssetAssessment] ([AssetAssessmentID]),
    CONSTRAINT [FK_tAssetIssueAssetAssessmentMap_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID])
);




GO

GO


GO

GO


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Association of AssetIssues and AssetAssessments' , @level0type=N'SCHEMA',@level0name=N'Diagnostics', @level1type=N'TABLE',@level1name=N'tAssetIssueAssetAssessmentMap'
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueAssetAssessmentMap';

