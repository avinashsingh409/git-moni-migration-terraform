﻿CREATE TABLE [Diagnostics].[tAssetIssueArchive] (
    [AssetIssueArchiveID]      INT              IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]             INT              NOT NULL,
    [AssetID]                  INT              NOT NULL,
    [IssueTypeID]              INT              NULL,
    [AssetIssueCategoryTypeID] INT              NOT NULL,
    [IssueCauseTypeID]         INT              NULL,
    [Priority]                 NVARCHAR (50)    NULL,
    [Action]                   NVARCHAR (255)   NULL,
    [CreateDate]               DATETIME         NOT NULL,
    [ChangeDate]               DATETIME         NOT NULL,
    [CreatedBy]                NVARCHAR (255)   NOT NULL,
    [ChangedBy]                NVARCHAR (255)   NOT NULL,
    [Confidence_Pct]           FLOAT (53)       NULL,
    [Risk]                     INT              NULL,
    [ModelConditionID]         INT              NULL,
    [IssueTitle]               VARCHAR (255)    NULL,
    [IssueSummary]             NVARCHAR (MAX)   NULL,
    [CloseDate]                DATETIME         NULL,
    [IsHidden]                 BIT              DEFAULT ((0)) NOT NULL,
    [GlobalID]                 UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CreatedByUserID]          INT              NULL,
    [LastEmailSent]            DATETIME         NULL,
    [ActivityStatusID]         INT              CONSTRAINT [DF_tAssetIssueArchive_ActivityStatusID] DEFAULT ((1)) NOT NULL,
    [ResolutionStatusID]       INT              CONSTRAINT [DF_tAssetIssueArchive_ResolutionStatusID] DEFAULT ((1)) NOT NULL,
    [OpenDate]                 DATETIME         NULL,
    [AssignedTo]               INT              NULL,
    [IssueShortSummary]        VARCHAR (MAX)    NULL,
    [Scorecard]                BIT              CONSTRAINT [DB_tAssetIssueArchive_scorecard] DEFAULT ((1)) NOT NULL,
    [ResolveBy]                DATETIME         NULL,
    CONSTRAINT [PK_tAssetIssueArchive] PRIMARY KEY CLUSTERED ([AssetIssueArchiveID] ASC)
);














GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueArchive';

