﻿CREATE TABLE Diagnostics.tCategoryIssueTypeMap(
	[CategoryIssueTypeMapID] [INT] IDENTITY(1,1) NOT NULL,
	[AssetIssueCategoryTypeID] [INT] NOT NULL,
	[IssueTypeID] [INT] NULL
	CONSTRAINT [PK_CategoryIssueTypeMap] PRIMARY KEY NONCLUSTERED ([CategoryIssueTypeMapID] ASC) ,
	CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryIssueTypeMap_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID])
		REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID])
		ON DELETE CASCADE,
	CONSTRAINT [FK_Diagnostics_tIssueType_Diagnostics_tCategoryIssueTypeMap_IssueTypeID] FOREIGN KEY (IssueTypeID)
		REFERENCES [Diagnostics].[tIssueType]([IssueTypeID])
		ON DELETE CASCADE
)


GO

CREATE INDEX [IX_IssueTypeID] ON [Diagnostics].[tCategoryIssueTypeMap] ([IssueTypeID])  WITH (FILLFACTOR=100);
GO