﻿CREATE TABLE [Diagnostics].[tCategoryNotificationType](
	[CategoryNotificationTypeID] [int] NOT NULL,
	[CategoryNotificationTypeDesc] [nvarchar](255) NOT NULL,
	[CategoryNotificationTypeAbbrev] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryNotificationTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'Diagnostics', @level1type=N'TABLE',@level1name=N'tCategoryNotificationType'
GO