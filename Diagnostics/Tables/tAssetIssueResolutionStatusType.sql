﻿CREATE TABLE [Diagnostics].[tAssetIssueResolutionStatusType] (
    [AssetIssueResolutionStatusTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [AssetIssueResolutionStatusTypeDesc] NVARCHAR (255) NOT NULL,
    [CreatedBy]                          INT            NOT NULL,
    [CreateDate]                         DATETIME       CONSTRAINT [DF_tAssetIssueResolutionStatusType_CreateDate] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_AssetIssueResolutionStatusTypeID] PRIMARY KEY NONCLUSTERED ([AssetIssueResolutionStatusTypeID] ASC),
    CONSTRAINT [FK_tAssetIssueResolutionStatusType_tUser_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);





