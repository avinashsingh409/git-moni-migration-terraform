﻿CREATE TABLE [Diagnostics].[tAssetIssueAssociatedAsset] (
    [AssetIssueAssociatedAssetID]		INT	IDENTITY (1, 1) NOT NULL,
	[AssetIssueID] 						[int] NOT NULL,
	[AssetID] 							[int] NOT NULL
	
    CONSTRAINT [PK_tAssetIssueAssociatedAsset] PRIMARY KEY NONCLUSTERED ([AssetIssueAssociatedAssetID] ASC),
	CONSTRAINT [FK_Diagnostics_tAssetIssue_tAssetIssueAssociatedAsset_AssetIssueID] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) 
		ON DELETE CASCADE,
	CONSTRAINT [FK_Asset_tAsset_Diagnostics_tAssetIssueAssociatedAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
	
	CONSTRAINT [UK_tAssetIssueAssociatedAsset_AssetIssueID_AssetID] UNIQUE NONCLUSTERED (AssetIssueID, AssetID)
)