﻿CREATE TABLE [Diagnostics].[tIssueCauseTypeAssetIssueMap] (
    [IssueCauseTypeAssetIssueMapID] INT IDENTITY (1, 1) NOT NULL,
    [IssueCauseTypeID]              INT NOT NULL,
    [AssetIssueID]                  INT NOT NULL,
    CONSTRAINT [PK_tIssueCauseTypeAssetIssueMap] PRIMARY KEY CLUSTERED ([IssueCauseTypeAssetIssueMapID] ASC),
    CONSTRAINT [FK_Diagnostics_tIssueCauseTypeAssetIssueMap_AssetIssueID_Diagnostics_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Diagnostics_tIssueCauseTypeAssetIssueMap_IssueCauseTypeID_Diagnostics_tIssueCauseType] FOREIGN KEY ([IssueCauseTypeID]) REFERENCES [Diagnostics].[tIssueCauseType] ([IssueCauseTypeID])
);



