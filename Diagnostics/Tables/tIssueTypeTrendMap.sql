CREATE TABLE [Diagnostics].[tIssueTypeTrendMap] (
    [IssueTypeTrendMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeID]    INT NOT NULL,
    [IssueTypeID]         INT NOT NULL,
    [TrendID]             INT NOT NULL,
    CONSTRAINT [PK_tIssueTypeTrendMap] PRIMARY KEY CLUSTERED ([IssueTypeTrendMapID] ASC),
    CONSTRAINT [FK_AssetClassTypeIDtIssueTypeTrendMap_tTrend] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_TrendIDtIssueTypeTrendMap_tTrend] FOREIGN KEY ([TrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])
);




GO

GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueTypeTrendMap';

