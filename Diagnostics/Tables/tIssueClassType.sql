CREATE TABLE [Diagnostics].[tIssueClassType] (
    [IssueClassTypeID]     INT            NOT NULL,
    [IssueClassTypeDesc]   NVARCHAR (255) NOT NULL,
    [IssueClassTypeAbbrev] NVARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([IssueClassTypeID] ASC)
);





GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueClassType';

