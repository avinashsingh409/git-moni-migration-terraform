﻿--Diagnostics.tAssetAssessmentAttachment table
CREATE TABLE [Diagnostics].[tAssetAssessmentAttachment] (
    [AssetAssessmentAttachmentID] INT              IDENTITY (1, 1) NOT NULL,
    [AssetAssessmentID]           INT              NOT NULL,
    [AttachmentName]              NVARCHAR (255)   NOT NULL,
    [Comments]                    VARCHAR (MAX)    NULL,
    [GlobalID]                    UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tAssetAssessmentAttachment] PRIMARY KEY CLUSTERED ([AssetAssessmentAttachmentID] ASC),
    CONSTRAINT [FK_tAssetAssessmentAttachment_tAssetAssessment] FOREIGN KEY ([AssetAssessmentID]) REFERENCES [Diagnostics].[tAssetAssessment] ([AssetAssessmentID]) ON DELETE CASCADE
);






GO

GO


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Attachment storage for Asset Assessments' , @level0type=N'SCHEMA',@level0name=N'Diagnostics', @level1type=N'TABLE',@level1name=N'tAssetAssessmentAttachment'
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetAssessmentAttachment';

