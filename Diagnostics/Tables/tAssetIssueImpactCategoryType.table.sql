﻿--DROP TABLE Diagnostics.tAssetIssueAssetIssueImpactTypeMap
--DROP TABLE Diagnostics.tAssetIssueImpactType
--DROP TABLE Diagnostics.tAssetIssueImpactCategoryType

CREATE TABLE [Diagnostics].[tAssetIssueImpactCategoryType] (
    [AssetIssueImpactCategoryTypeID]       INT            NOT NULL,
    [AssetIssueImpactCategoryTypeDesc]     NVARCHAR (50)  NOT NULL,
    [AssetIssueImpactCategoryTypeCalcDesc] NVARCHAR (511) NULL,
    CONSTRAINT [PK_tAssetIssueImpactCategoryType] PRIMARY KEY CLUSTERED ([AssetIssueImpactCategoryTypeID] ASC)
);





 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueImpactCategoryType';

