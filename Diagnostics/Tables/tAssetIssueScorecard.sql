﻿CREATE TABLE [Diagnostics].[tAssetIssueScorecard] (
    [ScorecardID]         INT             IDENTITY (1, 1) NOT NULL,
    [AssetID]             INT             NOT NULL,
    [IssueClassTypeID]    INT             NULL,
    [Timestamp]           DATETIME2 (7)   NOT NULL,
    [Save]                BIT             DEFAULT ((0)) NOT NULL,
    [CreatedBy]           INT             NOT NULL,
    [Scorecard]           NVARCHAR (MAX)  NULL,
    [IsCompressed]        BIT             CONSTRAINT [DF_tAssetIssueScorecard_IsCompressed] DEFAULT ((0)) NOT NULL,
    [CompressedScorecard] VARBINARY (MAX) NULL,
    CONSTRAINT [FK_AccessControl_tUser_Diagnostics_tAssetIssueScorecard] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_Asset_tAsset_Diagnostics_tAssetIssueScorecard] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);

GO

CREATE INDEX [IDX_AssetID_Timestamp] ON [Diagnostics].[tAssetIssueScorecard] ([AssetID], [Timestamp])  WITH (FILLFACTOR=100);
GO