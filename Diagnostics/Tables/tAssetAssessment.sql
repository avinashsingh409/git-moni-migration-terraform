﻿--Diagnostics.tAssetAssessment table
CREATE TABLE [Diagnostics].[tAssetAssessment] (
    [AssetAssessmentID] INT              IDENTITY (1, 1) NOT NULL,
    [AssetID]           INT              NOT NULL,
    [AssessmentTypeID]  INT              NOT NULL,
    [CreateDate]        DATETIME         DEFAULT (getdate()) NOT NULL,
    [ChangeDate]        DATETIME         DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         NVARCHAR (255)   NOT NULL,
    [ChangedBy]         NVARCHAR (255)   NOT NULL,
    [Findings]          NVARCHAR (MAX)   NOT NULL,
    [Recommendations]   NVARCHAR (MAX)   NULL,
    [Comments]          NVARCHAR (MAX)   NULL,
    [AssessmentScore]   FLOAT (53)       NOT NULL,
    [Latitude]          FLOAT (53)       DEFAULT (NULL) NULL,
    [Longitude]         FLOAT (53)       DEFAULT (NULL) NULL,
    [SiteVisit]         VARCHAR (255)    NULL,
    [Direction]         FLOAT (53)       DEFAULT ((0)) NOT NULL,
    [GlobalID]          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tAssetAssessment] PRIMARY KEY CLUSTERED ([AssetAssessmentID] ASC),
    CONSTRAINT [FK_tAssetAssessment_tAssessmentType] FOREIGN KEY ([AssessmentTypeID]) REFERENCES [Diagnostics].[tAssessmentType] ([AssessmentTypeID]),
    CONSTRAINT [FK_tAssetAssessment_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE
);






GO

GO


GO

GO


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assessment records of individual assets.' , @level0type=N'SCHEMA',@level0name=N'Diagnostics', @level1type=N'TABLE',@level1name=N'tAssetAssessment'
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetAssessment';

