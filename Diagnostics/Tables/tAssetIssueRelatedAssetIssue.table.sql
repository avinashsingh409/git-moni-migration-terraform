CREATE TABLE [Diagnostics].[tAssetIssueRelatedAssetIssue] (
    [AssetIssueRelatedAssetIssueID] INT IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]                  INT NOT NULL,
    [RelatedAssetIssueID]           INT NOT NULL,
    CONSTRAINT [PK_AssetIssueRelatedAssetIssue] PRIMARY KEY CLUSTERED ([AssetIssueRelatedAssetIssueID] ASC),
    CONSTRAINT [FK_Diagnostics_tAssetIssueRelatedAssetIssue_AssetIssueID_Diagnostics_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Diagnostics_tAssetIssueRelatedAssetIssue_RelatedAssetIssueID_Diagnostics_tAssetIssue] FOREIGN KEY ([RelatedAssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueRelatedAssetIssue';

