CREATE TABLE [Diagnostics].[tAssetClassTypeIssueTypeMap] (
    [AssetClassTypeIssueTypeMapID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeID]             INT            NULL,
    [IssueTypeID]                  INT            NOT NULL,
    [DisplayOrder]                 INT            NULL,
    [VariableTypeID]               INT            NULL,
    [ValueTypeID]                  INT            NULL,
    [AttributeTypeID]              INT            NULL,
    [AttributeValue]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tAssetClassTypeIssueTypeMap] PRIMARY KEY CLUSTERED ([AssetClassTypeIssueTypeMapID] ASC),
    CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_IssueTypeID_tIssueType] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID]),
    CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetClassTypeIssueTypeMap_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tAssetClassTypeIssueTypeMap_AssetClassTypeID _IssueTypeID] UNIQUE NONCLUSTERED ([AssetClassTypeID] ASC, [IssueTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of AssetClassTypeIssueTypeMaps.', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetClassTypeIssueTypeMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetClassTypeIssueTypeMap';

