﻿CREATE TABLE [Diagnostics].[tIssueType] (
    [IssueTypeID]      INT            NOT NULL,
    [IssueTypeDesc]    NVARCHAR (255) NOT NULL,
    [IssueTypeAbbrev]  NVARCHAR (255) NOT NULL,
    [TrendDirection]   INT            NULL,
    [IsBVApproved]     BIT            NULL,
    [CreateDate]       DATETIME       NULL,
    [ChangeDate]       DATETIME       NULL,
    [CreatedByUserID]  INT            NOT NULL,
    [ChangedByUserID]  INT            NULL,
    CONSTRAINT [PK_tIssueType] PRIMARY KEY CLUSTERED ([IssueTypeID] ASC),
    CONSTRAINT [FK_Diagnostics_tIssueType_ChangedByUserID_AccessControl_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_Diagnostics_tIssueType_CreatedByUserID_AccessControl_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
);
















GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of IssueTypes.', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueType';


GO
CREATE TRIGGER [Diagnostics].[ValidateIssueTypeInsertUpdate]
	ON [Diagnostics].[tIssueType]
	FOR INSERT,UPDATE
AS
BEGIN

	DECLARE @IssueTypeDesc as nvarchar(255)
	
	DECLARE @OK as int
	
	DECLARE curUP INSENSITIVE CURSOR
		FOR SELECT IssueTypeDesc FROM inserted
	
	OPEN curUP
	FETCH NEXT FROM curUP INTO @IssueTypeDesc
	
	SET @OK = 1
	WHILE @OK = 1
	
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
					DECLARE @exIssueTypeDescCount as int
					SET @exIssueTypeDescCount = 0
					SELECT @exIssueTypeDescCount = (SELECT COUNT(IssueTypeID) FROM [Diagnostics].[tIssueType] WHERE IssueTypeDesc = @IssueTypeDesc AND IsBVApproved = 1);
					
					IF @exIssueTypeDescCount > 1 -- items are inserted before this code is run, so violation of business rull would occur if COUNT > 1
						BEGIN
						RAISERROR(N'VIOLATION of tIssueType business rule: IssueTypeDesc: %s already exists',
							16, -- Severity.
							1, -- State.,
							@IssueTypeDesc);
							ROLLBACK TRANSACTION
						END
						
					FETCH NEXT FROM curUP INTO @IssueTypeDesc
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	CLOSE curUP;
	DEALLOCATE curUP;

END

