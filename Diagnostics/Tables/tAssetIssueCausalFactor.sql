CREATE TABLE [Diagnostics].[tAssetIssueCausalFactor] (
    [AssetIssueCausalFactorID] INT           IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]             INT           NOT NULL,
    [Rating]                   INT           NOT NULL,
    [CausalFactor]             VARCHAR (255) NOT NULL,
    [Note]                     VARCHAR (MAX) NULL,
    [CreateDate]               DATETIME      CONSTRAINT [DF_tAssetIssueCausalFactor_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]               DATETIME      CONSTRAINT [DF_tAssetIssueCausalFactor_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                VARCHAR (255) NOT NULL,
    [ChangedBy]                VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAssetIssueCausalFactor] PRIMARY KEY CLUSTERED ([AssetIssueCausalFactorID] ASC),
    CONSTRAINT [FK_tAssetIssueCausalFactor_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE
);






GO

GO


GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueCausalFactor';

