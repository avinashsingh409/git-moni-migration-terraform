﻿CREATE TABLE [Diagnostics].[tAssetIssueKeywordMap] (
    [AssetIssueKeywordMapID] BIGINT   IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]           INT      NOT NULL,
    [KeywordID]              BIGINT   NOT NULL,
    [CreatedBy]              INT      NOT NULL,
    [TagDate]                DATETIME NOT NULL,
    [BackDate]               DATE     NULL,
    CONSTRAINT [PK_tAssetIssueKeywordMap] PRIMARY KEY CLUSTERED ([AssetIssueKeywordMapID] ASC),
    CONSTRAINT [FK_tAssetIssueKeywordMap_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetIssueKeywordMap_tUser] FOREIGN KEY ([CreatedBy]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);




GO
CREATE NONCLUSTERED INDEX [IX_tAssetIssueKeywordMap_AssetIssueID]
    ON [Diagnostics].[tAssetIssueKeywordMap]([AssetIssueID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tAssetIssueKeywordMap_KeywordID]
    ON [Diagnostics].[tAssetIssueKeywordMap]([KeywordID] ASC);

