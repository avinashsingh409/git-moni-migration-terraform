﻿CREATE TABLE [Diagnostics].[tAssetIssueActivityStatusType]
(	[AssetIssueActivityStatusTypeID] [int] IDENTITY(1,1) NOT NULL,
	[AssetIssueActivityStatusTypeDesc] nvarchar(255) not null unique nonclustered,
	[IsOpen] bit not null,
	[IsDefault] bit not null default 0,
 CONSTRAINT [PK_AssetIssueActivityStatusTypeID] PRIMARY KEY NONCLUSTERED 
(
	[AssetIssueActivityStatusTypeID] ASC
)
)

