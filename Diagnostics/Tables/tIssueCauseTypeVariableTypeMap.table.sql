CREATE TABLE [Diagnostics].[tIssueCauseTypeVariableTypeMap] (
    [IssueCauseTypeVariableTypeMapID] INT            IDENTITY (1, 1) NOT NULL,
    [IssueCauseTypeID]                INT            NOT NULL,
    [VariableTypeID]                  INT            NOT NULL,
    [ValueTypeID]                     INT            NULL,
    [Symptom]                         NVARCHAR (255) NULL,
    [AssetClassTypeID]                INT            NULL,
    [SymptomLevel]                    INT            DEFAULT ((1)) NOT NULL,
    [AttributeTypeID]                 INT            NULL,
    [AttributeValue]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tIssueCauseTypeVariableTypeMap] PRIMARY KEY CLUSTERED ([IssueCauseTypeVariableTypeMapID] ASC),
    CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_IssueCauseTypeID_tIssueCauseType] FOREIGN KEY ([IssueCauseTypeID]) REFERENCES [Diagnostics].[tIssueCauseType] ([IssueCauseTypeID]),
    CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_tAttributeType] FOREIGN KEY ([AttributeTypeID]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tIssueCauseTypeVariableTypeMap_VariableTypeID_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [UK_tIssueCauseTypeVariableTypeMap_IssueCauseTypeID_VariableTypeID_ValueTypeID_Symptom] UNIQUE NONCLUSTERED ([IssueCauseTypeID] ASC, [VariableTypeID] ASC, [ValueTypeID] ASC, [Symptom] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of IssueCauseTypeVariableTypeMaps.', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueCauseTypeVariableTypeMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueCauseTypeVariableTypeMap';

