﻿CREATE TABLE Diagnostics.tCategoryAccessRule(
	[CategoryAccessRuleID] [INT] IDENTITY(1,1) NOT NULL,
	[AssetIssueCategoryTypeID] [INT] NOT NULL,
	[AssetID] [INT] NULL,
	[AssetDescendants] [BIT] DEFAULT(1),
	[AssetClassTypeID] [INT] NULL,
	[AssetClassTypeDescendants] [BIT] DEFAULT(1),
	[CreatedBy] [INT] NOT NULL,
	[ChangedBy] [INT] NOT NULL,
	[CreateDate] [DATETIME] NOT NULL DEFAULT (getdate()),
	[ChangeDate] [DATETIME] NOT NULL DEFAULT (getdate()),
	CONSTRAINT [PK_CategoryAccessRule] PRIMARY KEY NONCLUSTERED ([CategoryAccessRuleID] ASC) ,
	CONSTRAINT [FK_AccessControl_tUser_Diagnostics_tCategoryAccessRule_CreatedBy] FOREIGN KEY([CreatedBy])
		REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
		
	CONSTRAINT [FK_AccessControl_tUser_Diagnostics_tCategoryAccessRule_ChangedBy] FOREIGN KEY([ChangedBy])
		REFERENCES [AccessControl].[tUser] ([SecurityUserID]),

	CONSTRAINT [FK_Asset_tAsset_Diagnostics_tCategoryAccessRule_AssetID] FOREIGN KEY ([AssetID])
		REFERENCES [Asset].[tAsset] ([AssetID])
		ON DELETE CASCADE,

	CONSTRAINT [FK_Asset_tAssetClassType_Diagnostics_tCategoryAccessRule_AssetClassTypeID] FOREIGN KEY ([AssetClassTypeID])
		REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
	
	CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryAccessRule_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID])
		REFERENCES [Diagnostics].[tAssetIssueCategoryType]([AssetIssueCategoryTypeID])	
		ON DELETE CASCADE
)
