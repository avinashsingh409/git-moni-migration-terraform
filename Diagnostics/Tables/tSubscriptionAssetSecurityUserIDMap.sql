﻿CREATE TABLE [Diagnostics].[tSubscriptionAssetSecurityUserIDMap] (
    [SubscriptionAssetSecurityUserIDMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetID]                              INT NOT NULL,
    [SecurityUserID]                       INT NOT NULL,
    [Type]                                 INT NOT NULL,
    CONSTRAINT [PK_tSubscriptionAssetSecurityUserIDMap] PRIMARY KEY CLUSTERED ([SubscriptionAssetSecurityUserIDMapID] ASC),
    CONSTRAINT [CHK_Diagnostics_tSubscriptionAssetSecurityUserIDMap_Type] CHECK ([Type]>(0)),
    CONSTRAINT [FK_Diagnostics_tSubscriptionAssetSecurityUserIDMap_Type_Asset_tDiscussionAssetMapType] FOREIGN KEY ([Type]) REFERENCES [Asset].[tDiscussionAssetMapType] ([DiscussionAssetMapTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSubscriptionAssetSecurityUserIDMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSubscriptionAssetSecurityUserIDMap_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);










GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tSubscriptionAssetSecurityUserIDMap';

