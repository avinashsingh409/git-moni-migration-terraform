﻿CREATE TABLE [Diagnostics].[tAssetIssueImpactCalculator](
	[AssetIssueID] [int] NOT NULL,
	[TotalImpactCost] [float] NULL,
	[TotalImpactCostMonthly] [float] NULL,
	CONSTRAINT [PK_AssetIssueImpactCalculator] PRIMARY KEY CLUSTERED ([AssetIssueID] ASC),
	CONSTRAINT [FK_Diagnostics_tAssetIssueImpactCalculator_Diagnostics_tAssetIssue] FOREIGN KEY([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID])
		ON UPDATE CASCADE ON DELETE CASCADE
)