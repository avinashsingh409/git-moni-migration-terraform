﻿CREATE TABLE [Diagnostics].[tAssetIssueAssetIssueImpactTypeMap] (
    [AssetIssueAssetIssueImpactTypeMapID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]                        INT            NOT NULL,
    [AssetIssueImpactTypeID]              INT            NOT NULL,
    [Impact]                              FLOAT (53)     NULL,
    [Impact_Cost]                         FLOAT (53)     NULL,
    [InputString]                         NVARCHAR (MAX) NULL,
    [Impact_Cost_Monthly]                 FLOAT (53)     NULL,
    [ImpactContent]                       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tAssetIssueAssetIssueImpactTypeMap] PRIMARY KEY CLUSTERED ([AssetIssueAssetIssueImpactTypeMapID] ASC),
    CONSTRAINT [FK_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueAssetIssueImpactTypeMap_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueAssetIssueImpactTypeMap_tAssetIssueImpactType] FOREIGN KEY ([AssetIssueImpactTypeID]) REFERENCES [Diagnostics].[tAssetIssueImpactType] ([AssetIssueImpactTypeID]) ON DELETE CASCADE ON UPDATE CASCADE
);







 



GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueAssetIssueImpactTypeMap';

