﻿CREATE TABLE Diagnostics.tCategoryAssetIssueImpactTypeMap(
	[CategoryAssetIssueImpactTypeMapID] [INT] IDENTITY(1,1) NOT NULL,
	[AssetIssueCategoryTypeID] [INT] NOT NULL,
	[AssetIssueImpactTypeID] [INT] NULL
	CONSTRAINT [PK_CategoryAssetIssueImpactTypeMap] PRIMARY KEY NONCLUSTERED ([CategoryAssetIssueImpactTypeMapID] ASC) ,
	CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryAssetIssueImpactTypeMap_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID])
		REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID])
		ON DELETE CASCADE,
	CONSTRAINT [FK_Diagnostics_tAssetIssueImpactType_Diagnostics_tCategoryIssueTypeMap_AssetIssueImpactTypeID] FOREIGN KEY (AssetIssueImpactTypeID)
		REFERENCES [Diagnostics].[tAssetIssueImpactType]([AssetIssueImpactTypeID])
		ON DELETE CASCADE
)
