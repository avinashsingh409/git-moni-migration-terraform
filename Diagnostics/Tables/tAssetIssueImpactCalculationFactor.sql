CREATE TABLE [Diagnostics].[tAssetIssueImpactCalculationFactor] (
    [AssetIssueImpactCalculationFactorID] INT            IDENTITY (1, 1) NOT NULL,
    [CalcuationFactorAbbrev]              NVARCHAR (100) NOT NULL,
    [CalculationFactorDesc]               NVARCHAR (100) NOT NULL,
    [CalculationFactor]                   REAL           NOT NULL,
    [CalculationFactorRatio]              INT            NOT NULL,
    [CalculationFactorUnits]              NVARCHAR (50)  NULL,
    [AssetIssueImpactCategoryTypeID]      INT            NOT NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueImpactCalculationFactor';

