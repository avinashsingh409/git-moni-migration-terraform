--Diagnostics.tAssessmentType table

CREATE TABLE [Diagnostics].[tAssessmentType] (
    [AssessmentTypeID]     INT            NOT NULL,
    [AssessmentTypeDesc]   NVARCHAR (255) NOT NULL,
    [AssessmentTypeAbbrev] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAssessmentType] PRIMARY KEY CLUSTERED ([AssessmentTypeID] ASC)
);




GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Collection of Assessment Types' , @level0type=N'SCHEMA',@level0name=N'Diagnostics', @level1type=N'TABLE',@level1name=N'tAssessmentType'
GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssessmentType';

