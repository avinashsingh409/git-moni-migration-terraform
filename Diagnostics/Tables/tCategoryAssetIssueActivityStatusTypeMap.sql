﻿CREATE TABLE Diagnostics.tCategoryAssetIssueActivityStatusTypeMap(
	[CategoryAssetIssueActivityStatusTypeMapID] [INT] IDENTITY(1,1) NOT NULL,
	[AssetIssueCategoryTypeID] [INT] NOT NULL,
	[AssetIssueActivityStatusTypeID] [INT] NULL
	CONSTRAINT [PK_CategoryAssetIssueActivityStatusTypeMap] PRIMARY KEY NONCLUSTERED ([CategoryAssetIssueActivityStatusTypeMapID] ASC) ,
	CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryAssetIssueActivityStatusTypeMap_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID])
		REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID])
		ON DELETE CASCADE,
	CONSTRAINT [FK_Diagnostics_tAssetIssueActivityStatusType_Diagnostics_tCategoryAssetIssueActivityStatusTypeMap_AssetIssueActivityStatusTypeID] FOREIGN KEY ([AssetIssueActivityStatusTypeID])
		REFERENCES [Diagnostics].[tAssetIssueActivityStatusType]([AssetIssueActivityStatusTypeID])
		ON DELETE CASCADE
)
