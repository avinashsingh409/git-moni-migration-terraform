﻿CREATE TABLE [Diagnostics].[tSubscriptionAssetIssueSecurityUserIDMap] (
    [SubscriptionAssetIssueSecurityUserIDMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]                              INT NOT NULL,
    [SecurityUserID]                            INT NOT NULL,
    CONSTRAINT [PK_tSubscriptionAssetIssueSecurityUserIDMap] PRIMARY KEY CLUSTERED ([SubscriptionAssetIssueSecurityUserIDMapID] ASC),
    CONSTRAINT [FK_tSubscriptionAssetIssueSecurityUserIDMap_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tSubscriptionAssetIssueSecurityUserIDMap_tUser] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [UK_tSubscriptionAssetIssueSecurityUserIDMap] UNIQUE NONCLUSTERED ([AssetIssueID] ASC, [SecurityUserID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tSubscriptionAssetIssueSecurityUserIDMap';

