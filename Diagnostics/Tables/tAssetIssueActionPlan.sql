﻿CREATE TABLE [Diagnostics].[tAssetIssueActionPlan] (
    [AssetIssueActionPlanID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetIssueID]           INT            NOT NULL,
    [ActivityNumber]         INT            NOT NULL,
    [ResponsibleParty]       NVARCHAR (255) NOT NULL,
    [ActivityDate]           DATETIME       NULL,
    [Activity]               NVARCHAR (MAX) NOT NULL,
    [CreateDate]             DATETIME       CONSTRAINT [DF_tAssetIssueActionPlan_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]             DATETIME       CONSTRAINT [DF_tAssetIssueActionPlan_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              NVARCHAR (255) NOT NULL,
    [ChangedBy]              NVARCHAR (255) NOT NULL,
    [Status]                 NVARCHAR (255) CONSTRAINT [DF_tAssetIssueActionPlan_Status] DEFAULT (N'Open') NOT NULL,
    CONSTRAINT [PK_tAssetIssueActionPlan] PRIMARY KEY CLUSTERED ([AssetIssueActionPlanID] ASC),
    CONSTRAINT [FK_tAssetIssueActionPlan_tAssetIssue] FOREIGN KEY ([AssetIssueID]) REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID]) ON DELETE CASCADE
);








GO

GO


GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueActionPlan';

