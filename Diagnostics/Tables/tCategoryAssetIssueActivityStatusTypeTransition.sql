﻿CREATE TABLE Diagnostics.tCategoryAssetIssueActivityStatusTypeTransition(
	[CategoryAssetIssueActivityStatusTypeTransitionID] [INT] IDENTITY(1,1) NOT NULL,
	[AssetIssueCategoryTypeID] [INT] NOT NULL,
	[StartAssetIssueActivityStatusTypeID] [INT] NULL,
	[EndAssetIssueActivityStatusTypeID] [INT] NULL,
	CONSTRAINT [PK_CategoryAssetIssueActivityStatusTypeTransition] PRIMARY KEY NONCLUSTERED ([CategoryAssetIssueActivityStatusTypeTransitionID] ASC) ,
	CONSTRAINT [FK_Diagnostics_tAssetIssueCategoryType_Diagnostics_tCategoryAssetIssueActivityStatusTypeTransition_AssetIssueCategoryTypeID] FOREIGN KEY ([AssetIssueCategoryTypeID])
		REFERENCES [Diagnostics].[tAssetIssueCategoryType] ([AssetIssueCategoryTypeID])
		ON DELETE CASCADE,
	CONSTRAINT [FK_Diagnostics_tAssetIssueActStatusType_Diagnostics_tCategoryAssetIssueActStatusTypeTransition_StartAssetIssueActStatusTypeID] FOREIGN KEY (StartAssetIssueActivityStatusTypeID)
		REFERENCES [Diagnostics].[tAssetIssueActivityStatusType]([AssetIssueActivityStatusTypeID]),
	CONSTRAINT [FK_Diagnostics_tAssetIssueActStatusType_Diagnostics_tCategoryAssetIssueActStatusTypeTransition_EndAssetIssueActStatusTypeID] FOREIGN KEY (EndAssetIssueActivityStatusTypeID)
		REFERENCES [Diagnostics].[tAssetIssueActivityStatusType]([AssetIssueActivityStatusTypeID])		
)
