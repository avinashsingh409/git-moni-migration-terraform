﻿CREATE TABLE [Diagnostics].[tCategoryNotificationAttachment]
(
	[CategoryNotificationAttachmentID] INT IDENTITY (1, 1) NOT NULL,
	[CategoryNotificationID] INT NOT NULL,
	[FileGUID] uniqueidentifier NOT NULL,
	[FileSize] INT NOT NULL DEFAULT 0,

	CONSTRAINT [PK_tCategoryNotificationAttachment] PRIMARY KEY NONCLUSTERED ([CategoryNotificationAttachmentID] ASC),
    CONSTRAINT [FK_Diagnostics_tCategoryNotificationAttachment_CategoryNotificationID] FOREIGN KEY ([CategoryNotificationID]) REFERENCES [Diagnostics].[tCategoryNotification] ([CategoryNotificationID]) ON DELETE CASCADE
)
GO