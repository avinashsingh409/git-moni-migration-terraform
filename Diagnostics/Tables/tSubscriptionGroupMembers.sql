CREATE TABLE [Diagnostics].[tSubscriptionGroupMembers] (
    [SubGroupMemberID] INT IDENTITY (1, 1) NOT NULL,
    [GroupID]          INT NOT NULL,
    [SubscriberID]     INT NOT NULL,
    PRIMARY KEY CLUSTERED ([SubGroupMemberID] ASC),
    CONSTRAINT [fk_GroupId] FOREIGN KEY ([GroupID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [fk_SubscriberId] FOREIGN KEY ([SubscriberID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID])
);


GO


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tSubscriptionGroupMembers';

