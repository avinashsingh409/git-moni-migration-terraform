CREATE TABLE [Diagnostics].[tAssetIssueCategoryType] (
    [AssetIssueCategoryTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [CategoryAbbrev]           NVARCHAR (255) NOT NULL,
    [CategoryDesc]             NVARCHAR (255) NOT NULL,
	[IssueClassTypeID]		   INT			  NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ChangedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL DEFAULT (getdate()),
	[ChangeDate] [datetime] NOT NULL DEFAULT (getdate()),
    [IsHidden]                 BIT            CONSTRAINT [DF_tAssetIssueCategoryType_IsHidden] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AssetIssueCategoryType] PRIMARY KEY CLUSTERED ([AssetIssueCategoryTypeID] ASC),
	CONSTRAINT [FK_Diagnostics_tIssueClassType_IssueClassTypeID_Diagnostics_tAssetIssueCategoryType] FOREIGN KEY ([IssueClassTypeID]) REFERENCES [Diagnostics].[tIssueClassType](IssueClassTypeID) ON DELETE CASCADE
);
GO

EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueCategoryType';
GO 

ALTER TABLE [Diagnostics].[tAssetIssueCategoryType]  ADD  CONSTRAINT [FK_AccessControl_tUser_CreatedBy_Diagnostics_tAssetIssueCategoryType] FOREIGN KEY([CreatedBy])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO

ALTER TABLE [Diagnostics].[tAssetIssueCategoryType] ADD  CONSTRAINT [FK_AccessControl_tUser_ChangedBy_Diagnostics_tAssetIssueCategoryType] FOREIGN KEY([ChangedBy])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO
