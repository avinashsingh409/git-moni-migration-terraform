﻿CREATE TABLE Diagnostics.tDiscussionAssetIssueMap(
	[DiscussionAssetIssueMapID] INT NOT NULL IDENTITY(1,1),
	[DiscussionID] UNIQUEIDENTIFIER NOT NULL,
	[AssetIssueID] INT NOT NULL,
	CONSTRAINT [PK_tDiscussionAssetIssueMap] PRIMARY KEY CLUSTERED 
	(
		[DiscussionAssetIssueMapID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
GO

ALTER TABLE [Diagnostics].[tDiscussionAssetIssueMap]  ADD  CONSTRAINT [FK_Diagnostics_DiscussionAssetIssueMap_AssetIssueID_Diagnostics_tAssetIssue] FOREIGN KEY([AssetIssueID])
REFERENCES [Diagnostics].[tAssetIssue] ([AssetIssueID])
ON DELETE CASCADE
GO

ALTER TABLE [Diagnostics].[tDiscussionAssetIssueMap]  ADD CONSTRAINT [FK_Diagnostics_DiscussionAssetIssueMap_DiscussionID_Discussions_tDiscussion] FOREIGN KEY ([DiscussionID])
REFERENCES [Discussions].[tDiscussion] ([DiscussionID])
ON DELETE CASCADE
GO