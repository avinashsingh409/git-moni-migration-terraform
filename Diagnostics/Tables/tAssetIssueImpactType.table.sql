CREATE TABLE [Diagnostics].[tAssetIssueImpactType] (
    [AssetIssueImpactTypeID]         INT           NOT NULL,
    [AssetIssueImpactCategoryTypeID] INT           NOT NULL,
    [AssetIssueImpactTypeDesc]       NVARCHAR (50) NOT NULL,
    [Units]                          NVARCHAR (50) NOT NULL,
    [DisplayOrder]                   INT           NOT NULL,
    CONSTRAINT [PK_tAssetIssueImpactType] PRIMARY KEY CLUSTERED ([AssetIssueImpactTypeID] ASC),
    CONSTRAINT [FK_tAssetIssueImpactType_tAssetIssueImpactCategoryType] FOREIGN KEY ([AssetIssueImpactCategoryTypeID]) REFERENCES [Diagnostics].[tAssetIssueImpactCategoryType] ([AssetIssueImpactCategoryTypeID]) ON DELETE CASCADE ON UPDATE CASCADE
);









GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tAssetIssueImpactType';

