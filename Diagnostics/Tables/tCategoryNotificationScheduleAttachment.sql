﻿CREATE TABLE [Diagnostics].[tCategoryNotificationScheduleAttachment]
(
	[CategoryNotificationScheduleAttachmentID] INT IDENTITY (1, 1) NOT NULL,
	[CategoryNotificationScheduleID] INT NOT NULL,
	[FileGUID] uniqueidentifier NOT NULL,
	
	CONSTRAINT [PK_tCategoryNotificationScheduleAttachment] PRIMARY KEY NONCLUSTERED ([CategoryNotificationScheduleAttachmentID] ASC),
    CONSTRAINT [FK_Diagnostics_tCategoryNotificationScheduleAttachment_CategoryNotificationScheduleID] FOREIGN KEY ([CategoryNotificationScheduleID]) REFERENCES [Diagnostics].[tCategoryNotificationSchedule] ([CategoryNotificationScheduleID]) ON DELETE CASCADE
)
GO