﻿CREATE TABLE [Diagnostics].[tIssueCauseType] (
    [IssueCauseTypeID] INT            NOT NULL,
    [IssueTypeID]      INT            NOT NULL,
    [IssueCauseDesc]   NVARCHAR (255) NOT NULL,
    [IssueCauseAbbrev] NVARCHAR (255) NOT NULL,
    [IsBVApproved]     BIT            NULL,
    [CreateDate]       DATETIME       NULL,
    [ChangeDate]       DATETIME       NULL,
    [CreatedByUserID]  INT            NOT NULL,
    [ChangedByUserID]  INT            NULL,
    CONSTRAINT [PK_tIssueCauseType] PRIMARY KEY CLUSTERED ([IssueCauseTypeID] ASC),
    CONSTRAINT [FK_Diagnostics_tIssueCauseType_ChangedByUserID_AccessControl_tUser] FOREIGN KEY ([ChangedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_Diagnostics_tIssueCauseType_CreatedByUserID_AccessControl_tUser] FOREIGN KEY ([CreatedByUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]),
    CONSTRAINT [FK_tIssueCauseType_IssueTypeID_tIssueType] FOREIGN KEY ([IssueTypeID]) REFERENCES [Diagnostics].[tIssueType] ([IssueTypeID])
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of IssueCauseTypes.', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueCauseType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Diagnostics', @level1type = N'TABLE', @level1name = N'tIssueCauseType';


GO

CREATE TRIGGER [Diagnostics].[ValidateIssueCauseTypeInsertUpdate]
	ON [Diagnostics].[tIssueCauseType]
	FOR INSERT,UPDATE
AS
BEGIN

	DECLARE @IssueCauseDesc as nvarchar(255)
	
	DECLARE @OK as int
	
	DECLARE curUP INSENSITIVE CURSOR
		FOR SELECT IssueCauseDesc FROM inserted
	
	OPEN curUP
	FETCH NEXT FROM curUP INTO @IssueCauseDesc
	
	SET @OK = 1
	WHILE @OK = 1
	
		BEGIN
			IF @@FETCH_STATUS = 0
				BEGIN
					DECLARE @exIssueCauseDescCount as int
					SET @exIssueCauseDescCount = 0
					SELECT @exIssueCauseDescCount = (SELECT COUNT(IssueCauseTypeID) FROM [Diagnostics].[tIssueCauseType] WHERE IssueCauseDesc = @IssueCauseDesc AND IsBVApproved = 1);
					
					IF @exIssueCauseDescCount > 0
						BEGIN
							RAISERROR(N'VIOLATION of tIssueCauseType business rule: IssueCauseDesc: %s already exists',
								16, -- Severity.
								1, -- State.,
								@IssueCauseDesc);
								ROLLBACK TRANSACTION
						END
						
					FETCH NEXT FROM curUP INTO @IssueCauseDesc
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	
	CLOSE curUP;
	DEALLOCATE curUP;

END