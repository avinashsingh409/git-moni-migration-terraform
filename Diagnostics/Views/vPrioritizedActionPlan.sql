﻿CREATE VIEW [Diagnostics].[vPrioritizedActionPlan]
AS


SELECT
	ap.AssetIssueActionPlanID
	, ap.AssetIssueID
	, ap.ActivityNumber
	, ap.ResponsibleParty
	, ap.ActivityDate
	, ap.Activity
	, ap.CreateDate
	, ap.ChangeDate
	, ap.CreatedBy
	, ap.ChangedBy
	, ap.[Status]
FROM (
	SELECT nx.AssetIssueID, nx.AssetIssueActionPlanID, nx.rn, nx.PriorityAction, ROW_NUMBER() OVER (PARTITION BY nx.AssetIssueID ORDER BY nx.rn) AS town
	FROM (
		SELECT apo.AssetIssueID, apo.AssetIssueActionPlanID, apo.rn, apo.PriorityAction
		FROM (
			SELECT AssetIssueID, AssetIssueActionPlanID, ROW_NUMBER() OVER (PARTITION BY AssetIssueID ORDER BY CreateDate) AS rn, 1 AS PriorityAction
			FROM Diagnostics.tAssetIssueActionPlan
			WHERE ResponsibleParty <> '' AND [Status] IN ('Open', 'Assigned')
		) apo
	) nx
		
) abx
JOIN Diagnostics.tAssetIssueActionPlan ap ON abx.AssetIssueActionPlanID = ap.AssetIssueActionPlanID
WHERE abx.town = 1