﻿CREATE VIEW [Diagnostics].[vAssetIssue]
AS
    SELECT ai.[AssetIssueID]
      , ai.[AssetID]
	  , ast.AssetDesc
	  , ast.AssetAbbrev 
	  , act.AssetClassTypeAbbrev
	  , act.AssetClassTypeDesc
	  , asstype.AssetTypeAbbrev
	  , asstype.AssetTypeDesc 
      , ai.[IssueTypeID]
      , ai.[AssetIssueCategoryTypeID]
	  , ai.[ActivityStatusID]
      , actStatus.[AssetIssueActivityStatusTypeDesc] AS [ActivityStatus]
	  , ai.[ResolutionStatusID]
      , resStatus.[AssetIssueResolutionStatusTypeDesc] AS [ResolutionStatus]
      , ai.[IssueCauseTypeID]
	  , cause.IssueCauseDesc
	  , cause.IssueCauseAbbrev 
      , COALESCE( ai.[Priority], '') [Priority]
      , ai.[Action]
      , ai.[CreateDate]
      , ai.[ChangeDate]
      , ai.[CreatedBy]
      , ai.[ChangedBy]
      , ai.[Confidence_Pct]
      , ai.[Risk]
      , ai.[ModelConditionID]
      , ai.[IssueTitle]
	  , ai.[IssueShortSummary]
      , ai.[IssueSummary]
      , ai.[CloseDate]
      , ai.[GlobalID]
      , ai.[CreatedByUserID]
      , ai.[IsHidden]
      , ai.[LastEmailSent]
	  , usr.SecurityUserID AS AssignedTo
	  , ai.[Scorecard]
	  , ai.[ResolveBy]
	  , COALESCE(usr.Username, '') AS AssignedToUsername
	  , COALESCE(usr.Email, '') AS AssignedToEmail
	  , CASE WHEN ai.CloseDate IS NULL THEN DATEDIFF(second,ai.CreateDate,GETDATE()) --Open Issue
			ELSE DATEDIFF(second,ai.CreateDate,ai.CloseDate) END AS OpenDuration --Closed Issue
	  , ai.OpenDate
	  , COALESCE( issueType.IssueTypeDesc, '') IssueTypeDesc
	  , COALESCE( issueType.IssueTypeAbbrev, '') IssueTypeAbbrev
	  , issueType.TrendDirection
	  , catType.CategoryAbbrev
	  , catType.CategoryDesc
	  , catType.IssueClassTypeID
      , catType.IsHidden AS CategoryHidden
	  , classType.IssueClassTypeAbbrev
	  , classType.IssueClassTypeDesc
	  , calc.TotalImpactCost ImpactTotal
	  , calc.TotalImpactCostMonthly ImpactTotalMonthly

    FROM Diagnostics.tAssetIssue ai
        INNER JOIN Diagnostics.tAssetIssueCategoryType catType
        ON catType.AssetIssueCategoryTypeID = ai.AssetIssueCategoryTypeID
        INNER JOIN Diagnostics.tIssueClassType classType
        ON catType.IssueClassTypeID = classType.IssueClassTypeID
        INNER JOIN Diagnostics.tAssetIssueActivityStatusType actStatus
        ON ai.ActivityStatusID = actStatus.[AssetIssueActivityStatusTypeID]
        INNER JOIN Diagnostics.tAssetIssueResolutionStatusType resStatus
        ON ai.ResolutionStatusID = resStatus.[AssetIssueResolutionStatusTypeID]
        LEFT JOIN Diagnostics.tIssueType issueType
        ON issueType.IssueTypeID = ai.IssueTypeID
        LEFT JOIN AccessControl.tUser usr
        ON usr.SecurityUserID = ai.AssignedTo
        LEFT JOIN Asset.tAsset ast
        ON ai.AssetID = ast.AssetID
        INNER JOIN Asset.tAssetClassType act
        ON ast.AssetClassTypeID = act.AssetClassTypeID
        INNER JOIN Asset.tAssetType asstype
        ON asstype.AssetTypeID = act.AssetTypeID
        LEFT JOIN Diagnostics.tIssueCauseType cause
        ON ai.IssueCauseTypeID = cause.IssueCauseTypeID
		LEFT JOIN Diagnostics.tAssetIssueImpactCalculator calc
		ON ai.AssetIssueID = calc.AssetIssueID
    WHERE ai.IsHidden = 0 AND catType.IsHidden = 0
