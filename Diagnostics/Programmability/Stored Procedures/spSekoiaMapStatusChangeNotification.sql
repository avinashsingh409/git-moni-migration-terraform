﻿CREATE PROCEDURE [CapPrior].[spSekoiaMapStatusChangeNotification] (
	@Flow CapPrior.tpProjectCashFlow READONLY,
	@UserID int,
	@appliesToManualChange int,
	@appliesToImportChange int
	)
AS
BEGIN
-- This proc assumes that @Flow only contains 1 record
DECLARE @assetID int = (select AssetID from @Flow)

DECLARE @currentValue float = (select top 1 tPCF.Value from CapPrior.tProjectCashFlow tPCF join @Flow f on tPCF.AssetID = f.AssetID 
                               where tPCF.Timestamp < getdate() and f.Type = tPCF.Type AND tPCF.Value <> 99 order by tPCF.Timestamp desc)

Declare @statusChanges Diagnostics.tpAssetIssueStatusChange

DECLARE @assetClassTypeID int = (Select AssetClassTypeID FROM Asset.tAsset tA join @Flow f on tA.AssetID = f.AssetID )
declare @newValue float = (select Value from @Flow)

DECLARE @mapName varchar(255) = (select [TYPE] from @Flow)

INSERT INTO @statusChanges (AssetIssueID, AssetIssueGUID, AssetID, IssueTitle, ChangeDate, ChangedBy, ActivityStatusFrom,
			 ActivityStatusTo, ResolutionStatusFrom, ResolutionStatusTo, NotificationID, NotificationTypeID)
  SELECT -1, NULL, @assetID, NULL, getdate(), @UserID, @currentValue, @newValue, NULL, NULL, cN.CategoryNotificationID, cN.CategoryNotificationTypeID
  FROM Diagnostics.tCategoryNotification cN 
  INNER JOIN CapPrior.ufnGetSekoiaMapStatusChangeNotification(@assetID, @mapName, @currentValue, @newValue, @appliesToManualChange, @appliesToImportChange) caF 
    ON caF.NotificationID = cN.CategoryNotificationID

IF EXISTS(SELECT * FROM @statusChanges)
		BEGIN		
			BEGIN TRY
				SET XACT_ABORT OFF	
				-- PBI 11430
				Exec Diagnostics.spEmailMapStatusChange @statusChanges -- #1
				Exec Diagnostics.spUpdateCashflowAssetIssueStatusChange @statusChanges -- #2
				Exec Diagnostics.spModifyAttributeIssueStatusChange @statusChanges -- #3
				Exec Diagnostics.spAddDeleteKeyTagOnStatusChange @statusChanges -- #4
				Exec Diagnostics.spAddDiscussionEntryOnStatusChange @statusChanges -- #5				
				Exec Diagnostics.spCreateAssetIssueOnWorkflowEvent @statusChanges -- # 6											
				SET XACT_ABORT ON
			END TRY
			BEGIN CATCH				
				PRINT 'Caught error in map status change automated event'				
			END CATCH
		END
END
GO

GRANT EXECUTE
    ON OBJECT::[CapPrior].[spSekoiaMapStatusChangeNotification] TO [TEUser]
    AS [dbo];
GO