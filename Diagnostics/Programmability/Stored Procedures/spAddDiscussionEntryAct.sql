﻿------

CREATE PROCEDURE Diagnostics.spAddDiscussionEntryAct
	@UserID INT,
	@AssetID INT,
	@AssetIssueID INT,
	@Title NVARCHAR(255),
	@Content NVARCHAR(MAX),
	@AuditTitle NVARCHAR(255),
	@AuditContent NVARCHAR(MAX),
	@BlogDate DATETIMEOFFSET(7),
	@SekoiaMapChange BIT = 0  -- if this is a delayed action Map Status change (not issue status), Lambda handler will need to populate this parameter with 1 before calling
AS
BEGIN
	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @EntryID UNIQUEIDENTIFIER
	DECLARE @AuditEntryID UNIQUEIDENTIFIER
    
	SET @BlogDate = ISNULL(@BlogDate, SYSDATETIMEOFFSET())

	BEGIN TRY
		BEGIN
			IF @Title is not null
			BEGIN
				IF @SekoiaMapChange = 0 AND @AssetIssueID > 0
					BEGIN
						EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @Title, @Content, @BlogDate, 0, @EntryID OUTPUT
					END
				ELSE
					BEGIN
						EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @Title, @Content, @BlogDate, 0, @EntryID OUTPUT
					END
			END

			IF @AuditTitle is not null
			BEGIN
				IF @SekoiaMapChange = 0 AND @AssetIssueID > 0
					BEGIN
						EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, @BlogDate, 1, @AuditEntryID OUTPUT
					END
				ELSE
					BEGIN
						EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitle, @AuditContent, @BlogDate, 1, @AuditEntryID OUTPUT
					END
			END 			

			IF @EntryID IS NULL
			BEGIN
				SET @ErrorMsg = @ErrorMsg + '; Failed to add discussion entry'
			END
						
			IF @AuditEntryID IS NULL
			BEGIN
				SET @ErrorMsg = @ErrorMsg + '; Failed to add audit discussion entry'
			END
		END
	END TRY
	BEGIN CATCH
		BEGIN
			SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
		END
	END CATCH

	IF @ErrorMsg <> ''
	  BEGIN
		RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAddDiscussionEntryAct] TO [TEUser]
    AS [dbo];
GO

