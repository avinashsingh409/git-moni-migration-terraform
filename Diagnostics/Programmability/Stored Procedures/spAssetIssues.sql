﻿CREATE PROCEDURE [Diagnostics].[spAssetIssues] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID int = NULL,
	@startDate datetime = '1/1/1980',
	@endDate datetime = '1/1/2050',
	@changeStartDate datetime = '1/1/1980',
	@changeEndDate datetime = '1/1/2050',
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@activityStatus NVARCHAR(255) = NULL,
	@resolutionStatus NVARCHAR(255) = NULL,
	@issueTypeID int = NULL,
	@issueClassTypeID int = NULL,
	@startIndex int = 1, 
	@endIndex int = 15,
	@sort int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	WITH records as (
		SELECT ROW_NUMBER() OVER (
		
		ORDER BY 
		CASE @sort WHEN 1 THEN [Diagnostics].[vAssetIssue].[IssueTypeAbbrev] ELSE NULL END,
		CASE @sort WHEN 2 THEN [Diagnostics].[vAssetIssue].[CreateDate] ELSE NULL END,
		CASE @sort WHEN 3 THEN [Diagnostics].[vAssetIssue].[ChangeDate] ELSE NULL END,
		CASE @sort WHEN 4 THEN [Diagnostics].[vAssetIssue].[CloseDate] ELSE NULL END,
		CASE @sort WHEN 5 THEN [Diagnostics].[vAssetIssue].[IssueTitle] ELSE NULL END,
		CASE @sort WHEN 6 THEN [Diagnostics].[vAssetIssue].[Priority] ELSE NULL END,
		CASE @sort WHEN 7 THEN [Diagnostics].[vAssetIssue].[CategoryAbbrev] ELSE NULL END,
		CASE @sort WHEN 8 THEN [Diagnostics].[vAssetIssue].[ActivityStatus] ELSE NULL END,
		CASE @sort WHEN 9 THEN [Diagnostics].[vAssetIssue].[ResolutionStatus] ELSE NULL END,
		CASE @sort WHEN 10 THEN [Diagnostics].[vAssetIssue].[ResolveBy] ELSE NULL END,
		CASE @sort WHEN -1 THEN [Diagnostics].[vAssetIssue].[IssueTypeAbbrev] ELSE NULL END DESC,
		CASE @sort WHEN -2 THEN [Diagnostics].[vAssetIssue].[CreateDate] ELSE NULL END DESC,
		CASE @sort WHEN -3 THEN [Diagnostics].[vAssetIssue].[ChangeDate] ELSE NULL END DESC,
		CASE @sort WHEN -4 THEN [Diagnostics].[vAssetIssue].[CloseDate] ELSE NULL END DESC,
		CASE @sort WHEN -5 THEN [Diagnostics].[vAssetIssue].[IssueTitle] ELSE NULL END DESC,
		CASE @sort WHEN -6 THEN [Diagnostics].[vAssetIssue].[Priority] ELSE NULL END DESC,
		CASE @sort WHEN -7 THEN [Diagnostics].[vAssetIssue].[CategoryAbbrev] ELSE NULL END DESC,
		CASE @sort WHEN -8 THEN [Diagnostics].[vAssetIssue].[ActivityStatus] ELSE NULL END DESC,
		CASE @sort WHEN -9 THEN [Diagnostics].[vAssetIssue].[ResolutionStatus] ELSE NULL END DESC,
		CASE @sort WHEN -10 THEN [Diagnostics].[vAssetIssue].[ResolveBy] ELSE NULL END DESC,
		Diagnostics.vAssetIssue.AssetIssueID
		) AS ROW_NUMBER,
			   Diagnostics.vAssetIssue.AssetIssueID
		FROM Diagnostics.vAssetIssue
		--Join to provide security 
		INNER JOIN Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID,@assetID) securityCheck 
			ON securityCheck.AssetID=Diagnostics.vAssetIssue.AssetID
		WHERE (Diagnostics.vAssetIssue.CreateDate BETWEEN @startDate AND @endDate) AND
		(Diagnostics.vAssetIssue.ChangeDate BETWEEN @changeStartDate AND @changeEndDate) AND
		(@closeStartDate IS NULL OR (Diagnostics.vAssetIssue.CloseDate > @closeStartDate)) AND
		(@closeEndDate IS NULL OR (Diagnostics.vAssetIssue.CloseDate < @closeENDDate)) AND
		(@activityStatus IS NULL OR (Diagnostics.vAssetIssue.[ActivityStatus] = @activityStatus )) AND 
		(@resolutionStatus IS NULL OR (Diagnostics.vAssetIssue.[ResolutionStatus] = @resolutionStatus )) AND 
		(@issueTypeID IS NULL OR (Diagnostics.vAssetIssue.IssueTypeID = @issueTypeID)) AND
		(@issueClassTypeID IS NULL OR (Diagnostics.vAssetIssue.IssueClassTypeID = @issueClassTypeID))
	)

	SELECT 
	r.ROW_NUMBER,
		asset.AssetDesc as asset_AssetDesc,
		asset.AssetAbbrev as asset_AssetAbbrev,
		asset.AssetClassTypeID as asset_AssetClassTypeID,
		syst.AssetDesc as system_AssetDesc,
		syst.AssetAbbrev as system_AssetAbbrev,
		syst.AssetClassTypeID as system_AssetClassTypeID,
		unit.AssetDesc as unit_AssetDesc,
		unit.AssetAbbrev as unit_AssetAbbrev,
		unit.AssetClassTypeID as unit_AssetClassTypeID,
		station.AssetDesc as station_AssetDesc,
		station.AssetAbbrev as station_AssetAbbrev,
		station.AssetClassTypeID as station_AssetClassTypeID,
		stationgroup.AssetDesc as stationgroup_AssetDesc,
		stationgroup.AssetAbbrev as stationgroup_AssetAbbrev,
		stationgroup.AssetClassTypeID as stationgroup_AssetClassTypeID,
		client.AssetDesc as client_AssetDesc,
		client.AssetAbbrev as client_AssetAbbrev,
		client.AssetClassTypeID as client_AssetClassTypeID,

		a.AssetIssueID as AssetIssueID,
		a.GlobalID as AssetIssueGlobalID,
		a.CreateDate as CreateDate,
		a.CreatedBy as CreatedBy,
		a.ChangeDate as ChangeDate,
		a.ChangedBy as ChangedBy,
		a.CloseDate as CloseDate,
		a.IssueTypeID as IssueTypeID,
		a.IssueTypeAbbrev as IssueTypeAbbrev,
		a.IssueTypeDesc as IssueTypeDesc,
		a.IssueClassTypeID as IssueClassTypeID,
		a.IssueClassTypeAbbrev as IssueClassTypeAbbrev,
		a.IssueClassTypeDesc as IssueClassTypeDesc,
		a.IssueSummary as IssueSummary,
		a.IssueTitle as IssueTitle,
		a.Confidence_Pct as ConfidencePct,
		a.[Priority] as [Priority],
		a.[ActivityStatus] as [ActivityStatus],
		a.[ResolutionStatus] as [ResolutionStatus],
		a.AssetIssueCategoryTypeID as AssetIssueCategoryTypeID,
		a.CategoryAbbrev as CategoryAbbrev, 
		a.CategoryDesc as CategoryDesc,
		a.Scorecard as Scorecard,
		a.ResolveBy as ResolveBy,
		CASE WHEN sub.SecurityUserID IS NULL THEN 0 ELSE 1 END as IsSubscribed
		
		FROM Diagnostics.vAssetIssue a 
		LEFT JOIN Asset.tAsset client 
			ON client.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'CLT')
		LEFT JOIN Asset.tAsset stationgroup 
			ON stationgroup.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'SG')
		LEFT JOIN Asset.tAsset station
			ON station.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'ST')
		LEFT JOIN Asset.tAsset unit
			ON unit.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'UN')
		LEFT JOIN Asset.tAsset syst
			ON syst.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'SYS')
		LEFT JOIN Asset.tAsset asset 
			ON asset.AssetID = a.AssetID
		LEFT JOIN (
			SELECT DISTINCT Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap.AssetIssueID, 
							Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap.SecurityUserID
			FROM Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap 
			WHERE Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap.SecurityUserID = @securityUserID) sub
			ON sub.AssetIssueID = a.AssetIssueID
		INNER JOIN records r ON a.AssetIssueID = r.AssetIssueID
	WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex
END
GO


GO
GRANT EXECUTE
	ON OBJECT::[Diagnostics].[spAssetIssues] TO [TEUser]
	AS [dbo];