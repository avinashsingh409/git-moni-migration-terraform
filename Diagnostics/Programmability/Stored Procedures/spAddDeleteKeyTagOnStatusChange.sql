﻿CREATE PROCEDURE [Diagnostics].[spAddDeleteKeyTagOnStatusChange]

@statusChanges [Diagnostics].[tpAssetIssueStatusChange] READONLY
AS
BEGIN
	DECLARE @ErrorMsg AS VARCHAR(MAX) = ''

	DECLARE @results AS TABLE
	(
	  UserID INT,
	  AssetID INT,
	  AssetIssueID INT,
	  ChangeDate DATETIME,
	  ActivityStatusFrom NVARCHAR(255),
	  ActivityStatusTo NVARCHAR(255),
	  ResolutionStatusFrom NVARCHAR(255),
	  ResolutionStatusTo NVARCHAR(255),
	  KeyTagOnIssue BIT,
	  KeyTagAction NVARCHAR(40),
	  KeyTag NVARCHAR(255),
	  SekoiaMapChange BIT,
	  SekoiaMapName NVARCHAR(255),
	  DelayHours INT NULL,
	  DelayGuard NVARCHAR(255) NULL,
	  CancelOnActivityStatusChange INT NULL,
	  CancelOnResolutionStatusChange INT NULL
	 )

	INSERT INTO @results
	SELECT 
		s.ChangedBy,
		s.AssetID,
		s.AssetIssueID,
		s.ChangeDate,
		s.ActivityStatusFrom,
		s.ActivityStatusTo,
		s.ResolutionStatusFrom,
		s.ResolutionStatusTo,
		n.KeyTagOnIssue,
		n.KeyTagAction,
		n.KeyTag,
		ISNULL(n.SekoiaMapChange,0),
		n.SekoiaMapName,
		n.DelayHours, 
		n.DelayGuard,
		n.CancelOnActivityStatusChange,
		n.CancelOnResolutionStatusChange
	FROM @statusChanges s	
	INNER JOIN Diagnostics.tCategoryNotification n ON s.NotificationID = n.CategoryNotificationID
	WHERE s.NotificationTypeID = 4 AND n.ModifyKeyTag = 1

	DECLARE @UserID INT
	DECLARE @AssetID INT
	DECLARE @AssetIssueID INT
	DECLARE @KeyTagOnIssue BIT
	DECLARE @KeyTagAction NVARCHAR(40)
	DECLARE @KeyTag NVARCHAR(255)
	DECLARE @ChangeDate DATETIME
	DECLARE @ActivityStatusFrom NVARCHAR(255)
	DECLARE @ActivityStatusTo NVARCHAR(255)
	DECLARE @ResolutionStatusFrom NVARCHAR(255)
	DECLARE @ResolutionStatusTo NVARCHAR(255)
	DECLARE @EntryID UNIQUEIDENTIFIER = NULL
	DECLARE @AuditTitleNow NVARCHAR(255)
	DECLARE @AuditContentNow NVARCHAR(MAX)
	DECLARE @Result INT
	DECLARE @SekoiaMapChange BIT
	DECLARE @SekoiaMapName NVARCHAR(255)

	DECLARE @ScheduleID INT
	DECLARE @DelayHours INT
	DECLARE @DelayGuard NVARCHAR(255)
	DECLARE @CancelOnActivityStatusChange INT
	DECLARE @CancelOnResolutionStatusChange INT
	DECLARE @ScheduledDelay DATETIME
	DECLARE @StatusChange NVARCHAR(255)
	DECLARE @AuditTitleFuture NVARCHAR(255)

	DECLARE cur INSENSITIVE CURSOR FOR 
		SELECT UserID, AssetID, AssetIssueID, 
		CASE WHEN SekoiaMapChange = 1 THEN 0 ELSE KeyTagOnIssue END AS KeyTagOnIssue, KeyTagAction, KeyTag, ChangeDate, ActivityStatusFrom, ActivityStatusTo, ResolutionStatusFrom, ResolutionStatusTo, SekoiaMapChange, SekoiaMapName, DelayHours, DelayGuard, CancelOnActivityStatusChange, CancelOnResolutionStatusChange FROM @results 
		
	OPEN cur;
	FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @KeyTagOnIssue, @KeyTagAction, @KeyTag, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @SekoiaMapChange, @SekoiaMapName, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange 
	WHILE @@FETCH_STATUS = 0
	BEGIN			
		BEGIN TRY
		--not available in Workflow Editor - NOT converted to standard issue discussion entry format
			DECLARE @label as varchar(255)

			IF @SekoiaMapChange = 0
				BEGIN
				SET @label = 'Activity Status'			  
				END
			ELSE
				BEGIN
				SET @label = 'Map Status for map: ' + @SekoiaMapName			  
				END	

			SET @AuditTitleNow = 'Status Change - ' + LEFT(@KeyTagAction,10) + ' Key Tag'
			SET @AuditContentNow = 'Key Tag = ' + @KeyTag + '; ' + @label + ': ' + ISNULL(@ActivityStatusFrom,'<none>') + ' to ' + ISNULL(@ActivityStatusTo,'<none>') + '; '
			IF @SekoiaMapChange = 0
				BEGIN
				SET @AuditContentNow  = @AuditContentNow  + 'Resolution Status: '
				+ ISNULL(@ResolutionStatusFrom,'<none>') + ' to ' + ISNULL(@ResolutionStatusTo,'<none>')
				END


			IF @DelayHours IS NOT NULL
				BEGIN
					SET @ScheduledDelay = DATEADD(hour, @DelayHours, @ChangeDate)
					SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
					SET @AuditTitleNow = @AuditTitleNow + ' Delayed for ' + CAST(@DelayHours AS NVARCHAR(20)) + ' hour(s)'
					SET @AuditTitleFuture = 'Status Change from ' + @StatusChange +  ' - Delayed ' + LEFT(@KeyTagAction,10) + ' Key Tag'

					EXEC Diagnostics.spUpsertCategoryNotificationSchedule
						@ScheduledDate = @ScheduledDelay, 
						@DelayGuard = @DelayGuard,
						@CancelOnActivityStatusChange = @CancelOnActivityStatusChange,
						@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
						@CategoryNotificationTypeID = 4,
						@UserID = @UserID,
						@AssetID = @AssetID, 
						@AssetIssueID = @AssetIssueID, 
						@KeyTagOnIssue = @KeyTagOnIssue, 
						@KeyTagAction = @KeyTagAction, 
						@KeyTag = @KeyTag,
						@AuditTitle = @AuditTitleFuture, 
						@AuditContent = @AuditContentNow, 
						@NewCategoryNotificationScheduleID = @ScheduleID

					IF @AssetIssueID > 0
					  BEGIN
						EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitleNow, @AuditContentNow, NULL, 1, NULL
					  END
					ELSE
					  BEGIN
					    EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitleNow, @AuditContentNow, NULL, 1, NULL
					  END
				END
			ELSE
				EXEC Diagnostics.spAddDeleteKeyTagOnStatusChange_Act @UserID, @AssetID, @AssetIssueID, @KeyTagOnIssue, @KeyTagAction, @KeyTag, NULL, @EntryID, @AuditTitleNow, @AuditContentNow
						
			FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @KeyTagOnIssue, @KeyTagAction, @KeyTag, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @SekoiaMapChange, @SekoiaMapName, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange 
		END TRY

		BEGIN CATCH
			SET @ErrorMsg = @ErrorMsg + '; ' + ERROR_MESSAGE()
			FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @KeyTagOnIssue, @KeyTagAction, @KeyTag, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @SekoiaMapChange, @SekoiaMapName, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange 
		END CATCH
	END
	CLOSE cur
	DEALLOCATE cur

	IF @ErrorMsg <> ''
		BEGIN
			RAISERROR( @ErrorMsg,16, 1);
		END
END
GO


GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAddDeleteKeyTagOnStatusChange] TO [TEUser]
    AS [dbo];
GO
