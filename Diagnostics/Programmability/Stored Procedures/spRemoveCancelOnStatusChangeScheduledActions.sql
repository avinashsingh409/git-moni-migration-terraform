﻿-- GRANT was missing from FromVersion_19-3-1-0_Script12_DiscussionAndEmailOverrideForWorkflow.sql

CREATE PROCEDURE [Diagnostics].[spRemoveCancelOnStatusChangeScheduledActions]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] Readonly
AS
BEGIN
	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @AuditTitle NVARCHAR(255)
	DECLARE @AuditContent NVARCHAR(MAX)
	
	BEGIN TRY
		BEGIN																
			DELETE s FROM Diagnostics.tCategoryNotificationSchedule s
				INNER JOIN @statusChanges c ON c.AssetIssueID = s.AssetIssueID
				WHERE (((s.CancelOnActivityStatusChange > 0) AND (ISNULL(c.ActivityStatusFrom, '') != ISNULL(c.ActivityStatusTo, ''))) 
				OR ((s.CancelOnResolutionStatusChange > 0) AND (ISNULL(c.ResolutionStatusFrom, '') != ISNULL(c.ResolutionStatusTo, ''))))
			
			--could iterate through @statusChanges to write blog message for all removed actions from schedule table

			--SET @AuditTitle = 'Removed Delayed Action Because of Status Change Cancel Flag'
			--SET @AuditContent = 'Activity Status: ' + ISNULL(@ActivityStatusFrom,'<none>') + ' to ' + ISNULL(@ActivityStatusTo,'<none>') + '; Resolution Status: '
			--				+ ISNULL(@ResolutionStatusFrom,'<none>') + ' to ' + ISNULL(@ResolutionStatusTo,'<none>')
	
			--	EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitleNow, @AuditContentNow, @ChangeDate, 1, NULL
        END
	END TRY
	BEGIN CATCH
		BEGIN
			SET @ErrorMsg = @ErrorMsg + ' Error removing scheduled actions: ' + ERROR_MESSAGE()
		END
	END CATCH
	
END

GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spRemoveCancelOnStatusChangeScheduledActions] TO [TEUser]
    AS [dbo]
GO