﻿-------

CREATE PROCEDURE [Diagnostics].[spUpdateIssueStatusOnAssetIssueResolutionStatusChange]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] READONLY
AS
BEGIN
	DECLARE @ErrorMsg AS VARCHAR(MAX) = ''
	DECLARE @results AS TABLE
	(
		UserID INT,
		AssetID INT,
		AssetIssueID INT,
		IssueTitle NVARCHAR(255),
		ChangeDate DATETIME,
		ActivityStatusFrom NVARCHAR(255),
		ActivityStatusTo NVARCHAR(255),
		ResolutionStatusFrom NVARCHAR(255),
		ResolutionStatusTo NVARCHAR(255),
		ActivityStatusTargetID INT NOT NULL,
		DelayHours INT NULL,
		DelayGuard NVARCHAR(255) NULL,
		CancelOnActivityStatusChange INT NULL,
		CancelOnResolutionStatusChange INT NULL
	)

	INSERT INTO @results
	SELECT
		s.ChangedBy,
		s.AssetID,
		s.AssetIssueID,
		s.IssueTitle,
		s.ChangeDate,
		s.ActivityStatusFrom,
		s.ActivityStatusTo,
		s.ResolutionStatusFrom,
		s.ResolutionStatusTo,
		n.AutoChangeActivityStatusTarget,
		n.DelayHours,
	    n.DelayGuard,
		n.CancelOnActivityStatusChange, 
		n.CancelOnResolutionStatusChange
	FROM @statusChanges s
	JOIN Diagnostics.tCategoryNotification n ON s.NotificationID = n.CategoryNotificationID
	WHERE s.NotificationTypeID = 9 AND n.AutoChangeActivityStatus = 1

	-- TJC Aug 2019: I assume the intent was to prefer category notification actions where BOTH from and to resolution status match over just a from OR to transition
	-- but the deletes below do not JOIN on AssetIssueID
	-- which means actions for an AssetIssue with just a from OR to match would be deleted when another AssetIssue action matches both
	-- for now, it appears to be a solution in search of a problem; if needed later, it needs to be more 'specific'

	-- delete less specific items where more specific items are available
	--DELETE r FROM @results r
	--	INNER JOIN @results more_specific on more_specific.ResolutionStatusFromID = r.ResolutionStatusFromID
	--	WHERE more_specific.ResolutionStatusToID IS NOT NULL AND r.ResolutionStatusToID IS NULL
	--DELETE r FROM @results r
	--	INNER JOIN @results more_specific on more_specific.ResolutionStatusToID = r.ResolutionStatusToID
	--	WHERE more_specific.ResolutionStatusFromID IS NOT NULL AND r.ResolutionStatusFromID IS NULL

	DECLARE @ScheduleID INT
	DECLARE @DelayHours INT
	DECLARE @DelayGuard NVARCHAR(255)
	DECLARE @CancelOnActivityStatusChange INT = NULL
	DECLARE @CancelOnResolutionStatusChange INT = NULL
	DECLARE @ScheduledDelay DATETIME
	DECLARE @UserID AS INT
	DECLARE @AssetID AS INT
	DECLARE @AssetIssueID AS INT
	DECLARE @ActivityStatusFrom NVARCHAR(255)
	DECLARE @ActivityStatusTo NVARCHAR(255)
	DECLARE @ResolutionStatusFrom NVARCHAR(255)
	DECLARE @ResolutionStatusTo NVARCHAR(255)
	DECLARE @DestinationStatusID INT
	DECLARE @ChangeDate AS DATETIME
	DECLARE @IssueTitle NVARCHAR(255)
	DECLARE @AuditTitleNow NVARCHAR(255)
	DECLARE @AuditContentNow NVARCHAR(MAX)
	DECLARE @AuditTitleFuture NVARCHAR(255)
	DECLARE @AuditContentFuture NVARCHAR(MAX)
	DECLARE @StatusChange NVARCHAR(255)
	DECLARE @OK bit
	
	DECLARE cur CURSOR LOCAL FOR
		SELECT UserID, AssetID, AssetIssueID, IssueTitle, ChangeDate, ActivityStatusFrom, ActivityStatusTo, ResolutionStatusFrom, ResolutionStatusTo, ActivityStatusTargetID,
			DelayHours, DelayGuard, CancelOnActivityStatusChange, CancelOnResolutionStatusChange 
			FROM @results 
	OPEN cur;
	FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @IssueTitle, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
	  , @DestinationStatusID, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
	
	SET @OK = 1
	
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN			
			BEGIN TRY
				BEGIN
				--not available in Workflow Editor - NOT converted to standard issue discussion entry format
				SET @AuditTitleNow = 'Resolution Status Change - Update Activity Status'
				SET @AuditContentNow = 'Resolution status changed from ' + ISNULL(@ResolutionStatusFrom,'<none>') + ' to ' + ISNULL(@ResolutionStatusTo,'<none>') + '. '
				SET @AuditContentFuture = 'Issue ''' + @IssueTitle + ''' updating activity status to: ' + CAST(@DestinationStatusID as nvarchar(10)) + '. '
							+ 'Resolution status changed from ' + ISNULL(@ResolutionStatusFrom,'<none>') + ' to ' + ISNULL(@ResolutionStatusTo,'<none>') + '. '
			
				IF @DelayHours > 0
					BEGIN
					SET @ScheduledDelay = DATEADD(hour,@DelayHours,@ChangeDate)
					SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
					SET @AuditTitleNow = @AuditTitleNow + '  Delayed for  ' + CAST(@DelayHours AS NVARCHAR(20)) + ' hours'
					SET @AuditTitleFuture = 'Resolution Status Change from ' + @StatusChange +  ' - Delayed Update Activity Status Occurred'
				
					EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
						@ScheduledDate = @ScheduledDelay,
						@DelayGuard = @DelayGuard,
						@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
						@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
						@CategoryNotificationTypeID = 9,
						@AssetID = @AssetID,
						@UserID = @UserID,
						@AssetIssueID = @AssetIssueID,
						@SetStatusID = @DestinationStatusID,
						@AssetIssueTitle = @IssueTitle,
						@AuditTitle = @AuditTitleFuture,
						@AuditContent = @AuditContentFuture,
						@NewCategoryNotificationScheduleID = @ScheduleID

					EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitleNow, @AuditContentNow, NULL, 1, NULL
					END
				ELSE
					BEGIN
					EXEC [Diagnostics].[spUpdateIssueStatusOnAssetIssueResolutionStatusChange_Act] 
						@DestinationStatusID = @DestinationStatusID,
						@AssetIssueID = @AssetIssueID,
						@AssetID = @AssetID,
						@UserID = @UserID,
						@AuditTitle = @AuditTitleNow,
						@AuditContent = @AuditContentFuture,  --has the issue & activity status change details
						@ChangeDate = NULL
					END
				FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @IssueTitle, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
					, @DestinationStatusID, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
				END
		END TRY
		BEGIN CATCH
			BEGIN
			SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
			FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @IssueTitle, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
				, @DestinationStatusID, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
			END
		END CATCH
		END
	ELSE
		BEGIN
		SET @OK = 0
		END
	CLOSE cur;
	DEALLOCATE cur;

	IF @ErrorMsg<>''
	  BEGIN
	  RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpdateIssueStatusOnAssetIssueResolutionStatusChange]   TO [TEUser]
    AS [dbo];
GO