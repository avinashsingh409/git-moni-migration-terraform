﻿CREATE PROCEDURE [Diagnostics].[spAssetIssuesCount] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID int = NULL,
	@startDate datetime = '1/1/1980',
	@endDate datetime = '1/1/2050',
	@changeStartDate datetime = '1/1/1980',
	@changeEndDate datetime = '1/1/2050',
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@activityStatus NVARCHAR(255) = NULL,
	@resolutionStatus NVARCHAR(255) = NULL,
	@issueTypeID int = NULL,
	@issueClassTypeID int = NULL,
	@startIndex int = 1, 
	@endIndex int = 15
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET ARITHABORT ON
    
	-- Insert statements for procedure here
	SELECT COUNT(Diagnostics.vAssetIssue.AssetIssueID) 
	FROM Diagnostics.vAssetIssue
	--Join to provide security 
	INNER JOIN Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,@AssetID) securityCheck 
		ON securityCheck.AssetID=Diagnostics.vAssetIssue.AssetID
	WHERE (Diagnostics.vAssetIssue.CreateDate BETWEEN @startDate AND @endDate) AND
	(Diagnostics.vAssetIssue.ChangeDate BETWEEN @changeStartDate AND @changeEndDate) AND
	(@closeStartDate IS NULL OR (Diagnostics.vAssetIssue.CloseDate > @closeStartDate)) AND
	(@closeEndDate IS NULL OR (Diagnostics.vAssetIssue.CloseDate < @closeENDDate)) AND
	(@activityStatus IS NULL OR (Diagnostics.vAssetIssue.[ActivityStatus] = @activityStatus )) AND 
	(@resolutionStatus IS NULL OR (Diagnostics.vAssetIssue.[ResolutionStatus] = @resolutionStatus )) AND 
	(@issueTypeID IS NULL OR (Diagnostics.vAssetIssue.IssueTypeID = @issueTypeID)) AND
	(@issueClassTypeID IS NULL OR (Diagnostics.vAssetIssue.IssueClassTypeID = @issueClassTypeID))
	
END
GO

GO
GRANT EXECUTE
	ON OBJECT::[Diagnostics].[spAssetIssuesCount] TO [TEUser]
	AS [dbo];