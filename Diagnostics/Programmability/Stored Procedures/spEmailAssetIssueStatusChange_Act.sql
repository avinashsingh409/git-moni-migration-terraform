﻿----

-----
--this is no longer called from the delayed action Lambda function
--the updated Lambda function is handling the email, attachments and issue discussion entries for instant and scheduled emails
--updated to use AssetIssue blog and datetimeoffset for consistency

CREATE PROCEDURE [Diagnostics].[spEmailAssetIssueStatusChange_Act]
	@UserID INT,
	@AssetID INT,
	@AssetIssueID INT, 
	@EmailSubject NVARCHAR(MAX),
	@EmailToList NVARCHAR(MAX),
	@EmailCcList NVARCHAR(MAX),
	@EmailBccList NVARCHAR(MAX),
	@EmailBody NVARCHAR(MAX),
	@EmailCategory NVARCHAR(255),
	@FormatBodyAsHTML BIT = 1,
	@CreatedBy INT,
	@CreateDate DATETIMEOFFSET(7) ,
	@AuditTitle NVARCHAR(255),
	@AuditContent NVARCHAR(MAX)
AS
BEGIN
	DECLARE @NewID UNIQUEIDENTIFIER
	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	
	SET @CreateDate = ISNULL(@CreateDate, SYSDATETIMEOFFSET())

	BEGIN TRY
		BEGIN
			IF (Coalesce(@EmailToList, @EmailCcList, @EmailBccList) is not null)
				BEGIN
					EXEC Notification.spSaveEmailQueue 
						@EmailSubject = @EmailSubject,
						@EmailToList = @EmailToList, 
						@EmailCcList = @EmailCcList,
						@EmailBccList = @EmailBccList, 
						@EmailBody = @EmailBody, 
						@CreatedBy = @CreatedBy,
						@CreateDate = @CreateDate,
						@EmailCategory = @EmailCategory,
						@FormatBodyAsHTML = @FormatBodyAsHTML,
						@NewID = @NewID OUTPUT
					EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, @CreateDate, 1, NULL
				END
		END
	END TRY
	BEGIN CATCH
		BEGIN
			SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
		END
	END CATCH

	IF @ErrorMsg <> ''
	  BEGIN
		RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spEmailAssetIssueStatusChange_Act] TO [TEUser]
    AS [dbo];
GO