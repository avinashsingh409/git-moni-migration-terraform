﻿-----

CREATE PROCEDURE [Diagnostics].[spCreateActionPlanIssueStatusChange_Act]
	@AssetIssueID INT,
	@ActionPlanAssignedToEmail NVARCHAR(255),
	@ActionPlanDate DATETIME,
	@AssetIssueUserAssignedAction NVARCHAR(MAX),
	@UserName NVARCHAR(255),
	@AssetID INT,
	@UserID INT,
	@AuditTitle NVARCHAR(255),
	@AuditContent NVARCHAR(MAX),
	@ChangeDate DATETIMEOFFSET(7) 
AS
BEGIN
	DECLARE @ErrorMsg AS VARCHAR(MAX) = ''
	DECLARE @NewAssetIssueActionPlanID AS INT

	SET @ChangeDate = ISNULL(@ChangeDate, SYSDATETIMEOFFSET())

	EXEC Diagnostics.spUpsertAssetIssueActionPlan NULL, @AssetIssueID, 0, @ActionPlanAssignedToEmail, @ActionPlanDate, @AssetIssueUserAssignedAction, 'Open', @UserName, @NewAssetIssueActionPlanID OUTPUT
	IF @NewAssetIssueActionPlanID IS NULL
		BEGIN
			SET @ErrorMsg = @ErrorMsg + '; Failed to add Action Plan'
		END

	EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, @ChangeDate, 0, NULL

	IF @ErrorMsg <> ''
		BEGIN
			PRINT @ErrorMsg;
		END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spCreateActionPlanIssueStatusChange_Act] TO [TEUser]
    AS [dbo];
GO

