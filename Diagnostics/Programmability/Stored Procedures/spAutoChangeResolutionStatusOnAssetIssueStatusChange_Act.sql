﻿------

CREATE PROCEDURE [Diagnostics].[spAutoChangeResolutionStatusOnAssetIssueStatusChange_Act]
	@SetResolutionStatusID INT,
	@AssetIssueID INT,
	@AssetID INT,
	@UserID INT,
	@AuditTitle NVARCHAR(255),   -- action name text
	@AuditContent NVARCHAR(MAX),  -- should be empty
	@ChangeDate DATETIMEOFFSET(7)
AS
BEGIN
	DECLARE @ResStatusFrom NVARCHAR(255)
	DECLARE @ResStatusUpdate NVARCHAR(255)
	DECLARE @AssetName NVARCHAR(255)
	DECLARE @UserName NVARCHAR(50)
	
	SET @ChangeDate = ISNULL(@ChangeDate, SYSDATETIMEOFFSET())
	
	IF ((@AssetIssueID > 0) AND (@SetResolutionStatusID > 0))
	BEGIN
		SELECT @UserName = Username FROM AccessControl.tUser WHERE SecurityUserID = @UserID
		
		SELECT @AssetName = AssetDesc FROM Asset.tAsset WHERE AssetID = @AssetID

		SELECT @ResStatusFrom = AssetIssueResolutionStatusTypeDesc 
		  FROM Diagnostics.tAssetIssueResolutionStatusType t
		  INNER JOIN Diagnostics.tAssetIssue i ON i.AssetIssueID = @AssetIssueID AND i.ResolutionStatusID = t.AssetIssueResolutionStatusTypeID
		
		SELECT @ResStatusUpdate = AssetIssueResolutionStatusTypeDesc 
		  FROM Diagnostics.tAssetIssueResolutionStatusType
		  WHERE AssetIssueResolutionStatusTypeID = @SetResolutionStatusID   

		SET @AuditContent = '<p>Item Status was changed on ' + CONVERT(NVARCHAR, CAST(@ChangeDate as DATETIME), 0) + ' by an automated action triggered by: ' + @UserName 
			      + '</p><p>' + @AuditTitle + '</p><p>Entity Status was changed for: ' + @AssetName 
				  + '</p><p>Old Status: ' + @ResStatusFrom + '</p><p>Updated Status: ' + @ResStatusUpdate + '</p>' 
		SET @AuditTitle = 'Status Changed'

		UPDATE Diagnostics.tAssetIssue SET ResolutionStatusID = @SetResolutionStatusID WHERE AssetIssueID = @AssetIssueID
	
		EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, @ChangeDate, 0, NULL  
	END
	ELSE
	BEGIN
		RAISERROR( 'Invalid parameter in Diagnostics.spAutoChangeResolutionStatusOnAssetIssueStatusChange_Act', 16, 1);
	END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAutoChangeResolutionStatusOnAssetIssueStatusChange_Act] TO [TEUser]
    AS [dbo];
GO