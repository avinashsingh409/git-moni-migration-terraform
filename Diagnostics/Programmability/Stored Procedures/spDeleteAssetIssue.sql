﻿-- =============================================
-- Author:		Josh Bowen
-- Create date: 9/30/2013
-- Description:	Delete an Asset Issue
-- =============================================
CREATE PROCEDURE Diagnostics.spDeleteAssetIssue 
	-- Add the parameters for the stored procedure here
	@assetIssueID int = 0,
	@securityUserID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @assetIssueID > 0
	  BEGIN  
		UPDATE Diagnostics.tAssetIssue
		SET IsHidden = 1 
		WHERE AssetIssueID = @assetIssueID AND 
		(CreatedByUserID = @securityUserID OR CreatedByUserID IS NULL )
	  END
END

GO
GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spDeleteAssetIssue] TO [TEUser]
    AS [dbo];

