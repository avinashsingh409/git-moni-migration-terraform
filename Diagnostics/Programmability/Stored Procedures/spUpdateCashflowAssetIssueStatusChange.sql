﻿-----

CREATE PROCEDURE [Diagnostics].[spUpdateCashflowAssetIssueStatusChange]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] READONLY
AS
Begin
	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @UserID as int
	DECLARE @AssetID as int
	DECLARE @AssetIssueID INT
	DECLARE @Type as nvarchar(255)
	DECLARE @Value as float
	DECLARE @ScheduleID INT
	DECLARE @ChangeDate DATETIME
	DECLARE @DelayHours INT
	DECLARE @DelayDays INT = 0
	DECLARE @DelayBreakout NVARCHAR(255) = ''
	DECLARE @DelayGuard NVARCHAR(255)
	DECLARE @CancelOnActivityStatusChange INT = NULL
	DECLARE @CancelOnResolutionStatusChange INT = NULL
	DECLARE @ScheduledDelay DATETIME
	DECLARE @AuditTitle NVARCHAR(255)
	DECLARE @AuditContent NVARCHAR(MAX)
	DECLARE @AuditTitleSched NVARCHAR(255)
	DECLARE @AuditContentSched NVARCHAR(MAX)
	DECLARE @StatusChange NVARCHAR(255)
	DECLARE @ActionName NVARCHAR(255)
	DECLARE @UserName NVARCHAR(128)
	DECLARE @ActivityStatusFrom NVARCHAR(255)
	DECLARE @ActivityStatusTo NVARCHAR(255)
	DECLARE @ResolutionStatusFrom NVARCHAR(255)
	DECLARE @ResolutionStatusTo NVARCHAR(255)

		-- incoming (@statusChanges) will have assetID from AssetIssue row
		-- only update CF/map status for asset with matching AssetClassTypeID defined in tCategoryNotification row  

	DECLARE cur CURSOR LOCAL FOR
		SELECT 
			s.ChangedBy,
			a.AssetID,
			s.AssetIssueID,
			n.CashflowType,
			n.CashflowValue,
			s.ChangeDate,
			s.ActivityStatusFrom,
			s.ActivityStatusTo,
			s.ResolutionStatusFrom,
			s.ResolutionStatusTo,
			n.DelayHours,
			n.DelayGuard,
			n.CancelOnActivityStatusChange, 
			n.CancelOnResolutionStatusChange,
			ISNULL(n.ActionName,''),
			u.UserName
			FROM @statusChanges s
			INNER JOIN Diagnostics.tCategoryNotification n On s.NotificationID = n.CategoryNotificationID AND n.UpdateCashflow = 1
			INNER JOIN Asset.tAsset a ON a.AssetID = s.AssetID AND a.AssetClassTypeID = n.AssetClassTypeID	
			INNER JOIN AccessControl.tUser u ON s.ChangedBy = u.SecurityUserID
			WHERE s.NotificationTypeID = 2 AND n.UpdateCashflow = 1
	OPEN cur
	FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Type, @Value, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
		, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange, @ActionName, @UserName

	DECLARE @Ok BIT
	SET @Ok = 1

	WHILE @Ok = 1
		IF @@FETCH_STATUS = 0
			BEGIN
			BEGIN TRY
				SET @AuditTitle = 'Action Name: ' + @ActionName

				IF (ISNULL(@ResolutionStatusFrom,' ') <> ISNULL(@ResolutionStatusTo,' '))   --status change portion of discussion entry so Lambda/_Act doesn't have to build it 
					BEGIN
					SET @AuditContent = '<p>Old Status: ' + ISNULL(@ResolutionStatusFrom,'<none>') + '</p><p>New Status: ' + ISNULL(@ResolutionStatusTo,'<none>') + '</p>'
					END
				ELSE
					BEGIN
					SET @AuditContent = '<p>Old Status: ' + ISNULL(@ActivityStatusFrom,'<none>') + '</p><p>New Status: ' + ISNULL(@ActivityStatusTo,'<none>') + '</p>'
					END

				IF @DelayHours > 0
					BEGIN
					SET @DelayDays = @DelayHours / 24
					SET @DelayHours = @DelayHours % 24
					IF @DelayDays > 0 
						BEGIN
						SET @DelayBreakout = CAST(@DelayDays AS NVARCHAR(20)) + ' day'
						IF @DelayDays > 1
							BEGIN
							SET @DelayBreakout = @DelayBreakout + 's'
							END
						IF @DelayHours > 0
							BEGIN
							SET @DelayBreakout = @DelayBreakout + ' and '
							END
						END
					IF @DelayHours > 0
						BEGIN
						SET @DelayBreakout = @DelayBreakout + CAST(@DelayHours AS NVARCHAR(20)) + ' hour'
						IF @DelayHours > 1
							BEGIN
							SET @DelayBreakout = @DelayBreakout + 's'
							END
						END

					SET @ScheduledDelay = dateadd(hour,@DelayHours,@ChangeDate)
					SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
					SET @AuditTitleSched = 'Status Change - ''' + @ActionName + ''' action scheduled for ' + @DelayBreakout + ' delay'
					SET @AuditContentSched = @AuditContent + '<p>' + @AuditTitle + '</p><p>Delay Action for: ' + @DelayBreakout + '</p>'													   
					
					EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
						@ScheduledDate = @ScheduledDelay,
						@DelayGuard = @DelayGuard,
						@CategoryNotificationTypeID = 2,
						@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
						@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
						@AssetID = @AssetID,
						@AssetIssueID = @AssetIssueID,
						@CashflowType = @Type,
						@CashflowValue = @Value,
						@UserID = @UserID,
						@AuditTitle = @AuditTitle,
						@AuditContent = @AuditContent, 
						@NewCategoryNotificationScheduleID = @ScheduleID OUTPUT
					
					IF @AssetIssueID > 0
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitleSched, @AuditContentSched, NULL, 1, NULL  -- content blank for this entry, populated for entry during _Act 
						END
					ELSE
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitleSched, @AuditContentSched, NULL, 1, NULL  -- content blank for this entry, populated for entry during _Act 
						END
				END
				ELSE
				BEGIN
					EXEC Diagnostics.spUpdateCashFlowAssetIssueStatusChange_Act 
						@UserID = @UserID, 
						@AssetID = @AssetID, 
						@AssetIssueID = @AssetIssueID,
						@Type = @Type, 
						@Value = @Value, 
						@AuditTitle = @AuditTitle,
						@AuditContent = @AuditContent
				END

				FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Type, @Value, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
					, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange, @ActionName, @UserName

			END TRY
			BEGIN CATCH
				 BEGIN
				 SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
				 FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Type, @Value, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
					, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange, @ActionName, @UserName
				 END
			END CATCH
			END
		ELSE
			BEGIN
			SET @Ok = 0
			END
	CLOSE cur
	DEALLOCATE cur

	IF @ErrorMsg <> ''
	  BEGIN
	  RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpdateCashflowAssetIssueStatusChange] TO [TEUser]
    AS [dbo]
GO