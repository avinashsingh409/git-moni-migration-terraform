﻿-----

CREATE PROCEDURE [Diagnostics].[spCreateActionPlanIssueStatusChange]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] Readonly
AS
BEGIN
	DECLARE @ErrorMsg as varchar(max) = ''

	DECLARE @results AS TABLE
	(
	  UserID int,
	  UserName nvarchar(255),
	  AssetIssueID int,
	  AssetID int,
	  ActionPlanAssignedToEmail nvarchar(255),
	  AssetIssueUserAssignedAction nvarchar(max),
	  ActionPlanActionDate int,
	  ChangeDate datetime,
	  ResolutionStatusFrom nvarchar(255),
	  ResolutionStatusTo nvarchar(255),
	  DelayHours int, 
	  DelayGuard nvarchar(255),
	  CancelOnActivityStatusChange INT NULL,
	  CancelOnResolutionStatusChange INT NULL
	)
	
	INSERT INTO @results
	SELECT 
		s.ChangedBy,
		u.Username,
		s.AssetIssueID,
		s.AssetID,
		n.ActionPlanAssignedToEmail,
		n.AssetIssueUserAssignedAction,
		n.ActionPlanActionDate,
		s.ChangeDate,
		s.ResolutionStatusFrom,
		s.ResolutionStatusTo,
		n.DelayHours, 
		n.DelayGuard,
		n.CancelOnActivityStatusChange,
		n.CancelOnResolutionStatusChange
	FROM @statusChanges s	
	Join Diagnostics.tCategoryNotification n ON s.NotificationID = n.CategoryNotificationID
	Left Join AccessControl.tUser u On s.ChangedBy= u.SecurityUserID
	WHERE s.NotificationTypeID = 6 AND n.AddActionPlan = 1

	DECLARE @ScheduleID INT
	DECLARE @newAssetIssueActionPlanID as int
	DECLARE @UserID as int
	DECLARE @UserName as nvarchar(255)
	DECLARE @AssetIssueID as int
	DECLARE @AssetID as int
	DECLARE @ActionPlanAssignedToEmail as nvarchar(255)
	DECLARE @AssetIssueUserAssignedAction as nvarchar(max)
	DECLARE @ActionPlanActionDate as int
	DECLARE @ActionPlanDate as datetime
	DECLARE @ChangeDate as datetime
	DECLARE @ResolutionStatusFrom nvarchar(255)
	DECLARE @ResolutionStatusTo nvarchar(255)
	DECLARE @DelayHours as int
	DECLARE @DelayGuard nvarchar(255)
	DECLARE @CancelOnActivityStatusChange INT = NULL
	DECLARE @CancelOnResolutionStatusChange INT = NULL
	DECLARE @ScheduledDelay datetime
	DECLARE @AuditTitleNow NVARCHAR(255)
	DECLARE @AuditContentNow NVARCHAR(MAX)
	DECLARE @AuditTitleFuture NVARCHAR(255)
	DECLARE @AuditContentFuture NVARCHAR(MAX)
	DECLARE @StatusChange NVARCHAR(255)

	DECLARE cur INSENSITIVE CURSOR FOR 
		SELECT UserID, UserName, AssetIssueID, AssetID, ActionPlanAssignedToEmail, AssetIssueUserAssignedAction, ActionPlanActionDate, ChangeDate, ResolutionStatusFrom, ResolutionStatusTo, DelayHours, DelayGuard, CancelOnActivityStatusChange, CancelOnResolutionStatusChange FROM @results 
		
	OPEN cur;
	FETCH NEXT FROM cur INTO @UserID, @UserName, @AssetIssueID, @AssetID, @ActionPlanAssignedToEmail, @AssetIssueUserAssignedAction, @ActionPlanActionDate, @ChangeDate, @ResolutionStatusFrom, @ResolutionStatusTo, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
	
	IF @ActionPlanActionDate is not NULL
		BEGIN
			set @ActionPlanDate = DATEADD(day, @ActionPlanActionDate, GETDATE())
		END

	WHILE @@FETCH_STATUS = 0
	BEGIN			
		BEGIN TRY
		--not available in Workflow Editor - NOT converted to standard issue discussion entry format
			 SET @AuditContentNow = 'Resolution Status: ' + ISNULL(@ResolutionStatusFrom,'<none>') + ' to ' + ISNULL(@ResolutionStatusTo,'<none>')

			IF @DelayHours IS NULL
				BEGIN
					SET @AuditTitleNow = 'Status Change - Create Action Plan for "' + @AssetIssueUserAssignedAction + '"'
					EXEC Diagnostics.spCreateActionPlanIssueStatusChange_Act @AssetIssueID, @ActionPlanAssignedToEmail, @ActionPlanDate, @AssetIssueUserAssignedAction, @UserName, @AssetID, @UserID, @AuditTitleNow, @AuditContentNow, NULL
				END
			ELSE
				BEGIN
					SET @ScheduledDelay = DATEADD(hour, @DelayHours, @ChangeDate)
					SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
					SET @AuditTitleNow = 'Status Change - Create Action Plan Delayed for ' + CAST(@DelayHours AS NVARCHAR(20)) + ' hours'
					SET @AuditTitleFuture = 'Status Change from ' + @StatusChange +  ' - Delayed Create Action Plan Occurred'
					SET @AuditContentFuture = 'Created action plan for "' + @AssetIssueUserAssignedAction + '"'

					EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
						@AssetID = @AssetID,
						@AssetIssueID = @AssetIssueID,
						@ScheduledDate=@ScheduledDelay, 
						@DelayGuard=@DelayGuard,
						@CategoryNotificationTypeID = 6,
						@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
						@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
						@AssignedToEmail=@ActionPlanAssignedToEmail,
						@ActionPlanDate=@ActionPlanDate,
						@UserAssignedAction=@AssetIssueUserAssignedAction,
						@AuditTitle=@AuditTitleFuture,
						@AuditContent=@AuditContentFuture, 
						@NewCategoryNotificationScheduleID=@ScheduleID

					EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitleNow, @AuditContentNow, NULL, 1, NULL
				END
			FETCH NEXT FROM cur INTO @UserID, @UserName, @AssetIssueID, @AssetID, @ActionPlanAssignedToEmail, @AssetIssueUserAssignedAction, @ActionPlanActionDate, @ChangeDate, @ResolutionStatusFrom, @ResolutionStatusTo, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
		END TRY

		BEGIN CATCH
			SET @ErrorMsg = @ErrorMsg + '; ' + ERROR_MESSAGE()

			FETCH NEXT FROM cur INTO @UserID, @UserName, @AssetIssueID, @AssetID, @ActionPlanAssignedToEmail, @AssetIssueUserAssignedAction, @ActionPlanActionDate, @ChangeDate, @ResolutionStatusFrom, @ResolutionStatusTo, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
		END CATCH
	END
	CLOSE cur;
	DEALLOCATE cur;

	IF @ErrorMsg <> ''
	BEGIN
		PRINT @ErrorMsg;
	END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spCreateActionPlanIssueStatusChange] TO [TEUser]
    AS [dbo];
GO