﻿-----

CREATE Procedure [Diagnostics].[spUpdateIssueStatusOnAssetIssueResolutionStatusChange_Act]
	@DestinationStatusID INT,
	@AssetIssueID INT,
	@AssetID INT,
	@UserID INT,
	@auditTitle NVARCHAR(255),
	@auditContent NVARCHAR(MAX),
	@ChangeDate DATETIMEOFFSET(7)
AS
BEGIN
	SET @ChangeDate = ISNULL(@ChangeDate, SYSDATETIMEOFFSET())

	UPDATE Diagnostics.tAssetIssue SET ActivityStatusID = @DestinationStatusID WHERE AssetIssueID = @AssetIssueID
	
	EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @auditTitle, @auditContent, @ChangeDate, 0, NULL
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpdateIssueStatusOnAssetIssueResolutionStatusChange_Act]   TO [TEUser]
    AS [dbo];
GO