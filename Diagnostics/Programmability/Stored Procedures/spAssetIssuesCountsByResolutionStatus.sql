﻿CREATE PROCEDURE [Diagnostics].[spAssetIssuesCountsByResolutionStatus] 
	@securityUserID int,
	@assetID int = NULL,
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@changeStartDate datetime = NULL,
	@changeEndDate datetime = NULL,
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@openDurationHigh int = NULL,
	@openDurationLow int = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	@impactCostHigh float = NULL,
	@impactCostLow float = NULL,
	@assignedTo NVARCHAR(255) = NULL,
	@scorecard bit = NULL,
	@priority NVARCHAR(255) = NULL,
	@changedBy NVARCHAR(255) = NULL,
	@activityStatus Base.tpIntList READONLY,
	@resolutionStatus Base.tpIntList READONLY,
	@issueTypeID Base.tpIntList  READONLY,
	@issueClassTypeID Base.tpIntList READONLY,
	@issueCategoryType Base.tpIntList READONLY,
	@issueCauseType Base.tpIntList READONLY,
	@taggedAssetIssueID Base.tpIntList READONLY,
	@includeAssociatedAssets bit = 0
AS
BEGIN
	SET NOCOUNT ON;  -- to prevent extra result sets from interfering with SELECT statements
    SET ARITHABORT ON
	
    DECLARE @owningAssetsResolutionStatus TABLE(ResolutionStatusID int not null,AssetIssueID int,ResolutionStatus nvarchar(255) not null, CategoryType nvarchar(255) null);
	DECLARE @assetByResolutionStatus TABLE(ResolutionStatusID int not null, AssetIssueID int,ResolutionStatus nvarchar(255) not null, CategoryType nvarchar(255) null);
    DECLARE @assets base.tpIntList;
    
    insert into @assets select assetid from Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,@assetID) 
    --associated assets
	IF @includeAssociatedAssets = 1
	BEGIN
		DECLARE @associatedAssets base.tpIntList;
		DECLARE @associatedAssetsResolutionStatus TABLE(ResolutionStatusID int not null,AssetIssueID int,ResolutionStatus nvarchar(255) not null, CategoryType nvarchar(255) null);
		INSERT INTO @associatedAssets SELECT DISTINCT a.AssetID FROM Diagnostics.vAssetIssue a
		LEFT JOIN Diagnostics.tAssetIssueKeywordMap b ON b.AssetIssueID = a.AssetIssueID
		INNER JOIN Diagnostics.tAssetIssueAssociatedAsset c ON c.AssetIssueID = a.AssetIssueID
		WHERE c.AssetID IN (select id from @assets)

		IF EXISTS(SELECT 1/0 FROM @associatedAssets)
		BEGIN
		INSERT INTO @associatedAssetsResolutionStatus SELECT ai.[ResolutionStatusID] as ResolutionStatusID , ai.AssetIssueID as AssetIssueID, rt.AssetIssueResolutionStatusTypeDesc as ResolutionStatus, ct.CategoryAbbrev as CategoryType 
		FROM
		[Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
								@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
								@openDurationHigh, @openDurationLow, @id, @title, @impactCostHigh,@impactCostLow,
								@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,
								@issueCategoryType, @issueCauseType,@taggedAssetIssueID,@associatedAssets, @scorecard,
								@priority, @changedBy) filter  
		INNER JOIN Diagnostics.tAssetIssue ai ON filter.AssetIssueID = ai.AssetIssueID
		INNER JOIN Diagnostics.tAssetIssueAssociatedAsset c on c.AssetIssueID = filter.AssetIssueID
		INNER JOIN Diagnostics.tAssetIssueResolutionStatusType rt ON ai.ResolutionStatusID = rt.AssetIssueResolutionStatusTypeID
		INNER JOIN Diagnostics.tAssetIssueCategoryType ct ON ai.AssetIssueCategoryTypeID = ct.AssetIssueCategoryTypeID
		WHERE c.AssetID IN (select id from @assets)
		END
    END
	INSERT INTO @owningAssetsResolutionStatus
	SELECT 	ai.[ResolutionStatusID] as ResolutionStatusID , ai.AssetIssueID as AssetIssueID, rt.AssetIssueResolutionStatusTypeDesc as ResolutionStatus, ct.CategoryAbbrev as CategoryType
	FROM
	[Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
							@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
							@openDurationHigh, @openDurationLow, @id, @title, @impactCostHigh,@impactCostLow,
							@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,
							@issueCategoryType, @issueCauseType,@taggedAssetIssueID,@assets, @scorecard,
							@priority, @changedBy) filter
	INNER JOIN Diagnostics.tAssetIssue ai ON filter.AssetIssueID = ai.AssetIssueID
	INNER JOIN Diagnostics.tAssetIssueResolutionStatusType rt ON ai.ResolutionStatusID = rt.AssetIssueResolutionStatusTypeID
    INNER JOIN Diagnostics.tAssetIssueCategoryType ct ON ai.AssetIssueCategoryTypeID = ct.AssetIssueCategoryTypeID

	INSERT INTO @assetByResolutionStatus SELECT * FROM @owningAssetsResolutionStatus
	UNION
	SELECT * FROM @associatedAssetsResolutionStatus WHERE AssetIssueID NOT IN (SELECT AssetIssueID FROM @owningAssetsResolutionStatus)

	SELECT ResolutionStatusID,COUNT(AssetIssueID) as IssuesCount,ResolutionStatus, CategoryType FROM @assetByResolutionStatus 
	GROUP BY ResolutionStatusID, ResolutionStatus, CategoryType

END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAssetIssuesCountsByResolutionStatus] TO [TEUser]
    AS [dbo];
GO


