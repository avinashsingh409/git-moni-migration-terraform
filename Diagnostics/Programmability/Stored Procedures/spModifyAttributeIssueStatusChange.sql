﻿------

CREATE PROCEDURE [Diagnostics].[spModifyAttributeIssueStatusChange]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] Readonly
As
Begin
	DECLARE @ErrorMsg VARCHAR(MAX) = ''

	DECLARE @results TABLE
	(
	  UserID INT,
	  AssetID INT,
	  AssetIssueID INT,
	  AttributeName NVARCHAR(255),
	  AttributeValue NVARCHAR(MAX),
	  ChangeDate DATETIME,
	  ActivityStatusFrom NVARCHAR(255),
	  ActivityStatusTo NVARCHAR(255),
	  ResolutionStatusFrom NVARCHAR(255),
	  ResolutionStatusTo NVARCHAR(255),
	  DelayHours INT NULL,
	  DelayGuard NVARCHAR(255) NULL,
	  CancelOnActivityStatusChange INT NULL, 
	  CancelOnResolutionStatusChange INT NULL,
	  ActionName NVARCHAR(255) NULL,
	  UserName NVARCHAR(50) NULL
	)

	INSERT INTO @results
	Select 
		s.ChangedBy,
		s.AssetID,
		s.AssetIssueID,
		n.AttributeName,
		n.AttributeValue,
		s.ChangeDate,
		s.ActivityStatusFrom,
		s.ActivityStatusTo,
		s.ResolutionStatusFrom,
		s.ResolutionStatusTo,
		n.DelayHours,
	    n.DelayGuard,
		n.CancelOnActivityStatusChange, 
		n.CancelOnResolutionStatusChange,
		ISNULL(n.ActionName,''),
		u.UserName
	FROM @statusChanges s	
	INNER JOIN Diagnostics.tCategoryNotification n On s.NotificationID = n.CategoryNotificationID
	INNER JOIN AccessControl.tUser u ON s.ChangedBy = u.SecurityUserID
	WHERE s.NotificationTypeID = 3 AND n.ModifyAttribute = 1
	
	DECLARE @ScheduleID INT
	DECLARE @CategoryIDs Base.tpIntList   --not used currently
	DECLARE @UserID INT
	DECLARE @AssetID INT
	DECLARE @AssetIssueID INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Value NVARCHAR(MAX)
	DECLARE @PrevValue NVARCHAR(MAX)
	DECLARE @ActivityStatusFrom NVARCHAR(255)
	DECLARE @ActivityStatusTo NVARCHAR(255)
	DECLARE @ResolutionStatusFrom NVARCHAR(255)
	DECLARE @ResolutionStatusTo NVARCHAR(255)
	DECLARE @ChangeDate DATETIME
	DECLARE @AuditTitle NVARCHAR(255)
	DECLARE @AuditContent NVARCHAR(MAX)
	DECLARE @AuditTitleSched NVARCHAR(255)
	DECLARE @AuditContentSched NVARCHAR(MAX)
	DECLARE @DelayHours INT
	DECLARE @DelayDays INT = 0
	DECLARE @DelayBreakout NVARCHAR(255) = ''
	DECLARE @DelayGuard NVARCHAR(255)
	DECLARE @CancelOnActivityStatusChange INT = NULL
	DECLARE @CancelOnResolutionStatusChange INT = NULL
	DECLARE @ScheduledDelay DATETIME
	DECLARE @StatusChange NVARCHAR(255)
	DECLARE @ActionName NVARCHAR(255)
	DECLARE @AssetName NVARCHAR(255)
	DECLARE @UserName NVARCHAR(128)
	
	DECLARE cur CURSOR LOCAL FOR
		SELECT UserID, AssetID, AssetIssueID, AttributeName, AttributeValue, ChangeDate, ActivityStatusFrom, ActivityStatusTo, ResolutionStatusFrom, ResolutionStatusTo
			, DelayHours, DelayGuard, CancelOnActivityStatusChange, CancelOnResolutionStatusChange, ActionName, UserName FROM @results 
	OPEN cur;
	FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Name, @Value, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
		, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange, @ActionName, @UserName
	DECLARE @OK bit
	SET @OK = 1
	
	WHILE @OK = 1
		IF @@FETCH_STATUS = 0
			BEGIN			
			BEGIN TRY
				BEGIN
					SET @AuditTitle = 'Action Name: ' + @ActionName

					IF (ISNULL(@ResolutionStatusFrom,' ') <> ISNULL(@ResolutionStatusTo,' '))   --status change portion of discussion entry so Lambda doesn't have to build it 
					  BEGIN
						SET @AuditContent = '<p>Old Status: ' + ISNULL(@ResolutionStatusFrom,'<none>') + '</p><p>New Status: ' + ISNULL(@ResolutionStatusTo,'<none>') + '</p>'
					  END
					ELSE
					  BEGIN
						SET @AuditContent = '<p>Old Status: ' + ISNULL(@ActivityStatusFrom,'<none>') + '</p><p>New Status: ' + ISNULL(@ActivityStatusTo,'<none>') + '</p>'
					  END
					  
					IF @DelayHours > 0
					  BEGIN
						SET @DelayDays = @DelayHours / 24
						SET @DelayHours = @DelayHours % 24
						IF @DelayDays > 0 
						  BEGIN
							SET @DelayBreakout = CAST(@DelayDays AS NVARCHAR(20)) + ' day'
							IF @DelayDays > 1
							  BEGIN
							    SET @DelayBreakout = @DelayBreakout + 's'
							  END
							IF @DelayHours > 0
							  BEGIN
							    SET @DelayBreakout = @DelayBreakout + ' and '
							  END
						  END
						IF @DelayHours > 0
						  BEGIN
							SET @DelayBreakout = @DelayBreakout + CAST(@DelayHours AS NVARCHAR(20)) + ' hour'
							IF @DelayHours > 1
							  BEGIN
							    SET @DelayBreakout = @DelayBreakout + 's'
							  END
						  END
						
						SET @ScheduledDelay = DATEADD(hour,@DelayHours,@ChangeDate)
						SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
						SET @AuditTitleSched = 'Status Change - ''' + @ActionName + ''' action scheduled for ' + @DelayBreakout + ' delay'
						SET @AuditContentSched = @AuditContent + '<p>' + @AuditTitle + '</p><p>Delay Action for: ' + @DelayBreakout + '</p>'													   

						EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
							@ScheduledDate = @ScheduledDelay, 
							@DelayGuard = @DelayGuard,
							@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
							@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
							@CategoryNotificationTypeID = 3,
							@UserID = @UserID, 
							@AssetID = @AssetID,
							@AssetIssueID = @AssetIssueID,
							@AttributeName = @Name,
							@AttributeValue = @Value,
							@AuditTitle = @AuditTitle,
							@AuditContent = @AuditContent, 
							@Timestamp = @ChangeDate, 
							@NewCategoryNotificationScheduleID = @ScheduleID
					
					IF @AssetIssueID > 0
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitleSched, @AuditContentSched, NULL, 1, NULL
						END
					ELSE
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitleSched, @AuditContentSched, NULL, 1, NULL
						END
						
					END
				ELSE
					BEGIN
						EXEC Diagnostics.spModifyAttributeAct @UserID, @AssetID, @AssetIssueID, @Name, @Value, @AuditTitle, @AuditContent, NULL
					END
				FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Name, @Value, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
					, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange, @ActionName, @UserName
				END
			END TRY
			BEGIN CATCH
				 BEGIN
				 SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
				 FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Name, @Value, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo
					, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange, @ActionName, @UserName
				 END
			END CATCH
			END
		ELSE
			BEGIN
			SET @OK = 0
			END
	CLOSE cur;
	DEALLOCATE cur;

	IF @ErrorMsg <> ''
	  BEGIN
	  RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spModifyAttributeIssueStatusChange] TO [TEUser]
    AS [dbo];
GO
