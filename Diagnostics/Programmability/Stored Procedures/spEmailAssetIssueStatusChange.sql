﻿-----

CREATE Procedure [Diagnostics].[spEmailAssetIssueStatusChange]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] Readonly
As
Begin
	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @ScheduleID INT
	DECLARE @UserID INT
	DECLARE @AssetID INT
	DECLARE @AssetIssueID INT
	DECLARE @ActivityStatusFrom NVARCHAR(255)
	DECLARE @ActivityStatusTo NVARCHAR(255)
	DECLARE @ResolutionStatusFrom NVARCHAR(255)
	DECLARE @ResolutionStatusTo NVARCHAR(255)
	DECLARE @ChangeDate DATETIME
	DECLARE @ChangedBy INT
	DECLARE @AuditTitle NVARCHAR(255)
	DECLARE @AuditContent NVARCHAR(MAX)
	DECLARE @AuditTitleSched NVARCHAR(255)
	DECLARE @AuditContentSched NVARCHAR(MAX)
	DECLARE @DelayHours INT
	DECLARE @DelayDays INT = 0
	DECLARE @DelayBreakout NVARCHAR(255) = ''
	DECLARE @DelayGuard NVARCHAR(255)
	DECLARE @CancelOnActivityStatusChange INT = NULL
	DECLARE @CancelOnResolutionStatusChange INT = NULL
	DECLARE @ScheduledDelay DATETIME
	DECLARE @EmailToList NVARCHAR(MAX)
	DECLARE @EmailCcList NVARCHAR(MAX)
	DECLARE @EmailBccList NVARCHAR(MAX)
	DECLARE @EmailSubject NVARCHAR(MAX)
	DECLARE @EmailContent NVARCHAR(MAX)
	DECLARE @StatusChange NVARCHAR(255)
	DECLARE @Now DATETIME = GETDATE()
	DECLARE @Attachments NVARCHAR(MAX) = ''
	DECLARE @ActionName NVARCHAR(255)

	SELECT * INTO #results FROM Diagnostics.ufnGetIssueTransitionEmail(@statusChanges)

	IF OBJECT_ID('tempdb..#IssueUpsert_EmailOverride') IS NOT NULL  --this should exist if override
	  BEGIN
		--If the user wants to override the contents of an auto generated email triggered by this transition there is no 
		--good way to pass those override values down to the trigger that actually sends the email.  So this is the least
		--bad way to do it.  We create a temp table with the override values.  The trigger will then check for the existence
		--of this temp table and use those contents if they exist.  Finally, the table is deleted once the 
		--stored procedure is finished.  This is a messy technique.  If a better one is found this should be updated. 
		
		--The stored procedure that creates and populates the temp table is spUpsertAssetIssue

		--This may create a warning that the temp table doesn't exist, but the check above makes sure the code only
		--executes if the table does exist.  I don't know how to make that warning go away.  

		UPDATE r
		  SET r.EmailToList = tt.[TO],
			  r.EmailCcList = tt.[CC],
			  r.EmailBccLIst = tt.[BCC],
			  r.EmailSubject = tt.[Subject],
			  r.EmailContent = tt.[Content]
		  FROM #results r
		  INNER JOIN #IssueUpsert_EmailOverride tt ON r.AssetIssueGUID = tt.AssetIssueGUID AND r.CategoryNotificationID = tt.CategoryNotificationID
		  WHERE r.EmailOverride = 1															 

		-- CatNot join condition is necessary for when multiple email actions with override are defined under one status change.  
		-- We are currently limited to only one email override at issue save (first returned in query), 
		-- but without this condition, all these email actions would match and the override email contents would be duplicated for all of them.  
		-- The accepted handling is one (lucky) override email and one or more base definition emails, even though they are defined with override checked.
	  
		IF OBJECT_ID('tempdb..#IssueUpsert_EmailOverride_Attachments') IS NOT NULL   --this exists only if there are attachments defined in override
		  BEGIN																	-- override attachments are a complete set (base+override) defined in the temp table
			SELECT @Attachments = STRING_AGG(convert(nvarchar(40), a.FileGUID),';')
			  FROM #IssueUpsert_EmailOverride_Attachments a
		   	  INNER JOIN #results r ON r.AssetIssueGUID = a.AssetIssueGUID AND r.CategoryNotificationID = a.CategoryNotificationID
			  WHERE r.EmailOverride = 1
		  END
	  END
	ELSE        -- if not an override, use base attachments
	  BEGIN  -- in unit tests, these base attachments are duplicated x 2  (#results ?); SELECT DISTINCT in spUpsertCategoryNotificationSchedule takes care of it until we can track it down
		SELECT @Attachments = STRING_AGG(convert(nvarchar(40), a.FileGUID),';')
		  FROM Diagnostics.tCategoryNotificationAttachment a
		  INNER JOIN #results r ON r.CategoryNotificationID = a.CategoryNotificationID
	  END

	DECLARE cur CURSOR LOCAL FOR
		SELECT AssetID, AssetIssueID, EmailSubject, EmailToList, EmailCcList, EmailBccList, EmailContent, ChangeDate, ChangedBy, ISNULL(ActionName,'')
			, ActivityStatusFrom, ActivityStatusTo, ResolutionStatusFrom, ResolutionStatusTo, DelayHours, DelayGuard, CancelOnActivityStatusChange, CancelOnResolutionStatusChange
		FROM #results 
			   
	OPEN cur;
	FETCH NEXT FROM cur INTO @AssetID, @AssetIssueID, @EmailSubject, @EmailToList, @EmailCcList, @EmailBccList, @EmailContent, @ChangeDate, @ChangedBy, @ActionName
		, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
	
	DECLARE @OK bit
	SET @OK = 1
	
	WHILE @OK = 1
	IF @@FETCH_STATUS = 0
		BEGIN			
		BEGIN TRY
			IF (Coalesce(@EmailToList, @EmailCcList, @EmailBccList) IS NOT NULL)
				BEGIN
					SET @AuditTitle = 'Action Name: ' + @ActionName -- for Lambda use

					IF (ISNULL(@ResolutionStatusFrom,' ') <> ISNULL(@ResolutionStatusTo,' '))   --status change portion of discussion entry so Lambda doesn't have to build it 
					  BEGIN
						SET @AuditContent = '<p>Old Status: ' + ISNULL(@ResolutionStatusFrom,'<none>') + '</p><p>New Status: ' + ISNULL(@ResolutionStatusTo,'<none>') + '</p>'
					  END
					ELSE
					  BEGIN
						SET @AuditContent = '<p>Old Status: ' + ISNULL(@ActivityStatusFrom,'<none>') + '</p><p>New Status: ' + ISNULL(@ActivityStatusTo,'<none>') + '</p>'
					  END

					IF @DelayHours > 0
					  BEGIN
							SET @DelayDays = @DelayHours / 24
							SET @DelayHours = @DelayHours % 24
							IF @DelayDays > 0 
							  BEGIN
								SET @DelayBreakout = CAST(@DelayDays AS NVARCHAR(20)) + ' day'
								IF @DelayDays > 1
								  BEGIN
									SET @DelayBreakout = @DelayBreakout + 's'
								  END
								IF @DelayHours > 0
								  BEGIN
									SET @DelayBreakout = @DelayBreakout + ' and '
								  END
							  END
							IF @DelayHours > 0
							  BEGIN
								SET @DelayBreakout = @DelayBreakout + CAST(@DelayHours AS NVARCHAR(20)) + ' hour'
								IF @DelayHours > 1
								  BEGIN
									SET @DelayBreakout = @DelayBreakout + 's'
								  END
							  END
							SET @ScheduledDelay = DATEADD(hour,@DelayHours,@ChangeDate)
							SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
							SET @AuditTitleSched = 'Status Change - ''' + @ActionName + ''' action scheduled for ' + @DelayBreakout + ' delay'
							SET @AuditContentSched = @AuditContent + '<p>' + @AuditTitle + '</p><p>Delay Action for: ' + @DelayBreakout + '</p>'													   

							EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
								@ScheduledDate = @ScheduledDelay, 
								@DelayGuard = @DelayGuard,
								@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
								@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
								@CategoryNotificationTypeID = 1,
								@UserID = @ChangedBy, 
								@AssetID = @AssetID,
								@AssetIssueID = @AssetIssueID,
								@AuditTitle = @AuditTitle,        -- this will be replaced in Lambda function with actual email subject, body and attachments
								@AuditContent = @AuditContent,    -- status change portion of discussion entry so Lambda doesn't have to build it 
								@Timestamp = @ChangeDate, 
								@EmailSubject = @EmailSubject,
								@EmailToList = @EmailToList,
								@EmailCcList = @EmailCcList,
								@EmailBccList = @EmailBccList,
								@EmailBody = @EmailContent,
								@EmailCategory = 'Issue',
								@FormatBodyAsHTML = 1,
								@CreatedBy = @ChangedBy,
								@Attachments = @Attachments,
								@NewCategoryNotificationScheduleID = @ScheduleID
				
							EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @ChangedBy, @AuditTitleSched, @AuditContentSched, NULL, 1, NULL
						END
					ELSE
						BEGIN
							-- to enable email attachments, put in schedule table with NOW scheduled date
							-- some of these params will not apply as there is no delay functionality needed
							-- Lambda function will process more frequently and handle sending the email, attachments and discussion entry
																
							EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
								@ScheduledDate = @Now, 
								@DelayGuard = @DelayGuard,
								@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
								@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
								@CategoryNotificationTypeID = 1,
								@UserID = @ChangedBy, 
								@AssetID = @AssetID,
								@AssetIssueID = @AssetIssueID,
								@AuditTitle = @AuditTitle,        -- this will be replaced in Lambda function with actual email subject, body and attachments
								@AuditContent = @AuditContent,    --	populating with ActionName and Status change so Lambda doesn't have to build them for discussion entry
								@Timestamp = @ChangeDate, 
								@EmailSubject = @EmailSubject,
								@EmailToList = @EmailToList,
								@EmailCcList = @EmailCcList,
								@EmailBccList = @EmailBccList,
								@EmailBody = @EmailContent,
								@EmailCategory = 'Issue',
								@FormatBodyAsHTML = 1,
								@CreatedBy = @ChangedBy,
								@Attachments = @Attachments,
								@NewCategoryNotificationScheduleID = @ScheduleID
								
						END
					END				
				FETCH NEXT FROM cur INTO @AssetID, @AssetIssueID, @EmailSubject, @EmailToList, @EmailCcList, @EmailBccList, @EmailContent, @ChangeDate, @ChangedBy, @ActionName
					, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
		END TRY
		BEGIN CATCH
				BEGIN
					SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
				 
					FETCH NEXT FROM cur INTO @AssetID, @AssetIssueID, @EmailSubject, @EmailToList, @EmailCcList, @EmailBccList, @EmailContent, @ChangeDate, @ChangedBy, @ActionName
					, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
				END
		END CATCH
		END
	ELSE
		BEGIN
			SET @OK = 0
		END
	CLOSE cur;
	DEALLOCATE cur;
	DROP TABLE #results

	IF @ErrorMsg <> ''
	  BEGIN
	  RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spEmailAssetIssueStatusChange] TO [TEUser]
    AS [dbo];
GO
