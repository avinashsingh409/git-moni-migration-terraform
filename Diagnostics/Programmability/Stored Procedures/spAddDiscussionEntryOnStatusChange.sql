﻿-----

CREATE PROCEDURE [Diagnostics].[spAddDiscussionEntryOnStatusChange]
	
@statusChanges [Diagnostics].[tpAssetIssueStatusChange] READONLY
AS
BEGIN
	DECLARE @ErrorMsg as varchar(max) = ''

	DECLARE @results AS TABLE
	(
	  UserID int,
	  AssetID int,
	  AssetIssueID INT,
	  EntryTitle nvarchar(255), 
      EntryContent nvarchar(MAX),
	  ChangeDate datetime,
	  ActivityStatusFrom nvarchar(255), -- if from a map status change, the from and to will be in the ActivityStatusFrom/To fields
	  ActivityStatusTo nvarchar(255),
	  ResolutionStatusFrom nvarchar(255),
	  ResolutionStatusTo nvarchar(255),
	  SekoiaMapChange bit,
	  SekoiaMapName nvarchar(255),
	  DelayHours INT NULL,
	  DelayGuard NVARCHAR(255) NULL,
	  CancelOnActivityStatusChange INT NULL,
	  CancelOnResolutionStatusChange INT NULL
	 )

	INSERT INTO @results
	SELECT 
		s.ChangedBy,
		s.AssetID,
		s.AssetIssueID,
		n.EntryTitle,
		n.EntryContent,
		s.ChangeDate,
		s.ActivityStatusFrom,
		s.ActivityStatusTo,
		s.ResolutionStatusFrom,
		s.ResolutionStatusTo,
		ISNULL(n.SekoiaMapChange,0),
		n.SekoiaMapName,
		n.DelayHours,
	    n.DelayGuard,
		n.CancelOnActivityStatusChange,
		n.CancelOnResolutionStatusChange
	FROM @statusChanges s	
	INNER JOIN Diagnostics.tCategoryNotification n ON s.NotificationID = n.CategoryNotificationID
	WHERE s.NotificationTypeID = 5 AND n.AddDiscussionEntry = 1

	DECLARE @UserID as int
	DECLARE @AssetID as int
	DECLARE @AssetIssueID INT
	DECLARE @ScheduleID INT
	DECLARE @Title as nvarchar(255)
	DECLARE @Content as nvarchar(max)
	DECLARE @ChangeDate as datetime
	DECLARE @ActivityStatusFrom nvarchar(255)
	DECLARE @ActivityStatusTo nvarchar(255)
	DECLARE @ResolutionStatusFrom nvarchar(255)
	DECLARE @ResolutionStatusTo nvarchar(255)
	DECLARE @EntryID as uniqueidentifier = NULL
	DECLARE @auditContent nvarchar(max)
	DECLARE @SekoiaMapChange bit
	DECLARE @SekoiaMapName nvarchar(255)
	DECLARE @AuditTitleNow NVARCHAR(255)
	DECLARE @AuditContentNow NVARCHAR(MAX)
	DECLARE @AuditTitleFuture NVARCHAR(255)
	DECLARE @AuditContentFuture NVARCHAR(MAX)
	DECLARE @DelayHours INT
	DECLARE @DelayGuard NVARCHAR(255)
	DECLARE @CancelOnActivityStatusChange INT = NULL
	DECLARE @CancelOnResolutionStatusChange INT = NULL
	DECLARE @StatusChange NVARCHAR(255)
	DECLARE @ScheduledDelay datetime
	DECLARE @label1 as varchar(255)
	DECLARE @label2 as varchar(255)
	
	DECLARE cur INSENSITIVE CURSOR FOR 
		SELECT UserID, AssetID, AssetIssueID, EntryTitle, EntryContent, ChangeDate, ActivityStatusFrom, ActivityStatusTo, ResolutionStatusFrom, ResolutionStatusTo, SekoiaMapChange, SekoiaMapName
			, DelayHours, DelayGuard, CancelOnActivityStatusChange, CancelOnResolutionStatusChange FROM @results 
		
	OPEN cur;
	FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Title, @Content, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @SekoiaMapChange
		, @SekoiaMapName, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
	
	WHILE @@FETCH_STATUS = 0
	BEGIN			
		BEGIN TRY
			--not available in Workflow Editor - NOT converted to standard issue discussion entry format
			IF @SekoiaMapChange = 0
				BEGIN
					SET @label1 = 'Activity Status'
					SET @label2 = 'Resolution Status: ' + ISNULL(@ResolutionStatusFrom,'<none>') + ' to ' + ISNULL(@ResolutionStatusTo,'<none>')
					SET @AuditTitleNow = 'Status Change - Add Blog Entry'
				END
			ELSE
				BEGIN
					SET @label1 = 'Map Status for map: ' + @SekoiaMapName
					SET @label2 = ''
					SET @AuditTitleNow = 'Map Status Change - Add Blog Entry'
				END			

			SET @AuditContentNow = @label1 + ': ' + ISNULL(@ActivityStatusFrom,'<none>') + ' to ' + ISNULL(@ActivityStatusTo,'<none>') + '; ' + @label2
			
			IF @DelayHours IS NOT NULL
				BEGIN
					SET @ScheduledDelay = DATEADD(hour,@DelayHours,@ChangeDate)
					SET @StatusChange = CONVERT(NVARCHAR, @ChangeDate, 0)
					SET @AuditTitleNow = @AuditTitleNow + ' Delayed for ' + CAST(@DelayHours AS NVARCHAR(20)) + ' hours'
					SET @AuditTitleFuture = 'Status Change from ' + @StatusChange +  ' - Delayed Add Blog Entry Occurred'
					SET @AuditContentFuture = 'Blog Entry Added with Title "' + @Title + '"'

					EXEC Diagnostics.spUpsertCategoryNotificationSchedule 
					@ScheduledDate = @ScheduledDelay,
					@DelayGuard = @DelayGuard,
					@CancelOnActivityStatusChange = @CancelOnActivityStatusChange, 
					@CancelOnResolutionStatusChange = @CancelOnResolutionStatusChange,
					@CategoryNotificationTypeID = 5,
					@UserID = @UserID,
					@AssetID = @AssetID,
					@AssetIssueID = @AssetIssueID,
					@AuditTitle = @AuditTitleFuture,
					@AuditContent = @AuditContentFuture,
					@AutoTitle = @Title,
					@AutoContent = @Content,
					@Timestamp = @ChangeDate,
					@NewCategoryNotificationScheduleID = @ScheduleID
				
					IF @SekoiaMapChange = 0 AND @AssetIssueID > 0
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitleNow, @AuditContentNow, NULL, 1, NULL
						END
					ELSE
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitleNow, @AuditContentNow, NULL, 1, NULL
						END
				END
			ELSE
				BEGIN
					EXEC Diagnostics.spAddDiscussionEntryAct @UserID, @AssetID, @AssetIssueID, @Title, @Content, @AuditTitleNow, @AuditContentNow, NULL, @SekoiaMapChange
				END
					    			
			FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Title, @Content, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @SekoiaMapChange
		       , @SekoiaMapName, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
		END TRY

		BEGIN CATCH
			SET @ErrorMsg = @ErrorMsg + '; ' + ERROR_MESSAGE()

			FETCH NEXT FROM cur INTO @UserID, @AssetID, @AssetIssueID, @Title, @Content, @ChangeDate, @ActivityStatusFrom, @ActivityStatusTo, @ResolutionStatusFrom, @ResolutionStatusTo, @SekoiaMapChange
		      , @SekoiaMapName, @DelayHours, @DelayGuard, @CancelOnActivityStatusChange, @CancelOnResolutionStatusChange
		END CATCH
	END
	CLOSE cur;
	DEALLOCATE cur;

	IF @ErrorMsg <> ''
	BEGIN
		RAISERROR( @ErrorMsg,16, 1);
	END
END

GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAddDiscussionEntryOnStatusChange] TO [TEUser]
    AS [dbo];

GO