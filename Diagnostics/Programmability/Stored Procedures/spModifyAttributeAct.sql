﻿CREATE PROCEDURE [Diagnostics].[spModifyAttributeAct]
	@UserID INT,
	@AssetID INT,
	@AssetIssueID INT,
	@AttributeName NVARCHAR(255),
	@AttributeValue NVARCHAR(MAX),
	@AuditTitle NVARCHAR(255),    --should contain 'Action Name: My Action'
	@AuditContent NVARCHAR(MAX),  -- should contain the old and new status in HTML
	@BlogDate DATETIMEOFFSET(7)
AS
BEGIN
	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @NewID INT
    DECLARE @NewTypeID INT 
	DECLARE @CategoryIDs Base.tpIntList   --not used currently
	DECLARE @AttrChangeDate DATETIME = GETDATE()
	DECLARE @AttributePrevValue NVARCHAR(MAX)
	DECLARE @AssetName NVARCHAR(255)
	DECLARE @UserName NVARCHAR(50)
	
	SET @BlogDate = ISNULL(@BlogDate, SYSDATETIMEOFFSET())

	BEGIN TRY
		BEGIN
			SELECT @UserName = Username FROM AccessControl.tUser WHERE SecurityUserID = @UserID

			SELECT @AssetName = AssetDesc FROM Asset.tAsset WHERE AssetID = @AssetID

			-- attribute may not exist until Upsert
			SELECT @AttributePrevValue = Asset.ufnGetAttributeValue(t.DisplayFormat, aa.Attribute_int, aa.Attribute_string, aa.Attribute_float, aa.Attribute_date, o.AttributeOptionTypeDesc)
			  FROM Asset.tAssetAttribute aa
			  INNER JOIN Asset.tAttributeType t ON t.AttributeTypeID = aa.AttributeTypeID AND t.AttributeTypeDesc = @AttributeName
			  LEFT JOIN Asset.tAttributeOptionType o ON o.AttributeOptionTypeID = aa.AttributeOptionTypeID AND o.AttributeTypeID = aa.AttributeTypeID
			  WHERE aa.AssetID = @AssetID
			  
			EXEC Asset.spUpsertAssetAttribute 
			  @SecurityUserID = @UserID
			, @AssetID = @AssetID
			, @AttributeTypeID = NULL
			, @Name = @AttributeName
			, @Value = @AttributeValue
			, @Favorite = 0
			, @DisplayOrder = 10
			-- I'm not passing an argument for @Date here so that spUpsertAssetAttribute will try parsing @Value for date attributes
			-- The @Date parameter is used by ETL.spImportCapPriorProjects to adjust dates entered in the user's local time zone
			, @IncludeCategories = 0
			, @CategoryIDs = @CategoryIDs
			, @NewDisplayFormat = NULL
			, @NewID = @NewID
			, @NewTypeID = @NewTypeID
			
			SET @AuditContent = '<p>Attribute was changed on ' + CONVERT(NVARCHAR, @AttrChangeDate, 0) + ' by an automated action triggered by: ' + @UserName
			      + '</p><p>' + @AuditTitle + '</p><p>Entity Attribute was changed for: ' + @AssetName + '</p><p>Attribute Name: ' + ISNULL(@AttributeName, '<none>') 
				  + '</p><p>Old Value: ' + ISNULL(@AttributePrevValue, '<none>') + '</p><p>Updated Value: ' + ISNULL(@AttributeValue, '<none>') + '</p>' + @AuditContent
			SET @AuditTitle = 'Attribute Changed'
			
			IF @AssetIssueID > 0
				BEGIN
					EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, @BlogDate, 0, NULL
				END
			ELSE
				BEGIN
					EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitle, @AuditContent, @BlogDate, 0, NULL
				END
		END
	END TRY
	BEGIN CATCH
		BEGIN
			SET @ErrorMsg = @ErrorMsg + ' Error executing workflow: ' + ERROR_MESSAGE()
		END
	END CATCH

	IF @ErrorMsg <> ''
	  BEGIN
		RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spModifyAttributeAct] TO [TEUser]
    AS [dbo];
GO

