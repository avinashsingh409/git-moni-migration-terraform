﻿-----

CREATE PROCEDURE [Diagnostics].[spUpdateCashflowAssetIssueStatusChange_Act]
	@UserID int,
	@AssetID int,
	@AssetIssueID int,
	@Type as nvarchar(255),
	@Value as float,
	@AuditTitle NVARCHAR(255),    -- should contain 'Action Name: My Action'
	@AuditContent NVARCHAR(MAX)   -- should contain the old and new status in HTML
AS
	DECLARE @Cashflow As [CapPrior].[tpProjectCashFlow]
	DECLARE @CashflowAuditEntry As [CapPrior].[tpCashFlowChange]
	DECLARE @EntryID as uniqueidentifier = NULL
	DECLARE @ErrorMsg as varchar(max) = ''
	DECLARE	@TimeStamp datetime = GETDATE()
	DECLARE @UnitOfMeasure as nvarchar(255)
	DECLARE @Status as int
	DECLARE @return int
	DECLARE @errorCount int = 0
	DECLARE @CashflowRecordModified as int = 101
	DECLARE @ChangeDate DATETIME = GETDATE()
	DECLARE @PrevValue NVARCHAR(MAX)
	DECLARE @AssetName NVARCHAR(255)
	DECLARE @UserName NVARCHAR(50)

	DECLARE @CashflowChange Table (
		ProjectCashFlowID int,
		AssetID int,
		[Type] nvarchar(255),
		UnitOfMeasure nvarchar(255),
		Value float,
		[Status] int,
		[Timestamp] datetime)

			BEGIN TRY
				BEGIN
				
				SELECT @UserName = Username FROM AccessControl.tUser WHERE SecurityUserID = @UserID
				SELECT @AssetName = AssetDesc FROM Asset.tAsset WHERE AssetID = @AssetID

				-- TODO?
				-- may need to revisit this query 
				-- WF action says to set a particular milestone actual timestamp to NOW
				-- this query returns the latest milestone planned status before NOW
				-- is that the right 'place' to insert or update?
				-- does it need to fallback to always insert a record even if no CF rows exist for type/map and asset?

				INSERT INTO @Cashflow (UnitOfMeasure, AssetID, [Timestamp], [Type], [Status], [Value])
					SELECT TOP(1) c.UnitOfMeasure, @AssetID, @TimeStamp, @Type, c.[Status], @Value 
					FROM CapPrior.tProjectCashFlow c
					WHERE c.AssetID = @AssetID AND c.[Type] = @Type AND c.[Timestamp] < GETDATE()  
					ORDER BY c.Timestamp DESC				
				
				SELECT @UnitOfMeasure = cf.UnitOfMeasure, @Status = cf.[Status] FROM @Cashflow cf
								
				IF EXISTS (SELECT * FROM @Cashflow)
				BEGIN
					--Update cashflow
					SET @return = 1

					EXEC @return = CapPrior.spSaveProjectCashFlows @UserID = @UserID, @Flows = @Cashflow
					
					IF @return <> 0
					BEGIN
						SET @ErrorMsg = @ErrorMsg + '; Failed to update cash flow'
						SET @errorCount = @errorCount + 1; 
					END

					INSERT INTO @CashflowChange EXEC CapPrior.spGetCashflowChange @UserID = @UserID, @AssetID = @AssetID, @CFType = @Type, @ProjectTimeStamp = @TimeStamp
					
					INSERT INTO @CashflowAuditEntry (AuditId, AuditTypeId, CreatedBy, ProjectCashFlowID, UnitOfMeasure,
						AssetId, [TimeStamp], [Type], PreviousValue, PreviousStatus, GlobalID)
						SELECT -1, @CashflowRecordModified, @UserID, cfc.ProjectCashFlowID, cfc.UnitOfMeasure,
						cfc.AssetID, cfc.[Timestamp], cfc.[Type], cfc.Value, cfc.[Status], NEWID() FROM @CashflowChange cfc

					SELECT @PrevValue = cfc.Value FROM @CashflowChange cfc
					
					SET @return = 1
					
					EXEC @return = CapPrior.spSaveCashflowChange @UserID = @UserID, @Audits = @CashflowAuditEntry
					
					IF @return <> 0
					BEGIN
						SET @ErrorMsg = @ErrorMsg + '; Failed to add cash flow audit entry'
						SET @errorCount = @errorCount + 1; 
					END
					
					SET @AuditContent = '<p>Map Status was changed on ' + CONVERT(NVARCHAR, @ChangeDate, 0) + ' by an automated action triggered by: ' + @UserName 
						+ '</p><p>' + @AuditTitle + '</p><p>Entity Map Status was changed for: ' + @AssetName + '</p><p>Map: ' + @Type
						+ '</p><p>Old Value: ' + + CONVERT(NVARCHAR, @PrevValue) + '</p><p>Updated Value: ' + CONVERT(NVARCHAR, @Value) + '</p>' + @AuditContent
			
					SET @AuditTitle = 'Map Status Updated'

					IF @AssetIssueID > 0
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, NULL, 0, NULL
						END
					ELSE
						BEGIN
							EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitle, @AuditContent, NULL, 0, NULL
						END
  				END
				ELSE
				BEGIN
					-- see above
					-- no CF records found with actual or planned milestones timestamps before NOW
					-- should this happen?
					SET @ErrorMsg = @ErrorMsg + '; Failed to return cash flow record'
					SET @errorCount = @errorCount + 1; 
				END
			END				
			END TRY
			BEGIN CATCH
			BEGIN
				SET @ErrorMsg = @ErrorMsg + '; Error executing workflow: ' + ERROR_MESSAGE()
				SET @errorCount = @errorCount + 1; 
			END
			END CATCH

RETURN @errorCount
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpdateCashflowAssetIssueStatusChange_Act] TO [TEUser]
    AS [dbo]
GO