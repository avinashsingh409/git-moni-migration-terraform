﻿----

CREATE PROCEDURE [Diagnostics].[spAddDeleteKeyTagOnStatusChange_Act]
	@UserID INT,
	@AssetID INT,
	@AssetIssueID INT,
	@KeyTagOnIssue BIT,
	@KeyTagAction NVARCHAR(40),
	@KeyTag NVARCHAR(255),
	@ChangeDate DATETIMEOFFSET(7),
	@EntryID UNIQUEIDENTIFIER = NULL,
	@AuditTitle NVARCHAR(255),
	@AuditContent NVARCHAR(MAX)
AS
BEGIN
	DECLARE @ErrorMsg AS VARCHAR(MAX) = ''
	DECLARE @Result INT
		
	SET @ChangeDate = ISNULL(@ChangeDate, SYSDATETIMEOFFSET())
	
	BEGIN TRY
		EXEC @Result = Asset.spAddOrDeleteKeyTagMap @AssetID, @AssetIssueID, @UserID, @KeyTagOnIssue, @KeyTagAction, @KeyTag, @ChangeDate

		IF @Result = 0
			BEGIN
				SET @ErrorMsg = @ErrorMsg + '; Failed to add/remove key tag'
			END
			
		IF @AssetIssueID > 0
			BEGIN
				EXEC Discussions.spAddDiscussionEntryOnAssetIssueBlog @AssetID, @AssetIssueID, @UserID, @AuditTitle, @AuditContent, @ChangeDate, 0, @EntryID OUTPUT
			END
		ELSE
			BEGIN
				EXEC Discussions.spAddDiscussionEntryOnAssetBlog @AssetID, @UserID, @AuditTitle, @AuditContent, @ChangeDate, 0, @EntryID OUTPUT
			END

		IF @EntryID IS NULL
		BEGIN
			SET @ErrorMsg = @ErrorMsg + '; Failed to add audit discussion entry'
		END
	END TRY

	BEGIN CATCH
		SET @ErrorMsg = @ErrorMsg + '; ' + ERROR_MESSAGE()
	END CATCH

	IF @ErrorMsg <> ''
	  BEGIN
		RAISERROR( @ErrorMsg,16, 1);
	  END
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAddDeleteKeyTagOnStatusChange_Act] TO [TEUser]
    AS [dbo];
GO

