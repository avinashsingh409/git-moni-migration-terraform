﻿CREATE PROCEDURE [Diagnostics].[spAssetIssuesCountByCategory]
	@securityUserID int,
	@assetID int = NULL,
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@changeStartDate datetime = NULL,
	@changeEndDate datetime = NULL,
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@openDurationHigh int = NULL,
	@openDurationLow int = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	@impactCostHigh float = NULL,
	@impactCostLow float = NULL,
	@assignedTo NVARCHAR(255) = NULL,
	@scorecard bit = NULL,
	@priority NVARCHAR(255) = NULL,
	@changedBy NVARCHAR(255) = NULL,
	@activityStatus Base.tpIntList READONLY,
	@resolutionStatus Base.tpIntList READONLY,
	@issueTypeID Base.tpIntList  READONLY,
	@issueClassTypeID Base.tpIntList READONLY,
	@issueCategoryType Base.tpIntList READONLY,
	@issueCauseType Base.tpIntList READONLY,
	@taggedAssetIssueID Base.tpIntList READONLY,
	@includeAssociatedAssets bit = 0
AS
BEGIN
	SET NOCOUNT ON;  -- to prevent extra result sets from interfering with SELECT statements
    SET ARITHABORT ON
    
    DECLARE @owningAssetsByCategory TABLE(CategoryTypeID int not null,AssetIssueID int,CategoryType nvarchar(255) not null);
	DECLARE @assetByCategory TABLE(CategoryTypeID int not null, AssetIssueID int,CategoryType nvarchar(255) not null);
    DECLARE @assets base.tpIntList;
    
    INSERT INTO @assets SELECT assetid from Asset.ufnGetAssetIdsForuserStartingAt(@securityUserID,@assetID) 
    --associated assets
	IF @includeAssociatedAssets = 1
	BEGIN
	      DECLARE @associatedAssets base.tpIntList;
		  DECLARE @associatedAssetsByCategory TABLE(CategoryTypeID int not null,AssetIssueID int,CategoryType nvarchar(255) not null);
		  INSERT INTO @associatedAssets SELECT DISTINCT a.AssetID FROM Diagnostics.vAssetIssue a
		  LEFT JOIN Diagnostics.tAssetIssueKeywordMap b ON b.AssetIssueID = a.AssetIssueID
		  INNER JOIN Diagnostics.tAssetIssueAssociatedAsset c ON c.AssetIssueID = a.AssetIssueID
		  WHERE c.AssetID IN (select id from @assets)

		  IF EXISTS(SELECT 1/0 FROM @associatedAssets)
		  BEGIN
		  INSERT INTO @associatedAssetsByCategory SELECT ai.AssetIssueCategoryTypeID as CategoryTypeID, ai.AssetIssueID as AssetIssueID, ct.CategoryAbbrev as CategoryType
		  FROM [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
							@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
							@openDurationHigh, @openDurationLow, @id,@title, @impactCostHigh,@impactCostLow,
							@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,
							@issueCategoryType, @issueCauseType,@taggedAssetIssueID,@associatedAssets, @scorecard,
							@priority,@changedBy) filter
	      INNER JOIN Diagnostics.tAssetIssue ai ON filter.AssetIssueID = ai.AssetIssueID
		  INNER JOIN Diagnostics.tAssetIssueAssociatedAsset assc on assc.AssetIssueID = filter.AssetIssueID
	      INNER JOIN Diagnostics.tAssetIssueCategoryType ct ON ai.AssetIssueCategoryTypeID = ct.AssetIssueCategoryTypeID
		  WHERE assc.AssetID IN (select id from @assets)
		  END
	END
	INSERT INTO @owningAssetsByCategory
	SELECT ai.AssetIssueCategoryTypeID as CategoryTypeID ,ai.AssetIssueID, ct.CategoryAbbrev as CategoryType
	FROM [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
							@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
							@openDurationHigh, @openDurationLow, @id, @title, @impactCostHigh,@impactCostLow,
							@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,
							@issueCategoryType, @issueCauseType,@taggedAssetIssueID,@assets, @scorecard,
							@priority,@changedBy) filter
	INNER JOIN Diagnostics.tAssetIssue ai ON filter.AssetIssueID = ai.AssetIssueID
	INNER JOIN Diagnostics.tAssetIssueCategoryType ct ON ai.AssetIssueCategoryTypeID = ct.AssetIssueCategoryTypeID
	
	INSERT INTO @assetByCategory SELECT * FROM @owningAssetsByCategory
	UNION
	SELECT * FROM @associatedAssetsByCategory WHERE AssetIssueID NOT IN (SELECT AssetIssueID FROM @owningAssetsByCategory)

	SELECT CategoryTypeID,COUNT(AssetIssueID) as IssuesCount,CategoryType FROM @assetByCategory 
	GROUP BY CategoryTypeID, CategoryType

END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAssetIssuesCountByCategory] TO [TEUser]
    AS [dbo];

