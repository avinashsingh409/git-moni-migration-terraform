﻿CREATE Procedure [Diagnostics].[spEmailMapStatusChange]
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] Readonly
As
Begin
	Declare @count int
	Declare @countEmail int

	Select s.AssetIssueID,
		s.AssetID,
		a.AssetDesc,
		n.SekoiaMapName,
		s.ChangeDate,
		s.ChangedBy,
		u.FirstName + ' ' + u.LastName As FullName,
		s.ActivityStatusFrom,
		s.ActivityStatusTo,
		n.EmailRecipients as EmailToList,
		'' as EmailCcList,
		'' as EmailBccList,
		n.EmailSubject,
		n.EmailContent
	Into #results
	From @statusChanges s
	Left Join Asset.tAsset a On s.AssetID = a.AssetID
	Left Join AccessControl.tUser u On s.ChangedBy = u.SecurityUserID
	Left Join Diagnostics.tCategoryNotification n On s.NotificationID = n.CategoryNotificationID
	Where s.NotificationTypeID = 1 And n.EmailSubscribers = 1 and n.SekoiaMapChange = 1
	Set @count = @@ROWCOUNT
	
	If @count > 0
	Begin
		--Replace tokens in email subject and body
		--The REPLACE function returns NULL if any one of the arguments is NULL, so ensure the string_replacement parameter is not NULL

		Update #results Set EmailSubject = Replace(EmailSubject, '{MapName}', isnull(Convert(nvarchar(255), SekoiaMapName), ''))
		Update #results Set EmailContent = Replace(EmailContent, '{MapName}', isnull(Convert(nvarchar, SekoiaMapName), ''))

		Update #results Set EmailSubject = Replace(EmailSubject, '{AssetDesc}', isnull(AssetDesc, '')) 
		Update #results Set EmailContent = Replace(EmailContent, '{AssetDesc}', isnull(AssetDesc, ''))
		
		--URL only allowed in body
		Update #results Set EmailContent = Replace(EmailContent, '{AssetURL}', isnull(Diagnostics.ufnGenerateURLtoAsset(AssetID), ''))

		Update #results Set EmailSubject = Replace(EmailSubject, '{MapStatusFrom}', isnull(ActivityStatusFrom, ''))
		Update #results Set EmailContent = Replace(EmailContent, '{MapStatusFrom}', isnull(ActivityStatusFrom, ''))

		Update #results Set EmailSubject = Replace(EmailSubject, '{MapStatusTo}', isnull(ActivityStatusTo, ''))
		Update #results Set EmailContent = Replace(EmailContent, '{MapStatusTo}', isnull(ActivityStatusTo, ''))
		
		Update #results Set EmailSubject = Replace(EmailSubject, '{ChangedBy}', isnull(FullName, ''))
		Update #results Set EmailContent = Replace(EmailContent, '{ChangedBy}', isnull(FullName, ''))

		--Dates in the database are currently stored in server time (Central Time)
		Update #results Set EmailSubject = Replace(EmailSubject, '{ChangeDate}', isnull(Convert(VARCHAR(20), ChangeDate,100), '') + ' Central Time')
		Update #results Set EmailContent = Replace(EmailContent, '{ChangeDate}', isnull(Convert(VARCHAR(20), ChangeDate,100), '') + ' Central Time')

		--Insert message into email queue
		Begin Try
			Begin
				--null not allowed for following:
				-- EmailSubject
				-- EmailBody
				-- CreatedBy
				-- CreateDate
				-- EmailCategory
				-- FormatBodyAsHTML
				Insert Into [Notification].tEmailQueue (EmailSubject, EmailToList, EmailCcList, EmailBccList, EmailToDisplayNameList, EmailCcDisplayNameList, EmailBccDisplayNameList, EmailBody, CreatedBy, CreateDate, EmailCategory, FormatBodyAsHTML)
				Select r.EmailSubject, r.EmailToList, r.EmailCcList, r.EmailBccList, null, null, null, r.EmailContent, r.ChangedBy, r.ChangeDate, 'Issue', 1
				From #results r Where Coalesce(r.EmailToList, r.EmailCcList, r.EmailBccList) is not null
				Set @countEmail = @@RowCount
			
				If @count <> @countEmail
				Begin
					Print N'Failed to generate map status change messages: ' + Convert(nvarchar, @count - @countEmail);
					--What action should be taken if unable to generate messages?
				End
			End
		End Try
		Begin Catch
			Print N'Failed to insert map status change messages into email queue: ' + Convert(nvarchar, @count);
			Print Error_Message();
			--What action should be taken if unable to insert messages into email queue?
		End Catch
	End

	Drop Table #results
End
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spEmailMapStatusChange] TO [TEUser]
    AS [dbo];


GO