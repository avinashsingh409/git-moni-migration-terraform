﻿CREATE TYPE [Diagnostics].[tpAssetIssueStatusChange] AS TABLE(
	[AssetIssueID] [int] NOT NULL,
	[AssetIssueGUID] [uniqueidentifier] NULL,
	[AssetID] [int] NOT NULL,
	[IssueTitle] [nvarchar](255) NULL,
	[ChangeDate] [datetime] NOT NULL,
	[ChangedBy] [int] NOT NULL,
	[ActivityStatusFrom] [nvarchar](255) NULL,
	[ActivityStatusTo] [nvarchar](255) NULL,
	[ResolutionStatusFrom] [nvarchar](255) NULL,
	[ResolutionStatusTo] [nvarchar](255) NULL,
	[NotificationID] [int] NULL,    --all status changes need to be checked for delayed actions that need to be cancelled (removed) on status change
	[NotificationTypeID] [int] NULL  --only spRemoveCancelOnStatusChangeScheduledActions will need to process if these columns are null
)
GO

GRANT EXECUTE
    On Type::[Diagnostics].[tpAssetIssueStatusChange] To [TEUser]
GO