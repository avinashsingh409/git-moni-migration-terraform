﻿------------------

CREATE Function  [Diagnostics].[ufnGetAssetIssueStatusChange]
(
	@assetIssueCategoryTypeId int,
	@activityStatusFromId int = NULL,
	@activityStatusToId int,
	@resolutionStatusFromId int = NULL,
	@resolutionStatusToId int
)
RETURNS
@assetIssueActionTable Table
(
     [NotificationID] int NOT NULL
	,[NotificationTypeID] int NOT NULL
)

AS 
	BEGIN
	
	INSERT INTO @assetIssueActionTable (NotificationID, NotificationTypeID)
	SELECT n.CategoryNotificationID, n.CategoryNotificationTypeID 
	FROM Diagnostics.tCategoryNotification n
	WHERE n.AssetIssueCategoryTypeID = @assetIssueCategoryTypeId 
	AND
		(
			(	
				(((n.SourceAssetIssueActivityStatus = @activityStatusFromId) AND (n.TargetAssetIssueActivityStatus = @activityStatusToId))
					OR ((n.SourceAssetIssueActivityStatus = @activityStatusFromId) AND (n.TargetAssetIssueActivityStatus IS NULL))
					OR ((n.SourceAssetIssueActivityStatus IS NULL) AND (n.TargetAssetIssueActivityStatus = @activityStatusToId)))
				AND (ISNULL(@activityStatusFromId,-1) <> ISNULL(@activityStatusToId,-1))
			)
			OR
			(
				(((n.SourceAssetIssueResolutionStatus = @ResolutionStatusFromId) AND (n.TargetAssetIssueResolutionStatus = @ResolutionStatusToId))
					OR ((n.SourceAssetIssueResolutionStatus = @ResolutionStatusFromId) AND (n.TargetAssetIssueResolutionStatus IS NULL))
					OR ((n.SourceAssetIssueResolutionStatus IS NULL) AND (n.TargetAssetIssueResolutionStatus = @ResolutionStatusToId)))
				AND (ISNULL(@ResolutionStatusFromId,-1) <> ISNULL(@ResolutionStatusToId,-1))
			)
		)
	
	RETURN
	END
GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnGetAssetIssueStatusChange] TO [TEUser]
    AS [dbo];


GO