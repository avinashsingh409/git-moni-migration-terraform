﻿


Create Function [Diagnostics].[ufnGenerateURLtoIssue]
(
	@AssetIssueID int
)
Returns varchar(2000)
As
Begin
	Declare @link varchar(2000) --Max safe length for URL

	--Asset360 Post Office will replace {WebsiteRoot} token when email is sent
	Set @link = (Select '{WebsiteRoot}/IssuesManagement/Issue.html#!/snapshot?iid=' + Convert(varchar(36), GlobalID)
		From Diagnostics.tAssetIssue Where AssetIssueID = @AssetIssueID)
	
	Return isnull(@link, '')
End
Go

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[ufnGenerateURLtoIssue] TO [TEUser]
    AS [dbo];


GO