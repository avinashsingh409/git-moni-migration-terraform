﻿
CREATE FUNCTION [Diagnostics].[ufnAssetIssueImpacts]
(
)
RETURNS @impactsTable TABLE
(
  AssetIssueID int PRIMARY KEY not NULL,
  Fields varchar(255) not NULL,
  Impacts varchar(255) NULL
)
AS
BEGIN
insert into @impactsTable select * from [Diagnostics].[ufnAssetIssueImpactsWithIssueID](null)
RETURN 
END

GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnAssetIssueImpacts] TO [TEUser]
    AS [dbo];
GO