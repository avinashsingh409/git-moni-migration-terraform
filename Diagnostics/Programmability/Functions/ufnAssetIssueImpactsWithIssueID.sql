﻿CREATE FUNCTION [Diagnostics].[ufnAssetIssueImpactsWithIssueID]
(
  @AssetIssueID int
)
RETURNS @impactsTable TABLE
(
  AssetIssueID int PRIMARY KEY not NULL,
  Fields varchar(255) not NULL,
  Impacts varchar(255) NULL
)
AS
BEGIN

INSERT INTO @impactsTable (AssetIssueID,Fields,Impacts)
SELECT
	C.AssetIssueID, 'AssetIssueImpactTypeId,Impact,Impact_Cost' Fields, Left(C.Impacts,Len(C.Impacts)-1) Impacts
FROM
	(
	SELECT DISTINCT AssetIssueID, 
		(
			SELECT
				CAST(A.AssetIssueImpactTypeID AS VARCHAR) + ';' +
				ISNULL(CAST(A.Impact AS VARCHAR), '') + ';' + 
				ISNULL(CAST(A.Impact_Cost * (scen.PercentLikelihood / 100) AS VARCHAR), '') + ','
			FROM Diagnostics.tAssetIssueAssetIssueImpactTypeMap A
			JOIN Diagnostics.tAssetIssueImpactScenario scen
			ON
				(A.AssetIssueID = scen.OwningAssetIssueID) AND
				(
					(A.AssetIssueAssetIssueImpactTypeMapID = scen.Generation_AssetIssueAssetIssueImpactTypeMapID)
					OR (A.AssetIssueAssetIssueImpactTypeMapID = scen.Efficiency_AssetIssueAssetIssueImpactTypeMapID)
					OR (A.AssetIssueAssetIssueImpactTypeMapID = scen.Maintenance_AssetIssueAssetIssueImpactTypeMapID)
					OR (A.AssetIssueAssetIssueImpactTypeMapID = scen.OtherCosts_AssetIssueAssetIssueImpactTypeMapID)
				)
			WHERE A.AssetIssueID = B.AssetIssueID
			FOR XML PATH ('')
		) Impacts
	FROM Diagnostics.tAssetIssueAssetIssueImpactTypeMap B
	) C WHERE @AssetIssueID is null or (C.AssetIssueID = @AssetIssueID)
ORDER BY C.AssetIssueID OPTION (RECOMPILE)

RETURN

END

GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnAssetIssueImpactsWithIssueID] TO [TEUser]
    AS [dbo];
GO
