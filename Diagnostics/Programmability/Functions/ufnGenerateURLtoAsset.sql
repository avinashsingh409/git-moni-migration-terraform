﻿


Create Function [Diagnostics].[ufnGenerateURLtoAsset]
(
	@AssetID int
)
Returns varchar(2000)
As
Begin
	Declare @link varchar(2000) --Max safe length for URL

	--Asset360 Post Office will replace {WebsiteRoot} token when email is sent
	Set @link = (Select '{WebsiteRoot}/AssetExplorer/Index.html#!/info?asset=' + Convert(varchar(36), GlobalID)
		From Asset.tAsset Where AssetID = @AssetID)

	Return isnull(@link, '')
End
Go

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[ufnGenerateURLtoAsset] TO [TEUser]
    AS [dbo];


GO