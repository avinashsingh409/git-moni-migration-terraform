﻿
CREATE FUNCTION [Diagnostics].[ufnGetFilteredAssetIssueIDs]
(
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@changeStartDate datetime = NULL,
	@changeEndDate datetime = NULL,
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@openDurationHigh int = NULL,
	@openDurationLow int = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	@impactCostHigh float = NULL,
	@impactCostLow float = NULL,
	@assignedTo NVARCHAR(255) = NULL,
	@activityStatus Base.tpIntList READONLY,
	@resolutionStatus Base.tpIntList READONLY,
	@issueTypeID Base.tpIntList  READONLY,
	@issueClassTypeID Base.tpIntList READONLY,
	@issueCategoryType Base.tpIntList READONLY,
	@issueCauseType Base.tpIntList READONLY,
	@taggedAssetIssueID Base.tpIntList READONLY,
	@scorecard bit = NULL,
	@priority NVARCHAR(255) = NULL,
	@changedBy NVARCHAR(255) = NULL
)
RETURNS @issuesTable TABLE
(
  AssetIssueID int PRIMARY KEY not NULL
)
AS
BEGIN

DECLARE @assetIDs Base.tpIntList
INSERT INTO @assetIDs select distinct assetid from Diagnostics.vAssetIssue

INSERT INTO @issuesTable
SELECT AssetIssueID FROM Diagnostics.ufnGetFilteredAssetIssueIDsFromAssetIDs(@startDate,@endDate,@changeStartDate, @changeEndDate,@closeStartDate,@closeEndDate,@openDurationHigh,@openDurationLow,
@id,@title,@impactCostHigh,@impactCostLow,@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,@issueCategoryType,  @issueCauseType,@taggedAssetIssueID,@assetIDs,@scorecard,@priority,@changedBy)

RETURN
END
-- end of ufnGetFilteredAssetIssueIDs
GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnGetFilteredAssetIssueIDs] TO [TEUser]
    AS [dbo];
GO