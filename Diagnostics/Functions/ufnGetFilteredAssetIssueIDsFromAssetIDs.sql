﻿--Update the function to support aggregations in the dashboard.
CREATE FUNCTION [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]
(	
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@changeStartDate datetime = NULL,
	@changeEndDate datetime = NULL,
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@openDurationHigh int = NULL,
	@openDurationLow int = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	@impactCostHigh float = NULL,
	@impactCostLow float = NULL,
	@assignedTo NVARCHAR(255) = NULL,
	@activityStatus Base.tpIntList READONLY,
	@resolutionStatus Base.tpIntList READONLY,
	@issueTypeID Base.tpIntList  READONLY,
	@issueClassTypeID Base.tpIntList READONLY,
	@issueCategoryType Base.tpIntList READONLY,
	@issueCauseType Base.tpIntList READONLY,
	@taggedAssetIssueID Base.tpIntList READONLY,
	@assetIDs Base.tpIntList READONLY,
	@scorecard bit = NULL,
	@priority NVARCHAR(255) = NULL,
	@changedBy NVARCHAR(255) = NULL
)
RETURNS @returnTable TABLE (AssetIssueID int)
AS
BEGIN
	
	DECLARE @tt TABLE (
	id int,
	IssueTitle VARCHAR(255) NULL,
	CreateDate DateTime, 
	ChangeDate DateTime, 
	CloseDate DateTime NULL, 
	OpenDuration int NULL, 
	ImpactTotal float NULL,
	[Priority] NVARCHAR(50) NULL, 
	Scorecard BIT NOT NULL,
	ActivityStatusID INT NOT NULL,
	ResolutionStatusID INT NOT NULL,
	IssueTypeID INT NULL,
	IssueClassTypeID INT NOT NULL,
	AssetIssueCategoryTypeID INT NOT NULL,
	IssueCauseTypeID INT NULL,
	AssetIssueID INT NULL,
	ChangedBy VARCHAR(255) NULL
	);

	
	declare @assetcnt as int
	select @assetcnt = COUNT(*) from @assetIDs

	if @assetcnt>0
	    BEGIN
		INSERT INTO @tt(id,IssueTitle, CreateDate,ChangeDate, CloseDate, OpenDuration, ImpactTotal,[Priority], 
		Scorecard,ActivityStatusID,ResolutionStatusID,IssueTypeID,IssueClassTypeID,AssetIssueCategoryTypeID,IssueCauseTypeID, AssetIssueID, ChangedBy )
		SELECT vAssetIssue.AssetIssueID,vAssetIssue.IssueTitle, vAssetIssue.CreateDate, vAssetIssue.ChangeDate, vAssetIssue.CloseDate, vAssetIssue.OpenDuration,
		vAssetIssue.ImpactTotal,vAssetIssue.[Priority], vAssetIssue.Scorecard,vAssetIssue.ActivityStatusID,vAssetIssue.ResolutionStatusID,
		vAssetIssue.IssueTypeID,vAssetIssue.IssueClassTypeID,vAssetIssue.AssetIssueCategoryTypeID,vAssetIssue.IssueCauseTypeID, vAssetIssue.AssetIssueID, vAssetIssue.ChangedBy
		FROM Diagnostics.vAssetIssue 
		JOIN @assetIDs b ON Diagnostics.vAssetIssue.AssetID = b.id
		LEFT JOIN Diagnostics.tAssetIssueKeywordMap c ON Diagnostics.vAssetIssue.AssetIssueID= c.AssetIssueID OPTION (recompile)
	    END
	ELSE
	  BEGIN
	  INSERT INTO @tt(id,IssueTitle, CreateDate,ChangeDate, CloseDate, OpenDuration, ImpactTotal,[Priority], 
		Scorecard,ActivityStatusID,ResolutionStatusID,IssueTypeID,IssueClassTypeID,AssetIssueCategoryTypeID,IssueCauseTypeID, AssetIssueID, ChangedBy )
		SELECT vAssetIssue.AssetIssueID,vAssetIssue.IssueTitle, vAssetIssue.CreateDate, vAssetIssue.ChangeDate, vAssetIssue.CloseDate, vAssetIssue.OpenDuration,
		vAssetIssue.ImpactTotal,vAssetIssue.[Priority], vAssetIssue.Scorecard,vAssetIssue.ActivityStatusID,vAssetIssue.ResolutionStatusID,
		vAssetIssue.IssueTypeID,vAssetIssue.IssueClassTypeID,vAssetIssue.AssetIssueCategoryTypeID,vAssetIssue.IssueCauseTypeID, vAssetIssue.AssetIssueID, vAssetIssue.ChangedBy
		FROM Diagnostics.vAssetIssue 		
		LEFT JOIN Diagnostics.tAssetIssueKeywordMap c ON Diagnostics.vAssetIssue.AssetIssueID= c.AssetIssueID
	  END

	IF @id IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE (CONVERT(varchar(10), id) NOT LIKE '%' + @id + '%' )
	END

	IF @title IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE IssueTitle IS NOT NULL AND (IssueTitle NOT LIKE '%' + @title + '%')
	END

	IF @startDate IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE CreateDate < @startDate
	END

	IF @endDate IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE CreateDate > @endDate
	END
	
	IF @changeStartDate IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE ChangeDate < @changeStartDate
	END

	IF @changeEndDate IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE ChangeDate > @changeEndDate
	END

	IF @closeStartDate IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE CloseDate IS NOT NULL AND CloseDate < @closeStartDate
	END 

	IF @closeEndDate IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE CloseDate IS NOT NULL AND CloseDate < @closeEndDate
	END 

	IF @openDurationLow IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE OpenDuration IS NOT NULL AND OpenDuration < @openDurationLow
	END

	IF @openDurationHigh IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE OpenDuration IS NOT NULL AND OpenDuration > @openDurationHigh
	END

	IF @impactCostLow IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE ImpactTotal IS NULL OR ImpactTotal < @impactCostLow
	END 

	IF @impactCostHigh IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE ImpactTotal IS NOT NULL AND ImpactTotal > @impactCostHigh
	END 

	IF @assignedTo IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE id NOT IN (
			SELECT AssetIssueID
			FROM Diagnostics.tAssetIssueActionPlan
			WHERE ResponsibleParty LIKE '%' + @assignedTo + '%' AND [Status] <> 'Completed'
		)
	END

	IF @changedBy IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE ChangedBy IS NOT NULL AND (ChangedBy NOT LIKE '%' + @changedBy + '%')
	END

	IF @priority IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE [Priority] IS NOT NULL AND [Priority] NOT LIKE '%' + @priority + '%'
	END

	IF @scorecard IS NOT NULL
	BEGIN
		DELETE FROM @tt WHERE Scorecard <> @scorecard
	END 

	IF EXISTS (SELECT 1 FROM @activityStatus)
	BEGIN
		DELETE FROM @tt WHERE ActivityStatusID NOT IN (SELECT id FROM @activityStatus)
	END

	IF EXISTS (SELECT 1 FROM @resolutionStatus)
	BEGIN
		DELETE FROM @tt WHERE ResolutionStatusID NOT IN (SELECT id FROM @resolutionStatus)
	END

	IF EXISTS (SELECT 1 FROM @issueTypeID)
	BEGIN
		DELETE FROM @tt WHERE IssueTypeID NOT IN (SELECT id FROM @issueTypeID)
	END

	IF EXISTS (SELECT 1 FROM @issueClassTypeID)
	BEGIN
		DELETE FROM @tt WHERE IssueClassTypeID NOT IN (SELECT id FROM @issueClassTypeID)
	END

	IF EXISTS (SELECT 1 FROM @issueCategoryType)
	BEGIN
		DELETE FROM @tt WHERE AssetIssueCategoryTypeID NOT IN (SELECT id FROM @issueCategoryType)
	END

	IF EXISTS (SELECT 1 FROM @issueCauseType)
	BEGIN
		DELETE FROM @tt WHERE IssueCauseTypeID IS NULL OR IssueCauseTypeID NOT IN (SELECT id FROM @issueCauseType)
	END
	
	IF EXISTS (SELECT 1 FROM @taggedAssetIssueID)
	BEGIN
		DELETE FROM @tt WHERE AssetIssueID IS NULL OR AssetIssueID NOT IN (SELECT id FROM @taggedAssetIssueID)
	END


	INSERT INTO @returnTable(AssetIssueID) SELECT DISTINCT id FROM @tt
	RETURN
END
GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs] TO [TEUser]
    AS [dbo];
GO
