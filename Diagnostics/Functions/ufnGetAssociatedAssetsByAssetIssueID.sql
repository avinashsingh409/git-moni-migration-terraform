﻿
CREATE   FUNCTION [Diagnostics].[ufnGetAssociatedAssetsByAssetIssueID]
(
	@assetIssueID INT
)
RETURNS TABLE AS
RETURN
(
	SELECT
		issue.AssetIssueAssociatedAssetID,
		issue.AssetID,
		asset.AssetAbbrev,
		asset.GlobalID
	FROM [Diagnostics].[tAssetIssueAssociatedAsset] issue
		LEFT JOIN [Asset].[tAsset] asset
		on issue.AssetID = asset.AssetID
	WHERE issue.AssetIssueID = @AssetIssueID
)
GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnGetAssociatedAssetsByAssetIssueID] TO [TEUser]
    AS [dbo];
GO