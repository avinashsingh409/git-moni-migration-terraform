﻿CREATE FUNCTION [Diagnostics].[ufnGetIssueTransitionEmail]
(
	@statusChanges [Diagnostics].[tpAssetIssueStatusChange] Readonly
)
RETURNS @returntable TABLE
(
	AssetIssueID INT,
	AssetIssueGUID UNIQUEIDENTIFIER,
	AssetID int,
	AssetDesc NVARCHAR(255),
	IssueTitle NVARCHAR(255),
	ChangeDate DATETIME,
	ChangedBy INT,
	FullName NVARCHAR(MAX),
	ActivityStatusFrom NVARCHAR(255) NULL,
	ActivityStatusTo NVARCHAR(255) NULL,
	ResolutionStatusFrom NVARCHAR(255) NULL,
	ResolutionStatusTo NVARCHAR(255) NULL,
	EmailToList NVARCHAR(MAX),
	EmailCcList NVARCHAR(MAX),
	EmailBccList NVARCHAR(MAX),
	EmailSubject NVARCHAR(MAX),
	EmailContent NVARCHAR(MAX),
	EmailOverride BIT,
	ActionName NVARCHAR(255),
	DelayHours INT, 
	DelayGuard NVARCHAR(255),
	CancelOnActivityStatusChange INT NULL,
	CancelOnResolutionStatusChange INT NULL,
	CategoryNotificationID INT
)
AS
BEGIN
	INSERT INTO @returntable 
	SELECT s.AssetIssueID, 
		s.AssetIssueGUID,
		s.AssetID,
		a.AssetDesc,
		s.IssueTitle,
		s.ChangeDate,
		s.ChangedBy,
		u.FirstName + ' ' + u.LastName As FullName,
		s.ActivityStatusFrom,
		s.ActivityStatusTo,
		s.ResolutionStatusFrom,
		s.ResolutionStatusTo,
		n.EmailRecipients as EmailToList,
		'' as EmailCcList,
		'' as EmailBccList,
		n.EmailSubject,
		n.EmailContent,
		n.OverrideEmail,
		n.ActionName,
		n.DelayHours,
		n.DelayGuard,
		n.CancelOnActivityStatusChange,
		n.CancelOnResolutionStatusChange,
		n.CategoryNotificationID
	FROM @statusChanges s
	LEFT JOIN Asset.tAsset a ON s.AssetID = a.AssetID
	LEFT JOIN AccessControl.tUser u ON s.ChangedBy = u.SecurityUserID
	LEFT JOIN Diagnostics.tCategoryNotification n ON s.NotificationID = n.CategoryNotificationID
	WHERE s.NotificationTypeID = 1 AND n.EmailSubscribers = 1
	
	--Replace tokens in email subject and body
	--[Diagnostics].[ufnReplaceToken] function inserts 'N/A' if tokenValue parameter is empty or null

	-- if this is running before a new issue is saved, AssetIssueID is not yet defined
	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'IssueID', Convert(nvarchar, AssetIssueID))
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'IssueID', Convert(nvarchar, AssetIssueID))
	--URL only allowed in body
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'IssueURL', Diagnostics.ufnGenerateURLtoIssue(AssetIssueID))

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'IssueTitle', IssueTitle)
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'IssueTitle', IssueTitle)

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'AssetDesc', AssetDesc) 
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'AssetDesc', AssetDesc)
		
	--URL only allowed in body
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'AssetURL', Diagnostics.ufnGenerateURLtoAsset(AssetID))

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'ActivityStatusFrom', ActivityStatusFrom)
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'ActivityStatusFrom', ActivityStatusFrom)

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'ActivityStatusTo', ActivityStatusTo)
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'ActivityStatusTo', ActivityStatusTo)

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'ResolutionStatusFrom', ResolutionStatusFrom)
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'ResolutionStatusFrom', ResolutionStatusFrom)

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'ResolutionStatusTo', ResolutionStatusTo)
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'ResolutionStatusTo', ResolutionStatusTo)

	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'ChangedBy', FullName)
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'ChangedBy', FullName)

	--Dates in the database are currently stored in server time (Central Time)
	Update @returntable Set EmailSubject = [Diagnostics].[ufnReplaceToken](EmailSubject, 'ChangeDate', Convert(VARCHAR(20), ChangeDate, 100) + ' Central Time')
	Update @returntable Set EmailContent = [Diagnostics].[ufnReplaceToken](EmailContent, 'ChangeDate', Convert(VARCHAR(20), ChangeDate, 100) + ' Central Time')
	
	--replace Asset Attribute tokens
	DECLARE @UserId int
	SELECT @UserId = ChangedBy FROM @returnTable
	
	UPDATE @returntable SET EmailSubject = [Diagnostics].[ufnReplaceTransitionEmailTokens](EmailSubject, AssetID, @UserId)
	UPDATE @returntable SET EmailContent = [Diagnostics].[ufnReplaceTransitionEmailTokens](EmailContent, AssetID, @UserId)
	
	--replace Mailing List tokens
	UPDATE @returntable SET EmailToList = [Diagnostics].[ufnReplaceTransitionEmailTokens](EmailToList, AssetID, @UserId)
	UPDATE @returntable SET EmailCcList = [Diagnostics].[ufnReplaceTransitionEmailTokens](EmailCcList, AssetID, @UserId)
	UPDATE @returntable SET EmailBccList = [Diagnostics].[ufnReplaceTransitionEmailTokens](EmailBccList, AssetID, @UserId)
	
	RETURN
END

GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnGetIssueTransitionEmail] TO [TEUser]
    AS [dbo];
GO