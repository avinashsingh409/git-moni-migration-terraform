﻿CREATE FUNCTION [Diagnostics].[ufnAssetIssueCategories]
(
	@assetId int	
)
RETURNS @categoriesTable TABLE
(
  AssetIssueCategoryTypeID int PRIMARY KEY not NULL
)
AS
BEGIN

DECLARE @assetclasstypeid INT;
SELECT @assetclasstypeid = AssetClassTypeID FROM Asset.tAsset WHERE AssetID = @assetId;

DECLARE @assets TABLE(assetID INT, assetClassTypeID int);

INSERT INTO @assets(assetID, assetClassTypeID)
SELECT a.AssetID, a.AssetClassTypeID FROM Asset.ufnAssetAncestorsIncludeAsset(@assetId) anc
INNER JOIN Asset.tAsset a ON anc.AssetID = a.AssetID;

INSERT INTO @categoriesTable (AssetIssueCategoryTypeID)
SELECT DISTINCT a.AssetIssueCategoryTypeID FROM (
	--This should take care of the situation where the asset matches the search criteria independent of ancestors
	SELECT AssetIssueCategoryTypeID FROM Diagnostics.tCategoryAccessRule r
	WHERE (r.AssetID = @assetId AND (r.AssetClassTypeID IS NULL OR r.AssetClassTypeID = @assetclasstypeid))
	OR
	(r.AssetClassTypeID = @assetclasstypeid AND r.AssetID IS NULL)
	OR 
	(r.AssetClassTypeID IS NULL AND r.AssetID IS NULL)

	UNION

	--Find stuff where the asset ID matches the ancestors
	SELECT AssetIssueCategoryTypeID FROM Diagnostics.tCategoryAccessRule r
	INNER JOIN @assets a ON r.AssetID = a.AssetID 
	WHERE r.AssetDescendants = 1 AND (r.AssetClassTypeID IS NULL OR (r.AssetClassTypeID = a.assetClassTypeID AND r.AssetClassTypeDescendants = 0))

	UNION 

	--Find stuff where the category matches the ancestors
	SELECT AssetIssueCategoryTypeID FROM Diagnostics.tCategoryAccessRule r
	INNER JOIN @assets a ON r.AssetClassTypeID = a.assetClassTypeID
	WHERE r.AssetClassTypeDescendants = 1 AND (r.AssetID IS NULL OR (r.AssetID = a.AssetID AND r.AssetDescendants = 0))

	UNION

	--Find stuff where the asset ID and class type id matches the ancestors
	SELECT AssetIssueCategoryTypeID FROM Diagnostics.tCategoryAccessRule r
	WHERE r.AssetDescendants = 1 AND r.AssetClassTypeDescendants = 1 AND r.AssetID IS NOT NULL AND r.AssetClassTypeID IS NOT NULL
	AND EXISTS(SELECT 1 FROM @assets where AssetID = r.AssetID)
	AND EXISTS(SELECT 1 FROM @assets WHERE AssetClassTypeID = r.AssetClassTypeID)
	) a

RETURN 
END
GO

GRANT SELECT
    ON OBJECT::[Diagnostics].[ufnAssetIssueCategories] TO [TEUser]
    AS [dbo];
GO