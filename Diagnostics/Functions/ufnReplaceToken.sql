﻿CREATE FUNCTION [Diagnostics].[ufnReplaceToken]
(
	@stringExpression NVARCHAR(MAX),
	@tokenKey NVARCHAR(MAX),  -- Do NOT surround tokenKey with double curly braces
	@tokenValue NVARCHAR(MAX) -- Inserts 'N/A' if tokenValue is empty or null
)
RETURNS NVARCHAR(MAX) 
AS
BEGIN

	IF @tokenKey IS NULL
	RETURN @stringExpression

	DECLARE @result NVARCHAR(MAX) = @stringExpression

	-- Find token in stringExpression
	DECLARE @tokensFound table (
		Token NVARCHAR(255) null
	)

	INSERT INTO @tokensFound
	SELECT DISTINCT Token
	FROM 
	(
		SELECT Tokens2.X.value('.', 'NVARCHAR(255)') AS Token
		FROM (SELECT CAST(REPLACE(REPLACE((SELECT @stringExpression FOR xml PATH('')), '{{', '<A>{{'), '}}', '}}</A>') AS xml).query('.')) AS Tokens1(X)
		CROSS APPLY Tokens1.X.nodes('/A/text()') AS Tokens2(X)
	) found
	WHERE TRIM(REPLACE(REPLACE(Token, '{', ''), '}', '')) = @tokenKey


	-- Replace token in stringExpression
	DECLARE @token NVARCHAR(255)
	DECLARE cur INSENSITIVE CURSOR FOR 
		SELECT Token FROM @tokensFound
		
	OPEN cur;
	FETCH NEXT FROM cur INTO @token
	
	WHILE @@FETCH_STATUS = 0
	BEGIN			
		SELECT @result = REPLACE(@result, @token, ISNULL(NULLIF(@tokenValue, ''), 'N/A'))
		
		FETCH NEXT FROM cur INTO @token
	END
	CLOSE cur;
	DEALLOCATE cur;
		

	RETURN @result

END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[ufnReplaceToken] TO [TEUSER]
    AS [dbo];
GO