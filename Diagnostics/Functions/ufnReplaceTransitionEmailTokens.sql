﻿CREATE   FUNCTION [Diagnostics].[ufnReplaceTransitionEmailTokens]
(
	@Body varchar(max),
	@AssetID int,
	@SecurityID int
)
RETURNS varchar(max)
AS
BEGIN

DECLARE @Body_Processed varchar(max)
SELECT @Body_Processed = @Body

	-- before proceeding, check if tokens have equal open/close brackets
	-- if not, return original Body since it will cause an error if we proceed
	DECLARE @openBrackets int
	DECLARE @closeBrackets int

	SELECT @openBrackets = DATALENGTH(REPLACE(@Body_Processed, '{{', ''))
	SELECT @closeBrackets = DATALENGTH(REPLACE(@Body_Processed, '}}', ''))

	IF @openBrackets <> @closeBrackets
	BEGIN
		RETURN @Body
	END

SELECT @Body_Processed = @Body

--step 1 (filter out tokens)
	DECLARE @step1 table (
		Token varchar(255) null
	)

	INSERT INTO @step1
		SELECT DISTINCT Token
		FROM
		(
			SELECT Tokens2.X.value('.', 'varchar(255)') AS Token
			FROM	
				(
				SELECT 
					CAST(REPLACE(REPLACE((SELECT @Body 
										FOR xml PATH('')), '{{', '<A>{{'), '}}', '}}</A>') AS xml).query('.')) AS Tokens1(X)
			 cROSS APPLY Tokens1.X.nodes('/A/text()') AS Tokens2(X)
 
		) step1

	
-- step 2 (check if attribute belongs to different namespaces
	DECLARE @step2 TABLE (
		Token varchar(255) null,
		Field varchar(255) null,
		[Namespace] varchar(255) null
	)

	INSERT INTO @step2
	SELECT 
		Token,
		Field = TRIM(Field),
		[Namespace] = ''
	FROM
		(
			SELECT 
				Token,
				REPLACE(REPLACE(Token,'{', ''), '}','') AS Field 
			FROM @step1
		) s


	update @step2
		set [Namespace] = Substring(Field, 1,Charindex('.', Field)-1),
			[Field] = TRIM(Substring(Field, Charindex('.', Field)+1, LEN(Field)))
	where Field like '%.%'
	

--step 3 (get token values)
	DECLARE @step3 TABLE (
		Token varchar(255) null,
		Field varchar(255) null,
		[Namespace] varchar(255) null,
		AttributeTypeId int null,  -- null if Asset doesn't have this Attribute
		TypeId int null,
		AttributeValue varchar(255)
	)

	INSERT INTO @step3
	-- Asset
		SELECT 
			Token,
			Field,
			[Namespace],
			AttributeTypeId = a.AttributeTypeId,
			TypeID = aa.AttributeTypeID,
			AttributeValue = [Asset].[ufnGetAttributeValueForAssetAndUser](a.AttributeTypeId, @AssetId, @SecurityID)
		FROM
			(
				SELECT 
					Token,
					Field,
					[Namespace]
				FROM @step2
				WHERE
					Field <> ''
					AND
					([Namespace] = 'Asset'  -- (other namespace can use a different query and use "UNION ALL")
					OR [Namespace] IS NULL -- default if no namespace is supplied
					OR [Namespace] = '')
			) s
		LEFT JOIN Asset.tAttributeType a ON s.field = a.AttributeTypeDesc
		LEFT JOIN Asset.tAssetAttribute aa ON a.AttributeTypeID = aa.AttributeTypeID 
		AND aa.AssetID  = @assetID
	
	--WorkItems (use UNION ALL)
	--TagNames (use UNION ALL)

--step 4 (if owning asset doesn't have a value for the Attribute, check its ancestors for someone who does)
	Declare @Token varchar(255)
	Declare @Field varchar(255)
	Declare @Namespace varchar(255)
	declare @Value varchar(255)
	declare @ParentAssetId int
	declare @AttributeTypeID int
	declare @TypeID int
	
	
	DECLARE cur1 INSENSITIVE CURSOR FOR 
		SELECT Token, Field, [Namespace], AttributeTypeId, TypeID, AttributeValue
		FROM @step3
		
	OPEN cur1;
	FETCH NEXT FROM cur1 INTO @Token, @Field, @Namespace, @AttributeTypeId, @TypeID, @Value
	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN			
		DECLARE @ParentValue varchar(255)
		SELECT @ParentAssetId = null
		SELECT @ParentValue = null

		IF @TypeID IS null
		BEGIN
			SELECT @ParentAssetId = ParentAssetID FROM Asset.tAsset WHERE AssetID = @AssetID
			
			-- get value up ancestor tree
			WHILE @ParentValue IS NULL AND @ParentAssetId IS NOT NULL
			BEGIN
				
				select @ParentValue = [Asset].[ufnGetAttributeValueForAssetAndUser](@AttributeTypeId, @ParentAssetId, @SecurityID)

				--get parent of parent
				select @ParentAssetId = ParentAssetID from Asset.tAsset where AssetID = @ParentAssetId
			END
			--update vaue on @step2
			UPDATE @step3 SET AttributeValue = ISNULL(@ParentValue,'')  WHERE AttributeTypeId = @AttributeTypeId
		END
		FETCH NEXT FROM cur1  INTO @Token, @Field, @Namespace, @AttributeTypeId, @TypeID, @Value
	END
	CLOSE cur1;
	DEALLOCATE cur1;

--step 5 eliminate duplicate token (in cases where AttributeTypeDesc is common to more than 1 Asset)
	DECLARE @step4 TABLE (
		Token varchar(255) null,
		AttributeValue varchar(255) null
	)
	
	insert into @step4
	SELECT distinct Token, AttributeValue = max(AttributeValue) FROM @step3 
		Group by Token
		order by Token, AttributeValue asc
	

--step 6 (replace tokens with values on the EmailBody string)
	DECLARE cur3 INSENSITIVE CURSOR FOR 
		SELECT Token, AttributeValue FROM @step4
		
	OPEN cur3;
	FETCH NEXT FROM cur3 INTO @Token, @Value
	
	WHILE @@FETCH_STATUS = 0
	BEGIN			
		select @Body_Processed = REPLACE(@Body_Processed, @Token, isnull(NULLIF(@Value,''),'N/A'))
		FETCH NEXT FROM cur3  INTO @Token, @Value 
	END
	CLOSE cur3;
	DEALLOCATE cur3;

	RETURN @Body_Processed

END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[ufnReplaceTransitionEmailTokens] TO [TEUSER]
    AS [dbo];
GO
