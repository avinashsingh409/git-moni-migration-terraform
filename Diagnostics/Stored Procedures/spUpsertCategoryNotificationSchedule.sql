﻿-----

CREATE PROCEDURE [Diagnostics].[spUpsertCategoryNotificationSchedule]
	@CategoryNotificationScheduleID int = NULL,
	@ScheduledDate DATETIME,
	@DelayGuard nvarchar (255),
	@CancelOnActivityStatusChange INT = NULL,
	@CancelOnResolutionStatusChange INT = NULL,
	@CategoryNotificationTypeID int,
	@EmailSubject nvarchar (255) = NULL,
	@EmailToList nvarchar (MAX) = NULL,
	@EmailCcList nvarchar (MAX) = NULL,
	@EmailBccList nvarchar (MAX) = NULL,
	@EmailToDisplayName nvarchar (MAX) = NULL,
	@EmailCcDisplayName nvarchar (MAX) = NULL,
	@EmailBccDisplayName nvarchar (MAX) = NULL,
	@EmailBody nvarchar (MAX) = NULL,
	@CreatedBy int = NULL,
	@EmailCategory nvarchar (255) = NULL,
	@FormatBodyAsHTML bit = NULL,
	@UserID int = NULL,
	@AssetID int = NULL,
	@AttributeName nvarchar (255) = NULL,
	@AttributeValue nvarchar (MAX) = NULL,
	@AuditTitle nvarchar (255) = NULL,
	@AuditContent nvarchar (MAX) = NULL,
	@AutoTitle nvarchar (255) = NULL,
	@AutoContent nvarchar (MAX) = NULL,
	@AssetIssueType int = NULL,
	@AssetIssueCategoryID int = NULL,
	@AssetIssueAction nvarchar (255) = NULL,
	@AssetIssueTitle nvarchar (255) = NULL,
	@AssetIssueSummary nvarchar (MAX) = NULL,
	@AssetIssueShortSummary nvarchar (MAX) = NULL,
	@AssetIssuePriority nvarchar(50) = NULL,
	@ResolveByDate datetime = NULL,
	@UnitOfMeasure nvarchar (255) = NULL,
	@Timestamp datetime = NULL,
	@CashFlowType nvarchar (255) = NULL,
	@CashFlowStatus int = NULL,
	@CashFlowValue float = NULL,
	@AssetIssueID int = NULL,
	@KeyTagOnIssue bit = NULL,
	@KeyTagAction nvarchar (40) = NULL,
	@KeyTag nvarchar (255) = NULL,
	@AssignedToEmail nvarchar (255) = NULL,
	@ActionPlanDate datetime = NULL,
	@UserAssignedAction nvarchar (MAX) = NULL,
	@SetStatusID int = NULL,
	@WorkerID NVARCHAR (50) = NULL,
	@WorkerLastActivity DATETIME = NULL,
	@Attachments NVARCHAR(MAX) = NULL,
	@NewCategoryNotificationScheduleID int OUTPUT
    AS
	BEGIN
		SET @NewCategoryNotificationScheduleID = -1

		--Make sure that the convention of treating a -1 as no value is respected
		IF @CategoryNotificationScheduleID IS NOT NULL AND @CategoryNotificationScheduleID <= 0
		BEGIN
			SET @CategoryNotificationScheduleID = NULL;
		END


		IF @CategoryNotificationScheduleID IS NULL
		BEGIN
			--INSERT CATEGORY NOTIFICATION SCHEDULE ITEM
			DECLARE @newIDs base.tpintlist;

			INSERT INTO [Diagnostics].[tCategoryNotificationSchedule]
			   (
				[ScheduledDate]
				,[DelayGuard]
				,[CancelOnActivityStatusChange]
			    ,[CancelOnResolutionStatusChange]
				,[CategoryNotificationTypeID]
				,[EmailSubject]
				,[EmailToList]
				,[EmailCcList]
				,[EmailBccList]
				,[EmailToDisplayName]
				,[EmailCcDisplayName]
				,[EmailBccDisplayName]
				,[EmailBody]
				,[CreatedBy]
				,[EmailCategory]
				,[FormatBodyAsHTML]
				,[UserID]
				,[AssetID]
				,[AttributeName]
				,[AttributeValue]
				,[AuditTitle]
				,[AuditContent]
				,[AutoTitle]
				,[AutoContent]
				,[AssetIssueType]
				,[AssetIssueCategoryID]
				,[AssetIssueAction]
				,[AssetIssueTitle]
				,[AssetIssueSummary]
				,[AssetIssueShortSummary]
				,[AssetIssuePriority]
				,[ResolveByDate]
				,[UnitOfMeasure]
				,[Timestamp]
				,[CashFlowType]
				,[CashFlowStatus]
				,[CashFlowValue]
				,[AssetIssueID]
				,[KeyTagOnIssue]
				,[KeyTagAction]
				,[KeyTag]
				,[AssignedToEmail]
				,[ActionPlanDate]
				,[UserAssignedAction]
				,[SetStatusID]
				,[WorkerID]
				,[WorkerLastActivity])
				OUTPUT INSERTED.CategoryNotificationScheduleID
				INTO @newIDs
			 VALUES
				(
				@ScheduledDate,
				@DelayGuard,
				@CancelOnActivityStatusChange,
			    @CancelOnResolutionStatusChange,
				@CategoryNotificationTypeID,
				@EmailSubject,
				@EmailToList,
				@EmailCcList,
				@EmailBccList,
				@EmailToDisplayName,
				@EmailCcDisplayName,
				@EmailBccDisplayName,
				@EmailBody,
				@CreatedBy,
				@EmailCategory,
				@FormatBodyAsHTML,
				@UserID,
				@AssetID,
				@AttributeName,
				@AttributeValue,
				@AuditTitle,
				@AuditContent,
				@AutoTitle,
				@AutoContent,
				@AssetIssueType,
				@AssetIssueCategoryID,
				@AssetIssueAction,
				@AssetIssueTitle,
				@AssetIssueSummary,
				@AssetIssueShortSummary,
				@AssetIssuePriority,
				@ResolveByDate,
				@UnitOfMeasure,
				@Timestamp,
				@CashFlowType,
				@CashFlowStatus,
				@CashFlowValue,
				@AssetIssueID,
				@KeyTagOnIssue,
				@KeyTagAction,
				@KeyTag,
				@AssignedToEmail,
				@ActionPlanDate,
				@UserAssignedAction,
				@SetStatusID,
				@WorkerID,
				@WorkerLastActivity)

			SELECT @NewCategoryNotificationScheduleID = id from @newIDs;

		END
		ELSE
		BEGIN
			--UPDATE CATEGORY NOTIFICATION SCHEDULE ITEM
			set @NewCategoryNotificationScheduleID = @CategoryNotificationScheduleID;

			UPDATE [Diagnostics].[tCategoryNotificationSchedule]
			   SET
				[ScheduledDate] = @ScheduledDate
				,[DelayGuard] = @DelayGuard
				,[CancelOnActivityStatusChange] = @CancelOnActivityStatusChange
				,[CancelOnResolutionStatusChange] = @CancelOnResolutionStatusChange
				,[CategoryNotificationTypeID] = @CategoryNotificationTypeID
				,[EmailSubject] = @EmailSubject
				,[EmailToList] = @EmailToList
				,[EmailCcList] = @EmailCcList
				,[EmailBccList] = @EmailBccList
				,[EmailToDisplayName] = @EmailToDisplayName
				,[EmailCcDisplayName] = @EmailCcDisplayName
				,[EmailBccDisplayName] = @EmailBccDisplayName
				,[EmailBody] = @EmailBody
				,[CreatedBy] = @CreatedBy
				,[EmailCategory] = @EmailCategory
				,[FormatBodyAsHTML] = @FormatBodyAsHTML
				,[UserID] = @UserID
				,[AssetID] = @AssetID
				,[AttributeName] = @AttributeName
				,[AttributeValue] = @AttributeValue
				,[AuditTitle] = @AuditTitle
				,[AuditContent] = @AuditContent
				,[AutoTitle] = @AutoTitle
				,[AutoContent] = @AutoContent
				,[AssetIssueType] = @AssetIssueType
				,[AssetIssueCategoryID] = @AssetIssueCategoryID
				,[AssetIssueAction] = @AssetIssueAction
				,[AssetIssueTitle] = @AssetIssueTitle
				,[AssetIssueSummary] = @AssetIssueSummary
				,[AssetIssueShortSummary] = @AssetIssueShortSummary
				,[AssetIssuePriority] = @AssetIssuePriority
				,[ResolveByDate] = @ResolveByDate
				,[UnitOfMeasure] = @UnitOfMeasure
				,[Timestamp] = @Timestamp
				,[CashFlowType] = @CashFlowType
				,[CashFlowStatus] = @CashFlowStatus
				,[CashFlowValue] = @CashFlowValue
				,[AssetIssueID] = @AssetIssueID
				,[KeyTagOnIssue] = @KeyTagOnIssue
				,[KeyTagAction] = @KeyTagAction
				,[KeyTag] = @KeyTag
				,[AssignedToEmail] = @AssignedToEmail
				,[ActionPlanDate] = @ActionPlanDate
				,[UserAssignedAction] = @UserAssignedAction
				,[SetStatusID] = @SetStatusID
				,[WorkerID] = @WorkerID
				,[WorkerLastActivity] = @WorkerLastActivity
			 WHERE [CategoryNotificationScheduleID] = @NewCategoryNotificationScheduleID

			 -- this is probably not necessary...
			DELETE FROM Diagnostics.tCategoryNotificationScheduleAttachment 
				WHERE CategoryNotificationScheduleID = @NewCategoryNotificationScheduleID;
	
		END

		IF (LEN(@Attachments) > 0)
		BEGIN
			INSERT INTO Diagnostics.tCategoryNotificationScheduleAttachment(CategoryNotificationScheduleID, FileGUID) 
				SELECT DISTINCT @NewCategoryNotificationScheduleID, a.value 
				FROM STRING_SPLIT(@Attachments,';') a;
		END

		--this isn't really necessary if the caller pays attention to the output parameter appropriately,
		--but if they don't and expect to read in results with a DataReader, this should make sure they
		--can still get the new ID out of the sproc.
		SELECT @NewCategoryNotificationScheduleID;
	END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertCategoryNotificationSchedule] TO [TEUser]
    AS [dbo];

GO

