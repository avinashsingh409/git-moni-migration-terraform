﻿
CREATE PROCEDURE [Diagnostics].[spUpsertAssetIssueAssociatedAssets]
	@AssetIssueID						int,
	@AssetIDs							Base.TpIntList READONLY
    AS
	BEGIN
	
		declare @NewAssetIssueAssociatedAssetIDs as Base.TpIntList
		 
		--delete existing AssociatedAssets of the given AssetIssue
		DELETE FROM [Diagnostics].[tAssetIssueAssociatedAsset]
		WHERE AssetIssueID = @AssetIssueID
	
		INSERT INTO
			[Diagnostics].[tAssetIssueAssociatedAsset]
			(
				AssetIssueID,
				AssetID
			)
			OUTPUT INSERTED.AssetIssueAssociatedAssetID
			INTO @NewAssetIssueAssociatedAssetIDs
		SELECT 
			@AssetIssueID, 
			id as AssetID
		FROM @AssetIDs assets;
		
		SELECT id from @NewAssetIssueAssociatedAssetIDs;
	END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertAssetIssueAssociatedAssets] TO [TEUser]
    AS [dbo];
GO
