﻿
CREATE PROCEDURE [Diagnostics].[spUpsertCategoryAccessRule]
	@CategoryAccessRuleID int = null,
	@AssetIssueCategoryTypeID int,
	@AssetID int = null,
	@AssetDescendants bit = null,
	@AssetClassTypeID int = null,
	@AssetClassTypeDescendants bit = null,
	@SecurityUserID int,
	@NewCategoryAccessRuleID int OUTPUT
    AS 
	BEGIN
		SET @NewCategoryAccessRuleID = -1

		--Make sure that the convention of treating a -1 as no value is respected
		IF @CategoryAccessRuleID IS NOT NULL AND @CategoryAccessRuleID <= 0
		BEGIN
			SET @CategoryAccessRuleID = NULL; 
		END


		IF @CategoryAccessRuleID IS NULL
		BEGIN
			DECLARE @newIDs base.tpintlist;
			INSERT INTO [Diagnostics].[tCategoryAccessRule]
				([AssetIssueCategoryTypeID]
				,[AssetID]
				,[AssetDescendants]
				,[AssetClassTypeID]
				,[AssetClassTypeDescendants]
				,[CreatedBy]
				,[ChangedBy]
				,[CreateDate]
				,[ChangeDate])
			OUTPUT INSERTED.CategoryAccessRuleID
			INTO @newIDs
			VALUES
				(@AssetIssueCategoryTypeID
				,@AssetID
				,@AssetDescendants
				,@AssetClassTypeID
				,@AssetClassTypeDescendants
				,@SecurityUserID
				,@SecurityUserID
				,SYSDATETIME()
				,SYSDATETIME())

			select top 1 @NewCategoryAccessRuleID = id
			from @newIDs;

		END
		ELSE
		BEGIN
			set @NewCategoryAccessRuleID = @CategoryAccessRuleID;			

			UPDATE [Diagnostics].[tCategoryAccessRule]
				SET  
					 [AssetIssueCategoryTypeID] = @AssetIssueCategoryTypeID
					,[AssetID] = @AssetID
					,[AssetDescendants] = @AssetDescendants
					,[AssetClassTypeID] = @AssetClassTypeID
					,[AssetClassTypeDescendants] = @AssetClassTypeDescendants
					,[ChangeDate] = GETDATE() 
					,[ChangedBy] = @SecurityUserID
				WHERE AssetIssueCategoryTypeID = @NewCategoryAccessRuleID
		END

		--this isn't really necessary if the caller pays attention to the output parameter appropriately,
		--but if they don't and expect to read in results with a DataReader, this should make sure they
		--can still get the new ID out of the sproc.
		SELECT @NewCategoryAccessRuleID;
	END
GO
GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertCategoryAccessRule] TO [TEUser]
    AS [dbo];

