﻿CREATE PROCEDURE Diagnostics.spMapDiscussionToAssetIssue 
	-- Add the parameters for the stored procedure here
	@DiscussionID UNIQUEIDENTIFIER,
	@AssetIssueID INT
AS
BEGIN
	IF NOT EXISTS( SELECT * FROM Diagnostics.tDiscussionAssetIssueMap WHERE DiscussionID = @DiscussionID AND AssetIssueID = @AssetIssueID)
	BEGIN
		INSERT INTO Diagnostics.tDiscussionAssetIssueMap (DiscussionID,AssetIssueID)
		VALUES(@DiscussionID, @AssetIssueID);

		SELECT SCOPE_IDENTITY();
	END
	ELSE
		SELECT -1;
END
GO
GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spMapDiscussionToAssetIssue] TO [TEUser]
    AS [dbo];