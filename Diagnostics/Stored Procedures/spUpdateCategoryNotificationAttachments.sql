﻿-----------

CREATE PROCEDURE [Diagnostics].[spUpdateCategoryNotificationAttachments]
@CatNotifyID int,
@Attachments [Base].[tpGuidIntList] READONLY
AS
BEGIN
DECLARE @ct int 
	DELETE FROM Diagnostics.tCategoryNotificationAttachment 
		WHERE CategoryNotificationID = @CatNotifyID;
	
	SELECT @ct = COUNT(*) FROM @Attachments
	IF (@ct > 0)
	BEGIN
		INSERT INTO Diagnostics.tCategoryNotificationAttachment(CategoryNotificationID, FileGUID, FileSize) 
			SELECT @CatNotifyID, a.GuidID, a.IntID FROM @Attachments a;
	END
END
GO

GRANT EXECUTE ON OBJECT::[Diagnostics].[spUpdateCategoryNotificationAttachments] TO [TEUser] AS [dbo];
GO
