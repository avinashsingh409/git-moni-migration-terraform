﻿
CREATE PROCEDURE [Diagnostics].[spUpsertAssetIssueActionPlan] 
	-- Add the parameters for the stored procedure here
	@AssetIssueActionPlanID INT = null,
	@AssetIssueID INT,
	@ActivityNumber INT,
	@ResponsibleParty NVARCHAR(255),
	@ActivityDate DATETIME = null,
	@Activity NVARCHAR(MAX),
	@Status NVARCHAR(255),
	@ChangingUser NVARCHAR(255),
	@newAssetIssueActionPlanID int OUTPUT
    AS 
	BEGIN
		SET @newAssetIssueActionPlanID = -1

		--Make sure that the convention of treating a -1 as no value is respected
		IF @AssetIssueActionPlanID IS NOT NULL AND @AssetIssueActionPlanID <= 0
		BEGIN
			SET @AssetIssueActionPlanID = NULL; 
		END

		IF @AssetIssueActionPlanID IS NULL 
		BEGIN
			--INSERT NEW ASSET ISSUE ACTION PLAN
			INSERT INTO [Diagnostics].[tAssetIssueActionPlan]
				([AssetIssueID]
				,[ActivityNumber]
				,[ResponsibleParty]
				,[ActivityDate]
				,[Activity]
				,[CreatedBy]
				,[ChangedBy]
				,[Status])
			VALUES
				(@AssetIssueID
				,@ActivityNumber
				,@ResponsibleParty
				,@ActivityDate
				,@Activity
				,@ChangingUser
				,@ChangingUser
				,@Status)


				SET @newAssetIssueActionPlanID = SCOPE_IDENTITY();

		END
		ELSE
		BEGIN
			--UPDATE ASSET ISSUE
			SET @newAssetIssueActionPlanID = @AssetIssueActionPlanID;

			UPDATE [Diagnostics].[tAssetIssueActionPlan]
			   SET [AssetIssueID] = @AssetIssueID
				  ,[ActivityNumber] = @ActivityNumber
				  ,[ResponsibleParty] = @ResponsibleParty
				  ,[ActivityDate] = @ActivityDate
				  ,[Activity] = @Activity
				  ,[ChangeDate] = getdate()
				  ,[ChangedBy] = @ChangingUser
				  ,[Status] = @Status
			 WHERE AssetIssueActionPlanID = @AssetIssueActionPlanID

		END

		SELECT @newAssetIssueActionPlanID;
	END
GO
GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertAssetIssueActionPlan] TO [TEUser]
    AS [dbo];

