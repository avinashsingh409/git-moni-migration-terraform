﻿CREATE PROCEDURE [Diagnostics].[spAssetIssuesCountFiltered] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID Base.tpIntList READONLY,
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@changeStartDate datetime = NULL,
	@changeEndDate datetime = NULL,
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@openDurationHigh int = NULL,
	@openDurationLow int = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	@impactCostHigh float = NULL,
	@impactCostLow float = NULL,
	@assignedTo NVARCHAR(255) = NULL,
	@scorecard bit = NULL,
	@priority NVARCHAR(255) = NULL,
	@activityStatus Base.tpIntList READONLY,
	@resolutionStatus Base.tpIntList READONLY,
	@issueTypeID Base.tpIntList  READONLY,
	@issueClassTypeID Base.tpIntList READONLY,
	@issueCategoryType Base.tpIntList READONLY,
	@issueCauseType Base.tpIntList READONLY,
	@taggedAssetIssueID Base.tpIntList READONLY,
	@changedBy NVARCHAR(255) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET ARITHABORT ON
    
	declare @assetCnt as int
	select @assetCnt = COUNT(*) from @assetID

	DECLARE @blankAssets Base.tpIntList;
	
	--drop table #tmp_issues2
	select a.AssetIssueID,b.AssetID into #tmp_issues FROM [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs](
							@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
							@openDurationHigh,       @openDurationLow,@id,@title,@impactCostHigh,@impactCostLow,
							@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,       @issueClassTypeID, 
							@issueCategoryType, @issueCauseType, @taggedAssetIssueID, @blankAssets,@scorecard,
							@priority, @changedBy) a JOIN Diagnostics.tAssetIssue b on a.AssetIssueID = b.AssetIssueID
	DECLARE @securityassets Base.tpIntList;
	insert into @securityassets select distinct assetid from #tmp_issues
	select a.* into #tmp_issues2 from #tmp_issues a join 
	Asset.ufnUserHasAccessToAssets(@securityuserid,@securityassets) b 	
				on a.AssetID = b.AssetID 
	drop table #tmp_issues
	
	declare @assetCounts as table (cnt int, assetid int);
	insert into @assetCounts select 0,id from @assetID

	DECLARE @results TABLE(assetID int, issueCount int);

	if @assetCnt > 0
	  begin
	  insert into @results
	  select c.id as AssetID,COUNT(*) as issueCount from #tmp_issues2 a cross apply Asset.[ufnAssetAncestorsIncludeAsset](a.AssetID) b join @assetID c on b.assetid = c.id
  	  join Asset.tAsset d on c.id = d.AssetID JOIN Asset.tAssetClassType e on d.AssetClassTypeID = e.AssetClassTypeID
	    group by c.id 
	  end
	else
	  begin
	  insert into @results
	  select 0 as AssetID,COUNT(*) as issueCount from #tmp_issues2 a join Asset.tAsset b on a.AssetID = b.AssetID	  
	  end
	
	drop table #tmp_issues2

	select a.*,isnull(d.AssetDesc,'') as AssetDesc,isnull(d.AssetClassTypeID,0) as AssetClassTypeID,
	  isnull(e.AssetClassTypeDesc,'') as AssetClassTypeDesc from @results a left join Asset.tAsset d on a.assetID = d.AssetID left 
	JOIN Asset.tAssetClassType e on d.AssetClassTypeID = e.AssetClassTypeID order by 1
	
END

GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAssetIssuesCountFiltered] TO [TEUser]
    AS [dbo];

