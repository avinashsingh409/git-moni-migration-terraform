﻿CREATE PROCEDURE [Diagnostics].[spDeleteAssetIssueCategoryType]
	@id int 
	
AS
	
	BEGIN TRANSACTION

	IF EXISTS (SELECT * FROM Diagnostics.tAssetIssue WHERE AssetIssueCategoryTypeID = @id)
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category type: Asset Issues exist with type', 16,1)
		RETURN
	END 

	DELETE FROM Diagnostics.tCategoryNotification 
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category notification.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryAccessRule 
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category access rule.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryAssetIssueActivityStatusTypeTransition
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category status type transition.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryAssetIssueResolutionStatusTypeTransition
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category resolution type transition.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryAssetIssueActivityStatusTypeMap
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category status type map.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryAssetIssueResolutionStatusTypeMap
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category resolution type map.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryAssetIssueImpactTypeMap
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category impact type map.', 16,1)
		RETURN
	END

	DELETE FROM Diagnostics.tCategoryIssueTypeMap
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category issue type map.', 16,1)
		RETURN
	END


	DELETE FROM Diagnostics.tAssetIssueCategoryType 
	WHERE AssetIssueCategoryTypeID = @id

	IF @@Error <> 0
	BEGIN
		ROLLBACK

		RAISERROR ('Error in deleting asset issue category type.', 16,1)
		RETURN
	END

	COMMIT

RETURN 0

GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spDeleteAssetIssueCategoryType] TO [TEUser]
    AS [dbo];