﻿
CREATE PROCEDURE [Diagnostics].[spUpsertAssetIssueCategoryType]
	@AssetIssueCategoryTypeID int = null,
	@CategoryAbbrev nvarchar(255),
	@CategoryDesc nvarchar(255),
	@IssueClassTypeID int,
	@SecurityUserID int,
	@NewAssetIssueCategoryTypeID int OUTPUT
    AS 
	BEGIN
		SET @NewAssetIssueCategoryTypeID = -1

		--Make sure that the convention of treating a -1 as no value is respected
		IF @AssetIssueCategoryTypeID IS NOT NULL AND @AssetIssueCategoryTypeID <= 0
		BEGIN
			SET @AssetIssueCategoryTypeID = NULL; 
		END


		IF @AssetIssueCategoryTypeID IS NULL
		BEGIN
			--INSERT NEW ASSET ISSUE CATEGORY TYPE
			DECLARE @newIDs base.tpintlist;
			INSERT INTO [Diagnostics].[tAssetIssueCategoryType]
			   ([CategoryAbbrev]
			   ,[CategoryDesc]
			   ,[IssueClassTypeID]
			   ,[CreatedBy]
			   ,[ChangedBy]
			   ,[CreateDate]
			   ,[ChangeDate])
			OUTPUT INSERTED.AssetIssueCategoryTypeID
			INTO @newIDs
			VALUES
			   (@CategoryAbbrev
			   ,@CategoryDesc
			   ,@IssueClassTypeID
			   ,@SecurityUserID
			   ,@SecurityUserID
			   ,SYSDATETIME()
			   ,SYSDATETIME())


			select top 1 @NewAssetIssueCategoryTypeID = id
			from @newIDs;

		END
		ELSE
		BEGIN
			--UPDATE ASSET ISSUE CATEGORY TYPE
			set @NewAssetIssueCategoryTypeID = @AssetIssueCategoryTypeID;			

			--Permissions have been checked, need to update the asset issue
			UPDATE [Diagnostics].[tAssetIssueCategoryType]
				SET [CategoryAbbrev] = @CategoryAbbrev
					,[CategoryDesc] = @CategoryDesc
					,[IssueClassTypeID] = @IssueClassTypeID
					,[ChangeDate] = GETDATE()
					,[ChangedBy] = @SecurityUserID
				WHERE AssetIssueCategoryTypeID = @NewAssetIssueCategoryTypeID
		END
		
		--this isn't really necessary if the caller pays attention to the output parameter appropriately,
		--but if they don't and expect to read in results with a DataReader, this should make sure they
		--can still get the new ID out of the sproc.
		SELECT @NewAssetIssueCategoryTypeID;
	END
GO



GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertAssetIssueCategoryType] TO [TEUser]
    AS [dbo];

