﻿CREATE PROCEDURE [Diagnostics].[spAssetIssuesFiltered] 
	-- Add the parameters for the stored procedure here
	@securityUserID int,
	@assetID int = NULL,
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@changeStartDate datetime = NULL,
	@changeEndDate datetime = NULL,
	@closeStartDate datetime = NULL,
	@closeEndDate datetime = NULL,
	@openDurationHigh int = NULL,
	@openDurationLow int = NULL,
	@id NVARCHAR(10) = NULL,
	@title NVARCHAR(255) = NULL,
	@impactCostHigh float = NULL,
	@impactCostLow float = NULL,
	@assignedTo NVARCHAR(255) = NULL,
	@scorecard bit = NULL,
	@priority NVARCHAR(255) = NULL,
	@activityStatus Base.tpIntList READONLY,
	@resolutionStatus Base.tpIntList READONLY,
	@issueTypeID Base.tpIntList  READONLY,
	@issueClassTypeID Base.tpIntList READONLY,
	@issueCategoryType Base.tpIntList READONLY,
	@issueCauseType Base.tpIntList READONLY,
	@taggedAssetIssueID Base.tpIntList READONLY,
	@startIndex int = 1, 
	@endIndex int = 15,
	@sort NVARCHAR(100) = NULL,
	@sortOrder bit = 0,
	@changedBy NVARCHAR(255) = NULL,
	@total int OUTPUT,
	@includeAssociatedAssets bit = 0

WITH RECOMPILE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    
    SET ARITHABORT ON
    
    DECLARE @assets base.tpIntList
    DECLARE @associatedAssetsIssue base.tpIntList 	
    insert into @assets select AssetId from Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID,@assetID);
    
    DECLARE @issueIDs TABLE
	(
		AssetIssueID int,
		IsAssociated bit
	)
	if @includeAssociatedAssets = 1
	begin
	 		declare @associatedAssets base.tpIntList 
			insert into @associatedAssets select distinct a.AssetID from Diagnostics.vAssetIssue a
			left join Diagnostics.tAssetIssueKeywordMap b on b.AssetIssueID = a.AssetIssueID
			inner join Diagnostics.tAssetIssueAssociatedAsset c on c.AssetIssueID = a.AssetIssueID
			where c.AssetID IN(select id from @assets)
				
			if exists(select 1/0 from @associatedAssets)
			begin
		 		insert into @associatedAssetsIssue select distinct issueIDCheck.AssetIssueID as AssetIssueID  from
				[Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
				@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
				@openDurationHigh,	@openDurationLow,@id,@title,@impactCostHigh,@impactCostLow,
				@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,	@issueClassTypeID,@issueCategoryType, 
				@issueCauseType,@taggedAssetIssueID,@associatedAssets, @scorecard,@priority, @changedBy) issueIDCheck 
				JOIN Diagnostics.tAssetIssue b on issueIDCheck.AssetIssueID = b.AssetIssueID 
				INNER JOIN Diagnostics.tAssetIssueAssociatedAsset c on c.AssetIssueID = issueIDCheck.AssetIssueID
				WHERE c.AssetID IN(select id from @assets)
			end
	end
	declare @owningAssetsIssue base.tpIntList 
	insert into @owningAssetsIssue select distinct AssetIssueID from
    [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
		@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
		@openDurationHigh,	@openDurationLow,@id,@title,@impactCostHigh,@impactCostLow,
		@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,	@issueClassTypeID,@issueCategoryType, 
		@issueCauseType,@taggedAssetIssueID,@assets, @scorecard,@priority, @changedBy) issueIDCheck			
    insert into @issueIDs select id,0 from @owningAssetsIssue	
    union all
    select id,1 from @associatedAssetsIssue where id not in(select id from @owningAssetsIssue)


DECLARE @rows as TABLE
( ROW_NUMBER INT, AssetIssueID INT, IsAssociated BIT);

INSERT INTO @rows
SELECT ROW_NUMBER() OVER (	
		ORDER BY 
		CASE WHEN @sort IS NULL THEN [Diagnostics].[vAssetIssue].[AssetIssueID] ELSE NULL END,
		CASE WHEN @sort = 'AssetIssueID' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssetIssueID] ELSE NULL END,
		CASE WHEN @sort = 'IssueTypeAbbrev' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[IssueTypeAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'CreateDate' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[CreateDate] ELSE NULL END,
		CASE WHEN @sort = 'ChangeDate' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[ChangeDate] ELSE NULL END,
		CASE WHEN @sort = 'ChangedBy' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[ChangedBy] ELSE NULL END,
		CASE WHEN @sort = 'CloseDate' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[CloseDate] ELSE NULL END,
		CASE WHEN @sort = 'OpenDate' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[OpenDate] ELSE NULL END,
		CASE WHEN @sort = 'IssueTitle' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[IssueTitle] ELSE NULL END,
		CASE WHEN @sort = 'Priority' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[Priority] ELSE NULL END,
		CASE WHEN @sort = 'CategoryAbbrev' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[CategoryAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'ActivityStatus' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[ActivityStatus] ELSE NULL END,
		CASE WHEN @sort = 'ResolutionStatus' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[ResolutionStatus] ELSE NULL END,
		CASE WHEN @sort = 'ImpactTotal' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[ImpactTotal] ELSE NULL END,
		CASE WHEN @sort = 'AssignedTo' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssignedTo] ELSE NULL END,
		CASE WHEN @sort = 'OpenDuration' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[OpenDuration] ELSE NULL END,
		CASE WHEN @sort = 'AssetDesc' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssetDesc] ELSE NULL END,
		CASE WHEN @sort = 'IssueCauseDesc' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[IssueCauseDesc] ELSE NULL END,
		CASE WHEN @sort = 'Scorecard' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[Scorecard] ELSE NULL END,
		CASE WHEN @sort = 'AssetClassTypeAbbrev' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssetClassTypeAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'AssetClassTypeDesc' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssetClassTypeDesc] ELSE NULL END,
		CASE WHEN @sort = 'AssetTypeAbbrev' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssetTypeAbbrev] ELSE NULL END,
		CASE WHEN @sort = 'AssetTypeDesc' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[AssetTypeDesc] ELSE NULL END,
		CASE WHEN @sort = 'ResolveBy' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[ResolveBy] ELSE NULL END,
		CASE WHEN @sort = 'CreatedBy' AND @sortOrder = 0 THEN [Diagnostics].[vAssetIssue].[CreatedBy] ELSE NULL END,
		CASE WHEN @sort = 'AssetIssueID' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssetIssueID] ELSE NULL END DESC,
		CASE WHEN @sort = 'IssueTypeAbbrev' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[IssueTypeAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'CreateDate' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[CreateDate] ELSE NULL END DESC,
		CASE WHEN @sort = 'ChangeDate' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[ChangeDate] ELSE NULL END DESC,
		CASE WHEN @sort = 'CloseDate' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[CloseDate] ELSE NULL END DESC,
		CASE WHEN @sort = 'OpenDate' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[OpenDate] ELSE NULL END DESC,
		CASE WHEN @sort = 'IssueTitle' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[IssueTitle] ELSE NULL END DESC,
		CASE WHEN @sort = 'Priority' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[Priority] ELSE NULL END DESC,
		CASE WHEN @sort = 'CategoryAbbrev' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[CategoryAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'ActivityStatus' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[ActivityStatus] ELSE NULL END DESC,
		CASE WHEN @sort = 'ResolutionStatus' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[ResolutionStatus] ELSE NULL END DESC,
		CASE WHEN @sort = 'ImpactTotal' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[ImpactTotal] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssignedTo' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssignedTo] ELSE NULL END DESC,
		CASE WHEN @sort = 'OpenDuration' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[OpenDuration] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetDesc' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssetDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'IssueCauseDesc' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[IssueCauseDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'Scorecard' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[Scorecard] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetClassTypeAbbrev' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssetClassTypeAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetClassTypeDesc' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssetClassTypeDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetTypeAbbrev' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssetTypeAbbrev] ELSE NULL END DESC,
		CASE WHEN @sort = 'AssetTypeDesc' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[AssetTypeDesc] ELSE NULL END DESC,
		CASE WHEN @sort = 'ResolveBy' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[ResolveBy] ELSE NULL END DESC,
		CASE WHEN @sort = 'CreatedBy' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[CreatedBy] ELSE NULL END DESC,
		CASE WHEN @sort = 'ChangedBy' AND @sortOrder = 1 THEN [Diagnostics].[vAssetIssue].[ChangedBy] ELSE NULL END DESC,
		Diagnostics.vAssetIssue.AssetIssueID
		) AS ROW_NUMBER,
		Diagnostics.vAssetIssue.AssetIssueID, issueIDCheck.IsAssociated
		FROM Diagnostics.vAssetIssue
		INNER JOIN @issueIDs issueIDCheck
			ON issueIDCheck.AssetIssueID = Diagnostics.vAssetIssue.AssetIssueID OPTION (RECOMPILE);
	
	SET @total = (SELECT COUNT(*) FROM @rows);
			
	-- Insert statements for procedure here
	WITH records as (
		SELECT * FROM @rows
	)

	SELECT 
	    r.ROW_NUMBER,
		r.IsAssociated,
		asset.AssetDesc as asset_AssetDesc,
		asset.AssetAbbrev as asset_AssetAbbrev,
		asset.AssetClassTypeID as asset_AssetClassTypeID,
		syst.AssetDesc as system_AssetDesc,
		syst.AssetAbbrev as system_AssetAbbrev,
		syst.AssetClassTypeID as system_AssetClassTypeID,
		unit.AssetDesc as unit_AssetDesc,
		unit.AssetAbbrev as unit_AssetAbbrev,
		unit.AssetClassTypeID as unit_AssetClassTypeID,
		station.AssetDesc as station_AssetDesc,
		station.AssetAbbrev as station_AssetAbbrev,
		station.AssetClassTypeID as station_AssetClassTypeID,
		stationgroup.AssetDesc as stationgroup_AssetDesc,
		stationgroup.AssetAbbrev as stationgroup_AssetAbbrev,
		stationgroup.AssetClassTypeID as stationgroup_AssetClassTypeID,
		client.AssetDesc as client_AssetDesc,
		client.AssetAbbrev as client_AssetAbbrev,
		client.AssetClassTypeID as client_AssetClassTypeID,

		a.AssetIssueID as AssetIssueID,
		a.GlobalID as AssetIssueGlobalID,
		a.CreateDate as CreateDate,
		a.CreatedBy as CreatedBy,
		a.CreatedByUserID as CreatedByUserID,
		a.ChangeDate as ChangeDate,
		a.CloseDate as CloseDate,
		a.OpenDate as OpenDate,
		a.OpenDuration as OpenDuration,
		a.IssueTypeID as IssueTypeID,
		a.IssueTypeAbbrev as IssueTypeAbbrev,
		a.IssueTypeDesc as IssueTypeDesc,
		a.IssueClassTypeID as IssueClassTypeID,
		a.IssueClassTypeAbbrev as IssueClassTypeAbbrev,
		a.IssueClassTypeDesc as IssueClassTypeDesc,
		a.IssueShortSummary as IssueShortSummary,
		a.IssueSummary as IssueSummary,
		a.IssueTitle as IssueTitle,
		a.Confidence_Pct as ConfidencePct,
		a.[Priority] as [Priority],
		a.[ActivityStatus] as [ActivityStatus],
		a.[ResolutionStatus] as [ResolutionStatus],
		a.AssetIssueCategoryTypeID as AssetIssueCategoryTypeID,
		a.CategoryAbbrev as CategoryAbbrev, 
		a.CategoryDesc as CategoryDesc,
		a.ImpactTotal as ImpactTotal,
		a.AssignedTo as AssignedTo,
		a.AssignedToEmail as AssignedToEmail,
		a.AssignedToUsername as AssignedToUsername,
		a.Scorecard as Scorecard,
		a.AssetClassTypeAbbrev as AssetClassTypeAbbrev,
		a.AssetClassTypeDesc as AssetClassTypeDesc,
		a.AssetTypeAbbrev as AssetTypeAbbrev,
		a.AssetTypeDesc as AssetTypeDesc,
		a.ResolveBy as ResolveBy,
		a.ChangedBy as ChangedBy,
		CASE WHEN sub.SecurityUserID IS NULL THEN 0 ELSE 1 END as IsSubscribed
		
		FROM Diagnostics.vAssetIssue a 
		LEFT JOIN Asset.tAsset client 
			ON client.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'CLT')
		LEFT JOIN Asset.tAsset stationgroup 
			ON stationgroup.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'SG')
		LEFT JOIN Asset.tAsset station
			ON station.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'ST')
		LEFT JOIN Asset.tAsset unit
			ON unit.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'UN')
		LEFT JOIN Asset.tAsset syst
			ON syst.AssetID = Asset.ufnGetParentAssetIDOfAssetIDForAssetTypeAbbrev(a.AssetID,'SYS')
		LEFT JOIN Asset.tAsset asset 
			ON asset.AssetID = a.AssetID
		LEFT JOIN (
			SELECT DISTINCT Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap.AssetIssueID, 
							Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap.SecurityUserID
			FROM Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap 
			WHERE Diagnostics.tSubscriptionAssetIssueSecurityUserIDMap.SecurityUserID = @securityUserID) sub
			ON sub.AssetIssueID = a.AssetIssueID
		INNER JOIN records r ON a.AssetIssueID = r.AssetIssueID
	WHERE r.ROW_NUMBER BETWEEN @startIndex AND @endIndex
	ORDER BY r.ROW_NUMBER
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAssetIssuesFiltered] TO [TEUser]
    AS [dbo];

