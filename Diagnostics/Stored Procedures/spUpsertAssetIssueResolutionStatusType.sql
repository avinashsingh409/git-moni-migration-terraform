﻿
CREATE PROCEDURE [Diagnostics].[spUpsertAssetIssueResolutionStatusType]
	@AssetIssueResolutionStatusTypeID int = null,
	@AssetIssueResolutionStatusTypeDesc nvarchar(255),
	@SecurityUserID int,
	@NewAssetIssueResolutionStatusTypeID int OUTPUT
    AS 
	BEGIN
		SET @NewAssetIssueResolutionStatusTypeID = -1

		--Make sure that the convention of treating a -1 as no value is respected
		IF @AssetIssueResolutionStatusTypeID IS NOT NULL AND @AssetIssueResolutionStatusTypeID <= 0
		BEGIN
			SET @AssetIssueResolutionStatusTypeID = NULL; 
		END


		IF @AssetIssueResolutionStatusTypeID IS NULL
		BEGIN
			--INSERT NEW ASSET ISSUE RESOLUTION STATUS TYPE
			DECLARE @newIDs base.tpintlist;
			INSERT INTO [Diagnostics].[tAssetIssueResolutionStatusType]
			   ([AssetIssueResolutionStatusTypeDesc]
			   ,[CreatedBy]
			   ,[CreateDate]
			   )
			OUTPUT INSERTED.AssetIssueResolutionStatusTypeID
			INTO @newIDs
			VALUES
			   (@AssetIssueResolutionStatusTypeDesc
			   ,@SecurityUserID
			   ,SYSDATETIME()
			   )


			select top 1 @NewAssetIssueResolutionStatusTypeID = id
			from @newIDs;

		END
		ELSE
		BEGIN
			--UPDATE ASSET ISSUE RESOLUTION STATUS TYPE
			set @NewAssetIssueResolutionStatusTypeID = @AssetIssueResolutionStatusTypeID;			

			--Permissions have been checked, need to update the asset issue
			UPDATE [Diagnostics].[tAssetIssueResolutionStatusType]
				SET AssetIssueResolutionStatusTypeDesc = @AssetIssueResolutionStatusTypeDesc
				WHERE AssetIssueResolutionStatusTypeID = @NewAssetIssueResolutionStatusTypeID
		END
		
		--this isn't really necessary if the caller pays attention to the output parameter appropriately,
		--but if they don't and expect to read in results with a DataReader, this should make sure they
		--can still get the new ID out of the sproc.
		SELECT @NewAssetIssueResolutionStatusTypeID;
	END
GO
GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertAssetIssueResolutionStatusType] TO [TEUser]
    AS [dbo];

