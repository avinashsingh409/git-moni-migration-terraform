﻿CREATE PROCEDURE [Diagnostics].[spAssetIssueImpactCosts]  
 @securityUserID int,  
 @assetID int = NULL,  
 @startDate datetime = NULL,  
 @endDate datetime = NULL,  
 @changeStartDate datetime = NULL,  
 @changeEndDate datetime = NULL,  
 @closeStartDate datetime = NULL,  
 @closeEndDate datetime = NULL,  
 @openDurationHigh int = NULL,  
 @openDurationLow int = NULL,  
 @id NVARCHAR(10) = NULL,  
 @title NVARCHAR(255) = NULL,  
 @impactCostHigh float = NULL,  
 @impactCostLow float = NULL,  
 @assignedTo NVARCHAR(255) = NULL,  
 @scorecard bit = NULL,  
 @priority NVARCHAR(255) = NULL,  
 @changedBy NVARCHAR(255) = NULL,  
 @activityStatus Base.tpIntList READONLY,  
 @resolutionStatus Base.tpIntList READONLY,  
 @issueTypeID Base.tpIntList  READONLY,  
 @issueClassTypeID Base.tpIntList READONLY,  
 @issueCategoryType Base.tpIntList READONLY,  
 @issueCauseType Base.tpIntList READONLY,  
 @taggedAssetIssueID Base.tpIntList READONLY,
 @includeAssociatedAssets bit = 0
AS  
BEGIN  
 SET NOCOUNT ON;  -- to prevent extra result sets from interfering with SELECT statements
 SET ARITHABORT ON

 DECLARE @owningAssetsIssueImpact TABLE(AssetIssueImpactTypeID int not null, AssetIssueID int, SummedCost float);
 DECLARE @assetByIssueImpact TABLE(AssetIssueImpactTypeID int not null, AssetIssueID int, SummedCost float);		
 DECLARE @assetIDs Base.tpIntList

 INSERT into @assetIDs SELECT a.AssetID from Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID,@assetID) a
 
 select * into #tmp_issueIDs from [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs](   
	@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,  
	@openDurationHigh, @openDurationLow, @id, @title, @impactCostHigh,@impactCostLow,  
	@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,  
	@issueCategoryType, @issueCauseType,@taggedAssetIssueID,@assetIDs, @scorecard,  
	@priority, @changedBy)  
 
 SELECT  
	map.AssetIssueID  
	, map.AssetIssueImpactTypeID  
	, map.Impact_Cost * (scen.PercentLikelihood / 100) AS ProbabilityWeightedImpactCost  
 INTO #tmp_impacts  
 FROM [Diagnostics].[tAssetIssueAssetIssueImpactTypeMap] map  
 JOIN [Diagnostics].[tAssetIssueImpactScenario] scen  
	ON  
	(map.AssetIssueID = scen.OwningAssetIssueID) AND  
	(  
		(map.AssetIssueAssetIssueImpactTypeMapID = scen.Generation_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.Efficiency_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.Maintenance_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.OtherCosts_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.Scope_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.Schedule_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.Budget_AssetIssueAssetIssueImpactTypeMapID)  
		OR (map.AssetIssueAssetIssueImpactTypeMapID = scen.Safety_AssetIssueAssetIssueImpactTypeMapID)  
	)

--Associated Assets
 IF @includeAssociatedAssets = 1
 BEGIN
	 DECLARE @associatedAssets base.tpIntList;
	 DECLARE @associatedAssetsIssueImpact TABLE(AssetIssueImpactTypeID int not null, AssetIssueID int, SummedCost float);
	 INSERT INTO @associatedAssets SELECT DISTINCT a.AssetID FROM Diagnostics.vAssetIssue a
	 LEFT JOIN Diagnostics.tAssetIssueKeywordMap b ON b.AssetIssueID = a.AssetIssueID
	 INNER JOIN Diagnostics.tAssetIssueAssociatedAsset c ON c.AssetIssueID = a.AssetIssueID
	 WHERE c.AssetID IN (select id from @assetIDs)
 
	 IF EXISTS(SELECT 1/0 FROM @associatedAssets)
	 BEGIN
	 INSERT INTO @associatedAssetsIssueImpact SELECT aiit.AssetIssueImpactTypeID, ai.AssetIssueID as AssetIssueID, aiaiitm.ProbabilityWeightedImpactCost as SummedCost  
	 FROM [Diagnostics].[ufnGetFilteredAssetIssueIDsFromAssetIDs]( 
					@startDate, @endDate, @changeStartDate,@changeEndDate,@closeStartDate,@closeEndDate,
					@openDurationHigh, @openDurationLow, @id, @title, @impactCostHigh,@impactCostLow,
					@assignedTo,@activityStatus,@resolutionStatus,@issueTypeID,@issueClassTypeID,
					@issueCategoryType, @issueCauseType,@taggedAssetIssueID,@associatedAssets, @scorecard,
					@priority,@changedBy) filter
	 INNER JOIN Diagnostics.tAssetIssue ai ON filter.AssetIssueID = ai.AssetIssueID
	 INNER JOIN Diagnostics.tAssetIssueCategoryType catType ON catType.AssetIssueCategoryTypeID = ai.AssetIssueCategoryTypeID
	 INNER JOIN Diagnostics.tAssetIssueAssociatedAsset assc ON assc.AssetIssueID = filter.AssetIssueID
	 INNER JOIN #tmp_impacts aiaiitm ON ai.AssetIssueID = aiaiitm.AssetIssueID  
	 INNER JOIN Diagnostics.tAssetIssueImpactType aiit ON aiaiitm.AssetIssueImpactTypeID = aiit.AssetIssueImpactTypeID
	 WHERE ai.IsHidden = 0 AND catType.IsHidden = 0    --relevant WHERE clause from vAssetIssue 
	 AND assc.AssetID IN (select id from @assetIDs)
	 END
 END
 INSERT INTO @owningAssetsIssueImpact
 SELECT	
	aiit.AssetIssueImpactTypeID
	, ai.AssetIssueID as AssetIssueID
	, aiaiitm.ProbabilityWeightedImpactCost as SummedCost 
 FROM Diagnostics.tAssetIssue ai --only need asset ID and asset issue ID, use tAssetIssue not vAssetIssue
 INNER JOIN Diagnostics.tAssetIssueCategoryType catType
 ON catType.AssetIssueCategoryTypeID = ai.AssetIssueCategoryTypeID
 --Join to filter on asset tree  
 INNER JOIN  @assetIDs branch  
	on ai.AssetID = branch.ID   
 INNER JOIN #tmp_issueIDs issueIDCheck  
	ON issueIDCheck.AssetIssueID = ai.AssetIssueID    
 INNER JOIN #tmp_impacts aiaiitm 
	ON ai.AssetIssueID = aiaiitm.AssetIssueID  
 INNER JOIN [Diagnostics].[tAssetIssueImpactType] aiit 
	on aiaiitm.AssetIssueImpactTypeID = aiit.AssetIssueImpactTypeID
 WHERE ai.IsHidden = 0 AND catType.IsHidden = 0	   --relevant WHERE clause from vAssetIssue

 INSERT INTO @assetByIssueImpact SELECT * FROM @owningAssetsIssueImpact
 UNION
 SELECT * FROM @associatedAssetsIssueImpact WHERE AssetIssueID NOT IN (SELECT AssetIssueID FROM @assetByIssueImpact)

 SELECT AssetIssueImpactTypeID,SUM(SummedCost) as SummedCost FROM @assetByIssueImpact 
 GROUP BY AssetIssueImpactTypeID 
	
END  
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAssetIssueImpactCosts] TO [TEUser]
    AS [dbo];

