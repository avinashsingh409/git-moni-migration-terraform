﻿CREATE PROCEDURE [Diagnostics].[spUpsertCategoryNotification]
	@CategoryNotificationID int = NULL,
	@CategoryNotificationTypeID int ,
	@AssetIssueCategoryTypeID int ,
	@SourceAssetIssueActivityStatus int = NULL,
	@TargetAssetIssueActivityStatus int = NULL,
	@SourceAssetIssueResolutionStatus int = NULL,
	@TargetAssetIssueResolutionStatus int = NULL,
	@EmailSubscribers bit = 0,
	@EmailRecipients nvarchar(MAX),
	@EmailSubject nvarchar(max) = NULL,
	@EmailContent nvarchar(max) = NULL,
	@ModifyAttribute bit = 0,
	@AttributeName nvarchar(255) = NULL,
	@AttributeValue nvarchar(max) = NULL,
	@AddDiscussionEntry bit = 0,
	@EntryTitle nvarchar(255) = NULL,
	@EntryContent nvarchar(max) = NULL,
	@GenerateIssue bit = 0,
	@AssetIssueClass int = NULL,
	@AssetIssueTitle nvarchar(255) = NULL,
	@AssetIssueCategory int = NULL,
	@AssetIssueType int = NULL,
	@AssetIssueSummary nvarchar(max) = NULL,
	@AssetIssuePriority nvarchar(255) = NULL,
	@AssetIssueStatusID int = NULL,
	@AssetIssueResolutionStatusID int = NULL,
	@AssetIssueResolveBy int = NULL,
	@ActionPlanAssignedToEmail nvarchar(255) = NULL,
	@AssetIssueUserAssignedAction nvarchar(max) = NULL,
	@ActionPlanActionDate int = NULL,
	@UpdateCashflow bit = 0,
	@CashflowType nvarchar(255) = NULL,
	@CashflowValue float = NULL,
	@ModifyKeyTag bit = 0,
	@KeyTagOnIssue bit = NULL,
	@KeyTagAction nvarchar(40) = NULL,
	@KeyTag nvarchar(255) = NULL,
	@AddActionPlan bit = 0,
	@SekoiaMapChange bit = NULL,
	@SekoiaMapName nvarchar(255) = NULL,
	@AssetClassTypeID int = NULL,
	@SourceSekoiaMapValue float = NULL,
	@TargetSekoiaMapValue float = NULL,
	@AppliesToManualChange bit = NULL,
	@AppliesToImportChange bit = NULL,
	@AutoChangeActivityStatus bit = 0,
	@AutoChangeActivityStatusTarget int = NULL,
	@AutoChangeResolutionStatus bit = 0,
	@AutoChangeResolutionStatusTarget int = NULL,
	@MasterActionCNID int = NULL,
	@ActionName nvarchar(255) = NULL,
	@OwningResolutionStatusID int = NULL,
	@OwningActivityStatusID int = NULL,
	@OverrideEmail bit = 0,
	@DelayHours INT = NULL,
	@DelayGuard NVARCHAR(255) = NULL,
	@CancelOnActivityStatusChange INT = NULL,
	@CancelOnResolutionStatusChange INT = NULL,
	@NewCategoryNotificationID int OUTPUT
    AS
	BEGIN
		SET @NewCategoryNotificationID = -1

		--Make sure that the convention of treating a -1 as no value is respected
		IF @CategoryNotificationID IS NOT NULL AND @CategoryNotificationID <= 0
		BEGIN
			SET @CategoryNotificationID = NULL;
		END


		IF @CategoryNotificationID IS NULL
		BEGIN
			--INSERT CATEGORY NOTIFICATION
			DECLARE @newIDs base.tpintlist;

			INSERT INTO [Diagnostics].[tCategoryNotification]
			   ([CategoryNotificationTypeID]
			   ,[AssetIssueCategoryTypeID]
			   ,[SourceAssetIssueActivityStatus]
			   ,[TargetAssetIssueActivityStatus]
			   ,[SourceAssetIssueResolutionStatus]
			   ,[TargetAssetIssueResolutionStatus]
			   ,[EmailSubscribers]
			   ,[EmailRecipients]
			   ,[EmailSubject]
			   ,[EmailContent]
			   ,[ModifyAttribute]
			   ,[AttributeName]
			   ,[AttributeValue]
			   ,[AddDiscussionEntry]
			   ,[EntryTitle]
			   ,[EntryContent]
			   ,[GenerateIssue]
			   ,[AssetIssueClass]
			   ,[AssetIssueTitle]
			   ,[AssetIssueCategory]
			   ,[AssetIssueType]
			   ,[AssetIssueSummary]
			   ,[AssetIssuePriority]
			   ,[AssetIssueStatusID]
			   ,[AssetIssueResolutionStatusID]
			   ,[AssetIssueResolveBy]
			   ,[ActionPlanAssignedToEmail]
			   ,[AssetIssueUserAssignedAction]
			   ,[ActionPlanActionDate]
			   ,[UpdateCashflow]
			   ,[CashflowType]
			   ,[CashflowValue]
			   ,[ModifyKeyTag]
			   ,[KeyTagOnIssue]
			   ,[KeyTagAction]
			   ,[KeyTag]
			   ,[AddActionPlan]
			   ,[SekoiaMapChange]
			   ,[SekoiaMapName]
			   ,[AssetClassTypeID]
			   ,[SourceSekoiaMapValue]
			   ,[TargetSekoiaMapValue]
			   ,[AppliesToManualChange]
			   ,[AppliesToImportChange]
			   ,[AutoChangeActivityStatus]
			   ,[AutoChangeActivityStatusTarget]
			   ,[AutoChangeResolutionStatus]
			   ,[AutoChangeResolutionStatusTarget]
			   ,[MasterActionCNID]
			   ,[ActionName]
			   ,[OwningResolutionStatusID]
			   ,[OwningActivityStatusID]
			   ,[OverrideEmail]
			   ,[DelayHours]
			   ,[DelayGuard]
			   ,[CancelOnActivityStatusChange]
			   ,[CancelOnResolutionStatusChange]
			   )
				OUTPUT INSERTED.CategoryNotificationID
				INTO @newIDs
			 VALUES
			   (@CategoryNotificationTypeID,
				@AssetIssueCategoryTypeID,
				@SourceAssetIssueActivityStatus,
				@TargetAssetIssueActivityStatus,
				@SourceAssetIssueResolutionStatus,
				@TargetAssetIssueResolutionStatus,
				@EmailSubscribers,
				@EmailRecipients,
				@EmailSubject,
				@EmailContent,
				@ModifyAttribute,
				@AttributeName,
				@AttributeValue,
				@AddDiscussionEntry,
				@EntryTitle,
				@EntryContent,
				@GenerateIssue,
				@AssetIssueClass,
				@AssetIssueTitle,
				@AssetIssueCategory,
				@AssetIssueType,
				@AssetIssueSummary,
				@AssetIssuePriority,
				@AssetIssueStatusID,
				@AssetIssueResolutionStatusID,
				@AssetIssueResolveBy,
				@ActionPlanAssignedToEmail,
				@AssetIssueUserAssignedAction,
				@ActionPlanActionDate,
				@UpdateCashflow,
				@CashflowType,
				@CashflowValue,
				@ModifyKeyTag,
				@KeyTagOnIssue,
				@KeyTagAction,
				@KeyTag,
				@AddActionPlan,
				@SekoiaMapChange,
				@SekoiaMapName,
				@AssetClassTypeID,
				@SourceSekoiaMapValue,
				@TargetSekoiaMapValue,
				@AppliesToManualChange,
				@AppliesToImportChange,
				@AutoChangeActivityStatus,
				@AutoChangeActivityStatusTarget,
				@AutoChangeResolutionStatus,
				@AutoChangeResolutionStatusTarget,
				@MasterActionCNID,
				@ActionName,
				@OwningResolutionStatusID,
				@OwningActivityStatusID,
				@OverrideEmail,
				@DelayHours,
				@DelayGuard,
			    @CancelOnActivityStatusChange,
			    @CancelOnResolutionStatusChange)

			select @NewCategoryNotificationID = id
			from @newIDs;

		END
		ELSE
		BEGIN
			--UPDATE CATEGORY NOTIFICATION
			set @NewCategoryNotificationID = @CategoryNotificationID;

			UPDATE [Diagnostics].[tCategoryNotification]
			   SET [CategoryNotificationTypeID] = @CategoryNotificationTypeID
				  ,[AssetIssueCategoryTypeID] = @AssetIssueCategoryTypeID
				  ,[SourceAssetIssueActivityStatus] = @SourceAssetIssueActivityStatus
				  ,[TargetAssetIssueActivityStatus] = @TargetAssetIssueActivityStatus
				  ,[SourceAssetIssueResolutionStatus] = @SourceAssetIssueResolutionStatus
				  ,[TargetAssetIssueResolutionStatus] = @TargetAssetIssueResolutionStatus
				  ,[EmailSubscribers] = @EmailSubscribers
				  ,[EmailRecipients] = @EmailRecipients
				  ,[EmailSubject] = @EmailSubject
				  ,[EmailContent] = @EmailContent
				  ,[ModifyAttribute] = @ModifyAttribute
				  ,[AttributeName] = @AttributeName
				  ,[AttributeValue] = @AttributeValue
				  ,[AddDiscussionEntry] = @AddDiscussionEntry
				  ,[EntryTitle] = @EntryTitle
				  ,[EntryContent] = @EntryContent
				  ,[GenerateIssue] = @GenerateIssue
				  ,[AssetIssueClass] = @AssetIssueClass
				  ,[AssetIssueTitle] = @AssetIssueTitle
				  ,[AssetIssueCategory] = @AssetIssueCategory
				  ,[AssetIssueType] = @AssetIssueType
				  ,[AssetIssueSummary] = @AssetIssueSummary
				  ,[AssetIssuePriority] = @AssetIssuePriority
				  ,[AssetIssueStatusID] = @AssetIssueStatusID
				  ,[AssetIssueResolutionStatusID] = @AssetIssueResolutionStatusID
				  ,[AssetIssueResolveBy] = @AssetIssueResolveBy
				  ,[ActionPlanAssignedToEmail] = @ActionPlanAssignedToEmail
				  ,[AssetIssueUserAssignedAction] = @AssetIssueUserAssignedAction
				  ,[ActionPlanActionDate] = @ActionPlanActionDate
				  ,[UpdateCashflow] = @UpdateCashflow
				  ,[CashflowType] = @CashflowType
				  ,[CashflowValue] = @CashflowValue
				  ,[ModifyKeyTag] = @ModifyKeyTag
				  ,[KeyTagOnIssue] = @KeyTagOnIssue
				  ,[KeyTagAction] = @KeyTagAction
				  ,[KeyTag] = @KeyTag
				  ,[AddActionPlan] = @AddActionPlan
				  ,[SekoiaMapChange] = @SekoiaMapChange
				  ,[SekoiaMapName] = @SekoiaMapName
				  ,[AssetClassTypeID] = @AssetClassTypeID
				  ,[SourceSekoiaMapValue] = @SourceSekoiaMapValue
				  ,[TargetSekoiaMapValue] = @TargetSekoiaMapValue
				  ,[AppliesToManualChange] = @AppliesToManualChange
				  ,[AppliesToImportChange] = @AppliesToImportChange
				  ,[AutoChangeActivityStatus] = @AutoChangeActivityStatus
				  ,[AutoChangeActivityStatusTarget] = @AutoChangeActivityStatusTarget
				  ,[AutoChangeResolutionStatus] = @AutoChangeResolutionStatus
				  ,[AutoChangeResolutionStatusTarget] = @AutoChangeResolutionStatusTarget
				  ,[MasterActionCNID] = @MasterActionCNID
				  ,[ActionName] = @ActionName
				  ,[OwningResolutionStatusID] = @OwningResolutionStatusID
				  ,[OwningActivityStatusID] = @OwningActivityStatusID
				  ,[OverrideEmail] = @OverrideEmail
				  ,[DelayHours] = @DelayHours
				  ,[DelayGuard] = @DelayGuard
				  ,[CancelOnActivityStatusChange] = @CancelOnActivityStatusChange
				  ,[CancelOnResolutionStatusChange] = @CancelOnResolutionStatusChange
			 WHERE [CategoryNotificationID] = @NewCategoryNotificationID
		END

		--this isn't really necessary if the caller pays attention to the output parameter appropriately,
		--but if they don't and expect to read in results with a DataReader, this should make sure they
		--can still get the new ID out of the sproc.
		SELECT @NewCategoryNotificationID;
	END
GO
GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spUpsertCategoryNotification] TO [TEUser]
    AS [dbo];

