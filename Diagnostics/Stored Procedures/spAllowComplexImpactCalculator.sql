CREATE PROC [Diagnostics].[spAllowComplexImpactCalculator]
	@issueClassTypeID AS INT,
	@impactTypeIDs AS Base.tpIntList READONLY
AS
BEGIN 
    -- Valid impact types for Complex Calculator
    Declare @validImpactTypeIDs As Base.tpIntList
    Insert Into @validImpactTypeIDs Values
    -- AssetIssueImpactTypeID  AssetIssueImpactType             AssetIssueImpactCatetoryTypeID
         (2)  --               Generation Impact - Reliability       6
        ,(3)  --               Generation Impact - Derate            6
        ,(1)  --               Efficiency Impact (Heat Rate)         1
        ,(9)  --               Maintenance Cost Impact               7
        ,(10) --               Other Costs                           5

    -- This will generate and insert valid mapping records into the tCategoryAssetIssueImpactTypeMap table
    -- It handles invalid and null parameters by not generating any valid records to insert
    Insert Into Diagnostics.tCategoryAssetIssueImpactTypeMap
    Select a.AssetIssueCategoryTypeID, b.id as AssetIssueImpactTypeID
        From Diagnostics.tAssetIssueCategoryType a
    Cross Apply @impactTypeIDs b                                   -- Impact types specified by user
    Join @validImpactTypeIDs c on b.id = c.id                      -- Exclude any invalid impact types specified by user
    Left Outer Join Diagnostics.tCategoryAssetIssueImpactTypeMap d -- Identify all potential mapping records
        on a.AssetIssueCategoryTypeID = d.AssetIssueCategoryTypeID
        and b.id = d.AssetIssueImpactTypeID
    Where IssueClassTypeID = @issueClassTypeID                     -- Issue class type specified by user
        and d.CategoryAssetIssueImpactTypeMapID is null            -- Exclude any mapping records that already exist
    Return @@ROWCOUNT
END
GO

GRANT EXECUTE
    ON OBJECT::[Diagnostics].[spAllowComplexImpactCalculator] TO [TEUser]
    AS [dbo];
GO