﻿CREATE TABLE [ETL].[SmartDGMasterInputs] (
    [MasterInput] NVARCHAR (255) NOT NULL,
    [Value]       NVARCHAR (255) NULL,
    [Notes]       NVARCHAR (255) NULL,
    CONSTRAINT [UX_Master_Inputs] UNIQUE NONCLUSTERED ([MasterInput] ASC)
);

