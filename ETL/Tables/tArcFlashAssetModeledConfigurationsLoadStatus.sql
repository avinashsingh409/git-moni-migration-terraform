﻿CREATE TABLE [ETL].[tArcFlashAssetModeledConfigurationsLoadStatus] (
    [ArcFlashAssetModeledConfigurationsLoadStatusID] INT           IDENTITY (1, 1) NOT NULL,
    [ExternalSystemID]                               INT           NOT NULL,
    [SourceAssetID]                                  NVARCHAR (55) NULL,
    [AssetID]                                        INT           NULL,
    [Success]                                        BIT           NULL,
    [Status]                                         VARCHAR (255) NULL,
    [DateProcessed]                                  DATETIME      NULL,
    CONSTRAINT [PK_tArcFlashAssetModeledConfigurationsLoadStatus] PRIMARY KEY CLUSTERED ([ArcFlashAssetModeledConfigurationsLoadStatusID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ETL', @level1type = N'TABLE', @level1name = N'tArcFlashAssetModeledConfigurationsLoadStatus';

