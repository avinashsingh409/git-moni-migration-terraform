﻿CREATE TABLE [ETL].[SmartDGMasterInputs_ModuleType] (
    [ModuleTypeID]   INT           NOT NULL,
    [ModuleTypeDesc] NVARCHAR (63) NULL,
    CONSTRAINT [PK_SmartDGMasterInputs_ModuleType] PRIMARY KEY CLUSTERED ([ModuleTypeID] ASC)
);

