﻿CREATE TABLE [ETL].[tCapPriorGeoSpas] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [BatchID]           VARCHAR (50)   NOT NULL,
    [MapID]             INT            NOT NULL,
    [Title]             NVARCHAR (255) NULL,
    [GeoSpaDesc]        NVARCHAR (255) NULL,
    [MapTypeID]         INT            NOT NULL,
    [BaseMapID]         INT            NOT NULL,
    [IsPublic]          BIT            NOT NULL,
    [DisplayOrder]      INT            NULL,
    [GeoSpaKey]         NVARCHAR (30)  NOT NULL,
    [InitialExtent]     NVARCHAR (255) NULL,
    [ParentGeoSpaID]    INT            NULL,
    [MapConfig]         NVARCHAR (MAX) NULL,
    [ProjectNumber]     INT            NULL,
    [TimeBaseID]        INT            NULL,
    [AggregationTypeID] INT            NULL,
    [Decimate]          BIT            DEFAULT ((0)) NOT NULL,
    [Categories]        VARCHAR (MAX)  NULL,
    [DescendantsInherit] BIT           DEFAULT ((1)) NOT NULL, 
    CONSTRAINT [PK_GeoSpas_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
);





