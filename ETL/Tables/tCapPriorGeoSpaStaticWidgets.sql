﻿CREATE TABLE [ETL].[tCapPriorGeoSpaStaticRasterWidgets](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchID]          VARCHAR (50)   NOT NULL,
	[StaticRasterLayerID] [int] NOT NULL,
	[StaticRasterGroupID] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[WidgetTypeID] [int] NOT NULL,
	[RelativeXPosition] [real] NOT NULL,
	[RelativeYPosition] [real] NOT NULL,
	[AssetID] [int] NULL,
	[AssetVariableTypeTagMapID] [int] NULL,
 CONSTRAINT [PK_GeoSpaStaticRasterWidgets_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
);

