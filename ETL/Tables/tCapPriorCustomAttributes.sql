﻿CREATE TABLE [ETL].[tCapPriorCustomAttributes] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [BatchID]       VARCHAR (50)   NOT NULL,
    [ProjectNumber] INT            NULL,
    [Name]          NVARCHAR (255) NULL,
    [Value]         NVARCHAR (MAX) NULL,
    [DisplayOrder] INT NULL,
    [Favorite] BIT NULL,
    [Attribute_date] DATETIMEOFFSET(7) NULL,
    CONSTRAINT [PK_ETL_tCapPriorCustomAttributes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO

CREATE INDEX [IDX_BatchID_Includes] ON [ETL].[tCapPriorCustomAttributes] ([BatchID])  INCLUDE ([Name]) WITH (FILLFACTOR=100);
GO