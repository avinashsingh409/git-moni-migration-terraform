﻿CREATE TABLE [ETL].[tCapPriorGeoSpaLayers] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [BatchID]          VARCHAR (50)   NOT NULL,
    [AssetClassTypeID] INT            NOT NULL,
    [MapID]            INT            NOT NULL,
    [Title]            NVARCHAR (255) NULL,
    [GeoSpaLayerDesc]  NVARCHAR (255) NULL,
    [Geometry]         INT            NOT NULL,
    [Symbology]        NVARCHAR (MAX) NULL,
    [DisplayZoomMin] INT NULL, 
    [DisplayZoomMax] INT NULL, 
    CONSTRAINT [PK_GeoSpaLayers_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
);

