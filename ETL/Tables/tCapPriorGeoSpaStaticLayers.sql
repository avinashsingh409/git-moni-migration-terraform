﻿CREATE TABLE [ETL].[tCapPriorGeoSpaStaticRasterLayers] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
	[BatchID]          VARCHAR (50)   NOT NULL,
	[MapID]            INT            NOT NULL,
	[StaticRasterLayerID]            INT            NOT NULL,
    [RasterContentID]  [varchar](50)           NOT NULL,
   
    CONSTRAINT [PK_GeoSpaStaticRasterLayers_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
);

