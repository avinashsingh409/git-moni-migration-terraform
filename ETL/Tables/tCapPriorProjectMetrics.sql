﻿CREATE TABLE [ETL].[tCapPriorProjectMetrics] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [BatchID]         VARCHAR (50)  NOT NULL,
    [Category]        VARCHAR (255) NULL,
    [UnitOfMeasure]   VARCHAR (50)  NULL,
    [ProjectNumber]   INT           NULL,
    [ProjectName]     VARCHAR (255) NULL,
    [ProjectType]     INT           NULL,
    [LongDescription] VARCHAR (MAX) NULL,
    [Benefits]        VARCHAR (MAX) NULL,
    [Highlights]      VARCHAR (MAX) NULL,
    [IsCost]          BIT           NOT NULL,
    [IsRevenue]       BIT           NOT NULL,
    [IsBudgetImpact]  BIT           NOT NULL,
    [Year]            INT           NULL,
    [Value]           FLOAT (53)    NULL,
    [Status]          INT           NULL,
    [Date]            DATETIME      NULL,
    [IsBudgetVsActual] BIT NULL, 
    CONSTRAINT [PK_ProjectMetrics_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
);



