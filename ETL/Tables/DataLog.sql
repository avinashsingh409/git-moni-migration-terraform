﻿CREATE TABLE [ETL].[DataLog] (
    [DataID]        INT            NOT NULL,
    [FileName]      NVARCHAR (255) NULL,
    [MaintSw]       BIT            NOT NULL,
    [TimeCollected] DATETIME       NULL,
    [Eng]           NVARCHAR (31)  NULL,
    [PSRev]         NVARCHAR (31)  NULL,
    [STDCase]       NVARCHAR (28)  NULL,
    [Date]          NVARCHAR (15)  NULL,
    [SN]            NVARCHAR (15)  NULL,
    [FileN]         NVARCHAR (31)  NULL,
    [Config]        NVARCHAR (28)  NULL,
    [Revision]      NVARCHAR (28)  NULL,
    [ErrorLog]      NVARCHAR (MAX) NULL,
    [CautionLog]    NVARCHAR (MAX) NULL,
    [EQtxt]         NVARCHAR (255) NULL,
    [Project]       NVARCHAR (31)  NULL,
    [Loc]           NVARCHAR (31)  NULL,
    [Contr]         NVARCHAR (31)  NULL,
    [1st]           NVARCHAR (130) NULL,
    [2nd]           NVARCHAR (130) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_DataLog_DataID]
    ON [ETL].[DataLog]([DataID] ASC);

