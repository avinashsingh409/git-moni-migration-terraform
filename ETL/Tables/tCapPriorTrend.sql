﻿CREATE TABLE [ETL].[tCapPriorTrend] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [BatchID]             VARCHAR (50)   NOT NULL,
    [TrendID]             INT            NOT NULL,
    [Title]               NVARCHAR (255) NOT NULL,
    [ProjectNumber]       INT            NOT NULL,
    [DisplayOrder]        INT            NOT NULL,
    [TrendDesc]           NVARCHAR (255) NOT NULL,
    [LegendVisible]       BIT            CONSTRAINT [DF_ETLtCapPriorTrend_LegendVisible] DEFAULT ((0)) NOT NULL,
    [LegendDock]          TINYINT        CONSTRAINT [DF_ETLtCapPriorTrend_LegendLocation] DEFAULT ((0)) NOT NULL,
    [LegendContentLayout] TINYINT        CONSTRAINT [DF_ETLtCapPriorTrend_LegendContentLayout] DEFAULT ((0)) NOT NULL,
    [IsCustom]            BIT            CONSTRAINT [DF_ETLtCapPriorTrend_IsCustom] DEFAULT ((0)) NOT NULL,
    [IsPublic]            BIT            CONSTRAINT [DF_ETLtCapPriorTrend_IsPublic] DEFAULT ((1)) NOT NULL,
    [ChartTypeID]         INT            NULL,
    [FilterMin]           FLOAT (53)     NULL,
    [FilterMax]           FLOAT (53)     NULL,
    [FlatlineThreshold]   FLOAT (53)     NULL,
    [Exclude]             NVARCHAR (512) NULL,
    [PinTypeID]           INT            DEFAULT ((1)) NOT NULL,
    [TrendSource]         NVARCHAR (255) NULL,
    [ProjectId]           NVARCHAR (MAX) NULL,
    [Path]                NVARCHAR (MAX) NULL,
    [Math]                NVARCHAR (MAX) NULL,
    [SummaryTypeID]       INT            NULL,
    CONSTRAINT [PK_ETLtCapPriorTrend] PRIMARY KEY CLUSTERED ([ID] ASC)
);



