﻿CREATE TABLE [ETL].[ChangeLog] (
    [EquipmentID]      NVARCHAR (30)  NULL,
    [UserEmail]        NVARCHAR (55)  NULL,
    [DateTime]         NVARCHAR (40)  NULL,
    [Status]           NVARCHAR (55)  NULL,
    [Summary]          NVARCHAR (MAX) NULL,
    [PreModeled]       BIT            NOT NULL,
    [LogID]            INT            NULL,
    [Approver]         NVARCHAR (55)  NULL,
    [Approved]         BIT            NOT NULL,
    [DateApprd]        NVARCHAR (40)  NULL,
    [Modeler]          NVARCHAR (55)  NULL,
    [Modeled]          BIT            NOT NULL,
    [DateMdld]         NVARCHAR (40)  NULL,
    [PreModGrpID]      NVARCHAR (250) NULL,
    [PreModGrpDn]      BIT            NOT NULL,
    [Deleted]          BIT            NOT NULL,
    [ChangeLogID]      INT            NULL,
    [Priority]         NVARCHAR (35)  NULL,
    [IssueType]        NVARCHAR (1)   NULL,
    [Activity]         NVARCHAR (MAX) NULL,
    [Title]            NVARCHAR (50)  NULL,
    [ExternalSystemID] INT            NULL
);




GO
CREATE NONCLUSTERED INDEX [idx_ChangeLog_ChangeLogID]
    ON [ETL].[ChangeLog]([ChangeLogID] ASC);

