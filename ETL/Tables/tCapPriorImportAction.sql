﻿CREATE TABLE [ETL].[tCapPriorImportAction] (
    [ID]                           INT          IDENTITY (1, 1) NOT NULL,
    [BatchID]                      VARCHAR (50) NOT NULL,
    [Assets]                       BIT          CONSTRAINT [DF_tCapPriorImportAction_Assets] DEFAULT ((0)) NOT NULL,
    [Metrics]                      BIT          CONSTRAINT [DF_tCapPriorImportAction_Metrics] DEFAULT ((0)) NOT NULL,
    [DeleteMetrics]                BIT          CONSTRAINT [DF_tCapPriorImportAction_DeleteMetrics] DEFAULT ((0)) NOT NULL,
    [Maps]                         BIT          CONSTRAINT [DF_tCapPriorImportAction_Maps] DEFAULT ((0)) NOT NULL,
    [DeleteMaps]                   BIT          CONSTRAINT [DF_tCapPriorImportAction_DeleteMaps] DEFAULT ((0)) NOT NULL,
    [MapLayers]                    BIT          CONSTRAINT [DF_tCapPriorImportAction_MapLayers] DEFAULT ((0)) NOT NULL,
    [DeleteMapLayers]              BIT          CONSTRAINT [DF_tCapPriorImportAction_DeleteMapLayers] DEFAULT ((0)) NOT NULL,
    [MapStaticRasterLayers]        BIT          CONSTRAINT [DF_tCapPriorImportAction_MapStaticRasterLayers] DEFAULT ((0)) NOT NULL,
    [DeleteMapStaticRasterLayers]  BIT          CONSTRAINT [DF_tCapPriorImportAction_DeleteMapStaticRasterLayers] DEFAULT ((0)) NOT NULL,
    [MapStaticRasterWidgets]       BIT          CONSTRAINT [DF_tCapPriorImportAction_MapStaticRasterWidgets] DEFAULT ((0)) NOT NULL,
    [DeleteMapStaticRasterWidgets] BIT          CONSTRAINT [DF_tCapPriorImportAction_DeleteMapStaticRasterWidgets] DEFAULT ((0)) NOT NULL,
    [Trends]                       BIT          CONSTRAINT [DF_tCapPriorImportAction_Trends] DEFAULT ((0)) NOT NULL,
    [TrendSeries]                  BIT          CONSTRAINT [DF_tCapPriorImportAction_TrendSeries] DEFAULT ((0)) NOT NULL,
    [Historian]					   BIT			CONSTRAINT [DF_tCapPriorImportAction_Historian] DEFAULT ((0)) NOT NULL,
    [DeleteHistorian]			   BIT			CONSTRAINT [DF_tCapPriorImportAction_DeleteHistorian] DEFAULT ((0)) NOT NULL,
    [Tags]						   BIT			CONSTRAINT [DF_tCapPriorImportAction_Tags] DEFAULT ((0)) NOT NULL,
	[Attachments]                  BIT          CONSTRAINT [DF_tCapPriorImportAction_Attachments] DEFAULT ((0)) NOT NULL,
	[OPMTags]					   BIT			CONSTRAINT [DF_tCapPriorImportAction_OpmTags] DEFAULT ((0)) NOT NULL,
	[AppendAssetAttributes]		   BIT			CONSTRAINT [DF_tCapPriorImportAction_AppendAssetAttributes] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tCapPriorImportAction] PRIMARY KEY CLUSTERED ([ID] ASC),
);



