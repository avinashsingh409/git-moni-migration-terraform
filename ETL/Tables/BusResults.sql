﻿CREATE TABLE [ETL].[BusResults] (
    [BusKey]          INT           NULL,
    [MaintSw]         BIT           NOT NULL,
    [WDistance]       REAL          NULL,
    [IDBus]           NVARCHAR (28) NULL,
    [XFact]           REAL          NULL,
    [CGap]            REAL          NULL,
    [MaintSwID]       NVARCHAR (50) NULL,
    [Flag]            BIT           NOT NULL,
    [BusKeyNoPara]    INT           NULL,
    [BusKeyNoTie]     INT           NULL,
    [RecBusKey]       INT           NULL,
    [RecBusKeyNoPara] INT           NULL,
    [RecBusKeyNoTie]  INT           NULL,
    [SCFlag]          BIT           NOT NULL,
    [SCDataID]        INT           NULL,
    [SCFlagNoPara]    BIT           NOT NULL,
    [SCDataIDNoPara]  INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_BusResults_BusKey]
    ON [ETL].[BusResults]([BusKey] ASC);

