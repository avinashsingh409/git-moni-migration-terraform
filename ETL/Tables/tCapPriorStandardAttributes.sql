﻿CREATE TABLE [ETL].[tCapPriorStandardAttributes]
(
    [ID] INT IDENTITY (1, 1) NOT NULL,
    [BatchID] VARCHAR(50) NOT NULL,
    [AssetID] INT NULL,
    [AssetAttributeTypeID] INT NOT NULL,
    [Attribute_int] INT NULL,
    [Attribute_string] NVARCHAR(MAX) NULL,
    [Attribute_float] REAL NULL,
    [Favorite] BIT NOT NULL,
    [DisplayOrder] INT NOT NULL,
    CONSTRAINT [PK_ETL_tCapPriorStandardAttributes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO

CREATE INDEX [IDX_BatchID] ON [ETL].[tCapPriorStandardAttributes] ([BatchID])  WITH (FILLFACTOR=100);

GO
