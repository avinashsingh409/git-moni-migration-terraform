﻿CREATE TABLE [ETL].[tCapPriorHistorian]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
    [BatchID] NVARCHAR(50) NOT NULL, 
    [AssetID] INT NOT NULL, 
    [TagID] INT NOT NULL, 
    [DateTime] DATETIME NOT NULL, 
    [Value] REAL NOT NULL, 
    [Status] INT NOT NULL,
	CONSTRAINT [PK_ETLtCapPriorHistorian] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [UK_ETLtCapPriorHistorian_AssetID_TagID_DateTime] UNIQUE NONCLUSTERED ([BatchID] ASC, [AssetID] ASC, [TagID] ASC, [DateTime] ASC)
)
