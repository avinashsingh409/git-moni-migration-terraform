﻿CREATE TABLE [ETL].[SmartDGSites] (
    [SiteID]              NVARCHAR (100) NOT NULL,
    [Latitude]            FLOAT (53)     NOT NULL,
    [Longitude]           FLOAT (53)     NOT NULL,
    [SiteName]            NVARCHAR (100) NULL,
    [BuildingAreaSqFt]    NVARCHAR (50)  NULL,
    [SectorTypeID]        NVARCHAR (50)  NOT NULL,
    [WeatherFile]         NVARCHAR (150) NOT NULL,
    [CustomerRateClassID] NVARCHAR (150) NOT NULL,
    [LoadProfile]         NVARCHAR (200) NOT NULL,
    [CustomerDemographic] NVARCHAR (100) NULL,
    [CustomerGroup]       INT            NULL,
    [AnnualConsumption]   INT            CONSTRAINT [DF_SmartDGSites_AnnualConsumption] DEFAULT ((0)) NOT NULL,
    [GreenScore]          NVARCHAR (100) NULL,
    [Greenergy1]          NVARCHAR (50)  NULL,
    [Greenergy2]          NVARCHAR (50)  NULL,
    [Greenergy6]          NVARCHAR (50)  NULL,
    [PaybackGroup]        NVARCHAR (50)  NULL,
    [AdoptionGroup]       NVARCHAR (50)  NULL,
    [ExistingPVInstalled] INT            NULL,
    [ExistingPVkW]        REAL           NULL,
    CONSTRAINT [UK_SiteID] UNIQUE NONCLUSTERED ([SiteID] ASC)
);


GO
