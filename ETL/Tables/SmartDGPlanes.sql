﻿CREATE TABLE [ETL].[SmartDGPlanes] (
    [PlaneID]               NVARCHAR (100) NOT NULL,
    [SiteID]                NVARCHAR (100) NOT NULL,
    [PVAzimuth]             REAL           NOT NULL,
    [PVTilt]                REAL           NOT NULL,
    [PVShadingFactors]      NVARCHAR (100) NOT NULL,
    [PVSystemCapacity]      REAL           NOT NULL,
    [PVGroundCoverageRatio] REAL           NULL,
    CONSTRAINT [UX_SiteAndPlane] UNIQUE NONCLUSTERED ([PlaneID] ASC, [SiteID] ASC)
);

