﻿CREATE TABLE [ETL].[tArcFlashAssetLoadStatus] (
    [ArcFlashLoadStatusID] INT           IDENTITY (1, 1) NOT NULL,
    [ExternalSystemID]     INT           NOT NULL,
    [SourceAssetID]        VARCHAR (55)  NOT NULL,
    [SourceParentAssetID]  VARCHAR (55)  NULL,
    [AssetID]              INT           NULL,
    [ParentAssetID]        INT           NULL,
    [Success]              BIT           NULL,
    [Status]               VARCHAR (255) NULL,
    [DateProcessed]        DATETIME      NULL,
    CONSTRAINT [PK_tArcFlashAssetLoadStatus] PRIMARY KEY CLUSTERED ([ArcFlashLoadStatusID] ASC)
);



