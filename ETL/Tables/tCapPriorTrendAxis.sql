﻿CREATE TABLE [ETL].[tCapPriorTrendAxis] (
    [ID]        INT            IDENTITY (1, 1) NOT NULL,
    [BatchID]   VARCHAR (50)   NOT NULL,
    [Axis]      INT            NOT NULL,
    [TrendID]   INT            NOT NULL,
    [Title]     NVARCHAR (255) NULL,
    [Position]  TINYINT        CONSTRAINT [DF_ETLtCapPriorTrendAxis_Location] DEFAULT ((0)) NOT NULL,
    [Min]       FLOAT (53)     NULL,
    [Max]       FLOAT (53)     NULL,
    [Step]      FLOAT (53)     NULL,
    [MinorStep] FLOAT (53)     NULL,
    [IsDefault] BIT            CONSTRAINT [DF_ETLtCapPriorTrendAxis_IsDefault] DEFAULT ((0)) NOT NULL,
    [GridLine]  BIT            NULL,
    CONSTRAINT [PK_ETLtCapPriorTrendAxis] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UK_ETLtCapPriorTrendAxis_BatchID_TrendID_Axis] UNIQUE NONCLUSTERED ([BatchID] ASC, [TrendID] ASC, [Axis] ASC)
);



