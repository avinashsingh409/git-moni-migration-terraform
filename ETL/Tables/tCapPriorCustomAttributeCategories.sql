﻿CREATE TABLE [ETL].[tCapPriorCustomAttributeCategories]
(
	[ID] INT IDENTITY (1, 1) NOT NULL,
	[BatchID] VARCHAR (50) NOT NULL,
	[AttributeName] VARCHAR (255) NULL,
	[Categories] VARCHAR (MAX) NULL,
	CONSTRAINT [PK_ETL_tCapPriorCustomAttributeCategories] PRIMARY KEY CLUSTERED ([ID] ASC)
);
