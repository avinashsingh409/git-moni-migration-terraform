﻿CREATE TABLE [ETL].[PDResultList] (
    [PDKey]              INT           NULL,
    [ID]                 NVARCHAR (28) NULL,
    [BusID]              NVARCHAR (28) NULL,
    [MaintSw]            BIT           NOT NULL,
    [WDistance]          REAL          NULL,
    [XFact]              REAL          NULL,
    [CGap]               REAL          NULL,
    [MaintSwID]          NVARCHAR (50) NULL,
    [Exception]          NVARCHAR (5)  NULL,
    [Flag]               BIT           NOT NULL,
    [PDKeyNoPara]        INT           NULL,
    [PDKeyNoTie]         INT           NULL,
    [RecPDKey]           INT           NULL,
    [RecPDKeyNoPara]     INT           NULL,
    [RecPDKeyNoTie]      INT           NULL,
    [ExceptionNoPara]    NVARCHAR (5)  NULL,
    [ExceptionNoTie]     NVARCHAR (5)  NULL,
    [ExceptionRec]       NVARCHAR (5)  NULL,
    [ExceptionRecNoPara] NVARCHAR (5)  NULL,
    [ExceptionRecNoTie]  NVARCHAR (5)  NULL,
    [SCIDataID]          INT           NULL,
    [SCIFlagNoPara]      BIT           NOT NULL,
    [SCIDataIDNoPara]    INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_PDResultList_PDKey]
    ON [ETL].[PDResultList]([PDKey] ASC);

