﻿CREATE TABLE [ETL].[tCapPriorProject] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [BatchID]             VARCHAR (50)  NOT NULL,
    [ProjectNumber]       INT           NULL,
    [ParentProjectNumber] INT           NULL,
    [ProjectName]         VARCHAR (255) NULL,
    [ProjectType]         INT           NULL,
    [Preferred]           VARCHAR (10)  NULL,
    [AttributesJSON]      VARCHAR (MAX) NULL,
    [DisplayOrder] INT NULL,
    CONSTRAINT [PK_Project_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IDX_CapPriorProject_BatchID]
    ON [ETL].[tCapPriorProject]([BatchID] ASC)
    INCLUDE([ProjectNumber], [ParentProjectNumber], [ProjectName], [ProjectType]);

