﻿CREATE TABLE [ETL].[tCapPriorTag]
(
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [BatchID]             VARCHAR (50)   NOT NULL,
    [TagID]				  INT            NOT NULL,
    [TagDesc]             NVARCHAR(400)            NOT NULL,
	[AssetID]			  INT            NOT NULL,
	[VariableTypeID]	  INT            NULL,
	[ValueTypeID]		  INT            NULL,
	[UnitOfMeasure]		  varchar(50)    NULL,
	[ExistingPDTagMap]	  INT			 NULL,
	CONSTRAINT [PK_ETLtCapPriorTag] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [UK_ETLtCapPriorTag_TagID_TagDesc] UNIQUE NONCLUSTERED ([BatchID] ASC,[TagID] ASC, [TagDesc] ASC, [ValueTypeID] ASC),
	CONSTRAINT [UK_ETLtCapPriorTag_AssetID_TagDesc] UNIQUE NONCLUSTERED ([BatchID] ASC,[AssetID] ASC, [TagDesc] ASC, [ValueTypeID] ASC)
)
