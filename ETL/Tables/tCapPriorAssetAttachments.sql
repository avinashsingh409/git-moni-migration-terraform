﻿CREATE TABLE [ETL].[tCapPriorAssetAttachments]
(
    [ID] INT IDENTITY (1, 1) NOT NULL,
	[BatchID] VARCHAR(50) NOT NULL,
    [ProjectNumber] INT NULL,
    [Title] VARCHAR(255) NULL,
    [Caption] VARCHAR(MAX) NULL,
    [DisplayOrder] INT NULL,
    [Favorite] BIT NULL,
    [Tags] VARCHAR(MAX) NULL,
    [Categories] VARCHAR(MAX) NULL,
    [UpdatingGuid] VARCHAR(50) NULL,
    [ImportedFileGuid] VARCHAR(50) NULL,
	[ImportedFileFormat] VARCHAR(255) NULL,
	[ImportedFileRepositoryName] VARCHAR(255) NULL,
	[AttachmentTypeID] INT NULL,
    CONSTRAINT [PK_AssetAttachment_ETL] PRIMARY KEY CLUSTERED ([ID] ASC)
)
