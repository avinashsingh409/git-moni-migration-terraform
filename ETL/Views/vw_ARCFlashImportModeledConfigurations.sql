﻿------------------------------------------------------------------------------------------------------
CREATE VIEW [ETL].[vw_ARCFlashImportModeledConfigurations] (ExternalSystemID, SourceID, AssetID, WorstAF_ProtectiveDevice, WorstAF_ProtectiveDeviceAssetID, WorstAF_ProtectiveBoundary, WorstAF_HazardCategory, WorstAF_IncidentEnergy, WorstAF_WorkingDistance, WorstAF_HazardBoundary, WorstAF_TimeCollected,
NoParallel_ProtectiveDevice, NoParallel_ProtectiveDeviceAssetID, NoParallel_ProtectiveBoundary, NoParallel_HazardCategory,  NoParallel_IncidentEnergy,  NoParallel_WorkingDistance, NoParallel_HazardBoundary, NoParallel_TimeCollected,
NoTie_ProtectiveDevice,NoTie_ProtectiveDeviceAssetID, NoTie_ProtectiveBoundary, NoTie_HazardCategory, NoTie_IncidentEnergy,  NoTie_WorkingDistance, NoTie_HazardBoundary, NoTie_TimeCollected,
RecWorstAF_ProtectiveDevice,RecWorstAF_ProtectiveDeviceAssetID, RecWorstAF_ProtectiveBoundary, RecWorstAF_HazardCategory,  RecWorstAF_IncidentEnergy, RecWorstAF_WorkingDistance, RecWorstAF_HazardBoundary, RecWorstAF_TimeCollected,
RecNoParallel_ProtectiveDevice, RecNoParallel_ProtectiveDeviceAssetID,  RecNoParallel_ProtectiveBoundary, RecNoParallel_HazardCategory, RecNoParallel_IncidentEnergy, RecNoParallel_WorkingDistance, RecNoParallel_HazardBoundary, RecNoParallel_TimeCollected,
RecNoTie_ProtectiveDevice,RecNoTie_ProtectiveDeviceAssetID, RecNoTie_ProtectiveBoundary, RecNoTie_HazardCategory, RecNoTie_IncidentEnergy, RecNoTie_WorkingDistance, RecNoTie_HazardBoundary, RecNoTie_TimeCollected)
AS
Select DISTINCT m.ExternalSystemID, e.ID, m.assetid, af.FCTPD as WorstAF_ProtectiveDevice, map1.AssetID as WorstAF_ProtectiveDeviceAssetID, af.PBoundary as WorstAF_ProtectiveBoundary, af.RCategory as WorstAF_HazardCategory, af.IEnergy as WorstAF_IncidentEnergy, af.WDistance as WorstAF_WorkingDistance, CEILING(af.PBoundary*1.2) as WorstAF_HazardBoundary, dl1.TimeCollected as WorstAF_TimeCollected,
af2.FCTPD as NoParallel_ProtectiveDevice, map2.AssetID as NoParallel_ProtectiveDeviceAssetID, af2.PBoundary as NoParallel_ProtectiveBoundary, af2.RCategory as NoParallel_HazardCategory, af2.IEnergy as NoParallel_IncidentEnergy, af2.WDistance as NoParallel_IncEngDistance, CEILING(af2.PBoundary*1.2) as NoParallel_HazardBoundary, dl2.TimeCollected as NoParallel_TimeCollected,
af3.FCTPD as NoTie_ProtectiveDevice,map3.AssetID as NoTie_ProtectiveDeviceAssetID, af3.PBoundary as NoTie_ProtectiveBoundary, af3.RCategory as NoTie_HazardCategory, af3.IEnergy as NoTie_IncidentEnergy, af3.WDistance as NoTie_WorkingDistance, CEILING(af3.PBoundary*1.2) as NoTie_HazardBoundary, dl3.TimeCollected as NoTie_TimeCollected,
af4.FCTPD as RecWorstAF_ProtDev, map4.AssetID as RecWorstAF_ProtectiveDeviceAssetID, af4.PBoundary as RecWorstAF_ProtectiveBoundary, af4.RCategory as RecWorstAF_HazardCategory, af4.IEnergy as RecWorstAF_IncidentEnergy, af4.WDistance as RecWorstAF_WorkingDistance, CEILING(af4.PBoundary*1.2) as RecWorstAF_HazardBoundary, dl4.TimeCollected as RecWorstAF_TimeCollected,
af5.FCTPD as RecNoParallel_ProtectiveDevice, map5.AssetID as RecNoParallel_ProtectiveDeviceAssetID, af5.PBoundary as RecNoParallel_Boundary, af5.RCategory as RecNoParallel_Hazard, af5.IEnergy as RecNoParallel_IEnergy, af5.WDistance as RecNoParallel_WorkingDistance,CEILING(af5.PBoundary*1.2) as RecNoParallel_HazardBoundary, dl5.TimeCollected as RecNoParallel_TimeCollected,
af6.FCTPD as RecNoTie_ProtectiveDevice, map6.AssetID as RecNoTie_ProtectiveDeviceAssetID, af6.PBoundary as RecNoTie_ProtectiveBoundary, af6.RCategory as RecNoTie_Hazard, af6.IEnergy as RecNoTie_IncidentEnergy, af6.WDistance as RecNoTie_WorkingDistance,CEILING(af6.PBoundary*1.2) as RecNoTie_HazardBoundary, dl6.TimeCollected as RecNoTie_TimeCollected
FROM ETL.EQDataMaster e
LEFT JOIN Asset.tExternalSystemKeyAssetMap m on m.ExternalSystemKey = e.ID
LEFT JOIN Asset.tAssetAttribute a on a.AssetID = m.AssetID
INNER JOIN Asset.tAttributeType at on a.AttributeTypeID = at.AttributeTypeID
LEFT JOIN ETL.BusResults br ON br.BusKey = e.BusKey
LEFT JOIN ETL.BusArcFlash AF on br.BusKey = af.BusKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map1 on map1.ExternalSystemKey = af.FCTPD
LEFT JOIN ETL.BusArcFlash AF2 on br.BusKeyNoPara = AF2.BusKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map2 on map2.ExternalSystemKey = af2.FCTPD
LEFT JOIN ETL.BusArcFlash Af3 on br.BusKeyNoTie = AF3.BusKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map3 on map3.ExternalSystemKey = af3.FCTPD
LEFT JOIN ETL.BusArcFlash Af4 on br.RecBusKey = AF4.BusKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map4 on map4.ExternalSystemKey = af4.FCTPD
LEFT JOIN ETL.BusArcFlash Af5 on br.RecBusKeyNoPara = AF5.BusKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map5 on map5.ExternalSystemKey = af5.FCTPD
LEFT JOIN ETL.BusArcFlash Af6 on br.RecBusKeyNoTie = AF6.BusKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map6 on map6.ExternalSystemKey = af6.FCTPD
LEFT JOIN ETL.DataLog DL1 on af.DataID = dl1.DataID
LEFT JOIN ETL.DataLog DL2 on af2.DataID = dl2.DataID
LEFT JOIN ETL.DataLog DL3 on af3.DataID = dl3.DataID
LEFT JOIN ETL.DataLog DL4 on af4.DataID = dl4.DataID
LEFT JOIN ETL.DataLog DL5 on af5.DataID = dl5.DataID
LEFT JOIN ETL.DataLog DL6 on af6.DataID = dl6.DataID
WHERE at.AttributeTypeDesc = 'Voltage'
and CAST(a.Attribute_float as float) <= 68
and e.ImportAsset = 1
UNION
Select DISTINCT m.ExternalSystemID, e.ID, m.assetid, af.FCTPD as WorstAF_ProtectiveDevice, map1.AssetID as WorstAF_ProtectiveDeviceAssetID, af.PBoundary as WorstAF_ProtectiveBoundary, af.RCategory as WorstAF_HazardCategory, af.IEnergy as WorstAF_IncidentEnergy, af.WDistance as WorstAF_WorkingDistance, CEILING(af.PBoundary*1.2) as WorstAF_HazardBoundary, dl1.TimeCollected as WorstAF_TimeCollected,
af2.FCTPD as NoParallel_ProtectiveDevice, map2.AssetID as NoParallel_ProtectiveDeviceAssetID, af2.PBoundary as NoParallel_ProtectiveBoundary, af2.RCategory as NoParallel_HazardCategory, af2.IEnergy as NoParallel_IncidentEnergy, af2.WDistance as NoParallel_IncEngDistance, CEILING(af2.PBoundary*1.2) as NoParallel_HazardBoundary, dl2.TimeCollected as NoParallel_TimeCollected,
af3.FCTPD as NoTie_ProtectiveDevice,map3.AssetID as NoTie_ProtectiveDeviceAssetID, af3.PBoundary as NoTie_ProtectiveBoundary, af3.RCategory as NoTie_HazardCategory, af3.IEnergy as NoTie_IncidentEnergy, af3.WDistance as NoTie_WorkingDistance, CEILING(af3.PBoundary*1.2) as NoTie_HazardBoundary, dl3.TimeCollected as NoTie_TimeCollected,
af4.FCTPD as RecWorstAF_ProtDev, map4.AssetID as RecWorstAF_ProtectiveDeviceAssetID, af4.PBoundary as RecWorstAF_ProtectiveBoundary, af4.RCategory as RecWorstAF_HazardCategory, af4.IEnergy as RecWorstAF_IncidentEnergy, af4.WDistance as RecWorstAF_WorkingDistance, CEILING(af4.PBoundary*1.2) as RecWorstAF_HazardBoundary, dl4.TimeCollected as RecWorstAF_TimeCollected,
af5.FCTPD as RecNoParallel_ProtectiveDevice, map5.AssetID as RecNoParallel_ProtectiveDeviceAssetID, af5.PBoundary as RecNoParallel_Boundary, af5.RCategory as RecNoParallel_Hazard, af5.IEnergy as RecNoParallel_IEnergy, af5.WDistance as RecNoParallel_WorkingDistance,CEILING(af5.PBoundary*1.2) as RecNoParallel_HazardBoundary, dl5.TimeCollected as RecNoParallel_TimeCollected,
af6.FCTPD as RecNoTie_ProtectiveDevice, map6.AssetID as RecNoTie_ProtectiveDeviceAssetID, af6.PBoundary as RecNoTie_ProtectiveBoundary, af6.RCategory as RecNoTie_Hazard, af6.IEnergy as RecNoTie_IncidentEnergy, af6.WDistance as RecNoTie_WorkingDistance,CEILING(af6.PBoundary*1.2) as RecNoTie_HazardBoundary, dl6.TimeCollected as RecNoTie_TimeCollected
FROM ETL.EQDataMaster e
LEFT JOIN Asset.tExternalSystemKeyAssetMap m on m.ExternalSystemKey = e.ID
LEFT JOIN Asset.tAssetAttribute a on a.AssetID = m.AssetID
INNER JOIN Asset.tAttributeType at on a.AttributeTypeID = at.AttributeTypeID
LEFT JOIN ETL.PDResultList pr ON pr.PDKey = e.PDKey
LEFT JOIN ETL.PDArcFlash af on pr.PDKey = af.PDKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map1 on map1.ExternalSystemKey = af.FCTPD
LEFT JOIN ETL.PDArcFlash AF2 on pr.PDKeyNoPara = AF2.PDKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map2 on map2.ExternalSystemKey = af2.FCTPD
LEFT JOIN ETL.PDArcFlash Af3 on pr.PDKeyNoTie = AF3.PDKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map3 on map3.ExternalSystemKey = af3.FCTPD
LEFT JOIN ETL.PDArcFlash Af4 on pr.RecPDKey = AF4.PDKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map4 on map4.ExternalSystemKey = af4.FCTPD
LEFT JOIN ETL.PDArcFlash Af5 on pr.RecPDKeyNoPara = AF5.PDKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map5 on map5.ExternalSystemKey = af5.FCTPD
LEFT JOIN ETL.PDArcFlash Af6 on pr.RecPDKeyNoTie = AF6.PDKey
LEFT JOIN Asset.tExternalSystemKeyAssetMap map6 on map6.ExternalSystemKey = af6.FCTPD
LEFT JOIN ETL.DataLog DL1 on af.DataID = dl1.DataID
LEFT JOIN ETL.DataLog DL2 on af2.DataID = dl2.DataID
LEFT JOIN ETL.DataLog DL3 on af3.DataID = dl3.DataID
LEFT JOIN ETL.DataLog DL4 on af4.DataID = dl4.DataID
LEFT JOIN ETL.DataLog DL5 on af5.DataID = dl5.DataID
LEFT JOIN ETL.DataLog DL6 on af6.DataID = dl6.DataID
WHERE e.EqType in ('HVCB', 'FUSE','LVCB')
and at.AttributeTypeDesc = 'Voltage'
and CAST(a.Attribute_float as float) <= 68
and e.ImportAsset = 1