﻿-----------------------------------------------------------
CREATE PROCEDURE [ETL].[spArcFlash_AssetImport] 
	-- Add the parameters for the stored procedure here
	@SecurityUserID int,
	@SelectedAssetID int -- the ID of the asset in the heirarchy currently selected by the user.
	AS
DECLARE @SourceAssetDesc varchar(255)
DECLARE @SourceAssetAbbrev varchar(50)
DECLARE @NewAssetID int
DECLARE @ParentAssetDesc varchar(100)
DECLARE @EQType varchar(30)
DECLARE @AssetClassTypeID int
DECLARE @SelectedExtSystemID int
DECLARE @ImportedExtSystemID int
DECLARE @ParentAssetID int
DECLARE @NomlKV float
DECLARE @AttrVolt int
DECLARE @DisplayOrder int
DECLARE @Error int
DECLARE @RowCount int
DECLARE @Success bit
DECLARE @LoadStatus varchar(255) 
DECLARE @ExtSysDesc varchar(40)

DECLARE @AssetAlreadyMapped bit;

Declare @NewAssetIDs Table (AssetID int);

-- Retrieve all of the Type ID's needed for processing
SELECT @SelectedExtSystemID = ExternalSystemID
from Asset.tExternalSystemKeyAssetMap
where AssetID = @SelectedAssetID 

SELECT @AttrVolt = AttributeTypeID
from Asset.tAttributeType
where AttributeTypeKey = 'Voltage'


--create a temp table that brings in assets
--asset class type and voltage attribute
DECLARE @ASSETTEMP TABLE (
                     SourceAssetDesc varchar(50), 
                     SourceAlternateAssetDesc varchar(255),
                     ParentAssetID varchar(100), 
                     EQType varchar(30), 
                     NomlKV float,
                     DisplayOrder int,
					 ExternalSystemID int
                     )                     
-- Insert into the temporary table a list of the records to be updated
-- Only process the Assets NOT previously inserted into database
-- by checking the external mapping table

INSERT INTO @ASSETTEMP (SourceAssetDesc,	SourceAlternateAssetDesc,	ParentAssetID,			EQType,				NomlKV,		DisplayOrder, ExternalSystemID)
  SELECT Distinct		e.EquipmentID,		e.EquipmentDesc,			e.ParentEquipmentID,	e.EquipmentType,	e.NomlKV,	0, e.ExternalSystemID
  FROM ETL.AssetConfigurationsDataMaster e
  --WHERE e.EquipmentID not in -- this checks for assets already mapped
  --     (select ExternalSystemKey
  --      from Asset.tExternalSystemKeyAssetMap
  --      where ExternalSystemID = @ExtSystemID)
  ORDER BY e.EquipmentID ASC

-- Start looping through the records

  DECLARE assetTempCursor CURSOR
  for select * from @ASSETTEMP;
    
  OPEN assetTempCursor; 

  
  FETCH NEXT FROM assetTempCursor 
  INTO 
	@SourceAssetAbbrev , 
	@SourceAssetDesc , 
	@ParentAssetDesc ,
	@EQType ,
	@NomlKV ,
	@DisplayOrder ,
	@ImportedExtSystemID;
	
	set @SourceAssetDesc = RTRIM(@SourceAssetDesc)
    set @SourceAssetAbbrev = RTRIM(@SourceAssetAbbrev)
    set @ParentAssetDesc=RTRIM(@ParentAssetDesc)
  
WHILE @@FETCH_STATUS = 0
BEGIN


--Wrap the transaction so that if any data point fails then
--the entire transaction will be rolled back.  
BEGIN TRANSACTION PROCESSASSET
	BEGIN TRY
		SET @Success = 1
		SET @LoadStatus = NULL
		SET @NewAssetID = NULL
		SET @AssetClassTypeID = NULL
		SET @ParentAssetID = NULL
		SET @LoadStatus = ''
		SET @AssetAlreadyMapped  = 0;
		PRINT 'Working on @AssetID = ' + @SourceAssetAbbrev 
		

		if(@ImportedExtSystemID <> @SelectedExtSystemID)
		BEGIN
			set @LoadStatus = 'Imported external system ID for asset [' + CAST(@ImportedExtSystemID as varchar(10)) + '] differs from that of the selected asset in the heirarchy [' + CAST(@SelectedExtSystemID as varchar(10)) + ']. Cannot import asset.';
			Print @LoadStatus;
			SET @Success = 0
		END
		ELSE IF (SELECT COUNT(*) FROM Asset.tExternalSystemKeyAssetMap where ExternalSystemID = @ImportedExtSystemID AND ExternalSystemKey = @SourceAssetAbbrev) > 0
		BEGIN 
			Print 'Asset already mapped; no new asset inserted.'
			select @NewAssetID = AssetID from asset.tExternalSystemKeyAssetMap where ExternalSystemID = @ImportedExtSystemID AND ExternalSystemKey = @SourceAssetAbbrev
			set @LoadStatus = 'Asset already mapped to ID ' + CAST(@NewAssetID as nvarchar(10)) + '; no new asset inserted. Data retrieved from existing asset.'
			select @ParentAssetID = ParentAssetID, @AssetClassTypeID = AssetClassTypeID
			from Asset.tAsset
			where AssetID = @NewAssetID
				--work done; fetch next record from cursor, then loop.		
		END 
		ELSE
		BEGIN
			
			-- Get AssetClassTypeID for the equipment
			SELECT @AssetClassTypeID = AssetClassTypeID
			from Asset.tAssetClassType 
			where AssetClassTypeKey = @EQType
  
			-- Get parent of the asset
			--**Rewrite this. Look to the external system map, find the asset from that table using the client's external system ID and the equipment ID.
			PRINT 'Working on @ParentAssetID = ' +  @ParentAssetDesc 
			Select @ParentAssetID = AssetID
			from Asset.tAsset a
			where a.AssetDesc = RTRIM(@ParentAssetDesc)
  
			--Continue processing if ParentAssetID and AssetClassTypeID is found
			IF (@ParentAssetID IS NOT NULL AND @AssetClassTypeID IS NOT NULL)
			BEGIN
				DELETE FROM @NewAssetIDs;
				print '@NewAssetIDs cleared; attempting asset insertion for ' + @SourceAssetAbbrev;
				INSERT INTO Asset.tAsset (ParentAssetID, AssetAbbrev, AssetDesc, AssetClassTypeID, DisplayOrder)
				output INSERTED.AssetID INTO @NewAssetIDs
				values (@ParentAssetID, 
						@SourceAssetAbbrev,
					CASE WHEN @SourceAssetDesc IS NULL THEN @SourceAssetAbbrev
						WHEN @SourceAssetDesc = '' THEN @SourceAssetAbbrev  
								ELSE @SourceAssetDesc
								END, 
						@AssetClassTypeID, 
					CASE WHEN @DisplayOrder IS NULL THEN 0
						 WHEN @DisplayOrder = '' THEN 0
							  ELSE @DisplayOrder
							  END)
				;
				SELECT top 1 @NewAssetID = AssetID from @NewAssetIDs;
				if (@NewAssetID IS NULL)--asset was not inserted 
				BEGIN
					PRINT 'Could not insert Asset ' +  @SourceAssetAbbrev 
					SET @Success = 0
					SET @LoadStatus = 'Could not insert Asset into Asset.tAsset table'
				END
				ELSE
				BEGIN
					SET @RowCount = 0
					PRINT 'Mapping Asset ' +  @SourceAssetAbbrev 
					INSERT INTO Asset.tExternalSystemKeyAssetMap (AssetID, ExternalSystemKey, ExternalSystemID)
					values (@NewAssetID, @SourceAssetAbbrev, @ImportedExtSystemID)
   
					-- Voltage is required for each asset
					INSERT INTO Asset.tAssetAttribute (AssetID, AttributeTypeID, Attribute_float)
					values (@NewAssetID, @AttrVolt, @NomlKV)
					SET @LoadStatus = 'New asset was created.'
				END --else (mapping asset)

			END --processing asset

			ELSE
			BEGIN
				SET @Success = 0
				IF (@ParentAssetID IS NULL)
				BEGIN
					SET @LoadStatus = 'Failed to find parent Asset ID'
				END
				ELSE
				BEGIN
					SET @LoadStatus = 'Failed to find asset class type'
				END
			END 
		END
	END TRY

	BEGIN CATCH --check for errors
		print 'Exception occurred: '
		SET @Success = 0
		select @LoadStatus = ERROR_MESSAGE();
		print @LoadStatus;
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION PROCESSASSET
		END
	END CATCH

-- Process entire asset if all segments succeed
-- Check for success and commit or rollback transaction
	IF (@Success = 1) AND (@@TRANCOUNT > 0)
	BEGIN
		print 'Succeeded; Committing Transaction PROCESSASSET';
		COMMIT TRANSACTION PROCESSASSET
	END
	ELSE
	BEGIN
		print 'Did not succeed.';
		IF (@@TRANCOUNT > 0)
		BEGIN 
			print 'Rolling back Transaction PROCESSASSET';
			ROLLBACK TRANSACTION PROCESSASSET
		END
	END
    
	INSERT INTO ETL.tArcFlashAssetLoadStatus 
			(ExternalSystemID, SourceAssetID, SourceParentAssetID, 
			AssetID, parentAssetID, Success, 
			Status, DateProcessed)
	values	(@ImportedExtSystemID, @SourceAssetAbbrev, @ParentAssetDesc,
			@NewAssetID, @ParentAssetID, @Success,
			@LoadStatus, GETDATE())


  --work done; fetch next record from cursor.
	FETCH NEXT FROM assetTempCursor 
	INTO 
		@SourceAssetAbbrev , 
		@SourceAssetDesc , 
		@ParentAssetDesc ,
		@EQType ,
		@NomlKV ,
		@DisplayOrder ,
		@ImportedExtSystemID;
  
	set @SourceAssetDesc = RTRIM(@SourceAssetDesc)
	set @SourceAssetAbbrev = RTRIM(@SourceAssetAbbrev)
	set @ParentAssetDesc=RTRIM(@ParentAssetDesc)
END --WHILE loop


CLOSE assetTempCursor;
DEALLOCATE assetTempCursor;
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spArcFlash_AssetImport] TO [TEUser]
    AS [dbo];

