﻿CREATE PROCEDURE [ETL].[spSaveCapPriorImportAction]
	@ID INT = NULL
	, @BatchID VARCHAR(50)
	, @Assets BIT = 0
	, @Metrics BIT = 0
	, @DeleteMetrics BIT = 0
	, @Maps BIT = 0
	, @DeleteMaps BIT = 0
	, @MapLayers BIT = 0
	, @DeleteMapLayers BIT = 0
	, @MapStaticRasterLayers BIT = 0
	, @DeleteMapStaticRasterLayers BIT = 0
	, @MapStaticRasterWidgets BIT = 0
	, @DeleteMapStaticRasterWidgets BIT = 0
	, @Trends BIT = 0
	, @TrendSeries BIT = 0
	, @Historian BIT = 0
	, @DeleteHistorian BIT = 0
	, @Tags BIT = 0
	, @Attachments BIT = 0
	, @OpmTags BIT = 0
	, @AppendAssetAttributes BIT = 0
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT ID FROM ETL.tCapPriorImportAction WHERE ID = @ID)
	BEGIN
		UPDATE ETL.tCapPriorImportAction
		SET
			BatchID = @BatchID
			, Assets = @Assets
			, Metrics = @Metrics
			, DeleteMetrics = @DeleteMetrics
			, Maps = @Maps
			, DeleteMaps = @DeleteMaps
			, MapLayers = @MapLayers
			, DeleteMapLayers = @DeleteMapLayers
			, MapStaticRasterLayers = @MapStaticRasterLayers
			, DeleteMapStaticRasterLayers = @DeleteMapStaticRasterLayers
			, MapStaticRasterWidgets = @MapStaticRasterWidgets
			, DeleteMapStaticRasterWidgets = @DeleteMapStaticRasterWidgets
			, Trends = @Trends
			, TrendSeries = @TrendSeries
			, Historian = @Historian
			, DeleteHistorian = @DeleteHistorian
			, Tags = @Tags
			, Attachments = @Attachments
			, AppendAssetAttributes = @AppendAssetAttributes
			, OPMTags = @OpmTags
		WHERE ETL.tCapPriorImportAction.ID = @ID;
		SET @NewID = @ID;
	END
	ELSE
	BEGIN
		INSERT INTO ETL.tCapPriorImportAction (BatchID, Assets, Metrics, DeleteMetrics, Maps, DeleteMaps, MapLayers, DeleteMapLayers
			, MapStaticRasterLayers, DeleteMapStaticRasterLayers, MapStaticRasterWidgets, DeleteMapStaticRasterWidgets, Trends, TrendSeries
			, Historian, DeleteHistorian, Tags, Attachments,AppendAssetAttributes, OPMTags)
		VALUES (@BatchID, @Assets, @Metrics, @DeleteMetrics, @Maps, @DeleteMaps, @MapLayers, @DeleteMapLayers
			, @MapStaticRasterLayers, @DeleteMapStaticRasterLayers, @MapStaticRasterWidgets, @DeleteMapStaticRasterWidgets, @Trends, @TrendSeries
			, @Historian, @DeleteHistorian, @Tags, @Attachments,@AppendAssetAttributes, @OpmTags);
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END

END

GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spSaveCapPriorImportAction] TO [TEUser]
    AS [dbo];

