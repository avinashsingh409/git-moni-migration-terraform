﻿

--Procedure will process all asset issues from the change log
--This is pure ETL - brought in once
--In case of failure the status table is updated and the
--change log ID will be processed in next run 

CREATE PROCEDURE [ETL].[spArcFlash_AssetIssueLoad] 
	-- Add the parameters for the stored procedure here
	@SecurityUserID int,
	@SelectedAssetID int
AS


DECLARE @SourceAssetID varchar(30)
DECLARE @AssetID int
DECLARE @IssueDateTime datetime
DECLARE @ChangeLogID int
DECLARE @IssueType char(1) 
DECLARE @Priority nvarchar(55)
DECLARE @Title nvarchar(55)
DECLARE @Summary nvarchar(max)
DECLARE @Activity nvarchar (max)
DECLARE @AssetIssueID bigint
DECLARE @AssetIssueActivityID bigint
DECLARE @AssetIssueCategoryAF int
DECLARE @IssueTypeMit int 
DECLARE @IssueTypeLabel int
DECLARE @IssueTypeGeneric int
DECLARE @ErrorMessage nvarchar(250)
DECLARE @Success bit
DECLARE @ETLStatus varchar(255)
DECLARE @SelectedExternalSystemID int 
DECLARE @SelectedExternalSystemDesc nvarchar(255)
DECLARE @ImportedExternalSystemID int
DECLARE @IssueResolutionNotMitigated int
DECLARE @IssueResolutionNotApplicable int
DECLARE @CreatedByUserID int
DECLARE @ChangedByUserID int
Declare @UserEmail nvarchar(55)

DECLARE @CurrentUserEmail varchar(255);
select @CurrentUserEmail = ISNULL(u.Email, u.EmailUnique)
from AccessControl.tUser u
where SecurityUserID = @SecurityUserID  --this will need to be replaced; use the security user ID of the user who is importing the data.


--this must use the one in the data from excel. Check against the external system ID for the selected asset too!that'll need to be passed in.
DECLARE @SelectedAssetExternalSystemID int;
SELECT @SelectedAssetExternalSystemID = ExternalSystemID
from asset.tExternalSystemKeyAssetMap
where AssetID = @SelectedAssetID

SELECT @SelectedExternalSystemID = ExternalSystemID, @SelectedExternalSystemDesc = ExternalSystemDesc 
from asset.tExternalSystem a
where a.ExternalSystemID = @SelectedAssetExternalSystemID;

DECLARE @ISSUETEMP TABLE (
                     ExternalSystemID int NULL,
 	                 SourceAssetID nvarchar(30) NULL, --this is an equipment ID, not an AssetID from asset360
 	                 AssetID int null,
 	                 ChangeLogID int not null,
 	                 IssueType char(1) null,
 	                 Priority nvarchar(55) null,
 	                 Title nvarchar(55) null,
 	                 Summary nvarchar(max) null,
 	                 Activity nvarchar (max) null,
 	                 DateTime DateTime null,
					 UserEmail nvarchar(55),
 	                 CreatedByUserID int
                     )
      
      
INSERT INTO ETL.tArcFlashIssueLoadStatus (ExternalSystemID, ChangeLogID, SourceAssetID, Success, Status, DateProcessed)
SELECT DISTINCT cl.ExternalSystemID, cl.ChangeLogID, cl.EquipmentID, 0, 
'Unprocessed. Verify that the indicated equipment item is inserted in the system, and try again.',
SYSDATETIME()
From ETL.ChangeLog cl
WHERE NOT EXISTS
 (SELECT * from ETL.tArcFlashIssueLoadStatus e
  where e.ChangeLogID = cl.ChangeLogID
    and e.ExternalSystemID = cl.ExternalSystemID)
                       
INSERT INTO @ISSUETEMP (ExternalSystemID, SourceAssetID, AssetID, ChangeLogID,
                        IssueType, Priority, Title, Summary,
                        Activity, DateTime, UserEmail, CreatedByUserID)
     Select cl.ExternalSystemID, cl.EquipmentID, e.AssetID,cl.ChangeLogID,
              cl.IssueType,cl.Priority, cl.Title, cl.Summary, 
              cl.Activity, cl.DateTime, cl.UserEmail, @SecurityUserID
        From ETL.ChangeLog cl --this is the imported data. 
        LEFT JOIN Asset.tExternalSystemKeyAssetMap e on RTRIM(cl.EquipmentID) = e.ExternalSystemKey
        WHERE cl.ChangeLogID in 
        (select ChangeLogID
         from ETL.tArcFlashIssueLoadStatus
         where (success is NULL or success = 0) 
           and cl.ExternalSystemID = e.ExternalSystemID)
        ORDER BY cl.ChangeLogID, EquipmentID, DateTime

        --these will probably need to stay as they are.
SELECT @AssetIssueCategoryAF = AssetIssueCategoryTypeID
from Diagnostics.tAssetIssueCategoryType
where CategoryAbbrev = 'Arc Flash'

SELECT @IssueTypeMit = IssueTypeID
from Diagnostics.tIssueType
where IssueTypeDesc = 'Arc Flash Recommended Actions'

SELECT @IssueTypeLabel = IssueTypeID
from Diagnostics.tIssueType 
where IssueTypeDesc = 'Arc Flash Label'


SELECT @IssueTypeGeneric = IssueTypeID
from Diagnostics.tIssueType 
where IssueTypeDesc = 'Generic Arc Flash'

SELECT @IssueResolutionNotMitigated = AssetIssueResolutionStatusTypeID 
from Diagnostics.tAssetIssueResolutionStatusType
where AssetIssueResolutionStatusTypeDesc = 'Not Mitigated'

SELECT @IssueResolutionNotApplicable = AssetIssueResolutionStatusTypeID
from Diagnostics.tAssetIssueResolutionStatusType
where AssetIssueResolutionStatusTypeDesc = 'Not Applicable'
        

-- Start looping through the records

  DECLARE issuesTempCursor CURSOR
  for select 
	SourceAssetID,
	AssetID, 
	ChangeLogID,
	[DateTime],
	IssueType,
	[Priority],
	Title,
	Summary,
	Activity,
	UserEmail,
	CreatedByUserID,
	ExternalSystemID
  from @ISSUETEMP;
    
  OPEN issuesTempCursor; 

  
  FETCH NEXT FROM issuesTempCursor 
  INTO 
	@SourceAssetID,
	@AssetID,
	@ChangeLogID,
	@IssueDateTime,
	@IssueType,
	@Priority,
	@Title,
	@Summary,
	@Activity,
	@UserEmail,
    @CreatedByUserID,
    @ImportedExternalSystemID
  
WHILE @@FETCH_STATUS = 0
BEGIN
BEGIN TRANSACTION ProcessAssetIssue
 BEGIN TRY
  SET @Success = 1
  SET @ETLStatus = NULL
  SET @AssetIssueID = NULL
  SET @ErrorMessage = NULL
  SET @AssetIssueID = NULL
  SET @AssetIssueActivityID = NULL

  IF @UserEmail IS NULL OR @UserEmail = '' SET @UserEmail = @CurrentUserEmail

  PRINT 'Working on @AssetID = ' + CAST(@SourceAssetID as varchar(100))
  IF (@ImportedExternalSystemID <> @SelectedExternalSystemID)
  BEGIN
      PRINT 'Imported external system ID does not match selected external system ID.'
      SET @Success = 0
      SET @ETLStatus = 'Imported external system ID (' + CAST(@ImportedExternalSystemID as varchar(10)) + 
	  ') does not match selected external system ID (' + CAST(@SelectedExternalSystemID as varchar(10)) + '.'
  END
  ELSE IF (@AssetID IS NULL)
    BEGIN
      PRINT 'AssetID is not found in SII database'
      SET @Success = 0
      SET @ETLStatus = 'AssetID is not mapped in database'
    END
   ELSE
    BEGIN
      INSERT INTO Diagnostics.tAssetIssue (AssetID, IssueTypeID, ResolutionStatusID, AssetIssueCategoryTypeID, Priority,ActivityStatusID,  IssueTitle, IssueSummary, CreatedBy, ChangedBy, CreateDate, ChangeDate, CreatedByUserID)
       values (@AssetID, 
                  CASE @IssueType
                       WHEN 'G' THEN @IssueTypeGeneric
                       WHEN 'L' THEN @issueTypeLabel
                       WHEN 'M' THEN @IssueTypeMit
                       end,
                  CASE @IssueType 
                       WHEN 'G' THEN @IssueResolutionNotApplicable
                       WHEN 'L' THEN @IssueResolutionNotApplicable
                       WHEN 'M' THEN @IssueResolutionNotMitigated
                       end,
                  @AssetIssueCategoryAF,			--get user emails from the import file, if they're in there.      v this one is the importer's own ID.`
                  @Priority, 1, @Title,  @Summary, @UserEmail, @UserEmail, @IssueDateTime, @IssueDateTime, @CreatedByUserID)

        SET @AssetIssueID = SCOPE_IDENTITY()
        IF (@AssetIssueID is not null) AND (@Activity IS NOT NULL)
         BEGIN
           INSERT INTO Diagnostics.tAssetIssueActionPlan (AssetIssueID, ActivityNumber, ResponsibleParty, ActivityDate, Activity, CreatedBy, ChangedBy)
           values (@AssetIssueID, 1, @SelectedExternalSystemDesc, @IssueDateTime, @Activity, @UserEmail, @UserEmail)--as above, use from file if present
           SET @AssetIssueActivityID = SCOPE_IDENTITY()
         END
        IF (@AssetIssueID IS NULL)
         BEGIN
          SET @Success = 0
          SET @ETLStatus = 'Asset issue ID could not be created '
         END
    END
 END TRY
 BEGIN CATCH 
   ROLLBACK Transaction ProcessAssetIssue
   SELECT @ETLStatus = Cast (Error_Message() as varchar(250))
   SET @Success = 0

 END CATCH
     
IF (@Success = 1) AND (@@TRANCOUNT > 0)
  BEGIN
    COMMIT TRANSACTION ProcessAssetIssue
  END
 ELSE
  BEGIN
   IF (@@TRANCOUNT > 0)
    BEGIN 
     ROLLBACK TRANSACTION ProcessAssetIssue
    END
  END
   
   
   UPDATE ETL.tArcFlashIssueLoadStatus 
   Set success = @Success, Status =@ETLStatus, AssetID = @AssetID, DateProcessed = GETDATE(),
   AssetIssueID = @AssetIssueID, AssetIssueActionPlanID = @AssetIssueActivityID
   where ChangeLogID = @ChangeLogID and
         ExternalSystemID = @ImportedExternalSystemID
  

  FETCH NEXT FROM issuesTempCursor 
  INTO 
	@SourceAssetID,
	@AssetID,
	@ChangeLogID,
	@IssueDateTime,
	@IssueType,
	@Priority,
	@Title,
	@Summary,
	@Activity,
	@UserEmail,
    @CreatedByUserID,
    @ImportedExternalSystemID
  
     
 END --while loop
 
  close issuesTempCursor; 
  deallocate issuesTempCursor;
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spArcFlash_AssetIssueLoad] TO [TEUser]
    AS [dbo];

