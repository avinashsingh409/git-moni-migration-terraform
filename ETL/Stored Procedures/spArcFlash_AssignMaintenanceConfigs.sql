﻿

-- =============================================
-- Author:		Ryan Irwin
-- Create date: 2017-11-20
-- Description:	Find missing asset modeled configs
--  with null MaintenanceConfigIDs, and assign the
--  appropriate IDs to those fields. For now, that's
--  just the worst-case maintenance mode for each 
--  asset.
-- =============================================
CREATE PROCEDURE [ETL].[spArcFlash_AssignMaintenanceConfigs]
	-- Add the parameters for the stored procedure here
	@SecurityUserID int,
	@SelectedAssetID int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @affectedAssets TABLE (ID int);

	Insert into @affectedAssets
	select distinct a.AssetID
	from arcFlash.tAssetModeledConfigurations a
	inner join Asset.ufnGetAssetDescendants (@SelectedAssetID, 99, 0) b on a.assetID = b.assetID
	where a.MaintenanceConfigurationID is null;
	
	declare @temp int;
	select @temp = COUNT(*) from @affectedAssets;
	print convert(nvarchar(10), @temp) + ' affected assets.'

	DECLARE affectedAssetsCursor CURSOR
	for select ID From @affectedAssets;

	DECLARE @assetID int;
	DECLARE @fetchStatus int;
	DECLARE @maintConfigID int;

	Open affectedAssetsCursor;
	FETCH NEXT FROM affectedAssetsCursor into @assetID;
	SET @fetchStatus = @@FETCH_STATUS;
	while @fetchStatus = 0
	BEGIN
		print 'Processing asset ID ' + convert(nvarchar(10), @assetID);

		select @temp = COUNT(*) from ArcFlash.tAssetModeledConfigurations
		WHERE AssetID = @assetID
		AND MaintenanceConfigurationID is null;
		print 'Found ' + convert(nvarchar(10),@temp) + ' configs for that asset with no maintenance config ID.'
		
		--now, find the worst-case maintenance configuration ID

		select top 1 @maintConfigID = AssetModeledConfigurationTypeID
		from	ArcFlash.tAssetModeledConfigurations
		where	AssetID = @assetID 
		AND	
		(
			ConfigAbbrev like '%Maintenance Mode%' or ConfigAbbrev like '%Maint Mode%'
			or ConfigDesc like '%Maintenance Mode%' or ConfigDesc like '%Maint Mode%'
			or ModelRevision  like '%Maintenance Mode%' or ModelRevision like '%Maint Mode%'
		)
		ORDER BY IncidentEnergy_cal_cm2 DESC;
		print 'Found maintenance config id ' + convert(nvarchar(10), @maintConfigID) + '. Updating configurations...'

		UPDATE arcFlash.tAssetModeledConfigurations
		SET MaintenanceConfigurationID = @maintConfigID,
		    ChangedByUserID = @SecurityUserID,
			ChangeDate = SYSDATETIME()
		WHERE AssetID = @assetID
		AND MaintenanceConfigurationID is null;

		
		select @temp = COUNT(*) from ArcFlash.tAssetModeledConfigurations
		WHERE AssetID = @assetID
		AND MaintenanceConfigurationID is null;
		print convert(nvarchar(10),@temp) + ' configs for that asset still no maintenance config ID.'
		select @temp = COUNT(*) from ArcFlash.tAssetModeledConfigurations
		WHERE AssetID = @assetID
		AND MaintenanceConfigurationID = @maintConfigID;
		print convert(nvarchar(10),@temp) + ' configs for that asset now have maintenance config ID ' + convert(nvarchar(10), @maintConfigID);
		
		--fetch the next asset ID and loop.
		FETCH NEXT FROM affectedAssetsCursor into @assetID;
		SET @fetchStatus = @@FETCH_STATUS;
	END
	CLOSE affectedAssetsCursor;
	DEALLOCATE affectedAssetsCursor;
END
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spArcFlash_AssignMaintenanceConfigs] TO [TEUser]
    AS [dbo];

