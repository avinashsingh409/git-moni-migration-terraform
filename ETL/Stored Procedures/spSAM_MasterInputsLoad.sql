﻿

CREATE PROCEDURE [ETL].[spSAM_MasterInputsLoad]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @InflationRate real,
	@ModuleType int,
	@DCtoACRatio real,
	@RateEscalation real,
	@AnalysisPeriod int,
	@BaseYear int,
	@InverterEfficiency real,
	@Availability real,
	@Degradation real,
	@RemainingSystemLosses real,
	@DoNEMWorkAround bit,
	@CapacityLimitDC real,
	@AdjustConstant real,
	@MinimumSystemCapacity real,
	@MinimumPaybackDelta real,
	@CapacityReductionStepSize real,
	@UseLifetimeOutput int,
	@EnergyCurtailment nvarchar(250),
	@ArrayType int,
	@MasterInputSetDesc nvarchar(255);

	--The Master Inputs field has a unique key constraint, so duplicates won't be allowed.
	--So for any unique Master Input there should be exactly one value. 
	
	SELECT top 1 @InflationRate = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Inflation Rate%';

	SELECT top 1 @ModuleType = b.ModuleTypeID
	FROM [ETL].[SmartDGMasterInputs] a
	INNER JOIN ETL.SmartDGMasterInputs_ModuleType b on a.Value = b.ModuleTypeDesc
	WHERE [MasterInput] like 'Module Type';
	
	SELECT top 1 @DCtoACRatio = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'DC%AC%Ratio%';
	
	SELECT top 1 @RateEscalation = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Rate Escalation%';
	
	SELECT top 1 @AnalysisPeriod = CONVERT(int, CONVERT(real, Value))
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Analysis Period%years%';
	
	SELECT top 1 @BaseYear = CONVERT(int, CONVERT(real, Value))
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Base Year%';

	SELECT top 1 @InverterEfficiency = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Inverter Efficiency%';

	SELECT top 1 @Availability = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Availability%';

	SELECT top 1 @Degradation = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Degradation%';

	SELECT top 1 @RemainingSystemLosses = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Remaining System Losses%';
	
	DECLARE @DoNEMWorkAround_RawValue nvarchar(15);
	SELECT top 1 @DoNEMWorkAround_RawValue = Value
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like '%NEM%Workaround%';
	if(@DoNEMWorkAround_RawValue like 'no' or @DoNEMWorkAround_RawValue like 'false' or @DoNEMWorkAround_RawValue like '0') Set @DoNEMWorkAround = 0;
	else if (@DoNEMWorkAround_RawValue like 'yes' or @DoNEMWorkAround_RawValue like 'true' or @DoNEMWorkAround_RawValue like '1') Set @DoNEMWorkAround = 1;
	
	SELECT top 1 @CapacityLimitDC = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Max%Capacity%';
	
	SELECT top 1 @AdjustConstant = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'adjust%constant%';

	SELECT top 1 @MinimumSystemCapacity = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Min%Capacity%';

	SELECT top 1 @MinimumPaybackDelta = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Min%Payback%';

	SELECT top 1 @CapacityReductionStepSize = CONVERT(real, Value)
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'Capacity reduction step size%';

	SELECT top 1 @UseLifetimeOutput = CONVERT(int, CONVERT(real, Value))
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like '%use%lifetime%output%';

	SELECT top 1 @EnergyCurtailment = Value
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like '%energy%curtailment%';

	SELECT top 1 @ArrayType = CONVERT(int, CONVERT(real, Value))
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like '%array%type%';
	
	SELECT top 1 @MasterInputSetDesc = Value
	FROM [ETL].[SmartDGMasterInputs]
	WHERE [MasterInput] like 'SAMMasterInputSetDesc';


	INSERT INTO [SAM].[tSAMMasterInputSet]
           ([SAMMasterInputSetDesc]
		   ,[InflationRate]
           ,[ModuleType]
           ,[DCtoACRatio]
           ,[RateEscalation]
           ,[AnalysisPeriod]
           ,[BaseYear]
           ,[InverterEfficiency]
           ,[Availability]
           ,[Degradation]
           ,[RemainingSystemLosses]
           ,[DoNEMWorkAround]
           ,[CapacityLimitDC]
           ,[AdjustConstant]
           ,[MinimumSystemCapacity]
           ,[MinimumPaybackDelta]
           ,[CapacityReductionStepSize]
           ,[UseLifetimeOutput]
           ,[EnergyCurtailment]
           ,[ArrayType])
     VALUES
           (@MasterInputSetDesc
		   ,@InflationRate
           ,@ModuleType
           ,@DCtoACRatio
           ,@RateEscalation
           ,@AnalysisPeriod
           ,@BaseYear
           ,@InverterEfficiency
           ,@Availability
           ,@Degradation
           ,@RemainingSystemLosses
           ,@DoNEMWorkAround
           ,@CapacityLimitDC
           ,@AdjustConstant
           ,@MinimumSystemCapacity
           ,@MinimumPaybackDelta
           ,@CapacityReductionStepSize
           ,@UseLifetimeOutput
           ,@EnergyCurtailment
           ,@ArrayType)
		   ;
END
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spSAM_MasterInputsLoad] TO [TEUser]
    AS [dbo];

