﻿--- Loads the modeled configurations for an asset
--- If the asset has no configurations they are inserted into
--- the ArcFlash.tAssetModeledConfigurations table
--- If configurations exist, they are updated and a
--- copy is placed of the old configuration into the
--- ArcFlash.tAssetModeledConfigurationsHistory table

CREATE PROCEDURE [ETL].[spArcFlash_ModeledConfigurationsBuilder] 
	@SecurityUserID int,
	@SelectedAssetID int -- the ID of the asset in the heirarchy currently selected by the user.
AS


DECLARE @ExternalSystemID int ;
DECLARE @EquipmentID nvarchar(MAX) ;
DECLARE @ParentEquipmentID nvarchar(MAX) ;
DECLARE @EquipmentType nvarchar(MAX) ;
DECLARE @NomlkV float ;
DECLARE @DateCompiled datetime ;
DECLARE @EquipmentDesc nvarchar(MAX) ;
DECLARE @ProtectiveDeviceEquipmentName nvarchar(MAX) ;
DECLARE @ProtectiveBoundary_ft float ;
DECLARE @HazardBoundary_ft float ;
DECLARE @HazardCategoryTypeID int ;
DECLARE @IncidentEnergy_cal_cm2 float ;
DECLARE @WorkingDistance_in float ;
DECLARE @BFCurrent float ;
DECLARE @ArcCurrent float ;
DECLARE @PDCurrent float ;
DECLARE @FCT float ;
DECLARE @BusType nvarchar(MAX) ;
DECLARE @GroundingType nvarchar(MAX) ;
DECLARE @CondGap int ;
DECLARE @Xfactor float ;
DECLARE @SCStudyCase nvarchar(MAX) ;
DECLARE @ConfigCase nvarchar(MAX) ;
DECLARE @ModelRevision nvarchar(MAX) ;
DECLARE @ModelName nvarchar(MAX) ;
DECLARE @ConfigAbbrev nvarchar(MAX) ;
DECLARE @ConfigDesc nvarchar(MAX) ;
DECLARE @PDType nvarchar(MAX) ;
DECLARE @IDSCXRratio float ;
DECLARE @IDSCMF float ;
DECLARE @IDSCflag bit ;
DECLARE @IDSymkA float ;
DECLARE @IDAdjSymkA float ;
DECLARE @IDAsymDeg float ;
DECLARE @IDcapSymkA float ;
DECLARE @IDcapAdjSymkA float ;
DECLARE @IDcapkV float ;
DECLARE @MDSCXRratio float ;
DECLARE @MDSCMF float ;
DECLARE @MDSCflag bit ;
DECLARE @MDSymkA float ;
DECLARE @MDAsymkA float ;
DECLARE @MDAsymPeakkA float ;
DECLARE @MDcapSymkA float ;
DECLARE @MDcapAsymkA float ;
DECLARE @MDcapAsymPeakkA float ;
DECLARE @To_BusID nvarchar(MAX) ;
DECLARE @Fm_BusID nvarchar(MAX) ;
DECLARE @Ot_BusID nvarchar(MAX) ;
DECLARE @IsActiveConfiguration bit ;
DECLARE @DateAssigned datetime ;
DECLARE @ModeledBy nvarchar(MAX) ;
DECLARE @Label_DeviceID nvarchar(MAX) ;
DECLARE @Label_Voltage nvarchar(MAX) ;
DECLARE @Label_MaintSwitchStatus nvarchar(MAX) ;
DECLARE @Label_IncidentEnergy nvarchar(MAX) ;
DECLARE @Label_LAB nvarchar(MAX) ;
DECLARE @Label_PAB nvarchar(MAX) ;
DECLARE @Label_RAB nvarchar(MAX) ;
DECLARE @Label_GloveClass nvarchar(MAX) ;
DECLARE @Label_Exception nvarchar(MAX) ;
DECLARE @Label_MaintSwitchID nvarchar(MAX) ;
DECLARE @Label_FPB nvarchar(MAX) ;
DECLARE @Label_ProtDevice nvarchar(MAX) ;
DECLARE @Label_CatDesc nvarchar(MAX) ;
DECLARE @Label_PPEDescription nvarchar(max) ;
DECLARE @Label_BusID nvarchar(MAX) ;
DECLARE @Label_DeviceType nvarchar(MAX) ;
DECLARE @Label_DisclaimerText nvarchar(max) ;
DECLARE @Label_BVText nvarchar(max) ;
DECLARE @Label_AFStudyDate nvarchar(max) ;

DECLARE @AssetID int;
DECLARE @ProtectiveDeviceAssetID int;

DECLARE @Error int
DECLARE @RowCount int

DECLARE @Success bit
DECLARE @LoadStatus varchar(MAX) 
DECLARE @NewData int

--Table variables do not participate in transactions. So we can log all errors to this temporary table within a transaction,
--then after rolling back the transaction, insert from this table to the error recording table in the database. This will 
--tell the user everything that went wrong, while still restoring 
DECLARE @tempReports table
(
	ExternalSystemID int not null,
	SourceAssetID nvarchar(55) null,
	AssetID int null,
	Success bit null,
	Status varchar(MAX) null,
	DateProcessed datetime null
)


DECLARE @SelectedAssetExternalSystemID int;
select @SelectedAssetExternalSystemID = ExternalSystemID 
from asset.tExternalSystemKeyAssetMap
where AssetID = @SelectedAssetID;


declare @rootSelectedAssetID int 
SELECT top 1 @rootSelectedAssetID = a.AssetID from Asset.tAsset a
inner join Asset.tExternalSystemKeyAssetMap b on a.AssetID = b.AssetID
where a.AssetClassTypeID = 2000 and ExternalSystemID = @SelectedAssetExternalSystemID
ORDER BY a.AssetClassTypeID

declare @outerFetchStatus int, @innerFetchStatus int;

declare @currentEquipmentID nvarchar(MAX)
declare @currentExternalSystemID int, @currentAssetID int
declare @rollbackAssetWork bit;
declare assetSpecificCursor CURSOR
FOR SELECT DISTINCT EquipmentID, ExternalSystemID from etl.AssetConfigurationsDataMaster;
open assetSpecificCursor

FETCH NEXT FROM assetSpecificCursor into @currentEquipmentID, @currentExternalSystemID;
SET @outerFetchStatus = @@FETCH_STATUS;
while @outerFetchStatus = 0
BEGIN


  select @currentAssetID = AssetID
  from asset.tExternalSystemKeyAssetMap
  WHERE ExternalSystemID = @currentExternalSystemID
  AND ExternalSystemKey = @currentEquipmentID
  PRINT 'Working on @currentAssetID = ' + CAST(@currentAssetID as varchar(10)) + ' (@currentEquipmentID ' + @currentEquipmentID + 
		', @currentExternalSystemID = ' + CAST(@currentExternalSystemID as varchar(10)) + ')';

 if(SELECT COUNT(*) FROM asset.tExternalSystemKeyAssetMap WHERE ExternalSystemID = @currentExternalSystemID AND ExternalSystemKey = @currentEquipmentID) > 1 
 BEGIN
   SET @LoadStatus = 'More than one asset ID found mapped to external system ID '+ CAST(@currentExternalSystemID as varchar(10)) 
	 + ' and equipment ID "' + @currentEquipmentID + '". Cannot import.'
   INSERT INTO ETL.tArcFlashAssetModeledConfigurationsLoadStatus (ExternalSystemID, AssetID, SourceAssetID, DateProcessed, Status, Success)
   VALUES (@currentExternalSystemID, NULL, @currentEquipmentID, SYSDATETIME(), @LoadStatus, 0)
   FETCH NEXT FROM assetSpecificCursor into @currentEquipmentID, @currentExternalSystemID;
   SET @outerFetchStatus = @@FETCH_STATUS;
   continue;
 END
 else if(SELECT COUNT(*) FROM asset.tExternalSystemKeyAssetMap WHERE AssetID = @currentAssetID) > 1 
 BEGIN
   SET @LoadStatus = 'More than one external system ID and external system key mapped to asset ID '+ CAST(@currentAssetID as varchar(10)) + '. Cannot import.'
   INSERT INTO ETL.tArcFlashAssetModeledConfigurationsLoadStatus (ExternalSystemID, AssetID, SourceAssetID, DateProcessed, Status, Success)
   VALUES (@currentExternalSystemID, NULL, @currentEquipmentID, SYSDATETIME(), @LoadStatus, 0)
   FETCH NEXT FROM assetSpecificCursor into @currentEquipmentID, @currentExternalSystemID;
   SET @outerFetchStatus = @@FETCH_STATUS;
   continue;
 END

 set @rollbackAssetWork = 0;

 BEGIN TRANSACTION AssetWork
 
  --check for existing data in SII database
  --**Thsi has to be rewritten now that we're denormalizing away the current 6-record config type table.
  --**ALSO: delete what's in the ArcFlash modeled configs table for this asset, before inseritng the new configurations from teh imported data.
  --that delet will be for teh asset we're working on in this transaction, and will get rolled back if any part of this asset's import fails.

  UPDATE a SET MaintenanceConfigurationID = NULL 
  FROM ArcFlash.tAssetModeledConfigurations a JOIN ArcFlash.tAssetModeledConfigurations b on 
  a.MaintenanceConfigurationID = b.AssetModeledConfigurationTypeID WHERE b.AssetID = @currentAssetID

  DELETE FROM ArcFlash.tAssetModeledConfigurations
  WHERE AssetID = @currentAssetID;
  
  DECLARE assetConfigDataMasterCursor CURSOR
  for select 
     ExternalSystemID
	,EquipmentID
	,ParentEquipmentID
	,EquipmentType
	,NomlkV
	,DateCompiled
	,EquipmentDesc
	,ProtectiveDeviceEquipmentName
	,ProtectiveBoundary_ft
	,HazardBoundary_ft
	,HazardCategoryTypeID
	,IncidentEnergy_cal_cm2
	,WorkingDistance_in
	,BFCurrent
	,ArcCurrent
	,PDCurrent
	,FCT
	,BusType
	,GroundingType
	,CondGap
	,Xfactor
	,SCStudyCase
	,ConfigCase
	,ModelRevision
	,ModelName
	,ConfigAbbrev
	,ConfigDesc
	,PDType
	,IDSCXRratio
	,IDSCMF
	,IDSCflag
	,IDSymkA
	,IDAdjSymkA
	,IDAsymDeg
	,IDcapSymkA
	,IDcapAdjSymkA
	,IDcapkV
	,MDSCXRratio
	,MDSCMF
	,MDSCflag
	,MDSymkA
	,MDAsymkA
	,MDAsymPeakkA
	,MDcapSymkA
	,MDcapAsymkA
	,MDcapAsymPeakkA
	,To_BusID
	,Fm_BusID
	,Ot_BusID
	,IsActiveConfiguration
	,DateAssigned
	,ModeledBy
	,Label_DeviceID
	,Label_Voltage
	,Label_MaintSwitchStatus
	,Label_IncidentEnergy
	,Label_LAB
	,Label_PAB
	,Label_RAB
	,Label_GloveClass
	,Label_Exception
	,Label_MaintSwitchID
	,Label_FPB
	,Label_ProtDevice
	,Label_CatDesc
	,Label_PPEDescription
	,Label_BusID
	,Label_DeviceType
	,Label_DisclaimerText
	,Label_BVText
	,Label_AFStudyDate
   from ETL.AssetConfigurationsDataMaster
  where EquipmentID = @currentEquipmentID and ExternalSystemID = @currentExternalSystemID
  
  open assetConfigDataMasterCursor
   



-- Start looping through the records 
  FETCH NEXT FROM assetConfigDataMasterCursor 
  INTO 
	@ExternalSystemID
	,@EquipmentID
	,@ParentEquipmentID
	,@EquipmentType
	,@NomlkV
	,@DateCompiled
	,@EquipmentDesc
	,@ProtectiveDeviceEquipmentName
	,@ProtectiveBoundary_ft
	,@HazardBoundary_ft
	,@HazardCategoryTypeID
	,@IncidentEnergy_cal_cm2
	,@WorkingDistance_in
	,@BFCurrent
	,@ArcCurrent
	,@PDCurrent
	,@FCT
	,@BusType
	,@GroundingType
	,@CondGap
	,@Xfactor
	,@SCStudyCase
	,@ConfigCase
	,@ModelRevision
	,@ModelName
	,@ConfigAbbrev
	,@ConfigDesc
	,@PDType
	,@IDSCXRratio
	,@IDSCMF
	,@IDSCflag
	,@IDSymkA
	,@IDAdjSymkA
	,@IDAsymDeg
	,@IDcapSymkA
	,@IDcapAdjSymkA
	,@IDcapkV
	,@MDSCXRratio
	,@MDSCMF
	,@MDSCflag
	,@MDSymkA
	,@MDAsymkA
	,@MDAsymPeakkA
	,@MDcapSymkA
	,@MDcapAsymkA
	,@MDcapAsymPeakkA
	,@To_BusID
	,@Fm_BusID
	,@Ot_BusID
	,@IsActiveConfiguration
	,@DateAssigned
	,@ModeledBy
	,@Label_DeviceID
	,@Label_Voltage
	,@Label_MaintSwitchStatus
	,@Label_IncidentEnergy
	,@Label_LAB
	,@Label_PAB
	,@Label_RAB
	,@Label_GloveClass
	,@Label_Exception
	,@Label_MaintSwitchID
	,@Label_FPB
	,@Label_ProtDevice
	,@Label_CatDesc
	,@Label_PPEDescription
	,@Label_BusID
	,@Label_DeviceType
	,@Label_DisclaimerText
	,@Label_BVText
	,@Label_AFStudyDate

  
SET @innerFetchStatus = @@FETCH_STATUS;
WHILE @innerFetchStatus = 0
BEGIN

 BEGIN TRY
--Wrap the transaction so that if any data point fails then
--the entire transaction will be rolled back.  
  SET @Success = 1
  SET @LoadStatus = NULL
  SET @AssetID = null;
  SET @ProtectiveDeviceAssetID = null;

  if (@ExternalSystemID <> @SelectedAssetExternalSystemID)
  BEGIN
	SET @Success = 0
	SET @LoadStatus = 'Conflicting External System IDs. Selected asset gave ' + CAST(@SelectedAssetExternalSystemID as varchar(10))
	    + ', but import file supplied ' + CAST(@ExternalSystemID as varchar(10)) + '. Please resolve this conflict and try again.';
	SET @AssetID = null;
	SET @rollbackAssetWork = 1;
  END
  ELSE
  BEGIN

    select @AssetID = AssetID
  from asset.tExternalSystemKeyAssetMap
  WHERE ExternalSystemID = @ExternalSystemID
  AND ExternalSystemKey = @EquipmentID
  
    --!!!TODO: Verify that this block works as intended!!!
    select @ProtectiveDeviceAssetID = AssetID
  from asset.tExternalSystemKeyAssetMap
  WHERE ExternalSystemID = @ExternalSystemID
  AND ExternalSystemKey = @ProtectiveDeviceEquipmentName
  
    PRINT 'Working on @AssetID = ' + CAST(@AssetID as varchar(100)) + ' (@EquipmentID ' + @EquipmentID + ')';
   
    --continue processing if AssetID is valid  
    IF (@AssetID IS NULL)
    BEGIN
         SET @Success = 0
         SET @LoadStatus = 'Failed to find Asset ID for equipment ID "' + @EquipmentID + '" and external system ID ' + CAST(@ExternalSystemID as varchar(10)) + '.'
		 SET @rollbackAssetWork = 1;
    END 
	ELSE IF (@AssetID <> @currentAssetID)
	BEGIN
         SET @Success = 0;
         SET @LoadStatus = '@AssetID found to be ' + CAST(@AssetID as varchar(10)) + ' rather than ' + CAST(@currentAssetID as varchar(10)) + ' as expected.';
		 SET @rollbackAssetWork = 1;
	END
	ELSE
    BEGIN   
    
      --Insert Configuration
		INSERT INTO ArcFlash.tAssetModeledConfigurations 
			([AssetID]
           ,[DateCompiled]
           ,[DateAssigned]
           ,[ProtectiveDeviceAssetID]
           ,[ProtectiveBoundary_ft]
           ,[HazardBoundary_ft]
           ,[HazardCategoryTypeID]
           ,[IncidentEnergy_cal_cm2]
           ,[WorkingDistance_in]
           ,[ActiveConfiguration]
           ,[Label_DeviceID]
           ,[Label_Voltage]
           ,[Label_MaintSwitchStatus]
           ,[Label_IncidentEnergy]
           ,[Label_LAB]
           ,[Label_PAB]
           ,[Label_RAB]
           ,[Label_GloveClass]
           ,[Label_Exception]
           ,[Label_MaintSwitchID]
           ,[Label_FPB]
           ,[Label_ProtDevice]
           ,[Label_CatDesc]
           ,[Label_PPEDescription]
           ,[Label_BusID]
           ,[Label_DeviceType]
           ,[Label_DisclaimerText]
           ,[Label_BVText]
           ,[CreateDate]
           ,[ChangeDate]
           ,[CreatedByUserID]
           ,[ChangedByUserID]
           ,[BFCurrent]
           ,[ArcCurrent]
           ,[PDCurrent]
           ,[FCT]
           ,[BusType]
           ,[GroundingType]
           ,[CondGap]
           ,[Xfactor]
           ,[SCStudyCase]
           ,[ConfigCase]
           ,[ModelRevision]
           ,[ModelName]
           ,[ConfigAbbrev]
           ,[ConfigDesc]
           ,[PDType]
           ,[IDSCXRratio]
           ,[IDSCMF]
           ,[IDSCflag]
           ,[IDSymkA]
           ,[IDAdjSymkA]
           ,[IDAsymDeg]
           ,[IDcapSymkA]
           ,[IDcapAdjSymkA]
           ,[IDcapkV]
           ,[MDSCXRratio]
           ,[MDSCMF]
           ,[MDSCflag]
           ,[MDSymkA]
           ,[MDAsymkA]
           ,[MDAsymPeakkA]
           ,[MDcapSymkA]
           ,[MDcapAsymkA]
           ,[MDcapAsymPeakkA]
           ,[To_BusID]
           ,[Fm_BusID]
           ,[Ot_BusID]
           ,[ModeledBy]
           ,[Label_AFStudyDate])
		   values 
			(@AssetID
           ,@DateCompiled
           ,@DateAssigned
           ,@ProtectiveDeviceAssetID
           ,@ProtectiveBoundary_ft
           ,@HazardBoundary_ft
           ,@HazardCategoryTypeID
           ,@IncidentEnergy_cal_cm2
           ,@WorkingDistance_in
           ,@IsActiveConfiguration
           ,@Label_DeviceID
           ,@Label_Voltage
           ,@Label_MaintSwitchStatus
           ,@Label_IncidentEnergy
           ,@Label_LAB
           ,@Label_PAB
           ,@Label_RAB
           ,@Label_GloveClass
           ,@Label_Exception
           ,@Label_MaintSwitchID
           ,@Label_FPB
           ,@Label_ProtDevice
           ,@Label_CatDesc
           ,@Label_PPEDescription
           ,@Label_BusID
           ,@Label_DeviceType
           ,@Label_DisclaimerText
           ,@Label_BVText
           ,sysdatetime()
           ,sysdatetime()
           ,@SecurityUserID
           ,@SecurityUserID
           ,@BFCurrent
           ,@ArcCurrent
           ,@PDCurrent
           ,@FCT
           ,@BusType
           ,@GroundingType
           ,@CondGap
           ,@Xfactor
           ,@SCStudyCase
           ,@ConfigCase
           ,@ModelRevision
           ,@ModelName
           ,@ConfigAbbrev
           ,@ConfigDesc
           ,@PDType
           ,@IDSCXRratio
           ,@IDSCMF
           ,@IDSCflag
           ,@IDSymkA
           ,@IDAdjSymkA
           ,@IDAsymDeg
           ,@IDcapSymkA
           ,@IDcapAdjSymkA
           ,@IDcapkV
           ,@MDSCXRratio
           ,@MDSCMF
           ,@MDSCflag
           ,@MDSymkA
           ,@MDAsymkA
           ,@MDAsymPeakkA
           ,@MDcapSymkA
           ,@MDcapAsymkA
           ,@MDcapAsymPeakkA
           ,@To_BusID
           ,@Fm_BusID
           ,@Ot_BusID
           ,@ModeledBy
           ,@Label_AFStudyDate)
	  		
        SET @LoadStatus = 'Inserted new configuration for asset'
    END-- processing asset configurations
    
  END
END TRY

BEGIN CATCH --check for errors
     SET @Success = 0
     SET @LoadStatus = ERROR_MESSAGE()
	 SET @rollbackAssetWork = 1;
END CATCH

IF (@LoadStatus IS NOT NULL)
 BEGIN
  INSERT INTO @tempReports (ExternalSystemID, AssetID, SourceAssetID, DateProcessed, Status, Success)
  VALUES (@SelectedAssetExternalSystemID, @AssetID, @EquipmentID, GETDATE(), @LoadStatus, @Success)
 END


  --work done; fetch next set before looping.
  FETCH NEXT FROM assetConfigDataMasterCursor 
  INTO 
	@ExternalSystemID
	,@EquipmentID
	,@ParentEquipmentID
	,@EquipmentType
	,@NomlkV
	,@DateCompiled
	,@EquipmentDesc
	,@ProtectiveDeviceEquipmentName
	,@ProtectiveBoundary_ft
	,@HazardBoundary_ft
	,@HazardCategoryTypeID
	,@IncidentEnergy_cal_cm2
	,@WorkingDistance_in
	,@BFCurrent
	,@ArcCurrent
	,@PDCurrent
	,@FCT
	,@BusType
	,@GroundingType
	,@CondGap
	,@Xfactor
	,@SCStudyCase
	,@ConfigCase
	,@ModelRevision
	,@ModelName
	,@ConfigAbbrev
	,@ConfigDesc
	,@PDType
	,@IDSCXRratio
	,@IDSCMF
	,@IDSCflag
	,@IDSymkA
	,@IDAdjSymkA
	,@IDAsymDeg
	,@IDcapSymkA
	,@IDcapAdjSymkA
	,@IDcapkV
	,@MDSCXRratio
	,@MDSCMF
	,@MDSCflag
	,@MDSymkA
	,@MDAsymkA
	,@MDAsymPeakkA
	,@MDcapSymkA
	,@MDcapAsymkA
	,@MDcapAsymPeakkA
	,@To_BusID
	,@Fm_BusID
	,@Ot_BusID
	,@IsActiveConfiguration
	,@DateAssigned
	,@ModeledBy
	,@Label_DeviceID
	,@Label_Voltage
	,@Label_MaintSwitchStatus
	,@Label_IncidentEnergy
	,@Label_LAB
	,@Label_PAB
	,@Label_RAB
	,@Label_GloveClass
	,@Label_Exception
	,@Label_MaintSwitchID
	,@Label_FPB
	,@Label_ProtDevice
	,@Label_CatDesc
	,@Label_PPEDescription
	,@Label_BusID
	,@Label_DeviceType
	,@Label_DisclaimerText
	,@Label_BVText
	,@Label_AFStudyDate
	
SET @innerFetchStatus = @@FETCH_STATUS;
END --inner WHILE loop


 if (@rollbackAssetWork = 1)
 BEGIN
   Rollback Transaction AssetWork
   SET @LoadStatus = 'Error(s) occurred while importing configs for AssetID ' + CAST(@currentAssetID as varchar(10)) +
     ', ExternalSystemID ' + CAST(@currentExternalSystemID as varchar(10)) + ' and EquipmentID "' + @currentEquipmentID + 
	 '". See load status records for details. No changes committed for this asset; reimport after correcting.'
   INSERT INTO ETL.tArcFlashAssetModeledConfigurationsLoadStatus (ExternalSystemID, AssetID, SourceAssetID, DateProcessed, Status, Success)
   VALUES (@currentExternalSystemID, NULL, @currentEquipmentID, SYSDATETIME(), @LoadStatus, 0)
 END
 else
 begin
   COMMIT TRANSACTION AssetWork
 end

close assetConfigDataMasterCursor;
deallocate assetConfigDataMasterCursor;

INSERT INTO ETL.tArcFlashAssetModeledConfigurationsLoadStatus (ExternalSystemID, AssetID, SourceAssetID, DateProcessed, Status, Success)
select ExternalSystemID, AssetID, SourceAssetID, DateProcessed, Status, Success from @tempReports;
delete from @tempReports;

FETCH NEXT FROM assetSpecificCursor into @currentEquipmentID, @currentExternalSystemID;
SET @outerFetchStatus = @@FETCH_STATUS;

END --outer WHILE loop

close assetSpecificCursor;
deallocate assetSpecificCursor;
;
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spArcFlash_ModeledConfigurationsBuilder] TO [TEUser]
    AS [dbo];

