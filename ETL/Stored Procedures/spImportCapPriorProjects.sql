﻿CREATE PROC [ETL].[spImportCapPriorProjects]
	(@parentAssetId int,
	@batchId varchar(50),
	@securityUserID int,
	@errorMsg varchar(255) OUTPUT)
AS

BEGIN

    SET nocount ON
	SET @errorMsg = ''
	DECLARE @returnValue AS int = -1
	DECLARE @foundParent AS int
	SELECT @foundParent = 1
	FROM asset.tAsset
	WHERE AssetID = @parentAssetId

	IF @foundParent IS NULL
      BEGIN
		RAISERROR ('Error - parent asset id not found.', 13, -1, -1)
		RETURN @returnValue
	END

	DECLARE @createdBy AS varchar(255)
	SELECT @createdBy = email
	FROM AccessControl.tUser
	WHERE SecurityUserID = @securityUserID

	IF @createdBy IS NULL
      BEGIN
		RAISERROR ('Error - user id not found.', 13,-1,-1)
		RETURN @returnValue
	END

	IF NOT EXISTS (SELECT BatchID
	FROM ETL.tCapPriorImportAction
	WHERE BatchID = @batchId)
		BEGIN
		RAISERROR('Error - import action not found.', 13, -1, -1)
		RETURN @returnValue
	END

	DECLARE @exAssets base.tpIntList
	INSERT INTO @exAssets
		(id)
	SELECT AssetID
	FROM Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @parentAssetId)

	DECLARE @assets bit, @tags bit, @metrics bit, @deleteMetrics bit, @historian bit, @maps bit, @deleteMaps bit, @layers bit, @deletelayers bit
		, @rasters bit, @deleteRasters bit, @widgets bit, @deleteWidgets bit, @trends bit, @trendSeries bit, @attachments bit, @OPMTags BIT, @AppendAssetAttributes BIT;

	SELECT @assets = Assets, @tags = Tags, @metrics = Metrics, @deleteMetrics = DeleteMetrics, @historian = Historian
		, @maps = Maps, @deleteMaps = DeleteMaps, @layers = MapLayers, @deleteLayers = DeleteMapLayers, @rasters = MapStaticRasterLayers
		, @deleteRasters = DeleteMapStaticRasterLayers, @widgets = MapStaticRasterWidgets
		, @deleteWidgets = MapStaticRasterWidgets, @trends = Trends, @trendSeries = TrendSeries
		, @attachments = Attachments, @OPMTags = OPMTags, @AppendAssetAttributes = AppendAssetAttributes
	FROM ETL.tCapPriorImportAction
	WHERE BatchID = @batchId;

	CREATE TABLE #tmp_capprior_assets (
		[ID]                [int]            IDENTITY(1,1) NOT NULL,
		AssetID             int              NULL,
		ParentAssetID       int              NULL,
		UniqueID            uniqueidentifier NULL,
		ProjectNumber       int              NULL,
		ParentProjectNumber int              NULL,
		ProjectName         varchar(255)     NULL,
		ProjectType         int              NULL,
		AttributesJSON      varchar(max),
		Preferred           bit              NULL,
		DisplayOrder        int              NULL,
		[level]             int              NULL
    );

	CREATE INDEX IDX_1 ON #tmp_capprior_assets(AssetID);
	CREATE INDEX IDX_2 ON #tmp_capprior_assets(ParentAssetID);
	CREATE INDEX IDX_3 ON #tmp_capprior_assets(UniqueID);
	CREATE INDEX IDX_4 ON #tmp_capprior_assets(ProjectNumber);
	CREATE INDEX IDX_5 ON #tmp_capprior_assets(ParentProjectNumber);
	CREATE INDEX IDX_6 ON #tmp_capprior_assets(level);

	CREATE TABLE #tmpTags  (
		[ProjectNumber]  int           NULL,
		[AssetID]        int           NULL,
		[AssetName]      nvarchar(255) NULL,
		[TagID]          int           NULL,
		[PDServerID]     int           NULL,
		[PDTagID]        int           NULL,
		[TagName]        nvarchar(400) NULL,
		[TagDesc]        nvarchar(400) NULL,
		[VariableTypeID] int           NULL,
		[ValueTypeID]    int           NULL,
		[EngUnits]       nvarchar(255) NULL,
		[PDTagMapID]	 int		   NULL
	);


	CREATE INDEX IDX_1 ON #tmpTags(ProjectNumber);
	CREATE INDEX IDX_2 ON #tmpTags(AssetID);
	CREATE INDEX IDX_3 ON #tmpTags(TagName);
	CREATE INDEX IDX_4 ON #tmpTags(ValueTypeID);


	CREATE TABLE #tmpProjectCashFlow (
		[UnitOfMeasure]  nvarchar(255) NOT NULL,
		[AssetID]        int           NOT NULL,
		[Timestamp]      datetime      NOT NULL,
		[Type]           nvarchar(255) NOT NULL,
		[Value]          float         NOT NULL,
		[Status]         int           NOT NULL
	);

	CREATE INDEX IDX_1 ON #tmpProjectCashFlow([AssetID]);
	CREATE INDEX IDX_2 ON #tmpProjectCashFlow([Timestamp]);
	CREATE INDEX IDX_3 ON #tmpProjectCashFlow([Type]);


	DECLARE @tmpTagsExist AS bit = 0

	DECLARE @topNodes TABLE (
		ProjectNumber int          NULL,
		ProjectName   varchar(max),
		AssetID       int          NULL
	);

	DECLARE @latTypeId AS int = 153
	DECLARE @longTypeId AS int = 154
	DECLARE @polylineTypeId AS int = 194
	DECLARE @polygonTypeId AS int = 195

	--ETL.tCapPriorProject.ProjectNumber is the 'assetID' field from the import spreadsheet, per SekoiaImport.cs:624
	INSERT INTO #tmp_capprior_assets
		(UniqueID, ProjectNumber, ParentProjectNumber, ProjectName, ProjectType, AttributesJSON, Preferred, DisplayOrder)
	SELECT newid(), ProjectNumber, ParentProjectNumber,
		CASE WHEN UPPER(ISNULL(Preferred,'')) = 'Y' THEN '* ' ELSE '' END + ProjectName,
		ProjectType,
		REPLACE(AttributesJSON,'\','\\'),
		CASE WHEN UPPER(ISNULL(Preferred,'')) = 'Y' THEN 1 ELSE 0 END AS Preferred,
		DisplayOrder
	FROM ETL.tCapPriorProject
	WHERE BatchID = @batchId AND ProjectNumber IS NOT NULL

	DECLARE @dupProjectName AS varchar(255)
	DECLARE @dupParentProjectNumber AS int
	SELECT TOP 1
		@dupProjectName = ProjectName, @dupParentProjectNumber = ParentProjectNumber
	FROM (SELECT projectname, parentprojectnumber, count(*) AS CNT
		FROM #tmp_capprior_assets
		GROUP BY ProjectName,ParentProjectNumber) a
	WHERE Cnt>1

	IF @dupProjectName IS NOT NULL
	   BEGIN
		DECLARE @dupProjectNum AS int
		SELECT TOP 1
			@dupProjectNum=projectNumber
		FROM #tmp_capprior_assets
		WHERE ParentProjectNumber = @dupParentProjectNumber AND ProjectName = @dupProjectName
		SET @errorMsg = 'Error in excel inputs - all siblings must have unique names. (eg. ' + @dupProjectName + ', Project Number: ' + CONVERT(varchar,@dupProjectNum) + ')'
		RAISERROR (@errorMsg ,13, -1, -1)
		RETURN @returnValue
	END

	DECLARE @dupProjectNumber AS int
	SELECT TOP 1
		@dupProjectNumber = projectNumber
	FROM (SELECT projectNumber, count(*) AS CNT
		FROM #tmp_capprior_assets
		GROUP BY projectNumber) a
	WHERE Cnt>1

	IF @dupProjectNumber IS NOT NULL
	   BEGIN
		SET @errorMsg = 'Error in excel inputs - all project numbers must be unique. (eg. ' + ' Project Number: ' + CONVERT(varchar,@dupProjectNumber) + ')'
		RAISERROR (@errorMsg ,13, -1, -1)
		RETURN @returnValue
	END

	DECLARE @tmpAssetCount int
	SELECT @tmpAssetCount = count(*)
	FROM #tmp_capprior_assets

	DECLARE @assetId AS int

	INSERT INTO @topNodes
		(ProjectNumber, ProjectName)
			SELECT DISTINCT ProjectNumber, projectName
		FROM #tmp_capprior_assets
		WHERE ParentProjectNumber IS NULL

	INSERT INTO @topNodes
		(ProjectNumber, ProjectName)
		SELECT DISTINCT ProjectNumber, projectName
		FROM #tmp_capprior_assets a
		WHERE NOT EXISTS (SELECT ProjectNumber FROM #tmp_capprior_assets b WHERE a.ParentProjectNumber = b.ProjectNumber)

	DECLARE @topNodeCount AS int = 0

	SELECT @topNodeCount = count(*)
	FROM @topNodes
	IF @topNodeCount = 0
      BEGIN
		RAISERROR ('Error in excel inputs - at least one project must have a parent that is blank or not contained in the data set.',13, -1, -1)
		RETURN @returnValue
	END

	UPDATE @topNodes SET AssetID=b.AssetID FROM @topNodes a JOIN Asset.tAsset b ON a.ProjectName = b.AssetDesc WHERE b.ParentAssetID = @ParentAssetID

	DECLARE @cnt AS int
	SELECT @cnt = COUNT(*)
	FROM #tmp_capprior_assets
	WHERE ProjectType NOT IN (SELECT assetclasstypeid
	FROM asset.tAssetClassType)
	IF @cnt>0
	  BEGIN
		RAISERROR ('Error in excel inputs - at least one project type was not valid.',13, -1, -1)
		RETURN @returnValue
	END

	DECLARE @cntWrongPreferred AS int
	DECLARE @parentID AS int
	DECLARE @done AS bit = 0
	DECLARE @level AS int = 0
	DECLARE @topProjectNumber AS int
	DECLARE @topAssetID AS int
	DECLARE @maxlevel AS int = 99 --Set max level to 99 below the sql default limit of 100 for recursive rollback

	SELECT @cnt = COUNT(*)
	FROM #tmp_capprior_assets
	WHERE LEN(projectname)>50

	IF @cnt>0
	  BEGIN
		RAISERROR ( 'Error in excel inputs - at least one project name is greater than 50 characters long.',13, -1, -1)
		RETURN @returnValue
	END

	UPDATE #tmp_capprior_assets SET ParentAssetID = @parentAssetId,level = 0,ParentProjectNumber = -999 WHERE ProjectNumber IN (SELECT ProjectNumber
	FROM @topNodes)

	--Set AssetID in #tmp_capprior_assets to the actual AssetID
	UPDATE a SET a.Assetid = B.assetid FROM #tmp_capprior_assets a JOIN @topNodes b ON a.ProjectNumber = b.ProjectNumber

	WHILE @done=0
		BEGIN
		UPDATE x SET x.assetid = a.assetid,UniqueID = a.globalid FROM #tmp_capprior_assets x JOIN asset.tAsset a ON x.ParentAssetID = a.ParentAssetID WHERE x.[level] = @level AND
			x.ProjectName = a.AssetDesc

		UPDATE x SET x.ParentAssetID = a.AssetID, level = @level + 1 FROM #tmp_capprior_assets x JOIN #tmp_capprior_assets a ON x.ParentProjectNumber = a.ProjectNumber WHERE a.[level] = @level

		SET @level = @level + 1
		SELECT @cnt = COUNT(*)
		FROM #tmp_capprior_assets
		WHERE AssetID IS NULL
		--Loop need to goes @maxlevel + 2 to allow level recorded in #tmp_capprior_assets to exceed @maxlevel
		IF @cnt=0 or @level > @maxlevel + 2
		  BEGIN
			SET @done = 1
		END
	END

	--Check to ensure maximum depth level is not exceeded
	IF EXISTS(SELECT * FROM #tmp_capprior_assets WHERE level >= @maxlevel)
		BEGIN
		RAISERROR ('Error in excel inputs - the asset hierarchy exceeds maximum depth level of 99', 13, -1, -1)
		RETURN @returnValue		    
	END

	BEGIN TRANSACTION

		BEGIN TRY
			SET @done = 0
			SET @level = 0

			WHILE @done=0
				BEGIN
				INSERT INTO Asset.tAsset
					(ParentAssetID,AssetAbbrev,AssetDesc,AssetClassTypeID,DisplayOrder,GlobalID,CreatedBy,ChangedBy)
				SELECT
					ParentAssetID, ProjectName, ProjectName, ProjectType, DisplayOrder, UniqueID, @createdBy, @createdBy
				FROM #tmp_capprior_assets
				WHERE level = @level AND AssetID IS NULL
				UPDATE x SET x.assetid = a.assetid FROM #tmp_capprior_assets x JOIN asset.tAsset a ON x.UniqueID = a.GlobalID WHERE x.[level] = @level AND x.AssetID IS NULL
				UPDATE x SET x.ParentAssetID = a.AssetID, level = @level + 1 FROM #tmp_capprior_assets x JOIN #tmp_capprior_assets a ON x.ParentProjectNumber = a.ProjectNumber WHERE a.[level] = @level
				SET @level = @level + 1
				SELECT @cnt = COUNT(*)
				FROM #tmp_capprior_assets
				WHERE AssetID IS NULL
				IF @cnt=0 or @level > @maxlevel
				  BEGIN
					SET @done = 1
				END
			END
		END TRY
		BEGIN CATCH
			PRINT N'Script Failed, rolling back';
			ROLLBACK TRANSACTION
			PRINT ERROR_MESSAGE();
			SET @errorMsg = ERROR_MESSAGE()
			RAISERROR ( @errorMsg,13, -1, -1)
			RETURN @returnValue
		END CATCH

		BEGIN TRY
			BEGIN TRANSACTION
			  UPDATE a SET a.AssetClassTypeID = x.ProjectType, a.ChangedBy = @createdBy, a.ChangeDate = GETDATE(), a.DisplayOrder = x.DisplayOrder FROM Asset.tAsset a JOIN #tmp_capprior_assets x ON a.AssetID = x.AssetID
			  WHERE a.AssetClassTypeID <> x.ProjectType or a.DisplayOrder<>X.DisplayOrder
			COMMIT TRANSACTION;

		END TRY
		BEGIN CATCH
			PRINT N'Script Failed, rolling back';
			ROLLBACK TRANSACTION
			PRINT ERROR_MESSAGE();
			SET @errorMsg = ERROR_MESSAGE()
			RAISERROR (@errorMsg, 13, -1, -1)
			RETURN @returnValue
		END CATCH

		-- do some checking to see if the user has sufficient permissions to modify the categories they have specified
		DECLARE @customAttributeCategories TABLE (
			AttributeName varchar(255) NULL,
			CategoryID    int          NULL,
			CategoryName  varchar(255) NULL
		);

		INSERT INTO @customAttributeCategories
			(AttributeName, CategoryID, CategoryName)
		SELECT cac.AttributeName, NULL, ufn.string
		FROM
			[ETL].[tCapPriorCustomAttributeCategories] cac
		CROSS APPLY [Base].[ufnSplitString](cac.Categories, ';') ufn
		WHERE cac.BatchID = @batchId;

		--*----------
		DECLARE @mapCategories TABLE (
			Title        varchar(255) NULL,
			MapID        int          NULL,
			CategoryID   int          NULL,
			CategoryName varchar(255) NULL
		);
		INSERT INTO @mapCategories
			(Title, MapID, CategoryID, CategoryName)
		SELECT geo.Title, geo.MapID, NULL, ufn.string
		FROM
			[ETL].[tCapPriorGeoSpas] geo
		CROSS APPLY [Base].[ufnSplitString](geo.Categories, ';') ufn
		WHERE geo.BatchID = @batchId;

		--*----------

		DECLARE @firstImportedAssetID int
		SELECT TOP 1
			@firstImportedAssetID = AssetID
		FROM #tmp_capprior_assets
		DECLARE @categoryAttributeTypeID int
		SELECT TOP 1
			@categoryAttributeTypeID = CategoryTypeID
		FROM Asset.tCategoryType
		WHERE CategoryTypeName LIKE 'Attributes'
		DECLARE @categoryTypeIDs AS Base.tpIntList
		INSERT INTO @categoryTypeIDs
		VALUES
			(@categoryAttributeTypeID)

		-- check and see if user has any categories or attributes they aren't allowed to modify.
		DECLARE @restrictedValues TABLE (
			ID   int          NULL,
			Name varchar(255) NULL
			);

		DECLARE @categoryNameList AS Base.tpStringList
		INSERT INTO @categoryNameList
		SELECT DISTINCT CategoryName
		FROM @customAttributeCategories
		UNION
		SELECT DISTINCT CategoryName
		FROM @mapCategories
		INSERT INTO @restrictedValues
			([Name])
		SELECT CategoryName
		FROM Asset.ufnWhichCategoriesAreProhibited(@firstImportedAssetID, @securityUserID, @categoryNameList, @categoryAttributeTypeID)
		IF EXISTS(SELECT TOP 1
			*
		FROM @restrictedValues)
		BEGIN
			DECLARE @concatCategories varchar(MAX)
			SELECT @concatCategories = COALESCE(@concatCategories + ', ', '') + [Name]
			FROM (SELECT DISTINCT TOP 5
					[Name]
				FROM @restrictedValues) AS A
			IF (SELECT COUNT(*)
			FROM @restrictedValues) > 5
			BEGIN
				SET @concatCategories = @concatCategories +  ', ...'
			END
			SET @concatCategories = 'User does not have permission to import categories: ' + @concatCategories
			RAISERROR(@concatCategories, 13, 1)
			RETURN @returnValue
		END

		DECLARE @attributeStringList AS Base.tpStringList
		INSERT INTO @attributeStringList
		SELECT DISTINCT [Name]
		FROM [ETL].[tCapPriorCustomAttributes]
		WHERE BatchID = @batchId
		INSERT INTO @restrictedValues
			([Name])
		SELECT AttributeName
		FROM Asset.ufnWhichAttributeNamesAreProhibited(@firstImportedAssetID, @securityUserID, @attributeStringList)
		OPTION (RECOMPILE)

		IF EXISTS (SELECT TOP 1
			*
		FROM @restrictedValues)
		BEGIN
			DECLARE @concatAttributes varchar(MAX)
			SELECT @concatAttributes = COALESCE(@concatAttributes + ', ', '') + [Name]
			FROM (SELECT DISTINCT TOP 5
					[Name]
				FROM @restrictedValues) AS A
			IF (SELECT COUNT(*)
			FROM @restrictedValues) > 5
			BEGIN
				SET @concatAttributes = @concatAttributes + ', ...'
			END
			SET @concatAttributes = 'User does not have permission to import attributes: ' + @concatAttributes
			RAISERROR(@concatAttributes, 13, 1)
			RETURN @returnValue
		END

		-- now pull in the real category IDs
		UPDATE ca
		SET CategoryID = ufn.CategoryID
		FROM @customAttributeCategories ca
			INNER JOIN Asset.ufnGetAvailableAssetCategories(@securityUSerID, @parentAssetID, @categoryAttributeTypeID) ufn
			ON ufn.Name = ca.CategoryName

		BEGIN TRY
			BEGIN TRANSACTION

				-- Add the categories
				WHILE EXISTS(SELECT TOP 1
					*
				FROM @customAttributeCategories
				WHERE CategoryID IS NULL)
				BEGIN
					DECLARE @categoryName varchar(255)
					DECLARE @categoryID int
					SELECT TOP 1
						@categoryName = CategoryName
					FROM @customAttributeCategories
					WHERE CategoryID IS NULL
					EXEC Asset.spInsertCategory @securityUserID, @firstImportedAssetID, @categoryName, @categoryTypeIDs, 0, @categoryID OUTPUT
					UPDATE @customAttributeCategories SET CategoryID = @categoryID WHERE CategoryName = @categoryName
					-- We want the map category to have the same categoryID, but a different CategoryMapID
					UPDATE @mapCategories SET CategoryID = @categoryID WHERE CategoryName = @categoryName
				END

				--This will delete all asset attributes for the selected assets where the user has access to delete the attributes

				create table #validattributes
				(
					AssetID int,
					AttributeTypeID int,
					IsValid bit
				)

				CREATE INDEX tmp_idx_1 ON #validattributes (AssetID);
				CREATE INDEX tmp_idx_2 ON #validattributes (AttributeTypeID);

				insert into #validattributes select distinct A.AssetID, b.AttributeTypeID,0 from #tmp_capprior_assets a join Asset.tassetattribute b on a.AssetID = b.AssetID
				UPDATE a set IsValid = 1 FROM #validattributes a WHERE (SELECT IsValid FROM Asset.ufnAttributeIsValidForUser(@securityUserID, AssetID, AttributeTypeID)) = 1

				IF @AppendAssetAttributes = 0
				BEGIN
					--Overwrite: Delete all attributes for assets in this import
					DELETE a FROM Asset.tAssetAttribute a
					JOIN #validattributes b on a.AssetID = b.AssetID AND a.AttributeTypeID = b.AttributeTypeID
					WHERE b.IsValid = 1
				END
				ELSE
				BEGIN
					--Append: Only delete attributes that are specified on Assets tab for assets in this import
					CREATE TABLE #appendAttributes
					(
						AssetID INT,
						AttributeTypeID INT
					)
					CREATE INDEX tmp_idx_1 ON #appendAttributes (AssetID);
					CREATE INDEX tmp_idx_2 ON #appendAttributes (AttributeTypeID);

					INSERT INTO #appendAttributes
						SELECT DISTINCT c.AssetID, t.AttributeTypeID
						FROM [ETL].[tCapPriorProject] p
						JOIN [ETL].[tCapPriorCustomAttributes] ca ON p.BatchID = ca.BatchID
						JOIN #tmp_capprior_assets c ON p.ProjectNumber = c.ProjectNumber
						JOIN Asset.tAssetAttribute a ON c.AssetID = a.AssetID
						JOIN Asset.tAttributeType t ON a.AttributeTypeID = t.AttributeTypeID
						WHERE p.BatchID = @batchId AND ca.[Name] = t.AttributeTypeDesc
					UNION
						SELECT DISTINCT c.AssetID, sa.AssetAttributeTypeID AS AttributeTypeID
						FROM [ETL].[tCapPriorProject] p
						JOIN [ETL].[tCapPriorStandardAttributes] sa ON p.BatchID = sa.BatchID
						JOIN #tmp_capprior_assets c ON p.ProjectNumber = c.ProjectNumber
						WHERE p.BatchID = @batchId

					DELETE a FROM Asset.tAssetAttribute a
					JOIN #validattributes b ON a.AssetID = b.AssetID AND a.AttributeTypeID = b.AttributeTypeID
					JOIN #appendAttributes d ON a.AssetID = d.AssetID AND a.AttributeTypeID = d.AttributeTypeID
					WHERE b.IsValid = 1
				END

				CREATE TABLE #newAttributes  (
					attributeName NVARCHAR(255) NULL,
					attributeTypeID INT NULL,
					attributeValue NVARCHAR(MAX) NOT NULL,
					attributeDate DATETIMEOFFSET NULL,
					displayOrder INT NULL,
					favorite BIT NULL,
					attributeID INT,
					assetID INT NOT NULL,
					IsValid bit NULL,
					KPITagAssetRefID INT NULL --used to update KPI Tag references; see below.
				)

				CREATE INDEX IDX_1 ON #newAttributes(attributeName);
				CREATE INDEX IDX_2 ON #newAttributes(attributeTypeID);
				CREATE INDEX IDX_3 ON #newAttributes(assetID);
				CREATE INDEX IDX_4 ON #newAttributes(isValid);

				INSERT INTO #newAttributes(attributeName, attributeValue, attributeDate, displayOrder, favorite, assetID)
				SELECT ca.[Name], ca.[Value], ca.[Attribute_date], ca.DisplayOrder, ca.Favorite,imp.AssetID
				FROM ETL.tCapPriorCustomAttributes ca
				INNER JOIN #tmp_capprior_assets imp ON imp.ProjectNumber = ca.ProjectNumber --the 'local' assetID field from the import spreadsheet, per SekoiaImport.cs:624
				WHERE ca.BatchID = @batchID

				-- We do not currently support dates as standard attributes. I'm setting the @Date parameter to an invalid value for all standard attributes as a precaution
				-- so that spUpsertAssetAttribute will try to parse @Value for any date attribrutes.
				-- A future enhancement to support dates as standard attributes would require adding an Attribute_date column to ETL.tCapPriorStandardAttributes and
				-- modifying CapPriorImportRepository.ExtractAttributes in SekoiaImport.cs to populate the field.
				DECLARE @invalidAttributeDate DATETIMEOFFSET = '1900-01-01T00:00:00+00:00' -- Ignore the @Date parameter and try to parse @Value for date attributes

				INSERT INTO #newAttributes(attributeName, attributeValue, attributeDate, displayOrder, favorite, assetID, attributeTypeID)
				SELECT attType.AttributeTypeDesc, COALESCE(sa.Attribute_string, COALESCE(CONVERT(NVARCHAR(255),sa.Attribute_int), CONVERT(NVARCHAR(255),sa.Attribute_float,128))), @invalidAttributeDate, sa.DisplayOrder, sa.Favorite, a.AssetID , attType.AttributeTypeID
				FROM ETL.tCapPriorStandardAttributes sa
				INNER JOIN #tmp_capprior_assets a ON a.ProjectNumber = sa.AssetID
				INNER JOIN Asset.tAttributeType attType ON sa.AssetAttributeTypeID = attType.AttributeTypeID
				WHERE sa.BatchID = @batchID

				--KPI Tag attributes reference tags using their spreadsheet assetID, the ProjectNumber.
				--Update those attribute strings with the saved assetIDs, replacing the spreadsheet assetIDs.
				--But any KPI Tag attribute can reference any imported asset. So, capture the referenced ID
				--in a new field, then join against the imported asset table matching that referenced ID to
				--the project number, and replace the attribute's referenced ID with the post-import asset ID
				--for that asset. Obviously this can only work if the KPI Tag string includes such a reference;
				--some use literal numbers or other/existing tag names, and those need no updating.
				--Note that we designate an asset ID to be updated by containing it in angle brackets <>!
				UPDATE #newAttributes
				SET KPITagAssetRefID = CONVERT(int, REPLACE(SUBSTRING(attributeValue, CHARINDEX('Value;<',attributeValue)+7, 15), '>',''))
				WHERE (attributeName like 'KPI Tag%' OR attributeName like 'Asset Tag') AND CHARINDEX('Value;<',attributeValue) > 0;

				UPDATE a
				SET a.attributeValue = REPLACE(attributeValue, CONCAT('<', CONVERT(NVARCHAR(32),a.KPITagAssetRefID), '>'), CONVERT(NVARCHAR(32),b.assetID))
				FROM #newAttributes a
				INNER JOIN #tmp_capprior_assets b on a.KPITagAssetRefID = b.ProjectNumber


				create table #AssetsWithClient
				(
				 ClientID int,
				 AssetID int
				)

				create index idx_1 ON #AssetsWithClient(ClientID);
				create index idx_2 ON #AssetsWithClient(AssetID);

				create table #AssetsWithClientTemp
				(
				 ClientID int,
				 AssetID int,
				 AncestryLevel int
				)

				create index idx_1 ON #AssetsWithClientTemp(ClientID);
				create index idx_2 ON #AssetsWithClientTemp(AssetID);
				create index idx_3 ON #AssetsWithClientTemp(AncestryLevel);

				insert into #AssetsWithClientTemp (AssetID,ClientID,AncestryLevel)
				select distinct a.AssetID,c.ClientID,b.AncestryLevel from #newAttributes a join Asset.tAssetHop b on a.AssetID = b.EndingAssetId join Asset.tClient c on b.HopAssetId = c.AssetID

				create table #assetAndAncestryLevel
				(
					assetId int,
					ancestrylevel int
				)

				create index idx_1 on #assetAndAncestryLevel(assetId)
				insert into #assetAndAncestryLevel
				select AssetID,MAX(AncestryLevel) as AncestryLevel from #AssetsWithClientTemp group by AssetID

				INSERT INTO #AssetsWithClient (AssetID,ClientID) SELECT X.AssetID,X.ClientID from #AssetsWithClientTemp x JOIN #assetAndAncestryLevel b ON x.AssetID = b.assetid and x.AncestryLevel = b.ancestrylevel

				create table #Attributes
				(
				  ClientID int null,
				  AttributeTypeID int null,
				  AttributeName varchar(255),
				  IsTextDataType bit,
				  IsFloatDataType bit,
				  IsStandard bit
				)

				create index idx_1 ON #Attributes(ClientID);
				create index idx_2 ON #Attributes(AttributeName);
				create index idx_3 ON #Attributes(IsTextDataType);
				create index idx_4 ON #Attributes(IsFloatDataType);
				create index idx_5 ON #Attributes(IsStandard);


				INSERT INTO #Attributes (ClientID,AttributeName,IsTextDataType,IsFloatDataType,IsStandard)
				SELECT DISTINCT ClientID,AttributeName,0,0,0 FROM #newAttributes a JOIN #AssetsWithClient b on A.AssetID = b.AssetID

				-- find all existing custom attributes and mark whether they are 'string'.  String attributes will be handled in a special case since they don't
				-- require range or value validation
				update a set a.AttributeTypeID = b.AttributeTypeID,
				IsTextDataType = CASE WHEN b.DisplayFormat = 'string' then 1 else 0 END,
				IsFloatDataType = CASE WHEN b.DisplayFormat like 'f%' then 1 else 0 END
				 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
				AND a.ClientID = b.ClientID

				-- find all standard attributes
				update a set a.AttributeTypeID = b.AttributeTypeID,
				IsTextDataType = CASE WHEN b.DisplayFormat = 'string' then 1 else 0 END,
				IsFloatDataType = CASE WHEN b.DisplayFormat like 'f%' then 1 else 0 END,
				IsStandard = 1
				 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
				WHERE b.ClientID IS NULL

				-- Insert new attribute types if they are string and custom
				INSERT INTO Asset.tAttributeType (AttributeTypeDesc, AttributeTypeKey, EngrUnits, DisplayFormat, IsStandard, IsExempt, ClientID)
				SELECT AttributeName,SUBSTRING(REPLACE(AttributeName,' ',''),0,99),NULL,'string',0,0,ClientID FROM #Attributes WHERE
				IsStandard = 0 AND AttributeTypeID IS NULL AND ClientID IS NOT NULL

				-- Re-update table variable with new custom attribute types
				update a set a.AttributeTypeID = b.AttributeTypeID,
				IsTextDataType = CASE WHEN b.DisplayFormat = 'string' then 1 else 0 END
				 FROM #Attributes a JOIN Asset.tAttributeType b on a.AttributeName = b.AttributeTypeDesc
				AND a.ClientID = b.ClientID WHERE a.AttributeTypeID IS NULL

				-- update table variable with Attribute Type ID for attributes that already exist and are custom
				UPDATE a SET a.AttributeTypeID = C.AttributeTypeID FROM #newAttributes a JOIN #AssetsWithClient b on A.AssetID = b.AssetID JOIN #Attributes c on A.attributeName =
				c.AttributeName and b.ClientID = c.ClientID and c.IsStandard = 0

				-- update table variable with Attribute Type ID for string or float attributes that already exist and are standard
				UPDATE a SET a.AttributeTypeID = C.AttributeTypeID FROM #newAttributes a JOIN #AssetsWithClient b on A.AssetID = b.AssetID JOIN #Attributes c on A.attributeName =
				c.AttributeName and (c.IsTextDataType=1 or c.IsFloatDataType=1) AND c.IsStandard = 1

				UPDATE a set IsValid = 1 FROM #newAttributes a WHERE (SELECT IsValid FROM Asset.ufnAttributeIsValidForUser(@securityUserID, AssetID, AttributeTypeID)) = 1

				INSERT Asset.tAssetAttribute  (AssetID,AttributeTypeID,AttributeOptionTypeID,Attribute_int,Attribute_string, Attribute_float,Favorite,DisplayOrder,
				CreatedByUserID,ChangedByUserID,CreateDate,ChangeDate)
				SELECT DISTINCT assetID,b.attributeTypeID,NULL,NULL,attributeValue,null,favorite,displayOrder,@securityUserID,@securityUserID,
				GETDATE(),GETDATE() FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID where b.attributeTypeID is not null AND
				c.IsTextDataType = 1 AND b.IsValid = 1

				DECLARE @badValueAttributeName as varchar(255)

				set @badValueAttributeName = NULL
				SELECT TOP 1 @badValueAttributeName = b.attributeName FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID where b.attributeTypeID is not null AND
				ISNULL(attributeValue,'') <> '' AND c.IsFloatDataType = 1 AND (SELECT IsValid FROM Asset.ufnAttributeIsValidForUser(@securityUserID, AssetID, b.AttributeTypeID)) = 1 AND (LEN(attributeValue)>309 OR ISNUMERIC(attributeValue)=0)

				IF @badValueAttributeName IS NOT NULL
				  BEGIN
				  SET @errorMsg = 'Error in excel inputs - expecting numeric value for attribute: ' + @badValueAttributeName
				  RAISERROR (@errorMsg, 13, -1, -1)
				  RETURN
				  END

				INSERT Asset.tAssetAttribute  (AssetID,AttributeTypeID,AttributeOptionTypeID,Attribute_int,Attribute_string, Attribute_float,Favorite,DisplayOrder,
				CreatedByUserID,ChangedByUserID,CreateDate,ChangeDate)
				SELECT DISTINCT assetID,b.attributeTypeID,NULL,NULL,null,CASE WHEN ISNULL(attributeValue,'')='' THEN NULL ELSE attributeValue END,favorite,displayOrder,@securityUserID,@securityUserID,
				GETDATE(),GETDATE() FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID where b.attributeTypeID is not null AND
				c.IsFloatDataType = 1 AND IsValid = 1

				-- insert blank date and radio attribute values
				INSERT Asset.tAssetAttribute  (AssetID,AttributeTypeID,Favorite,DisplayOrder,
				CreatedByUserID,ChangedByUserID,CreateDate,ChangeDate)
				SELECT DISTINCT assetID,b.attributeTypeID,favorite,displayOrder,@securityUserID,@securityUserID,
				GETDATE(),GETDATE() FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID
				JOIN Asset.tAttributeType d on c.AttributeTypeID = d.AttributeTypeID
				where b.attributeTypeID is not null AND UPPER(d.displayformat) in ('DATE','RADIO') AND IsValid = 1 AND b.attributeValue=''

				-- mark any processed attributes as done by setting their id to 999
				UPDATE b SET b.attributeID = 999 FROM #newAttributes b JOIN #Attributes c on b.attributeTypeID = c.AttributeTypeID where b.attributeTypeID is not null AND
				(c.IsTextDataType = 1 or c.IsFloatDataType=1)

				-- mark any processed attributes that are blank dates as done by setting their id to 999
				update a SET attributeID = 999 from #newAttributes a join Asset.tAttributeType b on a.attributeTypeID = b.AttributeTypeID
				WHERE attributeID IS NULL AND a.attributeValue=''  and UPPER(b.DisplayFormat) in ('DATE','RADIO') AND IsValid = 1

				-- loop through any remaining unprocessed attributes
				WHILE EXISTS(SELECT TOP 1 * FROM #newAttributes WHERE attributeID IS NULL)
				BEGIN

					DECLARE @newAttributeID INT;
					DECLARE @newTypeID INT;

					DECLARE @attAssetID INT;
					DECLARE @attributeName NVARCHAR(255);
					DECLARE @attributeValue NVARCHAR(MAX);
					DECLARE @attributeDate DATETIMEOFFSET;
					DECLARE @attributeTypeID INT;
					DECLARE @favorite BIT;
					DECLARE @displayOrder INT;
					DECLARE @newID INT;
					SET @attributeName = NULL;
					SET @attributeTypeID = NULL;
					SELECT TOP 1 @attAssetID = assetID, @attributeName = attributeName, @attributeValue = attributeValue, @attributeDate = attributeDate, @favorite = favorite, @displayOrder = displayOrder, @attributeTypeID = attributeTypeID FROM #newAttributes
					WHERE attributeID IS NULL;

					BEGIN TRY
						DECLARE @emptylist as base.tpintlist
						EXEC Asset.spUpsertAssetAttribute 
						  @SecurityUserID = @securityUserID
						, @AssetID = @attAssetID
						, @AttributeTypeID = @attributeTypeID
						, @Name = @attributeName
						, @Value = @attributeValue
						, @Favorite = @favorite
						, @DisplayOrder = @displayOrder
						, @Date = @attributeDate
						, @IncludeCategories = 0
						, @CategoryIDs = @emptylist
						, @NewDisplayFormat = NULL
						, @NewID = @newAttributeID OUTPUT
						, @NewTypeID = @newTypeID OUTPUT
					END TRY
					BEGIN CATCH
						SET @errorMsg = 'Error in excel inputs - Invalid attribute value: ' + @attributeName + ':' + @attributeValue
						RAISERROR (@errorMsg, 13, -1, -1)
						RETURN
					END CATCH

					UPDATE #newAttributes
					SET attributeID = @newAttributeID
					WHERE attributeName = @attributeName AND assetID = @attAssetID

				END

				DECLARE @AssetCategories as TABLE
				(
				  CategoryID int,
				  AttributeTypeID int,
				  AssetID int,
				  ClientID int,
				  Processed bit NULL
				  )

				-- delete category maps for custom attributes
				DELETE m from Asset.tAssetAttributeCategoryMap m JOIN #newAttributes a ON m.AttributeTypeID = a.attributeTypeID JOIN #AssetsWithClient b on A.AssetID = b.AssetID JOIN #Attributes c on A.attributeName =
				c.AttributeName and b.ClientID = c.ClientID AND c.IsTextDataType = 1 AND c.IsStandard = 0

				INSERT INTO @AssetCategories (CategoryID,AttributeTypeID,AssetID,Processed) SELECT DISTINCT b.CategoryID,a.attributeTypeID,a.assetID,0 FROM #newAttributes a JOIN
				@customAttributeCategories b ON a.attributeName = b.attributeName WHERE a.attributeTypeID IS NOT NULL

				UPDATE A SET ClientID = b.ClientID FROM @AssetCategories a JOIN #AssetsWithClient b on a.AssetID = b.AssetID OPTION (RECOMPILE)

				declare @clientcategories as table
				(
				  ClientID int,
				  ClientAssetID int,
				  AttributeTypeID int,
				  CategoryID int,
				  Processed bit
				)

				-- the upsert of categories is at a clientid level, so consolidate them down to a distinct list by client asset id
				INSERT INTO @clientcategories (ClientID,ClientAssetID,AttributeTypeID,CategoryID,Processed) select distinct A.ClientID,b.AssetID, a.AttributeTypeID,a.CategoryID,0 from @AssetCategories a JOIN
				Asset.tClient b on a.ClientID = b.ClientID

				-- loop through assets and attributes to set their categories
				WHILE EXISTS(SELECT TOP 1 * FROM @clientcategories WHERE Processed=0)
				  BEGIN
				  DECLARE @curAttributeTypeID as int
				  DECLARE @curAssetID as int
				  SELECT TOP 1 @curAttributeTypeID = AttributeTypeID, @curAssetID = ClientAssetID FROM @clientcategories WHERE Processed = 0 OPTION (RECOMPILE)

				  DECLARE @categoryIDs Base.tpIntList
				  DELETE FROM @categoryIDs;
				  INSERT INTO @categoryIDs(id)
					SELECT CategoryID FROM @clientcategories WHERE AttributeTypeID= @curAttributeTypeID and ClientAssetID = @curAssetID OPTION (RECOMPILE)
				  EXEC [Asset].[spSetAssetAttributeCategories] @curAttributeTypeID, @curAssetID, @CategoryIDs
				  UPDATE @clientcategories SET Processed = 1 WHERE ClientAssetID = @curAssetID and AttributeTypeID = @curAttributeTypeID OPTION (RECOMPILE)
				  END

				--*----------
				-- Check to see if any of our map categories have category IDs
				DECLARE @categoryMapTypeID int
				SELECT TOP 1
					@categoryMapTypeID = CategoryTypeID
				FROM Asset.tCategoryType
				WHERE CategoryTypeName LIKE 'Maps'
				DECLARE @mapCategoryTypeIDs AS Base.tpIntList
				INSERT INTO @mapCategoryTypeIDs
				VALUES
					(@categoryMapTypeID)

				UPDATE mc
				SET CategoryID = ufn.CategoryID
				FROM @mapCategories mc
					INNER JOIN Asset.ufnGetAvailableAssetCategories(@securityUSerID, @parentAssetID, @categoryMapTypeID) ufn
					ON ufn.Name = mc.CategoryName
				-- For each category that is not null, add the categoryType of 'Map' if it is not there already

				DECLARE @mapsToAddToCategoryTypeMap TABLE (
					CategoryID     int,
					CategoryTypeID int
				);
				INSERT INTO @mapsToAddToCategoryTypeMap
					(CategoryID, CategoryTypeID)
				SELECT mc.[CategoryID], @categoryMapTypeID
				FROM @mapCategories AS mc
				WHERE mc.CategoryID IS NOT NULL
				MERGE INTO [Asset].[tCategoryTypeMap] A
				USING @mapsToAddToCategoryTypeMap B
				ON (A.CategoryID = B.CategoryID AND A.CategoryTypeID = @categoryMapTypeID)
				WHEN NOT MATCHED THEN
				INSERT([CategoryID], [CategoryTypeID])
				VALUES(B.CategoryID, @categoryMapTypeID);

				-- Insert Map categories
				WHILE EXISTS(SELECT TOP 1
					*
				FROM @mapCategories
				WHERE CategoryID IS NULL)
				BEGIN
					DECLARE @mapCategoryName varchar(255)
					DECLARE @mapCategoryID int
					SELECT TOP 1
						@mapCategoryName = CategoryName
					FROM @mapCategories
					WHERE CategoryID IS NULL
					EXEC Asset.spInsertCategory @securityUserID, @firstImportedAssetID, @mapCategoryName, @mapCategoryTypeIDs, 0, @mapCategoryID OUTPUT
					UPDATE @mapCategories SET CategoryID = @mapCategoryID WHERE CategoryName = @mapCategoryName
				END

				--*----------

				IF @tags= 1
				BEGIN
					DECLARE @assetName nvarchar(255);
					DECLARE @tagID int;
					DECLARE @pDServerID int;
					DECLARE @tagName nvarchar(400);
					DECLARE @tagDesc nvarchar(400);
					DECLARE @variableTypeID int;
					DECLARE @valueTypeID int;
					DECLARE @engUnits nvarchar(255);

					--Insert tags into temporary table
					INSERT INTO #tmpTags
						(ProjectNumber,AssetID,AssetName,TagID,PDServerID,PDTagID,TagName,TagDesc,VariableTypeID,ValueTypeID,EngUnits, PDTagMapID)
					SELECT a.ProjectNumber, a.AssetID, a.ProjectName, b.TagID, NULL, NULL, NULL, b.TagDesc, b.VariableTypeID, b.ValueTypeID, b.UnitOfMeasure, b.ExistingPDTagMap
					FROM #tmp_capprior_assets a
						RIGHT JOIN ETL.tCapPriorTag b ON a.ProjectNumber = b.AssetID
					WHERE BatchID = @batchId

					-- pull in the existing OPM and InfluxDB Servers for this asset.
					IF @OPMTags = 1
					BEGIN
						declare @assetsAndTheirOpmPDServerIDs table (assetID int, pdServerID int)
						INSERT INTO @assetsAndTheirOpmPDServerIDs
							SELECT a.AssetID, ProcessData.ufnGetPDServerForAssetID(a.AssetID, 1, NULL)
								FROM (SELECT DISTINCT AssetID FROM #tmp_capprior_assets) AS a
						INSERT INTO @assetsAndTheirOpmPDServerIDs
							SELECT b.AssetID, ProcessData.ufnGetPDServerForAssetID(b.AssetID, 14, NULL)
								FROM (SELECT DISTINCT AssetID FROM #tmp_capprior_assets) AS b
						DELETE FROM @assetsAndTheirOpmPDServerIDs where pdServerID is NULL -- clean up where no server found
						DECLARE @realOpmTags table (
							fakeOpmTagID int null,
							opmTagName nvarchar(400) null,
							usedInAssetID int null,
							pdServerID int null,
							realPDTagID int null
						)
						INSERT INTO @realOpmTags
							SELECT tt.PDTagMapID, epd.PDTagName, aatops.assetID, t.PDServerID, t.PDTagID
								from #tmpTags tt
								left outer join etl.tCapPriorExistingPDTags epd on epd.PDTagID = tt.PDTagMapID
								left outer join @assetsAndTheirOpmPDServerIDs aatops on aatops.assetid = tt.assetid
								left outer join processdata.tTag t on t.TagName = epd.PDTagName and t.PDServerID = aatops.pdServerID
								where epd.BatchID = @batchId and tt.PDTagMapID is not null
						-- we've pulled in everything, and there could be errors, which we'll see by different null values
						if exists (select * from @realOpmTags where usedInAssetID is null)
						BEGIN
							-- this asset doesn't have an opm pd server, get out of here
							set @errorMsg = 'Error, could not find an associated OPM PD Server for the imported asset'
							Raiserror(@errorMsg, 13, -1, -1)
							RETURN @returnValue
						END
						if exists (select * from @realOpmTags where pdServerID is NULL)
						BEGIN
							-- couldn't find a corresponding PDTag matching based on the PDServers for the Asset and the TagName.
							set @errorMsg = 'Error, could not find a matching tag for the OPM tag in the servers for this asset'
							Raiserror(@errorMsg, 13, -1, -1)
							RETURN @returnValue
						END

						-- figure out the relevant AssetVariableTypeTagMap records based on the import
						declare @avttm table (avttmid int, assetid int, tagid int, variabletypeid int, valuetypeid int)

						insert into @avttm
							select distinct avttm.AssetVariableTypeTagMapID, avttm.AssetID, avttm.TagID, avttm.VariableTypeID, avttm.ValueTypeID
								from ProcessData.tServer s
								inner join @assetsAndTheirOpmPDServerIDs aatops on aatops.pdServerID = s.PDServerID -- restricting to only those pdServers that were found in import
								inner join ProcessData.tTag pdtt on pdtt.PDServerID = s.PDServerID
								inner join ProcessData.tAssetVariableTypeTagMap avttm on avttm.TagID = pdtt.PDTagID -- AssetVariableTypeTagMap only showing tags from servers in this spreadsheet
								inner join #tmp_capprior_assets a on a.AssetID = avttm.AssetID -- restricting AssetVariableTypeTagMap to only those assets included in this spreadsheet

						-- delete from AssetVariableTypeTagMap those records not found in this spreadsheet
						declare @avttmToDelete table (avttmid int)
						insert into @avttmToDelete
							select avttm.avttmid
							from @avttm avttm
							inner join #tmp_capprior_assets a on a.AssetID = avttm.assetid
							left outer join @realOpmTags r on r.realPDTagID = avttm.tagid and r.usedInAssetID = a.AssetID
							where r.usedInAssetID is null -- only pull those records that aren't found

						-- delete from real table
						delete m from ProcessData.tAssetVariableTypeTagMap m inner join @avttmToDelete d on d.avttmid = m.AssetVariableTypeTagMapID
						delete from @avttmToDelete

						-- check for any updated ValueTypeIDs or VariableTypeIDs
						update a
							set a.variabletypeid = t.VariableTypeID, a.valuetypeid = t.ValueTypeID
							from @avttm a
							join #tmpTags t on t.AssetID = a.assetid and t.PDTagID = a.tagid
						update avt
							set
								avt.VariableTypeID = a.variabletypeid,
								avt.ValueTypeID = a.valuetypeid,
								avt.ChangeDate = GETDATE(),
								avt.ChangedBy = @createdBy
							from ProcessData.tAssetVariableTypeTagMap avt
							join @avttm a on a.avttmid = avt.AssetVariableTypeTagMapID

						-- insert new mappings values into @avttm
						insert into @avttm
							(assetid, tagid, variabletypeid, valuetypeid)
							select
								t.AssetID, o.realPDTagID, t.VariableTypeID, t.ValueTypeID
							from #tmpTags t
							inner join @realOpmTags o on o.usedInAssetID = t.AssetID and o.fakeOpmTagID = t.PDTagMapID
							left outer join @avttm a on a.assetid = t.AssetID and a.tagid = o.realPDTagID
							where a.assetid is null -- nothing matched in @avttm

						-- insert remaining @avttm into AssetVariableTypeTagMap
						insert into ProcessData.tAssetVariableTypeTagMap
							(AssetID, TagID, VariableTypeID, ValueTypeID,
								CreatedBy, ChangedBy, CreateDate, ChangeDate, CalcString)
							select
								a.assetid, a.tagid, a.variabletypeid, a.valuetypeid,
								@createdBy, @createdBy, GETDATE(), GETDATE(), ''
							from @avttm a where a.avttmid is null -- these are inserts

						-- finally, come back through and update the #tmpTags table with the PDTagID of these tags
						update p
							set PDTagID = o.realPDTagID
							from #tmpTags p
							inner join @realOpmTags o on o.fakeOpmTagID = p.PDTagMapID and o.usedInAssetID = p.AssetID
					END

					-- double check and make sure no OPM tags made it through unmapped
					-- this error will be thrown if there was somehow an uncaught error in the OPM block above, or if user
					-- has PDTagMapIDs that are non-null and the OpmTags field in ImportAction is false.
					if exists (select top 1 * from #tmpTags where PDTagMapID is not null and PDTagID is null)
					BEGIN
						set @errorMsg = 'there was an error matching a PDTagMapID to the actual PDTagList'
						Raiserror(@errorMsg, 13, -1, -1)
						RETURN @returnValue
					END

					-- no reason this should be a cursor here
					declare @assetsAndTheirTSQLHistorianPDServerIDs table (assetID int, pdServerID int)
					insert into @assetsAndTheirTSQLHistorianPDServerIDs
						select a.AssetID, ProcessData.ufnGetPDServerForAssetID(a.AssetID, 10, 'tSQLHistorian')
							from (select distinct assetid from #tmpTags where (ValueTypeID is NULL OR ValueTypeID NOT IN (25, 26, 27)) and PDTagMapID IS NULL) as a
					UPDATE tt
						SET tt.PDServerID = aatpid.pdServerID
						FROM #tmpTags tt
						INNER JOIN @assetsAndTheirTSQLHistorianPDServerIDs aatpid on aatpid.assetID = tt.AssetID
						WHERE tt.ValueTypeID IS NULL OR tt.ValueTypeID NOT IN (25, 26, 27) and aatpid.pdServerID IS NOT NULL

					--Identify Asset with missing PDServer for Tags required for tSQLHistorian and throw out error
					SELECT DISTINCT b.ProjectName AS ProjectName
					INTO #missingPDServers
					FROM #tmpTags AS a JOIN #tmp_capprior_assets AS b ON a.AssetID = b.AssetID
					WHERE a.PDServerID IS NULL AND (ValueTypeID IS NULL OR ValueTypeID NOT IN (25,26,27)) AND a.PDTagMapID IS NULL

					SELECT @cnt = count(*)
					FROM #missingPDServers;
					IF @cnt > 0
					BEGIN
						SELECT @errorMsg = @errorMsg + ProjectName + ','
						FROM #missingPDServers
						SELECT @errorMsg = SUBSTRING(@errorMsg, 0, LEN(@errorMsg) - 1)
						--trim extra "," at end
						SET @errorMsg = 'Error in finding PDServer(tSQLHistorian) for the following assets: ( ' + @errorMsg + ')'
						RAISERROR (@errorMsg,13, -1, -1)
						DROP TABLE #missingPDServers
						RETURN @returnValue
					END

					DROP TABLE #missingPDServers

					--Loop through and find corresponding PDTagID, create AdHocTag if not found.
					DECLARE tag_cursor CURSOR FOR
						SELECT AssetID, AssetName, TagID, PDServerID, TagDesc, VariableTypeID, ValueTypeID, EngUnits
					FROM #tmpTags
					WHERE (ValueTypeID IS NULL OR ValueTypeID NOT IN (25,26,27)) AND PDTagMapID IS NULL
					OPEN tag_cursor
					FETCH NEXT FROM tag_cursor INTO @assetID, @assetName, @tagID, @pDServerID, @tagDesc, @variableTypeID, @valueTypeID, @engUnits
					DECLARE @tag_Status int
					SET @tag_Status = @@FETCH_STATUS

					WHILE @tag_Status = 0
						BEGIN
						DECLARE @pDTagID int;
						SET @pDTagID = NULL;

						DECLARE @strAssetID AS nvarchar(10);
						SELECT @strAssetID =  CAST(@assetID AS nvarchar(10));

						--Search by TagDesc provided. If not found, search by composite Tagname
						SELECT @pDTagID = PDTagID
						FROM ProcessData.tTag
						WHERE PDServerID = @pDServerID AND TagName = @tagDesc
						IF @pDTagID IS NULL
							BEGIN
							SET @tagName ='ACTUAL:' + @tagDesc + '___' + @strAssetID;
							SELECT @pDTagID = PDTagID
							FROM ProcessData.tTag
							WHERE PDServerID = @pDServerID AND TagName = @tagName
						END

						IF @pDTagID IS NOT NULL
								--Corresponding tag is found, set the PDTagID in #tmpTags
								BEGIN
							UPDATE #tmpTags SET PDTagID = @pDTagID WHERE AssetID =@assetID AND TagID = @tagID
						END
							ELSE
								--No corresponding tag is found, create AdHoc tags
								BEGIN
							SET @tagName ='ACTUAL:' + @tagDesc + '___' + @strAssetID;
							IF @valueTypeID IS NULL
										BEGIN
								SET @valueTypeID = 23
							--Set valueType = Output
							END
							EXEC [ProcessData].[spCreateTag] @pDServerID, @tagName, @assetID, @variableTypeID,@engUnits, @securityUserID,@createdBy,@valueTypeID,@tagDesc,0,@pDTagID OUTPUT
							UPDATE #tmpTags SET PDTagID = @pDTagID, ValueTypeID = @valueTypeID WHERE AssetID = @assetID AND TagID = @tagID
						END

						FETCH NEXT FROM tag_cursor
							INTO @assetID, @assetName, @tagID, @pDServerID, @tagDesc, @variableTypeID, @valueTypeID, @engUnits
						SET @tag_Status = @@FETCH_STATUS
					END
					CLOSE tag_cursor
					DEALLOCATE tag_cursor

					SELECT @cnt = COUNT(*)
					FROM #tmpTags
					IF @cnt > 0
						BEGIN
						SET @tmpTagsExist = 1
					END
				END

				-- ***************************************************************************************************************
				-- *********************************               Metrics               *****************************************
				-- ***************************************************************************************************************
				IF @metrics = 1
				BEGIN
					--Insert Sekoia ProjectCashFlow records to temporary table
					INSERT INTO #tmpProjectCashFlow(UnitOfMeasure,AssetID,[Timestamp],[Type],[Value],[Status])
					SELECT CASE WHEN IsRevenue = 1 THEN 'Revenue' WHEN IsCost = 1 THEN 'Cost' ELSE isnull(UnitOfMeasure,'') END AS UnitOfMeasure, b.AssetID,
						CASE
								WHEN isnull(year,0) BETWEEN 2001 AND 3000 THEN
									CAST(CAST(year AS varchar) + '-' + CAST(6 AS varchar) + '-' + CAST(1 AS varchar) AS datetime)
								WHEN isnull(year,0) > 3000 THEN
									DATEADD(d,year,'12/30/1899') -- Excel Date Number
								WHEN [date] IS NOT NULL THEN
									[date]
								ELSE
									'01/01/2016'
									END AS TimeStamp, Category AS [Type], [Value], isnull([Status],0)
					FROM ETL.tCapPriorProjectMetrics a JOIN #tmp_capprior_assets b ON a.ProjectNumber = b.ProjectNumber
					WHERE BatchID = @batchId AND [Value] IS NOT NULL AND a.IsBudgetVsActual = 0 AND (a.Category<>'BudgetImpact_$' OR IsBudgetImpact=0)

					INSERT INTO #tmpProjectCashFlow(UnitOfMeasure,AssetID,[Timestamp],[Type],[Value],[Status])
					SELECT '$' AS UnitOfMeasure, b.AssetID,
						CASE WHEN isnull(year,0) BETWEEN 2001 AND 3000 THEN
									CAST(CAST(year AS varchar) + '-' + CAST(6 AS varchar) + '-' + CAST(1 AS varchar) AS datetime)
								WHEN isnull(year,0) > 3000 THEN
									DATEADD(d,year,'12/30/1899') -- Excel Date Number
								WHEN [date] IS NOT NULL THEN
									[date]
								ELSE
									'01/01/2016'
									END AS TimeStamp, 'BudgetImpact_$' AS [Type], [Value], 0
					FROM ETL.tCapPriorProjectMetrics a JOIN #tmp_capprior_assets b ON a.ProjectNumber = b.ProjectNumber
					WHERE BatchID = @batchId AND [Value] IS NOT NULL AND IsBudgetImpact=1 AND a.IsBudgetVsActual = 0

					--Insert Sekoia BudgetvsActual records
					INSERT INTO #tmpProjectCashFlow(UnitOfMeasure,AssetID,[Timestamp],[Type],[Value],[Status])
					SELECT isnull(UnitOfMeasure,'') AS UnitOfMeasure, b.AssetID,
						CASE WHEN isnull(year,0) BETWEEN 2001 AND 3000 THEN
							CAST(CAST(year AS varchar) + '-' + CAST(6 AS varchar) + '-' + CAST(1 AS varchar) AS datetime)
						WHEN isnull(year,0) > 3000 THEN
							DATEADD(d,year,'12/30/1899') -- Excel Date Number
						WHEN [date] IS NOT NULL THEN
							[date]
						ELSE
							'01/01/2016'
							END AS TimeStamp, Category AS [Type], [Value], isnull([Status],0)
					FROM ETL.tCapPriorProjectMetrics a JOIN #tmp_capprior_assets b ON a.ProjectNumber = b.ProjectNumber
					WHERE BatchID = @batchId AND [Value] IS NOT NULL AND a.IsBudgetVsActual = 1

					IF @deleteMetrics = 1
					BEGIN
						DELETE a FROM CapPrior.tProjectCashFlow a JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID;
					END
					ELSE
					BEGIN
						--delete cashflow with the same timestamp
						DELETE a FROM CapPrior.tProjectCashFlow a JOIN #tmpProjectCashFlow b
						ON a.[AssetID] = b.[AssetID] AND a.[Type] = b.[Type] AND a.[Timestamp] = b.[Timestamp]
					END

					INSERT INTO CapPrior.tProjectCashFlow([UnitOfMeasure], [AssetID], [Timestamp], [Type], [Value], [Status])
					SELECT [UnitOfMeasure], [AssetID], [Timestamp], [Type], [Value], [Status] FROM #tmpProjectCashFlow

					-- update cash flow tags

					DECLARE @valueTags TABLE (ETLTagID int,
						PDTagID  int)
					DECLARE @statusTags TABLE (ETLTagID int,
						PDTagID  int)

					INSERT INTO @valueTags
					SELECT DISTINCT c.TagID, b.ValueTagID
					FROM #tmp_capprior_assets a JOIN CapPrior.tProjectCashFlow b ON a.AssetID = b.AssetID
						JOIN #tmpTags c ON a.ProjectNumber = c.ProjectNumber AND b.Type = c.TagDesc
							AND c.ValueTypeID IN (26,27) AND b.ValueTagID IS NOT NULL

					INSERT INTO @statusTags
					SELECT DISTINCT c.TagID, b.StatusTagID
					FROM #tmp_capprior_assets a JOIN CapPrior.tProjectCashFlow b ON a.AssetID = b.AssetID
						JOIN #tmpTags c ON a.ProjectNumber = c.ProjectNumber AND b.Type = c.TagDesc
							AND c.ValueTypeID IN (25) AND b.StatusTagID IS NOT NULL

					UPDATE A SET PDTagID = b.PDTagID FROM #tmpTags A JOIN @valueTags b ON A.TagID = b.ETLTagID AND A.ValueTypeID IN (26,27)
					UPDATE A SET PDTagID = b.PDTagID FROM #tmpTags A JOIN @statusTags b ON A.TagID = b.ETLTagID AND A.ValueTypeID = 25

					--Insert ProcessData to tSQLHistorian
					IF @historian = 1
					BEGIN
						IF @tmpTagsExist = 0
						BEGIN
							SET @errorMsg = 'Error in importing data to tSQLHistorian due to missing PDTags'
							RAISERROR (@errorMsg,13, -1, -1)
							RETURN @returnValue
						END

						DECLARE @nullTags AS int = 0
						SELECT @nullTags = COUNT(*)
						FROM #tmpTags
						WHERE PDTagID IS NULL
						IF @nullTags > 0
						BEGIN
							DECLARE @nullTagID AS int = 0
							SELECT TOP 1
								@nullTagID = TagID
							FROM #tmpTags
							WHERE PDTagID IS NULL
							SET @errorMsg = 'Error in importing data to tSQLHistorian due to null PDTags ' + CONVERT(varchar(25),@nullTagID)
							RAISERROR (@errorMsg,13, -1, -1)
							RETURN @returnValue
						END

						DECLARE @sqlHistorian ProcessData.tpSqlHistorianData;
						INSERT INTO @sqlHistorian
							([PDTagID], [Timestamp], [Value], [Status], [Archive])
						SELECT b.PDTagID, [DateTime], [Value], isnull([Status],0), NULL
						FROM ETL.tCapPriorHistorian a JOIN #tmpTags b ON a.AssetID = b.ProjectNumber AND a.TagID = b.TagID
						WHERE BatchID = @batchId AND (b.ValueTypeID IS NULL OR b.ValueTypeID NOT IN (25,26,27))
						EXEC [ProcessData].spSaveSqlHistorianData @sqlHistorian
					END

				END

				IF @maps = 1
				BEGIN
					IF @deleteMaps = 1
					BEGIN
						DELETE rw FROM [GeoSpa].[tStaticRasterWidget] rw JOIN [GeoSpa].[tStaticRasterGroup] rg ON rw.StaticRasterGroupID = rg.StaticRasterGroupID
							JOIN [GeoSpa].[tStaticRasterLayer] rl ON rg.StaticRasterLayerID = rl.StaticRasterLayerID
							JOIN [GeoSpa].[tGeoSpa] a ON rl.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						DELETE rl FROM [GeoSpa].[tStaticRasterLayer] rl JOIN [GeoSpa].[tGeoSpa] a ON rl.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						DELETE l FROM [GeoSpa].[tGeoSpaLayer] l JOIN [GeoSpa].[tGeoSpa] a ON l.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						DELETE m FROM [GeoSpa].[tAncillaryLayerSourceMap] m JOIN [GeoSpa].[tGeoSpaAncillaryLayer] c ON m.GeoSpaAncillaryLayerID = c.GeoSpaAncillaryLayerID JOIN [GeoSpa].[tGeoSpa] a ON c.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						DELETE c FROM [GeoSpa].[tGeoSpaAncillaryLayer] c JOIN [GeoSpa].[tGeoSpa] a ON c.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						DELETE a FROM [GeoSpa].[tGeoSpaCategoryMap] a JOIN [GeoSpa].[tGeoSpa] b ON a.GeoSpaID = b.GeoSpaID JOIN #tmp_capprior_assets c ON b.AssetID = c.AssetID
						DELETE a FROM [GeoSpa].[tGeoSpa] a JOIN #tmp_capprior_assets b ON a.assetid = b.AssetID
					END
					ELSE
					BEGIN
						DECLARE @exGeoSpas TABLE (
							GeoSpaID int           NOT NULL
							,
							AssetID  int           NULL
							,
							Title    nvarchar(255) NOT NULL
						)
						INSERT INTO @exGeoSpas
							(GeoSpaID, AssetID, Title)
						SELECT g.GeoSpaID, g.AssetID, g.Title
						FROM GeoSpa.tGeoSpa g
							JOIN #tmp_capprior_assets c ON c.AssetID = g.AssetID
						DECLARE @exGSDeletes base.tpIntList
						INSERT INTO @exGSDeletes
							(id)
										SELECT GeoSpaID
							FROM @exGeoSpas
						EXCEPT
							SELECT e.GeoSpaID
							FROM @exGeoSpas e
								JOIN #tmp_capprior_assets a ON e.AssetID = a.AssetID
								JOIN ETL.tCapPriorGeoSpas x ON a.ProjectNumber = x.ProjectNumber AND e.Title = x.Title AND x.BatchID = @batchId

						DELETE rw FROM [GeoSpa].[tStaticRasterWidget] rw JOIN [GeoSpa].[tStaticRasterGroup] rg ON rw.StaticRasterGroupID = rg.StaticRasterGroupID
							JOIN [GeoSpa].[tStaticRasterLayer] rl ON rg.StaticRasterLayerID = rl.StaticRasterLayerID
							JOIN [GeoSpa].[tGeoSpa] a ON rl.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID JOIN @exGSDeletes d ON a.GeoSpaID = d.id
						DELETE rl FROM [GeoSpa].[tStaticRasterLayer] rl JOIN [GeoSpa].[tGeoSpa] a ON rl.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID JOIN @exGSDeletes d ON a.GeoSpaID = d.id
						DELETE l FROM [GeoSpa].[tGeoSpaLayer] l JOIN [GeoSpa].[tGeoSpa] a ON l.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID JOIN @exGSDeletes d ON a.GeoSpaID = d.id
						DELETE m FROM [GeoSpa].[tAncillaryLayerSourceMap] m JOIN [GeoSpa].[tGeoSpaAncillaryLayer] c ON m.GeoSpaAncillaryLayerID = c.GeoSpaAncillaryLayerID JOIN [GeoSpa].[tGeoSpa] a ON c.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID JOIN @exGSDeletes d ON c.GeoSpaID = d.id
						DELETE c FROM [GeoSpa].[tGeoSpaAncillaryLayer] c JOIN [GeoSpa].[tGeoSpa] a ON c.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID JOIN @exGSDeletes d ON a.GeoSpaID = d.id
						DELETE a FROM [GeoSpa].[tGeoSpaCategoryMap] a JOIN [GeoSpa].[tGeoSpa] b ON a.GeoSpaID = b.GeoSpaID JOIN #tmp_capprior_assets c ON b.AssetID = c.AssetID
						DELETE a FROM [GeoSpa].[tGeoSpa] a JOIN #tmp_capprior_assets b ON a.assetid = b.AssetID JOIN @exGSDeletes d ON a.GeoSpaID = d.id

						IF @deleteLayers = 1 OR @layers = 1
						BEGIN
							DELETE l FROM [GeoSpa].[tGeoSpaLayer] l JOIN [GeoSpa].[tGeoSpa] a ON l.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
							DELETE m FROM [GeoSpa].[tAncillaryLayerSourceMap] m JOIN [GeoSpa].[tGeoSpaAncillaryLayer] c ON m.GeoSpaAncillaryLayerID = c.GeoSpaAncillaryLayerID JOIN [GeoSpa].[tGeoSpa] a ON c.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
							DELETE c FROM [GeoSpa].[tGeoSpaAncillaryLayer] c JOIN [GeoSpa].[tGeoSpa] a ON c.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						END

						IF @deleteRasters = 1 OR @rasters = 1
						BEGIN
							DELETE rl FROM [GeoSpa].[tStaticRasterLayer] rl JOIN [GeoSpa].[tGeoSpa] a ON rl.GeoSpaID = a.GeoSpaID JOIN #tmp_capprior_assets b ON a.AssetID = b.AssetID
						END


						DECLARE @map_ID int;
						DECLARE @map_MapID int;
						DECLARE @map_title nvarchar(255);
						DECLARE @map_geospadesc nvarchar(255);
						DECLARE @map_maptypeid int;
						DECLARE @map_basemapid int;
						DECLARE @map_ispublic int;
						DECLARE @map_displayorder int;
						DECLARE @map_geospakey nvarchar(30);
						DECLARE @map_initialextent nvarchar(255);
						DECLARE @map_parentgeospaid int;
						DECLARE @map_mapconfig nvarchar(max);
						DECLARE @map_timebaseid int;
						DECLARE @map_aggregationtypeid int;
						DECLARE @map_decimate bit;
						DECLARE @map_projectNumber int;
						DECLARE @map_newMapID int;
						DECLARE @map_newAssetID int;
						DECLARE @map_categories nvarchar(255);
						DECLARE @map_inherit bit;

						DECLARE @layer_ID int;
						DECLARE @layer_assetclasstypeid int;
						DECLARE @layer_title nvarchar(255);
						DECLARE @layer_symbology nvarchar(max);
						DECLARE @layer_newLayerID int;
						DECLARE @layer_geospalayerdesc nvarchar(255);
						DECLARE @layer_geometry int;
						DECLARE @layer_zoom_min INT;
						DECLARE @layer_zoom_max INT;

						DECLARE @rasterlayer_ID int;
						DECLARE @rasterlayer_ContentID uniqueidentifier;
						DECLARE @rasterlayer_newRasterLayerID int;

						DECLARE @rasterwidget_GroupID int;
						DECLARE @rasterwidget_DisplayOrder int;
						DECLARE @rasterwidget_newRasterGroupID int;

						DECLARE @rasterwidget_ID int;
						DECLARE @rasterwidget_LayerID int;
						DECLARE @rasterwidget_TypeID int;
						DECLARE @rasterwidget_RelativeXPosition int;
						DECLARE @rasterwidget_RelativeYPosition int;
						DECLARE @rasterwidget_AssetID int;
						DECLARE @rasterwidget_AssetVariableTypeTagMapID int;




						DECLARE map_cursor CURSOR
							FOR SELECT e.ID, e.MapID, e.Title, e.GeoSpaDesc, e.MapTypeID, e.BaseMapID, e.IsPublic, e.DisplayOrder, e.GeoSpaKey, e.InitialExtent, e.ParentGeoSpaID, e.MapConfig, e.ProjectNumber, e.TimeBaseID, e.AggregationTypeID, e.Decimate, e.Categories, e.DescendantsInherit
						FROM ETL.tCapPriorGeoSpas e JOIN #tmp_capprior_assets x ON e.ProjectNumber = x.ProjectNumber
						WHERE e.BatchID = @batchId
						ORDER BY e.MapID


						OPEN map_cursor
						FETCH NEXT FROM map_cursor
						INTO @map_ID,@map_MapID,@map_title,@map_geospadesc,@map_maptypeid,@map_basemapid,@map_ispublic,@map_displayorder,@map_geospakey,@map_initialextent,@map_parentgeospaid,@map_mapconfig,@map_projectNumber,@map_timebaseid,@map_aggregationtypeid,@map_decimate,@map_categories,@map_inherit

						WHILE @@FETCH_STATUS = 0
						BEGIN

							SELECT @map_newAssetID = AssetID
							FROM #tmp_capprior_assets
							WHERE ProjectNumber = @map_projectNumber;

							IF EXISTS (SELECT GeoSpaID
							FROM @exGeoSpas
							WHERE AssetID = @map_newAssetID AND Title = @map_title)
							BEGIN
								SELECT @map_newMapID = GeoSpaID
								FROM @exGeoSpas
								WHERE AssetID = @map_newAssetID AND Title = @map_title;

								UPDATE g
								SET g.GeoSpaDesc = @map_geospadesc, g.MapTypeID = @map_maptypeid, g.BaseMapID = @map_basemapid
									, g.IsPublic = @map_ispublic, g.DisplayOrder = @map_displayorder, g.GeoSpaKey = @map_geospakey, g.InitialExtent = @map_initialextent
									, g.ChangedByUserID = @securityUserID, ChangeDate = GETDATE(), g.ParentGeoSpaID = @map_parentgeospaid
									, g.MapConfig = @map_mapconfig, g.AssetID = @map_newAssetID, g.TimeBaseID = @map_timebaseid, g.AggregationTypeID = @map_aggregationtypeid
									, g.Decimate = @map_decimate, g.DescendantsInherit = @map_inherit
								FROM GeoSpa.tGeoSpa g
								WHERE g.GeoSpaID = @map_newMapID
								-- rebuild GeoSpaCategoryMap
								-- we need to delete any entry in the tGeoSpaCategoryMap and re-add with the correct Category ID, if any.
								DELETE c FROM [GeoSpa].[tGeoSpaCategoryMap] c WHERE c.GeoSpaID = @map_newMapID
								IF @map_categories IS NOT NULL
								BEGIN
									INSERT INTO [GeoSpa].[tGeoSpaCategoryMap]
										([CategoryID], [GeoSpaID])
									SELECT DISTINCT m.CategoryID, @map_newMapID
									FROM @mapCategories m
									CROSS APPLY [Base].[ufnSplitString](@map_categories, ';') ufn
									WHERE ufn.string = m.CategoryName
								END
							END
							ELSE
							BEGIN
								INSERT INTO [GeoSpa].[tGeoSpa]
									([Title],[GeoSpaDesc],[MapTypeID],[BaseMapID],[IsPublic],[DisplayOrder],[GeoSpaKey],[InitialExtent],[CreatedByUserID],[ChangedByUserID],[CreateDate],[ChangeDate]
									,[ParentGeoSpaID],[MapConfig],[AssetID],[TimeBaseID],[AggregationTypeID],[Decimate],[DescendantsInherit])
								VALUES
									(@map_title, @map_geospadesc, @map_maptypeid, @map_basemapid, @map_ispublic, @map_displayorder, @map_geospakey, @map_initialextent, @securityUserID, @securityUserID, GETDATE(), GETDATE()
									, @map_parentgeospaid, @map_mapconfig, @map_newAssetID, @map_timebaseid, @map_aggregationtypeid, @map_decimate, @map_inherit)

								SET @map_newMapID = @@IDENTITY
								IF @map_categories IS NOT NULL
								BEGIN
									INSERT INTO [GeoSpa].[tGeoSpaCategoryMap]
										([CategoryID], [GeoSpaID])
									SELECT DISTINCT m.CategoryID, @map_newMapID
									FROM @mapCategories m
									CROSS APPLY [Base].[ufnSplitString](@map_categories, ';') ufn
									WHERE ufn.string = m.CategoryName
								END
							END

							IF @layers = 1 AND @map_maptypeid <> 3
							BEGIN

								DECLARE maplayer_cursor CURSOR
								FOR SELECT ID, AssetClassTypeID, Title, GeoSpaLayerDesc, [Geometry], Symbology, DisplayZoomMin, DisplayZoomMax
								FROM ETL.tCapPriorGeoSpaLayers
								WHERE BatchID = @batchId AND MapID = @map_MapID

								OPEN maplayer_cursor
								FETCH NEXT FROM maplayer_cursor
								INTO @layer_ID, @layer_assetclasstypeid, @layer_title,@layer_geospalayerdesc, @layer_geometry, @layer_symbology, @layer_zoom_min, @layer_zoom_max
								WHILE @@FETCH_STATUS = 0
								BEGIN

									INSERT INTO [GeoSpa].[tGeoSpaLayer]
										([GeoSpaID],[Title],[GeoSpaLayerDesc],[IsPublic],[DisplayOrder],[DisplayZoomMin],[DisplayZoomMax],[CreatedByUserID],[ChangedByUserID],[CreateDate],[ChangeDate])
									VALUES
										(@map_newMapID, @layer_title, @layer_geospalayerdesc, 1, NULL, @layer_zoom_min, @layer_zoom_max, @securityUserID, @securityUserID , GETDATE(), GETDATE())
									SET @layer_newLayerID = @@IDENTITY;

									DECLARE @sourceIDGUID uniqueidentifier = newid();

									INSERT INTO [GeoSpa].[tGeoSpaSql]
										([ContentID],[TableName],[GeoSpaFK],[TablePK],[IncludeFields],[ExcludeFields])
									VALUES
										(@sourceIDGUID, '[Asset].[tAssetAttribute]', 'AssetID', 'AssetID', NULL, NULL)

									INSERT INTO GeoSpa.tGeoSpaSource
										(GeoSpaSourceTypeID, GeoSpaSourceGUID)
									VALUES
										(2, @sourceIDGUID)
									DECLARE @sourceID int = @@IDENTITY;

									DECLARE @attSourceIDGUID uniqueidentifier = newid();

									INSERT INTO [GeoSpa].[tAttributesSql]
										([ContentID],[TableName],[GeoSpaFK],[TablePK],[IncludeFields],[ExcludeFields])
									VALUES
										(@attSourceIDGUID, '[UIConfig].[tAssetProperties]', 'AssetID', 'AssetID', NULL, NULL)

									INSERT INTO GeoSpa.tAttributeSource
										(AttributeSourceTypeID, AttributeSourceGUID)
									VALUES
										(1, @attSourceIDGUID)
									DECLARE @attributesourceID int = @@IDENTITY;

									INSERT INTO [GeoSpa].[tGeoSpaAssetGroup]
										([AssetClassTypeID],[GeometryTypeID],[DisplayOrder],[GeoSpaSourceID],[Icon],[SelectedIcon],[SymbologyProperties],[AttributeSourceID])
									VALUES
										(@layer_assetclasstypeid, @layer_geometry, NULL, @sourceID, NULL, NULL, @layer_symbology, @attributesourceID)
									DECLARE @groupID int = @@IDENTITY;

									INSERT INTO GeoSpa.tGeoSpaLayerGeoSpaAssetGroupMap
										(GeoSpaLayerID, GeoSpaAssetGroupID)
									VALUES(@layer_newLayerID, @groupID)


									FETCH NEXT FROM maplayer_cursor
									INTO @layer_ID, @layer_assetclasstypeid, @layer_title,@layer_geospalayerdesc, @layer_geometry, @layer_symbology, @layer_zoom_min, @layer_zoom_max
								END
								CLOSE maplayer_cursor
								DEALLOCATE maplayer_cursor

							END



							IF @rasters = 1 AND @map_maptypeid = 3
							BEGIN

								--Raster Map Layers and Widget Imports
								DECLARE rasterlayer_cursor CURSOR
								FOR SELECT StaticRasterLayerID, RasterContentID
								FROM ETL.tCapPriorGeoSpaStaticRasterLayers
								WHERE BatchID = @batchId AND MapID = @map_MapID

								OPEN rasterlayer_cursor
								FETCH NEXT FROM rasterlayer_cursor
								INTO @rasterlayer_ID, @rasterlayer_ContentID

								DECLARE @rasterlayer_Status int
								SET @rasterlayer_Status = @@FETCH_STATUS
								WHILE @rasterlayer_Status = 0
								BEGIN

									INSERT INTO [GeoSpa].[tStaticRasterLayer]
										([GeoSpaID]
										,[RasterContentID])
									VALUES
										(@map_newMapID
										, @rasterlayer_ContentID)
									SET @rasterlayer_newRasterLayerID = @@IDENTITY;

									--Insert Widgets associated with the layer
									DECLARE rasterwidget_cursor CURSOR
									FOR SELECT StaticRasterGroupID, DisplayOrder
									FROM ETL.tCapPriorGeoSpaStaticRasterWidgets
									WHERE BatchID = @batchId AND StaticRasterLayerID = @rasterlayer_ID
									GROUP BY StaticRasterGroupID, DisplayOrder

									OPEN rasterwidget_cursor
									FETCH NEXT FROM rasterwidget_cursor
									INTO @rasterwidget_GroupID, @rasterwidget_DisplayOrder

									DECLARE @rasterwidget_Status int
									SET @rasterwidget_Status = @@FETCH_STATUS
									WHILE @rasterwidget_Status = 0
									BEGIN

										INSERT INTO [GeoSpa].[tStaticRasterGroup]
											([StaticRasterLayerID]
											,[DisplayOrder])
										VALUES
											(@rasterlayer_newRasterLayerID
												, @rasterwidget_DisplayOrder)
										SET @rasterwidget_newRasterGroupID = @@IDENTITY;

										IF OBJECT_ID('tempdb..#TMP_RasterWidgets') IS NOT NULL
											DROP TABLE #TMP_RasterWidgets

										SELECT StaticRasterGroupID AS StaticRasterGroupID, WidgetTypeID AS WidgetTypeID, RelativeXPosition AS RelativeXPosition, RelativeYPosition AS RelativeYPosition,
											a.AssetID AS AssetID, AssetVariableTypeTagMapID AS AssetVariableTypeTagMapID
										INTO #TMP_RasterWidgets
										FROM [ETL].[tCapPriorGeoSpaStaticRasterWidgets] wt LEFT JOIN #tmp_capprior_assets a ON a.ProjectNumber = wt.AssetID
										WHERE BatchID = @batchId AND StaticRasterLayerID = @rasterlayer_ID AND StaticRasterGroupID = @rasterwidget_GroupID


										--Update StaticRasterGroupID
										UPDATE #TMP_RasterWidgets SET StaticRasterGroupID = @rasterwidget_newRasterGroupID

										INSERT INTO [GeoSpa].[tStaticRasterWidget]
											([StaticRasterGroupID]
											,[WidgetTypeID]
											,[RelativeXPosition]
											,[RelativeYPosition]
											,[AssetID]
											,[AssetVariableTypeTagMapID])
										SELECT *
										FROM #TMP_RasterWidgets

										DROP TABLE #TMP_RasterWidgets

										FETCH NEXT FROM rasterwidget_cursor
										INTO @rasterwidget_GroupID, @rasterwidget_DisplayOrder
										SET @rasterwidget_Status = @@FETCH_STATUS

									END
									CLOSE rasterwidget_cursor
									DEALLOCATE rasterwidget_cursor

									FETCH NEXT FROM rasterlayer_cursor
									INTO @rasterlayer_ID, @rasterlayer_ContentID
									SET @rasterlayer_Status = @@FETCH_STATUS
								END
								CLOSE rasterlayer_cursor
								DEALLOCATE rasterlayer_cursor

							END
													   							 
							FETCH NEXT FROM map_cursor
						INTO @map_ID,@map_MapID,@map_title,@map_geospadesc,@map_maptypeid,@map_basemapid,@map_ispublic,@map_displayorder,@map_geospakey,@map_initialextent,@map_parentgeospaid,@map_mapconfig,@map_projectNumber,@map_timebaseid,@map_aggregationtypeid,@map_decimate,@map_categories,@map_inherit
						END

						CLOSE map_cursor
						DEALLOCATE map_cursor

					END
				END

				IF @trends = 1
				BEGIN
					DECLARE @trend_ID int;
					DECLARE @trend_TrendID int;
					DECLARE @trend_NewTrendID int;
					DECLARE @trend_title nvarchar(255);
					DECLARE @trend_projectNumber int;
					DECLARE @trend_newAssetID int;
					DECLARE @trend_displayOrder int;
					DECLARE @trend_trendDesc nvarchar(255);
					DECLARE @trend_legendVisible bit;
					DECLARE @trend_legendDock tinyint;
					DECLARE @trend_legendContentLayout tinyint;
					DECLARE @trend_isCustom bit;
					DECLARE @trend_isPublic bit;
					DECLARE @trend_chartTypeID int;
					DECLARE @trend_filterMin float;
					DECLARE @trend_filterMax float;
					DECLARE @trend_flatlineThreshold float;
					DECLARE @trend_exclude nvarchar(512);
					DECLARE @trend_pinTypeID int;
					DECLARE @trend_trendSource nvarchar(255);
					DECLARE @trend_projectId nvarchar(max);
					DECLARE @trend_path nvarchar(max);
					DECLARE @trend_math nvarchar(max);
					DECLARE @trend_summaryTypeID int;

					DECLARE @axis_axis int;
					DECLARE @axis_title nvarchar(255);
					DECLARE @axis_position tinyint;
					DECLARE @axis_min float;
					DECLARE @axis_max float;
					DECLARE @axis_step float;
					DECLARE @axis_minorStep float;
					DECLARE @axis_isDefault bit;
					DECLARE @axis_gridLine bit;

					DECLARE @series_tagID int;
					DECLARE @series_variableID int;
					DECLARE @series_isMain bit;
					DECLARE @series_displayOrder int;
					DECLARE @series_axis int;
					DECLARE @series_displayText nvarchar(255);
					DECLARE @series_useDisplayText bit;
					DECLARE @series_valueTypeID int;
					DECLARE @series_isXAxis bit;
					DECLARE @series_isFiltered bit;
					DECLARE @series_filterMin float;
					DECLARE @series_filterMax float;
					DECLARE @series_showBestFitLine bit;
					DECLARE @series_assetClassTypeID int;
					DECLARE @series_scaleMin float;
					DECLARE @series_scaleMax float;
					DECLARE @series_scaleDecimals int;
					DECLARE @series_includeSelf bit;
					DECLARE @series_includeChildren bit;
					DECLARE @series_includeDescendants bit;
					DECLARE @series_includeParents bit;
					DECLARE @series_includeSiblings bit;
					DECLARE @series_includeAncestors bit;
					DECLARE @series_includeCousins bit;
					DECLARE @series_includeSecondCousins bit;
					DECLARE @series_includeUnit bit;
					DECLARE @series_includeUnitDescendants bit;
					DECLARE @series_includeAllAssets bit;
					DECLARE @series_chartTypeID int;
					DECLARE @series_isZAxis bit;
					DECLARE @series_flatlineThreshold float;
					DECLARE @series_exclude nvarchar(512);
					DECLARE @series_color nvarchar(56);
					DECLARE @series_stack nvarchar(56);
					DECLARE @series_stackType nvarchar(56);
					DECLARE @series_applyToAll bit;
					DECLARE @series_archive nvarchar(255);
					DECLARE @series_useGlobalDataRetrieval bit;
					DECLARE @series_summaryTypeID int;

					DECLARE trend_cursor CURSOR
					FOR
						SELECT e.ID, e.TrendID, e.Title, e.ProjectNumber, e.DisplayOrder, e.TrendDesc, e.LegendVisible, e.LegendDock, e.LegendContentLayout
							, e.IsCustom, e.IsPublic, e.ChartTypeID, e.FilterMin, e.FilterMax, e.FlatlineThreshold, e.Exclude, e.PinTypeID, e.TrendSource
							, e.ProjectId, e.[Path], e.Math, e.SummaryTypeID
					FROM ETL.tCapPriorTrend e JOIN #tmp_capprior_assets x ON e.ProjectNumber = x.ProjectNumber
					WHERE e.BatchID = @batchID

					OPEN trend_cursor
					FETCH next FROM trend_cursor
					INTO @trend_ID, @trend_TrendID, @trend_title, @trend_projectNumber, @trend_displayOrder, @trend_trendDesc, @trend_legendVisible, @trend_legendDock, @trend_legendContentLayout
						, @trend_isCustom, @trend_isPublic, @trend_chartTypeID, @trend_filterMin, @trend_filterMax, @trend_flatlineThreshold, @trend_exclude
						, @trend_pinTypeID, @trend_trendSource, @trend_projectId, @trend_path, @trend_math, @trend_summaryTypeID


					WHILE @@FETCH_STATUS = 0
					BEGIN

						SELECT @trend_newAssetID = AssetID
						FROM #tmp_capprior_assets
						WHERE ProjectNumber = @trend_projectNumber;

						INSERT INTO ProcessData.tTrend
							(TrendDesc, Title, LegendVisible, LegendDock, LegendContentLayout, IsCustom, CreatedBy, ChangedBy, CreateDate
							, ChangeDate, IsPublic, DisplayOrder, TrendKey, ParentTrendID, ChartTypeID, FilterMin, FilterMax, FlatlineThreshold, Exclude
							, PinTypeID, TrendSource, ProjectId, [Path], Math, SummaryTypeID)
						VALUES
							(@trend_trendDesc, @trend_title, @trend_legendVisible, @trend_legendDock, @trend_legendContentLayout, @trend_isCustom, @createdBy
							, @createdBy, GETDATE(), GETDATE(), @trend_isPublic, @trend_displayOrder, NULL, NULL, @trend_chartTypeID, @trend_filterMin
							, @trend_filterMax, @trend_flatlineThreshold, @trend_exclude, @trend_pinTypeID, @trend_trendSource, @trend_projectId, @trend_path
							, @trend_math, @trend_summaryTypeID);

						SET @trend_newTrendID = @@IDENTITY;

						INSERT INTO ProcessData.tAssetTrendMap
							(AssetID, TrendID)
						VALUES
							(@trend_newAssetID, @trend_newTrendID);

						IF @trendSeries = 1
						BEGIN
							IF @tmpTagsExist = 0
							BEGIN
								SET @errorMsg = 'Error in importing TrendSeries due to missing PDTags'
								RAISERROR (@errorMsg,13, -1, -1)
								RETURN @returnValue
							END

							DECLARE trendaxis_cursor CURSOR
							FOR SELECT Axis, Title, Position, [Min], [Max], Step, MinorStep, IsDefault, GridLine
							FROM ETL.tCapPriorTrendAxis
							WHERE BatchID = @batchId AND TrendID = @trend_trendID;

							OPEN trendaxis_cursor
							FETCH next FROM trendaxis_cursor
							INTO @axis_axis, @axis_title, @axis_position, @axis_min, @axis_max, @axis_step, @axis_minorStep, @axis_isDefault, @axis_gridLine

							WHILE @@FETCH_STATUS = 0
							BEGIN

								INSERT INTO ProcessData.tTrendAxis
									(PDTrendID, Axis, Title, Position, [Min], [Max], Step, MinorStep, IsDefault
									, CreatedBy, ChangedBy, CreateDate, ChangeDate, GridLine)
								VALUES
									(@trend_newTrendID, @axis_axis, @axis_title, @axis_position, @axis_min, @axis_max, @axis_step, @axis_minorStep
									, @axis_isDefault, @createdBy, @createdBy, GETDATE(), GETDATE(), @axis_gridLine)

								FETCH next FROM trendaxis_cursor
								INTO @axis_axis, @axis_title, @axis_position, @axis_min, @axis_max, @axis_step, @axis_minorStep, @axis_isDefault, @axis_gridLine
							END

							CLOSE trendaxis_cursor
							DEALLOCATE trendaxis_cursor

							-- if ther user did not enter a value type for the trend, then just match it with the corresponding tag id
							--
							-- make trend series value type always NULL so that trend series won't be missed when asset variable type tag map doesn't have that exact value type
							--
							DECLARE trendseries_cursor CURSOR
							FOR SELECT b.PDTagID, VariableID, IsMain, DisplayOrder, Axis, DisplayText, UseDisplayText, NULL, IsXAxis, IsFiltered, FilterMin
								, FilterMax, ShowBestFitLine, AssetClassTypeID, ScaleMin, ScaleMax, ScaleDecimals, IncludeSelf, IncludeChildren, IncludeDescendants
								, IncludeParents, IncludeSiblings, IncludeAncestors, IncludeCousins, IncludeSecondCousins, IncludeUnit, IncludeUnitDescendants
								, IncludeAllAssets, ChartTypeID, IsZAxis, FlatlineThreshold, Exclude, Color, Stack, StackType, ApplyToAll, Archive
								, UseGlobalDataRetrieval, SummaryTypeID
							FROM ETL.tCapPriorTrendSeries a JOIN #tmpTags b ON a.TagID = b.TagID AND (a.ValueTypeID IS NULL OR a.ValueTypeID = ISNULL(b.ValueTypeID,23))
							WHERE BatchID = @batchId AND TrendID = @trend_trendID;

							OPEN trendseries_cursor
							FETCH next FROM trendseries_cursor
							INTO @series_tagID, @series_variableID, @series_IsMain, @series_displayOrder, @series_axis, @series_displayText, @series_useDisplayText
								, @series_valueTypeID, @series_isXAxis, @series_isFiltered, @series_filterMin, @series_filterMax, @series_showBestFitLine
								, @series_assetClassTypeID, @series_scaleMin, @series_scaleMax, @series_scaleDecimals, @series_includeSelf, @series_includeChildren
								, @series_includeDescendants, @series_includeParents, @series_includeSiblings, @series_includeAncestors, @series_includeCousins
								, @series_includeSecondCousins, @series_includeUnit, @series_includeUnitDescendants, @series_includeAllAssets, @series_chartTypeID
								, @series_isZAxis, @series_flatlineThreshold, @series_exclude, @series_color, @series_stack, @series_stackType, @series_applyToAll
								, @series_archive, @series_useGlobalDataRetrieval, @series_summaryTypeID

							WHILE @@FETCH_STATUS = 0
							BEGIN

								INSERT INTO ProcessData.tTrendSeries
									(PDTrendID, PDTagID, PDVariableID, IsMain, DisplayOrder, Axis, DisplayText, CreatedBy
									, ChangedBy, CreateDate, ChangeDate, UseDisplayText, ValueTypeID, IsXAxis, IsFiltered, FilterMin, FilterMax
									, ShowBestFitLine, AssetClassTypeID, ScaleMin, ScaleMax, ScaleDecimals, IncludeSelf, IncludeChildren
									, IncludeDescendants, IncludeParents, IncludeSiblings, IncludeAncestors, IncludeCousins, IncludeSecondCousins, IncludeUnit
									, IncludeUnitDescendants, IncludeAllAssets, ChartTypeID, IsZAxis, FlatlineThreshold, Exclude, Color, Stack, StackType
									, ApplyToAll, Archive, UseGlobalDataRetrieval, SummaryTypeID)
								VALUES
									(@trend_NewTrendID, @series_tagID, @series_variableID, @series_isMain, @series_displayOrder, @series_axis
									, @series_displayText, @createdBy, @createdBy, GETDATE(), GETDATE(), @series_useDisplayText, @series_valueTypeID
									, @series_isXAxis, @series_isFiltered, @series_filterMin, @series_filterMax, @series_showBestFitLine
									, @series_assetClassTypeID, @series_scaleMin, @series_scaleMax, @series_scaleDecimals, @series_includeSelf
									, @series_includeChildren, @series_includeDescendants, @series_includeParents, @series_includeSiblings, @series_includeAncestors
									, @series_includeCousins, @series_includeSecondCousins, @series_includeUnit, @series_includeUnitDescendants
									, @series_includeAllAssets, @series_chartTypeID, @series_isZAxis, @series_flatlineThreshold, @series_exclude
									, @series_color, @series_stack, @series_stackType, @series_applyToAll, @series_archive, @series_useGlobalDataRetrieval
									, @series_summaryTypeID)

								FETCH next FROM trendseries_cursor
							INTO @series_tagID, @series_variableID, @series_IsMain, @series_displayOrder, @series_axis, @series_displayText, @series_useDisplayText
								, @series_valueTypeID, @series_isXAxis, @series_isFiltered, @series_filterMin, @series_filterMax, @series_showBestFitLine
								, @series_assetClassTypeID, @series_scaleMin, @series_scaleMax, @series_scaleDecimals, @series_includeSelf, @series_includeChildren
								, @series_includeDescendants, @series_includeParents, @series_includeSiblings, @series_includeAncestors, @series_includeCousins
								, @series_includeSecondCousins, @series_includeUnit, @series_includeUnitDescendants, @series_includeAllAssets, @series_chartTypeID
								, @series_isZAxis, @series_flatlineThreshold, @series_exclude, @series_color, @series_stack, @series_stackType, @series_applyToAll
								, @series_archive, @series_useGlobalDataRetrieval, @series_summaryTypeID
							END

							CLOSE trendseries_cursor
							DEALLOCATE trendseries_cursor
						END

						FETCH next FROM trend_cursor
					INTO @trend_ID, @trend_TrendID, @trend_title, @trend_projectNumber, @trend_displayOrder, @trend_trendDesc, @trend_legendVisible, @trend_legendDock, @trend_legendContentLayout
						, @trend_isCustom, @trend_isPublic, @trend_chartTypeID, @trend_filterMin, @trend_filterMax, @trend_flatlineThreshold, @trend_exclude
						, @trend_pinTypeID, @trend_trendSource, @trend_projectId, @trend_path, @trend_math, @trend_summaryTypeID

					END

					CLOSE trend_cursor
					DEALLOCATE trend_cursor
				END

				IF @attachments = 1
				BEGIN
					/***************** START VALIDATION *****************/
					-- check to make sure everyone has a guid put in for the file
					IF EXISTS (SELECT TOP 1
						*
					FROM ETL.tCapPriorAssetAttachments
					WHERE BatchID = @batchId AND ImportedFileGuid IS NULL)
					BEGIN
						RAISERROR('Unable to import, one or more attachments were not successfully imported and saved', 13, -1, -1)
						RETURN @returnValue
					END

					-- check for bad guids
					DECLARE @notFoundAttachmentGuidsForUpdate TABLE ([ID] uniqueidentifier NOT NULL);
					INSERT INTO @notFoundAttachmentGuidsForUpdate
					SELECT UpdatingGuid
					FROM ETL.tCapPriorAssetAttachments
					WHERE BatchID = @batchId AND UpdatingGuid IS NOT NULL
					DELETE t FROM @notFoundAttachmentGuidsForUpdate t INNER JOIN Asset.tAssetAttachment a ON a.AssetAttachmentID = t.ID
					IF EXISTS (SELECT TOP 1
						*
					FROM @notFoundAttachmentGuidsForUpdate)
					BEGIN
						DECLARE @missingAttachmentGuid varchar(MAX)
						SELECT TOP 1
							@missingAttachmentGuid = ID
						FROM @notFoundAttachmentGuidsForUpdate
						SET @missingAttachmentGuid = 'An attachment ''Update Guid'' was invalid. Invalid guid: ' + @missingAttachmentGuid
						RAISERROR(@missingAttachmentGuid, 13, -1, -1)
						RETURN @returnValue
					END
					-- now check for a guid put on the wrong asset id
					INSERT INTO @notFoundAttachmentGuidsForUpdate
					SELECT UpdatingGuid
					FROM ETL.tCapPriorAssetAttachments
					WHERE BatchID = @batchId AND UpdatingGuid IS NOT NULL
					DELETE t FROM @notFoundAttachmentGuidsForUpdate t
						INNER JOIN ETL.tCapPriorAssetAttachments a ON a.UpdatingGuid = CONVERT(varchar(50), t.ID)
						INNER JOIN #tmp_capprior_assets ta ON ta.ProjectNumber = a.ProjectNumber
						INNER JOIN Asset.tAssetAttachment aa ON aa.AssetAttachmentID = t.ID
					WHERE aa.AssetID = ta.AssetID
					IF EXISTS (SELECT TOP 1
						*
					FROM @notFoundAttachmentGuidsForUpdate)
					BEGIN
						DECLARE @attachmentWrongAssetIDGuid varchar(MAX)
						SELECT TOP 1
							@attachmentWrongAssetIDGuid = ID
						FROM @notFoundAttachmentGuidsForUpdate
						SET @attachmentWrongAssetIDGuid = 'An attachment ''Update Guid'' points to the wrong asset. Invalid guid: ' + @attachmentWrongAssetIDGuid
						RAISERROR(@attachmentWrongAssetIDGuid, 13, -1, -1)
						RETURN @returnValue
					END
					/***************** END VALIDATION *******************/

					/**************** START COPY *****************/
					-- first, copy the data over into Base.tImage
					INSERT INTO Base.tImage
						(ImageUniqueId, [Name], IsSql, IsDisk, IsAWS, IsAzure, CreatedBy, ChangedBy, CreateDate, ChangeDate, OwningAssetId, [Format], RepositoryName, [UploadComplete])
					SELECT
						a.ImportedFileGuid, a.Title,
						0 /*IsSql*/, 0 /*IsDisk*/, 1 /*IsAWS*/, 0 /*IsAzure*/,
						@createdBy, @createdBy,
						GETDATE(), GETDATE(),
						b.AssetID,
						a.ImportedFileFormat, a.ImportedFileRepositoryName,
						1
					FROM ETL.tCapPriorAssetAttachments a
						INNER JOIN #tmp_capprior_assets b ON b.ProjectNumber = a.ProjectNumber
					WHERE a.BatchID = @batchId

					-- create the new guids
					DECLARE @assetAttachmentEtlToActualMap TABLE([EtlId]      int              NULL,
						[ActualGuid] uniqueidentifier NULL)
					INSERT INTO @assetAttachmentEtlToActualMap
						(EtlId, ActualGuid)
					SELECT ID, NEWID()
					FROM ETL.tCapPriorAssetAttachments
					WHERE BatchID = @batchId
					-- only doing new attachments right now

					-- then copy all attachments, even updates get a new record. we'll handle pointing existing attachments to the new updates below...
					INSERT INTO Asset.tAssetAttachment
						(
						AssetAttachmentID, AssetID, CreateDate, ChangeDate, CreatedByUserID, ChangedByUserID, ContentID,
						DisplayInstructions, AttachmentType, Title, Caption, DisplayOrder, Favorite
						)
					SELECT
						tmap.ActualGuid, amap.AssetID,
						GETDATE(), GETDATE(), @securityUserID, @securityUserID,
						t.ImportedFileGuid,
						NULL, -- display instructions
						t.AttachmentTypeID,
						t.Title,
						t.Caption,
						t.DisplayOrder,
						t.Favorite
					FROM ETL.tCapPriorAssetAttachments t
						INNER JOIN @assetAttachmentEtlToActualMap tmap ON tmap.EtlId = t.ID
						INNER JOIN #tmp_capprior_assets amap ON amap.ProjectNumber = t.ProjectNumber
					WHERE t.BatchID = @batchId

					-- create a list of older versions of attachments that need to have their Head mapped
					DECLARE @attachmentPreviousVersionMap TABLE ([NewId]             uniqueidentifier,
						[PreviousVersionId] uniqueidentifier);
					-- user may have provided stale guid, so we need to make sure to cover all bases.
					-- case 1: user provides head guid
					-- case 2: user provides stale guid
					-- insert the head guid for case 1 and case 2, then insert all their previous versions
					INSERT INTO @attachmentPreviousVersionMap
					SELECT map.ActualGuid, a.UpdatingGuid
					-- previous head version (head = null)
					FROM ETL.tCapPriorAssetAttachments a
						INNER JOIN @assetAttachmentEtlToActualMap map ON map.EtlId = a.ID
						INNER JOIN Asset.tAssetAttachment aa ON aa.AssetAttachmentID = a.UpdatingGuid
					WHERE a.BatchID = @batchId AND a.UpdatingGuid IS NOT NULL AND aa.Head IS NULL
					-- head version
					-- now the stale versions
					INSERT INTO @attachmentPreviousVersionMap
					SELECT map.ActualGuid, aa.Head
					-- previous STALE version (head <> null)
					FROM ETL.tCapPriorAssetAttachments a
						INNER JOIN @assetAttachmentEtlToActualMap map ON map.EtlId = a.ID
						INNER JOIN Asset.tAssetAttachment aa ON aa.AssetAttachmentID = a.UpdatingGuid
					-- only 1 record
					WHERE a.BatchID = @batchId AND a.UpdatingGuid IS NOT NULL AND aa.Head IS NOT NULL
					-- a stale version
					-- last, insert all their children
					INSERT INTO @attachmentPreviousVersionMap
					SELECT pMap.NewId, aa.AssetAttachmentID
					-- previous versions (head = UpdatingGuid)
					FROM @attachmentPreviousVersionMap pMap
						INNER JOIN Asset.tAssetAttachment aa ON pMap.PreviousVersionId = aa.Head

					-- update existing previous versions to point at head
					UPDATE aaa
					SET
						Head = vmap.NewId
					FROM Asset.tAssetAttachment aaa
						INNER JOIN @attachmentPreviousVersionMap vmap ON vmap.PreviousVersionId = aaa.AssetAttachmentID
					/**************** END COPY *****************/

					/*************** CATEGORIES *****************/
					DECLARE @attachmentCategories TABLE (
						AttachmentGuid uniqueidentifier,
						CategoryID     int              NULL,
						CategoryName   varchar(255)     NULL
					)
					INSERT INTO @attachmentCategories
						(AttachmentGuid, CategoryName)
					SELECT map.ActualGuid, ufn.string
					FROM ETL.tCapPriorAssetAttachments a
						INNER JOIN @assetAttachmentEtlToActualMap map ON map.EtlId = a.ID
					CROSS APPLY Base.ufnSplitString(a.Categories, ';') ufn
					WHERE a.BatchID = @batchId

					DECLARE @attachmentCategoryTypeIDs AS Base.tpIntList
					DECLARE @attachmentCategoryTypeID int
					SELECT TOP 1
						@attachmentCategoryTypeID = CategoryTypeID
					FROM Asset.tCategoryType
					WHERE CategoryTypeName LIKE 'Attachments'
					INSERT INTO @attachmentCategoryTypeIDs
					VALUES
						(@attachmentCategoryTypeID)

					-- check for empty categories
					IF EXISTS (SELECT TOP 1
						*
					FROM @attachmentCategories
					WHERE RTRIM(LTRIM(CategoryName)) IS NULL)
					BEGIN
						RAISERROR('Attachment categories cannot be null or whitespace', 13, 1)
						RETURN @returnValue
					END

					-- check for restrictions on imported categories
					DECLARE @attachmentRestrictedCategories TABLE ([Name] varchar(255) NULL)
					DECLARE @attachmentCategoryNameList AS Base.tpStringList
					INSERT INTO @attachmentCategoryNameList
					SELECT DISTINCT CategoryName
					FROM @attachmentCategories
					INSERT INTO @attachmentRestrictedCategories
						([Name])
					SELECT CategoryName
					FROM Asset.ufnWhichCategoriesAreProhibited(@firstImportedAssetID, @securityUserID, @attachmentCategoryNameList, @attachmentCategoryTypeID)
					IF EXISTS (SELECT TOP 1
						*
					FROM @attachmentRestrictedCategories)
					BEGIN
						DECLARE @attCategories varchar(MAX)
						SELECT @attCategories = COALESCE(@attCategories + ', ', '') + [Name]
						FROM (SELECT DISTINCT TOP 5
								[Name]
							FROM @attachmentRestrictedCategories) AS A
						IF (SELECT COUNT(*)
						FROM @attachmentRestrictedCategories) > 5
						BEGIN
							SET @attCategories = @attCategories +  ', ...'
						END
						SET @attCategories = 'User does not have permission to import categories: ' + @attCategories
						RAISERROR(@attCategories, 13, 1)
						RETURN @returnValue
					END

					-- check for restrictions on pre-existing categories
					DELETE FROM @attachmentCategoryNameList
					INSERT INTO @attachmentCategoryNameList
					SELECT cat.[Name]
					FROM @attachmentPreviousVersionMap map
						INNER JOIN Asset.tAssetAttachment aaa ON aaa.AssetAttachmentID = map.PreviousVersionId
						INNER JOIN Asset.tAssetAttachmentCategoryMap catMap ON catMap.AssetAttachmentID = aaa.AssetAttachmentID
						INNER JOIN Asset.tCategory cat ON cat.CategoryID = catMap.CategoryID

					INSERT INTO @attachmentRestrictedCategories
						([Name])
					SELECT CategoryName
					FROM Asset.ufnWhichCategoriesAreProhibited(@firstImportedAssetID, @securityUserID, @attachmentCategoryNameList, @attachmentCategoryTypeID)

					IF EXISTS (SELECT TOP 1
						*
					FROM @attachmentRestrictedCategories)
					BEGIN
						RAISERROR('Could not update existing Attachment, user does not have sufficient permission to its categories', 13, 1)
						RETURN @returnValue
					END

					-- proceed with figuring out which categories we need to create
					UPDATE acat
					SET
						CategoryID = ufn.CategoryID
					FROM @attachmentCategories acat
						INNER JOIN Asset.ufnGetAvailableAssetCategories(@securityUserID, @parentAssetId, @attachmentCategoryTypeID) ufn ON ufn.[Name] = acat.CategoryName

					WHILE EXISTS (SELECT TOP 1
						*
					FROM @attachmentCategories
					WHERE CategoryID IS NULL)
					BEGIN
						DECLARE @attCatName varchar(255)
						DECLARE @attCatID int
						SELECT TOP 1
							@attCatName = CategoryName
						FROM @attachmentCategories
						WHERE CategoryID IS NULL
						EXEC Asset.spInsertCategory @securityUserID, @firstImportedAssetID, @attCatName, @attachmentCategoryTypeIDs, 0, @attCatID OUTPUT
						UPDATE @attachmentCategories SET CategoryID = @attCatID WHERE CategoryName = @attCatName
					END

					DECLARE @attachmentGuidIntList AS Base.tpGuidIntList
					INSERT INTO @attachmentGuidIntList
					SELECT AttachmentGuid, CategoryID
					FROM @attachmentCategories
					EXEC Asset.spSetCategoriesForAssetAttachments @attachmentGuidIntList
					/************* END CATEGORIES ***************/

					/*************** TAGS *****************/
					-- Only the current version gets the Tags.
					-- extract the tags from the list
					DECLARE @attachmentTagKeywords TABLE ([AttachmentGuid] uniqueidentifier NULL,
						[KeywordID]      int              NULL,
						[KeywordName]    varchar(255)     NULL,
						[AssetID]        int              NULL)
					INSERT INTO @attachmentTagKeywords
						(AttachmentGuid, KeywordName, AssetID)
					SELECT map.ActualGuid, ufn.string, amap.AssetID
					FROM ETL.tCapPriorAssetAttachments a
						INNER JOIN @assetAttachmentEtlToActualMap map ON map.EtlId = a.ID
						INNER JOIN #tmp_capprior_assets amap ON amap.ProjectNumber = a.ProjectNumber
					CROSS APPLY Base.ufnSplitString(a.Tags, ';') ufn
					WHERE a.BatchID = @batchId

					-- check if any are reserved
					DECLARE @reservedKeywords Base.tpStringList
					INSERT INTO @reservedKeywords
						([value])
					SELECT tk.[Text]
					FROM Asset.ufnGetClientsForAsset(@firstImportedAssetID) ugcfa
						INNER JOIN Asset.tKeyword tk ON tk.ClientID = ugcfa.ClientID
						INNER JOIN @attachmentTagKeywords atk ON atk.KeywordName = tk.[Text]
					WHERE tk.SystemReserved = 1

					IF EXISTS (SELECT TOP 1
						*
					FROM @reservedKeywords)
					BEGIN
						DECLARE @attachmentKeywordsRestricted varchar(MAX)
						SELECT @attachmentKeywordsRestricted = COALESCE(@attachmentKeywordsRestricted + ', ', '') + [value]
						FROM (SELECT DISTINCT TOP 5
								[value]
							FROM @reservedKeywords) AS A
						IF (SELECT COUNT(*)
						FROM @reservedKeywords) > 5
						BEGIN
							SET @attachmentKeywordsRestricted = @attachmentKeywordsRestricted +  ', ...'
						END
						SET @attachmentKeywordsRestricted = 'Attachments cannot use reserved Tag keywords: ' + @attachmentKeywordsRestricted
						RAISERROR(@attachmentKeywordsRestricted, 13, 1)
						RETURN @returnValue
					END

					-- try to fill in tags with existing KeywordIDs.
					UPDATE atk
					SET
						KeywordID = k.KeywordID
					FROM @attachmentTagKeywords atk
						INNER JOIN Asset.ufnGetClientsForAsset(@firstImportedAssetID) ugcfa ON 1=1
						INNER JOIN Asset.tKeyword k ON k.[Text] = atk.KeywordName AND ugcfa.ClientID = k.ClientID

					-- remainder need to be added
					WHILE EXISTS (SELECT TOP 1
						*
					FROM @attachmentTagKeywords
					WHERE KeywordID IS NULL)
					BEGIN
						DECLARE @attachmentTagName varchar(255)
						DECLARE @attachmentAssetID int
						DECLARE @attachmentKeywordID int

						SELECT @attachmentTagName = KeywordName, @attachmentAssetID = AssetID
						FROM @attachmentTagKeywords
						WHERE KeywordID IS NULL

						EXEC Asset.spSaveTaggingKeyword @attachmentAssetID, @attachmentTagName, @securityUserID, @attachmentKeywordID OUTPUT

						UPDATE @attachmentTagKeywords SET KeywordID = @attachmentKeywordID WHERE KeywordName = @attachmentTagName
					END

					-- finally, push over into the map table
					DELETE kmap FROM Asset.tAssetAttachmentKeywordMap kmap
						INNER JOIN Asset.tAssetAttachment a ON a.AssetAttachmentID = kmap.AssetAttachmentID
						INNER JOIN @assetAttachmentEtlToActualMap emap ON emap.ActualGuid = a.AssetAttachmentID

					INSERT INTO Asset.tAssetAttachmentKeywordMap
						(AssetAttachmentID, KeywordID, CreatedBy, TagDate)
					SELECT AttachmentGuid, KeywordID, @securityUserID, GETDATE()
					FROM @attachmentTagKeywords

				/************* END TAGS ***************/
				END

			COMMIT TRANSACTION;
		END TRY
		BEGIN CATCH
			PRINT N'Script Failed, rolling back';
			ROLLBACK TRANSACTION
			PRINT ERROR_MESSAGE();
			SET @errorMsg = ERROR_MESSAGE()
			RAISERROR ( @errorMsg,13, -1, -1)
			RETURN @returnValue
		END CATCH

	-- save all
	COMMIT TRANSACTION;

	SELECT @returnValue = Assetid
	FROM #tmp_capprior_assets
	WHERE [level]=0

	RETURN @returnValue
END
GO

GRANT EXECUTE
    ON OBJECT::[ETL].[spImportCapPriorProjects] TO [TEUser]
    AS [dbo];
GO