﻿
CREATE PROCEDURE ETL.spSAM_SitesLoad
	@samSiteSetID int,
	@assetID int,
	@projectID int
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO SAM.tSAMSite
	(
	    SAMSiteSetID,
	    AssetID,
	    ProjectID,
	    SiteID,
	    Latitude,
	    Longitude,
	    SiteName,
	    BuildingAreaSqFt,
	    SectorTypeID,
	    WeatherFile,
	    CustomerRateClassID,
	    LoadProfile,
	    CustomerDemographic,
	    CustomerGroup,
	    AnnualConsumption,
	    GreenScore,
	    Greenergy1,
	    Greenergy2,
	    Greenergy6,
	    PaybackGroup,
	    AdoptionGroup,
	    ExistingPVInstalled,
	    ExistingPVkW
	)
	SELECT	@samSiteSetID, @assetID, @projectID, sd.SiteID, sd.Latitude, sd.Longitude, sd.SiteName, sd.BuildingAreaSqFt, 
			sd.SectorTypeID, sd.WeatherFile, sd.CustomerRateClassID,
			sd.LoadProfile, sd.CustomerDemographic, sd.CustomerGroup, sd.AnnualConsumption, sd.GreenScore, sd.Greenergy1, sd.Greenergy2, sd.Greenergy6, 
			sd.PaybackGroup, sd.AdoptionGroup, sd.ExistingPVInstalled, sd.ExistingPVkW
	FROM ETL.SmartDGSites sd

END
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spSAM_SitesLoad] TO [TEUser]
    AS [dbo];

