﻿
CREATE PROCEDURE ETL.spSAM_PlanesLoad
	@samPlaneSetID int,
	@assetID int,
	@projectID int
AS
BEGIN
	SET NOCOUNT ON;

	INSERT SAM.tSAMPlane
	(
		SAMPlaneSetID,
		AssetID,
		ProjectID,
		SiteID,
		PlaneID,
		PVAzimuth,
		PVTilt,
		PVShadingFactors,
		PVSystemCapacity,
		PVGroundCoverageRatio
		--ProductionProfile  --unused
	)
	SELECT	@samPlaneSetID, @assetID, @projectID, sd.SiteID, sd.PlaneID, sd.PVAzimuth,
			sd.PVTilt, sd.PVShadingFactors, sd.PVSystemCapacity,
			sd.PVGroundCoverageRatio
	FROM ETL.SmartDGPlanes sd

END
GO
GRANT EXECUTE
    ON OBJECT::[ETL].[spSAM_PlanesLoad] TO [TEUser]
    AS [dbo];

