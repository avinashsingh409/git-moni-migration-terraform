CREATE TABLE [AWS].[tRunningInstance] (
    [InstanceID]      NVARCHAR (255) NOT NULL,
    [StartTime]       DATETIME       NOT NULL,
    [Task]            NVARCHAR (50)  NULL,
    [LastTimeTouched] DATETIME       NULL,
    CONSTRAINT [PK_AWS_tRunningInstance] PRIMARY KEY CLUSTERED ([InstanceID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AWS', @level1type = N'TABLE', @level1name = N'tRunningInstance';

