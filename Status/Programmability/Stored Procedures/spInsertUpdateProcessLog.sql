CREATE PROCEDURE [Status].[spInsertUpdateProcessLog]
	@ProcessLogEntryID int,
	@SourceAssetID int,
	@ProcessServerID int,
	@ProcessTypeID int,
	@StartRunTime [datetime],
	@EndRunTime [datetime],
	@ProcessStatusTypeID int,
	@EarliestDataProcessTime datetime,
	@LatestDataProcessTime [datetime],
	@Log [nvarchar](max),
	@LogFileName [nvarchar] (255),
	@RecordsTransferred [int],
    @OutputProcessLogEntryID int OUTPUT
AS
BEGIN
	BEGIN TRANSACTION
	DECLARE @TotalStartRecords int, @TotalEndRecords int;
	DECLARE @ExpectedDifference int, @ActualDifference int;

	--lock the process log table for this transaction.
	SELECT @TotalStartRecords = COUNT(*) FROM [Status].[tProcessLog] WITH (TABLOCK, HOLDLOCK);

	

	SET @OutputProcessLogEntryID = -1
	IF (@ProcessLogEntryID <= 0)
	BEGIN
		--Make sure it's acceptable for us to actually start the indicated process.
		IF (@ProcessTypeID = 1) --ActualsCollector: don't run if another one or a MATSCacher is running.
		BEGIN
			IF (SELECT COUNT(*) FROM [Status].[tProcessLog] 
				WHERE ProcessTypeID in (1,2) 
				AND (ProcessStatusTypeID < 2 OR EndRunTime is null)
				) > 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN -1
			END
		END
		ELSE IF @ProcessTypeID = 2 --MATSCaching: don't run if actualsCollector or another MATSCacher is running.
		BEGIN
			IF (SELECT COUNT(*) FROM [Status].[tProcessLog] 
				WHERE ProcessTypeID in (1,2) 
				AND (ProcessStatusTypeID < 2 OR EndRunTime is null)
				) > 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN -1
			END
		END

		
		INSERT INTO [Status].[tProcessLog]
		(SourceAssetID, ProcessServerID, StartRunTime, EndRunTime, 
		ProcessStatusTypeID, EarliestDataProcessTime, LatestDataProcessTime,
		Log, LogFileName, RecordsTransferred, ProcessTypeID)
		values (@SourceAssetID, @ProcessServerID, @StartRunTime, @EndRunTime,
		@ProcessStatusTypeID, @EarliestDataProcessTime, @LatestDataProcessTime,
		@Log, @logFileName, @RecordsTransferred, @ProcessTypeID)
		SET @OutputProcessLogEntryID = @@IDENTITY

		SELECT @TotalEndRecords = COUNT(*) FROM [Status].[tProcessLog];
		SET @ExpectedDifference = 1;
	END
	ELSE
	BEGIN
	  UPDATE [Status].[tProcessLog]
	  set SourceAssetID = @SourceAssetID,
	      ProcessServerID = @ProcessServerID,
	      StartRunTime = @StartRunTime,
	      EndRunTime = @EndRunTime,
	      ProcessStatusTypeID = @ProcessStatusTypeID,
		  ProcessTypeID = @ProcessTypeID,
	      EarliestDataProcessTime = @EarliestDataProcessTime,
	      LatestDataProcessTime = @latestDataProcessTime,
	      Log = @Log,
	      LogFileName = @LogFileName,
	      RecordsTransferred = @RecordsTransferred
	   WHERE ProcessLogEntryID = @ProcessLogEntryID
	   SET @OutputProcessLogEntryID = @ProcessLogEntryID
	   
	  SELECT @TotalEndRecords = COUNT(*) FROM [Status].[tProcessLog];
	  SET @ExpectedDifference = 0;
	END

	--If I understood the locking logic correctly, this should never be false.
	--But in case I didn't, make sure nothing else got added beside our intended addition.
	IF @TotalEndRecords - @TotalStartRecords = @ExpectedDifference
	BEGIN
	COMMIT TRANSACTION
	RETURN 1
	END
	ELSE
	BEGIN
	ROLLBACK TRANSACTION
	set @OutputProcessLogEntryID = -1
	return -1
	END
END

GO
GRANT EXECUTE
    ON OBJECT::[Status].[spInsertUpdateProcessLog] TO [TEUser]
    AS [dbo];

