﻿
CREATE TABLE [Status].[tProcessType](
	[ProcessTypeID] [int] NOT NULL,
	[ProcessTypeAbbrev] [nvarchar](255) NOT NULL,
	[ProcessTypeDesc] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_tProcessType] PRIMARY KEY CLUSTERED 
(
	[ProcessTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]




