﻿
CREATE TABLE [Status].[tProcessStatusType] (
    [ProcessStatusTypeID]     INT            NOT NULL,
    [ProcessStatusTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [ProcessStatusTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tProcessStatusType] PRIMARY KEY CLUSTERED ([ProcessStatusTypeID] ASC)
);




