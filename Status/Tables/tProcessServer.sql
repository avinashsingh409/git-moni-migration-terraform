﻿
CREATE TABLE [Status].[tProcessServer](
	[ProcessServerID] [int] NOT NULL,
	[ProcessServerAbbrev] [nvarchar](255) NOT NULL,
	[ProcessServerDesc] [nvarchar](max) NOT NULL,
	[EndPoint] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tProcessServer] PRIMARY KEY CLUSTERED 
(
	[ProcessServerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



GO

