﻿
CREATE TABLE [Status].[tProcessLog](
	[ProcessLogEntryID] [int] IDENTITY(1,1) NOT NULL,
	[SourceAssetID] [int] NULL,
	[SourceAssetGlobalID] UniqueIdentifier NULL,
	[ProcessServerID] [int] NULL,
	[ProcessTypeID] [int] not null,
	[StartRunTime] [datetime] NULL,
	[EndRunTime] [datetime] NULL,
	[ProcessStatusTypeID] [int] NOT NULL,
	[EarliestDataProcessTime] [datetime] NULL,
	[LatestDataProcessTime] [datetime] NULL,
	[Log] [nvarchar](max) NULL,
	[LogFileName] [nvarchar] (255) NULL,
	[RecordsTransferred] [int] NULL,
 CONSTRAINT [PK_tProcess] PRIMARY KEY CLUSTERED 
(
	[ProcessLogEntryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



GO

/****** Object:  ForeignKey [FK_tProcess_tProcessServer]    Script Date: 03/03/2015 10:23:29 ******/
ALTER TABLE [Status].[tProcessLog]  ADD  CONSTRAINT [FK_tProcess_tProcessServer] FOREIGN KEY([ProcessServerID])
REFERENCES [Status].[tProcessServer] ([ProcessServerID])
GO

ALTER TABLE [Status].[tProcessLog] CHECK CONSTRAINT [FK_tProcess_tProcessServer]
GO


GO

/****** Object:  ForeignKey [FK_tProcess_tProcessStatus]    Script Date: 03/03/2015 10:23:29 ******/
ALTER TABLE [Status].[tProcessLog]  ADD  CONSTRAINT [FK_tProcess_tProcessStatusType] FOREIGN KEY([ProcessStatusTypeID])
REFERENCES [Status].[tProcessStatusType] ([ProcessStatusTypeID])
GO

ALTER TABLE [Status].[tProcessLog] CHECK CONSTRAINT [FK_tProcess_tProcessStatusType]
GO


GO

/****** Object:  ForeignKey [FK_tProcess_tProcessStatus]    Script Date: 03/03/2015 10:23:29 ******/
ALTER TABLE [Status].[tProcessLog]  ADD  CONSTRAINT [FK_tProcess_tProcessType] FOREIGN KEY([ProcessTypeID])
REFERENCES [Status].[tProcessType] ([ProcessTypeID])
GO

ALTER TABLE [Status].[tProcessLog] CHECK CONSTRAINT [FK_tProcess_tProcessType]
GO


GO

EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'Status', @level1type=N'TABLE',@level1name=N'tProcessLog'
GO

