CREATE PROCEDURE [Notification].[spDeleteNotification]
	@SecurityUserID INT,
	@NotificationID INT, 
	@DeleteCount INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [Notification].[tNotifications] WHERE NotificationID = @NotificationID AND SecurityUserID = @SecurityUserID;
	SET @DeleteCount = @@ROWCOUNT;
END
GO

GRANT EXECUTE
    ON OBJECT::[Notification].[spDeleteNotification]  TO [TEUser]
    AS [dbo];
GO