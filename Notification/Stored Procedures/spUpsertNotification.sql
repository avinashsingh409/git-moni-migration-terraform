CREATE PROCEDURE [Notification].[spUpsertNotification]
    @NotificationID INT = -1,
    @NotificationGroupID UNIQUEIDENTIFIER = NULL,
    @NotificationTypeID INT,
	@SecurityUserID INT,
	@Message NVARCHAR(MAX),
	@Options NVARCHAR(255) = NULL,
	@Result NVARCHAR(MAX),
	@Viewed BIT,
	@NewNotificationID INT OUTPUT
AS
BEGIN
	IF NOT EXISTS (SELECT TOP 1 Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID)
	BEGIN
        RAISERROR('User not found', 13, -1, -1)
	END

	--if the Notification ID passed in is positive, check if exist.
	IF @NotificationID IS NOT NULL AND @NotificationID > 0
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 NotificationID FROM [Notification].[tNotifications] WHERE NotificationID = @NotificationID)
		BEGIN
			RAISERROR('Notification does not exist', 13, -1, -1)
		END
	END

	--If the Notification Type isn't valid, throw an error.
	IF NOT EXISTS (SELECT TOP 1 NotificationTypeID FROM [Notification].[tNotificationType] WHERE NotificationTypeID = @NotificationTypeID)
	BEGIN
        RAISERROR('Unrecognized Notification Type', 13, -1, -1)
	END

	IF @Message is null
	BEGIN
        RAISERROR('Must provide Message in Notification', 13, -1, -1)
	END

	IF @Result is null
	BEGIN
        RAISERROR('Must provide Result in Notification', 13, -1, -1)
	END

	IF @Viewed is null
	BEGIN
        RAISERROR('Must provide value of viewed in Notification', 13, -1, -1)
	END

	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @Date DATETIME = NULL
		SET @Date = SYSDATETIME();

		SET NOCOUNT ON;
		IF @NotificationID IS NULL OR @NotificationID < 0
		BEGIN
			IF @NotificationGroupID IS NULL
				SET @NotificationGroupID = NEWID();

			INSERT INTO [Notification].[tNotifications]
			   ([NotificationGroupID],
				[NotificationTypeID],
				[SecurityUserID],
				[Message],
				[Options],
				[Result],
				[Viewed],
				[CreatedDate],
				[CreatedBy],
				[ChangedDate],
				[ChangedBy])
		 VALUES
			   (@NotificationGroupID,
				@NotificationTypeID,
				@SecurityUserID,
				@Message,
				@Options,
				@Result,
				@Viewed,
				@Date,
				@SecurityUserID,
				@Date,
				@SecurityUserID)

			   SET @NewNotificationID = (SELECT SCOPE_IDENTITY());
		END
		ELSE
		BEGIN
			UPDATE [Notification].[tNotifications]
			SET 
				[NotificationTypeID] = @NotificationTypeID,
				[Message] = @Message,
				[Options] = @Options,
				[Result] = @Result,
				[Viewed] = @Viewed,
				[ChangedDate] = @Date,
				[ChangedBy] = @SecurityUserID
			WHERE NotificationID = @NotificationID;

			SET @NewNotificationID = @NotificationID;
		END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

GRANT EXECUTE
    ON OBJECT::[Notification].[spUpsertNotification] TO [TEUser]
    AS [dbo];
GO