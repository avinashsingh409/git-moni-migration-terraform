﻿
/****** Object:  Store Procedure [Notification].[spDeleteAttachmentById] ******/
CREATE PROCEDURE [Notification].[spDeleteAttachmentById]
	 @AttachmentID [int]
	 , @DeleteCount INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM Notification.tAttachment WHERE AttachmentID = @AttachmentID;
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[Notification].[spDeleteAttachmentById] TO [TEUser]
    AS [dbo];
