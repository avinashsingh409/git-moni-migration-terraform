CREATE PROCEDURE [Notification].[spDeleteAllNotificationsByGroup]
	@SecurityUserID INT,
	@NotificationGroupID UNIQUEIDENTIFIER,
	@DeleteCount INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [Notification].[tNotifications] WHERE NotificationGroupID = @NotificationGroupID AND SecurityUserID = @SecurityUserID;
	SET @DeleteCount = @@ROWCOUNT;
END
GO

GRANT EXECUTE
    ON OBJECT::[Notification].[spDeleteAllNotificationsByGroup] TO [TEUser]
    AS [dbo];
GO