CREATE PROCEDURE [Notification].[spUpdateNotificationView]
	@SecurityUserID INT,
	@NotificationID INT, 
	@Viewed BIT
AS
BEGIN
	DECLARE @Date DATETIME = NULL
	SET @Date = SYSDATETIME();

	UPDATE [Notification].[tNotifications]
	SET 
		[Viewed] = @Viewed,
		[ChangedDate] = @Date,
		[ChangedBy] = @SecurityUserID
	WHERE NotificationID = @NotificationID;
END
GO

GRANT EXECUTE
    ON OBJECT::[Notification].[spUpdateNotificationView] TO [TEUser]
    AS [dbo];
GO