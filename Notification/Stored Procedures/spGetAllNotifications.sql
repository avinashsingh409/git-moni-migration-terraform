CREATE PROCEDURE [Notification].[spGetAllNotifications]
	@SecurityUserID INT,
	@Page INT,
	@PageSize INT,
	@NotificationGroupID UNIQUEIDENTIFIER, 
	@Viewed BIT
AS
BEGIN
	SELECT
		notif.NotificationID,
		notif.NotificationGroupID,
		notif.NotificationTypeID,
		notif.SecurityUserID,
		notif.Message,
		notif.Options,
		notif.Result,
		notif.Viewed,
		notif.CreatedDate,
		userInfo1.Username AS CreatedBy,
		notif.ChangedDate,
		userInfo2.Username AS ChangedBy
	FROM [Notification].[tNotifications] notif 
	LEFT JOIN [AccessControl].[tUser] userInfo1 ON notif.CreatedBy = userInfo1.SecurityUserID
	LEFT JOIN [AccessControl].[tUser] userInfo2 ON notif.ChangedBy = userInfo2.SecurityUserID
	WHERE notif.SecurityUserID = @SecurityUserID AND (@NotificationGroupID IS NULL OR notif.NotificationGroupID = @NotificationGroupID) AND 
		(@Viewed IS NULL OR notif.Viewed = @Viewed)
	ORDER BY [CreatedDate] DESC
	OFFSET (@Page * @PageSize) ROWS FETCH NEXT @PageSize ROWS ONLY;
END
GO

GRANT EXECUTE
    ON OBJECT::[Notification].[spGetAllNotifications] TO [TEUser]
    AS [dbo];
GO