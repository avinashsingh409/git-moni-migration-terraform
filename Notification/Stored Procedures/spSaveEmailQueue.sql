﻿--Spaces are not valid characters in the address fields.
--This should protect against a failure by the EmailClient.

CREATE PROCEDURE [Notification].[spSaveEmailQueue]
	@EmailSubject NVARCHAR(255) = NULL
	, @EmailToList NVARCHAR(max) = NULL
	, @EmailCcList NVARCHAR(max) = NULL
	, @EmailBccList NVARCHAR(max) = NULL
	, @EmailToDisplayNameList NVARCHAR(max) = NULL
	, @EmailCcDisplayNameList NVARCHAR(max) = NULL
	, @EmailBccDisplayNameList NVARCHAR(max) = NULL
	, @EmailBody NVARCHAR(max)
    , @CreatedBy INT
	, @CreateDate Datetime
	, @EmailCategory NVARCHAR(255)
	, @FormatBodyAsHTML Bit
	, @NewID UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN
		--RMB: Review [Diagnostics].[spEmailAssetIssueStatusChange] if changes are made to this procedure
		DECLARE @TmpTable TABLE (EmailQueueID uniqueidentifier);
		INSERT INTO [Notification].tEmailQueue (EmailSubject, EmailToList, EmailCcList, EmailBccList, EmailToDisplayNameList, EmailCcDisplayNameList, EmailBccDisplayNameList, EmailBody, CreatedBy, CreateDate, EmailCategory,FormatBodyAsHTML)
		OUTPUT INSERTED.EmailQueueID INTO @TmpTable
		VALUES (@EmailSubject, REPLACE(@EmailToList, ' ', ''), REPLACE(@EmailCcList, ' ', ''), REPLACE(@EmailBccList, ' ', ''), @EmailToDisplayNameList, @EmailCcDisplayNameList, @EmailBccDisplayNameList, @EmailBody, @CreatedBy, @CreateDate,@EmailCategory,@FormatBodyAsHTML);

		SELECT @NewID = EmailQueueID FROM @TmpTable;
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Notification].[spSaveEmailQueue] TO [TEUser]
    AS [dbo];


GO