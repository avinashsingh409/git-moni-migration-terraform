﻿
/****** Object:  Store Procedure [Notification].[spSaveAttachment] ******/
CREATE PROCEDURE [Notification].[spSaveAttachment]
	 @GlobalID uniqueidentifier
	, @AttachmentName NVARCHAR(255) = NULL
	, @DataLength INT
	, @AttachmentFile [varbinary](max)
	, @MediaType NVARCHAR(255)
	, @MIMEContentID NVARCHAR(255) = NULL
	, @NewID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN
		INSERT INTO [Notification].tAttachment (GlobalID,AttachmentName, DataLength, AttachmentFile,MediaType, MIMEContentID)
		VALUES (@GlobalID, @AttachmentName, @DataLength, @AttachmentFile,@MediaType,@MIMEContentID);
		SET @NewID = (SELECT SCOPE_IDENTITY());
	END
END
GO
GRANT EXECUTE
    ON OBJECT::[Notification].[spSaveAttachment] TO [TEUser]
    AS [dbo];
