CREATE PROCEDURE [Notification].[spUpdateNotificationViewByGroup]
	@SecurityUserID INT,
	@NotificationGroupID UNIQUEIDENTIFIER, 
	@Viewed BIT
AS
BEGIN
	DECLARE @Date DATETIME = NULL
	SET @Date = SYSDATETIME();

	UPDATE [Notification].[tNotifications]
	SET 
		[Viewed] = @Viewed,
		[ChangedDate] = @Date,
		[ChangedBy] = @SecurityUserID
	WHERE NotificationGroupID = @NotificationGroupID;
END
GO

GRANT EXECUTE
    ON OBJECT::[Notification].[spUpdateNotificationViewByGroup] TO [TEUser]
    AS [dbo];
GO