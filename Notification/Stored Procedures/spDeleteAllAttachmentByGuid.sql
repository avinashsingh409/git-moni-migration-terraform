﻿
/****** Object:  Store Procedure [Notification].[spDeleteAllAttachmentByGuid] ******/
CREATE PROCEDURE [Notification].[spDeleteAllAttachmentByGuid]
	 @GlobalID uniqueidentifier
	 , @DeleteCount INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM Notification.tAttachment WHERE GlobalID = @GlobalID;
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[Notification].[spDeleteAllAttachmentByGuid] TO [TEUser]
    AS [dbo];
