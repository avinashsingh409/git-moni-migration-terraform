﻿
/****** Object:  Store Procedure [Notification].[spDeleteAttachmentById] ******/
CREATE PROCEDURE [Notification].[spDeleteEmailQueue]
	 @EmailQueueID uniqueidentifier
	 , @DeleteCount INT OUTPUT
AS
BEGIN
	DELETE FROM Notification.tAttachment WHERE GlobalID = @EmailQueueID;

	SET NOCOUNT ON;
	DELETE FROM Notification.tEmailQueue WHERE EmailQueueID = @EmailQueueID;
	SET @DeleteCount = @@ROWCOUNT;
END
GO
GRANT EXECUTE
    ON OBJECT::[Notification].[spDeleteEmailQueue] TO [TEUser]
    AS [dbo];
