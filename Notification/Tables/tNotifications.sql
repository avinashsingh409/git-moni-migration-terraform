CREATE TABLE [Notification].[tNotifications]
(
    [NotificationID] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[NotificationGroupID] UNIQUEIDENTIFIER NOT NULL,
	[NotificationTypeID] INT NOT NULL,
	[SecurityUserID] INT NOT NULL,
	[Message] NVARCHAR(MAX) NOT NULL,
	[Options] NVARCHAR(255) NULL,
	[Result] NVARCHAR(MAX) NOT NULL,
	[Viewed] BIT NOT NULL,
	[CreatedDate] DATETIME NOT NULL,
	[CreatedBy] INT NOT NULL,
    [ChangedDate] DATETIME NOT NULL,
	[ChangedBy] INT NOT NULL,
	CONSTRAINT [FK_Notifications_tUser_SecurityUserID] FOREIGN KEY ([SecurityUserID]) REFERENCES [AccessControl].[tUser] ([SecurityUserID]) ON DELETE CASCADE,
	CONSTRAINT [FK_Notifications_NotificationType_NotificationTypeID] FOREIGN KEY ([NotificationTypeID]) REFERENCES [Notification].[tNotificationType] ([NotificationTypeID]) ON DELETE CASCADE
)
GO