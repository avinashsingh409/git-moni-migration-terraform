﻿CREATE TABLE [Notification].[tEmailQueue](
	[EmailQueueID] [uniqueidentifier] NOT NULL,
	[EmailSubject] [nvarchar](255) NOT NULL,
	[EmailToList] [nvarchar](max) NULL,
	[EmailCcList] [nvarchar](max) NULL,
	[EmailBccList] [nvarchar](max) NULL,
	[EmailToDisplayNameList] [nvarchar](max) NULL,
	[EmailCcDisplayNameList] [nvarchar](max) NULL,
	[EmailBccDisplayNameList] [nvarchar](max) NULL,
	[EmailBody] [nvarchar](max) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[SendDate] [datetime] NULL,
	[SendErrorMessage] [nvarchar](max) NULL,
	[EmailCategory] [nvarchar](255) NOT NULL,
	[FormatBodyAsHTML] [bit] NOT NULL
 CONSTRAINT [PK_Notification.tEmailQueue] PRIMARY KEY CLUSTERED
(
	[EmailQueueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Notification].[tEmailQueue] ADD CONSTRAINT [FK_tEmailQueue_tUser] FOREIGN KEY([CreatedBy])
REFERENCES [AccessControl].[tUser] ([SecurityUserID])
GO

ALTER TABLE [Notification].[tEmailQueue] CHECK CONSTRAINT [FK_tEmailQueue_tUser]
GO
ALTER TABLE [Notification].[tEmailQueue] ADD  CONSTRAINT [DF__tEmailQueue_EmailQueueID]  DEFAULT (newid()) FOR [EmailQueueID]
GO
CREATE INDEX [IDX_tEmailQueue_SendDate] ON [Notification].[tEmailQueue] ([SendDate])  ;