﻿CREATE TRIGGER [Notification].[tr_tEmailQueue_InsertUpdate]
   ON  [Notification].[tEmailQueue]
   AFTER INSERT,UPDATE
AS
BEGIN

    --This is a safety net to make sure that values are compliant with the relevant RFC

    SET NOCOUNT ON;
	UPDATE q
	SET EmailToList = REPLACE(i.EmailToList, ' ', ''), EmailCcList = REPLACE(i.EmailCcList, ' ', ''), EmailBccList = REPLACE(i.EmailBccList, ' ', '')
	FROM Notification.tEmailQueue q
	JOIN inserted i on i.EmailQueueID = q.EmailQueueID


END
