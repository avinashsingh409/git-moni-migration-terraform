﻿CREATE TABLE [Notification].[tAttachment] (
    [AttachmentID]   INT              IDENTITY (1, 1) NOT NULL,
    [GlobalID]       UNIQUEIDENTIFIER NOT NULL,
    [AttachmentName] NVARCHAR (255)   NULL,
    [DataLength]     INT              NOT NULL,
    [AttachmentFile] VARBINARY (MAX)  NOT NULL,
    [MediaType]      NVARCHAR (255)   NOT NULL,
    [MIMEContentID] NVARCHAR(255) NULL, 
    CONSTRAINT [PK_Notification.tAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC)
);

