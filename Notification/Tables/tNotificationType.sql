CREATE TABLE [Notification].[tNotificationType]
(
    [NotificationTypeID] INT NOT NULL PRIMARY KEY, 
    [NotificationTypeAbbrev] NVARCHAR(50) NOT NULL, 
    [NotificationTypeDesc] NVARCHAR(255) NOT NULL,
)
GO

EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Notification', @level1type = N'TABLE', @level1name = N'tNotificationType';
GO