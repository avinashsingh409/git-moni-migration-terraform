﻿CREATE PROCEDURE [Analytx].[spCreateContentCatalogPumpJob] 
(
	@ts datetime
)
AS
BEGIN
     IF @ts IS NOT NULL
	 BEGIN
		 IF EXISTS (SELECT * FROM AnalytX.tContentCatalogPumpQueue WHERE JobStatus = 0)
		 BEGIN
			IF NOT EXISTS (SELECT * FROM AnalytX.tContentCatalogPumpQueue WHERE JobStatus = 0 AND StartTime <= @ts)
			BEGIN
				UPDATE AnalytX.tContentCatalogPumpQueue SET 
					StartTime = @ts
					,EndTime = DATEADD(DAY, -1, GETDATE())
					,ChangeDate = GETDATE()
				 WHERE JobStatus = 0
			 END
			 ELSE
				 UPDATE AnalytX.tContentCatalogPumpQueue SET 
						EndTime = DATEADD(DAY, -1, GETDATE())
						,ChangeDate = GETDATE()
					 WHERE JobStatus = 0
		 END
		 ELSE
		 BEGIN
			INSERT INTO AnalytX.tContentCatalogPumpQueue ([StartTime],[EndTime],[JobStatus], [CreateDate]) VALUES (@ts, DATEADD(DAY, -1, GETDATE()), 0, GETDATE())
		 END
	 END
END