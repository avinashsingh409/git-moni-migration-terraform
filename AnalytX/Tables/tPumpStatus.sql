﻿CREATE TABLE [AnalytX].[tPumpStatus] (
    [PumpStatusID]      INT          IDENTITY (1, 1) NOT NULL,
    [PDServerID]        INT          NOT NULL,
    [LastProcessedTime] DATETIME     NOT NULL,
    [ExpectedLag]       INT          NOT NULL,
    [Status]            VARCHAR (50) NULL,
    CONSTRAINT [PK_AnalytXtPumpStatus_tPumpStatusID] PRIMARY KEY CLUSTERED ([PumpStatusID] ASC),
    CONSTRAINT [FK_ProcessData_tServer_PDServerID] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]) ON DELETE CASCADE
);








GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tPumpStatus';

