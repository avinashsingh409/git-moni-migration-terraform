CREATE TABLE [AnalytX].[tAssetAppContextDetail] (
    [AssetAppContextDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]                 INT            NOT NULL,
    [AssetAppContextSetID]    INT            NOT NULL,
    [ItemID]                  NVARCHAR (255) NOT NULL,
    [Name]                    NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAssetAppContextDetail] PRIMARY KEY CLUSTERED ([AssetAppContextDetailID] ASC),
    CONSTRAINT [FK_AnalytX_tAssetAppContextDetail_AssetAppContextSetID_AnalytX_tAssetAppContextSet] FOREIGN KEY ([AssetAppContextSetID]) REFERENCES [AnalytX].[tAssetAppContextSet] ([AssetAppContextSetID]),
    CONSTRAINT [FK_AnalytX_tAssetAppContextDetail_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tAssetAppContextDetail';

