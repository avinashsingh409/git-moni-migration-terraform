CREATE TABLE [AnalytX].[tAXShare] (
    [AXShareID] INT IDENTITY (1, 1) NOT NULL,
    [AssetID]   INT NOT NULL,
    [AXGroupID] INT NOT NULL,
    [CanBrowse] BIT CONSTRAINT [DF_DefaultShare_CanBrowse] DEFAULT ((0)) NOT NULL,
    [CanEdit]   BIT CONSTRAINT [DF_DefaultShare_CanEdit] DEFAULT ((0)) NOT NULL,
    [CanShare]  BIT CONSTRAINT [DF_DefaultShare_CanShare] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [FK_tAXShare_tAsset_AssetID] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAXShare_tAXGroup_AXGroupID] FOREIGN KEY ([AXGroupID]) REFERENCES [AnalytX].[tAXGroup] ([AXGroupID]) ON DELETE CASCADE
);




GO

GO


GO


GO

GO


GO


GO

GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tAXShare';

