CREATE TABLE [AnalytX].[tAppKeyItemID] (
    [AppKey] NCHAR (255) NOT NULL,
    [ItemID] NCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAppKeyItemID] PRIMARY KEY CLUSTERED ([AppKey] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tAppKeyItemID';

