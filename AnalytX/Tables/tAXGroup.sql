CREATE TABLE [AnalytX].[tAXGroup] (
    [AXGroupID] INT            IDENTITY (1, 1) NOT NULL,
    [AXGroup]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_AXGroup] PRIMARY KEY CLUSTERED ([AXGroupID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tAXGroup';

