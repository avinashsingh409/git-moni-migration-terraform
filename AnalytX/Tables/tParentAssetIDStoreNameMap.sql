﻿CREATE TABLE [AnalytX].[tParentAssetIDStoreNameMap] (
    [ParentAssetIDStoreNameMapID] INT           IDENTITY (1, 1) NOT NULL,
    [AssetID]                     INT           NOT NULL,
    [StoreName]                   VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tParentAssetIDStoreNameMap] PRIMARY KEY CLUSTERED ([ParentAssetIDStoreNameMapID] ASC),
    CONSTRAINT [FK_tParentAssetIDStoreNameMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tParentAssetIDStoreNameMap_AssetID] UNIQUE NONCLUSTERED ([AssetID] ASC)
);






GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tParentAssetIDStoreNameMap';

