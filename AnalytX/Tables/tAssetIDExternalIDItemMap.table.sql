CREATE TABLE [AnalytX].[tAssetIDExternalIDItemMap] (
    [AssetIDExternalIDItemMapID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]                    INT            NOT NULL,
    [ExternalID]                 NVARCHAR (255) NOT NULL,
    [ItemID]                     NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAXAssetIDExternalIDAXCallMap] PRIMARY KEY CLUSTERED ([AssetIDExternalIDItemMapID] ASC),
    CONSTRAINT [FK_AnalytX_tAXAssetIDExternalIDAXCallMap_AssetID_Asset_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID])
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tAssetIDExternalIDItemMap';

