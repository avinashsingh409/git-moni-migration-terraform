﻿CREATE TABLE [AnalytX].[tContentCatalogPumpQueue] (
    [ContentCatalogPumpQueueID] INT      IDENTITY (1, 1) NOT NULL,
    [StartTime]                 DATETIME NOT NULL,
    [EndTime]                   DATETIME NOT NULL,
    [JobStatus]                 INT      NOT NULL,
    [CreateDate]                DATETIME CONSTRAINT [DF_tContentCatalogPumpQueue_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                DATETIME CONSTRAINT [DF_tContentCatalogPumpQueue_ChangeDate] DEFAULT (getdate()) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of time periods that need to be pumped into the AX Content Catalog', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tContentCatalogPumpQueue';

