CREATE TABLE [AnalytX].[tAssetAppContextSet] (
    [AssetAppContextSetID]   INT            IDENTITY (1, 1) NOT NULL,
    [AssetAppContextSetDesc] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tAssetAppContextSet] PRIMARY KEY CLUSTERED ([AssetAppContextSetID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'AnalytX', @level1type = N'TABLE', @level1name = N'tAssetAppContextSet';

