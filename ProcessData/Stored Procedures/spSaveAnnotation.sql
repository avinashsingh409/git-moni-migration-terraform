﻿CREATE PROCEDURE [ProcessData].[spSaveAnnotation] 
	   @securityUserID int, 
	   @annotationID bigint = null OUTPUT, 
	   @assetID int, 
	   @tagID int, 
	   @startDate datetime = NULL,
	   @endDate datetime = NULL,
	   @annotationCategoryID int,
	   @annotationProperties ProcessData.tpAnnotationProperty READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @rights as bit = 0
	SELECT @rights = Asset.ufnDoesUserHaveAccessToAsset(@assetID,@securityUserID,-1)
	
    if @rights=0
	BEGIN
		RETURN -1;
    END
	
	BEGIN TRANSACTION	
	DECLARE @currentAnnotationID bigint;
	
	IF @annotationID IS NULL BEGIN
		INSERT INTO tAnnotation( [AssetID], [TagID], [AnnotationCategoryID], [StartDate], [EndDate], [CreateDate], [CreatedByUser] )
		VALUES( @assetID, @tagID, @annotationCategoryID, @startDate, @endDate, GetDate(), @securityUserID);
		SELECT @currentAnnotationID = SCOPE_IDENTITY();
	END
	ELSE BEGIN
		UPDATE tAnnotation
		  SET [AssetID] = @assetID, [TagID] = @tagID, [AnnotationCategoryID] = @annotationCategoryID, [StartDate] = @startDate, [EndDate] = @endDate
		WHERE [AnnotationID] = @annotationID;
		SELECT @currentAnnotationID = @annotationID;
	END
	
	DECLARE @recordsAffected int;
	SET @recordsAffected = 0;
	
	DECLARE @propertiesToSave int;
	SELECT @propertiesToSave = COUNT(*) FROM @annotationProperties;
	
	UPDATE ProcessData.tAnnotationPropertyMap
	SET ProcessData.tAnnotationPropertyMap.ValueAsText = props.ValueAsText
	FROM ProcessData.tAnnotationPropertyMap
	INNER JOIN @annotationProperties props 
		ON props.AnnotationPropertyMapID = tAnnotationPropertyMap.AnnotationPropertyMapID;
	
	SELECT @recordsAffected = @recordsAffected + @@ROWCOUNT
	
	UPDATE ProcessData.tAnnotationPropertyMap
	SET ProcessData.tAnnotationPropertyMap.ValueAsText = props.ValueAsText
	FROM ProcessData.tAnnotationPropertyMap
	INNER JOIN @annotationProperties props 
		ON props.AnnotationID = tAnnotationPropertyMap.AnnotationID
		AND props.AnnotationTypePropertyID = tAnnotationPropertyMap.AnnotationTypePropertyID
		AND props.AnnotationPropertyMapID IS NULL

	SELECT @recordsAffected = @recordsAffected + @@ROWCOUNT
		
	INSERT INTO ProcessData.tAnnotationPropertyMap
	(
		AnnotationID,
		AnnotationTypePropertyID,
		ValueAsText
	)
	SELECT @currentAnnotationID, AnnotationTypePropertyID, ValueAsText
	FROM @annotationProperties
	WHERE AnnotationPropertyMapID IS NULL
	AND [@annotationProperties].AnnotationTypePropertyID IS NOT NULL
	AND @annotationID IS NULL                 --TJC - this was missing and caused extra insert and failure with rollback below

	SELECT @recordsAffected = @recordsAffected + @@ROWCOUNT
	--PRINT 'Recs affected = ' + CAST(@recordsAffected AS varchar(10)) + ' - Props to save = ' + CAST(@propertiesToSave AS varchar(10))

	IF (@recordsAffected = @propertiesToSave) 
	  BEGIN
		COMMIT TRANSACTION;
		SELECT @annotationID = @currentAnnotationID;
	  END 
	ELSE 
	  BEGIN
		ROLLBACK TRANSACTION;
		RETURN -1;
	  END 
	
END


GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spSaveAnnotation] TO [TEUser]
    AS [dbo];

