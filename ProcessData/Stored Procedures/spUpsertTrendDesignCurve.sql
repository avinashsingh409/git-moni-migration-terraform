﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendDesignCurve]
	@TrendDesignCurveID INT = NULL,
	@TrendID INT,
	@Axis INT,
	@DisplayText NVARCHAR(255) = NULL,
	@Color NVARCHAR(56) = NULL,
	@Type NVARCHAR(255) = NULL,
	@Values NVARCHAR(MAX) = NULL,
	@Repeat NVARCHAR(255) = NULL,
	@SecurityUserID int,
	@NewTrendDesignCurveID int OUTPUT
AS
BEGIN
	DECLARE @Username NVARCHAR(255);
	SELECT @Username = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID;

	SET @NewTrendDesignCurveID = -1
	IF(@TrendDesignCurveID IS NULL OR @TrendDesignCurveID <= 0)
	BEGIN
		INSERT INTO [ProcessData].[tTrendDesignCurve]
           ([PDTrendID]
           ,[Axis]
           ,[DisplayText]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[Color]
           ,[Type]
           ,[Values]
           ,[Repeat])
		VALUES
           (@TrendID
           ,@Axis
           ,@DisplayText
           ,@Username
           ,@Username
           ,GetDate()
           ,GETDATE()
           ,@Color
           ,@Type
           ,@Values
           ,@Repeat)
		   SET @NewTrendDesignCurveID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE [ProcessData].[tTrendDesignCurve]
		SET [PDTrendID] = @TrendID
		  ,[Axis] = @Axis
		  ,[DisplayText] = @DisplayText
		  ,[ChangedBy] = @Username
		  ,[ChangeDate] = GetDate()
		  ,[Color] = @Color
		  ,[Type] = @Type
		  ,[Values] = @Values
		  ,[Repeat] = @Repeat
		WHERE @TrendDesignCurveID = @TrendDesignCurveID
		
		SET @NewTrendDesignCurveID = @TrendDesignCurveID 
	END
	SELECT @NewTrendDesignCurveID
END
GO 
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendDesignCurve] TO [TEUser]
    AS [dbo];
GO