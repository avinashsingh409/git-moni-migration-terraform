﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendPin]
	@TrendPinID INT = NULL,
	@TrendID INT,
	@StartTime DateTime, 
	@EndTime DateTime,
	@Name NVARCHAR(255),
	@Hidden bit,
	@SpanUnits NVARCHAR(255),
	@NameFormat NCHAR(255),
	@CriteriaObjectID INT = NULL,
	@SecurityUserID int,
	@NewTrendPinID int OUTPUT
AS
BEGIN
	DECLARE @Username NVARCHAR(255);
	SELECT @Username = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID;

	DELETE FROM Criteria.tTrendCoMap WHERE PDTrendId = @TrendID AND TrendCoMapName = @Name

	IF(@CriteriaObjectID IS NOT NULL)
	BEGIN
		INSERT INTO Criteria.tTrendCoMap(PDTrendId, CoId, TrendCoMapName, Hidden)
		VALUES (@TrendID, @CriteriaObjectID, @Name, @Hidden)	
	END

	SET @NewTrendPinID = -1
	IF(@TrendPinID IS NULL OR @TrendPinID <= 0)
	BEGIN
		INSERT INTO [ProcessData].[tTrendPin]
           ([PDTrendID]
           ,[StartTime]
           ,[EndTime]
           ,[Name]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[Hidden]
           ,[SpanUnits]
           ,[NameFormat])
		VALUES
           (@TrendID
           ,@StartTime
           ,@EndTime
           ,@Name
           ,@Username
           ,@Username
           ,GETDATE()
           ,GETDATE()
           ,@Hidden
           ,@SpanUnits
           ,@NameFormat)
		SET @NewTrendPinID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE [ProcessData].[tTrendPin]
	   SET [PDTrendID] = @TrendID
		  ,[StartTime] = @StartTime
		  ,[EndTime] = @EndTime
		  ,[Name] = @Name
		  ,[CreatedBy] = @Username
		  ,[ChangedBy] = @Username
		  ,[CreateDate] = GetDate()
		  ,[ChangeDate] = GetDate()
		  ,[Hidden] = @Hidden
		  ,[SpanUnits] = @SpanUnits
		  ,[NameFormat] = @NameFormat
		WHERE PDTrendPinID = @TrendPinID
		
		SET @NewTrendPinID = @TrendPinID 
	END
	SELECT @NewTrendPinID
END
GO 
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendPin] TO [TEUser]
    AS [dbo];
GO