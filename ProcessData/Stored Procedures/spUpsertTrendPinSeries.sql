﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendPinSeries]
	@PDTrendPinSeriesID INT = NULL,
	@PDTrendPinID INT,
	@SeriesID INT = NULL,
	@FilterMin float = NULL, 
	@FilterMax float = NULL, 
	@FlatlineThreshold float = NULL, 
	@Color NVARCHAR(56) = NULL, 
	@Exclude NVARCHAR(512) = NULL, 
	@ApplyToAll BIT = 0,
	@Archive NVARCHAR(255) = NULL, 
	@NewPDTrendPinSeriesID int OUTPUT
AS
BEGIN

	SET @NewPDTrendPinSeriesID = -1
	IF(@PDTrendPinSeriesID IS NULL OR @PDTrendPinSeriesID <= 0)
	BEGIN
		INSERT INTO [ProcessData].[tTrendPinSeries]
           ([PDTrendPinID]
           ,[SeriesID]
           ,[FilterMin]
           ,[FilterMax]
           ,[FlatlineThreshold]
           ,[Color]
           ,[Exclude]
           ,[ApplyToAll]
           ,[Archive])
		VALUES
           (@PDTrendPinID
           ,@SeriesID
           ,@FilterMin
           ,@FilterMax
           ,@FlatlineThreshold
           ,@Color
           ,@Exclude
           ,@ApplyToAll
           ,@Archive)
		SET @NewPDTrendPinSeriesID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE [ProcessData].[tTrendPinSeries]
	   SET [PDTrendPinID] = @PDTrendPinID
		  ,[SeriesID] = @SeriesID
		  ,[FilterMin] = @FilterMin
		  ,[FilterMax] = @FilterMax
		  ,[FlatlineThreshold] = @FlatlineThreshold
		  ,[Color] = @Color
		  ,[Exclude] = @Exclude
		  ,[ApplyToAll] = @ApplyToAll
		  ,[Archive] = @Archive
		WHERE PDTrendPinSeriesID = @PDTrendPinSeriesID
		
		SET @NewPDTrendPinSeriesID = @PDTrendPinSeriesID 
	END
	SELECT @NewPDTrendPinSeriesID
END
GO 

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendPinSeries] TO [TEUser]
    AS [dbo];
GO