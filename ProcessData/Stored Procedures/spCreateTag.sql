﻿CREATE PROCEDURE [ProcessData].[spCreateTag]
    @pdServerId int,
    @tagName nvarchar(400),
    @assetId int,
    @variableTypeId int,
    @engUnits nvarchar(255),
    @securityUserId int,
    @userName nvarchar(255),
    @valueTypeId int,
    @tagDesc nvarchar(400) = NULL,
	@autoGenExternalID bit = 1,
	@newTagId int = null output
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @tagId int;
    DECLARE @rightNow datetime;
	DECLARE @externalID nvarchar(400);

	IF(@autoGenExternalID = 1) 
		BEGIN
			SET @externalID = @tagName;
		END
	ELSE 
		BEGIN
			SET @externalID = '';
		END

    SET @rightNow = GETDATE();

	IF(@tagDesc IS NULL) 
		BEGIN
		INSERT INTO ProcessData.tTag (PDServerID,TagName,TagDesc,EngUnits,ExistsOnServer,ExternalID,CreatedBy,ChangedBy,CreateDate,ChangeDate,NDCreated)
		VALUES(@pdServerId,@tagName,@tagName+'(Auto-gen)',@engUnits,1,@externalID,@securityUserId,@securityUserId,@rightNow,@rightNow,0);
		END
	ELSE
		BEGIN
		INSERT INTO ProcessData.tTag (PDServerID,TagName,TagDesc,EngUnits,ExistsOnServer,ExternalID,CreatedBy,ChangedBy,CreateDate,ChangeDate,NDCreated)
		VALUES(@pdServerId,@tagName,@tagDesc,@engUnits,1,@externalID,@securityUserId,@securityUserId,@rightNow,@rightNow,0);
		END

    SET @tagId=SCOPE_IDENTITY();
	SET @newTagId = @tagID;

    IF (@assetId IS NULL) BEGIN
	   SELECT @tagId;
	   RETURN;
    END;

    -- Create AssetVariableTypeTagMap entries
    INSERT INTO ProcessData.tAssetVariableTypeTagMap(AssetID, VariableTypeID, TagID, CalcString, CreatedBy, ChangedBy, CreateDate, ChangeDate, ValueTypeID)
    VALUES(@assetId, @variableTypeId, @tagId, null, @userName, @userName, @rightNow, @rightNow, @valueTypeId)
    SELECT @tagId;
	
END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spCreateTag] TO [TEUser]
    AS [dbo];

