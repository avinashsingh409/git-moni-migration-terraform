﻿CREATE PROCEDURE [ProcessData].[spInsertSQLHistorianData]
      @tblData ProcessData.tpProcessData READONLY
AS
BEGIN
insert into ProcessData.tSQLHistorian (TagID,TimeStamp,Value,Status,Archive) select b.PDTagID,a.TimeStamp,a.Value,a.Status,a.Archive from @tblData a join ProcessData.tTag b on 
a.ServerID = b.PDServerID and a.TagName = b.TagName
END
GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spInsertSQLHistorianData] TO [TEUser]
    AS [dbo];
GO
