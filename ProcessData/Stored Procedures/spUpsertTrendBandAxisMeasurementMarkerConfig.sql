﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendBandAxisMeasurementMarkerConfig]
	@TrendBandAxisMeasurementMarkerConfigID [int] = NULL,
	@TrendBandAxisMeasurementID [int],
	@Name NVARCHAR(255) = NULL,
	@Symbol NVARCHAR(MAX)  = NULL,
	@Color NVARCHAR(255)  = NULL,
	@FillColor NVARCHAR(255)  = NULL,
	@BorderThickness INT  = NULL,
	@PDVariableID INT  = NULL,
	@ValueTypeID INT  = NULL,
	@PDTagID INT  = NULL,
	@AggregationType INT  = NULL,
	@Min FLOAT  = NULL,
	@Max FLOAT  = NULL,
	@MinInclusive BIT = 0,
	@MaxInclusive BIT = 0,
	@NewTrendBandAxisMeasurementMarkerConfigID int OUTPUT
AS
BEGIN
	SET @TrendBandAxisMeasurementMarkerConfigID = -1
		IF (@TrendBandAxisMeasurementMarkerConfigID IS NULL OR @TrendBandAxisMeasurementMarkerConfigID <= 0)
		BEGIN
			INSERT INTO [ProcessData].[tTrendBandAxisMeasurementMarkerConfig]
				   ([TrendBandAxisMeasurementID]
				   ,[Name]
				   ,[Symbol]
				   ,[Color]
				   ,[FillColor]
				   ,[BorderThickness]
				   ,[PDVariableID]
				   ,[ValueTypeID]
				   ,[PDTagID]
				   ,[AggregationType]
				   ,[Min]
				   ,[Max]
				   ,[MinInclusive]
				   ,[MaxInclusive])
			 VALUES
				   (@TrendBandAxisMeasurementID
				   ,@Name
				   ,@Symbol
				   ,@Color
				   ,@FillColor
				   ,@BorderThickness
				   ,@PDVariableID
				   ,@ValueTypeID
				   ,@PDTagID
				   ,@AggregationType
				   ,@Min
				   ,@Max
				   ,@MinInclusive
				   ,@MaxInclusive)
			SET @NewTrendBandAxisMeasurementMarkerConfigID = SCOPE_IDENTITY();
		END
		ELSE
		BEGIN
			UPDATE [ProcessData].[tTrendBandAxisMeasurementMarkerConfig]
			   SET [TrendBandAxisMeasurementID] = @TrendBandAxisMeasurementID
				  ,[Name] = @Name
				  ,[Symbol] = @Symbol
				  ,[Color] = @Color
				  ,[FillColor] = @FillColor
				  ,[BorderThickness] = @BorderThickness
				  ,[PDVariableID] = @PDVariableID
				  ,[ValueTypeID] = @ValueTypeID
				  ,[PDTagID] = @PDTagID
				  ,[AggregationType] = @AggregationType
				  ,[Min] = @Min
				  ,[Max] = @Max
				  ,[MinInclusive] = @MinInclusive
				  ,[MaxInclusive] = @MaxInclusive
			 WHERE TrendBandAxisMeasurementMarkerConfigID = @TrendBandAxisMeasurementMarkerConfigID
			SET @NewTrendBandAxisMeasurementMarkerConfigID = @TrendBandAxisMeasurementMarkerConfigID;
		END
	END
	SELECT @NewTrendBandAxisMeasurementMarkerConfigID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendBandAxisMeasurementMarkerConfig] TO [TEUser]
    AS [dbo];
GO