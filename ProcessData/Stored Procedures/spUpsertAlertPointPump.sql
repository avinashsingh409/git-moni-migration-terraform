﻿CREATE   PROCEDURE [ProcessData].[spUpsertAlertPointPump]
	@AlertPointPumpID int = NULL,
	@DisplayName nvarchar(400),
	@GlobalID uniqueidentifier = null,
	@ExternalID nvarchar(400) = null,
	@NDPumpID nvarchar(400) = null,
	@AlertPointID nvarchar(400) = null,
	@UpsertAlertPointPumpID int OUTPUT
AS
BEGIN
	SET @UpsertAlertPointPumpID = -1;
	IF (@AlertPointPumpID IS NULL)
	BEGIN
		DECLARE @InsertedID Base.tpIntList;
		INSERT INTO ProcessData.tAlertPointPump
		(
			DisplayName
			, ExternalID
			, NDPumpID
			, AlertPointID
		)
		OUTPUT inserted.AlertPointPumpID INTO @InsertedID(id)
		VALUES
		(
			@DisplayName
			, @ExternalID
			, @NDPumpID
			, @AlertPointID
		);
		SELECT TOP 1 @UpsertAlertPointPumpID = id FROM @InsertedID;
	END
	ELSE
	BEGIN
		IF NOT EXISTS (SELECT AlertPointPumpID FROM ProcessData.tAlertPointPump WHERE AlertPointPumpID = @AlertPointPumpID)
		BEGIN
			raiserror (N'AlertPointPumpID does not exist', 13, -1, -1);
		END
		
		UPDATE ProcessData.tAlertPointPump
		SET
			DisplayName = @DisplayName
			, GlobalID = @GlobalID
			, ExternalID = @ExternalID
			, NDPumpID = @NDPumpID
			, AlertPointID = @AlertPointID
		WHERE AlertPointPumpID = @AlertPointPumpID;
		SET @UpsertAlertPointPumpID = @AlertPointPumpID;
	END

	SELECT @UpsertAlertPointPumpID;
END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertAlertPointPump] TO [TEUser]
    AS [dbo];

