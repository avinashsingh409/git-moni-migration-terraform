﻿
CREATE PROCEDURE ProcessData.spDeleteTags
    @TVP Base.tpIntList READONLY
    AS 
	UPDATE ProcessData.tTag SET ExistsOnServer = 0
	FROM ProcessData.tTag T
	INNER JOIN @TVP S ON S.id = T.PDTagID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spDeleteTags] TO [TEUser]
    AS [dbo];

