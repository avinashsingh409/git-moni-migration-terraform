CREATE PROCEDURE [ProcessData].[spInsertAssetVariableTypeTagMaps]
    @TVP ProcessData.tpAssetVariableTypeTagMap READONLY,
	@UserID nvarchar(max)
    AS 
begin
declare @now datetime = getdate()
declare @output Table(
		AssetVariableTypeTagMapID int not null,
		TagID int not null 
	);
	INSERT INTO ProcessData.tAssetVariableTypeTagMap (AssetID,VariableTypeID,TagID,CalcString,CreatedBy,ChangedBy,CreateDate,ChangeDate,WeightingVariableTypeID,WeightingValueTypeID,ValueTypeID,StatusVariableTypeID,StatusValueTypeID)
	OUTPUT inserted.AssetVariableTypeTagMapID, inserted.TagID INTO @output(AssetVariableTypeTagMapID,TagID)
	SELECT AssetID,VariableTypeID,TagID,CalcString,@UserID,@UserID,@now,@now,WeightingVariableTypeID,WeightingValueTypeID,ValueTypeID,StatusVariableTypeID,StatusValueTypeID
	FROM @TVP
	select * from @output
end
GO
GRANT EXECUTE
ON OBJECT::[ProcessData].[spInsertAssetVariableTypeTagMaps] TO [TEUser]
AS [dbo];
GO