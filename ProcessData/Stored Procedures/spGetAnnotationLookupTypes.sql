﻿CREATE PROCEDURE [ProcessData].[spGetAnnotationLookupTypes]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM ProcessData.tAnnotationTypePropertyType 

	SELECT * FROM ProcessData.tAnnotationType 

	SELECT * FROM ProcessData.tAnnotationCategory 

	SELECT * FROM ProcessData.tAnnotationTypeProperty 
	
END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spGetAnnotationLookupTypes] TO [TEUser]
    AS [dbo];

