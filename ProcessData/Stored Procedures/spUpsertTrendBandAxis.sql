﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendBandAxis]
	@TrendBandAxisID [int] = NULL,
	@PDTrendID [int],
	@Label NVARCHAR(MAX) = NULL,
	@Opposite bit = 0,
	@IsVertical bit = 0,
	@RenderAs NVARCHAR(255) = NULL,
	@BarWidth [int] = NULL,
	@Summarize NVARCHAR(255) = NULL,
	@Clockwise bit = NULL,
	@StartAngle real = NULL,
	@EndAngle real = NULL,
	@NewTrendBandAxisID int OUTPUT
AS
BEGIN
	SET @NewTrendBandAxisID = -1
		IF (@TrendBandAxisID IS NULL OR @TrendBandAxisID <= 0)
		BEGIN
			INSERT INTO ProcessData.tTrendBandAxis(
				PDTrendID
				, Label
				, Opposite
				, IsVertical
				, RenderAs
				, BarWidth
				, Summarize 
				, Clockwise
				, StartAngle
				, EndAngle
			)
			VALUES(
				@PDTrendID
				, @Label
				, @Opposite
				, @IsVertical
				, @RenderAs
				, @BarWidth
				, @Summarize
				, @Clockwise
				, @StartAngle
				, @EndAngle
			)
			SET @NewTrendBandAxisID = SCOPE_IDENTITY();
		END
		ELSE
		BEGIN
			UPDATE ProcessData.tTrendBandAxis
			SET
				PDTrendID    = @PDTrendID
				, Label      = @Label
				, Opposite   = @Opposite
				, IsVertical = @IsVertical
				, RenderAs   = @RenderAs
				, BarWidth   = @BarWidth
				, Summarize  = @Summarize
				, Clockwise  = @Clockwise
				, StartAngle = @StartAngle
				, EndAngle   = @EndAngle
			WHERE ProcessData.tTrendBandAxis.TrendBandAxisID = @TrendBandAxisID
			SET @NewTrendBandAxisID = @TrendBandAxisID;
		END
	END
	SELECT @NewTrendBandAxisID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendBandAxis] TO [TEUser]
    AS [dbo];
GO