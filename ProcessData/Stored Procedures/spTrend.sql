﻿
CREATE PROCEDURE [ProcessData].[spTrend]
	-- Add the parameters for the stored procedure here
	@securityUserID int = null, 
	@isPublic bit = 1,
	@gallery nvarchar(50) = null,
	@assetClassTypeID as int = null,
	@assetID as int = null,
	@trendKey as nvarchar(30) = null,
	@filterCategoryIDs Base.tpIntList READONLY
AS
BEGIN

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @paramDef NVARCHAR(200)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here

	SET @sql = N'SELECT [ProcessData].[tTrend].[PDTrendID]
		,[TrendDesc]
		,[Title]
		,[LegendVisible]
		,[LegendDock]
		,[LegendContentLayout]
		,[IsCustom]
		,[CreatedBy]
		,[ChangedBy]
		,[CreateDate]
		,[ChangeDate]
		,[IsPublic]
		,[DisplayOrder]
		,[ParentTrendID]
		,[TrendKey]
		,[TrendKeyUnique]
		,g.[GalleryTypeID] GalleryTypeID
		,g.[GalleryTypeDesc] GalleryTypeDesc
		,[ChartTypeID]
		,[FilterMin]
		,[FilterMax]
		,[FlatlineThreshold]
		,[Exclude]
		,[PinTypeID]
		,[TrendSource]
		,[ProjectId]
		,[Path]
		,[Math]
		,SummaryTypeID
		,XAxisGridlines
	FROM [ProcessData].[tTrend]
		LEFT JOIN [ProcessData].[tGalleryTypeMap] map ON [ProcessData].[tTrend].PDTrendID = map.PDTrendID
		LEFT JOIN [ProcessData].[tGalleryType] g ON map.GalleryTypeID = g.GalleryTypeID
		INNER JOIN [ProcessData].[ufnGetRoleSecuritizedTrendsFromUser](@securityUserId, @filterCategoryIDs, 7) trend ON [ProcessData].[tTrend].PDTrendID = trend.PDTrendID
	WHERE (1=1) ';

	IF (@isPublic = 1)
		SET @sql = @sql + 'AND ([ProcessData].[tTrend].IsPublic = 1) ';
	ELSE
		SET @sql = @sql + 'AND (CreatedBy = (SELECT TOP 1 Username from AccessControl.tUser WHERE SecurityUserID = @securityUserID)) ';

	IF (@gallery IS NULL)
		SET @sql = @sql + 'AND (map.GalleryTypeID is NULL) ';
	ELSE
		SET @sql = @sql + 'AND (@gallery = g.GalleryTypeDesc)) ';

	IF (@assetClassTypeID IS NOT NULL)
		SET @sql = @sql + 'AND (EXISTS (SELECT TrendID from ProcessData.tAssetClassTypeTrendMap WHERE ProcessData.tAssetClassTypeTrendMap.AssetClassTypeID = @assetClassTypeID AND ProcessData.tAssetClassTypeTrendMap.TrendID = [ProcessData].[tTrend].PDTrendID)) ';

	IF (@assetID IS NOT NULL)
		SET @sql = @sql + 'AND (EXISTS (SELECT TrendID from ProcessData.tAssetClassTypeTrendMap INNER JOIN Asset.tAsset ON ProcessData.tAssetClassTypeTrendMap.AssetClassTypeID = Asset.tAsset.AssetClassTypeID WHERE Asset.tAsset.AssetID = @assetID AND ProcessData.tAssetClassTypeTrendMap.TrendID = [ProcessData].[tTrend].PDTrendID))';

	IF (@trendKey IS NOT NULL)
		SET @sql = @sql + 'AND (TrendKeyUnique = @trendKey) ';

	SET @paramDef = N'@securityUserID int,	@isPublic bit, @gallery nvarchar(50), @assetClassTypeID as int, @assetID as int, @trendKey as nvarchar(30), @filterCategoryIDs Base.tpIntList READONLY';

	EXECUTE sp_executesql @sql, @paramDef, @securityUserID, @isPublic, @gallery, @assetClassTypeID, @assetID, @trendKey, @filterCategoryIDs

END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spTrend] TO [TEUser]
    AS [dbo];

