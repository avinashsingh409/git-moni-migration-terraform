﻿CREATE PROCEDURE [ProcessData].[spOrderedTagMaps] 	
	@securityUserID int,
	@assetID int,
	@selectedAssetID int,
	@name varchar(255) = null,
	@desc varchar(255) = null,
	@assetSearch varchar(255) = null,
	@variable varchar (255) = null,
	@units varchar(255) = null,
	@skip int,
	@take int
AS
    BEGIN

        SET NOCOUNT ON;

        IF @securityUserID <= 0
            BEGIN
                SET @selectedAssetID = @assetID;
            END;

        SELECT a.AssetID, CONVERT(INT, NULL) AS level
        INTO   #tmp_levels
        FROM   Asset.tAsset AS a
        JOIN   Asset.ufnGetAssetTreeBranch(@selectedAssetID) AS b
            ON a.assetid = b.AssetID;

        -- get descendants  
        INSERT INTO #tmp_levels ( AssetID, level )
        SELECT a.AssetID, NULL
        FROM   Asset.ufnAssetAncestors(@selectedAssetID) AS a;
        UPDATE a
        SET    a.level = CASE WHEN b.AncestryLevel < 0 THEN ( -b.AncestryLevel ) ELSE 0 END
        FROM   #tmp_levels AS a
        JOIN   Asset.tAssetHop AS b
            ON a.AssetID = b.EndingAssetId
        WHERE  b.HopAssetId = @selectedAssetID;

        /*Using my version of this function*/
        SELECT ISNULL(AssetID, -2147483647) AS AssetID
        INTO   #tmp_assets
        FROM   Asset.ufnGetAssetIdsForUserStartingAt(@securityUserID, @assetID)
        WHERE  AssetID IS NOT NULL
        OPTION ( RECOMPILE );

        /*This is nice*/
        CREATE CLUSTERED INDEX c_assets ON #tmp_assets ( AssetID );

        DECLARE @maxlevel AS INT;
        SELECT @maxlevel = MAX(level)
        FROM   #tmp_levels;

        DECLARE @parentAssetID AS INT;
        SELECT @parentAssetID = parentAssetID
        FROM   Asset.tAsset
        WHERE  AssetID = @selectedAssetID;

        SELECT @maxlevel = MAX(level)
        FROM   #tmp_levels;
        INSERT INTO #tmp_levels ( AssetID, level )
        SELECT a.AssetID, @maxlevel + 1
        FROM   Asset.tAsset AS a
        WHERE  a.ParentAssetID = @parentAssetID
        AND    a.AssetID <> @selectedAssetID;

        SELECT @maxlevel = MAX(level)
        FROM   #tmp_levels;

        -- go up the tree and get ancestors  
        UPDATE a
        SET    a.level = -b.AncestryLevel + @maxlevel + 2
        FROM   #tmp_levels AS a
        JOIN   Asset.tAssetHop AS b
            ON a.AssetID = b.HopAssetId
        WHERE  b.EndingAssetId = @selectedAssetID
        AND    a.AssetID <> @selectedAssetID;

        /*There's a single repartition streams operator that runs for >10s if we don't induce batch mode*/
        CREATE TABLE #batcheroo ( id INT NOT NULL, INDEX c CLUSTERED COLUMNSTORE );

        /*Put this in a temp table first*/
        WITH T1
            AS
             (
                 SELECT     ROW_NUMBER() OVER ( ORDER BY ISNULL(l.level, 999),
                                                         asset.DisplayOrder,
                                                         asset.AssetAbbrev,
                                                         ISNULL(VariableAbbrev, 'ZZZ'),
                                                         CASE WHEN t.TagDesc = '' THEN t.TagName ELSE t.tagdesc END,
                                                         t.PDTagID ) AS ROW,
                            map.AssetVariableTypeTagMapID
                 FROM       ProcessData.tAssetVariableTypeTagMap AS map
                 INNER JOIN ProcessData.tTag AS t
                     ON map.TagID = t.PDTagID
                 INNER JOIN #tmp_assets AS ast
                     ON map.AssetID = ast.AssetID
                 INNER JOIN Asset.tAsset AS asset
                     ON map.AssetID = asset.AssetID
                 INNER JOIN Asset.tAssetClassType AS ac
                     ON asset.AssetClassTypeID = ac.AssetClassTypeID
                 LEFT JOIN  #tmp_levels AS l
                     ON map.AssetID = l.AssetID
                 LEFT JOIN  ProcessData.tVariableType AS vt
                     ON map.VariableTypeID = vt.PDVariableID
                 /*Induce batch mode*/
                 LEFT JOIN  #batcheroo
                     ON 1 = 0
                 WHERE      ( @name IS NULL OR t.TagName LIKE '%' + @name + '%' )
                 AND        ( @desc IS NULL OR t.TagDesc LIKE '%' + @desc + '%' )
                 AND        ( @units IS NULL OR t.EngUnits LIKE '%' + @units + '%' )
                 AND        ( @variable IS NULL OR vt.VariableAbbrev LIKE '%' + @variable + '%' )
                 AND        ( @assetSearch IS NULL OR asset.AssetAbbrev LIKE '%' + @assetSearch + '%' )
             )
        SELECT   ISNULL(AssetVariableTypeTagMapID, -2147483647) AS AssetVariableTypeTagMapID,
                 ISNULL(T1.ROW, -2147483647) AS ROW
        INTO     #T1
        FROM     T1
        /*This means we need to recompile anyway*/
        WHERE    T1.ROW BETWEEN @skip + 1 AND @skip + @take
        AND      AssetVariableTypeTagMapID IS NOT NULL
        ORDER BY T1.ROW
        /*Control big overestimate in memory grant*/
        OPTION ( RECOMPILE, MAX_GRANT_PERCENT = 13 );

        /*Be kind rewind*/
        CREATE CLUSTERED INDEX c_whatever
            ON #T1 ( AssetVariableTypeTagMapID, ROW );

        SELECT     ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID,
                   ProcessData.tAssetVariableTypeTagMap.AssetID,
                   ProcessData.tAssetVariableTypeTagMap.VariableTypeID,
                   ProcessData.tAssetVariableTypeTagMap.TagID,
                   ProcessData.tAssetVariableTypeTagMap.CalcString,
                   ProcessData.tAssetVariableTypeTagMap.CreatedBy,
                   ProcessData.tAssetVariableTypeTagMap.ChangedBy,
                   ProcessData.tAssetVariableTypeTagMap.CreateDate,
                   ProcessData.tAssetVariableTypeTagMap.ChangeDate,
                   ProcessData.tAssetVariableTypeTagMap.ValueTypeID,
                   ProcessData.tAssetVariableTypeTagMap.WeightingVariableTypeID,
                   ProcessData.tAssetVariableTypeTagMap.WeightingValueTypeID,
                   ProcessData.tAssetVariableTypeTagMap.StatusVariableTypeID,
                   ProcessData.tAssetVariableTypeTagMap.StatusValueTypeID,
                   Asset.tAsset.ParentAssetID,
                   Asset.tAsset.AssetAbbrev,
                   Asset.tAsset.AssetDesc,
                   Asset.tAsset.AssetClassTypeID,
                   Asset.tAsset.AssetTreeNodeChildConfigurationTypeID,
                   Asset.tAsset.DisplayOrder,
                   Asset.tAsset.CreatedBy,
                   Asset.tAsset.ChangedBy,
                   Asset.tAsset.CreateDate,
                   Asset.tAsset.ChangeDate,
                   Asset.tAsset.GlobalID,
                   Asset.tAsset.IsHidden,
                   Asset.tAsset.Track,
                   Asset.tAssetClassType.AssetTypeID,
                   Asset.tAssetClassType.AssetClassTypeKey,
                   Asset.tAssetClassType.AssetClassTypeAbbrev,
                   Asset.tAssetClassType.AssetClassTypeDesc,
                   Asset.tAssetType.AssetTypeAbbrev,
                   Asset.tAssetType.AssetTypeDesc,
                   0 AS NumChildren,
                   ProcessData.tVariableType.PDVariableID,
                   ProcessData.tVariableType.VariableDesc,
                   ProcessData.tVariableType.VariableAbbrev,
                   ProcessData.tVariableType.VariableName,
                   ProcessData.tVariableType.EngUnits,
                   ProcessData.tVariableType.CalcString,
                   ProcessData.tVariableType.FunctionTypeID,
                   ProcessData.tVariableType.WeightingVariableTypeID,
                   ProcessData.tVariableType.DefaultValueTypeID,
                   ProcessData.tTag.PDTagID,
                   ProcessData.tTag.PDServerID,
                   ProcessData.tTag.TagName,
                   ProcessData.tTag.TagDesc,
                   ProcessData.tTag.EngUnits AS TagUnits,
                   ProcessData.tTag.ExistsOnServer,
                   ProcessData.tTag.ExternalID,
                   ProcessData.tTag.Qualifier,
                   ProcessData.tTag.Mapping,
                   ProcessData.tTag.GlobalID,
                   ProcessData.tTag.NDCreated,
                   ProcessData.tTag.NDPath,
                   ProcessData.tTag.NDName,
                   ProcessData.tTag.NDId,
                   ProcessData.tTag.Qualifier,
                   ProcessData.tTag.Mapping,
                   ProcessData.tValueType.ValueTypeID,
                   ProcessData.tValueType.ValueTypeDesc,
                   ProcessData.tValueType.ValueTypeKey,
                   ProcessData.tValueType.Comment,
                   ProcessData.tValueType.IsDefault,
                   ProcessData.tValueType.IssuePrecedence,
                   l.level
        FROM       ProcessData.tAssetVariableTypeTagMap
        JOIN       Asset.tAsset
            ON ProcessData.tAssetVariableTypeTagMap.AssetID = Asset.tAsset.AssetID
        JOIN       Asset.tAssetClassType
            ON Asset.tAsset.AssetClassTypeId = Asset.tAssetClassType.AssetClassTypeID
        JOIN       Asset.tAssetType
            ON Asset.tAssetClassType.AssetTypeID = Asset.tAssetType.AssetTypeID
        LEFT JOIN  ProcessData.tVariableType
            ON ProcessData.tAssetVariableTypeTagMap.VariableTypeID = ProcessData.tVariableType.PDVariableID
        LEFT JOIN  ProcessData.tTag
            ON ProcessData.tAssetVariableTypeTagMap.TagID = ProcessData.tTag.PDTagID
        LEFT JOIN  ProcessData.tValueType
            ON ProcessData.tAssetVariableTypeTagMap.ValueTypeID = ProcessData.tValueType.ValueTypeID
        LEFT JOIN  #tmp_levels AS l
            ON Asset.tAsset.AssetID = l.AssetID
        /*Join to the temp table instead*/
		INNER JOIN #T1 AS T1
            ON T1.AssetVariableTypeTagMapID = ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID
        ORDER BY   T1.ROW;

        DROP TABLE #tmp_levels;
        DROP TABLE #tmp_assets;

    END

GO

GRANT EXECUTE
	ON OBJECT::[ProcessData].[spOrderedTagMaps]  TO [TEUser]
	AS [dbo];
