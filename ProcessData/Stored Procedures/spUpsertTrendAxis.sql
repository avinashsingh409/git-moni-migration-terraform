﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendAxis]
	@TrendAxisID INT = NULL,
	@TrendID INT,
	@Axis INT,
	@Title NVARCHAR(255) = NULL,
	@Position TINYINT,
	@Min FLOAT = NULL,
	@Max FLOAT = NULL,
	@Step FLOAT = NULL,
	@MinorStep FLOAT = NULL,
	@IsDefault BIT,
	@GridLine BIT = NULL,
	@SecurityUserID int,
	@NewTrendAxisID int OUTPUT
AS
BEGIN
	DECLARE @Username NVARCHAR(255);
	SELECT @Username = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID;

	SET @NewTrendAxisID = -1
	IF(@TrendAxisID IS NULL OR @TrendAxisID <= 0)
	BEGIN
		INSERT INTO [ProcessData].[tTrendAxis]
			   ([PDTrendID]
			   ,[Axis]
			   ,[Title]
			   ,[Position]
			   ,[Min]
			   ,[Max]
			   ,[Step]
			   ,[MinorStep]
			   ,[IsDefault]
			   ,[CreatedBy]
			   ,[ChangedBy]
			   ,[CreateDate]
			   ,[ChangeDate]
			   ,[GridLine])
		 VALUES
			   (@TrendID
			   ,@Axis
			   ,@Title
			   ,@Position
			   ,@Min
			   ,@Max
			   ,@Step
			   ,@MinorStep
			   ,@IsDefault
			   ,@Username
			   ,@Username
			   ,GetDate()
			   ,GetDate()
			   ,@GridLine)
		   SET @NewTrendAxisID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE [ProcessData].[tTrendAxis]
		   SET [PDTrendID] = @TrendID
			  ,[Axis] = @Axis
			  ,[Title] = @Title
			  ,[Position] = @Position
			  ,[Min] = @Min
			  ,[Max] = @Max
			  ,[Step] = @Step
			  ,[MinorStep] = @MinorStep
			  ,[IsDefault] = @IsDefault
			  ,[ChangedBy] = @Username
			  ,[ChangeDate] = GetDate()
			  ,[GridLine] = @GridLine
		 WHERE PDTrendAxisID = @TrendAxisID
		
		SET @NewTrendAxisID = @TrendAxisID 
	END
	SELECT @NewTrendAxisID
END
GO 
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendAxis] TO [TEUser]
    AS [dbo];
GO