﻿
		CREATE PROCEDURE [ProcessData].[spSaveSqlHistorianData] 
			   @rows ProcessData.tpSqlHistorianData READONLY
		AS
		BEGIN
			SET NOCOUNT ON;
			DECLARE @recordsAffected int;
			SET @recordsAffected = 0;
			
			DECLARE @tagEarliestLatest TABLE (
				  TagId int NOT NULL,
				  Earliest datetime NOT NULL,
				  Latest datetime NOT NULL,
                  Archive nvarchar(255) NULL
			)
			
			INSERT INTO @tagEarliestLatest(TagId, Earliest, Latest, Archive)
				  SELECT r.PDTagID, min(r.Timestamp), max(r.Timestamp), r.Archive
				  FROM @rows r
				  GROUP BY r.PDTagID, r.Archive
			
			-- UPDATE common rows
			UPDATE ts
			SET ts.[Value] = r.[Value], ts.Status = r.Status
			FROM ProcessData.tSQLHistorian ts
			INNER JOIN @rows r ON ts.TagID = r.PDTagID AND ts.TimeStamp = r.Timestamp AND ((ts.Archive = r.Archive) OR (ISNULL(ts.Archive,r.Archive) IS NULL))
			
			SELECT @recordsAffected = @recordsAffected + @@ROWCOUNT

			-- DELETE rows from historian that are in range but not present on rows
			DELETE ts
			FROM ProcessData.tSQLHistorian ts
			INNER JOIN @tagEarliestLatest tel ON tel.TagId = ts.TagID AND ts.TimeStamp BETWEEN tel.Earliest AND tel.Latest AND ((ts.Archive = tel.Archive) OR (ISNULL(ts.Archive,tel.Archive) IS NULL))
			LEFT JOIN @rows r ON ts.TagID = r.PDTagID AND ts.TimeStamp = r.Timestamp AND ((ts.Archive = r.Archive) OR (ISNULL(ts.Archive,r.Archive) IS NULL))
			WHERE r.PDTagID IS NULL
			
			SELECT @recordsAffected = @recordsAffected + @@ROWCOUNT
			
			-- INSERT rows that are not yet in the historian table
			INSERT INTO ProcessData.tSQLHistorian( TagID, TimeStamp, [Value], Status, Archive)
			SELECT r.PDTagID, r.Timestamp, r.[Value], r.Status, r.Archive
			FROM @rows r
			LEFT JOIN ProcessData.tSQLHistorian ts ON ts.TagID = r.PDTagID AND ts.TimeStamp = r.Timestamp AND ((ts.Archive = r.Archive) OR (ISNULL(ts.Archive,r.Archive) IS NULL))
			WHERE ts.TagID IS NULL
				
			SELECT @recordsAffected = @recordsAffected + @@ROWCOUNT	
				
			--SELECT @recordsAffected
		END
GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spSaveSqlHistorianData] TO [TEUser]
    AS [dbo];

