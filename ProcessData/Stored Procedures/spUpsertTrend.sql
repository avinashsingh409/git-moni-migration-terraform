﻿CREATE PROCEDURE [ProcessData].[spUpsertTrend]
	@TrendID INT = NULL,
	@TrendDesc NVARCHAR(255),
	@Title NVARCHAR(255) = NULL,
	@LegendVisible bit,
	@LegendDock tinyint,
	@LegendContentLayout tinyint,
	@IsCustom bit,
	@IsPublic bit,
	@DisplayOrder int,
	@ParentTrendID INT = NULL,
	@ChartTypeID INT = NULL,
	@FilterMin float = NULL,
	@FilterMax float = NULL,
	@FlatlineThreshold float = NULL,
	@Exclude NVARCHAR(512) = NULL,
	@PinTypeID INT,
	@TrendSource NVARCHAR(255) = NULL,
	@ProjectID NVARCHAR(MAX) = NULL,
	@Path NVARCHAR(MAX) = NULL,
	@Math NVARCHAR(MAX) = NULL,
	@SecurityUserID int,
	@SummaryTypeID INT = NULL,
	@XAxisGridlines bit = 0,
	@NewTrendID int OUTPUT
AS
BEGIN
	DECLARE @Username NVARCHAR(255);
	SELECT @Username = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID;

	SET @NewTrendID = -1
	IF(@TrendID IS NULL OR @TrendID <= 0)
	BEGIN
	INSERT INTO [ProcessData].[tTrend]
           ([TrendDesc]
           ,[Title]
           ,[LegendVisible]
           ,[LegendDock]
           ,[LegendContentLayout]
           ,[IsCustom]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[IsPublic]
           ,[DisplayOrder]
           ,[ParentTrendID]
           ,[ChartTypeID]
           ,[FilterMin]
           ,[FilterMax]
           ,[FlatlineThreshold]
           ,[Exclude]
           ,[PinTypeID]
           ,[TrendSource]
           ,[ProjectId]
           ,[Path]
           ,[Math]
		   ,[SummaryTypeID]
		   ,[XAxisGridlines])
     VALUES
           (@TrendDesc
           ,@Title
           ,@LegendVisible
           ,@LegendDock
           ,@LegendContentLayout
           ,@IsCustom
           ,@Username
           ,@Username
           ,GetDate()
           ,GetDate()
           ,@IsPublic
           ,@DisplayOrder
           ,@ParentTrendID
           ,@ChartTypeID
           ,@FilterMin
           ,@FilterMax
           ,@FlatlineThreshold
           ,@Exclude
           ,@PinTypeID
           ,@TrendSource
           ,@ProjectID
           ,@Path
           ,@Math
		   ,@SummaryTypeID
		   ,@XAxisGridlines)
		   SET @NewTrendID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN

		UPDATE ProcessData.tTrend
		   SET TrendDesc =  @TrendDesc
		   ,Title= @Title 
		   ,LegendVisible = @LegendVisible
		   ,LegendDock = @LegendDock 
		   ,LegendContentLayout = @LegendContentLayout
		   ,IsCustom = @IsCustom 
		   ,IsPublic = @IsPublic 
		   ,ChangedBy= @Username
		   ,ChangeDate=GETDATE()
		   ,ParentTrendID = @ParentTrendID 
		   ,ChartTypeID = @ChartTypeID
		   ,FilterMin = @FilterMin 
		   ,FilterMax = @FilterMax 
		   ,FlatlineThreshold = @FlatlineThreshold 
		   ,Exclude = @Exclude 
		   ,PinTypeID = @PinTypeID 
		   ,TrendSource = @TrendSource 
		   ,Math = @Math 
		   ,SummaryTypeID = @SummaryTypeID
		   ,XAxisGridlines = @XAxisGridlines
		   WHERE PDTrendID = @TrendID 
		   SET @NewTrendID = @TrendID 
	END
	SELECT @NewTrendID
END
GO 
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrend] TO [TEUser]
    AS [dbo];
GO