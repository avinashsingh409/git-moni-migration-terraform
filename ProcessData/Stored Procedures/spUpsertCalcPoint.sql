﻿


-- spUpsertCalcPoint
CREATE PROCEDURE [ProcessData].[spUpsertCalcPoint]
	@CalcPointID int = NULL,
	@PDServerID int,
	@CalcPointName nvarchar(400),
	@CalcPointDesc nvarchar(400) = NULL,
	@ExternalID nvarchar(400),
	@NDReleaseCreated bit,
	@NDReleasePath nvarchar(400) = NULL,
	@NDReleaseName nvarchar(400) = NULL,
	@NDCreated bit,
	@NDPath nvarchar(400) = NULL,
	@NDName nvarchar(400) = NULL,
	@SecurityUserID int,
	@ChangeDate datetime,
	@NDSnapshotAction int,
	@NDId nvarchar(400) = NULL,
	@NDReleaseId nvarchar(400) = NULL,
	@ReleaseNodeId nvarchar(400) = NULL,
	@ReleaseParentNodeId nvarchar(400) = NULL,
	@NewCalcPointID int OUTPUT
AS
BEGIN
	SET @NewCalcPointID = -1
		IF (@CalcPointID IS NULL)
		BEGIN
			INSERT INTO ProcessData.tCalcPoint(
				PDServerID
				, CalcPointName
				, CalcPointDesc
				, ExternalID
				, NDReleaseCreated
				, NDReleasePath
				, NDReleaseName
				, NDCreated
				, NDPath
				, NDName
				, CreatedBy
				, ChangedBy
				, NDSnapshotAction
				, NDId
				, NDReleaseId
				, ReleaseNodeId
				, ReleaseParentNodeId
			)
			VALUES(
				@PDServerID
				, @CalcPointName
				, @CalcPointDesc
				, @ExternalID
				, @NDReleaseCreated
				, @NDReleasePath
				, @NDReleaseName
				, @NDCreated
				, @NDPath
				, @NDName
				, @SecurityUserID
				, @SecurityUserID
				, @NDSnapshotAction
				, @NDId
				, @NDReleaseId
				, @ReleaseNodeId
				, @ReleaseParentNodeId
			)
			SET @NewCalcPointID = SCOPE_IDENTITY();
		END
		ELSE
		BEGIN
			UPDATE ProcessData.tCalcPoint
			SET
				PDServerID = @PDServerID
				, CalcPointName = @CalcPointName
				, CalcPointDesc = @CalcPointDesc
				, ExternalID = @ExternalID
				, NDReleaseCreated = @NDReleaseCreated
				, NDReleasePath = @NDReleasePath
				, NDReleaseName = @NDReleaseName
				, NDCreated = @NDCreated
				, NDPath = @NDPath
				, NDName = @NDName
				, ChangedBy = @SecurityUserID
				, ChangeDate = @ChangeDate
				, NDSnapshotAction = @NDSnapshotAction
				, NDId = @NDId
				, NDReleaseId = @NDReleaseId
				, ReleaseNodeId = @ReleaseNodeId
				, ReleaseParentNodeId = @ReleaseParentNodeId
			WHERE ProcessData.tCalcPoint.CalcPointID = @CalcPointID
			SET @NewCalcPointID = @CalcPointID;
		END
	END
	SELECT @NewCalcPointID
	


GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertCalcPoint] TO [TEUser]
    AS [dbo];

