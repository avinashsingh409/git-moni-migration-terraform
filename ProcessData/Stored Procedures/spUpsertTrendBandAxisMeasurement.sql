﻿
CREATE PROCEDURE [ProcessData].[spUpsertTrendBandAxisMeasurement]
	@TrendBandAxisMeasurementID [int] = NULL,
	@BarID [int] = NULL,
	@PDTrendID [int],
	@Axis INT,
	@Name NVARCHAR(255) = NULL,
	@Type NVARCHAR(255) = NULL,
	@Offset FLOAT = NULL,
	@TooltipStyle NVARCHAR(255) = NULL,
	@Symbol NVARCHAR(MAX) = NULL,
	@Color NVARCHAR(255) = NULL,
	@FillColor NVARCHAR(255) = NULL,
	@BorderThickness INT = NULL,
	@Invert BIT = 0,
	@Value1 FLOAT = NULL,
	@Value2 FLOAT = NULL,
	@Value3 FLOAT = NULL,
	@Value4 FLOAT = NULL,
	@Value5 FLOAT = NULL,
	@PDVariableID1 INT = NULL,
	@PDVariableID2 INT = NULL,
	@PDVariableID3 INT = NULL,
	@PDVariableID4 INT = NULL,
	@PDVariableID5 INT = NULL,
	@ValueTypeID1 INT = NULL,
	@ValueTypeID2 INT = NULL,
	@ValueTypeID3 INT = NULL,
	@ValueTypeID4 INT = NULL,
	@ValueTypeID5 INT = NULL,
	@AggregationType1 INT = NULL,
	@AggregationType2 INT = NULL,
	@AggregationType3 INT = NULL,
	@AggregationType4 INT = NULL,
	@AggregationType5 INT = NULL,
	@AttributeTypeID1 INT = NULL,
	@AttributeTypeID2 INT = NULL,
	@AttributeTypeID3 INT = NULL,
	@AttributeTypeID4 INT = NULL,
	@AttributeTypeID5 INT = NULL,
	@TagID1 INT = NULL,
	@TagID2 INT = NULL,
	@TagID3 INT = NULL,
	@TagID4 INT = NULL,
	@TagID5 INT = NULL,
	@Params1 NVARCHAR(MAX) = NULL,
	@Params2 NVARCHAR(MAX) = NULL,
	@Params3 NVARCHAR(MAX) = NULL,
	@Params4 NVARCHAR(MAX) = NULL,
	@Params5 NVARCHAR(MAX) = NULL,
	@IsFiltered BIT = 0,
	@FilterMin FLOAT = NULL,
	@FilterMax FLOAT = NULL,
	@FlatlineThreshold FLOAT = NULL,
	@Exclude NVARCHAR(512) = NULL,
	@ApplyToAll BIT = 0,
	@UseGlobalDataRetrieval BIT = 1,
	@Flags NVARCHAR(512) = NULL,
	@DisplayOrder int = NULL,
	@NewTrendBandAxisMeasurementID int OUTPUT
AS
BEGIN
	SET @NewTrendBandAxisMeasurementID = -1
		IF (@TrendBandAxisMeasurementID IS NULL OR @TrendBandAxisMeasurementID <= 0)
		BEGIN
			INSERT INTO [ProcessData].[tTrendBandAxisMeasurement]
				   ([PDTrendID]
				   ,[BarID]
				   ,[Axis]
				   ,[Name]
				   ,[Type]
				   ,[Offset]
				   ,[TooltipStyle]
				   ,[Symbol]
				   ,[Color]
				   ,[FillColor]
				   ,[BorderThickness]
				   ,[Invert]
				   ,[Value1]
				   ,[Value2]
				   ,[Value3]
				   ,[Value4]
				   ,[Value5]
				   ,[PDVariableID1]
				   ,[PDVariableID2]
				   ,[PDVariableID3]
				   ,[PDVariableID4]
				   ,[PDVariableID5]
				   ,[ValueTypeID1]
				   ,[ValueTypeID2]
				   ,[ValueTypeID3]
				   ,[ValueTypeID4]
				   ,[ValueTypeID5]
				   ,[AggregationType1]
				   ,[AggregationType2]
				   ,[AggregationType3]
				   ,[AggregationType4]
				   ,[AggregationType5]
				   ,[AttributeTypeID1]
				   ,[AttributeTypeID2]
				   ,[AttributeTypeID3]
				   ,[AttributeTypeID4]
				   ,[AttributeTypeID5]
				   ,[TagID1]
				   ,[TagID2]
				   ,[TagID3]
				   ,[TagID4]
				   ,[TagID5]
				   ,[Params1]
				   ,[Params2]
				   ,[Params3]
				   ,[Params4]
				   ,[Params5]
				   ,[IsFiltered]
				   ,[FilterMin]
				   ,[FilterMax]
				   ,[FlatlineThreshold]
				   ,[Exclude]
				   ,[ApplyToAll]
				   ,[UseGlobalDataRetrieval]
				   ,[Flags]
				   ,[DisplayOrder]
				  )
			 VALUES
					(@PDTrendID
					,@BarID
					,@Axis
					,@Name
					,@Type
					,@Offset
					,@TooltipStyle
					,@Symbol
					,@Color
					,@FillColor
					,@BorderThickness
					,@Invert
					,@Value1
					,@Value2
					,@Value3
					,@Value4
					,@Value5
					,@PDVariableID1
					,@PDVariableID2
					,@PDVariableID3
					,@PDVariableID4
					,@PDVariableID5
					,@ValueTypeID1
					,@ValueTypeID2
					,@ValueTypeID3
					,@ValueTypeID4
					,@ValueTypeID5
					,@AggregationType1
					,@AggregationType2
					,@AggregationType3
					,@AggregationType4
					,@AggregationType5
					,@AttributeTypeID1
					,@AttributeTypeID2
					,@AttributeTypeID3
					,@AttributeTypeID4
					,@AttributeTypeID5
					,@TagID1
					,@TagID2
					,@TagID3
					,@TagID4
					,@TagID5
					,@Params1
					,@Params2
					,@Params3
					,@Params4
					,@Params5
					,@IsFiltered
					,@FilterMin
					,@FilterMax
					,@FlatlineThreshold
					,@Exclude
					,@ApplyToAll
					,@UseGlobalDataRetrieval
					,@Flags
					,@DisplayOrder)
			SET @NewTrendBandAxisMeasurementID = SCOPE_IDENTITY();
		END
		ELSE
		BEGIN
			UPDATE [ProcessData].[tTrendBandAxisMeasurement]
				SET [PDTrendID] = @PDTrendID
				  ,[BarID] = @BarID
				  ,[Axis] = @Axis
				  ,[Name] = @Name
				  ,[Type] = @Type
				  ,[Offset] = @Offset
				  ,[TooltipStyle] = @TooltipStyle
				  ,[Symbol] = @Symbol
				  ,[Color] = @Color
				  ,[FillColor] = @FillColor
				  ,[BorderThickness] = @BorderThickness
				  ,[Invert] = @Invert
				  ,[Value1] = @Value1
				  ,[Value2] = @Value2
				  ,[Value3] = @Value3
				  ,[Value4] = @Value4
				  ,[Value5] = @Value5
				  ,[PDVariableID1] = @PDVariableID1
				  ,[PDVariableID2] = @PDVariableID2
				  ,[PDVariableID3] = @PDVariableID3
				  ,[PDVariableID4] = @PDVariableID4
				  ,[PDVariableID5] = @PDVariableID5
				  ,[ValueTypeID1] = @ValueTypeID1
				  ,[ValueTypeID2] = @ValueTypeID2
				  ,[ValueTypeID3] = @ValueTypeID3
				  ,[ValueTypeID4] = @ValueTypeID4
				  ,[ValueTypeID5] = @ValueTypeID5
				  ,[AggregationType1] = @AggregationType1
				  ,[AggregationType2] = @AggregationType2
				  ,[AggregationType3] = @AggregationType3
				  ,[AggregationType4] = @AggregationType4
				  ,[AggregationType5] = @AggregationType5
				  ,[AttributeTypeID1] = @AttributeTypeID1
				  ,[AttributeTypeID2] = @AttributeTypeID2
				  ,[AttributeTypeID3] = @AttributeTypeID3
				  ,[AttributeTypeID4] = @AttributeTypeID4
				  ,[AttributeTypeID5] = @AttributeTypeID5
				  ,[TagID1] = @TagID1
				  ,[TagID2] = @TagID2
				  ,[TagID3] = @TagID3
				  ,[TagID4] = @TagID4
				  ,[TagID5] = @TagID5
				  ,[Params1] = @Params1
				  ,[Params2] = @Params2
				  ,[Params3] = @Params3
				  ,[Params4] = @Params4
				  ,[Params5] = @Params5
				  ,[IsFiltered] = @IsFiltered
				  ,[FilterMin] = @FilterMin
				  ,[FilterMax] = @FilterMax
				  ,[FlatlineThreshold] = @FlatlineThreshold
				  ,[Exclude] = @Exclude
				  ,[ApplyToAll] = @ApplyToAll
				  ,[UseGlobalDataRetrieval] = @UseGlobalDataRetrieval
				  ,[Flags] = @Flags
			      ,[DisplayOrder] = @DisplayOrder
			 WHERE TrendBandAxisMeasurementID = @TrendBandAxisMeasurementID
			SET @NewTrendBandAxisMeasurementID = @TrendBandAxisMeasurementID;
		END
	END
	SELECT @NewTrendBandAxisMeasurementID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendBandAxisMeasurement] TO [TEUser]
    AS [dbo];
GO