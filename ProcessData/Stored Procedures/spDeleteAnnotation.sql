﻿CREATE PROCEDURE [ProcessData].[spDeleteAnnotation] 
	   @securityUserID int, 
	   @annotationID bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @assetID int;
	
	SELECT @assetID = [AssetID]
	FROM [ProcessData].[tAnnotation]
	WHERE [AnnotationID] = @annotationID;
	    
	DELETE ProcessData.tAnnotationPropertyMap 
	FROM ProcessData.tAnnotationPropertyMap tapm
	INNER JOIN ProcessData.tAnnotation ta ON ta.AnnotationID = tapm.AnnotationID	
	WHERE ta.AnnotationID = @annotationID AND Asset.ufnDoesUserHaveAccessToAsset(ta.AssetID,@securityUserID,-1)=1
	
	DELETE ProcessData.tAnnotation	
	FROM ProcessData.tAnnotation ta 	
	WHERE ta.AnnotationID = @annotationID AND Asset.ufnDoesUserHaveAccessToAsset(ta.AssetID,@securityUserID,-1)=1
	
END;

GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spDeleteAnnotation] TO [TEUser]
    AS [dbo];

