﻿CREATE PROCEDURE [ProcessData].[spGetPDServerForAsset]
				@assetid INT,
				@ServerID INT OUTPUT
AS
BEGIN
	SET @ServerID = 0 
	DECLARE @TempServerID INT
	Set @TempServerID = (SELECT PDServerID FROM ProcessData.tServer tS WHERE tS.AssetID = @assetid
	AND ts.PDServerTypeID = 14)
	IF (@TempServerID is not null AND @TempServerID <> 0)
		Begin
		SET @ServerID = @TempServerID
		end
END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spGetPDServerForAsset] TO [TEUser]
    AS [dbo];
GO