﻿
CREATE PROCEDURE [ProcessData].[spAddTagSync] 
	@pdServerId int,
	@userName nvarchar(max) = 'bv',
	@password nvarchar(max) = 'password'
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @unitAbbrev NVARCHAR(50) = ProcessData.ufnGetPDServerAssetAbbreviation(@pdServerId);
	
	IF (@unitAbbrev IS NULL)
		RETURN 0;

	DECLARE @pdGUID UNIQUEIDENTIFIER = (SELECT GlobalID FROM ProcessData.tServer WHERE PDServerID = @pdServerId);

	--TagSync
	DECLARE @tsGUID UNIQUEIDENTIFIER = NEWID()
	INSERT INTO Configuration.tDeployedApp (ApplicationID, AppTypeID, DisplayName, InternalName, AppReference) Values (@tsGUID, 4, 'nDTagSynchronization_' + @unitAbbrev, 'nDTagSynchronization_' + @unitAbbrev, @pdGUID)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'LogDir', '.\Log', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'LogFile', 'nDTagSynchronization_' + @unitAbbrev + '.log', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'UseSlackLogger', '1', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'OAuthUserName', @userName, null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'OAthUserPassword', @password, null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'NDEndpoint', 'prod', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'PDServerID', CONVERT(NVARCHAR(5), @pdServerId), null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'ApplicationName', 'nDTagSync_' + @unitAbbrev, null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunSetting', '1', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunEverySeconds', '60', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunCP', 'true', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunCPStore', 'true', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunSnapshot', 'true', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunSchemaChangeSnapshot', 'true', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'RunDPStore', 'true', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'ProcessExisting', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'PumpDestroyCreate', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'DeleteExistingCPReleaseVariables', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'ScheduleTimesX', '07:00 AM;08:00 AM;09:00 AM;10:00 AM;11:00 AM;12:00 PM;01:00 PM;02:00 PM;03:00 PM;04:00 PM;05:00 PM;06:00 PM', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'SimStartExpr', 'sim[', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'SimEndExpr', ']', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'PointFrequency', '60', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@tsGUID, 4, 'CreateInactivePoints', 'false', null, 0)
END
GO

GRANT EXECUTE
	ON OBJECT::[ProcessData].[spAddTagSync] TO [TEUser]
	AS [dbo]
GO