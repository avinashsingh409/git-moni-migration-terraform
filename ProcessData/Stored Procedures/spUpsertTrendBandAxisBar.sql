﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendBandAxisBar]
	@TrendBandAxisBarID [int] = NULL,
	@PDTrendID [int],
	@Label NVARCHAR(255) = NULL,
	@AssetID [int] = NULL,
	@AssetClassTypeID [int] = NULL,
	@CriteriaObjectID [int] = NULL,
	@IncludeSelf [bit] = 0,
	@IncludeChildren [bit] = 0,
	@IncludeDescendants [bit] = 0,
	@IncludeParents [bit] = 0,
	@IncludeSiblings [bit] = 0,
	@IncludeAncestors [bit] = 0,
	@IncludeCousins [bit] = 0,
	@IncludeSecondCousins [bit] = 0,
	@IncludeUnit [bit] = 0,
	@IncludeUnitDescendants [bit] = 0,
	@IncludeAllAssets [bit] = 0,
	@PDVariableID [int] = NULL,
	@ValueTypeID [bit] = NULL,
	@PDTagID [int] = NULL,
	@TimeDivision NVARCHAR(255) = NULL, --Should be changed to foreign key to type table 
	@DisplayOrder int = 10,
	@Context NVARCHAR(255) = NULL, --Should be changed to foreign key to type table 
	@NewTrendBandAxisBarID int OUTPUT
AS
BEGIN
	SET @NewTrendBandAxisBarID = -1
		IF (@TrendBandAxisBarID IS NULL OR @TrendBandAxisBarID <= 0)
		BEGIN
			INSERT INTO [ProcessData].[tTrendBandAxisBar]
           ([PDTrendID]
           ,[Label]
           ,[AssetID]
           ,[AssetClassTypeID]
           ,[IncludeSelf]
           ,[IncludeChildren]
           ,[IncludeDescendants]
           ,[IncludeParents]
           ,[IncludeSiblings]
           ,[IncludeAncestors]
           ,[IncludeCousins]
           ,[IncludeSecondCousins]
           ,[IncludeUnit]
           ,[IncludeUnitDescendants]
           ,[IncludeAllAssets]
		   ,[CriteriaObjectID]
           ,[PDVariableID]
           ,[ValueTypeID]
           ,[PDTagID]
           ,[TimeDivision]
           ,[DisplayOrder]
           ,[Context])
     VALUES
           (@PDTrendID
           ,@Label
           ,@AssetID
           ,@AssetClassTypeID
           ,@IncludeSelf
           ,@IncludeChildren
           ,@IncludeDescendants
           ,@IncludeParents
           ,@IncludeSiblings
           ,@IncludeAncestors
           ,@IncludeCousins
           ,@IncludeSecondCousins
           ,@IncludeUnit
           ,@IncludeUnitDescendants
           ,@IncludeAllAssets
		   ,@CriteriaObjectID
           ,@PDVariableID
           ,@ValueTypeID
           ,@PDTagID
           ,@TimeDivision
           ,@DisplayOrder
           ,@Context)
			SET @NewTrendBandAxisBarID = SCOPE_IDENTITY();
		END
		ELSE
		BEGIN
			UPDATE [ProcessData].[tTrendBandAxisBar]
			SET
			PDTrendID = @PDTrendID
           ,Label = @Label
           ,AssetID = @AssetID
           ,AssetClassTypeID = @AssetClassTypeID
           ,IncludeSelf = @IncludeSelf
           ,IncludeChildren = @IncludeChildren
           ,IncludeDescendants = @IncludeDescendants
           ,IncludeParents = @IncludeParents
           ,IncludeSiblings = @IncludeSiblings
           ,IncludeAncestors = @IncludeAncestors
           ,IncludeCousins = @IncludeCousins
           ,IncludeSecondCousins = @IncludeSecondCousins
           ,IncludeUnit = @IncludeUnit
           ,IncludeUnitDescendants = @IncludeUnitDescendants
           ,IncludeAllAssets = @IncludeAllAssets
           ,PDVariableID = @PDVariableID
           ,ValueTypeID = @ValueTypeID
		   ,CriteriaObjectID = @CriteriaObjectID
           ,PDTagID = @PDTagID
           ,TimeDivision = @TimeDivision
           ,DisplayOrder = @DisplayOrder
           ,Context = @Context
			WHERE [ProcessData].[tTrendBandAxisBar].TrendBandAxisBarID = @TrendBandAxisBarID
			SET @NewTrendBandAxisBarID = @TrendBandAxisBarID;
		END
	END
	SELECT @NewTrendBandAxisBarID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendBandAxisBar] TO [TEUser]
    AS [dbo];
GO