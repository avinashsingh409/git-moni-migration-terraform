﻿CREATE PROCEDURE ProcessData.spInsertTags
    @TVP ProcessData.tpTag READONLY,
	@SecurityUserID int
    AS 
begin
	declare @output Table(
		PDTagID int not null,
		TagName nvarchar(400) not null --TagName has a unique key constraint, so it's a valid key to go back to the IDs with
	);
	INSERT INTO ProcessData.tTag (PDServerID,TagName,TagDesc,EngUnits,ExistsOnServer,ExternalID,Qualifier,Mapping,CreatedBy,ChangedBy,GlobalID,NDCreated,NDPath,NDName,NDId)
	OUTPUT inserted.PDTagID, inserted.TagName INTO @output(PDTagID,TagName)
	SELECT PDServerID,TagName,TagDesc,EngUnits,1,ExternalID,Qualifier,Mapping,@SecurityUserID,@SecurityUserID,GlobalID,NDCreated,NDPath,NDName,NDId
	FROM @TVP
	select * from @output
end
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spInsertTags] TO [TEUser]
    AS [dbo];
GO