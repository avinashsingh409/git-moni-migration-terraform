CREATE PROCEDURE [ProcessData].[spSetPDServerForNewAsset]
				@assetid INT,
				@result INT OUTPUT
AS
BEGIN
	DECLARE @availablePDServerID int = 0
	DECLARE @assetComment nvarchar(255)
		
	IF EXISTS (SELECT ServerDesc FROM ProcessData.tServer tS WHERE tS.AssetID = @assetid)
		BEGIN
		SET @result = 0 
		END
	ELSE
		BEGIN
			SET @availablePDServerID = (SELECT TOP 1 PDServerID from ProcessData.tServer
										WHERE PDServerTypeID = 14 AND Active <> 1 AND AssetID is NULL
										AND Comment = 'Available' order by PDServerID asc ) 
			IF @availablePDServerID <> 0
				BEGIN
				SET @assetComment = (SELECT AssetDesc FROM Asset.tAsset tA where tA.AssetID = @assetid)
				SET @result = 1 
				
					UPDATE ProcessData.tServer
					SET Comment = @assetComment, Active = 1, AssetID = @assetid 
					WHERE PDServerID = @availablePDServerID
				
				END
			ELSE
				BEGIN
				SET @result = 0 
				END
		END
	Select @result
END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spSetPDServerForNewAsset] TO [TEUser]
    AS [dbo];
GO
