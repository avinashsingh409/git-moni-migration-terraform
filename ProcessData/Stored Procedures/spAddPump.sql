﻿
CREATE PROCEDURE [ProcessData].[spAddPump] 
	@pdServerId int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @unitAbbrev NVARCHAR(50) = ProcessData.ufnGetPDServerAssetAbbreviation(@pdServerId);
	
	IF @unitAbbrev IS NULL
		RETURN 0;

	DECLARE @archiveDir NVARCHAR(MAX) = ProcessData.ufnGetArchiveDirectory(@pdServerId);

	IF @archiveDir IS NULL
		RETURN 0;
	
	DECLARE @pdGUID UNIQUEIDENTIFIER = (SELECT GlobalID FROM ProcessData.tServer WHERE PDServerID = @pdServerId);

	DECLARE @pumpGUID UNIQUEIDENTIFIER = NEWID()
	INSERT INTO Configuration.tDeployedApp (ApplicationID, AppTypeID, DisplayName, InternalName, AppReference) Values (@pumpGUID, 3, 'OPM2NDTransfer_' + @unitAbbrev, 'OPM2NDTransfer_' + @unitAbbrev, @pdGUID)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'ApplicationName', 'OPM2NDTransfer_' + @unitAbbrev, null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'LogDir', '.\Log', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'LogLevel', 'Info', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'LogFile', 'OPM2NDTransfer_' + @unitAbbrev + '.log', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'LogFileCount', '5', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'NDEndpoint', '1', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'PDServerID', CONVERT(NVARCHAR(5), @pdServerId), null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'AllowCalcPoints', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'AllowInactivePoints', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'ArchiveDirectory', @archiveDir, null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'ExitNow', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'DestroyPump', 'false', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'MinuteInterval', '5', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'MaxMinutesLimit', '15', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'BackfillMinutesLimit', '120', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'UpdateWarningMinutes', '10', null, 0)
	INSERT INTO Configuration.tAppSettings (GlobalID, AppTypeID, KeyName, Value, Comment, LocalOverRide) VALUES (@pumpGUID, 3, 'CycleDelay', '15', null, 0)
END
GO

GRANT EXECUTE
	ON OBJECT::[ProcessData].[spAddPump] TO [TEUser]
	AS [dbo]
GO