﻿CREATE PROCEDURE [ProcessData].[spUpsertTrendArea]
@TrendAreaID INT = NULL,
@PDTrendID INT,
@Axes NVARCHAR(255),
@Size INT = NULL,
@Invert BIT = 0,
@DisplayOrder INT = 10,
@NewTrendAreaID int OUTPUT
AS
BEGIN
	SET @NewTrendAreaID = -1
		IF (@TrendAreaID IS NULL OR @TrendAreaID <= 0)
		BEGIN
			INSERT INTO [ProcessData].[tTrendArea]
           ([PDTrendID]
           ,[Axes]
           ,[Size]
           ,[Invert]
           ,[DisplayOrder])
     VALUES
           (@PDTrendID
           ,@Axes
           ,@Size
           ,@Invert
           ,@DisplayOrder)
		   SET @NewTrendAreaID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
	UPDATE [ProcessData].[tTrendArea]
	SET [PDTrendID] = @PDTrendID
      ,[Axes] = @Axes
      ,[Size] = @Size
      ,[Invert] = @Invert
      ,[DisplayOrder] = @DisplayOrder
	WHERE TrendAreaID = @TrendAreaID 
	SET @NewTrendAreaID = @TrendAreaID;
	END

END 
SELECT @NewTrendAreaID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendArea] TO [TEUser]
    AS [dbo];
GO