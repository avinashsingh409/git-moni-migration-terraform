﻿CREATE PROCEDURE [ProcessData].[spSaveNewAnnotationCategory] 
	   @name nvarchar(128),
	   @description nvarchar(255),
	   @icon nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO ProcessData.tAnnotationCategory	(AnnotationCategoryName, AnnotationCategoryDesc, Icon)
	VALUES(@name, @description, @icon)
	
	SELECT SCOPE_IDENTITY();		
END;
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spSaveNewAnnotationCategory] TO [TEUser]
    AS [dbo];

