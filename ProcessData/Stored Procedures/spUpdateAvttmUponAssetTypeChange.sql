CREATE PROCEDURE ProcessData.spUpdateAvttmUponAssetTypeChange
	@assetID INT,
	@oldAssetClassTypeID INT,
	@newAssetClassTypeID INT,
	@securityUser nvarchar(max)
AS
BEGIN

	--Store variableTypeIds of the new AssetClassType in temporary table
	DECLARE @newClassTypeVariableTypeIDs table (id int);
	INSERT INTO @newClassTypeVariableTypeIDs(id) 
	(SELECT VariableTypeID FROM ProcessData.tAssetClassTypeVariableTypeMap  WHERE AssetClassTypeID = @NewAssetClassTypeID);
	
	----Scenario where we update the VariableTypeID to NULL if it is not associated with newAssetClassType and TagID is NOT NULL
	UPDATE ProcessData.tAssetVariableTypeTagMap
	SET VariableTypeID = null
	WHERE VariableTypeID IS NOT NULL 
	AND VariableTypeID NOT IN (SELECT id FROM @newClassTypeVariableTypeIDs)
	AND TagID IS NOT NULL
	AND AssetId=@AssetID

	----Scenario where we delete the AVTTM record if it is not associated with new AssetClassType and TagID is NULL
	DELETE FROM ProcessData.tAssetVariableTypeTagMap
	WHERE (VariableTypeID IS NULL OR VariableTypeID NOT IN (SELECT id FROM @newClassTypeVariableTypeIDs))
	AND TagID IS NULL
	AND AssetId=@AssetID
	
	----Scenario where we insert a new AVTTM record for variableTypes that exists only in newAssetClassType
	DECLARE @currentDate AS DATETIME;
	SET @currentDate = GETDATE();

	INSERT INTO ProcessData.tAssetVariableTypeTagMap(AssetID, VariableTypeID, CreatedBy, ChangedBy, CreateDate, ChangeDate) 
	(Select @AssetId, new.id, @securityUser, @securityUser, @currentDate, @currentDate
	FROM @newClassTypeVariableTypeIDs new
	WHERE NOT EXISTS 
	(
		SELECT VariableTypeID 
		FROM ProcessData.tAssetVariableTypeTagMap avttm 
		WHERE avttm.VariableTypeID = new.id AND avttm.AssetID = @AssetId)
	);

END
GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpdateAvttmUponAssetTypeChange] TO [TEUser]
    AS [dbo];
GO