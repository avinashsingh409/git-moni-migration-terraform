﻿CREATE PROCEDURE ProcessData.spUpdateTags
    @TVP ProcessData.tpTag READONLY,
	@SecurityUserID int
    AS 
	UPDATE ProcessData.tTag SET
	   PDServerID = S.PDServerID,
       TagName = S.TagName,
       TagDesc = S.TagDesc,
       EngUnits = S.EngUnits,
       ExistsOnServer = S.ExistsOnServer,
       ExternalID = S.ExternalID,
       Qualifier = S.Qualifier,
       Mapping = S.Mapping,
       GlobalID = S.GlobalID,
       NDCreated = S.NDCreated,
       NDPath = S.NDPath,
       NDName = S.NDName,
	   NDId = S.NDId,
       ChangedBy = @SecurityUserID
	FROM ProcessData.tTag T
	INNER JOIN @TVP S ON S.PDTagID = T.PDTagID
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpdateTags] TO [TEUser]
    AS [dbo];
GO
