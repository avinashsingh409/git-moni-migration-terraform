﻿
CREATE PROCEDURE [ProcessData].[spDeleteCalcPoint]
	@CalcPointID int
AS
BEGIN
	DECLARE @Result int = 0;
	DELETE FROM ProcessData.tCalcPoint WHERE CalcPointID = @CalcPointID
	SET @Result = @@ROWCOUNT;
	
	SELECT @Result;
END
GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spDeleteCalcPoint] TO [TEUser]
    AS [dbo];

