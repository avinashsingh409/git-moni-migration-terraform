﻿

CREATE PROCEDURE [ProcessData].[spUpsertTrendSeries]
	@TrendSeriesID INT = NULL,
	@TrendID INT,
	@PDTagID INT = NULL,
	@PDVariableID INT = NULL,
	@IsMain BIT,
	@DisplayOrder INT,
	@Axis INT,
	@DisplayText NVARCHAR(255) = NULL,
	@UseDisplayText BIT,
	@ValueTypeID INT = NULL,
	@IsXAxis BIT,
	@IsFiltered BIT,
	@FilterMin FLOAT = NULL,
	@FilterMax FLOAT = NULL,
	@ShowBestFitLine BIT,
	@AssetClassTypeID INT = NULL,
	@ScaleMin FLOAT = NULL,
	@ScaleMax FLOAT = NULL,
	@ScaleDecimals INT = NULL,
	@IncludeSelf BIT,
	@IncludeChildren BIT,
	@IncludeDescendants BIT,
	@IncludeParents BIT,
	@IncludeSiblings BIT,
	@IncludeAncestors BIT,
	@IncludeCousins BIT,
	@IncludeSecondCousins BIT,
	@IncludeUnit BIT,
	@IncludeUnitDescendants BIT,
	@IncludeAllAssets BIT,
	@ChartTypeID INT = NULL,
	@IsZAxis BIT = NULL,
	@FlatlineThreshold FLOAT = NULL,
	@Exclude NVARCHAR(512) = NULL,
	@Color NVARCHAR(56) = NULL,
	@Stack NVARCHAR(56) = NULL,
	@StackType NVARCHAR(56) = NULL,
	@ApplyToAll BIT,
	@Archive NVARCHAR(255) = NULL,
	@UseGlobalDataRetrieval BIT,
	@SecurityUserID int,
	@SummaryTypeID int,
	@NewTrendSeriesID int OUTPUT
AS
BEGIN
	DECLARE @Username NVARCHAR(255);
	SELECT @Username = Username FROM AccessControl.tUser WHERE SecurityUserID = @SecurityUserID;

	SET @NewTrendSeriesID = -1
	IF(@TrendSeriesID IS NULL OR @TrendSeriesID <= 0)
	BEGIN
		INSERT INTO [ProcessData].[tTrendSeries]
           ([PDTrendID]
           ,[PDTagID]
           ,[PDVariableID]
           ,[IsMain]
           ,[DisplayOrder]
           ,[Axis]
           ,[DisplayText]
           ,[CreatedBy]
           ,[ChangedBy]
           ,[CreateDate]
           ,[ChangeDate]
           ,[UseDisplayText]
           ,[ValueTypeID]
           ,[IsXAxis]
           ,[IsFiltered]
           ,[FilterMin]
           ,[FilterMax]
           ,[ShowBestFitLine]
           ,[AssetClassTypeID]
           ,[ScaleMin]
           ,[ScaleMax]
           ,[ScaleDecimals]
           ,[IncludeSelf]
           ,[IncludeChildren]
           ,[IncludeDescendants]
           ,[IncludeParents]
           ,[IncludeSiblings]
           ,[IncludeAncestors]
           ,[IncludeCousins]
           ,[IncludeSecondCousins]
           ,[IncludeUnit]
           ,[IncludeUnitDescendants]
           ,[IncludeAllAssets]
           ,[ChartTypeID]
           ,[IsZAxis]
           ,[FlatlineThreshold]
           ,[Exclude]
           ,[Color]
           ,[Stack]
           ,[StackType]
           ,[ApplyToAll]
           ,[Archive]
           ,[UseGlobalDataRetrieval]
		   ,[SummaryTypeID])
     VALUES
           (@TrendID
           ,@PDTagID
           ,@PDVariableID
           ,@IsMain
           ,@DisplayOrder
           ,@Axis
           ,@DisplayText
           ,@Username
           ,@Username
           ,GetDate()
           ,GetDate()
           ,@UseDisplayText
           ,@ValueTypeID
           ,@IsXAxis
           ,@IsFiltered
           ,@FilterMin
           ,@FilterMax
           ,@ShowBestFitLine
           ,@AssetClassTypeID
           ,@ScaleMin
           ,@ScaleMax
           ,@ScaleDecimals
           ,@IncludeSelf
           ,@IncludeChildren
           ,@IncludeDescendants
           ,@IncludeParents
           ,@IncludeSiblings
           ,@IncludeAncestors
           ,@IncludeCousins
           ,@IncludeSecondCousins
           ,@IncludeUnit
           ,@IncludeUnitDescendants
           ,@IncludeAllAssets
           ,@ChartTypeID
           ,@IsZAxis
           ,@FlatlineThreshold
           ,@Exclude
           ,@Color
           ,@Stack
           ,@StackType
           ,@ApplyToAll
           ,@Archive
           ,@UseGlobalDataRetrieval
		   ,@SummaryTypeID)
		   SET @NewTrendSeriesID = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE [ProcessData].[tTrendSeries]
		   SET [PDTrendID] = @TrendID
			  ,[PDTagID] = @PDTagID
			  ,[PDVariableID] = @PDVariableID
			  ,[IsMain] = @IsMain
			  ,[DisplayOrder] = @DisplayOrder
			  ,[Axis] = @Axis
			  ,[DisplayText] = @DisplayText
			  ,[ChangedBy] = @Username
			  ,[ChangeDate] = GetDate()
			  ,[UseDisplayText] = @UseDisplayText
			  ,[ValueTypeID] = @ValueTypeID
			  ,[IsXAxis] = @IsXAxis
			  ,[IsFiltered] = @IsFiltered
			  ,[FilterMin] = @FilterMin
			  ,[FilterMax] = @FilterMax
			  ,[ShowBestFitLine] = @ShowBestFitLine
			  ,[AssetClassTypeID] = @AssetClassTypeID
			  ,[ScaleMin] = @ScaleMin
			  ,[ScaleMax] = @ScaleMax
			  ,[ScaleDecimals] = @ScaleDecimals
			  ,[IncludeSelf] = @IncludeSelf
			  ,[IncludeChildren] = @IncludeChildren
			  ,[IncludeDescendants] = @IncludeDescendants
			  ,[IncludeParents] = @IncludeParents
			  ,[IncludeSiblings] = @IncludeSiblings
			  ,[IncludeAncestors] = @IncludeAncestors
			  ,[IncludeCousins] = @IncludeCousins
			  ,[IncludeSecondCousins] = @IncludeSecondCousins
			  ,[IncludeUnit] = @IncludeUnit
			  ,[IncludeUnitDescendants] = @IncludeUnitDescendants
			  ,[IncludeAllAssets] = @IncludeAllAssets
			  ,[ChartTypeID] = @ChartTypeID
			  ,[IsZAxis] = @IsZAxis
			  ,[FlatlineThreshold] = @FlatlineThreshold
			  ,[Exclude] = @Exclude
			  ,[Color] = @Color
			  ,[Stack] = @Stack
			  ,[StackType] = @StackType
			  ,[ApplyToAll] = @ApplyToAll
			  ,[Archive] = @Archive
			  ,[UseGlobalDataRetrieval] = @UseGlobalDataRetrieval
			  ,[SummaryTypeID] = @SummaryTypeID
		 WHERE PDTrendSeriesID = @TrendSeriesID 
		
		SET @NewTrendSeriesID = @TrendSeriesID 
	END
	SELECT @NewTrendSeriesID
END
GO 
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spUpsertTrendSeries] TO [TEUser]
    AS [dbo];
GO