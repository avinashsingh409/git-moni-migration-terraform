﻿CREATE PROCEDURE [ProcessData].[spGetLatestAppendJobForAsset]
	@userId int,
	@assetId int
AS
BEGIN
	DECLARE @errorMsg varchar(255)
	DECLARE @globalId varchar(255)
	DECLARE @complete bit
	DECLARE @success bit
	DECLARE @inProgress bit
	DECLARE @openDate datetime
	DECLARE @closeDate datetime
	DECLARE @lastStatus varchar(255)
	DECLARE @lastRecord varchar(20)

	IF Asset.ufnDoesUserHaveAccessToAsset(@assetId, @userId, -1) = 0
	BEGIN
		SET @errorMsg = 'User ' + CONVERT(varchar(10), @userID) + ' does not have access to asset ' + CONVERT(varchar(10), @assetID) + '.';
		RAISERROR (@errorMsg, 13, -1, -1);
	END

	SET @globalId = CONVERT(varchar(255), (SELECT GlobalID FROM Asset.tAsset WHERE AssetID = @assetId))

	SELECT TOP 1 @complete = j.Complete, @success = j.Success, @lastStatus = cs.LastStatus, @openDate = j.OpenedDate, @closeDate = j.ClosedDate
	FROM Jobs.tJob j
	 JOIN Jobs.tJobCurrentStep cs ON cs.JobExtId = j.JobExtId
	 WHERE j.JobTypeId = 3 AND j.JobName LIKE 'nDAppendJob:' + @globalId
	 ORDER BY j.OpenedDate DESC

	IF @complete = 1 AND @success = 1
	BEGIN
		SET @inProgress = 0
		SET @lastRecord = RIGHT(@lastStatus, LEN(@lastStatus) - CHARINDEX('=', @lastStatus) -1)
		SELECT @inProgress AS InProgress, @complete AS Complete, @success AS Success, DATEADD(SS, CONVERT(bigint, @lastRecord),'19700101') as TransferEndTime
	END
	ELSE
	BEGIN
		SET @inProgress = 0
		IF @openDate is not null and @closeDate is null
		BEGIN
			SET @inProgress = 1
		END
		SELECT @inProgress as InProgress, @complete AS Complete, @success AS Success, NULL as TransferEndTime
	END

END
GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[spGetLatestAppendJobForAsset] TO [TEUser]
    AS [dbo];
GO