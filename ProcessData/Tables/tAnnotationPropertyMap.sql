﻿CREATE TABLE [ProcessData].[tAnnotationPropertyMap] (
    [AnnotationPropertyMapID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [AnnotationID]             BIGINT         NOT NULL,
    [AnnotationTypePropertyID] INT            NOT NULL,
    [ValueAsText]              NVARCHAR (255) NOT NULL,
    CONSTRAINT [tAnnotationPropertyMap_PK] PRIMARY KEY CLUSTERED ([AnnotationPropertyMapID] ASC),
    CONSTRAINT [tAnnotation_tAnnotationPropertyMap_FK1] FOREIGN KEY ([AnnotationID]) REFERENCES [ProcessData].[tAnnotation] ([AnnotationID]) ON DELETE CASCADE,
    CONSTRAINT [tAnnotationTypeProperty_tAnnotationPropertyMap_FK1] FOREIGN KEY ([AnnotationTypePropertyID]) REFERENCES [ProcessData].[tAnnotationTypeProperty] ([AnnotationTypePropertyID])
);


GO
/****** Object:  ForeignKey [tAnnotation_tAnnotationPropertyMap_FK1]    Script Date: 05/06/2016 15:20:24 ******/

GO


GO
/****** Object:  ForeignKey [tAnnotationTypeProperty_tAnnotationPropertyMap_FK1]    Script Date: 05/06/2016 15:20:24 ******/

GO

