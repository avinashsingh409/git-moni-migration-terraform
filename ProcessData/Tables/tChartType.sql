﻿CREATE TABLE [ProcessData].[tChartType] (
    [ChartTypeID]     INT            IDENTITY (1, 1) NOT NULL,
    [ChartTypeAbbrev] NVARCHAR (50)  NOT NULL,
    [ChartTypeDesc]   NVARCHAR (250) NOT NULL,
    [DisplayOrder]    INT            NOT NULL,
    CONSTRAINT [PK_tChartType] PRIMARY KEY CLUSTERED ([ChartTypeID] ASC)
);

