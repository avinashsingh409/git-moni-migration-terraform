﻿CREATE TABLE [ProcessData].[tTrendArea](
	[TrendAreaID] [int] IDENTITY(1,1) NOT NULL,
	[PDTrendID] [int] NOT NULL,
	[Axes] NVARCHAR(255) NULL, --Could be a mapping table
	[Size] int NULL,
	[Invert] bit NOT NULL DEFAULT((0)),
	[DisplayOrder] [int] NOT NULL DEFAULT((10)),
	 CONSTRAINT [PK_tTrendArea] PRIMARY KEY CLUSTERED 
	([TrendAreaID] ASC)
)
GO

ALTER TABLE [ProcessData].[tTrendArea] ADD CONSTRAINT [FK_tTrendArea_PDTrendID_tPDTrend] FOREIGN KEY([PDTrendID])
REFERENCES [ProcessData].[tTrend] ([PDTrendID])
ON DELETE CASCADE
GO
