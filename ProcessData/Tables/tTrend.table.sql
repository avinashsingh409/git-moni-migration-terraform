﻿CREATE TABLE [ProcessData].[tTrend] (
    [PDTrendID]           INT            IDENTITY (1, 1) NOT NULL,
    [TrendDesc]           NVARCHAR (255) NOT NULL,
    [Title]               NVARCHAR (255) NULL,
    [LegendVisible]       BIT            CONSTRAINT [DF_tPDTrend_LegendVisible] DEFAULT ((1)) NOT NULL,
    [LegendDock]          TINYINT        CONSTRAINT [DF_tPDTrend_LegendLocation] DEFAULT ((3)) NOT NULL,
    [LegendContentLayout] TINYINT        CONSTRAINT [DF_tPDTrend_LegendContentLayout] DEFAULT ((2)) NOT NULL,
    [IsCustom]            BIT            CONSTRAINT [DF_tPDTrend_IsCustom] DEFAULT ((0)) NOT NULL,
    [CreatedBy]           NVARCHAR (255) NOT NULL,
    [ChangedBy]           NVARCHAR (255) NOT NULL,
    [CreateDate]          DATETIME       CONSTRAINT [DF__tPDTrend__CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]          DATETIME       CONSTRAINT [DF__tPDTrend__ChangeDate] DEFAULT (getdate()) NOT NULL,
    [IsPublic]            BIT            CONSTRAINT [DF_tPDTrend_IsPublic] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]        INT            DEFAULT ((10)) NOT NULL,
    [TrendKey]            NVARCHAR (30)  NULL,
    [TrendKeyUnique]      AS             (coalesce([TrendKey],CONVERT([varchar](30),[PDTrendID],(0)))),
    [ParentTrendID]       INT            NULL,
    [ChartTypeID]         INT            NULL,
    [FilterMin] FLOAT NULL, 
    [FilterMax] FLOAT NULL, 
    [FlatlineThreshold] FLOAT NULL, 
    [Exclude] NVARCHAR(512) NULL, 
	[PinTypeID] int NOT NULL DEFAULT 1,
	[TrendSource] nvarchar(255) NULL,
	[ProjectId] NVARCHAR(MAX) NULL,
	[Path] NVARCHAR(MAX) NULL,
	[Math] NVARCHAR(MAX) NULL,
    [SummaryTypeID] INT NULL,
    [XAxisGridlines]      BIT            CONSTRAINT [DF_tPDTrend_XAxisGridlines] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tPDTrend] PRIMARY KEY CLUSTERED ([PDTrendID] ASC),
    CONSTRAINT [CK_tPDTrend_LegendContentLayout] CHECK ([LegendContentLayout]=(3) OR [LegendContentLayout]=(2) OR [LegendContentLayout]=(1) OR [LegendContentLayout]=(0)),
    CONSTRAINT [CK_tPDTrend_LegendDock] CHECK ([LegendDock]=(3) OR [LegendDock]=(2) OR [LegendDock]=(1) OR [LegendDock]=(0)),
    CONSTRAINT [FK_tTrend_tTrend] FOREIGN KEY ([ParentTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]),
    CONSTRAINT [UK_tPDTrend_TrendDesc] UNIQUE NONCLUSTERED ([TrendDesc] ASC),
	CONSTRAINT [FK_tTrend_PinType_tPinType] FOREIGN KEY([PinTypeID]) REFERENCES [ProcessData].[tPinType] ([PinTypeID]),
	CONSTRAINT [FK_tPDTrend_SummaryTypeID_tSummaryType] FOREIGN KEY([SummaryTypeID]) REFERENCES [ProcessData].[tSummaryType] ([SummaryTypeID])
);

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0=Left, 1=Top, 2=Right, 3=Bottom', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrend', @level2type = N'COLUMN', @level2name = N'LegendDock';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0=Spread, 1=Near, 2=Center, 3=Far', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrend', @level2type = N'COLUMN', @level2name = N'LegendContentLayout';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrend';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_tPDTrend_TrendKeyUnique]
    ON [ProcessData].[tTrend]([TrendKeyUnique] ASC);


GO
CREATE NONCLUSTERED INDEX [UK_tPDTrend_TrendKey]
    ON [ProcessData].[tTrend]([TrendKey] ASC);

