﻿CREATE TABLE [ProcessData].[tTrendBandAxisMeasurement] (
    [TrendBandAxisMeasurementID] INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendID]                  INT            NOT NULL,
    [Axis]                       INT            NOT NULL,
    [Name]                       NVARCHAR (255) NULL,
    [Type]                       NVARCHAR (255) NULL,
    [Offset]                     FLOAT (53)     NULL,
    [TooltipStyle]               NVARCHAR (255) NULL,
    [Symbol]                     NVARCHAR (MAX) NULL,
    [Color]                      NVARCHAR (255) NULL,
    [FillColor]                  NVARCHAR (255) NULL,
    [BorderThickness]            INT            NULL,
    [Invert]                     BIT            DEFAULT ((0)) NOT NULL,
    [Value1]                     FLOAT (53)     NULL,
    [Value2]                     FLOAT (53)     NULL,
    [Value3]                     FLOAT (53)     NULL,
    [Value4]                     FLOAT (53)     NULL,
    [Value5]                     FLOAT (53)     NULL,
    [PDVariableID1]              INT            NULL,
    [PDVariableID2]              INT            NULL,
    [PDVariableID3]              INT            NULL,
    [PDVariableID4]              INT            NULL,
    [PDVariableID5]              INT            NULL,
    [ValueTypeID1]               INT            NULL,
    [ValueTypeID2]               INT            NULL,
    [ValueTypeID3]               INT            NULL,
    [ValueTypeID4]               INT            NULL,
    [ValueTypeID5]               INT            NULL,
    [AggregationType1]           INT            NULL,
    [AggregationType2]           INT            NULL,
    [AggregationType3]           INT            NULL,
    [AggregationType4]           INT            NULL,
    [AggregationType5]           INT            NULL,
    [AttributeTypeID1]           INT            NULL,
    [AttributeTypeID2]           INT            NULL,
    [AttributeTypeID3]           INT            NULL,
    [AttributeTypeID4]           INT            NULL,
    [AttributeTypeID5]           INT            NULL,
    [Params1]                    NVARCHAR (MAX) NULL,
    [Params2]                    NVARCHAR (MAX) NULL,
    [Params3]                    NVARCHAR (MAX) NULL,
    [Params4]                    NVARCHAR (MAX) NULL,
    [Params5]                    NVARCHAR (MAX) NULL,
    [BarID]                      INT            NULL,
    [TagID1]                     INT            NULL,
    [TagID2]                     INT            NULL,
    [TagID3]                     INT            NULL,
    [TagID4]                     INT            NULL,
    [TagID5]                     INT            NULL,
    [IsFiltered]                 BIT            CONSTRAINT [DF_tTrendBandAxisMeasurement_IsFiltered] DEFAULT ((0)) NOT NULL,
    [FilterMin]                  FLOAT (53)     NULL,
    [FilterMax]                  FLOAT (53)     NULL,
    [FlatlineThreshold]          FLOAT (53)     NULL,
    [Exclude]                    NVARCHAR (512) NULL,
    [ApplyToAll]                 BIT            DEFAULT ((0)) NOT NULL,
    [UseGlobalDataRetrieval]     BIT            DEFAULT ((1)) NOT NULL,
    [Flags]                      NVARCHAR (512) NULL,
    [DisplayOrder] INT NULL, 
    CONSTRAINT [PK_tTrendBandAxisMeasurement] PRIMARY KEY CLUSTERED ([TrendBandAxisMeasurementID] ASC),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_AttributeTypeID1_tAttributeType] FOREIGN KEY ([AttributeTypeID1]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_AttributeTypeID2_tAttributeType] FOREIGN KEY ([AttributeTypeID2]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_AttributeTypeID3_tAttributeType] FOREIGN KEY ([AttributeTypeID3]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_AttributeTypeID4_tAttributeType] FOREIGN KEY ([AttributeTypeID4]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_AttributeTypeID5_tAttributeType] FOREIGN KEY ([AttributeTypeID5]) REFERENCES [Asset].[tAttributeType] ([AttributeTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_BarID_tTrendbandAxisBar] FOREIGN KEY ([BarID]) REFERENCES [ProcessData].[tTrendBandAxisBar] ([TrendBandAxisBarID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tTrendBandAxisMeasurement_PDVariableID1_tVariableType] FOREIGN KEY ([PDVariableID1]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_PDVariableID2_tVariableType] FOREIGN KEY ([PDVariableID2]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_PDVariableID3_tVariableType] FOREIGN KEY ([PDVariableID3]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_PDVariableID4_tVariableType] FOREIGN KEY ([PDVariableID4]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_PDVariableID5_tVariableType] FOREIGN KEY ([PDVariableID5]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_TagID1_tTag] FOREIGN KEY ([TagID1]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_TagID2_tTag] FOREIGN KEY ([TagID2]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_TagID3_tTag] FOREIGN KEY ([TagID3]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_TagID4_tTag] FOREIGN KEY ([TagID4]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_TagID5_tTag] FOREIGN KEY ([TagID5]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_ValueTypeID1_tValueType] FOREIGN KEY ([ValueTypeID1]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_ValueTypeID2_tValueType] FOREIGN KEY ([ValueTypeID2]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_ValueTypeID3_tValueType] FOREIGN KEY ([ValueTypeID3]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_ValueTypeID4_tValueType] FOREIGN KEY ([ValueTypeID4]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tTrendBandAxisMeasurement_ValueTypeID5_tValueType] FOREIGN KEY ([ValueTypeID5]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
);


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO