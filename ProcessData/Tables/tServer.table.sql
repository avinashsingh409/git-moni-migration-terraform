﻿CREATE TABLE [ProcessData].[tServer] (
    [PDServerID]        INT              IDENTITY (1, 1) NOT NULL,
    [PDServerTypeID]    INT              NOT NULL,
    [ServerDesc]        NVARCHAR (400)   NOT NULL,
    [Comment]           NVARCHAR (400)   NOT NULL,
    [FilterDesc]        NVARCHAR (MAX)   NULL,
    [Active]            BIT              CONSTRAINT [DF_tServer_Active] DEFAULT ((1)) NOT NULL,
    [AssetID]           INT              NULL,
    [GlobalID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tServer_GlobalID] DEFAULT (newid()) NULL,
    [ExternalID]        NVARCHAR (400)   NULL,
    [NDCreated]         BIT              CONSTRAINT [DF_tServer_nDCreated] DEFAULT ((0)) NULL,
    [NDPumpID]          NVARCHAR (400)   NULL,
    [NDPumpAction]      INT              CONSTRAINT [DF_tServer_NDPumpAction] DEFAULT ((0)) NOT NULL,
    [ApiDetails]        VARCHAR (MAX)    NULL,
    [NDNewArchitecture] BIT              DEFAULT ((0)) NOT NULL,
    [IsAlerts]          BIT              CONSTRAINT [DF_tServer_IsAlerts] DEFAULT ((0)) NOT NULL,
    [AlertPointPumpID]  INT              NULL,
    CONSTRAINT [PK_tPDServer] PRIMARY KEY CLUSTERED ([PDServerID] ASC),
    CONSTRAINT [FK_tAlertPointPump] FOREIGN KEY ([AlertPointPumpID]) REFERENCES [ProcessData].[tAlertPointPump] ([AlertPointPumpID]),
    CONSTRAINT [FK_tPDServer_AssetID_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tPDServer_PDServerTypeID_tPDServerType] FOREIGN KEY ([PDServerTypeID]) REFERENCES [ProcessData].[tServerType] ([PDServerTypeID]),
    CONSTRAINT [UK_tPDServer_Comment] UNIQUE NONCLUSTERED ([Comment] ASC)
);




















GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tServer';

