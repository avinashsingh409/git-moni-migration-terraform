﻿CREATE TRIGGER [ProcessData].[ValidateAssetVariableTypeTagMapInsertUpdate]
   ON  [ProcessData].[tAssetVariableTypeTagMap]
   FOR INSERT,UPDATE
AS
BEGIN
    BEGIN TRY
    DECLARE @AssetID as int
	DECLARE @AssetClassTypeID as int
	DECLARE @VariableTypeID as int	

	DECLARE @OK as int

	DECLARE curUP CURSOR LOCAL
			FOR SELECT a.AssetID,b.AssetClassTypeID,a.VariableTypeID FROM inserted a JOIN Asset.tAsset b ON a.AssetID=b.AssetID JOIN 
			ProcessData.tVariableType c on a.VariableTypeID = c.PDVariableID WHERE c.IsStandard = 1 AND c.IsExempt = 0 AND a.VariableTypeID IS NOT NULL
	OPEN curUP;
	FETCH NEXT FROM curUP INTO @AssetID,@AssetClassTypeID,@VariableTypeID
    SET @OK = 1
	WHILE @OK = 1
		BEGIN			
			IF @@FETCH_STATUS =  0
				BEGIN
								
                IF  @VariableTypeID IS NOT NULL AND NOT EXISTS(SELECT * FROM ProcessData.tAssetClassTypeVariableTypeMap WHERE AssetClassTypeID= @AssetClassTypeID AND VariableTypeID=@VariableTypeID)
                    BEGIN
					CLOSE curUP;
	                DEALLOCATE curUP;	
                    RAISERROR(N'VIOLATION OF tAssetVariableTypeTagMap business rule: VariableTypeID (%d)/AssetClassTypeID(%d) combination must already exist in ProcessData.tAssetClassTypeVariableTypeMap',
							16, -- Severity.
							1, -- State.,
							@VariableTypeID,
							@AssetClassTypeID
							);	
				
					RETURN
                    END                
				FETCH NEXT FROM curUP INTO @AssetID,@AssetClassTypeID,@VariableTypeID
				END
			ELSE
				BEGIN
				SET @OK = 0
				END
		END
	CLOSE curUP;
	DEALLOCATE curUP;
	END TRY
	BEGIN CATCH
	  PRINT N'Trigger Failed, rolling back';
	  PRINT ERROR_MESSAGE();
	END CATCH
END
GO