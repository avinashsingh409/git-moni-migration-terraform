﻿CREATE TABLE [ProcessData].[tTrendPin] (
    [PDTrendPinID] INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendID]    INT            NOT NULL,
    [StartTime]    DATETIME       NOT NULL,
    [EndTime]      DATETIME       NOT NULL,
    [Name]         NVARCHAR (255) NOT NULL,
    [CreatedBy]    NVARCHAR (255) NOT NULL,
    [ChangedBy]    NVARCHAR (255) NOT NULL,
    [CreateDate]   DATETIME       CONSTRAINT [DF__tPDTrendPin__CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]   DATETIME       CONSTRAINT [DF__tPDTrendPin__ChangeDate] DEFAULT (getdate()) NOT NULL,
	[Hidden]       BIT       DEFAULT((0)) NOT NULL,
    [SpanUnits]	   NVARCHAR(255)  NULL,
    [NameFormat] NCHAR(255) NULL, 
    CONSTRAINT [PK_tPDTrendPin] PRIMARY KEY CLUSTERED ([PDTrendPinID] ASC),
    CONSTRAINT [FK_tPDTrendPin_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);


GO
CREATE INDEX [IX_PDTrendID_Includes] ON [ProcessData].[tTrendPin] ([PDTrendID])  INCLUDE ([StartTime], [EndTime], [Name], [CreatedBy], [ChangedBy], [CreateDate], [ChangeDate], [Hidden], [SpanUnits], [NameFormat]) WITH (FILLFACTOR=100);


GO