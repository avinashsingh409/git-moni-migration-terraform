﻿
CREATE TABLE ProcessData.tTrendDataRetrieval (
    [PDTrendDataRetrievalID] INT       IDENTITY (1, 1) NOT NULL,
    [PDTrendID]              INT       NOT NULL,
    [Method]            INT            DEFAULT ((0)) NOT NULL,
    [MinInterval]       INT            NULL,
    [MinIntervalUnits]  NVARCHAR (56)  NULL,
    [Archive]           NVARCHAR (255) NULL,
    [Period] INT NULL, 
    CONSTRAINT [FK_tPDTrendDataRetrieval_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);

GO
