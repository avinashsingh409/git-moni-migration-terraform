CREATE TABLE [ProcessData].[tCalcPoint] (
    [CalcPointID]         INT            IDENTITY (1, 1) NOT NULL,
    [PDServerID]          INT            NOT NULL,
    [CalcPointName]       NVARCHAR (400) NOT NULL,
    [CalcPointDesc]       NVARCHAR (400) NULL,
    [ExternalID]          NVARCHAR (400) NOT NULL,
    [NDReleaseCreated]    BIT            CONSTRAINT [DF_tCalcPoint_NDReleaseCreated] DEFAULT ((0)) NOT NULL,
    [NDReleasePath]       NVARCHAR (400) NULL,
    [NDReleaseName]       NVARCHAR (400) NULL,
    [NDCreated]           BIT            CONSTRAINT [DF_tCalcPoint_NDCreated] DEFAULT ((0)) NOT NULL,
    [NDPath]              NVARCHAR (400) NULL,
    [NDName]              NVARCHAR (400) NULL,
    [CreatedBy]           INT            NOT NULL,
    [ChangedBy]           INT            NOT NULL,
    [CreateDate]          DATETIME       CONSTRAINT [DF_tCalcPoint_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]          DATETIME       CONSTRAINT [DF_tCalcPoint_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [NDSnapshotAction]    INT            CONSTRAINT [DF_tCalcPoint_NDSnapshotAction] DEFAULT ((0)) NOT NULL,
    [NDId]                NVARCHAR (400) NULL,
    [NDReleaseId]         NVARCHAR (400) NULL,
    [ReleaseNodeId]       NVARCHAR (400) NULL,
    [ReleaseParentNodeId] NVARCHAR (400) NULL,
    CONSTRAINT [PK_tCalcPoint] PRIMARY KEY CLUSTERED ([CalcPointID] ASC),
    CONSTRAINT [FK_tCalcPoint_tServer_PDServerID] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tCalcPoint_PDServerID_CalcPointName] UNIQUE NONCLUSTERED ([PDServerID] ASC, [CalcPointName] ASC)
);




GO

CREATE NONCLUSTERED INDEX [IDX_tCalcPoint_PDServerID_NDId_NDReleaseId] ON [ProcessData].[tCalcPoint]
(
	[PDServerID] ASC,
	[NDId] ASC,
	[NDReleaseId] ASC,
	CalcPointID ASC
)
GO
CREATE INDEX [IDX_tCalcPoint_PDServerID_includes] ON [ProcessData].[tCalcPoint] ([PDServerID])  
INCLUDE ([CalcPointName], [CalcPointDesc], [ExternalID], [NDReleaseCreated], [NDReleasePath], [NDReleaseName], [NDCreated], [NDPath], [NDName], [CreatedBy], [ChangedBy], 
[CreateDate], [ChangeDate], [NDSnapshotAction], [NDId], [NDReleaseId], [ReleaseNodeId], [ReleaseParentNodeId]) 
WITH (FILLFACTOR=100);