﻿CREATE TABLE [ProcessData].[tAnnotationCategory] (
    [AnnotationCategoryID]   INT            IDENTITY (1, 1) NOT NULL,
    [AnnotationCategoryName] NVARCHAR (128) NOT NULL,
    [AnnotationCategoryDesc] NVARCHAR (255) NULL,
    [Icon]                   NVARCHAR (255) NULL,
    CONSTRAINT [tAnnotationCategory_PK] PRIMARY KEY CLUSTERED ([AnnotationCategoryID] ASC),
    CONSTRAINT [IX_Annotation_Category_Name] UNIQUE NONCLUSTERED ([AnnotationCategoryName] ASC)
);


GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'ProcessData', @level1type=N'TABLE',@level1name=N'tAnnotationCategory'