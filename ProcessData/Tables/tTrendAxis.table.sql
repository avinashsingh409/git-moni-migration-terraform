﻿CREATE TABLE [ProcessData].[tTrendAxis] (
    [PDTrendAxisID] INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendID]     INT            NOT NULL,
    [Axis]          INT            NOT NULL,
    [Title]         NVARCHAR (255) NULL,
    [Position]      TINYINT        CONSTRAINT [DF_tPDTrendAxis_Location] DEFAULT ((0)) NOT NULL,
    [Min]           FLOAT (53)     NULL,
    [Max]           FLOAT (53)     NULL,
    [Step]          FLOAT (53)     NULL,
    [MinorStep]     FLOAT (53)     NULL,
    [IsDefault]     BIT            CONSTRAINT [DF_tPDTrendAxis_IsDefault] DEFAULT ((0)) NOT NULL,
    [CreatedBy]     NVARCHAR (255) NOT NULL,
    [ChangedBy]     NVARCHAR (255) NOT NULL,
    [CreateDate]    DATETIME       CONSTRAINT [DF__tPDTrendAxis__CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]    DATETIME       CONSTRAINT [DF__tPDTrendAxis__ChangeDate] DEFAULT (getdate()) NOT NULL,
    [GridLine]      BIT            NULL,
    CONSTRAINT [PK_tPDTrendAxis] PRIMARY KEY CLUSTERED ([PDTrendAxisID] ASC),
    CONSTRAINT [CK_tPDTrendAxis_Position] CHECK ([Position]=(2) OR [Position]=(0)),
    CONSTRAINT [FK_tPDTrendAxis_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tPDTrendAxis_PDTrendID_Axis] UNIQUE NONCLUSTERED ([PDTrendID] ASC, [Axis] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0=Near, 2=Far', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrendAxis', @level2type = N'COLUMN', @level2name = N'Position';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrendAxis';

