﻿CREATE TABLE [ProcessData].[tTrendBandAxis] (
    [TrendBandAxisID] INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendID]       INT            NOT NULL,
    [Label]           NVARCHAR (MAX) NULL,
    [Opposite]        BIT            DEFAULT ((0)) NOT NULL,
    [IsVertical]      BIT            DEFAULT ((0)) NOT NULL,
    [RenderAs]        NVARCHAR (255) NULL,
    [BarWidth]        INT            NULL,
    [Summarize]       NVARCHAR (255) NULL,
    [Clockwise]       BIT            NULL,
    [StartAngle]      REAL     NULL,
    [EndAngle]        REAL     NULL,
    CONSTRAINT [PK_tTrendBandAxis] PRIMARY KEY CLUSTERED ([TrendBandAxisID] ASC),
    CONSTRAINT [FK_tTrendBandAxis_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);


GO


GO