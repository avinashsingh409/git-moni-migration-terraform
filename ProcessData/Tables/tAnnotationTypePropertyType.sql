﻿CREATE TABLE [ProcessData].[tAnnotationTypePropertyType](
	[AnnotationTypePropertyTypeID] [tinyint] NOT NULL,
	[PropertyTypeName] [nvarchar](128) NOT NULL,
 CONSTRAINT [tAnnotationTypePropertyType_PK] PRIMARY KEY CLUSTERED 
(
	[AnnotationTypePropertyTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'ProcessData', @level1type=N'TABLE',@level1name=N'tAnnotationTypePropertyType'