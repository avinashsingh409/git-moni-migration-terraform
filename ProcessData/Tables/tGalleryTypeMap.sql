﻿CREATE TABLE [ProcessData].[tGalleryTypeMap] (
    [GalleryTypeID] INT NOT NULL,
    [PDTrendID]     INT NOT NULL,
    [GalleryOrder]  INT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tGalleryTypeMap] PRIMARY KEY CLUSTERED ([GalleryTypeID] ASC, [PDTrendID] ASC),
    CONSTRAINT [fk_tGalleryType_GalleryTypeID] FOREIGN KEY ([GalleryTypeID]) REFERENCES [ProcessData].[tGalleryType] ([GalleryTypeID]),
    CONSTRAINT [fk_tTrend_PDTrendID] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);



