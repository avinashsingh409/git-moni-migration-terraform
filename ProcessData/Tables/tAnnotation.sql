﻿CREATE TABLE [ProcessData].[tAnnotation] (
    [AnnotationID]         BIGINT   IDENTITY (1, 1) NOT NULL,
    [AssetID]              INT      NOT NULL,
    [TagID]                INT      NULL,
    [AnnotationCategoryID] INT      NOT NULL,
    [StartDate]            DATETIME NULL,
    [EndDate]              DATETIME NULL,
    [CreateDate]           DATETIME NOT NULL,
    [CreatedByUser]        INT      NOT NULL,
    CONSTRAINT [tAnnotation_PK] PRIMARY KEY CLUSTERED ([AnnotationID] ASC),
    CONSTRAINT [FK_tAnnotation_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAnnotation_tTag] FOREIGN KEY ([TagID]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [tAnnotationCategory_tAnnotation_FK1] FOREIGN KEY ([AnnotationCategoryID]) REFERENCES [ProcessData].[tAnnotationCategory] ([AnnotationCategoryID])
);


GO
/****** Object:  ForeignKey [tAnnotationCategory_tAnnotation_FK1]    Script Date: 05/06/2016 15:20:24 ******/

GO


GO


GO


GO


GO


GO


