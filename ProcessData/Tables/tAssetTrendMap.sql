﻿--Create table to map AssetIDs to TrendIDs 
CREATE TABLE [ProcessData].[tAssetTrendMap] (
    [AssetTrendMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetID]         INT NOT NULL,
    [TrendID]         INT NOT NULL,
    CONSTRAINT [PK_tAssetTrendMap] PRIMARY KEY CLUSTERED ([AssetTrendMapID] ASC),
    CONSTRAINT [FK_tAssetTrendMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetTrendMap_tTrend] FOREIGN KEY ([TrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);

GO

CREATE INDEX [IDX_TrendID] ON [ProcessData].[tAssetTrendMap] ([TrendID])  WITH (FILLFACTOR=100);
GO
CREATE INDEX [IDX_AssetID] ON [ProcessData].[tAssetTrendMap] ([AssetID])  WITH (FILLFACTOR=100);
GO


