﻿CREATE TABLE [ProcessData].[tTrendDesignCurve] (
    [TrendDesignCurveID] INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendID]          INT            NOT NULL,
    [Axis]               INT            NOT NULL,
    [DisplayText]        NVARCHAR (255) NULL,
    [CreatedBy]          NVARCHAR (255) NOT NULL,
    [ChangedBy]          NVARCHAR (255) NOT NULL,
    [CreateDate]         DATETIME       NOT NULL,
    [ChangeDate]         DATETIME       NOT NULL,
    [Color]              NVARCHAR (56)  NULL,
    [Type]               NVARCHAR (255) NULL,
    [Values]             NVARCHAR (MAX) NULL,
    [Repeat]             NVARCHAR (255) NULL,
    CONSTRAINT [FK_tPDTrendDesignCurve_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID])
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrendDesignCurve';

