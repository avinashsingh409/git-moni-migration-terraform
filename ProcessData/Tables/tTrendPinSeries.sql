﻿CREATE TABLE [ProcessData].[tTrendPinSeries] (
    [PDTrendPinSeriesID] INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendPinID]       INT            NOT NULL,
    [SeriesID]           INT            NULL,
    [FilterMin]          FLOAT (53)     NULL,
    [FilterMax]          FLOAT (53)     NULL,
    [FlatlineThreshold]  FLOAT (53)     NULL,
    [Color]              NVARCHAR (56)  NULL,
    [Exclude]            NVARCHAR (512) NULL,
    [ApplyToAll]         BIT            DEFAULT ((0)) NOT NULL,
    [Archive]            NVARCHAR (255) NULL,
    CONSTRAINT [FK_tPDTrendPinSeries_PDTrendPinID_tPDTrendPin] FOREIGN KEY ([PDTrendPinID]) REFERENCES [ProcessData].[tTrendPin] ([PDTrendPinID]) ON DELETE CASCADE
);


GO


GO