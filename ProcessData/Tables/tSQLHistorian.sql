﻿CREATE TABLE [ProcessData].[tSQLHistorian] (
    [ID]        INT            IDENTITY (1, 1) NOT NULL,
    [TagID]     INT            NOT NULL,
    [TimeStamp] DATETIME       NOT NULL,
    [Value]     REAL           CONSTRAINT [DF_tSQLHistorian_Value] DEFAULT ((0)) NOT NULL,
    [Status]    INT            CONSTRAINT [DF_tSQLHistorian_Status] DEFAULT ((0)) NOT NULL,
    [Archive]   NVARCHAR (255) NULL,
    CONSTRAINT [PK_tSQLHistorian] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tSQLHistorian_tTag] FOREIGN KEY ([TagID]) REFERENCES [ProcessData].[tTag] ([PDTagID])  ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [tSQLHistorian_TimeStamp]
    ON [ProcessData].[tSQLHistorian]([TimeStamp] ASC);


GO
CREATE NONCLUSTERED INDEX [tSQLHistorian_TagID]
    ON [ProcessData].[tSQLHistorian]([TagID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_tSQLHistorian_Archive]
    ON [ProcessData].[tSQLHistorian]([Archive] ASC)
    INCLUDE([TagID], [TimeStamp]);


GO
-- Missing Indexes
CREATE INDEX [IDX_tSQLHistorian_TagID_Archive_includes] ON [ProcessData].[tSQLHistorian] ([TagID], [Archive])  INCLUDE ([TimeStamp]) ;