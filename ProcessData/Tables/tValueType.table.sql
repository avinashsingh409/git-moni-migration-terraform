CREATE TABLE [ProcessData].[tValueType] (
    [ValueTypeID]     INT            NOT NULL,
    [ValueTypeDesc]   NVARCHAR (150) NOT NULL,
    [ValueTypeKey]    NVARCHAR (50)  NOT NULL,
    [Comment]         NVARCHAR (255) NOT NULL,
    [IsDefault]       BIT            NULL,
    [IssuePrecedence] INT            NULL,
    CONSTRAINT [PK_tValueType] PRIMARY KEY CLUSTERED ([ValueTypeID] ASC),
    CONSTRAINT [FK_tVariableType_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [UK_tValueType_ValueTypeDesc] UNIQUE NONCLUSTERED ([ValueTypeDesc] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of ValueTypes.', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tValueType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tValueType';

