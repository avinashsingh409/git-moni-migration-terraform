CREATE TABLE [ProcessData].[tServerType] (
    [PDServerTypeID] INT           NOT NULL,
    [ServerTypeDesc] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tPDServerType] PRIMARY KEY CLUSTERED ([PDServerTypeID] ASC),
    CONSTRAINT [UK_tPDServerType_ServerTypeDesc] UNIQUE NONCLUSTERED ([ServerTypeDesc] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tServerType';

