﻿CREATE TABLE [ProcessData].[tTag] (
    [PDTagID]        INT              IDENTITY (1, 1) NOT NULL,
    [PDServerID]     INT              NOT NULL,
    [TagName]        NVARCHAR (400)   NOT NULL,
    [TagDesc]        NVARCHAR (400)   NULL,
    [EngUnits]       NVARCHAR (255)   NULL,
    [ExistsOnServer] BIT              CONSTRAINT [DF_tPDTag_ExistsOnServer] DEFAULT ((1)) NOT NULL,
    [ExternalID]     NVARCHAR (400)   NOT NULL,
    [Qualifier]      NVARCHAR (256)   NULL,
    [Mapping]        NVARCHAR (400)   NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tTag_CreatedBy] DEFAULT ((-1)) NOT NULL,
    [ChangedBy]      INT              CONSTRAINT [DF_tTag_ChangedBy] DEFAULT ((-1)) NOT NULL,
    [CreateDate]     DATETIME         CONSTRAINT [DF_tTag_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]     DATETIME         CONSTRAINT [DF_tTag_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [GlobalID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tTag_GlobalID] DEFAULT (newid()) NULL,
    [NDCreated]      BIT              CONSTRAINT [DF_tTag_nDCreated] DEFAULT ((0)) NULL,
    [NDPath]         NVARCHAR (400)   NULL,
    [NDName]         NVARCHAR (400)   NULL,
    [NDId]           NVARCHAR (400)   NULL,
    CONSTRAINT [PK_tPDTag] PRIMARY KEY CLUSTERED ([PDTagID] ASC),
    CONSTRAINT [FK_tPDTag_PDServerID_tPDServer] FOREIGN KEY ([PDServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]) ON DELETE CASCADE,
    CONSTRAINT [UK_tPDTag_PDServerID_TagName] UNIQUE NONCLUSTERED ([PDServerID] ASC, [TagName] ASC)
);

GO
CREATE NONCLUSTERED INDEX [ix_tTag_PDServerID_ChangeDate_includes] ON [ProcessData].[tTag]
(
	[PDServerID] ASC,
	[ChangeDate] ASC
)
INCLUDE ( 	[TagName],
	[TagDesc],
	[EngUnits],
	[ExistsOnServer],
	[ExternalID],
	[Qualifier],
	[Mapping],
	[GlobalID],
	[NDCreated],
	[NDPath],
	[NDName],
	[NDId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]


GO

CREATE NONCLUSTERED INDEX [IDX_GlobalID] ON [ProcessData].[tTag]
(
	[GlobalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTag';


GO

CREATE TRIGGER [ProcessData].[TagUpdate]
   ON  [ProcessData].[tTag]
   AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessData.tTag SET ChangeDate = getdate() WHERE PDTagID IN (SELECT PDTagID FROM inserted)

	update x SET ExternalID = 'PD_' + Replace(LOWER(i.globalid),'-','_') FROM ProcessData.tTag x JOIN inserted i on x.PDTagID = i.PDTagID
	where i.GlobalID is not null and i.NDCreated = 1 and (i.ExternalID ='' or i.ExternalID is null)

	UPDATE x SET GlobalID = NEWID() FROM ProcessData.tTag x
	JOIN inserted i on x.PDTagID = i.PDTagID
	WHERE (i.GlobalID = '00000000-0000-0000-0000-000000000000'
		OR i.GlobalID is null)

	--Hot fix to strip extra carriage return from new tag editor on last column
	UPDATE x SET Mapping = LEFT(i.Mapping, LEN(i.Mapping)-1) FROM ProcessData.tTag x JOIN inserted i ON x.PDTagID = i.PDTagID 
	WHERE ASCII(RIGHT(i.mapping,1)) = 13

END