CREATE TABLE [ProcessData].[tAssetClassTypeVariableTypeMap] (
    [AssetClassTypeVariableTypeMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeID]                INT NOT NULL,
    [VariableTypeID]                  INT NOT NULL,
    [DisplayOrder]                    INT CONSTRAINT [DF_tAssetClassTypeVariableTypeMap_DisplayOrder] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tAssetClassTypeVariableTypeMap] PRIMARY KEY CLUSTERED ([AssetClassTypeVariableTypeMapID] ASC),
    CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_AssetClassTypeID_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_VariableTypeID_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tAssetClassTypeVariableTypeMap_WeightingVariableTypeID_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [UK_tAssetClassTypeVariableTypeMap_AssetClassTypeID_VariableTypeID] UNIQUE NONCLUSTERED ([AssetClassTypeID] ASC, [VariableTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of AssetClassTypeVariableTypeMaps.', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tAssetClassTypeVariableTypeMap';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tAssetClassTypeVariableTypeMap';

