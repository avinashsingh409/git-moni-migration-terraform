﻿CREATE TABLE [ProcessData].[tAlertPointPump] (
    [AlertPointPumpID] INT              IDENTITY (1, 1) NOT NULL,
    [DisplayName]      NVARCHAR (400)   NOT NULL,
    [GlobalID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tAlertPointPump_GlobalID] DEFAULT (newid()) NOT NULL,
    [ExternalID]       NVARCHAR (400)   NULL,
    [NDPumpID]         NVARCHAR (400)   NULL,
    CONSTRAINT [PK_tAlertPointPump] PRIMARY KEY CLUSTERED ([AlertPointPumpID] ASC),
    CONSTRAINT [UK_tAlertPointPump_ExternalID_NDPumpID] UNIQUE NONCLUSTERED ([ExternalID] ASC, [NDPumpID] ASC)
);

