﻿CREATE TABLE [ProcessData].[tAssetVariableTypeTagMap] (
    [AssetVariableTypeTagMapID] INT            IDENTITY (1, 1) NOT NULL,
    [AssetID]                   INT            NOT NULL,
    [VariableTypeID]            INT            NULL,
    [TagID]                     INT            NULL,
    [CalcString]                NVARCHAR (MAX) NULL,
    [CreatedBy]                 NVARCHAR (MAX) NOT NULL,
    [ChangedBy]                 NVARCHAR (MAX) NOT NULL,
    [CreateDate]                DATETIME       CONSTRAINT [DF_tAssetVariableTypeTagMap_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]                DATETIME       CONSTRAINT [DF_tAssetVariableTypeTagMap_ChangeDate] DEFAULT (getdate()) NOT NULL,
    [WeightingVariableTypeID]   INT            NULL,
    [WeightingValueTypeID]      INT            NULL,
    [ValueTypeID]               INT            NULL,
    [StatusVariableTypeID]      INT            NULL,
    [StatusValueTypeID]         INT            NULL,
    CONSTRAINT [PK_tAssetVariableTypeTagMap] PRIMARY KEY NONCLUSTERED ([AssetVariableTypeTagMapID] ASC),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_StatusValueTypeID_tValueType] FOREIGN KEY ([StatusValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_StatusVariableTypeID_tVariableType] FOREIGN KEY ([StatusVariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_tAsset] FOREIGN KEY ([AssetID]) REFERENCES [Asset].[tAsset] ([AssetID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_tTag] FOREIGN KEY ([TagID]) REFERENCES [ProcessData].[tTag] ([PDTagID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_tVariableType] FOREIGN KEY ([VariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_WeightingValueTypeID_tValueType] FOREIGN KEY ([WeightingValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [FK_tAssetVariableTypeTagMap_WeightingVariableTypeID_tVariableType] FOREIGN KEY ([WeightingVariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
);












GO
CREATE CLUSTERED INDEX [IX_tAssetVariableTypeTagMap_AssetID]
    ON [ProcessData].[tAssetVariableTypeTagMap]([AssetID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tAssetVariableTypeTagMap';


GO
CREATE NONCLUSTERED INDEX [IDX_tAssetVariableTypeTagMap]
    ON [ProcessData].[tAssetVariableTypeTagMap]([AssetID] ASC, [TagID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_AssetVariableTypeTagMap_VariableType_TagID]
    ON [ProcessData].[tAssetVariableTypeTagMap]([VariableTypeID] ASC, [TagID] ASC)
    INCLUDE([AssetVariableTypeTagMapID], [AssetID]);


GO
CREATE INDEX [IDX_AssetVariableTypeTagMap_TagID] ON [ProcessData].[tAssetVariableTypeTagMap] ( [TagID] ) INCLUDE ([AssetVariableTypeTagMapID], [VariableTypeID]) WITH (FILLFACTOR=100);

GO