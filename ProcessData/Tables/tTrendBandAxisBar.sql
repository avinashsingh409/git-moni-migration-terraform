﻿CREATE TABLE [ProcessData].[tTrendBandAxisBar](
	[TrendBandAxisBarID] [int] IDENTITY(1,1) NOT NULL,
	[PDTrendID] [int] NOT NULL,
	[Label] NVARCHAR(255) NULL,
	[AssetID] [int] NULL,
	[AssetClassTypeID] [int] NULL,
	[CriteriaObjectID] [int] NULL,
	[IncludeSelf] [bit] NOT NULL DEFAULT((0)),
	[IncludeChildren] [bit] NOT NULL DEFAULT((0)),
	[IncludeDescendants] [bit] NOT NULL DEFAULT((0)),
	[IncludeParents] [bit] NOT NULL DEFAULT((0)),
	[IncludeSiblings] [bit] NOT NULL DEFAULT((0)),
	[IncludeAncestors] [bit] NOT NULL DEFAULT((0)),
	[IncludeCousins] [bit] NOT NULL DEFAULT((0)),
	[IncludeSecondCousins] [bit] NOT NULL DEFAULT((0)),
	[IncludeUnit] [bit] NOT NULL DEFAULT((0)),
	[IncludeUnitDescendants] [bit] NOT NULL DEFAULT((0)),
	[IncludeAllAssets] [bit] NOT NULL DEFAULT((0)),
	[PDVariableID] [int] NULL,
	[ValueTypeID] [int] NULL,
	[PDTagID] [int] NULL,
	[TimeDivision] NVARCHAR(255) NULL, --Should be changed to foreign key to type table 
	[DisplayOrder] int NOT NULL DEFAULT((10)),
	[Context] NVARCHAR(255) NULL, --Should be changed to foreign key to type table 
	 CONSTRAINT [PK_tTrendBandAxisBar] PRIMARY KEY CLUSTERED 
	([TrendBandAxisBarID] ASC)
)
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_PDTrendID_tPDTrend] FOREIGN KEY([PDTrendID])
REFERENCES [ProcessData].[tTrend] ([PDTrendID])
ON DELETE CASCADE
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_AssetID_tAsset] FOREIGN KEY([AssetID])
REFERENCES [Asset].[tAsset] ([AssetID])
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_AssetClassTypeID_tAssetClassType] FOREIGN KEY([AssetClassTypeID])
REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID])
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_CriteraObjectID_tCO] FOREIGN KEY([CriteriaObjectID])
REFERENCES [Criteria].[tCO]([CoID])
ON DELETE CASCADE
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_PDVariableID_tVariableType] FOREIGN KEY([PDVariableID])
REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_ValueTypeID_tValueType] FOREIGN KEY([ValueTypeID])
REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
GO

ALTER TABLE [ProcessData].[tTrendBandAxisBar] ADD CONSTRAINT [FK_tTrendBandAxisBar_PDTagID_tTag] FOREIGN KEY([PDTagID])
REFERENCES [ProcessData].[tTag] ([PDTagID])
GO