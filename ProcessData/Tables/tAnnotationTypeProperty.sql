﻿CREATE TABLE [ProcessData].[tAnnotationTypeProperty](
	[AnnotationTypePropertyID] [int] IDENTITY(1,1) NOT NULL,
	[AnnotationTypeId] [int] NOT NULL,
	[AnnotationTypePropertyTypeID] [tinyint] NOT NULL,
	[PropertyName] [nvarchar](128) NOT NULL,
 [DisplayOrder] INT NOT NULL DEFAULT 1, 
    CONSTRAINT [tAnnotationTypeProperty_PK] PRIMARY KEY CLUSTERED 
(
	[AnnotationTypePropertyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [tAnnotationType_tAnnotationTypeProperty_FK1]    Script Date: 05/06/2016 15:20:24 ******/
ALTER TABLE [ProcessData].[tAnnotationTypeProperty]  ADD  CONSTRAINT [tAnnotationType_tAnnotationTypeProperty_FK1] FOREIGN KEY([AnnotationTypeId])
REFERENCES [ProcessData].[tAnnotationType] ([AnnotationTypeId])
GO

ALTER TABLE [ProcessData].[tAnnotationTypeProperty] CHECK CONSTRAINT [tAnnotationType_tAnnotationTypeProperty_FK1]
GO
/****** Object:  ForeignKey [tAnnotationTypePropertyType_tAnnotationTypeProperty_FK1]    Script Date: 05/06/2016 15:20:24 ******/
ALTER TABLE [ProcessData].[tAnnotationTypeProperty]  ADD  CONSTRAINT [tAnnotationTypePropertyType_tAnnotationTypeProperty_FK1] FOREIGN KEY([AnnotationTypePropertyTypeID])
REFERENCES [ProcessData].[tAnnotationTypePropertyType] ([AnnotationTypePropertyTypeID])
GO

ALTER TABLE [ProcessData].[tAnnotationTypeProperty] CHECK CONSTRAINT [tAnnotationTypePropertyType_tAnnotationTypeProperty_FK1]
GO
EXEC sys.sp_addextendedproperty @name=N'BV_IsTypeTable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'ProcessData', @level1type=N'TABLE',@level1name=N'tAnnotationTypeProperty'