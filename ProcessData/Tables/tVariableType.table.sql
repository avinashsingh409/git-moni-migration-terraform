CREATE TABLE [ProcessData].[tVariableType] (
    [PDVariableID]            INT            IDENTITY (1, 1) NOT NULL,
    [VariableDesc]            NVARCHAR (255) NOT NULL,
    [VariableAbbrev]          NVARCHAR (255) NOT NULL,
    [VariableName]            NVARCHAR (255) NOT NULL,
    [EngUnits]                NVARCHAR (255) NULL,
    [CalcString]              NVARCHAR (MAX) NULL,
    [FunctionTypeID]          INT            NULL,
    [WeightingVariableTypeID] INT            NULL,
    [DefaultValueTypeID]      INT            NULL,
	[IsStandard]              BIT            NOT NULL CONSTRAINT DF_tVariableType_IsStandard DEFAULT 0,
	[IsExempt]                BIT            NOT NULL CONSTRAINT DF_tVariableType_IsExempt DEFAULT 0,
    CONSTRAINT [PK_PDVariable] PRIMARY KEY CLUSTERED ([PDVariableID] ASC),
    CONSTRAINT [FK_tVariableType_FunctionTypeID_tFunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [ProcessData].[tFunctionType] ([FunctionTypeID]),
    CONSTRAINT [FK_tVariableType_WeightingVariableTypeID_tVariableType] FOREIGN KEY ([WeightingVariableTypeID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [UK_tVariableType_DefaultValueTypeID_ValueTypeID] FOREIGN KEY ([DefaultValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID]),
    CONSTRAINT [UK_tPDVariable_VariableDesc] UNIQUE NONCLUSTERED ([VariableDesc] ASC),
    CONSTRAINT [UK_tPDVariable_VariableName] UNIQUE NONCLUSTERED ([VariableName] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tVariableType';

