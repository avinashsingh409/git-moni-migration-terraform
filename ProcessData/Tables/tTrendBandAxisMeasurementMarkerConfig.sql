﻿CREATE TABLE [ProcessData].[tTrendBandAxisMeasurementMarkerConfig](
	TrendBandAxisMeasurementMarkerConfigID int IDENTITY(1,1) NOT NULL,
	[TrendBandAxisMeasurementID] [int] NOT NULL,
	[Name] NVARCHAR(255) NULL,
	[Symbol] NVARCHAR(MAX) NULL,
	[Color] NVARCHAR(255) NULL,
	[FillColor] NVARCHAR(255) NULL,
	[BorderThickness] int NULL,
	[PDVariableID] int NULL,
	[ValueTypeID] int NULL,
	[PDTagID] int NULL,
	[AggregationType] int NULL,
	[Min] float NULL,
	[Max] float NULL,
	[MinInclusive] bit DEFAULT((0)),
	[MaxInclusive] bit DEFAULT ((0))
	CONSTRAINT [PK_tTrendBandAxisMeasurementMarkerConfig] PRIMARY KEY CLUSTERED 
	([TrendBandAxisMeasurementMarkerConfigID] ASC)
	)
GO

ALTER TABLE [ProcessData].[tTrendBandAxisMeasurementMarkerConfig] ADD CONSTRAINT [FK_tTrendBandAxisMeasurementMarkerConfig_TrendBandAxisMeasurementID_tTrendBandAxisMeasurement] FOREIGN KEY([TrendBandAxisMeasurementID])
REFERENCES [ProcessData].[tTrendBandAxisMeasurement] ([TrendBandAxisMeasurementID])
ON DELETE CASCADE
GO

ALTER TABLE [ProcessData].[tTrendBandAxisMeasurementMarkerConfig] ADD CONSTRAINT [FK_tTrendBandAxisMeasurementMarkerConfig_PDVariableID_tVariableType] FOREIGN KEY([PDVariableID])
REFERENCES [ProcessData].[tVariableType] ([PDVariableID])
GO

ALTER TABLE [ProcessData].[tTrendBandAxisMeasurementMarkerConfig] ADD CONSTRAINT [FK_tTrendBandAxisMeasurementMarkerConfig_ValueTypeID_tValueType] FOREIGN KEY([ValueTypeID])
REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
GO

ALTER TABLE [ProcessData].[tTrendBandAxisMeasurementMarkerConfig] ADD CONSTRAINT [FK_tTrendBandAxisMeasurementMarkerConfig_PDTagID_tTag] FOREIGN KEY([PDTagID])
REFERENCES [ProcessData].[tTag] ([PDTagID])
GO