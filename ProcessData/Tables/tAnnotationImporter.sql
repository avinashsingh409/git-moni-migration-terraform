﻿CREATE TABLE [ProcessData].[tAnnotationImporter](
	[AnnotationImporterID] [int] IDENTITY(1,1) NOT NULL,
	[AnnotationImporterName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tAnnotationImporter] PRIMARY KEY CLUSTERED 
(
	[AnnotationImporterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO