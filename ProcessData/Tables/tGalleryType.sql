﻿CREATE TABLE [ProcessData].[tGalleryType] (
    [GalleryTypeID]   INT           NOT NULL,
    [GalleryTypeDesc] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tGalleryType] PRIMARY KEY CLUSTERED ([GalleryTypeID] ASC),
    CONSTRAINT [UK_tGalleryType_GalleryTypeDesc] UNIQUE NONCLUSTERED ([GalleryTypeDesc] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tGalleryType';

