﻿CREATE TABLE [ProcessData].[tAssetClassTypeTrendMap] (
    [AssetClassTypeTrendMapID] INT IDENTITY (1, 1) NOT NULL,
    [AssetClassTypeID]         INT NOT NULL,
    [TrendID]                  INT NOT NULL,
    CONSTRAINT [PK_tAssetClassTypeTrendMap] PRIMARY KEY CLUSTERED ([AssetClassTypeTrendMapID] ASC),
    CONSTRAINT [FK_AssetClassTypeIDtAssetClassTypeTrendMap_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_TrendIDtAssetClassTypeTrendMap_tTrend] FOREIGN KEY ([TrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE
);






GO

GO

GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tAssetClassTypeTrendMap';

