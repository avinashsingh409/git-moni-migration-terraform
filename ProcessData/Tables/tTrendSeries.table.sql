﻿CREATE TABLE [ProcessData].[tTrendSeries] (
    [PDTrendSeriesID]        INT            IDENTITY (1, 1) NOT NULL,
    [PDTrendID]              INT            NOT NULL,
    [PDTagID]                INT            NULL,
    [PDVariableID]           INT            NULL,
    [IsMain]                 BIT            CONSTRAINT [DF_tPDTrendSeries_IsMain] DEFAULT ((0)) NOT NULL,
    [DisplayOrder]           INT            CONSTRAINT [DF_tPDTrendSeries_DisplayOrder] DEFAULT ((0)) NOT NULL,
    [Axis]                   INT            CONSTRAINT [DF_tPDTrendSeries_Axis] DEFAULT ((0)) NOT NULL,
    [DisplayText]            NVARCHAR (255) NULL,
    [CreatedBy]              NVARCHAR (255) NOT NULL,
    [ChangedBy]              NVARCHAR (255) NOT NULL,
    [CreateDate]             DATETIME       CONSTRAINT [DF__tPDTrendTagMap__CreateDate] DEFAULT (getdate()) NOT NULL,
    [ChangeDate]             DATETIME       CONSTRAINT [DF__tPDTrendTagMap__ChangeDate] DEFAULT (getdate()) NOT NULL,
    [UseDisplayText]         BIT            CONSTRAINT [DF_tPDTrendSeries_UseDisplayText] DEFAULT ((0)) NOT NULL,
    [ValueTypeID]            INT            NULL,
    [IsXAxis]                BIT            CONSTRAINT [DF_tTrendSeries_IsXAxis] DEFAULT ((0)) NOT NULL,
    [IsFiltered]             BIT            CONSTRAINT [DF_tTrendSeries_IsFiltered] DEFAULT ((0)) NOT NULL,
    [FilterMin]              FLOAT (53)     NULL,
    [FilterMax]              FLOAT (53)     NULL,
    [ShowBestFitLine]        BIT            CONSTRAINT [DF_tTrendSeries_ShowBestFitLine] DEFAULT ((0)) NOT NULL,
    [AssetClassTypeID]       INT            NULL,
    [ScaleMin]               FLOAT (53)     NULL,
    [ScaleMax]               FLOAT (53)     NULL,
    [ScaleDecimals]          INT            NULL,
    [IncludeSelf]            BIT            DEFAULT ((1)) NOT NULL,
    [IncludeChildren]        BIT            DEFAULT ((0)) NOT NULL,
    [IncludeDescendants]     BIT            DEFAULT ((1)) NOT NULL,
    [IncludeParents]         BIT            DEFAULT ((0)) NOT NULL,
    [IncludeSiblings]        BIT            DEFAULT ((0)) NOT NULL,
    [IncludeAncestors]       BIT            DEFAULT ((1)) NOT NULL,
    [IncludeCousins]         BIT            DEFAULT ((0)) NOT NULL,
    [IncludeSecondCousins]   BIT            DEFAULT ((0)) NOT NULL,
    [IncludeUnit]            BIT            DEFAULT ((0)) NOT NULL,
    [IncludeUnitDescendants] BIT            DEFAULT ((0)) NOT NULL,
    [IncludeAllAssets]       BIT            DEFAULT ((0)) NOT NULL,
    [ChartTypeID]            INT            NULL,
    [IsZAxis]                BIT            DEFAULT ((0)) NULL,
    [FlatlineThreshold]      FLOAT (53)     NULL,
    [Exclude]                NVARCHAR (512) NULL,
    [Color]                  NVARCHAR (56)  NULL,
    [Stack]                  NVARCHAR (56)  NULL,
    [StackType]              NVARCHAR (56)  NULL,
    [ApplyToAll]             BIT            DEFAULT ((0)) NOT NULL,
    [Archive]                NVARCHAR (255) NULL,
    [UseGlobalDataRetrieval] BIT            DEFAULT ((1)) NOT NULL,
    [SummaryTypeID]          INT            NULL,
    CONSTRAINT [PK_tPDTrendSeries] PRIMARY KEY CLUSTERED ([PDTrendSeriesID] ASC),
    CONSTRAINT [FK_AssetClassTypeIDtTrendSeries_tAssetClassType] FOREIGN KEY ([AssetClassTypeID]) REFERENCES [Asset].[tAssetClassType] ([AssetClassTypeID]),
    CONSTRAINT [FK_tPDTrendSeries_PDTagID_tPDTag] FOREIGN KEY ([PDTagID]) REFERENCES [ProcessData].[tTag] ([PDTagID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tPDTrendSeries_PDTrendID_tPDTrend] FOREIGN KEY ([PDTrendID]) REFERENCES [ProcessData].[tTrend] ([PDTrendID]) ON DELETE CASCADE,
    CONSTRAINT [FK_tPDTrendSeries_PDVariableID_tPDVariable] FOREIGN KEY ([PDVariableID]) REFERENCES [ProcessData].[tVariableType] ([PDVariableID]),
    CONSTRAINT [FK_tPDTrendSeries_SummaryTypeID_tSummaryType] FOREIGN KEY ([SummaryTypeID]) REFERENCES [ProcessData].[tSummaryType] ([SummaryTypeID]),
    CONSTRAINT [FK_tTrendSeries_ValueTypeID_tValueType] FOREIGN KEY ([ValueTypeID]) REFERENCES [ProcessData].[tValueType] ([ValueTypeID])
);














GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tTrendSeries';


GO
CREATE NONCLUSTERED INDEX [IDX_TrendSeries_PDTrendID]
    ON [ProcessData].[tTrendSeries]([PDTrendID] ASC)
    INCLUDE([PDTrendSeriesID], [PDTagID], [PDVariableID], [AssetClassTypeID], [IncludeSelf], [IncludeChildren], [IncludeDescendants], [IncludeParents], [IncludeSiblings], [IncludeAncestors], [IncludeCousins], [IncludeSecondCousins], [IncludeUnit], [IncludeUnitDescendants], [IncludeAllAssets]);

