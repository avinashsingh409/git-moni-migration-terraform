CREATE TABLE [ProcessData].[tFunctionType] (
    [FunctionTypeID]     INT            NOT NULL,
    [FunctionTypeAbbrev] NVARCHAR (255) NOT NULL,
    [FunctionTypeDesc]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tFunctionType] PRIMARY KEY CLUSTERED ([FunctionTypeID] ASC),
    CONSTRAINT [UK_tFunctionType_FunctionTypeDesc] UNIQUE NONCLUSTERED ([FunctionTypeDesc] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Collection of FunctionTypes.', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tFunctionType';


GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tFunctionType';

