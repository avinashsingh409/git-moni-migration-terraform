﻿CREATE TABLE [ProcessData].[tSummaryType] (
    [SummaryTypeID] INT            NOT NULL,
    [SummaryDesc]   NVARCHAR (MAX) NOT NULL,
    [SummaryAbbrev] NVARCHAR (255) NOT NULL,
    [DisplayOrder]  INT            NOT NULL,
    CONSTRAINT [ProcessData_tSummaryType_PK] PRIMARY KEY CLUSTERED ([SummaryTypeID] ASC)
);


