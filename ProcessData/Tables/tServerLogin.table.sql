CREATE TABLE [ProcessData].[tServerLogin] (
    [ServerLoginID]  INT           IDENTITY (1, 1) NOT NULL,
    [ClientKey]      NVARCHAR (64) NOT NULL,
    [ClientUsername] NVARCHAR (8)  NOT NULL,
    [ClientPassword] NVARCHAR (8)  NOT NULL,
    [ServerID]       INT           NOT NULL,
    [ServerUsername] NVARCHAR (50) NULL,
    [ServerPassword] NVARCHAR (50) NULL,
    [ExpirationDate] DATETIME      NULL,
    [Custom1] NVARCHAR(50) NULL, 
    [Custom2] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_ProcessData_tServerLogin] PRIMARY KEY CLUSTERED ([ServerLoginID] ASC),
    CONSTRAINT [FK_tServerLogin_ServerID_tServer] FOREIGN KEY ([ServerID]) REFERENCES [ProcessData].[tServer] ([PDServerID]) ON DELETE CASCADE
);






GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'False', @level0type = N'SCHEMA', @level0name = N'ProcessData', @level1type = N'TABLE', @level1name = N'tServerLogin';

