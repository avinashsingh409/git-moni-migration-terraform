CREATE TYPE [ProcessData].[tpAssetVariableTypeTagMap] AS TABLE(
	[AssetVariableTypeTagMapID] [int] NULL,
	[AssetID] [int] NOT NULL,
	[VariableTypeID] [int] NULL,
	[TagID] [int] NULL,
	[CalcString] [nvarchar](max) NULL,
	[WeightingVariableTypeID] [int] NULL,
	[WeightingValueTypeID] [int] NULL,
	[ValueTypeID] [int] NULL,
	[StatusVariableTypeID] [int] NULL,
	[StatusValueTypeID] [int] NULL
)
GO
GRANT EXECUTE
ON TYPE::[ProcessData].[tpAssetVariableTypeTagMap] TO [TEUser];
GO