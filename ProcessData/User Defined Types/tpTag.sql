﻿CREATE TYPE [ProcessData].[tpTag] AS TABLE(
	[PDTagID] [int] NULL,
	[PDServerID] [int] NOT NULL,
	[TagName] [nvarchar](400) NOT NULL,
	[TagDesc] [nvarchar](400) NULL,
	[EngUnits] [nvarchar](255) NULL,
	[ExistsOnServer] [bit] NOT NULL,
	[ExternalID] [nvarchar](400) NOT NULL,
	[Qualifier] [nvarchar](256) NULL,
	[Mapping] [nvarchar](400) NULL,
	[GlobalID] [uniqueidentifier] NULL,
	[NDCreated] [bit] NULL,
	[NDPath] [nvarchar](400) NULL,
	[NDName] [nvarchar](400) NULL,
	[NDId] [nvarchar](400) NULL
)
GO
GRANT EXECUTE ON TYPE :: ProcessData.tpTag TO TEUser
GO