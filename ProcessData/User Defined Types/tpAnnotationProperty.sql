﻿/****** Object:  UserDefinedTableType [ProcessData].[tpAnnotationProperty]    Script Date: 05/06/2016 15:20:25 ******/
CREATE TYPE [ProcessData].[tpAnnotationProperty] AS TABLE (
    [AnnotationPropertyMapID]  BIGINT         NULL,
    [AnnotationID]             BIGINT         NULL,
    [AnnotationTypePropertyID] INT            NOT NULL,
    [ValueAsText]              NVARCHAR (MAX) NOT NULL);


GO
GRANT EXECUTE
    ON TYPE::[ProcessData].[tpAnnotationProperty] TO [TEUser];

