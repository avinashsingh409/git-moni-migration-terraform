﻿CREATE TYPE [ProcessData].[tpSqlHistorianData] AS TABLE(
	[PDTagID] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Value] [real] NOT NULL,
	[Status] [int] NOT NULL,
    [Archive] [nvarchar](255) NULL
)
GO
GRANT EXECUTE
    ON TYPE::[ProcessData].[tpSqlHistorianData] TO [TEUser];

