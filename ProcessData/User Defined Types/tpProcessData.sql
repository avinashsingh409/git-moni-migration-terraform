﻿CREATE TYPE [ProcessData].[tpProcessData] AS TABLE(
	ServerID int,
	TagName varchar(400),
	[TimeStamp] DateTime,
	[Value] real,
	[Status] int,
	[Archive] varchar(255)
)
GO

GRANT EXECUTE
    ON TYPE::[ProcessData].[tpProcessData] TO [TEUser];
GO
