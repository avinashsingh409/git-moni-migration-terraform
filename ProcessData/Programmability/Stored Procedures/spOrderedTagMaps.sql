CREATE PROCEDURE [ProcessData].[spOrderedTagMaps] 	
	@securityUserID int,
	@assetID int,
	@selectedAssetID int,
	@name varchar(255) = null,
	@desc varchar(255) = null,
	@assetSearch varchar(255) = null,
	@variable varchar (255) = null,
	@units varchar(255) = null,
	@skip int,
	@take int
AS
BEGIN

SET NOCOUNT ON;
	
if @securityUserID <= 0 
  begin
  set @selectedAssetID = @assetID
  end

select a.AssetID,CONVERT(int,null) as level into #tmp_levels from Asset.tAsset a join Asset.ufnGetAssetTreeBranch(@selectedAssetID) b on A.assetid = b.AssetID

-- get descendants
insert into #tmp_levels (AssetID,level) select a.AssetID,null from Asset.ufnAssetAncestors(@selectedAssetID) a
update a set level = case when b.AncestryLevel < 0 then (-b.AncestryLevel) else 0 end from #tmp_levels a join Asset.tAssetHop b on a.AssetID = b.EndingAssetId where b.HopAssetId = @selectedAssetID

declare @maxlevel as int
select @maxlevel = MAX(level) from #tmp_levels

-- go up the tree and get ancestors
update a set level = -b.AncestryLevel + @maxlevel + 2 from #tmp_levels a join Asset.tAssetHop b on a.AssetID = b.HopAssetId where b.EndingAssetId = @selectedAssetID and a.AssetID <> @selectedAssetID;

select @maxlevel = MAX(level) from #tmp_levels

declare @parentAssetID as int
select @parentAssetID = parentAssetID from Asset.tAsset where AssetID = @selectedAssetID

insert into #tmp_levels (AssetID,level) select a.AssetID,@maxlevel+1 from Asset.tAsset a where a.ParentAssetID = @parentAssetID and a.AssetID<>@selectedAssetID;

 WITH T1 AS 
 ( 
 SELECT ROW_NUMBER() OVER(ORDER BY 
 ISNULL(l.[Level],999), 
 Asset.DisplayOrder,
 asset.AssetAbbrev, ISNULL(VariableAbbrev,'ZZZ'), case when t.TagDesc = '' then t.TagName else t.tagdesc end, t.PDTagID) AS ROW, map.AssetVariableTypeTagMapID 
 FROM ProcessData.tAssetVariableTypeTagMap map 
 INNER JOIN ProcessData.tTag t ON map.TagID = t.PDTagID 
 INNER JOIN Asset.ufnGetAssetIdsForUserStartingAt(@securityuserid,@assetID) ast ON map.AssetID = ast.AssetID 
 INNER JOIN Asset.tAsset asset ON map.AssetID = asset.AssetID 
 INNER JOIN Asset.tAssetClassType ac on asset.AssetClassTypeID = ac.AssetClassTypeID 
 LEFT JOIN #tmp_levels l ON map.AssetID = l.AssetID
 LEFT JOIN ProcessData.tVariableType vt ON map.VariableTypeID = vt.PDVariableID 
 	WHERE  
 	(@name IS NULL OR t.TagName like '%' + @name + '%' ) 
 	AND (@desc IS NULL OR t.TagDesc like '%' + @desc + '%' ) 
 	AND (@units IS NULL OR t.EngUnits like '%' + @units + '%')  
 	AND (@variable IS NULL OR vt.VariableAbbrev like '%' + @variable + '%')  
 	AND (@assetSearch IS NULL OR asset.AssetAbbrev like '%' + @assetSearch + '%')  
 ) 
  
 SELECT  
    ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID 
   ,ProcessData.tAssetVariableTypeTagMap.AssetID 
   ,ProcessData.tAssetVariableTypeTagMap.VariableTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.TagID 
   ,ProcessData.tAssetVariableTypeTagMap.CalcString 
   ,ProcessData.tAssetVariableTypeTagMap.CreatedBy 
   ,ProcessData.tAssetVariableTypeTagMap.ChangedBy 
   ,ProcessData.tAssetVariableTypeTagMap.CreateDate 
   ,ProcessData.tAssetVariableTypeTagMap.ChangeDate 
   ,ProcessData.tAssetVariableTypeTagMap.ValueTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.WeightingVariableTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.WeightingValueTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.StatusVariableTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.StatusValueTypeID 
   ,Asset.tAsset.ParentAssetID 
   ,Asset.tAsset.AssetAbbrev 
   ,Asset.tAsset.AssetDesc 
   ,Asset.tAsset.AssetClassTypeID 
   ,Asset.tAsset.AssetTreeNodeChildConfigurationTypeID 
   ,Asset.tAsset.DisplayOrder 
   ,Asset.tAsset.CreatedBy 
   ,Asset.tAsset.ChangedBy 
   ,Asset.tAsset.CreateDate 
   ,Asset.tAsset.ChangeDate 
   ,Asset.tAsset.GlobalID 
   ,Asset.tAsset.IsHidden 
   ,Asset.tAsset.Track 
   ,Asset.tAssetClassType.AssetTypeID 
   ,Asset.tAssetClassType.AssetClassTypeKey 
   ,Asset.tAssetClassType.AssetClassTypeAbbrev 
   ,Asset.tAssetClassType.AssetClassTypeDesc 
   ,Asset.tAssetType.AssetTypeAbbrev 
   ,Asset.tAssetType.AssetTypeDesc 
   ,0 as NumChildren 
   ,ProcessData.tVariableType.PDVariableID 
   ,ProcessData.tVariableType.VariableDesc 
   ,ProcessData.tVariableType.VariableAbbrev 
   ,ProcessData.tVariableType.VariableName 
   ,ProcessData.tVariableType.EngUnits 
   ,ProcessData.tVariableType.CalcString 
   ,ProcessData.tVariableType.FunctionTypeID 
   ,ProcessData.tVariableType.WeightingVariableTypeID 
   ,ProcessData.tVariableType.DefaultValueTypeID 
   ,ProcessData.tTag.PDTagID
   ,ProcessData.tTag.PDServerID
   ,ProcessData.tTag.TagName
   ,ProcessData.tTag.TagDesc
   ,ProcessData.tTag.EngUnits as TagUnits
   ,ProcessData.tTag.ExistsOnServer
   ,ProcessData.tTag.ExternalID
   ,ProcessData.tTag.Qualifier 
   ,ProcessData.tTag.Mapping 
   ,ProcessData.tTag.GlobalID
   ,ProcessData.tTag.NDCreated
   ,ProcessData.tTag.NDPath
   ,ProcessData.tTag.NDName
   ,ProcessData.tTag.NDId
   ,ProcessData.tTag.Qualifier
   ,ProcessData.tTag.Mapping
   ,ProcessData.tValueType.ValueTypeID
   ,ProcessData.tValueType.ValueTypeDesc
   ,ProcessData.tValueType.ValueTypeKey
   ,ProcessData.tValueType.Comment
   ,ProcessData.tValueType.IsDefault
   ,ProcessData.tValueType.IssuePrecedence
   ,l.level   
 FROM ProcessData.tAssetVariableTypeTagMap
 JOIN Asset.tAsset ON ProcessData.tAssetVariableTypeTagMap.AssetID = Asset.tAsset.AssetID 
 JOIN Asset.tAssetClassType on Asset.tAsset.AssetClassTypeId=Asset.tAssetClassType.AssetClassTypeID 
 JOIN Asset.tAssetType on Asset.tAssetClassType.AssetTypeID=Asset.tAssetType.AssetTypeID 
 LEFT JOIN ProcessData.tVariableType ON ProcessData.tAssetVariableTypeTagMap.VariableTypeID = ProcessData.tVariableType.PDVariableID 
 LEFT JOIN ProcessData.tTag ON ProcessData.tAssetVariableTypeTagMap.TagID = ProcessData.tTag.PDTagID 
 LEFT JOIN ProcessData.tValueType ON ProcessData.tAssetVariableTypeTagMap.ValueTypeID = ProcessData.tValueType.ValueTypeID 
 LEFT JOIN #tmp_levels l ON Asset.tAsset.AssetID = l.AssetID
 INNER JOIN T1 ON T1.AssetVariableTypeTagMapID = ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID 
 WHERE T1.ROW between @skip+1 and @skip + @take 
 ORDER BY 
 T1.ROW
 --ISNULL(l.level,999), 
 --Asset.tAssetClassType.AssetTypeID, asset.tAsset.AssetAbbrev, ISNULL(VariableAbbrev,'ZZZ'), case when ProcessData.tTag.TagDesc = '' then ProcessData.tTag.TagName else ProcessData.tTag.tagdesc end, ProcessData.tTag.PDTagID 

drop table #tmp_levels;
	
END

GO

GRANT EXECUTE
	ON OBJECT::[ProcessData].[spOrderedTagMaps]  TO [TEUser]
	AS [dbo];
