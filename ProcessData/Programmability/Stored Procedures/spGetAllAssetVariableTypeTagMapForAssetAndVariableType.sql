﻿CREATE PROCEDURE [ProcessData].[spGetAllAssetVariableTypeTagMapForAssetAndVariableType] 
	-- Add the parameters for the stored procedure here
	@AssetID int,
	@VariableTypeID int = NULL,
	@ValueTypeID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
 SELECT 
    ProcessData.tAssetVariableTypeTagMap.AssetVariableTypeTagMapID 
   ,ProcessData.tAssetVariableTypeTagMap.AssetID 
   ,ProcessData.tAssetVariableTypeTagMap.VariableTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.TagID 
   ,ProcessData.tAssetVariableTypeTagMap.CalcString 
   ,ProcessData.tAssetVariableTypeTagMap.CreatedBy 
   ,ProcessData.tAssetVariableTypeTagMap.ChangedBy 
   ,ProcessData.tAssetVariableTypeTagMap.CreateDate 
   ,ProcessData.tAssetVariableTypeTagMap.ChangeDate 
   ,ProcessData.tAssetVariableTypeTagMap.ValueTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.WeightingVariableTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.WeightingValueTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.StatusVariableTypeID 
   ,ProcessData.tAssetVariableTypeTagMap.StatusValueTypeID 
   ,Asset.tAsset.ParentAssetID 
   ,Asset.tAsset.AssetAbbrev 
   ,Asset.tAsset.AssetDesc 
   ,Asset.tAsset.AssetClassTypeID 
   ,Asset.tAsset.AssetTreeNodeChildConfigurationTypeID 
   ,Asset.tAsset.DisplayOrder 
   ,Asset.tAsset.CreatedBy 
   ,Asset.tAsset.ChangedBy 
   ,Asset.tAsset.CreateDate 
   ,Asset.tAsset.ChangeDate 
   ,Asset.tAsset.GlobalID 
   ,Asset.tAsset.IsHidden 
   ,Asset.tAssetClassType.AssetTypeID 
   ,Asset.tAssetClassType.AssetClassTypeKey 
   ,Asset.tAssetClassType.AssetClassTypeAbbrev 
   ,Asset.tAssetClassType.AssetClassTypeDesc 
   ,Asset.tAssetType.AssetTypeAbbrev 
   ,Asset.tAssetType.AssetTypeDesc 
   ,0 as NumChildren
   ,ProcessData.tVariableType.PDVariableID 
   ,ProcessData.tVariableType.VariableDesc 
   ,ProcessData.tVariableType.VariableAbbrev 
   ,ProcessData.tVariableType.VariableName 
   ,ProcessData.tVariableType.EngUnits 
   ,ProcessData.tVariableType.CalcString 
   ,ProcessData.tVariableType.FunctionTypeID 
   ,ProcessData.tVariableType.WeightingVariableTypeID 
   ,ProcessData.tVariableType.DefaultValueTypeID 
   ,ProcessData.tTag.PDTagID
   ,ProcessData.tTag.PDServerID
   ,ProcessData.tTag.TagName
   ,ProcessData.tTag.TagDesc
   ,ProcessData.tTag.EngUnits as TagUnits
   ,ProcessData.tTag.ExistsOnServer
   ,ProcessData.tTag.ExternalID
   ,ProcessData.tTag.Qualifier
   ,ProcessData.tTag.Mapping
   ,ProcessData.tTag.GlobalID
   ,ProcessData.tTag.NDCreated
   ,ProcessData.tTag.NDPath
   ,ProcessData.tTag.NDName
   ,ProcessData.tTag.NDID
   ,ProcessData.tValueType.ValueTypeID
   ,ProcessData.tValueType.ValueTypeDesc
   ,ProcessData.tValueType.ValueTypeKey
   ,ProcessData.tValueType.Comment
   ,ProcessData.tValueType.IsDefault
   ,ProcessData.tValueType.IssuePrecedence
 FROM ProcessData.tAssetVariableTypeTagMap
 JOIN Asset.tAsset ON ProcessData.tAssetVariableTypeTagMap.AssetID = Asset.tAsset.AssetID 
 JOIN Asset.tAssetClassType on Asset.tAsset.AssetClassTypeId=Asset.tAssetClassType.AssetClassTypeID 
 JOIN Asset.tAssetType on Asset.tAssetClassType.AssetTypeID=Asset.tAssetType.AssetTypeID 
 LEFT JOIN ProcessData.tVariableType ON ProcessData.tAssetVariableTypeTagMap.VariableTypeID = ProcessData.tVariableType.PDVariableID 
 LEFT JOIN ProcessData.tTag ON ProcessData.tAssetVariableTypeTagMap.TagID = ProcessData.tTag.PDTagID 
 LEFT JOIN ProcessData.tValueType ON ProcessData.tAssetVariableTypeTagMap.ValueTypeID = ProcessData.tValueType.ValueTypeID 
 WHERE 
 (@VariableTypeID IS NULL OR ProcessData.tAssetVariableTypeTagMap.VariableTypeID = @VariableTypeID )
 AND
 (@ValueTypeID IS NULL OR ProcessData.tAssetVariableTypeTagMap.ValueTypeID = @ValueTypeID)
 AND 
 ProcessData.tAssetVariableTypeTagMap.AssetID = @AssetID
 END

GO
    GO
GRANT EXECUTE
    ON OBJECT::[ProcessData].[spGetAllAssetVariableTypeTagMapForAssetAndVariableType] TO [TEUser]
    AS [dbo];

