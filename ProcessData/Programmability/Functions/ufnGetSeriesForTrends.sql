﻿

CREATE FUNCTION [ProcessData].[ufnGetSeriesForTrends] 
(
	@assetID INT,
	@trends Base.tpIntList READONLY
)
RETURNS @result TABLE
(
	trendID int, 
	seriesID int,
	assetVariableTypeTagMapID int
)

BEGIN
DECLARE @trendBase as TABLE
(
	trendID int,
    seriesID int,
	tagID INT,
	variableID INT,
	acTypeID INT,
	includeAllAssets BIT,
	includeDescendants BIT,
	includeChildren BIT,
	includeAncestors BIT,
	includeParents BIT,
	includeSiblings BIT,
	includeUnitDescendants BIT,
	includeSecondCousins BIT,
	includeCousins BIT,
	includeUnit BIT,
	includeSelf BIT
)

	INSERT INTO @trendBase
	SELECT DISTINCT series.PDTrendID, series.PDTrendSeriesID, series.PDTagID, series.PDVariableID, series.AssetClassTypeID, series.IncludeAllAssets
	  , series.IncludeDescendants, series.IncludeChildren, series.IncludeAncestors, series.IncludeParents, series.IncludeSiblings
	  , series.IncludeUnitDescendants, series.IncludeSecondCousins, series.IncludeCousins, series.IncludeUnit, series.IncludeSelf
	FROM ProcessData.tTrendSeries series 
	INNER JOIN @Trends t ON series.PDTrendID = t.id
	
	--IF (#trendBase.TagID IS NOT NULL)
		
	INSERT INTO @result(trendID, seriesID, assetVariableTypeTagMapID )
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN @trendBase tb ON avt.TagID = tb.tagID --AND tb.tagID IS NOT NULL
	
	--ELSE
		
	DELETE FROM @trendBase WHERE tagID IS NOT NULL		-- only @trendBase.TagID IS NULL records needed for the rest of these insert queries

	-- includeAllAssets = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID )
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.tAsset asset ON avt.AssetID = asset.AssetID
	INNER JOIN @trendBase tb ON tb.includeAllAssets = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND (tb.acTypeID IS NULL OR (asset.AssetClassTypeID = tb.acTypeID))
		
	-- includeDescendants = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.ufnGetAssetTreeBranch(@assetID) branch ON avt.AssetID = branch.AssetID
	INNER JOIN Asset.tAsset ast ON branch.AssetID = ast.AssetID
	INNER JOIN @trendBase tb ON tb.includeDescendants = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND (tb.acTypeID IS NULL OR (ast.AssetClassTypeID = tb.acTypeID))
		
	-- includeChildren = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.tAsset asset ON avt.AssetID = asset.AssetID
	INNER JOIN @trendBase tb ON tb.includeChildren = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND asset.ParentAssetID = @assetID AND (tb.acTypeID IS NULL OR (asset.AssetClassTypeID = tb.acTypeID))
		
	-- includeAncestors = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.ufnAssetAncestors(@assetID) assetAnc ON avt.AssetID = assetAnc.AssetID
	INNER JOIN Asset.tAsset asset ON assetAnc.AssetID = asset.AssetID
	INNER JOIN @trendBase tb ON tb.includeAncestors = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND (tb.acTypeID IS NULL OR (asset.AssetClassTypeID = tb.acTypeID))
		
	-- includeParents = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.tAsset pAsset ON avt.AssetID = pAsset.AssetID
	INNER JOIN Asset.tAsset cAsset ON cAsset.ParentAssetID = pAsset.AssetID
	INNER JOIN @trendBase tb ON tb.includeParents = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND cAsset.AssetID = @assetID AND (tb.acTypeID IS NULL OR (pAsset.AssetClassTypeID = tb.acTypeID))
		
	-- includeSiblings = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.tAsset sibAsset ON avt.AssetID = sibAsset.AssetID
	INNER JOIN Asset.tAsset asset ON asset.ParentAssetID = sibAsset.ParentAssetID
	INNER JOIN @trendBase tb ON tb.includeSiblings = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND asset.AssetID = @assetID AND sibAsset.AssetID <> @assetID AND (tb.acTypeID IS NULL OR (sibAsset.AssetClassTypeID = tb.acTypeID))
		
	-- includeUnitDescendants = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.ufnGetAssetTreeBranch(Asset.ufnGetParentAssetIDOfAssetIdForAssetTypeAbbrev(@assetID,'UN')) unitDescendants ON avt.AssetID = unitDescendants.AssetID 
	INNER JOIN Asset.tAsset uAsset ON unitDescendants.AssetID = uAsset.AssetID
	INNER JOIN @trendBase tb ON tb.includeUnitDescendants = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND (tb.acTypeID IS NULL OR (uAsset.AssetClassTypeID = tb.acTypeID))
		
	-- includeCousins = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.tAsset cousAsset ON avt.AssetID = cousAsset.AssetID
	INNER JOIN Asset.tAsset uncles ON cousAsset.ParentAssetID = uncles.AssetID
	INNER JOIN Asset.tAsset grandparents ON uncles.ParentAssetID = grandparents.AssetID
	INNER JOIN Asset.tAsset parents ON grandparents.AssetID = parents.ParentAssetID
	INNER JOIN Asset.tAsset asset ON parents.AssetID = asset.ParentAssetID
	INNER JOIN @trendBase tb ON tb.includeCousins = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND asset.AssetID = @assetID AND cousAsset.AssetID <> @assetID AND cousAsset.ParentAssetID <> asset.ParentAssetID AND (tb.acTypeID IS NULL OR (cousAsset.AssetClassTypeID = tb.acTypeID))
		
	-- includeSecondCousins = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN Asset.tAsset secondCousAsset ON avt.AssetID = secondCousAsset.AssetID
	INNER JOIN Asset.tAsset onceRemoved ON secondCousAsset.ParentAssetID = onceRemoved.AssetID
	INNER JOIN Asset.tAsset greatUncles ON onceRemoved.ParentAssetID = greatUncles.AssetID 
	INNER JOIN Asset.tAsset greatGrandParents ON greatUncles.ParentAssetID = greatGrandParents.AssetID
	INNER JOIN Asset.tAsset grandParents ON greatGrandParents.AssetID = grandParents.ParentAssetID
	INNER JOIN Asset.tAsset parents ON grandParents.AssetID = parents.ParentAssetID
	INNER JOIN Asset.tAsset asset ON parents.AssetID = asset.ParentAssetID 
	INNER JOIN @trendBase tb ON tb.includeSecondCousins = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND asset.AssetID = @assetID AND secondCousAsset.AssetID <> @assetID AND (tb.acTypeID IS NULL OR (secondCousAsset.AssetClassTypeID = tb.acTypeID))
		
	-- includeUnit = 1
	DECLARE @unitID INT;
	SET @unitID = Asset.ufnGetParentUnitAssetIDOfAssetID(@assetID);

	IF (@unitID IS NOT NULL AND @unitID <> @assetID)
	BEGIN
		INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
		SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
		FROM ProcessData.tAssetVariableTypeTagMap avt 
		INNER JOIN @trendBase tb ON tb.includeUnit = 1 AND avt.VariableTypeID = tb.variableID 
			AND avt.TagID IS NOT NULL AND avt.AssetID = @unitID
	END
		
	-- includeSelf = 1
	INSERT INTO @result(trendID, seriesID,assetVariableTypeTagMapID)
	SELECT tb.trendID, tb.seriesID, avt.AssetVariableTypeTagMapID
	FROM ProcessData.tAssetVariableTypeTagMap avt 
	INNER JOIN @trendBase tb ON tb.includeSelf = 1 AND avt.VariableTypeID = tb.variableID 
		AND avt.TagID IS NOT NULL AND avt.AssetID = @assetID

	RETURN
END
GO