﻿CREATE FUNCTION [ProcessData].[ufnGetPDTagIDs]
(
	@SecurityUserID int,
    @TVP Base.tpIntList READONLY
)
RETURNS @tagIDs TABLE
(
  PDTagID int PRIMARY KEY not NULL
)
AS
BEGIN
    DECLARE @serverSecurity as TABLE
	(
	     ServerID int,
		 AssetID int,
		 UserHasRights bit 
	)
	
	IF EXISTS (SELECT 1 FROM @TVP)
	    BEGIN
	    INSERT INTO @serverSecurity (ServerID,AssetID, UserHasRights) 
		  SELECT DISTINCT T.PDServerID,S.AssetID,0
		    FROM
			ProcessData.tTag T
			INNER JOIN ProcessData.tServer S ON S.PDServerID = T.PDServerID			
			INNER JOIN @TVP I ON T.PDTagID = I.id

		if (@SecurityUserID>=0)
		  begin		  
		  UPDATE @serverSecurity set UserHasRights = Asset.ufnDoesUserHaveAccessToAsset(assetid,@SecurityUserID,-1)
		  end
		else
		  begin
		  UPDATE @serverSecurity set UserHasRights = 1
		  end

		INSERT INTO @tagIDs
		SELECT DISTINCT T.PDTagID
		FROM
			ProcessData.tTag T
			INNER JOIN @serverSecurity S ON S.ServerID = T.PDServerID			
			INNER JOIN @TVP I ON T.PDTagID = I.id
			WHERE T.ExistsOnServer = 1 and S.UserHasRights = 1
        END
	ELSE
	    BEGIN
	    INSERT INTO @serverSecurity (ServerID,AssetID, UserHasRights) 
		  SELECT DISTINCT S.PDServerID,S.AssetID,0 FROM  ProcessData.tServer S 

		if (@SecurityUserID>=0)
		  begin		  
		  UPDATE @serverSecurity set UserHasRights = Asset.ufnDoesUserHaveAccessToAsset(assetid,@SecurityUserID,-1)
		  end
		else
		  begin
		  UPDATE @serverSecurity set UserHasRights = 1
		  end

		INSERT INTO @tagIDs
		SELECT DISTINCT T.PDTagID
		FROM
			ProcessData.tTag T
			INNER JOIN @serverSecurity S ON S.ServerID = T.PDServerID WHERE T.ExistsOnServer = 1 and S.UserHasRights = 1		 	
		END
	RETURN
END


GO