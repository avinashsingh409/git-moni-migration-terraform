CREATE FUNCTION [ProcessData].[ufnGetArchiveDirectory] 
(
	@pdServerId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @serverDescription As NVARCHAR(400) = (SELECT PDS.ServerDesc FROM ProcessData.tServer PDS WHERE PDS.PDServerID = @pdServerId);
	DECLARE @firstPeriodOccurrence As INTEGER = (SELECT CHARINDEX('.', @serverDescription));
	DECLARE @firstColonOccurrence As INTEGER = (SELECT CHARINDEX(':', @serverDescription));

	RETURN (SELECT FORMATMESSAGE('\\%s\bvopm\server\%s\archive\1min', (SELECT SUBSTRING(@serverDescription, 1, @firstPeriodOccurrence - 1)), (SELECT SUBSTRING(@serverDescription, @firstColonOccurrence + 1, LEN(@serverDescription) - @firstColonOccurrence))));
END
GO

GRANT EXECUTE
	ON OBJECT::[ProcessData].[ufnGetArchiveDirectory] TO [TEUser]
	AS [dbo]
GO