﻿
CREATE FUNCTION [ProcessData].[ufnGetOutputPrioritizedMaps] 
(
	@assetID int,
	@varTypeID int
)
RETURNS 
@prioritizedMaps TABLE 
(
	AssetVariableTypeTagMapID int,
	TagID int,
	ValueTypeID int
)
AS
BEGIN
	DECLARE @outputValTypeID int;
	SET @outputValTypeID = 23;
	DECLARE @measuredValTypeID int;
	SET @measuredValTypeID = 1
	
	INSERT INTO @prioritizedMaps (AssetVariableTypeTagMapID, TagID, ValueTypeID)
	SELECT map.AssetVariableTypeTagMapID, map.TagID, map.ValueTypeID
	FROM ProcessData.tAssetVariableTypeTagMap map
	JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
	WHERE
		map.AssetID = @assetID
		AND map.VariableTypeID = @varTypeID
		AND map.ValueTypeID = @outputValTypeID
		AND map.TagID IS NOT NULL
		AND tag.ExistsOnServer = 1
	
	IF NOT EXISTS(SELECT AssetVariableTypeTagMapID FROM @prioritizedMaps)
		BEGIN
			INSERT INTO @prioritizedMaps (AssetVariableTypeTagMapID, TagID, ValueTypeID)
			SELECT map.AssetVariableTypeTagMapID, map.TagID, map.ValueTypeID
			FROM ProcessData.tAssetVariableTypeTagMap map
			JOIN ProcessData.tTag tag ON map.TagID = tag.PDTagID
			WHERE
				map.AssetID = @assetID
				AND map.VariableTypeID = @varTypeID
				AND (map.ValueTypeID = @measuredValTypeID OR map.ValueTypeID IS NULL)
				AND map.TagID IS NOT NULL
				AND tag.ExistsOnServer = 1
		END
		
	RETURN 
END