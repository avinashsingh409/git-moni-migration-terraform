﻿CREATE FUNCTION [ProcessData].[ufnGetTagIdsByAssetIdsVariableTypeIds]
(
	@securityUserID INT,
	@assetIds Base.tpIntList READONLY,
	@variableTypeIds Base.tpIntList READONLY,
	@valueTypeID INT = NULL
)
RETURNS TABLE AS RETURN
(
	SELECT
		map.AssetVariableTypeTagMapID
		, map.TagID
		, map.AssetID
		, map.VariableTypeID
		, map.ValueTypeID
		, t.TagDesc
		, t.TagName
		, t.PDServerID
	FROM ProcessData.tAssetVariableTypeTagMap map
	JOIN ProcessData.tTag t ON t.PDTagID = map.TagID
	JOIN @assetIds a ON a.id = map.AssetID
	JOIN @variableTypeIds vt ON vt.id = map.VariableTypeID
	JOIN Asset.ufnUserHasAccessToAssets(@securityUserID, @assetIds) acc ON acc.AssetID = a.id
	WHERE @valueTypeID IS NULL OR map.ValueTypeID = @valueTypeID
)
GO
GRANT SELECT
	ON OBJECT::[ProcessData].[ufnGetTagIdsByAssetIdsVariableTypeIds] TO [TEUser]
	AS [dbo];
