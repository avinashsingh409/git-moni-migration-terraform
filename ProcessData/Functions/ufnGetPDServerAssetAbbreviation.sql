﻿CREATE FUNCTION [ProcessData].[ufnGetPDServerAssetAbbreviation]
(
	@pdServerId int
)
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN (SELECT REPLACE(Asset.tAsset.AssetAbbrev, ' ', '') FROM Asset.tAsset WHERE Asset.tAsset.AssetID = (SELECT ProcessData.tServer.AssetID FROM ProcessData.tServer WHERE ProcessData.tServer.PDServerID=@pdServerId));
END
GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[ufnGetPDServerAssetAbbreviation] TO [TEUser]
    AS [dbo]
GO