CREATE FUNCTION [ProcessData].[ufnGetRoleSecuritizedTrendsFromUser] 
(	
	-- Add the parameters for the function here
	@securityUserID int,
	@filterCategoryIDs Base.tpIntList READONLY,
	@filterCategoryTypeID int = NULL
)
RETURNS TABLE AS RETURN
(
	SELECT
		*
	FROM
		ProcessData.ufnGetRoleSecuritizedSpecificCategorizedTrendsFromUser(@securityUserID, @filterCategoryIDs, @filterCategoryTypeID)
	UNION ALL
	SELECT
		DISTINCT(t.PDTrendID)
	FROM
		ProcessData.tTrend t
	LEFT JOIN
		ProcessData.tTrendCategoryMap tCM ON t.PDTrendID = tCM.PDTrendID
	WHERE
		tCM.PDTrendID IS NULL
)
GO

GRANT SELECT
    ON OBJECT::[ProcessData].[ufnGetRoleSecuritizedTrendsFromUser]  TO [TEUser]
    AS [dbo]
GO
