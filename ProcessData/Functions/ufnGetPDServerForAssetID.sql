﻿CREATE FUNCTION [ProcessData].[ufnGetPDServerForAssetID]
(
	@assetID int,
	@serverTypeID int,
	@serverDescContains varchar(255)
)
RETURNS int
AS
BEGIN

declare @result as int
select top 1 @result = b.PDServerID from Asset.ufnAssetAncestorsIncludeAsset(@assetID) a join processdata.tserver b on a.assetid = b.assetid where
b.PDServerTypeID = @serverTypeID and (@serverDescContains is null or @serverDescContains = '' or ServerDesc like '%' + @serverDescContains + '%')

RETURN @result
END

GO

GRANT EXECUTE
    ON OBJECT::[ProcessData].[ufnGetPDServerForAssetID] TO [TEUser]
    AS [dbo];

