CREATE FUNCTION [ProcessData].[ufnGetRoleSecuritizedSpecificCategorizedTrendsFromUser] 
(	
	-- Add the parameters for the function here
	@securityUserID INT,
	@filterCategoryIDs Base.tpIntList READONLY,
	@filterCategoryTypeID int = NULL
)
RETURNS TABLE AS RETURN
(
	SELECT
		DISTINCT(trends.PDTrendID)
	FROM
		ProcessData.tTrend trends
	LEFT JOIN
		ProcessData.tTrendCategoryMap trendCategoryMap ON trends.PDTrendID = trendCategoryMap.PDTrendID	
	INNER JOIN
		AccessControl.ufnGetRoleSecuritizedFilteredCategoriesFromUser(@securityUserId, @filterCategoryIDs, 7) permittedCategories ON trendCategoryMap.CategoryID = permittedCategories.CategoryID
)
GO

GRANT SELECT
    ON OBJECT::[ProcessData].[ufnGetRoleSecuritizedSpecificCategorizedTrendsFromUser]  TO [TEUser]
    AS [dbo]
GO
