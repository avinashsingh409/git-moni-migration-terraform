﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spUpdateISDCountryList]
@Countries [Weather].tpISDCountry READONLY
AS
BEGIN
	DECLARE @LastUpdate datetime = GetDate()

	UPDATE tISDCountry
	SET
		FipsID = C1.FipsID,
		CountryName = C1.CountryName,
		MinDate = C1.MinDate,
		MaxDate = C1.MaxDate,
		StationCount = C1.StationCount,
		Coverage = C1.Coverage,
		LastUpdate = @LastUpdate
	FROM
		@Countries C1
		JOIN [Weather].tISDCountry I1 ON C1.FipsID = I1.FipsID;

	INSERT INTO tISDCountry(
		FipsID,
		CountryName,
		MinDate,
		MaxDate,
		StationCount,
		Coverage,
		LastUpdate )
	SELECT
		C2.FipsID,
		C2.CountryName,
		C2.MinDate,
		C2.MaxDate,
		C2.StationCount,
		C2.Coverage,
		@LastUpdate		
	FROM 
		@Countries C2
		LEFT JOIN [Weather].tISDCountry I2
		ON C2.FipsID = I2.FipsID
	WHERE
		I2.FipsID IS NULL;
	
	DELETE FROM tISDCountry
	WHERE LastUpdate <> @LastUpdate			
END
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spUpdateISDCountryList] TO [TEUser]
    AS [dbo];

