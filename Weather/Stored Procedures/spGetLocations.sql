﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spGetLocations]
AS
SELECT
	LocationID,
	LocationName,
	Latitude,
	Longitude,
	GmtOffset,
	DstOffset
FROM
	[Weather].tLocations
ORDER BY
	LocationName
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spGetLocations] TO [TEUser]
    AS [dbo];

