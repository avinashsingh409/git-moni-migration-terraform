﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spGetClosestSurfaceWeatherStation]

@Latitude float,
@Longitude float

AS

DECLARE @Position geography = geography::STPointFromText('Point(' 
	+ CAST(@Longitude AS NVARCHAR(20)) + ' ' 
	+ CAST(@Latitude AS NVARCHAR(20)) + ')', 4326)

DECLARE	@MinimumDate NVARCHAR(8) = 
	CAST((YEAR(GetDate()) * 10000 + MONTH(GetDate()) * 100) AS NVARCHAR(8))

SELECT TOP 1
	SL.SurfaceWeatherStationID,
	SL.GeoPosition.STDistance(@Position) * .000621371 AS MilesDistant,
	SL.DataSetID,
	SL.StationID,
	SL.Country AS CountryCode,
	IC.CountryName,
	SL.LocationName,
	SL.Latitude,
	SL.Longitude,
	SL.StartDate,
	SL.EndDate,
	SL.DstOffset,
	SL.GmtOffset
FROM
	[Weather].tSurfaceWeatherStationList SL
	JOIN [Weather].tISDCountry IC ON SL.Country = IC.FipsID
WHERE
	YEAR(SL.EndDate) = 2013
ORDER BY
	GeoPosition.STDistance(@Position)
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spGetClosestSurfaceWeatherStation] TO [TEUser]
    AS [dbo];

