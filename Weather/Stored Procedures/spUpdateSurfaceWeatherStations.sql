﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spUpdateSurfaceWeatherStations]
@Stations [Weather].tpSurfaceWeatherStation READONLY
AS
BEGIN
	DECLARE @LastUpdate datetime = GetDate()

	UPDATE tSurfaceWeatherStationList 
	SET
		DataSetID = S1.DataSetID,
		Country = S1.Country,
		LocationName = S1.LocationName,
		Latitude = S1.Latitude,
		Longitude = S1.Longitude,
		StartDate = S1.StartDate,
		EndDate = S1.EndDate,
		GeoPosition = geography::STPointFromText('Point(' + CAST(S1.Longitude AS VARCHAR(20)) 
		+ ' ' + CAST(S1.Latitude AS VARCHAR(20)) + ')', 4326),
		LastUpdate = @LastUpdate
	FROM
		@Stations S1
		JOIN [Weather].tSurfaceWeatherStationList SL1 ON S1.StationID = SL1.StationID;

	INSERT INTO tSurfaceWeatherStationList (
		DataSetID,
		StationID,
		Country,
		LocationName,
		Latitude,
		Longitude,
		StartDate,
		EndDate,
		GeoPosition,
		LastUpdate )
	SELECT
		S2.DataSetID,
		S2.StationID,
		S2.Country,
		S2.LocationName,
		S2.Latitude,
		S2.Longitude,
		S2.StartDate,
		S2.EndDate,		
		geography::STPointFromText('Point(' + CAST(S2.Longitude AS VARCHAR(20)) + ' ' 
			+ CAST(S2.Latitude AS VARCHAR(20)) + ')', 4326),
		@LastUpdate		
	FROM 
		@Stations S2
		LEFT JOIN [Weather].tSurfaceWeatherStationList SL2
		ON S2.StationID = SL2.StationID
	WHERE
		SL2.StationID IS NULL;
	
	DELETE FROM tSurfaceWeatherStationList
	WHERE LastUpdate < @LastUpdate				
		
END
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spUpdateSurfaceWeatherStations] TO [TEUser]
    AS [dbo];

