﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spClosestADDSStation] 

@Latitude float,
@Longitude float

AS

DECLARE @Position geography = geography::STPointFromText('Point(' 
	+ CAST(@Longitude AS NVARCHAR(20)) + ' ' 
	+ CAST(@Latitude AS NVARCHAR(20)) + ')', 4326)

SELECT TOP 1
	ASL.GeoLocation.STDistance(@Position) * .000621371 AS MilesDistant,
	ASL.[station_id],
	ASL.[wmo_id],
	ASL.[latitude],
	ASL.[longitude],
	ASL.[elevation_m],
	ASL.[site],
	ASL.[state],
	ASL.[country],
	ASL.[site_type]
FROM
	[Weather].tAddsStationList ASL
WHERE
	site_type LIKE '%METAR%'
ORDER BY
	ASL.GeoLocation.STDistance(@Position)
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spClosestADDSStation] TO [TEUser]
    AS [dbo];

