﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spUpdateSurfaceWeatherStationTimeZoneOffsets] 
	@SurfaceWeatherStationID int,
	@DstOffset float,
	@GmtOffset float
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [Weather].tSurfaceWeatherStationList
	SET 
		DstOffset = @DstOffset,
		GmtOffset = @GmtOffset
	WHERE
		SurfaceWeatherStationID = @SurfaceWeatherStationID
	
END
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spUpdateSurfaceWeatherStationTimeZoneOffsets] TO [TEUser]
    AS [dbo];

