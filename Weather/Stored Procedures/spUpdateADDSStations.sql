﻿-----------------------------------------------------------------

CREATE PROCEDURE [Weather].[spUpdateADDSStations]
@Stations [Weather].tpADDSStation READONLY
AS
BEGIN
	DECLARE @LastUpdate datetime = GetDate()

	UPDATE tAddsStationList
	SET
		station_id = S1.station_id,
		latitude = S1.latitude,
		longitude = S1.longitude,
		elevation_m = S1.elevation_m,
		[site] = S1.[site],
		[state] = S1.[state],
		country = S1.country,
		site_type = S1.site_type,
		GeoLocation = geography::STPointFromText('Point(' + CAST(S1.longitude AS VARCHAR(20)) + ' ' 
			+ CAST(S1.latitude AS VARCHAR(20)) + ')', 4326),
		LastUpdate = @LastUpdate
	FROM
		@Stations S1
		JOIN [Weather].tAddsStationList A1 ON S1.station_id = A1.station_id;

	INSERT INTO tAddsStationList(
		station_id,
		latitude,
		longitude,
		elevation_m,
		[site],
		[state],
		country,
		site_type,
		GeoLocation,
		LastUpdate )
	SELECT
		S2.station_id,
		S2.latitude,
		S2.longitude,
		S2.elevation_m,
		S2.[site],
		S2.[state],
		S2.country,
		S2.site_type,
		geography::STPointFromText('Point(' + CAST(S2.longitude AS VARCHAR(20)) + ' ' 
			+ CAST(S2.latitude AS VARCHAR(20)) + ')', 4326),
		@LastUpdate		
	FROM 
		@Stations S2
		LEFT JOIN [Weather].tAddsStationList A2
		ON S2.station_id = A2.station_id
	WHERE
		A2.station_id IS NULL;
	
	DELETE FROM tAddsStationList
	WHERE LastUpdate < @LastUpdate		
END
GO
GRANT EXECUTE
    ON OBJECT::[Weather].[spUpdateADDSStations] TO [TEUser]
    AS [dbo];

