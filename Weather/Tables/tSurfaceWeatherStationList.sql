CREATE TABLE [Weather].[tSurfaceWeatherStationList] (
    [SurfaceWeatherStationID] INT               IDENTITY (1, 1) NOT NULL,
    [DataSetID]               VARCHAR (50)      NULL,
    [StationID]               VARCHAR (50)      NOT NULL,
    [Country]                 VARCHAR (50)      NULL,
    [LocationName]            VARCHAR (50)      NULL,
    [Latitude]                VARCHAR (50)      NOT NULL,
    [Longitude]               VARCHAR (50)      NOT NULL,
    [StartDate]               DATETIME          NULL,
    [EndDate]                 DATETIME          NULL,
    [GeoPosition]             [sys].[geography] NULL,
    [DstOffset]               FLOAT (53)        NULL,
    [GmtOffset]               FLOAT (53)        NULL,
    [LastUpdate]              DATETIME          NULL,
    CONSTRAINT [PK_SurfaceWeatherStationList] PRIMARY KEY CLUSTERED ([SurfaceWeatherStationID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Weather', @level1type = N'TABLE', @level1name = N'tSurfaceWeatherStationList';

