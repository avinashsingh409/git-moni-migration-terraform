CREATE TABLE [Weather].[tISDCountry] (
    [ISDCountryID] INT          IDENTITY (1, 1) NOT NULL,
    [FipsID]       VARCHAR (50) NOT NULL,
    [CountryName]  VARCHAR (50) NOT NULL,
    [MinDate]      DATETIME     NULL,
    [MaxDate]      DATETIME     NULL,
    [StationCount] INT          NULL,
    [Coverage]     FLOAT (53)   NULL,
    [LastUpdate]   DATETIME     NULL,
    CONSTRAINT [PK_ISDCountry] PRIMARY KEY CLUSTERED ([ISDCountryID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Weather', @level1type = N'TABLE', @level1name = N'tISDCountry';

