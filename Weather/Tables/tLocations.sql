CREATE TABLE [Weather].[tLocations] (
    [LocationID]   INT           IDENTITY (1, 1) NOT NULL,
    [LocationName] NVARCHAR (50) NOT NULL,
    [Latitude]     FLOAT (53)    NOT NULL,
    [Longitude]    FLOAT (53)    NOT NULL,
    [GmtOffset]    FLOAT (53)    NULL,
    [DstOffset]    FLOAT (53)    NULL,
    CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Weather', @level1type = N'TABLE', @level1name = N'tLocations';

