CREATE TABLE [Weather].[tAddsStationList] (
    [AddsStationID] INT               IDENTITY (1, 1) NOT NULL,
    [station_id]    VARCHAR (50)      NULL,
    [wmo_id]        VARCHAR (50)      NULL,
    [latitude]      FLOAT (53)        NULL,
    [longitude]     FLOAT (53)        NULL,
    [elevation_m]   FLOAT (53)        NULL,
    [site]          VARCHAR (50)      NULL,
    [state]         VARCHAR (50)      NULL,
    [country]       VARCHAR (50)      NULL,
    [site_type]     VARCHAR (50)      NULL,
    [GeoLocation]   [sys].[geography] NULL,
    [LastUpdate]    DATETIME          NULL,
    CONSTRAINT [PK_tAddsStationList] PRIMARY KEY CLUSTERED ([AddsStationID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'BV_IsTypeTable', @value = N'True', @level0type = N'SCHEMA', @level0name = N'Weather', @level1type = N'TABLE', @level1name = N'tAddsStationList';

