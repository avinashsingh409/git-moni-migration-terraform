﻿CREATE TYPE [Weather].[tpSurfaceWeatherStation] AS TABLE (
    [DataSetID]    VARCHAR (50) NULL,
    [StationID]    VARCHAR (50) NOT NULL,
    [Country]      VARCHAR (50) NULL,
    [LocationName] VARCHAR (50) NULL,
    [Latitude]     VARCHAR (50) NOT NULL,
    [Longitude]    VARCHAR (50) NOT NULL,
    [StartDate]    DATETIME     NULL,
    [EndDate]      DATETIME     NULL);


GO
GRANT EXECUTE
    ON TYPE::[Weather].[tpSurfaceWeatherStation] TO [TEUser];

