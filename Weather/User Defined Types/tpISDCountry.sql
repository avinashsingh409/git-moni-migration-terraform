﻿CREATE TYPE [Weather].[tpISDCountry] AS TABLE (
    [FipsID]       VARCHAR (50) NOT NULL,
    [CountryName]  VARCHAR (50) NOT NULL,
    [MinDate]      DATETIME     NULL,
    [MaxDate]      DATETIME     NULL,
    [StationCount] INT          NULL,
    [Coverage]     FLOAT (53)   NULL);


GO
GRANT EXECUTE
    ON TYPE::[Weather].[tpISDCountry] TO [TEUser];

