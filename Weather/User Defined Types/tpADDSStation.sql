﻿CREATE TYPE [Weather].[tpADDSStation] AS TABLE (
    [station_id]  VARCHAR (50) NOT NULL,
    [latitude]    FLOAT (53)   NOT NULL,
    [longitude]   FLOAT (53)   NOT NULL,
    [elevation_m] FLOAT (53)   NULL,
    [site]        VARCHAR (50) NULL,
    [state]       VARCHAR (50) NULL,
    [country]     VARCHAR (50) NULL,
    [site_type]   VARCHAR (50) NULL);


GO
GRANT EXECUTE
    ON TYPE::[Weather].[tpADDSStation] TO [TEUser];

